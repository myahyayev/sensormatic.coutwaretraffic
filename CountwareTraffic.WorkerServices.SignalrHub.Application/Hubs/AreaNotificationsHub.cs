﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public interface IAreaNotificationsHub
    {
        Task JoinAreasProcess(string groupName);
        Task AreaDeletedFailedReceived(AreaDeletedFailed command);
        Task AreaDeletedSuccessfullyReceived(AreaDeletedSuccessfully command);
        Task AreaChangedFailedReceived(AreaChangedFailed command);
        Task AreaChangedSuccessfullyReceived(AreaChangedSuccessfully command);
        Task AreaCreatedFailedReceived(AreaCreatedFailed command);
        Task AreaCreatedSuccessfullyReceived(AreaCreatedSuccessfully command);
    }

    //[Authorize]
    public class AreaNotificationsHub : Hub<IAreaNotificationsHub>
    {
        public async Task JoinAreasProcess(string groupName)
            => await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

        public async Task AreaDeletedFailedReceived(AreaDeletedFailed command)
            => await Clients.Group(command.TenantId.ToString()).AreaDeletedFailedReceived(command);

        public async Task AreaDeletedSuccessfullyReceived(AreaDeletedSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).AreaDeletedSuccessfullyReceived(command);

        public async Task AreaChangedFailedReceived(AreaChangedFailed command)
            => await Clients.Group(command.TenantId.ToString()).AreaChangedFailedReceived(command);

        public async Task AreaChangedSuccessfullyReceived(AreaChangedSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).AreaChangedSuccessfullyReceived(command);

        public async Task AreaCreatedFailedReceived(AreaCreatedFailed command)
            => await Clients.Group(command.TenantId.ToString()).AreaCreatedFailedReceived(command);

        public async Task AreaCreatedSuccessfullyReceived(AreaCreatedSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).AreaCreatedSuccessfullyReceived(command);
    }
}
