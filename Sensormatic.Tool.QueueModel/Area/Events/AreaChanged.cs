﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record AreaChanged : MessageEnvelope, IQueueEvent
    {
        public AreaChanged(Guid areaId, string areaName, string oldAreaName, Guid recordId, 
            TimeSpan? workingHoursStart, TimeSpan? oldWorkingHoursStart, 
            TimeSpan? workingHoursEnd, TimeSpan? oldWorkingHoursEnd,
             string workingTimeZone, string oldWorkingTimeZone,
            Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            AreaId = areaId;
            AreaName = areaName;
            OldAreaName = oldAreaName;
            RecordId = recordId;
            WorkingHoursStart = workingHoursStart;
            OldWorkingHoursStart = oldWorkingHoursStart;
            WorkingHoursEnd = workingHoursEnd;
            OldWorkingHoursEnd = oldWorkingHoursEnd;
            WorkingTimeZone = workingTimeZone;
            OldWorkingTimeZone = oldWorkingTimeZone;
        }
        public Guid AreaId { get; }
        public string AreaName { get; }

        public string OldAreaName { get; }
        public Guid RecordId { get; init; }
        public TimeSpan? WorkingHoursStart { get; }
        public TimeSpan? OldWorkingHoursStart { get; }
        public TimeSpan? WorkingHoursEnd { get; }
        public TimeSpan? OldWorkingHoursEnd { get; }
        public string WorkingTimeZone { get; set; }
        public string OldWorkingTimeZone { get; set; }
    }
}
