﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserPermissions : IQuery<IdentityUserPermissionsResponse>
    {
        public Guid UserId { get; set; }
        public string Language { get; set; }
    }
}
