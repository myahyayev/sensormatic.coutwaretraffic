﻿using CountwareTraffic.Services.Companies.Grpc;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class RegionService : IScopedSelfDependency
    {
        private readonly Region.RegionClient _regionClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;

        public RegionService(Region.RegionClient regionClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _regionClient = regionClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetRegionDetails> GetRegionByIdAsync(Guid regionId)
        {
            GetRegionRequest grpcRequest = new() { RegionId = regionId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_regionClient.GetRegionByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetRegionDetails
            {
                Id = new Guid(grpcResponse.RegionDetail.Id),
                Name = grpcResponse.RegionDetail.Name,
                Description = grpcResponse.RegionDetail.Description,
                CountryId = new Guid(grpcResponse.RegionDetail.CountryId),
                ManagerName = grpcResponse.RegionDetail.ManagerName,
                ManagerId = grpcResponse.RegionDetail.ManagerId == null ? null : new Guid(grpcResponse.RegionDetail.ManagerId),
                AuditCreateBy = new Guid(grpcResponse.RegionDetail.Audit.AuditCreateBy),
                AuditCreateDate = grpcResponse.RegionDetail.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(grpcResponse.RegionDetail.Audit.AuditModifiedBy),
                AuditModifiedDate = grpcResponse.RegionDetail.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
            };
        }

        public async Task<ApiPagedResponse<GetRegionDetails>> GetRegionsAsync(Guid countryId, DataSourceApiRequest paging)
        {
            GetRegionsRequest grpcRequest = new()
            {
                CountryId = countryId.ToString(),
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };


            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Companies.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Companies.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_regionClient.GetRegionsAsync, grpcRequest, hasClientSideLog: false);

            var regions = grpcResponse.RegionDetails.Select(region => new GetRegionDetails
            {
                Id = new Guid(region.Id),
                Name = region.Name,
                Description = region.Description,
                AuditCreateBy = new Guid(region.Audit.AuditCreateBy),
                AuditCreateDate = region.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(region.Audit.AuditModifiedBy),
                AuditModifiedDate = region.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                CountryId = new Guid(region.CountryId),
                ManagerId = region.ManagerId == null ? null : new Guid(region.ManagerId),
                ManagerName = region.ManagerName
            });

            return new ApiPagedResponse<GetRegionDetails>(regions, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task AddRegionAsync(Guid countryId, AddRegionApiRequest request)
        {
            CreateRegionRequest grpcRequest = new()
            {
                CountryId = countryId.ToString(),
                Description = request.Description,
                Name = request.Name,
                ManagerId = request.ManagerId.HasValue ? request.ManagerId.ToString() : null,
                ManagerName = request.ManagerName,
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_regionClient.AddRegionAsync, grpcRequest, hasClientSideLog: true);
        }

        public async Task ChangeRegionAsync(Guid regionId, ChangeRegionApiRequest request)
        {
            UpdateRegionRequest grpcRequest = new()
            {
                RegionId = regionId.ToString(),
                Description = request.Description,
                Name = request.Name,
                ManagerId = request.ManagerId.HasValue ? request.ManagerId.ToString() : null,
                ManagerName = request.ManagerName,
                CountryId = request.CountryId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_regionClient.ChangeRegionAsync, grpcRequest, hasClientSideLog: true);
        }

        public async Task CancelRegionAsync(Guid regionId)
        {
            DeleteRegionRequest grpcRequest = new() {  RegionId = regionId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_regionClient.DeleteRegionAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task<IEnumerable<Hierarchy>> GetHierarchyAsync(Guid regionId)
        {
            GetHierarchyRequest grpcRequest = new() { Id = regionId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_regionClient.GetHierarchyAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.Hierarchies.Select(hierarchy => new Hierarchy
            {
                Id =  new Guid(hierarchy.Id),
                Name = hierarchy.Name,
                 HierarchyLevel = hierarchy.HierarchyLevel switch
                 {
                     global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Default => throw new NotImplementedException(),
                     global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Tenant => HierarchyLevel.Tenant,
                     global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Company => HierarchyLevel.Company,
                     global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Country => HierarchyLevel.Country,
                     global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Region => HierarchyLevel.Region,
                     global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Area => HierarchyLevel.Area,
                     global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.SubArea => HierarchyLevel.SubArea,
                     global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Device => HierarchyLevel.Device,
                     _ => throw new NotImplementedException(),
                 }
            });
        }

        public async Task<Guid> GetRegionCountryIdAsync(Guid regionId)
        {
            GetRegionCountryIdRequest grpcRequest = new() { RegionId = regionId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_regionClient.GetRegionCountryIdAsync, grpcRequest, hasClientSideLog: false);

            return new Guid(grpcResponse.CountryLookupId);
        }

        public async Task<List<Parent>> GetDeviceFilterAsync(Guid parentId)
        {
            KeyValueRequest grpcRequest = new KeyValueRequest
            {
                ParentId = parentId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_regionClient.GetRegionsKeyValueAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.KeyValues.Select(u => new Parent
            {
                Children = null,
                Expanded = true,
                Label = u.Name,
                Data = new Data

                {
                    HierarchyLevel = HierarchyLevel.Region,
                    Id = new Guid(u.Id)
                }
            }).ToList();
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}


