﻿using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure.Services
{
    public class DeviceConnectionStatusProcessorService : IDeviceConnectionStatusProcessorService
    {
        protected readonly IQueueService _queueService;
        protected readonly ICache _redisCacheService;

        public DeviceConnectionStatusProcessorService(ICache redisCacheService, IQueueService queueService)
        {
            _redisCacheService = redisCacheService;
            _queueService = queueService;
        }

        public void Dispose() => GC.SuppressFinalize(this);

        private void CalculateRedisKeysTTL(ref int deviceConnectionStatusRedisKeyTTL, ref int deviceConnecctionStatusShadowRedisKeyTTL, ref DeviceConnectionStatusDto deviceConnectionStatus, ref DeviceDto device)
        {
            deviceConnectionStatusRedisKeyTTL = (device.ControlFrequency == -1) ? (int)deviceConnectionStatus.ExpireTime.Subtract(deviceConnectionStatus.ChangeTime).TotalSeconds : device.ControlFrequency;
            deviceConnecctionStatusShadowRedisKeyTTL = deviceConnectionStatusRedisKeyTTL + 10;
        }

        public virtual async Task ProcessDeviceConnectionStatus(DeviceDto device)
        {
            var deviceConnectionStatusRedisKey = string.Format(CacheKeys.DeviceConnectedStatus, device.Id, device.TenantId);
            var deviceConnectionStatusShadowRedisKey = string.Format(CacheKeys.DeviceConnectedStatusShadow, device.Id, device.TenantId);

            var devicePreviousConnectionStatus = await _redisCacheService.GetAsync<DeviceConnectionStatusDto>(deviceConnectionStatusShadowRedisKey);

            int deviceConnectionStatusRedisKeyTTL = 0, deviceConnectionStatusShadowRedisKeyTTL = 0;

            CalculateRedisKeysTTL(ref deviceConnectionStatusRedisKeyTTL, ref deviceConnectionStatusShadowRedisKeyTTL, ref devicePreviousConnectionStatus, ref device);


           
            if (device.ControlFrequency != -1) // if command comes from device connection
            {

                if (devicePreviousConnectionStatus is null || (!devicePreviousConnectionStatus.Status.Equals(DeviceStatus.Connected)))
                {
                    devicePreviousConnectionStatus = new
                        DeviceConnectionStatusDto()
                    {
                        Status = DeviceStatus.Connected,
                        ExpireTime = DateTime.UtcNow.AddSeconds(deviceConnectionStatusRedisKeyTTL),
                        DeviceId = device.Id,

                    };

                }
                else
                    return;

            }
            else if (device.ControlFrequency == -1)// if command comes from redis expire
            {
                //next stage is disconnected
                if (devicePreviousConnectionStatus.Status.Equals(DeviceStatus.Connected))
                {
                    //send disconnected
                    devicePreviousConnectionStatus = devicePreviousConnectionStatus with
                    { Status = DeviceStatus.DisConnected, ExpireTime = DateTime.UtcNow.AddDays(2) };

                }
                //next stage is Broken
                else if (devicePreviousConnectionStatus.Status.Equals(DeviceStatus.DisConnected))
                {
                    //send broken
                    devicePreviousConnectionStatus = devicePreviousConnectionStatus with
                    { Status = DeviceStatus.Broken, ExpireTime = DateTime.UtcNow };

                }
                else
                    return;

            }
                
           
            //refresh with new data
            CalculateRedisKeysTTL(ref deviceConnectionStatusRedisKeyTTL, ref deviceConnectionStatusShadowRedisKeyTTL, ref devicePreviousConnectionStatus, ref device);

            await _redisCacheService.AddAsync(deviceConnectionStatusRedisKey, devicePreviousConnectionStatus, expireTime: new TimeSpan(0, 0, deviceConnectionStatusRedisKeyTTL), @override: true);

            await _redisCacheService.AddAsync(deviceConnectionStatusShadowRedisKey, devicePreviousConnectionStatus, expireTime: new TimeSpan(0, 0, deviceConnectionStatusShadowRedisKeyTTL), @override: true);


            _queueService.Publish(new Sensormatic.Tool.QueueModel.DeviceStatusChanged(device.Id, devicePreviousConnectionStatus.Status.Id, Guid.Empty, Guid.Empty.ToString(), device.TenantId));

        }
    }
}
