﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Devices.Application
{
    [Contract]
    public class UpdateBrandModelLookup : ICommand
    {
        public Guid BrandModelId { get; set; }
        public string Name { get; set; }
    }
}