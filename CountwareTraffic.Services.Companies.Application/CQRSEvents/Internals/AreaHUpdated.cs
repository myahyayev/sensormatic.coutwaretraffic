﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaHUpdated : Convey.CQRS.Events.IEvent
    {
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid RegionId { get; set; }
        public Guid CountryId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
        public TimeSpan? WorkingHoursStart { get; set; }
        public TimeSpan? WorkingHoursEnd { get; set; }
    }
}
