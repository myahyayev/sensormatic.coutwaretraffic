﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public class GetVisitorDuration : IQuery<VisitorDurationDto>
    {
        public List<NodeFilter> NodeFilters { get; set; }
        public DateTime CurrentStartDate { get; set; }
    }
}
