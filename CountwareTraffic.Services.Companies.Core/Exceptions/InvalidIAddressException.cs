﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Core
{
    public class InvalidIAddressException : DomainException
    {
        public string Field { get; set; }
        public string Value { get; }
        public InvalidIAddressException(string field, string value)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1422 Address info invalid format. {field}-{value} ") }, 400, ResponseMessageType.Error)
        {
            Field = field;
            Value = value;
        }
    }
}
