﻿using Convey.CQRS.Queries;

namespace CountwareTraffic.Services.Events.Application
{
    public class GetDeviceFormIpAddressAndMac  : IQuery<DeviceDto>
    {
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
    }
}
