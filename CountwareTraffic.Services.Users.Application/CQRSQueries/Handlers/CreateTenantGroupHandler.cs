﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreateTenantGroupHandler : IQueryHandler<CreateTenantGroup, IdentityCreateTenantGroupResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public CreateTenantGroupHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityCreateTenantGroupResponse> HandleAsync(CreateTenantGroup query)
        {
            return await _identityServerService.CreateTenantGroupAsync(new IdentityCreateTenantGroupRequest
            {
                Name = query.Name,
                Description = query.Description
            });
        }
    }
}
