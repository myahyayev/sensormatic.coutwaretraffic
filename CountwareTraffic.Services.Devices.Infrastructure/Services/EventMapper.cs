﻿using CountwareTraffic.Services.Devices.Application;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class EventMapper : IEventMapper
    {
        public IEnumerable<Convey.CQRS.Events.IEvent> MapAll(IEnumerable<Sensormatic.Tool.Queue.IQueueEvent> events)
              => events.Select(Map);

        public Convey.CQRS.Events.IEvent Map(Sensormatic.Tool.Queue.IQueueEvent @event)
        {
            switch (@event)
            {
                case Sensormatic.Tool.QueueModel.SubAreaCreated e:
                    return new SubAreaCreated
                    {
                        Name = e.Name,
                        SubAreaId = e.SubAreaId,
                        UserId = e.UserId,
                        CorrelationId = e.CorrelationId,
                        TenantId = e.TenantId,
                        AreaId = e.AreaId,
                        AreaName = e.AreaName,
                        CityId = e.CityId,
                        CityName = e.CityName,
                        CompanyId = e.CompanyId,
                        CountryLookupId = e.CountryLookupId,
                        CompanyName = e.CompanyName,
                        CountryId = e.CountryId,
                        CountryName = e.CountryName,
                        DistrictId = e.DistrictId,
                        DistrictName = e.DistrictName,
                        RegionId = e.RegionId,
                        RegionName = e.RegionName
                    };


                case Sensormatic.Tool.QueueModel.SubAreaChanged e:
                    return new SubAreaChanged
                    {
                        OldName = e.OldName,
                        Name = e.Name,
                        SubAreaId = e.SubAreaId,
                        UserId = e.UserId,
                        CorrelationId = e.CorrelationId,
                        TenantId = e.TenantId
                    };


                case Sensormatic.Tool.QueueModel.SubAreaDeleted e:
                    return new SubAreaDeleted
                    {
                        SubAreaId = e.SubAreaId,
                        UserId = e.UserId,
                        CorrelationId = e.CorrelationId,
                        TenantId = e.TenantId,
                    };


                case Sensormatic.Tool.QueueModel.DeviceChangedRejected e:
                    return new DeviceChangedRejected
                    {
                        DeviceId = e.DeviceId,
                        Name = e.Name,
                        OldName = e.OldName,
                        UserId = e.UserId,
                        CorrelationId = e.CorrelationId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.DeviceCreatedCompleted e:
                    return new DeviceCreatedCompleted
                    {
                        DeviceId = e.DeviceId,
                        Name = e.Name,
                        UserId = e.UserId,
                        CorrelationId = e.CorrelationId,
                        TenantId = e.TenantId,
                    };

                case Sensormatic.Tool.QueueModel.DeviceCreatedRejected e:
                    return new DeviceCreatedRejected
                    {
                        DeviceId = e.DeviceId,
                        UserId = e.UserId,
                        CorrelationId = e.CorrelationId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.DeviceDeletedRejected e:
                    return new DeviceDeletedRejected
                    {
                        DeviceId = e.DeviceId,
                        UserId = e.UserId,
                        CorrelationId = e.CorrelationId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.DeviceStatusChanged e:
                    return new DeviceStatusChanged
                    {
                        DeviceId = e.DeviceId,
                        RecordId = e.RecordId,
                        DeviceStatus = e.DeviceStatus,
                        CorrelationId = e.CorrelationId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.DeviceHCreated e:
                    return new DeviceHCreated
                    {
                        DeviceId = e.DeviceId,
                        DeviceName = e.DeviceName,
                        SubAreaId = e.SubAreaId,
                        AreaId = e.AreaId,
                        RegionId = e.RegionId,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId
                    };

                case Sensormatic.Tool.QueueModel.DeviceHUpdated e:
                    return new DeviceHUpdated
                    {
                        DeviceId = e.DeviceId,
                        DeviceName = e.DeviceName,
                        SubAreaId = e.SubAreaId,
                        AreaId = e.AreaId,
                        RegionId = e.RegionId,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId
                    };

                case Sensormatic.Tool.QueueModel.DeviceHDeleted e:
                    return new DeviceHDeleted
                    {
                        DeviceId = e.DeviceId,
                        DeviceName = e.DeviceName,
                        SubAreaId = e.SubAreaId,
                        AreaId = e.AreaId,
                        RegionId = e.RegionId,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId
                    };
            }

            return null;
        }
    }
}
