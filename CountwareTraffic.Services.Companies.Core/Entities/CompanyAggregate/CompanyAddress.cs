﻿using NetTopologySuite.Geometries;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Efcore;
using System.Reflection;

namespace CountwareTraffic.Services.Companies.Core
{
    public record CompanyAddress : IDeletableValueObject
    {
        private string _street;
        private string _city;
        private string _state;
        private string _country;
        private string _zipCode;
        private Point _location;

        public string Street => _street;
        public string City => _city;
        public string State => _state;
        public string Country => _country;
        public string ZipCode => _zipCode;
        public Point Location => _location;


        private CompanyAddress() { }

        public static CompanyAddress Create(string street, string city, string state, string country, string zipCode, double latitude, double longitude)
        {
            if (!city.IsNullOrWhiteSpace() && city.Length > 100)
                throw new InvalidIAddressException(nameof(city), city);

            if (!street.IsNullOrWhiteSpace() && street.Length > 250)
                throw new InvalidIAddressException(nameof(street), street);

            if (!state.IsNullOrWhiteSpace() && state.Length > 250)
                throw new InvalidIAddressException(nameof(state), state);

            if (!country.IsNullOrWhiteSpace() && country.Length > 100)
                throw new InvalidIAddressException(nameof(country), country);

            if (!zipCode.IsNullOrWhiteSpace() && zipCode.Length > 20)
                throw new InvalidIAddressException(nameof(zipCode), zipCode);

            return new CompanyAddress
            {
                _street = street,
                _city = city,
                _state = state,
                _country = country,
                _zipCode = zipCode,
                _location = CreateLoaction(latitude, longitude)
            };
        }

        public void WhenChanged(string street, string city, string state, string country, string zipCode, double latitude, double longitude)
        {
            if (!city.IsNullOrWhiteSpace() && city.Length > 100)
                throw new InvalidIAddressException(nameof(city), city);

            if (!street.IsNullOrWhiteSpace() && street.Length > 250)
                throw new InvalidIAddressException(nameof(street), street);

            if (!state.IsNullOrWhiteSpace() && state.Length > 250)
                throw new InvalidIAddressException(nameof(state), state);

            if (!country.IsNullOrWhiteSpace() && country.Length > 100)
                throw new InvalidIAddressException(nameof(country), country);

            if (!zipCode.IsNullOrWhiteSpace() && zipCode.Length > 20)
                throw new InvalidIAddressException(nameof(zipCode), zipCode);

            _street = street;
            _city = city;
            _state = state;
            _country = country;
            _zipCode = zipCode;
            _location = CreateLoaction(latitude, longitude);
        }

        private static Point CreateLoaction(double latitude, double longitude)
        {
            Coordinate coordinate = new() { X = longitude, Y = latitude };

            Point point = new(coordinate);

            point.SRID = 4326;

            return point;
        }
    }
}
