﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Sensormatic.Tool.Common
{
    public class TimeZoneValues
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("abbr")]
        public string Abbr { get; set; }

        [JsonProperty("offset")]
        public double Offset { get; set; }

        [JsonProperty("isdst")]
        public bool Isdst { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("utc")]
        public List<string> Utc { get; set; }
    }
}
