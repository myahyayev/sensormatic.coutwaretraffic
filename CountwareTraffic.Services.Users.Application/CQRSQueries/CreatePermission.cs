﻿using Convey.CQRS.Queries;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreatePermission : IQuery<IdentityCreatePermissionResponse>
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
