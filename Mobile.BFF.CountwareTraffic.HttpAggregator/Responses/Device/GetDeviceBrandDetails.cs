﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetDeviceBrandDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
