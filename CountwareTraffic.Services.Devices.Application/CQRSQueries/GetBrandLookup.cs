﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class GetBrandLookup : IQuery<BrandLookupDetailsDto>
    {
        public Guid BrandId { get; set; }
    }
}
