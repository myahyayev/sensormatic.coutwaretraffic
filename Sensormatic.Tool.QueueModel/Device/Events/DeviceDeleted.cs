﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceDeleted : MessageEnvelope, IQueueEvent
    {
        public DeviceDeleted(Guid deviceId, string name, Guid recordId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            DeviceId = deviceId;
            Name = name;
            RecordId = recordId;
        }
        public Guid DeviceId { get; }
        public Guid RecordId { get; init; }
        public string Name { get; }
    }
}
