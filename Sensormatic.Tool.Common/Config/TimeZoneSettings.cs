﻿using Sensormatic.Tool.Ioc;

namespace Sensormatic.Tool.Common
{
    public class TimeZoneSettings : IConfigurationOptions
    {
        public string DefaultProvider { get; set; }
        public bool ProvideByLinux { get; set; }
    }
}
