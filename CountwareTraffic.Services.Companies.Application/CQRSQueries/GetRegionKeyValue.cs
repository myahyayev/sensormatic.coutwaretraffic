﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetRegionKeyValue : IQuery<List<KeyValueDto>>
    {
        public Guid ParentId { get; set; }
    }
}
