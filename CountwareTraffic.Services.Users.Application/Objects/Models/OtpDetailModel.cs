﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class OtpDetailModel
    {
        [JsonPropertyName("reference_text")]
        public string ReferenceText { get; set; }
    }
}
