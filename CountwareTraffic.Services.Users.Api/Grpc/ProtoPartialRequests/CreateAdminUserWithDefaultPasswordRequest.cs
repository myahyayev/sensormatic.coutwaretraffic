﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace CountwareTraffic.Services.Users.Grpc
{
    [ServiceLog]
    public sealed partial class CreateAdminUserWithDefaultPasswordRequest : SensormaticRequestValidate
    {
        public override void Validate()
        {
            if (UserName.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1467 Value cannot be null", nameof(UserName)));
            if (!UserName.IsValidUserName())
                ValidateResults.Add(new ErrorResult($"#E1468 Value is invalid", nameof(UserName)));
            if (UserName.Length < 3 || UserName.Length > 50)
                ValidateResults.Add(new ErrorResult($"#E1469 UserName length must be 3-50 character", nameof(UserName)));
            if (!PhoneNumber.IsValidMobilePhone())
                ValidateResults.Add(new ErrorResult($"#E1470 Phone Number is invalid 05XXXXXXXXX", nameof(PhoneNumber)));
            if (LastName.Length < 3)
                ValidateResults.Add(new ErrorResult($"#E1563 Last Name Value length must be at least three characters", nameof(LastName)));

        }
    }
}