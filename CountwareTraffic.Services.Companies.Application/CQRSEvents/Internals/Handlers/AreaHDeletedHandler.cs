﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaHDeletedHandler : IEventHandler<AreaHDeleted>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public AreaHDeletedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(AreaHDeleted command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId
            };

            CountryElasticDto countryElasticDto = new()
            {
                Id = command.CountryId
            };

            RegionElasticDto regionElasticDto = new()
            {
                Id = command.RegionId
            };

            AreaElasticDto areaElasticDto = new()
            {
                Id = command.AreaId,
                Name = command.AreaName
            };

            await _tenantHierarchyElasticSearchRepository.DeleteAreaAsync(tenantElasticDto, companyElasticDto, countryElasticDto, regionElasticDto, areaElasticDto);
        }
    }
}

