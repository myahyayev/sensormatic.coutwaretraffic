﻿using Microsoft.AspNetCore.Mvc;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class CreateUserApiRequest: SensormaticRequestValidate
    {
        [FromForm(Name = "id")]
        public Guid Id { get; set; }

        [FromForm(Name = "firstName")]
        public string FirstName { get; set; }

        [FromForm(Name = "lastName")]
        public string LastName { get; set; }

        [FromForm(Name = "email")]
        public string Email { get; set; }

        [FromForm(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [FromForm(Name = "userName")]
        public string UserName { get; set; }

        [FromForm(Name = "status")]
        public bool Status { get; set; }

        [FromForm(Name = "twoFactorEnabled")]
        public bool TwoFactorEnabled { get; set; }

        [FromForm(Name = "gender")]
        public int Gender { get; set; }

        [FromForm(Name = "birthdate")]
        public DateTime Birthdate { get; set; }

        [FromForm(Name = "userType")]
        public int UserType { get; set; }

        [FromForm(Name = "address")]
        public string Address { get; set; }

        [FromForm(Name = "allowSms")]
        public bool AllowSms { get; set; }

        [FromForm(Name = "allowEmail")]
        public bool AllowEmail { get; set; }

        [FromForm(Name = "profilePic")]
        public string ProfilePic { get; set; }

        [FromForm(Name = "areas")]
        public List<AreaDetailApi> Areas { get; set; }

        [FromForm(Name = "roles")]
        public List<string> Roles { get; set; }

        [FromForm(Name = "password")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string Password { get; set; }

        [FromForm(Name = "confirmPassword")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string ConfirmPassword { get; set; }

        public override void Validate()
        {
            if (LastName.Length<3)
                ValidateResults.Add(new ErrorResult($"#E1563 Last Name Value length must be at least three characters", nameof(LastName)));
            if (!PhoneNumber.IsValidMobilePhone())
                ValidateResults.Add(new ErrorResult($"#E1470 Phone Number is invalid 05XXXXXXXXX", nameof(PhoneNumber)));
        }
    }
}
