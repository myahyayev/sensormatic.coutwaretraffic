﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    public class AreaSubscriber : BackgroundService
    {
        private readonly IQueueService _queueService;
        private readonly ILogger<AreaSubscriber> _logger;

        public AreaSubscriber(ILogger<AreaSubscriber> logger, IQueueService queueService)
        {
            _logger = logger;
            _queueService = queueService;
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var template = new QueueConfigTemplate
            {
                PrefetchCount = 1,
                RetryCount = 0,
                RetryIntervalSeconds = 0,
                ExcludeExceptions = new List<Type> { typeof(Exception), typeof(HttpRequestException), typeof(ArgumentNullException) },
                AutoScale = true,
                ScaleUpTo = 10
            };

            _queueService.Subscribe<AreaCreatedConsumer>(Queues.CountwareTrafficCompaniesAreaCreated, template);
            _queueService.Subscribe<AreaChangedConsumer>(Queues.CountwareTrafficCompaniesAreaChanged, template);
            _queueService.Subscribe<AreaDeletedConsumer>(Queues.CountwareTrafficCompaniesAreaDeleted, template);

            return Task.CompletedTask;
        }

        public async override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Area forcing to stop...");

            _queueService.StopConsumers();

            await Task.Delay(5000);

            await base.StopAsync(cancellationToken);
        }
    }
}
