﻿using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application
{
    public class VerifyTokenModel
    {
        [JsonPropertyName("verify_token")]
        public string VerifyToken { get; set; }


        [JsonPropertyName("authority")]
        public string Authority { get; set; }


        [JsonPropertyName("parameters")]
        public string[] Parameters { get; set; }


        [JsonPropertyName("timeOut")]
        public int TimeOut { get; set; }
    }
}
