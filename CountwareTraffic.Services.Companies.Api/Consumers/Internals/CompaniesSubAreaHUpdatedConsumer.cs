﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesSubAreaHUpdatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.SubAreaHUpdated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesSubAreaHUpdatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.SubAreaHUpdated queuEvent)
        {
            var subAreaHUpdated = _eventMapper.Map(queuEvent) as SubAreaHUpdated;
            await _eventDispatcher.PublishAsync(subAreaHUpdated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
