﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class RoleAdminPaginationHandler : IQueryHandler<RoleAdminPagination, IdentityRoleAdminPaginationResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public RoleAdminPaginationHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;
        public async Task<IdentityRoleAdminPaginationResponse> HandleAsync(RoleAdminPagination query)
        {
            return await _identityServerService.RoleAdminPermissionPaginationRequestAsync(new Objects.Requests.IdentityRoleAdminPaginationRequest
            {
                Filters = query.Filters,
                Paging = query.Paging,
                Sorts = query.Sorts,
            }, query.Language);
        }
    }
}
