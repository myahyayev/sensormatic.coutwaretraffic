﻿namespace CountwareTraffic.Services.Devices.Core
{
    public class DeviceConnectionInfoEntityDto
    {
        public string IpAddress { get; set; }
        public int Port { get; set; }
        public string Identity { get; set; }
        public string Password { get; set; }
        public string UniqueId { get; set; }
        public string MacAddress { get; set; }
    }
}
