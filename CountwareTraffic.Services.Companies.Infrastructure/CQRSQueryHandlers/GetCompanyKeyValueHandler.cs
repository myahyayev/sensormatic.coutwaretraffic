﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetCompanyKeyValueHandler : IQueryHandler<GetCompanyKeyValue, List<KeyValueDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetCompanyKeyValueHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<KeyValueDto>> HandleAsync(GetCompanyKeyValue query)
        {
            var companyRepository = _unitOfWork.GetRepository<ICompanyRepository>();

            var keyValues = await companyRepository.GetAllKeyValueAsync();

            return keyValues.Select(u => new KeyValueDto
            {
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }
}


