﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class CityLookup : Entity<Guid>
    {
        private string _name;
        private Guid _countryId;

        public string Name => _name;
        public Guid CountryId => _countryId;

        private CityLookup()
        {
            _id = SequentialGuid.Current.Next(null);
        }
    }
}
