﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class GroupAddUser : IQuery<IdentityGroupAddUserResponse>
    {
        public Guid UserId { get; set; }
        public Guid GroupId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
