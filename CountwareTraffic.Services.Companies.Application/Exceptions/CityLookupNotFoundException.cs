﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CityLookupNotFoundException : AppException
    {
        public Guid Id { get; }
        public CityLookupNotFoundException(Guid id)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1402 City with id: {id} was not found") }, 404, ResponseMessageType.Error)
        {
            Id = id;
        }
    }
}
