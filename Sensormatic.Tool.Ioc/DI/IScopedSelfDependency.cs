﻿using System;

namespace Sensormatic.Tool.Ioc
{
    public interface IScopedSelfDependency : IDisposable { }
}
