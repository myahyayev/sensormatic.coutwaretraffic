﻿using Convey.CQRS.Queries;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class GetReportLanguages : IQuery<IEnumerable<ReportLanguageDto>>
    {
    }
}
