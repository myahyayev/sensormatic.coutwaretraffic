﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Devices.Infrastructure.Migrations
{
    public partial class DeleteICollectionDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Devices_SubAreas_SubAreaId1",
                schema: "devices",
                table: "Devices");

            migrationBuilder.DropIndex(
                name: "IX_Devices_SubAreaId1",
                schema: "devices",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "SubAreaId1",
                schema: "devices",
                table: "Devices");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "SubAreaId1",
                schema: "devices",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Devices_SubAreaId1",
                schema: "devices",
                table: "Devices",
                column: "SubAreaId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Devices_SubAreas_SubAreaId1",
                schema: "devices",
                table: "Devices",
                column: "SubAreaId1",
                principalSchema: "devices",
                principalTable: "SubAreas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
