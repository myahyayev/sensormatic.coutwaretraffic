﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record OneTimeReportGenerate : MessageEnvelope, IQueueEvent
    {
        public OneTimeReportGenerate(Guid reportId, string userName, Guid recordId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            ReportId = reportId;
            RecordId = recordId;
            UserName = userName;
        }

        public Guid ReportId { get; }
        public Guid RecordId { get; init; }
        public string UserName { get; set; }
    }
}
