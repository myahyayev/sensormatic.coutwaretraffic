﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityCreateTenantRequest
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("erpCode")]
        public string ErpCode { get; set; }

        [JsonPropertyName("erpGroupName")]
        public string ErpGroupName { get; set; }

        [JsonPropertyName("erpStartDate")]
        public DateTime ErpStartDate { get; set; }

        [JsonPropertyName("erpEndDate")]
        public DateTime ErpEndDate { get; set; }

        [JsonPropertyName("tenantGroupId")]
        public Guid TenantGroupId { get; set; }
    }
}
