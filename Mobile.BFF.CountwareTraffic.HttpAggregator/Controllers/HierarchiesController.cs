﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class HierarchiesController : BaseController
    {
        private readonly CompanyService _companyService;
        private readonly CountryService _countryService;
        private readonly RegionService _regionService;
        private readonly AreaService _areaService;
        private readonly SubAreaService _subAreaService;
        private readonly AllHierarchyService _hierarchyTreeService;
        private readonly DeviceService _deviceService;
     //   private readonly UserService _userService;

        public HierarchiesController(IServiceProvider provider, ILogger<HierarchiesController> logger, IHttpContextAccessor contextAccessor, CompanyService companyService, CountryService countryService, RegionService regionService, AreaService areaService, SubAreaService subAreaService, AllHierarchyService hierarchyTreeService, DeviceService deviceService, IHttpContextAccessor httpContextAccessor, UserService userService)
            : base(provider, logger, contextAccessor)
        {
            _companyService = companyService;
            _countryService = countryService;
            _areaService = areaService;
            _regionService = regionService;
            _subAreaService = subAreaService;
            _hierarchyTreeService = hierarchyTreeService;
            _deviceService = deviceService;
          //  _userService = userService;
        }


        [HttpGet("bylevel"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminOrganizationChartView })]
        public async Task<ApiResponse<List<Hierarchy>>> GetHierarchyAsync([FromQuery] HierarchyRequest hierarchyRequest)
        {



            Hierarchy defaultHierarchy = new() { HierarchyLevel = HierarchyLevel.Tenant, Id = (!TenantId.IsNullOrWhiteSpace() ? new Guid(TenantId) : Guid.Empty), Name = TenantName };

            return hierarchyRequest.HierarchyLevel switch
            {
                HierarchyLevel.Tenant =>
                Response(new List<Hierarchy>() { defaultHierarchy }, new ResultMessage("#S1081")),

                HierarchyLevel.Company =>
                Response((await _companyService.GetHierarchyAsync(hierarchyRequest.Id)).ToList(), new ResultMessage("#S1082")),

                HierarchyLevel.Country =>
                Response((await _countryService.GetHierarchyAsync(hierarchyRequest.Id)).ToList(), new ResultMessage("#S1083")),

                HierarchyLevel.Region =>
                Response((await _regionService.GetHierarchyAsync(hierarchyRequest.Id)).ToList(), new ResultMessage("#S1084")),

                HierarchyLevel.Area =>
                Response((await _areaService.GetHierarchyAsync(hierarchyRequest.Id)).ToList(), new ResultMessage("#S1085")),

                HierarchyLevel.SubArea =>
                Response((await _subAreaService.GetHierarchyAsync(hierarchyRequest.Id)).ToList(), new ResultMessage("#S1086")),

                HierarchyLevel.Device =>
                Response((await _deviceService.GetHierarchyAsync(hierarchyRequest.Id)).ToList(), new ResultMessage("#S1087")),
                _ => throw new NotImplementedException(),
            };
        }


        [HttpGet("device-filter"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminOrganizationChartView, Permissions.OrganizationChartView })]
        public async Task<ApiResponse<List<Parent>>> GetDeviceFilterAsync([FromQuery] DeviceFilterRequest deviceFilterRequest)
        {
            Parent defaultHierarchy = new()
            {
                Children = null,
                Expanded = true,
                Label = TenantName,
                Data = new Data
                {
                    HierarchyLevel = HierarchyLevel.Tenant,
                    Id = !TenantId.IsNullOrWhiteSpace() ? new Guid(TenantId) : Guid.Empty,
                }
            };


            return deviceFilterRequest.HierarchyLevel switch
            {

                HierarchyLevel.Tenant =>
                Response(new List<Parent>() { defaultHierarchy }, new ResultMessage("#S1088")),

                HierarchyLevel.Company =>
                Response((await _companyService.GetDeviceFilterAsync()).ToList(), new ResultMessage("#S1088")),

                HierarchyLevel.Country =>
                Response((await _countryService.GetDeviceFilterAsync(deviceFilterRequest.ParentId)).ToList(), new ResultMessage("#S1089")),

                HierarchyLevel.Region =>
                Response((await _regionService.GetDeviceFilterAsync(deviceFilterRequest.ParentId)).ToList(), new ResultMessage("#S1090")),

                HierarchyLevel.Area =>
                Response((await _areaService.GetDeviceFilterAsync(deviceFilterRequest.ParentId)).ToList(), new ResultMessage("#S1091")),

                HierarchyLevel.SubArea =>
                Response((await _subAreaService.GetDeviceFilterAsync(deviceFilterRequest.ParentId)).ToList(), new ResultMessage("#S1092")),

                HierarchyLevel.Device => throw new NotImplementedException(),
                _ => throw new NotImplementedException(),
            };
        }


        [HttpGet("tree")]
        [Permissions(Permissions = new[] { Permissions.AdminOrganizationChartView, Permissions.OrganizationChartView })]
        public async Task<ApiResponse<Parent>> GetHierarchyTreeAsync()
             => Response(await _hierarchyTreeService.GetAllHierarchyAsync(), new ResultMessage("#S1093"));
    }
}


