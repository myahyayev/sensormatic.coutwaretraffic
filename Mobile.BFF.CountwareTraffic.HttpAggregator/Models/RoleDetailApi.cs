﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class RoleDetailApi
    {
        [FromForm(Name = "id")]
        public Guid Id { get; set; }

        [FromForm(Name = "name")]
        public string Name { get; set; }
    }
}
