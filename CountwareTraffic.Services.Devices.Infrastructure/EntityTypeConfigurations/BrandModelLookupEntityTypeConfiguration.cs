﻿using CountwareTraffic.Services.Devices.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class BrandModelLookupEntityTypeConfiguration : IEntityTypeConfiguration<BrandModelLookup>
    {
        public void Configure(EntityTypeBuilder<BrandModelLookup> builder)
        {
            builder.ToTable("BrandModelLookups", SchemaNames.Devices);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder.HasOne<BrandLookup>().WithMany().HasForeignKey(x => x.BrandId);
        }
    }
}
