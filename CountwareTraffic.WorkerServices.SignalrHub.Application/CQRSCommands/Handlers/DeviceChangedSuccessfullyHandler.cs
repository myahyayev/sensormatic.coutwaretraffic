﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class DeviceChangedSuccessfullyHandler : ICommandHandler<DeviceChangedSuccessfully>
    {
        private readonly IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> _hubContext;
        public DeviceChangedSuccessfullyHandler(IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> hubContext) 
            => _hubContext = hubContext;


        public async Task HandleAsync(DeviceChangedSuccessfully command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .DeviceChangedSuccessfullyReceived(command);
    }
}
