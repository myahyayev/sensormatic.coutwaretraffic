﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class CreateBrandLookupHandler : ICommandHandler<CreateBrandLookup>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateBrandLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task HandleAsync(CreateBrandLookup command)
        {
            var brandRepository = _unitOfWork.GetRepository<IBrandLookupRepository>();

            var brand =  await brandRepository.GetByNameAsync(command.Name);

            if (brand is not null)
                throw new BrandAlreadyExistsException(command.Name);

            var brandLookup =  BrandLookup.Create(command.Name);

            await brandRepository.AddAsync(brandLookup);
            await _unitOfWork.CommitAsync();
        }
    }
}
