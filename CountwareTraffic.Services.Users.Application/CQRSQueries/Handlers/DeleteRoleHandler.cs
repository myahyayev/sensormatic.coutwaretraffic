﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteRoleHandler : IQueryHandler<DeleteRole, IdentityDeleteRoleResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public DeleteRoleHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityDeleteRoleResponse> HandleAsync(DeleteRole query)
        {
            return await _identityServerService.DeleteRoleAsync(query.Id);
        }
    }
}
