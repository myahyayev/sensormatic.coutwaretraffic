﻿using CountwareTraffic.Services.Events.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class AreaEntityTypeConfiguration : IEntityTypeConfiguration<Area>
    {
        public void Configure(EntityTypeBuilder<Area> builder)
        {
            builder.ToTable("Areas", SchemaNames.Events);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired();

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
                .Property(x => x.WorkingHoursStart)
                .HasField("_workingHoursStart")
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
                .Property(x => x.WorkingHoursEnd)
                .HasField("_workingHoursEnd")
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.WorkingTimeZone)
               .HasField("_workingTimeZone")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .HasMaxLength(255); 
        }
    }
}