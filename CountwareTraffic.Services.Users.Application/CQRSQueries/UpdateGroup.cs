﻿using Convey.CQRS.Queries;

namespace CountwareTraffic.Services.Users.Application
{
    public class UpdateGroup : IQuery<IdentityUpdateGroupResponse>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
