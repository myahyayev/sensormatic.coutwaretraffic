﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Devices.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeviceChangedRejectedHandler : IEventHandler<DeviceChangedRejected>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public DeviceChangedRejectedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(DeviceChangedRejected command)
        {
            var deviceRepository = _unitOfWork.GetRepository<IDeviceRepository>();

            var device = await deviceRepository.GetAsync(command.DeviceId, command.TenantId);

            device.WhenChangedRejected(command.OldName);

            await _unitOfWork.CommitAsync();

            _queueService.Publish(new DeviceChangedFailed
            {
                RecordId = Guid.NewGuid(),
                DeviceId = command.DeviceId,
                OldName = command.OldName,
                UserId = command.UserId,
                NewName = command.Name,
                UserName = String.Empty,
                TenantId = command.TenantId
            });
        }
    }
}
