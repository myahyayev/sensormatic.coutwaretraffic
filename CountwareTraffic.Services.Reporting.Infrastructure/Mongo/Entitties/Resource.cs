﻿using MongoDB.Bson;
using Sensormatic.Tool.MongoDb;
using System;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class Resource : MongoDbEntity
    {
        public static Resource Create(Guid languageId, string resouceKey, string content, string assemblyQualifiedName)
        {
            return new Resource
            {
                Id = ObjectId.GenerateNewId().ToString(),
                LanguageId = languageId.ToString(),
                ResouceKey = resouceKey,
                Content = content,
                AssemblyQualifiedName = assemblyQualifiedName,
            };
        }
        public string LanguageId { get; init; }
        public string ResouceKey { get; init; }
        public string Content { get; init; }
        public string AssemblyQualifiedName { get; set; }
    }
}
