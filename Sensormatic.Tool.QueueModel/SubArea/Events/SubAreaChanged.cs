﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record SubAreaChanged : MessageEnvelope, IQueueEvent
    {
        public SubAreaChanged(Guid subAreaId, string name, string oldName, Guid recordId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            SubAreaId = subAreaId;
            Name = name;
            OldName = oldName;
            RecordId = recordId;
        }
        public Guid SubAreaId { get; }
        public string Name { get; }
        public string OldName { get; }
        public Guid RecordId { get; init; }
    }
}
