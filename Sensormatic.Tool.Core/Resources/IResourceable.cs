﻿namespace Sensormatic.Tool.Core
{
    public interface IResourceable
    {
        string ResourceKey { get; }
    }
}
