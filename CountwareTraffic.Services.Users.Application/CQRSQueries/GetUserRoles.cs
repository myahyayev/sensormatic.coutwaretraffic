﻿using Convey.CQRS.Queries;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserRoles : IQuery<IdentityUserRolesResponse>
    {
        public string UserName { get; set; }
    }
}
