﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CompanyUpdated : Convey.CQRS.Events.IEvent
    {
        public Guid ComapnyId { get; init; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
        public string CompanyName { get; init; }
        public string TenantName { get; init; }
    }
}
