﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class GetAdminUserByIdHandler : IQueryHandler<GetAdminUserById, IdentityGetAdminUserByIdResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetAdminUserByIdHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public Task<IdentityGetAdminUserByIdResponse> HandleAsync(GetAdminUserById query)
        {
            return _identityServerService.GetAdminUserByIdAsync(new Objects.Requests.IdentityGetAdminUserByIdRequest
            {
                Id = query.Id
            });
        }
    }
}
