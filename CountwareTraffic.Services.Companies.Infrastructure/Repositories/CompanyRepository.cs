﻿using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Nest;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        private readonly new AreaDbContext _context;
        private readonly ICurrentTenant _currentTenant;

        public CompanyRepository(AreaDbContext context, ICurrentTenant currentTenant) : base(context)
        {
            _context = context;
            _currentTenant = currentTenant;
        }


        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible

        public async Task<Company> GetAsync(Guid id) => await base.GetQuery(_currentTenant.Id).Include(x => x.Address).Include(x => x.Contact).SingleOrDefaultAsync(x => x.Id == id);

        public async Task<bool> ExistsAsync(string name) => await base.GetQuery(_currentTenant.Id).AnyAsync(u => u.Name == name);

        public async Task<Company> GetAsync(string name) => await base.GetQuery(_currentTenant.Id).SingleOrDefaultAsync(x => x.Name == name);

        public async Task<bool> ExistsAsync(Guid id) => await base.GetQuery(_currentTenant.Id).AnyAsync(u => u.Id == id);

        public async Task<QueryablePagingValue<Company>> GetAllAsync(PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts)
        {
            var query = base.GetQuery(_currentTenant.Id);

            query = query.ApplyFilter(filters);

            foreach (var filter in filters)
            {
                if (filter.Field.ToLower() == "phonenumber")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.Contact.PhoneNumber == filter.Value),
                        FilterEnum.Neq => query.Where(u => u.Contact.PhoneNumber != filter.Value),
                        FilterEnum.StartsWith => query.Where(u => u.Contact.PhoneNumber.StartsWith(filter.Value)),
                        FilterEnum.Contains => query.Where(u => u.Contact.PhoneNumber.Contains(filter.Value)),
                        FilterEnum.EndsWith => query.Where(u => u.Contact.PhoneNumber.EndsWith(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }
                if (filter.Field.ToLower() == "emailaddress")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.Contact.EmailAddress == filter.Value),
                        FilterEnum.Neq => query.Where(u => u.Contact.EmailAddress != filter.Value),
                        FilterEnum.StartsWith => query.Where(u => u.Contact.EmailAddress.StartsWith(filter.Value)),
                        FilterEnum.Contains => query.Where(u => u.Contact.EmailAddress.Contains(filter.Value)),
                        FilterEnum.EndsWith => query.Where(u => u.Contact.EmailAddress.EndsWith(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }
            }


            query = query.ApplyOrderBy(sorts);

            foreach (var sort in sorts)
            {
                if (sort.Field.ToLower() == "phonenumber")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.Contact.PhoneNumber),
                        Direction.Desc => query.OrderByDescending(u => u.Contact.PhoneNumber),
                        _ => throw new NotImplementedException(),
                    };
                }
                if (sort.Field.ToLower() == "emailaddress")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.Contact.EmailAddress),
                        Direction.Desc => query.OrderByDescending(u => u.Contact.EmailAddress),
                        _ => throw new NotImplementedException(),
                    };
                }
            }

            var total = await query.CountAsync();

            if (total > 0)
            {
                query = query.Include(x => x.Address).Include(x => x.Contact)
                                                     .Skip((paging.Page - 1) * paging.Limit)
                                                     .Take(paging.Limit);

                var result = await query.ToListAsync();

                return new QueryablePagingValue<Company>(result, total);
            }

            return null;
        }

        public async Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName)
        {
            var sql = @"SELECT 
                        Companies.Id AS CopmanyId,
	                    Companies.Name AS CopmanyName 
	                   FROM [areas].[Companies] AS Companies 
		                 WHERE Id = @Id AND AuditIsDeleted = 0 AND TenantId = @TenantId";

            var connection = _context.Database.GetDbConnection();

            var data = await connection.QueryFirstOrDefaultAsync<HierarchyCteData>(sql, new
            {
                @TenantId = tenantId,
                @Id = id
            });

            var response = new List<Hierarchy>();

            response.Add(new Hierarchy() { Id = tenantId, Name = tenantName, HierarchyLevel = HierarchyLevel.Tenant });
            response.Add(new Hierarchy() { Id = data.CopmanyId, Name = data.CopmanyName, HierarchyLevel = HierarchyLevel.Company });
            return response;
        }

        public async Task<TenantHierarchyEntityDto> GetTenantHierarchyAsync(Guid tenantId, string tenantName)
        {
            var sql = @"WITH Companies (Id, Name) AS
                                (
                                   SELECT
                                      Id,
                                      Name
                                      FROM [areas].[Companies] AS Companies WHERE AuditIsDeleted = 0 AND TenantId = @TenantId
                                ),

                                Countries (Id, CompanyId, Name) AS
                                (
                                   SELECT
                                      Id,
                                      CompanyId,
                                      Name
	                                  FROM [areas].[Countries] AS Countries  WHERE AuditIsDeleted = 0  AND TenantId = @TenantId
                                ),

                                Regions (Id, CountryId, Name) AS
                                (
                                   SELECT
                                      Id,
                                      CountryId,
                                      Name
	                                  FROM [areas].[Regions] AS Regions  WHERE  AuditIsDeleted = 0  AND TenantId = @TenantId
                                ),

                                Areas (Id, RegionId, Name) AS
                                (
                                   SELECT
                                      Id,
                                      RegionId,
                                      Name
	                                  FROM [areas].[Areas] AS Regions  WHERE AuditIsDeleted = 0  AND TenantId = @TenantId
                                ),

                                SubAreas(Id, AreaId, Name) AS
                                (
                                  SELECT
                                      Id,
                                      AreaId,
                                      Name
	                                  FROM [areas].[SubAreas] AS SubAreas  WHERE AuditIsDeleted = 0  AND TenantId = @TenantId
                                )

                                SELECT 
	                               	Companies.Id AS CopmanyId, 
	                                Companies.Name AS CopmanyName, 
	                                Countries.Id  AS CountryId, 
	                                Countries.Name AS CountryName,
	                                Regions.Id AS RegionId, 
	                                Regions.Name AS RegionName,
	                                Areas.Id  AS AreaId,
	                                Areas.Name AS AreaName,
	                                SubAreas.Id  AS SubAreaId,
	                                SubAreas.Name AS SubAreaName
	                                FROM Companies	                                                               
                                    LEFT JOIN Countries  ON Companies.Id = Countries.CompanyId
									LEFT JOIN Regions    ON Countries.Id  = Regions.CountryId
									LEFT JOIn Areas      ON Regions.Id  = Areas.RegionId
									LEFT JOIn SubAreas      ON Areas.Id  = SubAreas.AreaId";

            var connection = _context.Database.GetDbConnection();

            var items = await connection.QueryAsync<HierarchyCteData>(sql, new
            {
                @TenantId = tenantId
            });

            var hierarchyDto = new TenantHierarchyEntityDto
            {
                Parent = new()
                {
                    Id = tenantId,
                    Name = tenantName,
                    HierarchyLevel = HierarchyLevel.Tenant,
                    Children = new()
                }
            };

            foreach (var item in items)
            {
                var company = hierarchyDto.Parent.Children.FirstOrDefault(x => x.Id == item.CopmanyId);
                if (company == null)
                {
                    company = new()
                    {
                        Id = item.CopmanyId,
                        Name = item.CopmanyName,
                        HierarchyLevel = HierarchyLevel.Company,
                        Children = new()
                    };

                    hierarchyDto.Parent.Children.Add(company);
                }

                if (item.CountryId == Guid.Empty) continue;

                var country = company.Children.FirstOrDefault(x => x.Id == item.CountryId);
                if (country == null)
                {
                    country = new()
                    {
                        Id = item.CountryId,
                        Name = item.CountryName,
                        HierarchyLevel = HierarchyLevel.Country,
                        Children = new()

                    };
                    company.Children.Add(country);
                }

                if (item.RegionId == Guid.Empty) continue;
                var region = country.Children.FirstOrDefault(x => x.Id == item.RegionId);
                if (region == null)
                {
                    region = new()
                    {
                        Id = item.RegionId,
                        Name = item.RegionName,
                        HierarchyLevel = HierarchyLevel.Region,
                        Children = new()

                    };
                    country.Children.Add(region);
                }

                if (item.AreaId == Guid.Empty) continue;
                var area = region.Children.FirstOrDefault(x => x.Id == item.AreaId);
                if (area == null)
                {
                    area = new()
                    {
                        Id = item.AreaId,
                        Name = item.AreaName,
                        HierarchyLevel = HierarchyLevel.Area,
                        Children = new()
                    };
                    region.Children.Add(area);
                }

                if (item.SubAreaId == Guid.Empty) continue;
                area.Children.Add(new()
                {
                    Id = item.SubAreaId,
                    Name = item.SubAreaName,
                    HierarchyLevel = HierarchyLevel.SubArea,
                    Children = new()
                });

            }

            return hierarchyDto;
        }

        public async Task<List<KeyValue>> GetAllKeyValueAsync()
        {
            var query = base.GetQuery(_currentTenant.Id);

            return await query.Select(x => new KeyValue { Id = x.Id, Name = x.Name }).ToListAsync();
        }


    }
}
