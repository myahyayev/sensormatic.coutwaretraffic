﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class CompaniesController : BaseController
    {
        private readonly CompanyService _companyService;
        private readonly CountryService _countryService;
        public CompaniesController(IServiceProvider provider, ILogger<CompaniesController> logger, IHttpContextAccessor contextAccessor, CompanyService companyService, CountryService countryService)
            : base(provider, logger, contextAccessor)
        {
            _companyService = companyService;
            _countryService = countryService;
        }

        [HttpGet("{companyId}/detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCompanyView, Permissions.CompanyView })]
        public async Task<ApiResponse<GetCompanyDetails>> GetCompanyDetailAsync(Guid companyId)
            => Response(await _companyService.GetCompanyByIdAsync(companyId), new ResultMessage("#S1070"));


        [HttpPost("details"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCompanyList })]
        public async Task<ApiPagedResponse<GetCompanyDetails>> GetCompanyDetailsAsync([FromBody] DataSourceApiRequest paging)
            => await _companyService.GetCompaniesAsync(paging);
      

        [HttpPost("add"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCompanyAdd })]
        public async Task<ApiResponse<CrudApiResponse>> AddCompanyAsync([FromBody] AddCompanyApiRequest request)
        {
            await _companyService.AddCompanyAsync(request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Create }, new ResultMessage("#S1071"));
        }


        [HttpPut("{companyId}/change"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCompanyEdit })]
        public async Task<ApiResponse<CrudApiResponse>> ChangeCompanyAsync(Guid companyId, [FromForm] ChangeCompanyApiRequest request)
        {
            await _companyService.ChangeCompanyAsync(companyId, request);
            return Response(new CrudApiResponse {  IsSuccess = true, OperationType = OperationType.Update }, new ResultMessage("#S1072"));
        }


        [HttpDelete("{companyId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCompanyDelete })]
        public async Task<ApiResponse<CrudApiResponse>> DeleteCompanyAsync(Guid companyId)
        {
            var countryControl = await _countryService.GetCountriesAsync(companyId, new DataSourceApiRequest());

            if (countryControl.TotalCount > 0)
            {
                return Response(new CrudApiResponse {  IsSuccess = false, OperationType = OperationType.Delete, Id = companyId }, new ResultMessage()
                {
                    CallToActionType = CallToActionType.GoToCountries,
                    Description = $"#W1027 A country has been defined in the Company with id {companyId}.",
                    Title = $"#W1027 Company with id {companyId} cannot be deleted."
                });
            }

            await _companyService.DeleteCompanyAsync(companyId);

            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Delete, Id = companyId }, new ResultMessage()
            {
                CallToActionType = CallToActionType.Ok,
                Description = $"#S1065 Company with {companyId} id has been deleted.",
                Title = $"#S1065 Company with {companyId} id has been deleted."
            });
        }
    }
}
