﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Companies.Core
{
    public class AreaStatus : Enumeration
    {
        public static AreaStatus Created = new(1, nameof(Created));
        public static AreaStatus Completed = new(2, nameof(Completed));
        public static AreaStatus Rejected = new(3, nameof(Rejected));
       
        public AreaStatus(int id, string name) : base(id, name) { }

        public static IEnumerable<AreaStatus> List() => new[] { Created, Completed, Rejected };

        public static AreaStatus FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
                throw new AreaStatusNameNotFoundException(List(), name);

            return state;
        }

        public static AreaStatus From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
                throw new AreaStatusIdNotFoundException(List(), id);

            return state;
        }
    }
}
