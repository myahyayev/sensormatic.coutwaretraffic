﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportGeneratingCompleted : IDomainEvent
    {
        public Guid ReportId { get; init; }
        public string FilePath { get; init; }
        public Guid FileId { get; init; }
        public Guid RecordId { get; init; }
        public int ReportStatus { get; init; }
        public Guid TenantId { get; init; }
        public string Name { get; init; }
        public string Description { get; init; }
        public string CreatedBy { get; init; }
        public string CreationDate { get; init; }
        public string Format { get; init; }
        public string Title { get; init; }
        public string Locations { get; init; }
        public string DateRange { get; init; }
        public string EmailBody { get; init; }
        public string EmailTo { get; init; }
        public string EmailSubject { get; set; }
        public string LanguageShortName { get; init; }

        public ReportGeneratingCompleted(Guid reportId, string filePath, Guid fileId, int reportStatus, Guid tenantId, string name, string description,  string createdBy, string creationDate, string format, string title, string locations, string dateRange, string emailTo, string emailBody, string emailSubject, string languageShortName)
        {
            ReportId = reportId;
            FilePath = filePath;
            FileId = fileId;
            ReportStatus = reportStatus;
            RecordId = Guid.NewGuid();
            TenantId = tenantId;
            Name = name;
            Description = description;
            CreatedBy = createdBy;
            CreationDate = creationDate;
            Format = format;
            Title = title;
            Locations = locations;
            DateRange = dateRange;
            EmailBody = emailBody;
            EmailTo = emailTo;
            EmailSubject = emailSubject;
            LanguageShortName = languageShortName;
        }
    }
}
