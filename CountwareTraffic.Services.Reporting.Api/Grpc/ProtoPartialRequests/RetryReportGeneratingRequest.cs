﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Reporting.Grpc
{
    [ServiceLog]
    public sealed partial class RetryReportGeneratingRequest : SensormaticRequestValidate
    {
        internal Guid _ReportId
        {
            get
            {
                if (Guid.TryParse(reportId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { reportId_ = value.ToString(); }
        }

        internal Guid? _ReportFileId
        {
            get
            {
                if (Guid.TryParse(reportFileId_, out Guid id))
                    return id;

                return null;
            }
            set { reportFileId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_ReportId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1536 ReportId cannot be null", nameof(_ReportId)));
        }
    }
}

