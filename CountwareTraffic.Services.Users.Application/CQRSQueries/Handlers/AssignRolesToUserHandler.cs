﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class AssignRolesToUserHandler : IQueryHandler<AssignRolesToUser, IdentityAssignRoleToUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public AssignRolesToUserHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityAssignRoleToUserResponse> HandleAsync(AssignRolesToUser query)
        {
            return await _identityServerService.AssignRolesToUserAsync(query.UserName);
        }
    }
}
