﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Devices.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class SubAreaCreatedHandler : IEventHandler<SubAreaCreated>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;

        public SubAreaCreatedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(SubAreaCreated @event)
        {
            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            if (!await subAreaRepository.ExistsAsync(@event.SubAreaId))
            {
                try
                {
                    var subArea = SubArea.Create(@event.SubAreaId, @event.Name, @event.AreaId, @event.AreaName, @event.RegionId, @event.RegionName, @event.CountryId, @event.CountryName, @event.CompanyId, @event.CompanyName, @event.CountryLookupId, @event.CityId, @event.CityName, @event.DistrictId, @event.DistrictName, @event.TenantId);

                    await subAreaRepository.AddAsync(subArea);

                    await _unitOfWork.CommitAsync();

                    _queueService.Publish(new SubAreaCreatedCompleted (@event.SubAreaId,   @event.UserId, "",  @event.TenantId ));
                }
                catch (Exception)
                {
                    _queueService.Publish(new SubAreaCreatedRejected(@event.SubAreaId, @event.UserId, "", @event.TenantId));
                }
            }
        }
    }
}
