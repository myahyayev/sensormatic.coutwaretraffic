﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetCountryLookupsHandler : IQueryHandler<GetCountryLookups, List<CountryLookupDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetCountryLookupsHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<List<CountryLookupDetailsDto>> HandleAsync(GetCountryLookups query)
        {
            var countryLookups = await _unitOfWork.GetRepository<ICountryLookupRepository>().GetAllAsync();

            if (countryLookups == null) return null;

            return countryLookups.Select(countryLookup => new CountryLookupDetailsDto
            {
                Id = countryLookup.Id,
                Name = countryLookup.Name,
                Capital = countryLookup.Capital,
                ContinentCode = countryLookup.ContinentCode,
                CurrencyCode = countryLookup.CurrencyCode,
                Iso = countryLookup.Iso,
                Iso3 = countryLookup.Iso3,
                IsoNumeric = countryLookup.IsoNumeric
            }).ToList();
        }
    }
}
