﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportParameters
    {
        public List<Location> Locations { get; set; }
        public DateTime DateRangeStartDate { get; set; }
        public DateTime DateRangeEndDate { get; set; }
    }
}
