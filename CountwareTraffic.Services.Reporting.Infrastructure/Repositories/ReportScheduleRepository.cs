﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportScheduleRepository : Repository<ReportSchedule>, IReportScheduleRepository
    {
        private readonly new ReportDbContext _context;

        public ReportScheduleRepository(ReportDbContext context) : base(context) => _context = context;

        public async Task<ReportSchedule> GetByReportIdAsync(Guid reportId)
            => await base.GetQuery(null)
                .Where(u => u.ReportId == reportId)
                .FirstOrDefaultAsync();

        public async Task<ScheduleFrequency> GetScheduleFrequencyAsync(Guid reportId)
            => await base.GetQuery(null)
                .Include(i => i.ScheduleFrequency)
                .Where(u => u.ReportId == reportId)
                .Select(u => u.ScheduleFrequency)
                .FirstOrDefaultAsync();

        public async Task<IEnumerable<ReportScheduleDetailEntityDto>> GetScheduledReportsAsync()
        {
            return await _context.ReportSchedules
                 .Include(i => i.ScheduleFrequency)
                 .Include(i => i.ScheduleDeliveryTime)
                 .Where(w => !w.AuditIsDeleted
                    && w.ScheduleFrequency.Id == ScheduleFrequency.Repeat.Id)
                 .Select(s => new ReportScheduleDetailEntityDto
                 {
                     Id = s.Id,
                     AtSetTime = s.AtSetTime,
                     AtSetTimeZone = s.AtSetTimeZone,
                     EndsOn = s.EndsOn,
                     DeliveryTime = s.ScheduleDeliveryTime,
                     Frequency = s.ScheduleFrequency,
                     RepeatOn = s.RepeatOn,
                     StartsOn = s.StartsOn,

                     Report = (from report in _context.Set<Report>()
                        .Include(i => i.TemplateType)
                        .Include(i => i.ReportStatus)
                        .Include(i => i.ReportFormat)
                        .Include(i => i.Email)
                               where s.ReportId == report.Id
                               && !report.AuditIsDeleted && report.IsActive
                               select new ReportEntityDto
                               {
                                   Id = report.Id,
                                   Name = report.Name,
                                   Description = report.Description,
                                   IsActive = report.IsActive,
                                   Status = report.ReportStatus,
                                   Format = report.ReportFormat,
                                   Parameters = report.Parameters,
                                   TemplateType = report.TemplateType,
                                   NotifyWhenComplete = report.NotifyWhenComplete,
                                   TenantId = report.TenantId,
                                   ReportEmail = new ReportEmailEntityDto
                                   {
                                       Body = report.Email.Body,
                                       Subject = report.Email.Subject,
                                       To = report.Email.To
                                   },
                                   ReportLanguage = (from reportLanguage in _context.Set<ReportLanguage>()
                                                     where reportLanguage.Id == report.LanguageId
                                                     select new ReportLanguageEntityDto
                                                     {
                                                         Id = reportLanguage.Id,
                                                         Name = reportLanguage.Name,
                                                         ShortName = reportLanguage.ShortName
                                                     }).FirstOrDefault(),

                                   ReportTemplate = (from reportTemplate in _context.Set<ReportTemplate>()
                                                     where reportTemplate.Id == report.TemplateId
                                                     select new ReportTemplateEntityDto
                                                     {
                                                         Id = reportTemplate.Id,
                                                         Name = reportTemplate.Name,
                                                         AssemblyQualifiedName = reportTemplate.AssemblyQualifiedName
                                                     })
                                                         .FirstOrDefault(),
                               }
                             ).FirstOrDefault(),

                 }).ToListAsync();
        }

        #region dispose
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion dispose
    }
}
