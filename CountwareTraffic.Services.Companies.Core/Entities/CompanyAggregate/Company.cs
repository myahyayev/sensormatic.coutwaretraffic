﻿using Sensormatic.Tool.Efcore;
using System;
using Sensormatic.Tool.Common;
namespace CountwareTraffic.Services.Companies.Core
{
    public class Company : AggregateRoot, IAuditable, IDateable, IDeletable, ITraceable, IMultiTenant
    {
        private string _name;
        private string _description;
        private CompanyAddress _address;
        private CompanyContact _contact;

        private string _erpCode;
        private DateTime? _erpStartDate;
        private DateTime? _erpEndDate;
        public string Name => _name;
        public string Description => _description;
        public CompanyAddress Address => _address;
        public CompanyContact Contact => _contact;
        public string ErpCode => _erpCode;
        public DateTime? ErpStartDate => _erpStartDate;
        public DateTime? ErpEndDate => _erpEndDate;


        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public bool AuditIsDeleted { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public Guid TenantId { get; set; }
        #endregion default properties

        private Company() { }


        public static Company Create(string name, string description, CompanyAddress address, CompanyContact contact)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1416 " + nameof(name));

            if (name.Length > 150)
                throw new ArgumentException("#E1417 " + nameof(name));

            if (!description.IsNullOrWhiteSpace() && description.Length > 2500)
                throw new ArgumentException("#E1418 " + nameof(description));

            return new Company
            {
                _name = name,
                _description = description,
                _address = address,
                _contact = contact
            };
        }

        public void CompleteChange(string name, string description, CompanyContactEntityDto contactEntityDto, CompanyAddressEntityDto addressEntityDto)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1419 " + nameof(name));

            if (name.Length > 150)
                throw new ArgumentException("#E1420 " + nameof(name));

            if (!description.IsNullOrWhiteSpace() && description.Length > 2500)
                throw new ArgumentException("#E1421 " + nameof(description));

            if (name != _name)
                _name = name;

            if (description != _description)
                _description = description;

            if (this._contact == null)
                this._contact = CompanyContact.Create(contactEntityDto.GsmDialCode, contactEntityDto.GsmNumber, contactEntityDto.PhoneDialCode, contactEntityDto.PhoneNumber, contactEntityDto.EmailAddress, contactEntityDto.PersonName, contactEntityDto.GsmDialCodeSecondary, contactEntityDto.GsmNumberSecondary, contactEntityDto.PhoneDialCodeSecondary, contactEntityDto.PhoneNumberSecondary, contactEntityDto.EmailAddressSecondary, contactEntityDto.PersonNameSecondary, contactEntityDto.GsmCountryCode, contactEntityDto.PhoneCountryCode, contactEntityDto.GsmCountryCodeSecondary, contactEntityDto.PhoneCountryCodeSecondary);

            else
                this._contact.WhenChanged(contactEntityDto.GsmDialCode, contactEntityDto.GsmNumber, contactEntityDto.PhoneDialCode, contactEntityDto.PhoneNumber, contactEntityDto.EmailAddress, contactEntityDto.PersonName, contactEntityDto.GsmDialCodeSecondary, contactEntityDto.GsmNumberSecondary, contactEntityDto.PhoneDialCodeSecondary, contactEntityDto.PhoneNumberSecondary, contactEntityDto.EmailAddressSecondary, contactEntityDto.PersonNameSecondary, contactEntityDto.GsmCountryCode, contactEntityDto.PhoneCountryCode, contactEntityDto.GsmCountryCodeSecondary, contactEntityDto.PhoneCountryCodeSecondary);


            if (this._address == null)
                this._address = CompanyAddress.Create(addressEntityDto.Street, addressEntityDto.City, addressEntityDto.State, addressEntityDto.Country, addressEntityDto.ZipCode, addressEntityDto.Latitude, addressEntityDto.Longitude);

            else
                this._address.WhenChanged(addressEntityDto.Street, addressEntityDto.City, addressEntityDto.State, addressEntityDto.Country, addressEntityDto.ZipCode, addressEntityDto.Latitude, addressEntityDto.Longitude);
        }

        public void WhenCreated(Guid tenantId, string tenantName, Guid companyId, string compnayName) => AddEvent(new CompanyHierarchyCreated(tenantId, tenantName, companyId, compnayName));

        public void WhenChanged(Guid tenantId, string tenantName, Guid companyId, string compnayName) => AddEvent(new CompanyHierarchyUpdated(tenantId, tenantName, companyId, compnayName));

        public void WhenDeleted(Guid tenantId, string tenantName, Guid companyId, string compnayName) => AddEvent(new CompanyHierarchyDeleted(tenantId, tenantName, companyId, compnayName));
    }
}
