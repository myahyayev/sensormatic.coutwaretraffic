﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesCompanyUpdatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.CompanyUpdated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesCompanyUpdatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.CompanyUpdated queuEvent)
        {
            var companyUpdated = _eventMapper.Map(queuEvent) as CompanyUpdated;
            await _eventDispatcher.PublishAsync(companyUpdated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
