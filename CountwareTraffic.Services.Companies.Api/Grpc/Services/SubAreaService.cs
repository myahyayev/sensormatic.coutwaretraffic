﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [Authorize]
    public class SubAreaService : Subarea.SubareaBase
    {
        private readonly ILogger<SubAreaService> _logger;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        public SubAreaService(ILogger<SubAreaService> logger, ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }
        public override async Task<GetSubAreaDetailResponse> GetSubAreaById(GetSubAreaRequest request, ServerCallContext context)
        {
            var subArea = await _queryDispatcher.QueryAsync(new GetSubArea { SubAreaId = request._SubAreaId });

            var response = new GetSubAreaDetailResponse();

            response.SubAreaDetail = new SubAreaDetail
            {
                Id = subArea.Id.ToString(),
                Name = subArea.Name,
                Description = subArea.Description,
                AreaId = subArea.AreaId.ToString(),
                Audit = new Audit
                {
                    AuditCreateBy = subArea.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(subArea.AuditCreateDate),
                    AuditModifiedBy = subArea.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(subArea.AuditModifiedDate),
                }
            };
            return response;
        }

        public override async Task<SubAreaPagingResponse> GetSubAreas(GetSubAreasRequest request, ServerCallContext context)
        {
            var pagingSubAreas = await _queryDispatcher.QueryAsync(new GetSubAreas
            {
                AreaId = request._AreaId,

                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            SubAreaPagingResponse response = new()
            {
                TotalCount = pagingSubAreas.TotalCount,
                HasNextPage = pagingSubAreas.HasNextPage,
                Page = pagingSubAreas.Page,
                Limit = pagingSubAreas.Limit,
                Next = pagingSubAreas.Next,
                Prev = pagingSubAreas.Prev
            };

            pagingSubAreas.Data.ToList().ForEach(subArea => response.SubAreaDetails.Add(new SubAreaDetail
            {
                Id = subArea.Id.ToString(),
                Name = subArea.Name,
                Description = subArea.Description,
                AreaId = subArea.AreaId.ToString(),
                Audit = new Audit
                {
                    AuditCreateBy = subArea.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(subArea.AuditCreateDate),
                    AuditModifiedBy = subArea.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(subArea.AuditModifiedDate)
                }
            }));

            return response;
        }

        public override async Task<CreateSuccessResponse> AddSubArea(CreateSubAreaRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new CreateSubArea
            {
                AreaId = request._AreaId,
                Name = request.Name,
                Description = request.Description
            });

            return new CreateSuccessResponse { Created = "Created" };
        }

        public override async Task<UpdateSuccessResponse> ChangeSubArea(UpdateSubAreaRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UpdateSubArea
            {
                SubAreaId = request._SubAreaId,
                Name = request.Name,
                Description = request.Description
            });

            return new UpdateSuccessResponse { Updated = "Updated" };;
        }

        public override async Task<DeleteSuccessResponse> DeleteSubArea(DeleteSubAreaRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new DeleteSubArea
            {
                SubAreaId = request._SubAreaId
            });

            return new DeleteSuccessResponse { Deleted = "Deleted" };
        }

        public override async Task<GetHierarchyResponse> GetHierarchy(GetHierarchyRequest request, ServerCallContext context)
        {
            var hierarchies = await _queryDispatcher.QueryAsync(new GetSubAreaHierarchy { Id = request._Id });

            GetHierarchyResponse response = new();

            hierarchies.ForEach(hierarchy => response.Hierarchies.Add(new Hierarchy
            {
                Id = hierarchy.Id.ToString(),
                Name = hierarchy.Name,
                HierarchyLevel = hierarchy.HierarchyLevel switch
                {
                    Core.HierarchyLevel.Tenant => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Tenant,
                    Core.HierarchyLevel.Company => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Company,
                    Core.HierarchyLevel.Country => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Country,
                    Core.HierarchyLevel.Region => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Region,
                    Core.HierarchyLevel.Area => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Area,
                    Core.HierarchyLevel.SubArea => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.SubArea,
                    Core.HierarchyLevel.Device => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Device,
                    _ => throw new NotImplementedException(),
                }
            }));

            return response;
        }

        public override async Task<KeyValueResponse> GetSubAreasKeyValue(KeyValueRequest request, ServerCallContext context)
        {
            var keyValues = await _queryDispatcher.QueryAsync(new GetSubAreaKeyValue() { ParentId = request._ParentId });

            KeyValueResponse response = new KeyValueResponse();

            keyValues.ForEach(u => response.KeyValues.Add(new KeyValue
            {
                Id = u.Id.ToString(),
                Name = u.Name
            }));

            return response;
        }
    }
}
