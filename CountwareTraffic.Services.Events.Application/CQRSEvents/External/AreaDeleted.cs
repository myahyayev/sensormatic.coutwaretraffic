﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public record AreaDeleted : Convey.CQRS.Events.IEvent
    {
        public Guid TenantId { get; set; }
        public Guid AreaId { get; set; }
        public Guid RecordId { get; set; }
        public Guid UserId { get; set; }
    }
}
