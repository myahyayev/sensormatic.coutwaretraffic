﻿using CountwareTraffic.Services.Reporting.Core;
using System;

namespace CountwareTraffic.Services.Reporting.ReportSchedule.Scheduler
{
    public class JobSchedule
    {
        public JobSchedule(Type jobType, string jobName, string description, Guid tenantId, DateTime? startsOn, DateTime? endsOn, string atSetTime, string atSetTimeZone, string repeatOn, TemplateType templateType)
        {
            JobType = jobType;
            JobName = jobName;
            StartsOn = startsOn;

            if (endsOn == null)
                endsOn = DateTime.MaxValue;

            EndsOn = endsOn;
            AtSetTime = atSetTime;
            AtSetTimeZone = atSetTimeZone;
            TemplateType = templateType;
            RepeatOn = repeatOn;
            TenantId = tenantId;
            Description = description;
        }
        public Type JobType { get; }
        public string JobName { get; }
        public string Description { get; set; }
        public DateTime? StartsOn { get; }
        public DateTime? EndsOn { get; }
        public string AtSetTime { get; }
        public string AtSetTimeZone { get; }
        public string RepeatOn { get; }
        public Guid TenantId { get; }
        public TemplateType TemplateType { get; }
    }
}
