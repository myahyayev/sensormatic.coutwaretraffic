﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceCreated : MessageEnvelope, IQueueEvent
    {
        public DeviceCreated(Guid deviceId, string name, string macAddress, string ipAddress, Guid subAreaId, string subAreaName, Guid areaId, string areaName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid countryLookupId, Guid cityId, string cityName, Guid districtId, string districtName, int controlFrequency,  Guid recordId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            DeviceId = deviceId;
            Name = name;
            MacAddress = macAddress;
            IpAddress = ipAddress;
            SubAreaId = subAreaId;
            SubAreaName = subAreaName;
            RecordId = recordId;
            AreaId = areaId;
            AreaName = areaName;
            RegionId = regionId;
            CountryId = countryId;
            CountryName = countryName;
            CompanyId = companyId;
            RegionName = regionName;
            CompanyName = companyName;
            CountryLookupId = countryLookupId;
            CityId = cityId;
            DistrictId = districtId;
            DistrictName = districtName;
            CityName = cityName;
            ControlFrequency = controlFrequency;
        }
        public Guid DeviceId { get; }
        public string Name { get; }
        public string MacAddress { get; }
        public string IpAddress { get; }
        public Guid SubAreaId { get; }
        public string SubAreaName { get; }
        public Guid AreaId { get; }
        public string AreaName { get; }
        public Guid RecordId { get; init; }
        public Guid RegionId { get; }
        public string RegionName { get; }
        public Guid CountryId { get; }
        public string CountryName { get; }
        public Guid CompanyId { get; }
        public string CompanyName { get; }
        public Guid CountryLookupId { get; }
        public Guid CityId { get; }
        public string CityName { get; }
        public Guid DistrictId { get; }
        public string DistrictName { get; }
        public int ControlFrequency { get; set; }
    }
}
