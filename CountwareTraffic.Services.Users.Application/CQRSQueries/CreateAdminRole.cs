﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries
{
    public class CreateAdminRole : IQuery<IdentityCreateAdminRoleResponse>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Permissions { get; set; }
    }
}
