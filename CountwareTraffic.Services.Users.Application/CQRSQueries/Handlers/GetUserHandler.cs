﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserHandler : IQueryHandler<GetUser, IdentityUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetUserHandler(IIdentityServerService identityServerService) => _identityServerService = identityServerService;

        public async Task<IdentityUserResponse> HandleAsync(GetUser query) => await _identityServerService.GetUserAsync();
    }
}
