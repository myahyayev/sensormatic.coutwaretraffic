﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaCreatedRejectedHandler : IEventHandler<AreaChangedRejected>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public AreaCreatedRejectedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(AreaChangedRejected command)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var area = await areaRepository.GetAsync(command.AreaId, command.TenantId);

            area.WhenCreatedRejected();

            await _unitOfWork.CommitAsync();

            _queueService.Publish(new AreaCreatedFailed
            {
                RecordId = Guid.NewGuid(),
                AreaId = command.AreaId,
                UserId = command.UserId,
                AreaStatus = AreaStatus.Rejected.Name,
                UserName = String.Empty,
                TenantId = command.TenantId
            });
        }
    }
}
