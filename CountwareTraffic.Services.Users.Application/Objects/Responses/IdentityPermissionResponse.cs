﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityPermissionResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public IdentityPermissionData Data { get; set; }
    }

    public class IdentityPermissionData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public string DescriptionLabel { get; set; }
    }
}
