﻿using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public interface IQueueEventMapper : IScopedDependency
    {
        Sensormatic.Tool.Queue.IQueueEvent Map(Sensormatic.Tool.Efcore.IDomainEvent @event, Guid userId, string correlationId);
        List<Sensormatic.Tool.Queue.IQueueEvent> MapAll(IEnumerable<Sensormatic.Tool.Efcore.IDomainEvent> events, Guid userId, string correlationId);
    }
}
