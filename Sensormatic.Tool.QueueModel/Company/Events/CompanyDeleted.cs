﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record CompanyDeleted : MessageEnvelope, IQueueEvent
    {
        public CompanyDeleted(Guid companyId, string companyName, string tenantName, Guid recordId, Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            CompanyId = companyId;
            CompanyName  = companyName;
            TenantName = tenantName;
            RecordId = recordId;
        }
        public Guid CompanyId { get; }
        public string CompanyName { get; }
        public string TenantName { get; }
        public Guid RecordId { get; init; }
    }
}
