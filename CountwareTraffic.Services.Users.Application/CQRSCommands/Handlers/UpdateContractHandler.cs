﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSCommands.Handlers
{
    public class UpdateContractHandler : IQueryHandler<UpdateContract, IdentityUpdateContractResponse>
    {
        private readonly IIdentityServerService _identityService;

        public UpdateContractHandler(IIdentityServerService identityService)
        {
            _identityService = identityService;
        }

        public async Task<IdentityUpdateContractResponse> HandleAsync(UpdateContract query)
        {
            return await _identityService.UpdateContractAsync(
                   new Objects.Requests.IdentityUpdateContractRequest
                   {
                       Id = query.Id,
                       AppType = query.AppType,
                       ContractBody = query.ContractBody,
                       ContractType = query.ContractType,
                       Language = query.Language,
                       Name = query.Name,
                       ReleaseDate = query.ReleaseDate,
                       Version = query.Version,
                   }).ConfigureAwait(false);
        }
    }
}
