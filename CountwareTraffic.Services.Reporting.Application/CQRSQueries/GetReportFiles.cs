﻿using Convey.CQRS.Queries;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class GetReportFiles : IQuery<PagingResult<ReportFileDto>>
    {
        public PagingQuery PagingQuery { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
        public Guid ReportId { get; set; }
    }
}
