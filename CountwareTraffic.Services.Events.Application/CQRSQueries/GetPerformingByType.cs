﻿using Convey.CQRS.Queries;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public class GetPerformingByType : IQuery<List<PerformingByTypeDto>>
    {
        public List<NodeFilter> NodeFilters { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Direction Direction { get; set; }
        public PerformType PerformType { get; set; }
        public int Size { get; set; }
    }
}
