﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesSubAreaHDeletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.SubAreaHDeleted>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesSubAreaHDeletedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.SubAreaHDeleted queuEvent)
        {
            var subAreaHDeleted = _eventMapper.Map(queuEvent) as SubAreaHDeleted;
            await _eventDispatcher.PublishAsync(subAreaHDeleted);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
