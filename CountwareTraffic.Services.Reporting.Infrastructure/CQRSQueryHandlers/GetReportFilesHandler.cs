﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using Sensormatic.Tool.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class GetReportFilesHandler : IQueryHandler<GetReportFiles, PagingResult<ReportFileDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetReportFilesHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;
        public async Task<PagingResult<ReportFileDto>> HandleAsync(GetReportFiles query)
        {
            var reportFileRepository = _unitOfWork.GetRepository<IReportFileRepository>();

            int page = query.PagingQuery.Page;
            int limit = query.PagingQuery.Limit;

            var qres = await reportFileRepository.GetByReportId(query.ReportId, query.PagingQuery, query.Filters, query.Sorts);

            if (qres == null)
                return PagingResult<ReportFileDto>.Empty;


            return new PagingResult<ReportFileDto>(qres.Entities.Select(reportFile => new ReportFileDto
            {
                Id = reportFile.Id,
                CreatedDate = reportFile.CreatedDate,
                FilePath = reportFile.FilePath,
                DateRangeStartDate = reportFile.DateRangeStartDate,
                DateRangeEndDate = reportFile.DateRangeEndDate

            }), qres.Total, page, limit, qres.Total > (limit * (page - 1)) + qres.Entities.Count);
        }
    }
}
