﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ChangeDeviceBrandModelApiRequest : SensormaticRequestValidate
    {
        public string Name { get; set; }

        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1234 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1235 Name invalid format", nameof(Name)));
        }
    }
}
