﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class KeyValueRequest : SensormaticRequestValidate
    {
        internal Guid _ParentId
        {
            get
            {
                if (Guid.TryParse(parentId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.parentId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_ParentId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1328 ParentId cannot be null", nameof(_ParentId)));
        }
    }
}