﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CompanyUpdatedHandler : IEventHandler<CompanyUpdated>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public CompanyUpdatedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository)
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(CompanyUpdated command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId,
                Name = command.TenantName
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId,
                Name = command.CompanyName
            };

            await _tenantHierarchyElasticSearchRepository.UpdateCompanyAsync(tenantElasticDto, companyElasticDto);
        }
    }
}

