﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class GetBrandModelLookup : IQuery<BrandModelLookupDetailsDto>
    {
        public Guid BrandModelId { get; set; }
    }
}
