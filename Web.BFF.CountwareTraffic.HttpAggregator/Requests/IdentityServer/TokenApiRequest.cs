﻿using Microsoft.AspNetCore.Mvc;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class TokenApiRequest :  SensormaticRequestValidate
    {
        [FromForm(Name = "username")]
        public string UserName { get; set; }

        [FromForm(Name = "password")]
        public string Password { get; set; }

        public override void Validate()
        {
            if (UserName.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(UserName)));

            if (Password.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(Password)));
        }
    }
}
