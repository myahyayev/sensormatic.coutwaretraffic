﻿using Sensormatic.Tool.Ioc;

namespace CountwareTraffic.Services.Settings.Core
{
    public interface INoteRepository : IRepository<Note>, IScopedDependency
    {
    }
}
