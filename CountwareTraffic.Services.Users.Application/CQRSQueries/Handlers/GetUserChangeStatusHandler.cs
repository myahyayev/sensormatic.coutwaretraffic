﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserChangeStatusHandler : IQueryHandler<GetUserChangeStatus, IdentityUserChangeStatusResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetUserChangeStatusHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityUserChangeStatusResponse> HandleAsync(GetUserChangeStatus query)
        {
            return await _identityServerService.GetUserChangeStatusAsync(query.Id);
        }
    }
}
