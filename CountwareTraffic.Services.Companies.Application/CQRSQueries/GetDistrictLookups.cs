﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetDistrictLookups : IQuery<List<DistrictLookupDetailsDto>>
    {
        public Guid CityId { get; set; }
    }
}
