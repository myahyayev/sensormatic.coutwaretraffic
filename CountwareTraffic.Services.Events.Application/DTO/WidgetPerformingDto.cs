﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class WidgetPerformingDto
    {
        public int InwardsCurrent { get; set; }
        public int InwardsPrevius { get; set; }
    }
}
