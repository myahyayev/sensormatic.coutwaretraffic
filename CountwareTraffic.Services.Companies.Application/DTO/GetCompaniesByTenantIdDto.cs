﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application.DTO
{
    public class GetCompaniesByTenantIdDto
    {
        public GetCompaniesByTenantIdDto()
        {
            Data = new();
        }

        public List<GetCompaniesByTenantIdItemDto> Data { get; set; }
    }

    public class GetCompaniesByTenantIdItemDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
