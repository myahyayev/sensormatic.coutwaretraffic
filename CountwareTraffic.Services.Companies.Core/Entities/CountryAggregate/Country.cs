﻿using Sensormatic.Tool.Efcore;
using System;
using Sensormatic.Tool.Common;
namespace CountwareTraffic.Services.Companies.Core
{
    public class Country : AggregateRoot, IAuditable, IDateable, IDeletable, ITraceable, IMultiTenant
    {
        private string _iso;
        private string _iso3;
        private int _isoNumeric;
        private string _name;
        private string _capital;
        private string _continentCode;
        private string _currencyCode;
        private Guid _companyId;
        private Guid _countryLookupId;

        public string Iso => _iso;
        public string Iso3 => _iso3;
        public int IsoNumeric => _isoNumeric;
        public string Name => _name;
        public string Capital => _capital;
        public string ContinentCode => _continentCode;
        public string CurrencyCode => _currencyCode;
        public Guid CompanyId => _companyId;
        public Guid CountryLookupId => _countryLookupId;


        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public bool AuditIsDeleted { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public Guid TenantId { get; set; }
        #endregion default properties

        private Country() { }

        public static Country Create(Guid countryLookupId, string iso, string iso3, int isoNumeric, string name, string capital, string continentCode, string currencyCode, Guid companyId)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1422 " + nameof(name));

            if (name.Length > 150)
                throw new ArgumentException("#E1423 " + nameof(name));

            if (iso.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1424 " + nameof(iso));

            if (iso.Length != 2)
                throw new InvalidIsoCodeException(iso);

            if (!iso3.IsNullOrWhiteSpace() && iso3.Length != 3)
                throw new InvalidIso3CodeException(iso3);

            if (isoNumeric < 1)
                throw new ArgumentException("#E1427 IsoNumeric must be above zero.", nameof(isoNumeric));

            if (capital.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1428 " + nameof(capital));

            if (capital.Length > 100)
                throw new ArgumentException("#E1429 " + nameof(capital));

            if (!continentCode.IsNullOrWhiteSpace() && continentCode.Length > 5)
                throw new ArgumentException("#E1430 " + continentCode);

            if (!currencyCode.IsNullOrWhiteSpace() && currencyCode.Length != 3)
                throw new InvalidCurrencyCodeException(currencyCode);

            return new Country
            {
                _iso = iso,
                _iso3 = iso3,
                _isoNumeric = isoNumeric,
                _name = name,
                _capital = capital,
                _continentCode = continentCode,
                _currencyCode = currencyCode,
                _companyId = companyId,
                _countryLookupId = countryLookupId
            };
        }

        public void CompleteChange(Guid countryLookupId, string iso, string iso3, int isoNumeric, string name, string capital, string continentCode, string currencyCode)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1432 " + nameof(name));

            if (name.Length > 150)
                throw new ArgumentException("#E1433 " + nameof(name));

            if (iso.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1434 " + nameof(iso));

            if (iso.Length != 2)
                throw new InvalidIsoCodeException(iso);

            if (!iso3.IsNullOrWhiteSpace() && iso3.Length != 3)
                throw new InvalidIso3CodeException(iso3);

            if (isoNumeric < 1)
                throw new ArgumentException("#E1435 IsoNumeric must be above zero.", nameof(isoNumeric));

            if (capital.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1436 " + nameof(capital));

            if (capital.Length > 100)
                throw new ArgumentException("#E1437 " + nameof(capital));

            if (!continentCode.IsNullOrWhiteSpace() && continentCode.Length > 5)
                throw new ArgumentException("#E1438 " + continentCode);

            if (!currencyCode.IsNullOrWhiteSpace() && currencyCode.Length != 3)
                throw new InvalidCurrencyCodeException(currencyCode);
            _iso = iso;
            _iso3 = iso3;
            _isoNumeric = isoNumeric;
            _name = name;
            _capital = capital;
            _continentCode = continentCode;
            _currencyCode = currencyCode;
            _countryLookupId = countryLookupId;
        }

        public void WhenCreated(Guid tenantId, Guid companyId, Guid countryId, string countryName)
            => AddEvent(new CountryHierarchyCreated(tenantId, companyId, countryId, countryName));

        public void WhenChanged(Guid tenantId, Guid companyId, Guid countryId, string countryName)
            => AddEvent(new CountryHierarchyUpdated(tenantId, companyId, countryId, countryName));

        public void WhenDeleted(Guid tenantId, string tenantName, Guid companyId, string compnayName, Guid countryId, string countryName)
            => AddEvent(new CountryHierarchyDeleted(tenantId, companyId, countryId, countryName));
    }
}

