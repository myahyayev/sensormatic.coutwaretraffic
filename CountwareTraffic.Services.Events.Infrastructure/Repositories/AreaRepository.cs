﻿using CountwareTraffic.Services.Events.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class AreaRepository : Repository<Area>, IAreaRepository
    {
        private readonly new EventDbContext _context;

        public AreaRepository(EventDbContext context) : base(context) => _context = context;

        #region dispose
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion dispose


        public async Task<Area> GetAsync(Guid id) => await base.GetQuery(null).SingleOrDefaultAsync(x => x.Id == id);
        public async Task<bool> ExistsAsync(Guid id) => await base.GetQuery(null).AnyAsync(u => u.Id == id);
    }
}
