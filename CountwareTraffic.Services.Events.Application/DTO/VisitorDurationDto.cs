﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class VisitorDurationDto
    {
        public VisitorDurationDto()
        {
            LastInwardsDate = LastOutwardsDate = DateTime.Now;
        }
        public DateTime LastInwardsDate { get; set; }
        public DateTime LastOutwardsDate { get; set; }
    }
}
