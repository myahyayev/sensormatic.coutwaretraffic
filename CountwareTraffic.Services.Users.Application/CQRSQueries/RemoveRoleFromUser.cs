﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class RemoveRoleFromUser : IQuery<IdentityRemoveRoleFromUserResponse>
    {
        public Guid UserId { get; set; }
        public string RoleName { get; set; }
    }
}
