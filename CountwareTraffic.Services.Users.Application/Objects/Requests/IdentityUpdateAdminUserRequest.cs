﻿using System.Collections.Generic;
using System;
using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application.Objects.Requests
{
    public class IdentityUpdateAdminUserRequest
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }

        [JsonPropertyName("lastName")]
        public string LastName { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonPropertyName("userName")]
        public string UserName { get; set; }

        [JsonPropertyName("status")]
        public bool Status { get; set; }

        [JsonPropertyName("twoFactorEnabled")]
        public bool TwoFactorEnabled { get; set; }

        [JsonPropertyName("gender")]
        public int Gender { get; set; }

        [JsonPropertyName("birthDate")]
        public DateTime Birthdate { get; set; }

        [JsonPropertyName("userType")]
        public int UserType { get; set; }

        [JsonPropertyName("address")]
        public string Address { get; set; }

        [JsonPropertyName("allowSms")]
        public bool AllowSms { get; set; }

        [JsonPropertyName("allowEmail")]
        public bool AllowEmail { get; set; }

        [JsonPropertyName("profilePic")]
        public string ProfilePic { get; set; }

        [JsonPropertyName("roles")]
        public List<string> Roles { get; set; }

        [JsonPropertyName("domain")]
        public string Domain { get; set; }

        [JsonPropertyName("tenantIds")]
        public List<string> TenantIds { get; set; }
    }
}
