﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record UnknownDeviceCreated : MessageEnvelope, IQueueEvent
    {
        public UnknownDeviceCreated(string name, string macAddress, string ipAddress, string serialNumber, int httpPort, int httpsPort, Guid recordId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            
            Name = name;
            MacAddress = macAddress;
            IpAddress = ipAddress;
            SerialNumber = serialNumber;
            HttpPort = httpPort;
            HttpsPort = httpsPort;
            RecordId = recordId;
        }
        public string MacAddress { get; }
        public string IpAddress { get; }
        public string Name { get; }
        public string SerialNumber { get; }
        public int HttpPort { get; }
        public int HttpsPort { get; }
        public Guid RecordId { get; init; }
    }
}
