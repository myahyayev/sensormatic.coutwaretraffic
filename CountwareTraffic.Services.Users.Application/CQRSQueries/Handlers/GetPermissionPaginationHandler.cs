﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetPermissionPaginationHandler : IQueryHandler<GetPermissionPagination, IdentityPermissionPaginationResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetPermissionPaginationHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityPermissionPaginationResponse> HandleAsync(GetPermissionPagination query)
        {
            return await _identityServerService.GetPermissionPaginationAsync(new IdentityPermissionPaginationRequest
            {
                Paging = query.Paging,
                Sorts = query.Sorts,
                Filters = query.Filters
            },query.Language);
        }
    }
}
