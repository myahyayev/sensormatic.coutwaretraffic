﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetCityLookups : IQuery<List<CityLookupDetailsDto>>
    {
        public Guid CountryId { get; set; }
    }
}
