﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class UserResetHandler : IQueryHandler<UserReset, bool>
    {
        private readonly IIdentityServerService _identityServerService;

        public UserResetHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<bool> HandleAsync(UserReset query)
        {
            return await _identityServerService.UserResetAsync(query.Id);
        }
    }
}
