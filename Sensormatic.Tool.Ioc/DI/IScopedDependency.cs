﻿using System;

namespace Sensormatic.Tool.Ioc
{
    public interface IScopedDependency : IDisposable { }
}
