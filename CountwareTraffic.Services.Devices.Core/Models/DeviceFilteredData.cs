﻿using System;

namespace CountwareTraffic.Services.Devices.Core
{
    public class DeviceFilteredData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid BrandId { get; set; }
        public Guid ModelId { get; set; }
        public string BrandName { get; set; }
        public string ModelName { get; set; }
        public int ControlFrequency { get; set; }
        public string FirmWare { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
        public Guid SubAreaId { get; set; }
        public string ConnectionInfo_IpAddress { get; set; }
        public int? ConnectionInfo_Port { get; set; }
        public string ConnectionInfo_Identity { get; set; }
        public string ConnectionInfo_Password { get; set; }
        public string ConnectionInfo_UniqueId { get; set; }
        public string ConnectionInfo_MacAddress { get; set; }
        public int DeviceStatusId { get; set; }
        public int DeviceTypeId { get; set; }
        public int DeviceCreationStatus { get; set; }
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public bool AuditIsDeleted { get; set; }
        public Guid TenantId { get; set; }
    }
}
