﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Engine.TrafficReport;
using DevExpress.XtraReports.UI;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public partial class DailyTrafficReport : ScheduleReport, ITrafficReport
    {
        private readonly ITrafficReportElasticSearchRepository _trafficReportElasticSearchRepository;

        public DailyTrafficReport() : base()
            => InitializeComponent();

        public DailyTrafficReport(IServiceProvider serviceProvider, IOptions<TimeZoneSettings> timeZoneSettings) : base(serviceProvider, timeZoneSettings)
        {
            _trafficReportElasticSearchRepository = serviceProvider.GetRequiredService<ITrafficReportElasticSearchRepository>();
            InitializeComponent();
        }

        public override void RefreshData()
        {
            if (ReportData is not null)
            {
                objectDataSource2.DataSource = ReportData as DailyTrafficReportData;

                FillDataSources(false);
                SetResources();
            }
        }
        public override void SetResources()
        {
            var reportData = ReportData as DailyTrafficReportData;

            List<XRTableCell> tablesHeaderlist = new();

            tablesHeaderlist.AddRange(ComparedStoresSummaryTableHeaderRow.Cells);
            tablesHeaderlist.AddRange(SummaryTableHeaderRow.Cells);

            tablesHeaderlist
                .ForEach(cell =>
                {
                    if (cell.Tag != null)
                        cell.Text = GetResouceContentByName(cell.Tag.ToString());
                });    
            
            ComparedStoresSummaryTitle.Text = reportData.ComparedStoresSummary.Title;
            ComparedStoresSummarySubTitle.Text = reportData.ComparedStoresSummary.SubTitle;
            ComparedStoresSummaryFooterTitle.Text = reportData.ComparedStoresSummary.FooterTitle;

            SummaryByStoreTitle.Text = reportData.SummaryByStore.Title;
            SummaryByStoreSubTitle.Text = reportData.SummaryByStore.SubTitle;
            SummaryByStoreFooterTitle.Text = reportData.SummaryByStore.FooterTitle;
            Total.Text = GetResouceContentByName(nameof(Total));

            TrafficByHourChartsTitle.Text = reportData.TrafficByHourChart.Title;
            TrafficByHourChartsSubTitle.Text = reportData.TrafficByHourChart.SubTitle;
            TrafficByHourChartsFooterText.Text = reportData.TrafficByHourChart.FooterTitle;

            TrafficByStoreChartsTitle.Text = reportData.TrafficByStoreChart.Title;
            TrafficByStoreChartsSubTitle.Text = reportData.TrafficByStoreChart.SubTitle;
            TrafficByStoreChartsFooter.Text = reportData.TrafficByStoreChart.FooterTitle;
        }
        public override async Task LoadDataSourceFromDataBaseAsync()
        {
            ReportData = new DailyTrafficReportData
            {
                ComparedStoresSummary = await TrafficComparedStoresSummaryAsync(),
                SummaryByStore = await TrafficSummaryByStoreAsync(),
                TrafficByHourChart = await TrafficByHourChartAsync(),
                TrafficByStoreChart = await TrafficByStoreChartAsync(),
                Title = TitleValue,
                SubTitle = SubTitleValue,
                CreatedBy = ReportCreator,
                CreatedDate = ReportCreatedDate
            };
            await base.LoadDataSourceFromDataBaseAsync();
        }


        public override string ResourceKey => $"{GetType().FullName}";

        #region private methods
        private async Task<BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary>> TrafficComparedStoresSummaryAsync()
            => new()
            {
                Content = await _trafficReportElasticSearchRepository
                    .TrafficComparedStoresSummaryAsync(ReportLocations, StartDate, EndDate, ReportPeriodType.Daily, TenantId, Language.Id),
                Title = GetResouceContentByName("ComparedStoresSummaryTitle"),
                SubTitle = GetResouceContentByName("ComparedStoresSummarySubTitle"),
                FooterTitle = $"{GetResouceContentByName("ComparedStoresSummaryFooterTitle")} : {ReportDateRange}"
            };
        

        private async Task<BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary>> TrafficSummaryByStoreAsync()
            => new()
            {   //todo:weather data will be injected to the model here
                Content = await _trafficReportElasticSearchRepository
                    .TrafficSummaryByStoreAsync(ReportLocations, StartDate, EndDate, ReportPeriodType.Daily, TenantId, Language.Id),
                Title = GetResouceContentByName("SummaryByStoreTitle"),
                FooterTitle = $"{GetResouceContentByName("SummaryByStoreFooterTitle")} : {ReportDateRange}"
            };


        private async Task<BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary>> TrafficByHourChartAsync()
            => new()
            {
                Content = await _trafficReportElasticSearchRepository
                    .TrafficByTimePeriodChartAsync(ReportLocations, StartDate, EndDate, ReportPeriodType.Daily, DateRangeType.ByHour, TenantId, Language.ShortName, Language.WindowsTimeZone, Language.LinuxTimeZone),
                Title = GetResouceContentByName("TrafficByHourChartsTitle"),
                FooterTitle = $"{GetResouceContentByName("TrafficByHourChartsFooterText")} : {ReportDateRange}"
            };


        private async Task<BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary>> TrafficByStoreChartAsync()
            => new()
            {
                Content = await _trafficReportElasticSearchRepository.TrafficByStoreChartAsync(ReportLocations, StartDate, EndDate, TenantId, Language.ShortName, Language.WindowsTimeZone, Language.LinuxTimeZone),
                Title = GetResouceContentByName("TrafficByStoreChartsTitle"),
                FooterTitle = $"{GetResouceContentByName("TrafficByStoreChartsFooter")} : {ReportDateRange}"
            };

        #endregion private methods
        //private void TrafficByHourChart_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        //{
        //    return;
        //    var diagram = (DevExpress.XtraCharts.XYDiagram)TrafficByHourChart.Diagram;


        //    var xAxis = diagram.AxisX;
        //    // xAxis.QualitativeScaleOptions.AutoGrid = false;
        //    diagram.AxisX.WholeRange.Auto = false;
        //    diagram.AxisX.VisualRange.Auto = true;

        //    var reportData = ReportData as DailyTrafficReportData;
        //    var a = reportData.TrafficByHourChart.Content.OrderBy(i=>i.CurrentPeriod.Caption);
        //    //a.Select(i => i.CurrentPeriod.Caption).ToList().ForEach((ii) => { xAxis.CustomLabels.Add(new CustomAxisLabel(ii)); });

        //    diagram.AxisX.WholeRange.MinValue = a.First().CurrentPeriod.Caption;
        //    diagram.AxisX.WholeRange.MaxValue = a.Last().CurrentPeriod.Caption;

        //}
    }
}
