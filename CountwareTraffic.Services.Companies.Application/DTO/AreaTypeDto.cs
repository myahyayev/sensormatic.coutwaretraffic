﻿namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
