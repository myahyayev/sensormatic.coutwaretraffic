﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ForgotPassword : IQuery<IdentityForgotPasswordResponse>
    {
        public string UserName { get; set; }
        public string Domain { get; set; }
        public string Language { get; set; }
    }
}
