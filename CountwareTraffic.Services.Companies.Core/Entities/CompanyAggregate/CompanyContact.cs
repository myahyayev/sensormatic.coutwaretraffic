﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;

namespace CountwareTraffic.Services.Companies.Core
{
    public record CompanyContact : IDeletableValueObject
    {
        private string _gsmDialCode;
        private string _gsmNumber;
        private string _gsmCountryCode;
        private string _phoneDialCode;
        private string _phoneNumber;
        private string _phoneCountryCode;
        private string _emailAddress;
        private string _personName;
        private string _gsmDialCodeSecondary;
        private string _gsmCountryCodeSecondary;
        private string _gsmNumberSecondary;
        private string _phoneDialCodeSecondary;
        private string _phoneNumberSecondary;
        private string _phoneCountryCodeSecondary;
        private string _emailAddressSecondary;
        private string _personNameSecondary;

        public string GsmCountryCode => _gsmCountryCode;
        public string PhoneCountryCode => _phoneCountryCode;
        public string GsmCountryCodeSecondary => _gsmCountryCodeSecondary;
        public string PhoneCountryCodeSecondary => _phoneCountryCodeSecondary;
        public string GsmDialCode => _gsmDialCode;
        public string GsmNumber => _gsmNumber;
        public string PhoneDialCode => _phoneDialCode;
        public string PhoneNumber => _phoneNumber;
        public string EmailAddress => _emailAddress;
        public string PersonName => _personName;
        public string GsmDialCodeSecondary => _gsmDialCodeSecondary;
        public string GsmNumberSecondary => _gsmNumberSecondary;
        public string PhoneDialCodeSecondary => _phoneDialCodeSecondary;
        public string PhoneNumberSecondary => _phoneNumberSecondary;
        public string EmailAddressSecondary => _emailAddressSecondary;
        public string PersonNameSecondary => _personNameSecondary;


        private CompanyContact() { }

        public static CompanyContact Create(string gsmDialCode, string gsmNumber, string phoneDialCode, string phoneNumber, string emailAddress,  string personName, string gsmDialCodeSecondary, string gsmNumberSecondary, string phoneDialCodeSecondary, string phoneNumberSecondary, string emailAddressSecondary, string personNameSecondary, string gsmCountryCode, string phoneCountryCode, string gsmCountryCodeSecondary, string phoneCountryCodeSecondary)
        {
            if (!personName.IsNullOrWhiteSpace() && personName.Length > 120)
                throw new InvalidIContactException(nameof(personName), personName);

            if (!personNameSecondary.IsNullOrWhiteSpace() && personNameSecondary.Length > 120)
                throw new InvalidIContactException(nameof(personNameSecondary), personNameSecondary);

            if (!gsmDialCode.IsNullOrWhiteSpace() && gsmDialCode.Length > 6)
                throw new InvalidIContactException(nameof(gsmDialCode), gsmDialCode);

            if (!phoneDialCode.IsNullOrWhiteSpace() && phoneDialCode.Length > 6)
                throw new InvalidIContactException(nameof(phoneDialCode), phoneDialCode);

            if (!phoneNumber.IsNullOrWhiteSpace() && phoneNumber.Length > 20)
                throw new InvalidIContactException(nameof(phoneNumber), phoneNumber);

            if (!gsmNumber.IsNullOrWhiteSpace() && gsmNumber.Length > 20)
                throw new InvalidIContactException(nameof(gsmNumber), gsmNumber);

            if (!emailAddress.IsNullOrWhiteSpace() && emailAddress.Length > 120)
                throw new InvalidIContactException(nameof(emailAddress), emailAddress);

            if (!gsmDialCodeSecondary.IsNullOrWhiteSpace() && gsmDialCodeSecondary.Length > 6)
                throw new InvalidIContactException(nameof(gsmDialCodeSecondary), gsmDialCodeSecondary);

            if (!phoneDialCodeSecondary.IsNullOrWhiteSpace() && phoneDialCodeSecondary.Length > 6)
                throw new InvalidIContactException(nameof(phoneDialCodeSecondary), phoneDialCodeSecondary);

            if (!phoneNumberSecondary.IsNullOrWhiteSpace() && phoneNumberSecondary.Length > 20)
                throw new InvalidIContactException(nameof(phoneNumberSecondary), phoneNumberSecondary);

            if (!gsmNumberSecondary.IsNullOrWhiteSpace() && gsmNumberSecondary.Length > 20)
                throw new InvalidIContactException(nameof(gsmNumberSecondary), gsmNumberSecondary);

            if (!emailAddressSecondary.IsNullOrWhiteSpace() && emailAddressSecondary.Length > 120)
                throw new InvalidIContactException(nameof(emailAddressSecondary), emailAddressSecondary);

            if (!gsmCountryCode.IsNullOrWhiteSpace() && gsmCountryCode.Length != 2)
                throw new InvalidIContactException(nameof(gsmCountryCode), gsmCountryCode);

            if (!phoneCountryCode.IsNullOrWhiteSpace() && phoneCountryCode.Length != 2)
                throw new InvalidIContactException(nameof(phoneCountryCode), phoneCountryCode);

            if (!gsmCountryCodeSecondary.IsNullOrWhiteSpace() && gsmCountryCodeSecondary.Length != 2)
                throw new InvalidIContactException(nameof(gsmCountryCodeSecondary), gsmCountryCodeSecondary);

            if (!phoneCountryCodeSecondary.IsNullOrWhiteSpace() && phoneCountryCodeSecondary.Length != 2)
                throw new InvalidIContactException(nameof(phoneCountryCodeSecondary), phoneCountryCodeSecondary);

            return new CompanyContact
            {
                _gsmDialCode = gsmDialCode,
                _phoneDialCode = phoneDialCode,
                _personName = personName,
                _gsmNumber = gsmNumber,
                _phoneNumber = phoneNumber,
                _emailAddress = emailAddress,
                _gsmDialCodeSecondary = gsmDialCodeSecondary,
                _phoneDialCodeSecondary = phoneDialCodeSecondary,
                _personNameSecondary = personNameSecondary,
                _gsmNumberSecondary = gsmNumberSecondary,
                _phoneNumberSecondary = phoneNumberSecondary,
                _emailAddressSecondary = emailAddressSecondary,
                _gsmCountryCode = gsmCountryCode,
                _phoneCountryCode = phoneCountryCode,
                _gsmCountryCodeSecondary = gsmCountryCodeSecondary,
                _phoneCountryCodeSecondary = phoneCountryCodeSecondary
            };
        }

        public void WhenChanged(string gsmDialCode, string gsmNumber, string phoneDialCode, string phoneNumber, string emailAddress, string personName, string gsmDialCodeSecondary, string gsmNumberSecondary, string phoneDialCodeSecondary, string phoneNumberSecondary, string emailAddressSecondary, string personNameSecondary, string gsmCountryCode, string phoneCountryCode, string gsmCountryCodeSecondary, string phoneCountryCodeSecondary)
        {
            if (!personName.IsNullOrWhiteSpace() && personName.Length > 120)
                throw new InvalidIContactException(nameof(personName), personName);

            if (!personNameSecondary.IsNullOrWhiteSpace() && personNameSecondary.Length > 120)
                throw new InvalidIContactException(nameof(personNameSecondary), personNameSecondary);

            if (!gsmDialCode.IsNullOrWhiteSpace() && gsmDialCode.Length > 6)
                throw new InvalidIContactException(nameof(gsmDialCode), gsmDialCode);

            if (!phoneDialCode.IsNullOrWhiteSpace() && phoneDialCode.Length > 6)
                throw new InvalidIContactException(nameof(phoneDialCode), phoneDialCode);

            if (!phoneNumber.IsNullOrWhiteSpace() && phoneNumber.Length > 20)
                throw new InvalidIContactException(nameof(phoneNumber), phoneNumber);

            if (!gsmNumber.IsNullOrWhiteSpace() && gsmNumber.Length > 20)
                throw new InvalidIContactException(nameof(gsmNumber), gsmNumber);

            if (!emailAddress.IsNullOrWhiteSpace() && emailAddress.Length > 120)
                throw new InvalidIContactException(nameof(emailAddress), emailAddress);

            if (!gsmDialCodeSecondary.IsNullOrWhiteSpace() && gsmDialCodeSecondary.Length > 6)
                throw new InvalidIContactException(nameof(gsmDialCodeSecondary), gsmDialCodeSecondary);

            if (!phoneDialCodeSecondary.IsNullOrWhiteSpace() && phoneDialCodeSecondary.Length > 6)
                throw new InvalidIContactException(nameof(phoneDialCodeSecondary), phoneDialCodeSecondary);

            if (!phoneNumberSecondary.IsNullOrWhiteSpace() && phoneNumberSecondary.Length > 20)
                throw new InvalidIContactException(nameof(phoneNumberSecondary), phoneNumberSecondary);

            if (!gsmNumberSecondary.IsNullOrWhiteSpace() && gsmNumberSecondary.Length > 20)
                throw new InvalidIContactException(nameof(gsmNumberSecondary), gsmNumberSecondary);

            if (!emailAddressSecondary.IsNullOrWhiteSpace() && emailAddressSecondary.Length > 120)
                throw new InvalidIContactException(nameof(emailAddressSecondary), emailAddressSecondary);

            if (!gsmCountryCode.IsNullOrWhiteSpace() && gsmCountryCode.Length != 2)
                throw new InvalidIContactException(nameof(gsmCountryCode), gsmCountryCode);

            if (!phoneCountryCode.IsNullOrWhiteSpace() && phoneCountryCode.Length != 2)
                throw new InvalidIContactException(nameof(phoneCountryCode), phoneCountryCode);

            if (!gsmCountryCodeSecondary.IsNullOrWhiteSpace() && gsmCountryCodeSecondary.Length != 2)
                throw new InvalidIContactException(nameof(gsmCountryCodeSecondary), gsmCountryCodeSecondary);

            if (!phoneCountryCodeSecondary.IsNullOrWhiteSpace() && phoneCountryCodeSecondary.Length != 2)
                throw new InvalidIContactException(nameof(phoneCountryCodeSecondary), phoneCountryCodeSecondary);


            _gsmDialCode = gsmDialCode;
            _phoneDialCode = phoneDialCode;
            _personName = personName;
            _gsmNumber = gsmNumber;
            _phoneNumber = phoneNumber;
            _emailAddress = emailAddress;
            _gsmDialCodeSecondary = gsmDialCodeSecondary;
            _phoneDialCodeSecondary = phoneDialCodeSecondary;
            _personNameSecondary = personNameSecondary;
            _gsmNumberSecondary = gsmNumberSecondary;
            _phoneNumberSecondary = phoneNumberSecondary;
            _emailAddressSecondary = emailAddressSecondary;
            _gsmCountryCode = gsmCountryCode;
            _phoneCountryCode = phoneCountryCode;
            _gsmCountryCodeSecondary = gsmCountryCodeSecondary;
            _phoneCountryCodeSecondary = phoneCountryCodeSecondary;
        }
    }
}
