﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class DapperDbConnectionFactory : IDbConnectionFactory
    {
        private readonly IDictionary<string, string> _connectionDict;

        public DapperDbConnectionFactory(IDictionary<string, string> connectionDict)
            => _connectionDict = connectionDict;

        public IDbConnection CreateDbConnection(string connectionName)
        {
            if (_connectionDict.TryGetValue(connectionName, out string connectionString))
                return new SqlConnection(connectionString);

            throw new ArgumentNullException();
        }
    }
}
