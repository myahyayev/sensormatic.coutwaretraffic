﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using CountwareTraffic.Services.Companies.Api;
namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class CreateCompanyRequest : SensormaticRequestValidate
    {
        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1265 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1266 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1267 Description invalid format", nameof(Description)));

            if (!City.IsNullOrWhiteSpace() && City.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1268 City invalid format", nameof(City)));

            if (!Street.IsNullOrWhiteSpace() && Street.Length > 250)
                ValidateResults.Add(new ErrorResult($"#E1269 Street invalid format", nameof(Street)));

            if (!State.IsNullOrWhiteSpace() && State.Length > 250)
                ValidateResults.Add(new ErrorResult($"#E1270 State invalid format", nameof(State)));

            if (!Country.IsNullOrWhiteSpace() && Country.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1271 Country invalid format", nameof(Country)));

            if (!ZipCode.IsNullOrWhiteSpace() && ZipCode.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1272 ZipCode invalid format", nameof(ZipCode)));

            if (!EmailAddress.IsNullOrWhiteSpace() && (!EmailValidator.EmailIsValid(EmailAddress) || EmailAddress.Length > 120))
                ValidateResults.Add(new ErrorResult($"#E1273 Email address invalid format", nameof(EmailAddress)));

            if (!PhoneNumber.IsNullOrWhiteSpace() && PhoneNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1274 PhoneNumber invalid format", nameof(PhoneNumber)));

            if (!GsmNumber.IsNullOrWhiteSpace() && GsmNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1275 GsmNumber invalid format", nameof(GsmNumber)));

            if (!GsmDialCode.IsNullOrWhiteSpace() && GsmDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1276 GsmDialCode invalid format", nameof(GsmDialCode)));

            if (!PhoneDialCode.IsNullOrWhiteSpace() && PhoneDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1277 PhoneDialCode invalid format", nameof(PhoneDialCode)));

            if (!PersonName.IsNullOrWhiteSpace() && PersonName.Length > 120)
                ValidateResults.Add(new ErrorResult($"#E1278 PersonName invalid format", nameof(PersonName)));

            if (!GsmDialCodeSecondary.IsNullOrWhiteSpace() && GsmDialCodeSecondary.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1279 GsmDialCodeSecondary invalid format", nameof(GsmDialCodeSecondary)));

            if (!GsmNumberSecondary.IsNullOrWhiteSpace() && GsmNumberSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1280 GsmNumberSecondary invalid format", nameof(GsmNumberSecondary)));

            if (!PhoneDialCodeSecondary.IsNullOrWhiteSpace() && PhoneDialCodeSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1281 PhoneDialCodeSecondary invalid format", nameof(PhoneDialCodeSecondary)));

            if (!PhoneNumberSecondary.IsNullOrWhiteSpace() && PhoneNumberSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1282 PhoneNumberSecondary invalid format", nameof(PhoneNumberSecondary)));

            if (!EmailAddressSecondary.IsNullOrWhiteSpace() && (!EmailValidator.EmailIsValid(EmailAddressSecondary) || EmailAddressSecondary.Length > 120))
                ValidateResults.Add(new ErrorResult($"#E1283 Email address secondary invalid format", nameof(EmailAddressSecondary)));

            if (!PersonNameSecondary.IsNullOrWhiteSpace() && PersonNameSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1284 PersonNameSecondary invalid format", nameof(PersonNameSecondary)));

            if (!GsmCountryCode.IsNullOrWhiteSpace() && GsmCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1285 GsmCountryCode invalid format", nameof(GsmCountryCode)));

            if (!PhoneCountryCode.IsNullOrWhiteSpace() && PhoneCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1286 PhoneCountryCode invalid format", nameof(PhoneCountryCode)));

            if (!GsmCountryCodeSecondary.IsNullOrWhiteSpace() && GsmCountryCodeSecondary.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1287 GsmCountryCodeSecondary invalid format", nameof(GsmCountryCodeSecondary)));

            if (!PhoneCountryCodeSecondary.IsNullOrWhiteSpace() && PhoneCountryCodeSecondary.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1288 PhoneCountryCodeSecondary invalid format", nameof(PhoneCountryCodeSecondary)));
        }
    }
}
