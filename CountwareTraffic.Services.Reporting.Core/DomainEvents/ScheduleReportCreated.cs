﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleReportCreated : IDomainEvent
    {
        public Guid ReportId { get; set; }
        public Guid RecordId { get; init; }

        public ScheduleReportCreated(Guid reportId)
        {
            ReportId = reportId;
            RecordId = Guid.NewGuid();
        }
    }
}
