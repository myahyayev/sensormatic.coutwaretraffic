﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetAllHierarchyHandler : IQueryHandler<GetAllHierarchy, string>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        private readonly ICurrentTenant _currentTenant;
        public GetAllHierarchyHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository, ICurrentTenant currentTenant)
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
            _currentTenant = currentTenant;
        }

        public async Task<string> HandleAsync(GetAllHierarchy query)
        {
            return await _tenantHierarchyElasticSearchRepository.GetTenantHierarchyJsonFormatAsync(_currentTenant.Id);
        }
    }
}
