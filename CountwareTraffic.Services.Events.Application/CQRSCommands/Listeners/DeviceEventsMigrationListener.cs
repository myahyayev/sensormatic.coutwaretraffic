﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public record DeviceMigrationEventsListener : Convey.CQRS.Commands.ICommand
    {
        public DeviceMigrationEventsListener()
        {
            Items = new();
        }
        public List<DeviceEventsRangeElement> Items { get; init; }
        public Guid RecordId { get; set; }
        public Guid DeviceId { get; set; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
    }
    public record DeviceEventsRangeElement
    {
        public string Description { get; init; }
        public Guid DeviceId { get; init; }
        public int OnTimeIOCount { get; init; }
        public int DirectionTypeId { get; init; }
        public Guid RecordId { get; init; }
        public DateTime EventDate { get; set; }
        public DateTime EventStartDate { get; set; }
        public DateTime EventEndDate { get; set; }
        public Guid UserId { get; set; }
        public string DeviceName { get; set; }
        public Guid TenantId { get; set; }
    }
}
