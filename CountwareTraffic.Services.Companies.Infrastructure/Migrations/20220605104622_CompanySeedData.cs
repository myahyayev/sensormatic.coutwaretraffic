﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class CompanySeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT INTO [areas].[SubAreaStatuses] ( Id, Name ) VALUES
                                                       (1, 'Created'), (2, 'Completed'), (3, 'Rejected')");

            migrationBuilder.Sql(@"INSERT INTO [areas].[AreaTypes] ( Id, Name ) VALUES
                                                       (1, 'Unknown'), (2, 'Franchising'), (3, 'CompanyOwned')");



            migrationBuilder.Sql(@"INSERT INTO [areas].[CountryLookups]
                                       ([Id]
                                       ,[Iso]
                                       ,[Iso3]
                                       ,[IsoNumeric]
                                       ,[Name]
                                       ,[Capital]
                                       ,[ContinentCode]
                                       ,[CurrencyCode])
                                 VALUES
                                       ('12679675-CD70-491E-B44D-485612C85952'
                                       ,'TR'
                                       ,'TUR'
                                       ,'792'
                                       ,'Turkey'
                                       ,'Ankara'
                                       ,'90'
                                       ,'TRY'
		                               )"
                              );



            Dictionary<Guid, string> cities = new Dictionary<Guid, string>();
            cities.Add(new Guid("002341E9-31CB-41E3-B1DF-039DB55C66DF"),	"Hatay");
            cities.Add(new Guid("E0434953-6EAB-4239-9186-07DA662A9AAB"), "Trabzon");
            cities.Add(new Guid("F7EFBF1E-C21A-4191-8902-099A7FE7B3F7"), "Zonguldak");
            cities.Add(new Guid("F34BAB24-E42D-4AAC-9608-0C810A730668"), "Karabük");
            cities.Add(new Guid("B2693E98-778B-46AB-853C-0D321A198EF3"), "Karaman");
            cities.Add(new Guid("38CCE807-1B5E-49A5-B664-0D5BB1E95E65"), "Mus");
            cities.Add(new Guid("2ACD65FB-A905-430D-8464-14F582132ECA"), "Artvin");
            cities.Add(new Guid("6AC6CF39-15B4-46A4-8416-16F12E08C943"), "Bursa");
            cities.Add(new Guid("6BDBD58A-7306-4DF6-9C7C-1DD7C0A17FDA"), "Ankara");
            cities.Add(new Guid("2237C918-866F-4ADD-B865-28D2ED64173C"), "Nevsehir");
            cities.Add(new Guid("0B9035D4-FF01-470A-8906-2D43C1C09863"), "Kütahya");
            cities.Add(new Guid("C656E6CC-010E-4D16-9F5C-2F2BFAE7B78D"), "Usak");
            cities.Add(new Guid("9C1A65CA-A1D3-4B89-BD13-2F35251C01DE"), "Gümüshane");
            cities.Add(new Guid("54DBF968-7BE9-4DD5-AA64-30088DA21E19"), "Bartin");
            cities.Add(new Guid("A82EAC68-9641-428E-82C5-35147BD56578"), "Kirikkale");
            cities.Add(new Guid("B82127EA-FE38-4E9D-B8C0-3ADEF4731D30"), "Eskisehir");
            cities.Add(new Guid("E38F265D-CA51-43F1-B897-3B3C8277736F"), "Ardahan");
            cities.Add(new Guid("233C6068-1C94-4A5E-9198-40927B2C0563"), "Kars");
            cities.Add(new Guid("D4807D29-A421-481A-A617-427F59490EB9"), "Çankiri");
            cities.Add(new Guid("19096911-55AE-4615-9A5B-4538D165BAEC"), "Nigde");
            cities.Add(new Guid("77647201-6A8B-4390-BBF8-471337DA6E88"), "Kirsehir");
            cities.Add(new Guid("BABB1CBC-AE2F-4009-9AB5-484A7766EE4A"), "Balikesir");
            cities.Add(new Guid("DBA3C927-5DFD-4AD2-A202-49E62775DB52"), "Sakarya");
            cities.Add(new Guid("0CCCB927-71C6-4B23-9C9B-4A6A5E57CD33"), "Mardin");
            cities.Add(new Guid("3EC80199-D912-42A8-B221-4B435F4982E4"), "Tekirdag");
            cities.Add(new Guid("681E35B6-5B14-4E53-BCA1-4B5A8E635F85"), "Adiyaman");
            cities.Add(new Guid("AA82F1D0-0AB1-42FA-9783-4C2D72CE3EA4"), "Adana");
            cities.Add(new Guid("035D5276-22A9-4545-BF89-4F032956B620"), "Amasya");
            cities.Add(new Guid("C978FF08-A3F8-4FDA-9E4F-55024EBFA7B6"), "Izmir");
            cities.Add(new Guid("A44354C2-36DF-4FE4-8F27-573A305F4B2A"), "Bingöl");
            cities.Add(new Guid("CF8D31C6-112F-4FC6-9FD7-5992A7D3FD9A"), "Sinop");
            cities.Add(new Guid("CF161B90-CFDD-4B1C-8CBE-61EA1BB0082F"), "Siirt");
            cities.Add(new Guid("830EFE51-BE02-44C1-9B4B-67D7DF66AC92"), "Osmaniye");
            cities.Add(new Guid("517982B9-8651-4F61-966C-69BC2E7B35FD"), "Aydin");
            cities.Add(new Guid("1382F76E-1E9B-4F02-ADCE-6DED9BC3D997"), "Bitlis");
            cities.Add(new Guid("DAD970B6-4670-4E44-8057-6F723FDA1CF4"), "Sirnak");
            cities.Add(new Guid("633CF9EA-6B42-41A9-AD5C-7373488F9BA8"), "Çanakkale");
            cities.Add(new Guid("E2857F4F-4325-4B92-89D3-7408329AA5EA"), "Kayseri");
            cities.Add(new Guid("1A20CED7-B63D-4362-87BD-7AA6C03718F3"), "Giresun");
            cities.Add(new Guid("6E3981F7-5765-452C-A927-7F1FB7AA97AA"), "Bolu");
            cities.Add(new Guid("85A5D31B-D6DC-48DC-83D0-80AC2D760EE6"), "Edirne");
            cities.Add(new Guid("011B0174-581A-47B3-A919-829803611AD9"), "Diyarbakir");
            cities.Add(new Guid("44F41FC7-4ECA-46C7-9EBD-899BFF3487D4"), "Sanliurfa");
            cities.Add(new Guid("49DF0B0E-3EC6-4B25-B21E-9256CC1C8205"), "Yozgat");
            cities.Add(new Guid("D3A27944-6404-4F10-AAE2-94FA38DDE42C"), "Tokat");
            cities.Add(new Guid("BB8492CC-857E-4C6B-A28F-9B6C2E066A99"), "Gaziantep");
            cities.Add(new Guid("76A14E8A-EB9B-42F1-8E12-9D8DC864B1C6"), "Konya");
            cities.Add(new Guid("2A3578A9-4736-43E0-875D-A228A408F274"), "Kahramanmaras");
            cities.Add(new Guid("627527DC-D1A4-4DE8-A04F-A34E9A4C84AA"), "Hakkâri");
            cities.Add(new Guid("88310C6A-563A-40EA-B713-A45DD3B7426F"), "Mersin");
            cities.Add(new Guid("A8E35E87-3F1C-4FC6-9DE2-AC290FE3283F"), "Samsun");
            cities.Add(new Guid("B75711E6-4228-4D0A-AC86-ACD0EACE0527"), "Bayburt");
            cities.Add(new Guid("A5522F8C-6F4C-4CFE-B5E8-B74461CABFB7"), "Istanbul");
            cities.Add(new Guid("AE1A06FA-8045-46FD-A1A3-B95B9B1C429A"), "Kocaeli");
            cities.Add(new Guid("50101B1C-3D6F-42F9-A056-BDBC31E6B554"), "Agri");
            cities.Add(new Guid("DBA06D1B-949A-43B6-903F-BDD2AEC66768"), "Kastamonu");
            cities.Add(new Guid("09E4E7DC-98CE-4283-8766-BE3AF41EBF1F"), "Elazig");
            cities.Add(new Guid("F50D4611-38F0-4AD2-BAA5-BFBBE78F5F76"), "Erzincan");
            cities.Add(new Guid("E08F37F3-E758-43C3-BF4D-C55230E16F2F"), "Burdur");
            cities.Add(new Guid("60E8570B-201C-4C31-8B48-C7CD6C5383E7"), "Kilis");
            cities.Add(new Guid("83D6547C-A7D8-4A1A-8C26-CB1BFB3DC94D"), "Batman");
            cities.Add(new Guid("ED5CB1FC-650C-44D0-8E2D-CC0486182FFF"), "Düzce");
            cities.Add(new Guid("9653E79F-C6F8-4BAA-A626-CE51804D0670"), "Isparta");
            cities.Add(new Guid("84248BAA-501A-4DD9-BD94-D1F89A9CC130"), "Antalya");
            cities.Add(new Guid("2C4079AC-07CA-4880-8565-D2D52A92D208"), "Manisa");
            cities.Add(new Guid("13AA008F-43B5-43B7-BF54-D50A26DD05EB"), "Rize");
            cities.Add(new Guid("77879F28-C5FF-4849-B04E-D6AA9738AACA"), "Ordu");
            cities.Add(new Guid("DFF477C0-7BAD-4804-9654-D7AF79D58564"), "Yalova");
            cities.Add(new Guid("C024B108-79A8-40EA-ACCF-D9F7D7B15DB3"), "Sivas");
            cities.Add(new Guid("1C9AA770-82E3-4280-AEB7-DCA30B86FD0A"), "Çorum");
            cities.Add(new Guid("7AC64D0A-79B5-4887-A32E-DEF1F1843B1C"), "Kirklareli");
            cities.Add(new Guid("93B2CA36-1957-4FD7-ACF2-E84E58E473D1"), "Mugla");
            cities.Add(new Guid("21D9A185-29C6-4383-A723-E8F121F26DD3"), "Igdir");
            cities.Add(new Guid("4422449D-33F9-45AC-BB9A-EB2BFD283165"), "Van");
            cities.Add(new Guid("E9D609D3-C6B9-4387-A6E1-EE17957A2DF8"), "Erzurum");
            cities.Add(new Guid("546AE5D8-3354-46F1-A8DE-EF2A020D681F"), "Tunceli");
            cities.Add(new Guid("BC9B1FF1-F728-47E6-B128-EFF2A29FE78B"), "Aksaray");
            cities.Add(new Guid("AF457A19-83BC-4E36-93BA-F70C901D6A28"), "Afyonkarahisar");
            cities.Add(new Guid("BF51D9DE-2B9D-4621-AF55-F77C1E6BB994"), "Denizli");
            cities.Add(new Guid("4422AD33-3F8B-49BD-9768-F783358A2F77"), "Bilecik");
            cities.Add(new Guid("17C5BF84-BDAB-4F37-AD11-FF97FC2F4120"), "Malatya");

            foreach (var city in cities)
            {
                string insertQuery = $@"INSERT INTO[areas].[CityLookups]
                (      
                       [Id]
                      ,[Name]
                      ,[CountryId]
                )
                 VALUES
                 (
                         '{city.Key}'
                       , '{city.Value}'
                       , '12679675-CD70-491E-B44D-485612C85952'
                 )";

                migrationBuilder.Sql(insertQuery);
            }



            Dictionary<Guid, string> districts = new Dictionary<Guid, string>(); //Sadece Istanbul
            districts.Add(new Guid("46AD83C2-16B2-4D64-B728-06400E3A3003"), "Basaksehir"); 
            districts.Add(new Guid("5A1C80FF-7DFD-45FE-8AFF-08BFE0DDA914"), "Çatalca"); 
            districts.Add(new Guid("17792749-22AA-44A0-B377-09920C5FE3E6"), "Bayrampasa");
            districts.Add(new Guid("AA33B185-5BB1-4A22-9178-0C12F8346098"), "Gaziosmanpasa");
            districts.Add(new Guid("303EC16F-F44C-4825-988C-16F4219397CC"), "Ümraniye");
            districts.Add(new Guid("00C5021A-9373-4839-8701-240205206AB7"), "Sisli"); 
            districts.Add(new Guid("A0199F11-2AEE-45B2-BBF6-32BDD9549FC2"), "Sultanbeyli"); 
            districts.Add(new Guid("FCAF9ED2-9A57-45B2-8D60-37094B2FC790"), "Adalar");
            districts.Add(new Guid("C4071272-00E3-4D4C-A1C8-46C3C70DCF8C"), "Eyüpsultan"); 
            districts.Add(new Guid("5337030B-C4BC-4C9F-BB71-507D4AE5AD00"), "Besiktas");
            districts.Add(new Guid("1E174D4D-E92D-4B99-BBEE-5160D83E9822"), "Tuzla");
            districts.Add(new Guid("85EF35DB-5FC8-4360-852F-52E399719096"), "Küçükçekmece");
            districts.Add(new Guid("9352FE0B-6E1C-41C6-9457-55240A184935"), "Üsküdar"); 
            districts.Add(new Guid("A1CEF825-F1EA-40E1-9D60-5BFB010CF3B2"), "Fatih");
            districts.Add(new Guid("B4E01120-2BDE-4E7F-9539-5ED72ACB4AC8"), "Silivri"); 
            districts.Add(new Guid("71C2CE35-4E4B-4962-909A-6038C70B5288"), "Kadiköy"); 
            districts.Add(new Guid("2FDF801A-4821-4875-A915-67352456E232"), "Atasehir"); 
            districts.Add(new Guid("900547DD-D91A-43F4-9490-676A7C97F240"), "Esenyurt"); 
            districts.Add(new Guid("16F74AB2-C7D4-4D24-AA59-69CC111483D7"), "Arnavutköy"); 
            districts.Add(new Guid("E02FE133-870A-4276-BEAA-8DDABD4DDEA6"), "Sile"); 
            districts.Add(new Guid("069B6D2A-C094-4415-8344-94A79BC9BB87"), "Esenler"); 
            districts.Add(new Guid("3AA1E505-5164-40C1-9336-9B2005ACDFF3"), "Kagithane");
            districts.Add(new Guid("799D48C8-776D-47F2-8A8E-9E4B9346A523"), "Sultangazi"); 
            districts.Add(new Guid("FDB063FD-E561-476A-B921-A50FA40058BB"), "Zeytinburnu"); 
            districts.Add(new Guid("F412179A-1781-4C77-91A6-ABBEA8C57827"), "Beyoglu"); 
            districts.Add(new Guid("FB80BCC9-8930-4003-B735-AEEB33DF311D"), "Avcilar"); 
            districts.Add(new Guid("E2888C18-BC5A-4FA4-8CA3-B6AA7A6BC599"), "Maltepe"); 
            districts.Add(new Guid("AF5A5F49-BEC7-4030-BA8E-B6D6B3DE36AF"), "Beykoz"); 
            districts.Add(new Guid("117F7A8B-1FFA-4FF9-B01E-B847EA50C995"), "Güngören"); 
            districts.Add(new Guid("0E1B04AB-C5E7-47AA-B9C0-BEF5FF940365"), "Bakirköy");
            districts.Add(new Guid("CA6D7DE5-D5FE-4032-96A0-C1F26BB626D8"), "Bahçelievler"); 
            districts.Add(new Guid("568A57F5-EEF0-4BE7-A0BC-C5D59AC950C3"), "Beylikdüzü"); 
            districts.Add(new Guid("FE559A7E-6092-437C-8760-C8BC8DFFE1EC"), "Sancaktepe"); 
            districts.Add(new Guid("EE57BA41-DAEA-4A9F-A3B3-CCD8AF59E082"), "Pendik");
            districts.Add(new Guid("D92928A5-8442-49D2-BC69-D02DC430DAAF"), "Büyükçekmece"); 
            districts.Add(new Guid("982E10F6-7D63-43C5-824C-DFC69EEF6D1D"), "Çekmeköy");
            districts.Add(new Guid("F1A9AD9A-C139-4C9E-B19F-EE048D27DFAD"), "Sariyer");
            districts.Add(new Guid("00B467BF-090E-454A-8956-F98C82FE1FF1"), "Bagcilar"); 
            districts.Add(new Guid("136648B7-7111-4103-9C6B-FEC65A68724C"), "Kartal");
            foreach (var district in districts) 
            {
                string insertQuery = $@"INSERT INTO[areas].[DistrictLookups]
                (      
                       [Id]
                      ,[Name]
                      ,[CityId]
                )
                 VALUES
                 (
                         '{district.Key}'
                       , '{district.Value}'
                       , 'A5522F8C-6F4C-4CFE-B5E8-B74461CABFB7'
                 )";

                migrationBuilder.Sql(insertQuery);
            }


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
