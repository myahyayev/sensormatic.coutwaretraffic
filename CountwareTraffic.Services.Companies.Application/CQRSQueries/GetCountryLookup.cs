﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetCountryLookup: IQuery<CountryLookupDetailsDto>
    {
        public Guid CountryId { get; set; }
    }
}
