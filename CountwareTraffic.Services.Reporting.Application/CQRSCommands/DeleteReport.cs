﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    [Contract]
    public class DeleteReport : ICommand
    {
        public Guid ReportId { get; set; }
    }
}
