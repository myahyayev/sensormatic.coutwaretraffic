﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class CompanyInitilaCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "areas");

            migrationBuilder.EnsureSchema(
                name: "area.app");

            migrationBuilder.CreateTable(
                name: "AreaTypes",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    Address_Street = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Address_City = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Address_State = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Address_Country = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Address_ZipCode = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Address_Location = table.Column<Point>(type: "geography", nullable: true),
                    Contact_GsmNumber = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Contact_PhoneNumber = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Contact_EmailAddress = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: true),
                    Contact_FaxNumber = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    AuditCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    AuditCreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CountryLookups",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Iso = table.Column<string>(type: "nvarchar(2)", maxLength: 2, nullable: false),
                    Iso3 = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: true),
                    IsoNumeric = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Capital = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ContinentCode = table.Column<string>(type: "nvarchar(2)", maxLength: 2, nullable: true),
                    CurrencyCode = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountryLookups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OutboxMessages",
                schema: "area.app",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EventRecordId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OccurredOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Data = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProcessedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastException = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsTryFromQueue = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutboxMessages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubAreaStatuses",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubAreaStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Iso = table.Column<string>(type: "nvarchar(2)", maxLength: 2, nullable: false),
                    Iso3 = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: true),
                    IsoNumeric = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Capital = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ContinentCode = table.Column<string>(type: "nvarchar(2)", maxLength: 2, nullable: true),
                    CurrencyCode = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: true),
                    CompanyId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CountryLookupId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    AuditCreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Countries_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "areas",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CityLookups",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    CountryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CityLookups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CityLookups_CountryLookups_CountryId",
                        column: x => x.CountryId,
                        principalSchema: "areas",
                        principalTable: "CountryLookups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    ManagerId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ManagerName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CountryId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    AuditCreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Regions_Countries_CountryId",
                        column: x => x.CountryId,
                        principalSchema: "areas",
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DistrictLookups",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    CityId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistrictLookups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DistrictLookups_CityLookups_CityId",
                        column: x => x.CityId,
                        principalSchema: "areas",
                        principalTable: "CityLookups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Areas",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    DistrictId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AreaTypeId = table.Column<int>(type: "int", nullable: false),
                    RegionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Address_Street = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Address_Location = table.Column<Point>(type: "geography", nullable: true),
                    Contact_GsmNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Contact_PhoneNumber = table.Column<string>(type: "nvarchar(16)", maxLength: 16, nullable: true),
                    Contact_EmailAddress = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Contact_FaxNumber = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    CityId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    AuditCreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Areas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Areas_AreaTypes_AreaTypeId",
                        column: x => x.AreaTypeId,
                        principalSchema: "areas",
                        principalTable: "AreaTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Areas_Regions_RegionId",
                        column: x => x.RegionId,
                        principalSchema: "areas",
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubAreas",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    AreaId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubAreaStatus = table.Column<int>(type: "int", nullable: false),
                    AuditCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditCreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubAreas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubAreas_Areas_AreaId",
                        column: x => x.AreaId,
                        principalSchema: "areas",
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubAreas_SubAreaStatuses_SubAreaStatus",
                        column: x => x.SubAreaStatus,
                        principalSchema: "areas",
                        principalTable: "SubAreaStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Areas_AreaTypeId",
                schema: "areas",
                table: "Areas",
                column: "AreaTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Areas_RegionId",
                schema: "areas",
                table: "Areas",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_CityLookups_CountryId",
                schema: "areas",
                table: "CityLookups",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Countries_CompanyId",
                schema: "areas",
                table: "Countries",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_DistrictLookups_CityId",
                schema: "areas",
                table: "DistrictLookups",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Regions_CountryId",
                schema: "areas",
                table: "Regions",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_SubAreas_AreaId",
                schema: "areas",
                table: "SubAreas",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_SubAreas_SubAreaStatus",
                schema: "areas",
                table: "SubAreas",
                column: "SubAreaStatus");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DistrictLookups",
                schema: "areas");

            migrationBuilder.DropTable(
                name: "OutboxMessages",
                schema: "area.app");

            migrationBuilder.DropTable(
                name: "SubAreas",
                schema: "areas");

            migrationBuilder.DropTable(
                name: "CityLookups",
                schema: "areas");

            migrationBuilder.DropTable(
                name: "Areas",
                schema: "areas");

            migrationBuilder.DropTable(
                name: "SubAreaStatuses",
                schema: "areas");

            migrationBuilder.DropTable(
                name: "CountryLookups",
                schema: "areas");

            migrationBuilder.DropTable(
                name: "AreaTypes",
                schema: "areas");

            migrationBuilder.DropTable(
                name: "Regions",
                schema: "areas");

            migrationBuilder.DropTable(
                name: "Countries",
                schema: "areas");

            migrationBuilder.DropTable(
                name: "Companies",
                schema: "areas");
        }
    }
}
