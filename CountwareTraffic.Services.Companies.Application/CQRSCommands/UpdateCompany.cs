﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    [Contract]
    public class UpdateCompany : ICommand
    {
        public Guid CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        //Contact
        public string EmailAddress { get; set; }
        public string GsmNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string GsmDialCode { get; set; }
        public string PhoneDialCode { get; set; }
        public string PersonName { get; set; }
        public string GsmDialCodeSecondary { get; set; }
        public string GsmNumberSecondary { get; set; }
        public string PhoneDialCodeSecondary { get; set; }
        public string PhoneNumberSecondary { get; set; }
        public string EmailAddressSecondary { get; set; }
        public string PersonNameSecondary { get; set; }
        public string GsmCountryCode { get; set; }
        public string PhoneCountryCode { get; set; }
        public string GsmCountryCodeSecondary { get; set; }
        public string PhoneCountryCodeSecondary { get; set; }
    }
}
