﻿namespace CountwareTraffic.Services.Reporting.Application
{
    public class ResourceDto
    {
        public string LanguageId { get; set; }
        public string ResouceKey { get; set; }
        public string Content { get; set; }
    }
}
