﻿using Convey.CQRS.Commands;

namespace CountwareTraffic.Services.Events.Application
{
    [Contract]
    public class CreateUnknownDevice : ICommand
    {
        public string Name { get; set; }
        public int HttpPort { get; set; }
        public int HttpsPort { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
        public string SerialNumber { get; set; }
    }
}
