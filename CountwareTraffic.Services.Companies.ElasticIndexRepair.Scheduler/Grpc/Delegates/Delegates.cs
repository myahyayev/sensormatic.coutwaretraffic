﻿using Grpc.Core;
using System;
using System.Threading;

namespace CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Delegates
{
    /// <summary>
    /// Tum unary metotlari try catch icerisinde call ede bilmek icin yazilmisdir. mahmud yhayayev
    /// </summary>
    /// <typeparam name="TRequest"></typeparam>
    /// <typeparam name="TResponse"></typeparam>
    /// <param name="request"></param>
    /// <param name="headers"></param>
    /// <param name="deadline"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public delegate AsyncUnaryCall<TResponse> UnaryCallgRPCServiceMethodHandler<TRequest, TResponse>(TRequest request, Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default)
        where TRequest : class
        where TResponse : class;
}
