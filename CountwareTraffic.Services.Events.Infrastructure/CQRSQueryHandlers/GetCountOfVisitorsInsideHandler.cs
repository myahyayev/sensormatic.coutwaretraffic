﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetCountOfVisitorsInsideHandler : IQueryHandler<GetCountOfVisitorsInside, CountOfVisitorsInsideDto>
    {
        private readonly IEventElasticSearchRepository _eventElasticSearchRepository;
        private readonly ILogger<GetCountOfVisitorsInsideHandler> _logger;
        public GetCountOfVisitorsInsideHandler(IEventElasticSearchRepository eventElasticSearchRepository, ILogger<GetCountOfVisitorsInsideHandler> logger)
        {
            _eventElasticSearchRepository = eventElasticSearchRepository;
            _logger = logger;
        }

        public async Task<CountOfVisitorsInsideDto> HandleAsync(GetCountOfVisitorsInside query)
            => await _eventElasticSearchRepository.GetCountOfVisitorsInside(query.NodeFilters, query.CurrentStartDate);
    }
}
