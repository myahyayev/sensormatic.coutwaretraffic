﻿using Convey.CQRS.Commands;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class ProcessOutboxScheduleHandler : ICommandHandler<ProcessOutboxSchedule>
    {
        private readonly IQueueService _queueService;
        private readonly ILogger<ProcessOutboxScheduleHandler> _logger;
        private readonly DeviceDbContext _context;
        public ProcessOutboxScheduleHandler(DeviceDbContext deviceDbContext,
            IQueueService queueService,
            ILogger<ProcessOutboxScheduleHandler> logger)
        {
            _queueService = queueService;
            _logger = logger;
            _context = deviceDbContext;
        }

        public async Task HandleAsync(ProcessOutboxSchedule command)
        {
            using (var connection = _context.Database.GetDbConnection())
            {
                const string sql = @";WITH tmpOutboxMessages 
                                             AS (  SELECT TOP 100 * 
			                                         FROM [device.app].[OutboxMessages]
			                                         WHERE [Status] IN (1,2)
			                                         ORDER BY [OccurredOn] ASC
                                                )
                                        UPDATE tmpOutboxMessages  
	                                        SET [Status] = 3
	                                        OUTPUT inserted.[Id],  
		                                           inserted.[Type],  
		                                           inserted.[Data]";

                var messages = (await connection.QueryAsync<OutboxMessageDto>(sql)).AsList();
                List<Guid> publishedMessagesIds = new();

                const string UpdateProcessedDateQuery = @"UPDATE [device.app].[OutboxMessages] SET [ProcessedDate] = @ProcessedDate, [Status] = @Status WHERE [Id] IN @Ids";

                if (messages.Count > 0)
                {
                    foreach (var message in messages)
                    {
                        try
                        {
                            Type type = Assemblies.QueueModelAssembly.GetType(message.Type);

                            var queueEvent = JsonConvert.DeserializeObject(message.Data, type) as IQueueEvent;

                            if (queueEvent != null)
                            {
                                _queueService.Publish(queueEvent);
                                publishedMessagesIds.Add(message.Id);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogCritical(ex, $"Devices ProcessOutboxSchedule Q.publish got error for {message.Id} record.");
                        }
                    }

                    await Policy.Handle<Exception>()
                        .CircuitBreakerAsync(3, TimeSpan.FromSeconds(10),
                        (exception, timeSpan) =>
                        {
                            _logger.LogCritical(exception, "Devices ProcessOutboxSchedule flag updating had Error!, will try again in 10 seconds.");
                        }, () =>
                        {
                            _logger.LogCritical($"Devices ProcessOutboxSchedule flag updating didn't work for {publishedMessagesIds.Serialize()} records.");
                        })
                        .ExecuteAsync(async () =>
                        {
                            if (!publishedMessagesIds.Any()) return;

                            await connection.ExecuteAsync(UpdateProcessedDateQuery, new { ProcessedDate = DateTime.UtcNow, Status = OperationStatus.Success, Ids = publishedMessagesIds });
                            _logger.LogInformation($"Devices ProcessOutboxSchedule flag updating worked for  {publishedMessagesIds.Serialize()} records.");
                        });
                }
            }
        }
    }
    internal static class Assemblies
    {
        public static readonly Assembly QueueModelAssembly = typeof(DeviceCreated).Assembly;
    }
}
