﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetEventsByTimePeriodHandler : IQueryHandler<GetEventsByTimePeriod, List<EventsByTimePeriodDto>>
    {
        private readonly IEventElasticSearchRepository _eventElasticSearchRepository;
        private readonly ILogger<GetEventsByTimePeriodHandler> _logger;
        public GetEventsByTimePeriodHandler(IEventElasticSearchRepository eventElasticSearchRepository, ILogger<GetEventsByTimePeriodHandler> logger)
        {
            _eventElasticSearchRepository = eventElasticSearchRepository;
            _logger = logger;
        }

        public async Task<List<EventsByTimePeriodDto>> HandleAsync(GetEventsByTimePeriod query)
            => await _eventElasticSearchRepository.GetEventsByTimePeriodAsync(query.StartDate, query.EndDate);
    }
}
