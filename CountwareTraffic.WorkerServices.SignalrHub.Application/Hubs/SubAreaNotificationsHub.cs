﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public interface ISubAreaNotificationsHub
    {
        Task JoinSubAreasProcess(string groupName);
        Task SubAreaDeletedFailedReceived(SubAreaDeletedFailed command);
        Task SubAreaDeletedSuccessfullyReceived(SubAreaDeletedSuccessfully command);
        Task SubAreaChangedFailedReceived(SubAreaChangedFailed command);
        Task SubAreaChangedSuccessfullyReceived(SubAreaChangedSuccessfully command);
        Task SubAreaCreatedFailedReceived(SubAreaCreatedFailed command);
        Task SubAreaCreatedSuccessfullyReceived(SubAreaCreatedSuccessfully command);
    }


    //[Authorize]
    public class SubAreaNotificationsHub : Hub<ISubAreaNotificationsHub>
    {
        public async Task JoinSubAreasProcess(string groupName)
            => await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

        public async Task SubAreaDeletedFailedReceived(SubAreaDeletedFailed command)
            => await Clients.Group(command.TenantId.ToString()).SubAreaDeletedFailedReceived(command);

        public async Task SubAreaDeletedSuccessfullyReceived(SubAreaDeletedSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).SubAreaDeletedSuccessfullyReceived(command);

        public async Task SubAreaChangedFailedReceived(SubAreaChangedFailed command)
            => await Clients.Group(command.TenantId.ToString()).SubAreaChangedFailedReceived(command);

        public async Task SubAreaChangedSuccessfullyReceived(SubAreaChangedSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).SubAreaChangedSuccessfullyReceived(command);

        public async Task SubAreaCreatedFailedReceived(SubAreaCreatedFailed command)
            => await Clients.Group(command.TenantId.ToString()).SubAreaCreatedFailedReceived(command);

        public async Task SubAreaCreatedSuccessfullyReceived(SubAreaCreatedSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).SubAreaCreatedSuccessfullyReceived(command);
    }
}
