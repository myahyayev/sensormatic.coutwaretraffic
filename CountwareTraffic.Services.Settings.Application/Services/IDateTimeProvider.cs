﻿using Sensormatic.Tool.Ioc;
using System;

namespace CountwareTraffic.Services.Settings.Application
{
    public interface IDateTimeProvider : ISingletonDependency
    {
        DateTime Now { get; }
    }
}
