﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreateGroupHandler : IQueryHandler<CreateGroup, IdentityCreateGroupResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public CreateGroupHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityCreateGroupResponse> HandleAsync(CreateGroup query)
        {
            return await _identityServerService.CreateGroupAsync(new IdentityCreateGroupRequest
            {
                Name = query.Name,
                Description = query.Description
            });
        }
    }
}
