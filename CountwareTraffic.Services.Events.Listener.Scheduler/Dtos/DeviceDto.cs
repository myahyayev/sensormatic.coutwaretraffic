﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class DeviceDto
    {
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string HttpPort { get; set; }
        public string HttpsPort { get; set; }
        public string SerialNumber { get; set; }
        public string DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string FirmWare { get; set; }
        public string BrandName { get; set; }
        public string ModelName  { get; set; }
        public List<EventDto> Events { get; set; }
        public DeviceDto() => Events = new List<EventDto>();
    }

   
}
