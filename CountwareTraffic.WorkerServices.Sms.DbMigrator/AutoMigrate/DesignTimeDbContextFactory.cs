﻿using CountwareTraffic.WorkerServices.Sms.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace CountwareTraffic.WorkerServices.Sms.DbMigrator
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<SmsDbContext>
    {
        public SmsDbContext CreateDbContext(string[] args)
        {
            var hostConfig = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();

            var builder = new DbContextOptionsBuilder<SmsDbContext>();
            var connectionString = hostConfig.GetConnectionString("SmsDbConnection");
            builder.UseSqlServer(connectionString/*, x => x.UseNetTopologySuite()*/, x => x.EnableRetryOnFailure());


            return new SmsDbContext(builder.Options);
        }
    }
}
