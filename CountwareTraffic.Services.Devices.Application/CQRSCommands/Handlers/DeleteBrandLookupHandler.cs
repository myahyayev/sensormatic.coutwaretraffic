﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Devices.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeleteBrandLookupHandler : ICommandHandler<DeleteBrandLookup>
    {
        private readonly IUnitOfWork _unitOfWork;
        public DeleteBrandLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task HandleAsync(DeleteBrandLookup command)
        {
            var brandRepository = _unitOfWork.GetRepository<IBrandLookupRepository>();

            var brand = await brandRepository.GetByIdAsync(command.BrandId);

            if (brand is null)
                throw new BrandLookupNotFoundException(command.BrandId);

            brandRepository.Remove(brand);

            await _unitOfWork.CommitAsync();
        }
    }
}
