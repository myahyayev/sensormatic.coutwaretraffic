﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public class DeviceStatusChangeSuccessfully : IQueueEvent
    {
        public Guid DeviceId { get; set; }
        public string Name { get; set; }
        public int DeviceStatusId { get; set; }
        public string DeviceStatusName { get; set; }
        public Guid RecordId { get; init; }
        public Guid TenantId { get; set; }
    }
}

