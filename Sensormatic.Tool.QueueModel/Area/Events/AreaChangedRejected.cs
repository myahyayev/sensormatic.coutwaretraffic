﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record AreaChangedRejected : MessageEnvelope, IQueueEvent
    {
        public AreaChangedRejected(Guid areaId, string name, string oldName,  Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            AreaId = areaId;
            Name = name;
            OldName = oldName;
        }
        public Guid AreaId { get; }
        public string Name { get; }
        public string OldName { get; }
        public Guid RecordId { get; init; }
    }
}
