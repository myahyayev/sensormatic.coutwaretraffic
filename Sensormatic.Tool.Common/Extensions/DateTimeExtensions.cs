﻿using System;

namespace Sensormatic.Tool.Common
{
    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static DateTime ConvertBySystemTimeZoneId(this DateTime dateTime, string windowsTimeZone, string linuxTimeZone, bool provideByLinux)
        {
            if (provideByLinux && !linuxTimeZone.IsNullOrWhiteSpace())
                return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateTime, TimeZoneInfo.Local.Id, linuxTimeZone);

            if (!provideByLinux && !windowsTimeZone.IsNullOrWhiteSpace())
                return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateTime, TimeZoneInfo.Local.Id, windowsTimeZone);

            return dateTime;
        }

        public static DateTimeOffset ConvertDateTimeOffSetBySystemTimeZoneId(this DateTime dateTime, string windowsTimeZone, string linuxTimeZone, bool provideByLinux)
        {
            DateTimeOffset original = new DateTimeOffset(dateTime);

            if (provideByLinux && !linuxTimeZone.IsNullOrWhiteSpace())
            {
                TimeZoneInfo cetInfo = TimeZoneInfo.FindSystemTimeZoneById(linuxTimeZone);
                DateTimeOffset cetTime = TimeZoneInfo.ConvertTime(original, cetInfo);
                return original
                    .Subtract(cetTime.Offset)
                    .ToOffset(cetTime.Offset);
            }

            if (!provideByLinux && !windowsTimeZone.IsNullOrWhiteSpace())
            {
                TimeZoneInfo cetInfo = TimeZoneInfo.FindSystemTimeZoneById(windowsTimeZone);
                DateTimeOffset cetTime = TimeZoneInfo.ConvertTime(original, cetInfo);
                return original
                    .Subtract(cetTime.Offset)
                    .ToOffset(cetTime.Offset);
            }

            return original;
        }

        /// <summary>
        /// checks whether two given periods overlap each other.
        /// </summary>
        /// <param name="BS">Base period start</param>
        /// <param name="BE">Base period end</param>
        /// <param name="TS">Test period start</param>
        /// <param name="TE">Test period end</param>
        /// <returns>
        /// 	<c>true</c> if the periods overlap; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasTimePeriodOverlap(DateTime BS, DateTime BE, DateTime TS, DateTime TE)
        {
            // More simple?
            // return !((TS < BS && TE < BS) || (TS > BE && TE > BE));


            return (
                // 1. Case:
                //
                //       TS-------TE
                //    BS------BE 
                //
                // TS is after BS but before BE
                (TS >= BS && TS < BE)
                || // or

                // 2. Case
                //
                //    TS-------TE
                //        BS---------BE
                //
                // TE is before BE but after BS
                (TE <= BE && TE > BS)
                || // or

                // 3. Case
                //
                //  TS----------TE
                //     BS----BE
                //
                // TS is before BS and TE is after BE
                (TS <= BS && TE >= BE)
            );


        }

    }
}