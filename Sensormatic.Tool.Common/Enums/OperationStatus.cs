﻿namespace Sensormatic.Tool.Common
{
    public enum OperationStatus : byte
    {
        Success = 0,
        Failed = 1,
        Pending = 2,
        Processing = 3
    }
}
