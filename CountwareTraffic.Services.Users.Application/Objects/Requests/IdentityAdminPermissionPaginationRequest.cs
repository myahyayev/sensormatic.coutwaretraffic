﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application.Objects.Requests
{
    public class IdentityAdminPermissionPaginationRequest
    {
        public PagingQuery Paging { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }

        public IdentityAdminPermissionPaginationRequest()
        {
            Paging = new PagingQuery();
            Filters = new List<GridFilter>();
            Sorts = new List<SortDescriptor>();
        }
    }
}
