﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantGroupPaginationHandler : IQueryHandler<GetTenantGroupPagination, IdentityTenantGroupPaginationResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetTenantGroupPaginationHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityTenantGroupPaginationResponse> HandleAsync(GetTenantGroupPagination query)
        {
            return await _identityServerService.GetTenantGroupPaginationAsync(new IdentityTenantGroupPaginationRequest
            {
                Paging = query.Paging,
                Filters = query.Filters,
                Sorts = query.Sorts
            });
        }
    }
}
