﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeletePermission : IQuery<IdentityDeletePermissionResponse>
    {
        public Guid Id { get; set; }
    }
}
