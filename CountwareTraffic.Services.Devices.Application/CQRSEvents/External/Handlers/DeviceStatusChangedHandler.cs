﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Devices.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeviceStatusChangedHandler : IEventHandler<DeviceStatusChanged>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public DeviceStatusChangedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(DeviceStatusChanged command)
        {
            var deviceRepository = _unitOfWork.GetRepository<IDeviceRepository>();

            var device = await deviceRepository.GetAsync(command.DeviceId, command.TenantId);

            DeviceStatus deviceStatus = DeviceStatus.From(command.DeviceStatus);

            if (device != null)
            {

                if (deviceStatus.Equals(DeviceStatus.Connected))
                {
                    device.WhenDeviceConnected();
                }
                else if (deviceStatus.Equals(DeviceStatus.DisConnected))
                {
                    device.WhenDeviceDisConnected();
                }
                else if (deviceStatus.Equals(DeviceStatus.Broken))
                {
                    device.WhenDeviceConnecttionBroken();
                }
                else
                    device.WhenDeviceConnecttionUnknown();

                _queueService.Publish(new DeviceStatusChangeSuccessfully
                {
                    DeviceId = device.Id,
                    TenantId = command.TenantId,
                    DeviceStatusId = deviceStatus.Id,
                    DeviceStatusName = deviceStatus.Name,
                    Name = device.Name,
                    RecordId = command.RecordId
                });

                await _unitOfWork.CommitAsync();
            }
        }
    }
}
