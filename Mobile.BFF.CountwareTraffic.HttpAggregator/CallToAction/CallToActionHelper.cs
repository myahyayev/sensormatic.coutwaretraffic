﻿using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public static class CallToActionHelper
    {
        public static List<CallToAction> Get(CallToActionType type)
        {
            var calltoaction = new List<CallToAction>();
            switch (type)
            {
                case CallToActionType.AbortCall:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Cancel" });
                    calltoaction.Add(new CallToAction { Name = "CallToCallCenter", Text = "Call" });
                    break;

                case CallToActionType.TryAgain:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Try Again" });
                    break;

                case CallToActionType.OkCall:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Ok" });
                    calltoaction.Add(new CallToAction { Name = "CallToCallCenter", Text = "Call" });
                    break;

                case CallToActionType.Ok:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Ok" });
                    break;

                case CallToActionType.GoToDevices:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Cancel" });
                    calltoaction.Add(new CallToAction { Name = "DeleteDevice", Text = "Go to devices" });
                    break;

                case CallToActionType.GoToCountries:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Cancel" });
                    calltoaction.Add(new CallToAction { Name = "DeletCountry", Text = "Go to countries" });
                    break;

                case CallToActionType.GoToRegions:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Cancel" });
                    calltoaction.Add(new CallToAction { Name = "DeletRegion", Text = "Go to regions" });
                    break;

                case CallToActionType.GoToAreas:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Cancel" });
                    calltoaction.Add(new CallToAction { Name = "DeletArea", Text = "Go to areas" });
                    break;

                case CallToActionType.GoToSubAreas:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Cancel" });
                    calltoaction.Add(new CallToAction { Name = "DeletSubArea", Text = "Go to subareas" });
                    break;

                case CallToActionType.ChangePassword:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Cancel" });
                    calltoaction.Add(new CallToAction { Name = "ChangePassword", Text = "Change Password" });
                    break;

                case CallToActionType.GoToBrandModel:
                    calltoaction.Add(new CallToAction { Name = "Close", Text = "Cancel" });
                    calltoaction.Add(new CallToAction { Name = "DeletModel", Text = "Go to brand models" });
                    break;

                default:
                    break;
            }

            return calltoaction;
        }
    }
}
