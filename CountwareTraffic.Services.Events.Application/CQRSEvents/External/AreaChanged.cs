﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class AreaChanged : Convey.CQRS.Events.IEvent
    {
        public Guid TenantId { get; set; }
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public string OldAreaName { get; set; }
        public Guid RecordId { get; init; }
        public TimeSpan? WorkingHoursStart { get; set; }
        public TimeSpan? OldWorkingHoursStart { get; set; }
        public TimeSpan? WorkingHoursEnd { get; set; }
        public TimeSpan? OldWorkingHoursEnd { get; set; }
        public string WorkingTimeZone { get; set; }
        public string OldWorkingTimeZone { get; set; }
        public Guid UserId { get; set; }
    }
}
