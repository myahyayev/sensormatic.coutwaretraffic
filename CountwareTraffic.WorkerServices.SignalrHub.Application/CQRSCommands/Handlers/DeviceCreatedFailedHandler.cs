﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class DeviceCreatedFailedHandler : ICommandHandler<DeviceCreatedFailed>
    {
        private readonly IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> _hubContext;
        public DeviceCreatedFailedHandler(IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> hubContext) 
            => _hubContext = hubContext;

        public async Task HandleAsync(DeviceCreatedFailed command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .DeviceCreatedFailedReceived(command);
    }
}
