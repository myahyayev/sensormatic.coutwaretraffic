﻿using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application.Objects.Requests
{
    public class IdentityGetContractsByParametersRequest
    {
        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("appType")]
        public int AppType { get; set; }

        [JsonPropertyName("contractType")]
        public string ContractType { get; set; }
    }
}
