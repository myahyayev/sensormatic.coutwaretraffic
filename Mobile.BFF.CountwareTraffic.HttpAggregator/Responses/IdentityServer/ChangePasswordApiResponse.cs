﻿using System.Text.Json.Serialization;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ChangePasswordApiResponse
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
