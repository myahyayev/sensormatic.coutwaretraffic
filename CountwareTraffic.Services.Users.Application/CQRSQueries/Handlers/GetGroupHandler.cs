﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetGroupHandler : IQueryHandler<GetGroup, IdentityGroupResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetGroupHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityGroupResponse> HandleAsync(GetGroup query)
        {
            return await _identityServerService.GetGroupAsync(query.Id);
        }
    }
}
