﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class CountryHierarchyBase
    {
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public Guid CompanyId { get; set; }
        public Guid TenantId { get; set; }
        public Guid RecordId { get; init; }

        public CountryHierarchyBase(Guid tenantId,  Guid companyId,  Guid countryId, string countryName)
        {
            CompanyId = companyId;
            RecordId = Guid.NewGuid();
            TenantId = tenantId;
            CountryId = countryId;
            CountryName = countryName;
        }
    }


    public class CountryHierarchyCreated : CountryHierarchyBase, IDomainEvent
    {
        public CountryHierarchyCreated(Guid tenantId, Guid companyId, Guid countryId, string countryName) 
            : base(tenantId, companyId, countryId, countryName)
        {
        }
    }

    public class CountryHierarchyDeleted : CountryHierarchyBase, IDomainEvent
    {
        public CountryHierarchyDeleted(Guid tenantId, Guid companyId, Guid countryId, string countryName)
            : base(tenantId, companyId, countryId, countryName)
        {
        }
    }
    public class CountryHierarchyUpdated : CountryHierarchyBase, IDomainEvent
    {
        public CountryHierarchyUpdated(Guid tenantId, Guid companyId, Guid countryId, string countryName) 
            : base(tenantId, companyId, countryId, countryName)
        {
        }
    }
}
