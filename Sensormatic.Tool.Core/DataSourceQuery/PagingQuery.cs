﻿namespace Sensormatic.Tool.Core
{
    public record PagingQuery
    {
        public int Page { get; set; }
        public int Limit { get; set; }
        public PagingQuery() { }
        public PagingQuery(int page, int limit)
        {
            Page = page;
            Limit = limit;
        }
    }
}

