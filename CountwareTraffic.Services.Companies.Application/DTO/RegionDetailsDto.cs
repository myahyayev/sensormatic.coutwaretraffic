﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class RegionDetailsDto : RegionDto
    {
        public Guid CountryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ManagerName { get; set; }
        public Guid? ManagerId { get; set; }
    }
}