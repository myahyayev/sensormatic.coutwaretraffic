﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetAreaHierarchy : IQuery<List<HierarchyDto>>
    {
        public Guid Id { get; set; }
    }

    public class GetRegionHierarchy : IQuery<List<HierarchyDto>>
    {
        public Guid Id { get; set; }
    }

    public class GetCountryHierarchy : IQuery<List<HierarchyDto>>
    {
        public Guid Id { get; set; }
    }

    public class GetCompanyHierarchy : IQuery<List<HierarchyDto>>
    {
        public Guid Id { get; set; }
    }

    public class GetSubAreaHierarchy : IQuery<List<HierarchyDto>>
    {
        public Guid Id { get; set; }
    }

    public class GetAllHierarchy : IQuery<string>
    {
    
    }
}
