﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesRegionDeletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.RegionDeleted>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesRegionDeletedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.RegionDeleted queuEvent)
        {
            var regionDeleted = _eventMapper.Map(queuEvent) as RegionDeleted;
            await _eventDispatcher.PublishAsync(regionDeleted);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
