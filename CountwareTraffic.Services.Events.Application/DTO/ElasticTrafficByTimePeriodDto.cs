﻿namespace CountwareTraffic.Services.Events.Application
{
    public class ElasticTrafficByTimePeriodDto
    {
        public string Category { get; set; }
        public int Value { get; set; }
    }
}
