﻿using Convey.CQRS.Queries;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantGroupPagination : IQuery<IdentityTenantGroupPaginationResponse>
    {
        public PagingQuery Paging { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
    }
}
