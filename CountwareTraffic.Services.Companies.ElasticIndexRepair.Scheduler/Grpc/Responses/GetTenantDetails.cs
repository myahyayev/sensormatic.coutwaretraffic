﻿using System;

namespace CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Responses
{
    public class GetTenantDetails
    {
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
