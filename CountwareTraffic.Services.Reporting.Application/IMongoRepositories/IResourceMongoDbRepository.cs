﻿using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Application
{
    public interface IResourceMongoDbRepository : ISingletonDependency
    {
        Task<ResourceDto> GetAsync(Guid languageId, string key);
        Task<List<ResourceDto>> GetByAssemblyQualifiedNameAsync(string assemblyQualifiedName);
    }
}
