﻿using CountwareTraffic.Services.Devices.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class BrandModelLookupRepository : Repository<BrandModelLookup>, IBrandModelLookupRepository
    {
        private readonly new DeviceDbContext _context;

        public BrandModelLookupRepository(DeviceDbContext context) : base(context) => _context = context;

        public async Task<IEnumerable<BrandModelLookup>> GetAllAsync(Guid brandId) => await _context.BrandModelLookups.Where(u => u.BrandId == brandId).OrderBy(u => u.Name).ToListAsync();

        public async Task<BrandModelLookup> GetByNameAsync(string name) => await base.GetQuery().SingleOrDefaultAsync(x => x.Name == name);

        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible
    }
}
