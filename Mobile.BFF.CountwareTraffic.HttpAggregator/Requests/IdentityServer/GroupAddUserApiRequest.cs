﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GroupAddUserApiRequest
    {
        [FromForm(Name = "userId")]
        public Guid UserId { get; set; }

        [FromForm(Name = "groupId")]
        public Guid GroupId { get; set; }

        [FromForm(Name = "startDate")]
        public DateTime StartDate { get; set; }

        [FromForm(Name = "endDate")]
        public DateTime EndDate { get; set; }
    }
}
