﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class TrafficController : BaseController
    {
        private readonly TrafficService _trafficService;
        private readonly UserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly CompanyService _companyService;
        private readonly CountryService _countryService;
        private readonly RegionService _regionService;
        private readonly AreaService _areaService;
        private readonly SubAreaService _subAreaService;
        public TrafficController(IServiceProvider provider, ILogger<TrafficController> logger, IHttpContextAccessor contextAccessor, TrafficService trafficService,
            UserService userService, CompanyService companyService, CountryService countryService, RegionService regionService, AreaService areaService,
            SubAreaService subAreaService)
            : base(provider, logger, contextAccessor)
        {
            _trafficService = trafficService;
            _userService = userService;
            _httpContextAccessor = contextAccessor;
            _companyService = companyService;
            _countryService = countryService;
            _regionService = regionService;
            _areaService = areaService;
            _subAreaService = subAreaService;
        }


        [HttpPost("widget-performing")]
        [Permissions(Permissions = new[] { Permissions.DashboardTrafficView })]
        public async Task<ApiResponse<GetWidgetPerformingApiResponse>> GetWidgetPerformingAsync(GetWidgetPerformingApiRequest request)
        {

            request.HierarchyTreeSelectFilters = await AreaControl(request.HierarchyTreeSelectFilters);
            return Response(await _trafficService.GetWidgetPerformingAsync(request), new ResultMessage("#S1127"));
        }


        [HttpPost("top-performing")]
        [Permissions(Permissions = new[] { Permissions.DashboardTrafficView })]
        public async Task<ApiResponse<IEnumerable<GetPerformingByTypeApiResponse>>> GetPerformingByTypeAsync(GetPerformingByTypeApiRequest request)
        {
            request.HierarchyTreeSelectFilters = await AreaControl(request.HierarchyTreeSelectFilters);
            return Response(await _trafficService.GetPerformingByTypeAsync(request), new ResultMessage("#S1128"));
        }


        [HttpPost("by-time-period")]
        [Permissions(Permissions = new[] { Permissions.DashboardTrafficView })]
        public async Task<ApiResponse<GetTrafficByTimePeriodApiResponse>> GetTrafficByTimePeriodAsync(GetTrafficByTimePeriodApiRequest request)
        {
            string language = _httpContextAccessor.GetLanguage();

            request.HierarchyTreeSelectFilters = await AreaControl(request.HierarchyTreeSelectFilters);
            return Response(await _trafficService.GetTrafficByTimePeriodAsync(request, language), new ResultMessage("#S1129"));
        }


        [HttpPost("count-of-visitors")]
        [Permissions(Permissions = new[] { Permissions.DashboardTrafficView })]
        public async Task<ApiResponse<GetCountOfVisitorsInsideApiResponse>> GetCountOfVisitorsInsideAsync(GetCountOfVisitorsInsideApiRequest request)
        {
            request.HierarchyTreeSelectFilters = await AreaControl(request.HierarchyTreeSelectFilters);
            return Response(await _trafficService.GetCountOfVisitorsInsideAsync(request), new ResultMessage("#S1130"));
        }


        [HttpPost("visitor-duration")]
        [Permissions(Permissions = new[] { Permissions.DashboardTrafficView })]
        public async Task<ApiResponse<GetVisitorDurationApiResponse>> GetVisitorDurationAsync(GetVisitorDurationApiRequest request)
        {
            request.HierarchyTreeSelectFilters = await AreaControl(request.HierarchyTreeSelectFilters);
            return Response(await _trafficService.GetVisitorDurationAsync(request), new ResultMessage("#S1131"));
        }

        private async Task<GetUserIdApiResponse> GetUserArea()
        {
            Guid userId = UserExtensions.GetUserId((ClaimsIdentity)User.Identity);
            var user = await _userService.GetUserIdAsync(userId);
            return user;
        }

        private async Task<List<HierarchyTreeSelectFilterRequest>> AreaControl(List<HierarchyTreeSelectFilterRequest> hierarchyTreeSelectFilters)
        {
            var user = await GetUserArea();
            if (!(user.UserType == (int)UserType.SuperAdmin) && !(user.UserType == (int)UserType.Admin))
            {
                if (hierarchyTreeSelectFilters == null || hierarchyTreeSelectFilters.Count == 0)
                {
                    hierarchyTreeSelectFilters = new List<HierarchyTreeSelectFilterRequest>();
                    user.Areas.ForEach(area =>
                        hierarchyTreeSelectFilters.Add(new HierarchyTreeSelectFilterRequest { Id = area.Id, HierarchyLevel = (HierarchyLevel)area.Level })
                    );
                }
            }
            return hierarchyTreeSelectFilters;
        }
    }
}
