﻿using CountwareTraffic.Services.Reporting.Application;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public interface IScheduleReport
    {
        void RefreshData();
        ValueTask<MemoryStream> ExportAsync();
        ValueTask<string> ExportBase64Async();
        Task LoadDataSourceFromDataBaseAsync();
        void SetResources();
        string ReportDateRange { get; }
        string ReportCreatedDate { get; }
        string TitleValue { get; }
        string SubTitleValue { get; }
    }
}