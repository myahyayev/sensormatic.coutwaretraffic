﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportFormatNameNotFoundException : DomainException
    {
        IEnumerable<ReportFormat> ReportFormats { get; }
        public string Name { get; }

        public ReportFormatNameNotFoundException(IEnumerable<ReportFormat> reportFormats, string name)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ReportFormat Name: {String.Join(",", reportFormats.Select(s => s.Name))}") }, 400, ResponseMessageType.Error)
        {
            ReportFormats = reportFormats;
            Name = name;
        }
    }
}
