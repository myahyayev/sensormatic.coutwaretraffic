﻿using Microsoft.AspNetCore.Mvc;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System.ComponentModel.DataAnnotations;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ChangePasswordApiRequest : SensormaticRequestValidate
    {
        [FromForm(Name = "currentPassword")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string CurrentPassword { get; set; }


        [FromForm(Name = "newPassword")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string NewPassword { get; set; }


        [FromForm(Name = "confirmPassword")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string ConfirmPassword { get; set; }

        public override void Validate()
        {
            if (CurrentPassword.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1236 Value cannot be null", nameof(CurrentPassword)));

            if (NewPassword.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1237 Value cannot be null", nameof(NewPassword)));

            if (ConfirmPassword.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1238 Value cannot be null", nameof(ConfirmPassword)));


            if (!CurrentPassword.IsNullOrWhiteSpace() && !NewPassword.IsNullOrWhiteSpace() && object.Equals(CurrentPassword, NewPassword))
            {
                ValidateResults.Add(new ErrorResult($"#E1239 New password cannot be the same as the current password"));

                return;
            }

            if ((!NewPassword.IsNullOrWhiteSpace() && !NewPassword.IsValidPassword()) || (!ConfirmPassword.IsNullOrWhiteSpace() && !ConfirmPassword.IsValidPassword()))
            {
                ValidateResults.Add(new ErrorResult($"#E1240 Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)"));

                return;
            }

            if (!NewPassword.IsNullOrWhiteSpace() && !ConfirmPassword.IsNullOrWhiteSpace() && !object.Equals(NewPassword, ConfirmPassword))
                ValidateResults.Add(new ErrorResult($"#E1241 The Password didn't match."));
        }
    }
}