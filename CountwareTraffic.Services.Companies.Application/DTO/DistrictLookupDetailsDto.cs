﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class DistrictLookupDetailsDto : DistrictLookupDto
    {
        public string Name { get; set; }
        public Guid CityId { get; set; }
    }
}
