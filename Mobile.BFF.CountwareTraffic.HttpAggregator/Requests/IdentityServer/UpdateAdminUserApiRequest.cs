﻿using System.Collections.Generic;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer
{
    public class UpdateAdminUserApiRequest
    {
        public UpdateAdminUserApiRequest()
        {
            Roles = new();
            TenantIds = new();
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public bool Status { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public int Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public int UserType { get; set; }
        public string Address { get; set; }
        public bool AllowSms { get; set; }
        public bool AllowEmail { get; set; }
        public string ProfilePic { get; set; }
        public List<string> Roles { get; set; }
        public List<string> TenantIds { get; set; }
    }

    public class UpdateAdminUserArea
    {
        public Guid Id { get; set; }
        public string Level { get; set; }
    }
}
