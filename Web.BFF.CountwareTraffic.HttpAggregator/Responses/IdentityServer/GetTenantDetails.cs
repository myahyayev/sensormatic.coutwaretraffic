﻿using System;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class GetTenantDetails
    {
        public Guid TenantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
