﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Exceptions
{
    public class ClientGrpcException : BaseException
    {
        public ClientGrpcException(ICollection<ErrorResult> errorResults, int statusCode, ResponseMessageType responseMessageType)
            : base(errorResults, statusCode, responseMessageType) { }
    }
}
