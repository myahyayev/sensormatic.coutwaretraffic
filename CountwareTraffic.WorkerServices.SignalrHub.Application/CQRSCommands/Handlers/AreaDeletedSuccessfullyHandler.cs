﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class AreaDeletedSuccessfullyHandler : ICommandHandler<AreaDeletedSuccessfully>
    {
        private readonly IHubContext<AreaNotificationsHub, IAreaNotificationsHub> _hubContext;
        public AreaDeletedSuccessfullyHandler(IHubContext<AreaNotificationsHub, IAreaNotificationsHub> hubContext)
            => _hubContext = hubContext;

        public async Task HandleAsync(AreaDeletedSuccessfully command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .AreaDeletedSuccessfullyReceived(command);
    }
}
