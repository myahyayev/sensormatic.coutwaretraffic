﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public record AreaChangedRejected : Convey.CQRS.Events.IEvent
    {
        public Guid AreaId { get; set; }
        public string Name { get; set; }
        public string OldName { get; set; }
        public Guid RecordId { get; set; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
    }
}
