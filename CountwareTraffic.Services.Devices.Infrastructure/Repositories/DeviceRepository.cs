﻿using CountwareTraffic.Services.Devices.Core;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure.Repositories
{
    public class DeviceRepository : Repository<Device>, IDeviceRepository
    {
        private readonly new DeviceDbContext _context;

        public DeviceRepository(DeviceDbContext context) : base(context) => _context = context;

        #region dispose
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion dispose

        public async Task<Device> GetAsync(Guid id, Guid tenantId) => await base.GetQuery()
                                    .Include(x => x.ConnectionInfo)
                                    .Include(x => x.DeviceStatus)
                                    .Include(x => x.DeviceType)
                                    .SingleOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);

        public async Task<Device> GetAsync(string name) => await base.GetQuery().SingleOrDefaultAsync(x => x.Name == name);

        public async Task<Device> GetDeletedAsync(Guid id, Guid tenantId) => await _context.Devices
                                    .Where(u => u.AuditIsDeleted)
                                    .Include(x => x.ConnectionInfo)
                                    .Include(x => x.DeviceStatus)
                                    .Include(x => x.DeviceType)
                                    .SingleOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);

        public async Task<bool> ExistsAsync(string name, Guid tenantId) => await base.GetQuery()
                                    .AnyAsync(u => u.Name == name && u.TenantId == tenantId);
        public async Task<Device> GetByIPAddressAndMacAsync(string ipAddress, string macAddress)
        {

            return await base.GetQuery()
                              .Where(u => u.IsActive && u.ConnectionInfo.IpAddress == ipAddress && u.ConnectionInfo.MacAddress == macAddress )   
                              .SingleOrDefaultAsync();
        }
        public async Task<Device> GetByMacAsync(string macAddress)
        {

            return await base.GetQuery()
                             .Where(u => u.IsActive && u.ConnectionInfo.MacAddress == macAddress)
                             .SingleOrDefaultAsync();
        }
        public async Task<QueryablePagingValue<Device>> GetAllAsync(Guid? subAreaId, Guid tenantId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts)
        {
            var query = base.GetQuery();

            query = query.ApplyFilter(filters.Where(u => u.Field.ToLower() != "isactive").ToList());

            foreach (var filter in filters)
            {
                if (filter.Field.ToLower() == "devicestatusid")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.DeviceStatus.Id == Int32.Parse(filter.Value)),
                        FilterEnum.Neq => query.Where(u => u.DeviceStatus.Id != Int32.Parse(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }

                if (filter.Field.ToLower() == "macaddress")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.ConnectionInfo.MacAddress == filter.Value),
                        FilterEnum.Neq => query.Where(u => u.ConnectionInfo.MacAddress != filter.Value),
                        FilterEnum.StartsWith => query.Where(u => u.ConnectionInfo.MacAddress.StartsWith(filter.Value)),
                        FilterEnum.Contains => query.Where(u => u.ConnectionInfo.MacAddress.Contains(filter.Value)),
                        FilterEnum.EndsWith => query.Where(u => u.ConnectionInfo.MacAddress.EndsWith(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }

                if (filter.Field.ToLower() == "ipaddress")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.ConnectionInfo.IpAddress == filter.Value),
                        FilterEnum.Neq => query.Where(u => u.ConnectionInfo.IpAddress != filter.Value),
                        FilterEnum.StartsWith => query.Where(u => u.ConnectionInfo.IpAddress.StartsWith(filter.Value)),
                        FilterEnum.Contains => query.Where(u => u.ConnectionInfo.IpAddress.Contains(filter.Value)),
                        FilterEnum.EndsWith => query.Where(u => u.ConnectionInfo.IpAddress.EndsWith(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }


                if (filter.Field.ToLower() == "isactive")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.IsActive == (filter.Value == "0") ? false: true),
                        FilterEnum.Neq => query.Where(u => u.IsActive != (filter.Value == "0") ? false : true),
                    };
                }
            }

            query = query.ApplyOrderBy(sorts);

            foreach (var sort in sorts)
            {
                if (sort.Field.ToLower() == "ipaddress")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.ConnectionInfo.IpAddress),
                        Direction.Desc => query.OrderByDescending(u => u.ConnectionInfo.IpAddress),
                        _ => throw new NotImplementedException(),
                    };
                }
                if (sort.Field.ToLower() == "macaddress")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.ConnectionInfo.MacAddress),
                        Direction.Desc => query.OrderByDescending(u => u.ConnectionInfo.MacAddress),
                        _ => throw new NotImplementedException(),
                    };
                }

                if (sort.Field.ToLower() == "devicestatusid")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.DeviceStatus.Id),
                        Direction.Desc => query.OrderByDescending(u => u.DeviceStatus.Id),
                        _ => throw new NotImplementedException(),
                    };
                }
            }

            int total = 0;

            if (subAreaId != null && subAreaId != Guid.Empty)
                total = await query.CountAsync(x => x.SubAreaId == subAreaId && x.TenantId == tenantId);

            else
                total = await query.CountAsync(x => x.TenantId == tenantId);


            if (total > 0)
            {
                query = query.Where(u => u.TenantId == tenantId);

                if (subAreaId != null && subAreaId != Guid.Empty)
                    query = query.Where(u => u.SubAreaId == subAreaId);

                query = query.Include(x => x.ConnectionInfo)
                              .Include(x => x.DeviceStatus)
                              .Include(x => x.DeviceType)
                              .Skip((paging.Page - 1) * paging.Limit)
                              .Take(paging.Limit);


                var result = await query.ToListAsync();

                return new QueryablePagingValue<Device>(result, total);
            }

            return null;
        }

        public async Task<QueryablePagingValue<DeviceFilteredData>> GetDevicesTreeSelectAsync(List<NodeFilter> nodeFilters, Guid tenantId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts)
        {
            string sqlSelectBegin = "SELECT * FROM ";
            string sqlSelectEnd = " ) AS FilteredQuery ";
            string sqlSortQuery = " ORDER BY FilteredQuery.Name ASC ";
            string sqlPrefix = " ) ";
            string sqlWhereClaue = "  ";
            string sqlFilteredQuery = @"(
                                            SELECT * FROM [devices].[Devices]  AS Devices 
	                                            WHERE devices.AuditIsDeleted = 0 AND
	                                                  devices.TenantId = @TenantId AND
	                                                  devices.SubAreaId IN (SELECT SubAreas.Id FROM [devices].[SubAreas] AS SubAreas 
	                                                                            WHERE SubAreas.AuditIsDeleted = 0 AND
                                                                                      SubAreas.TenantId = @TenantId
                                                                                   ";
            #region Node Filters
            if (nodeFilters != null && nodeFilters.Any()) sqlPrefix = " AND ( ";
            for (int i = 0; i < nodeFilters.Count; i++)
            {
                if (i != nodeFilters.Count -1)
                {
                    sqlPrefix += nodeFilters[i].HierarchyLevel switch
                    {
                        HierarchyLevel.Company => $"SubAreas.CompanyId = '{nodeFilters[i].Id}'  OR ",
                        HierarchyLevel.Country => $"SubAreas.CountryId = '{nodeFilters[i].Id}'  OR ",
                        HierarchyLevel.Region => $"SubAreas.RegionId = '{nodeFilters[i].Id}' OR ",
                        HierarchyLevel.Area => $"SubAreas.AreaId = '{nodeFilters[i].Id}'  OR ",
                        HierarchyLevel.SubArea => $"SubAreas.Id = '{nodeFilters[i].Id}'  OR ",
                        HierarchyLevel.Tenant => "",
                        HierarchyLevel.Device => "",
                        _ => "",
                    };
                }
                else
                {
                    sqlPrefix += nodeFilters[i].HierarchyLevel switch
                    {
                        HierarchyLevel.Company => $"SubAreas.CompanyId = '{nodeFilters[i].Id}' ) ) ",
                        HierarchyLevel.Country => $"SubAreas.CountryId = '{nodeFilters[i].Id}' ) ) ",
                        HierarchyLevel.Region => $"SubAreas.RegionId = '{nodeFilters[i].Id}' ) ) ",
                        HierarchyLevel.Area => $"SubAreas.AreaId = '{nodeFilters[i].Id}'  ) )  ",
                        HierarchyLevel.SubArea => $"SubAreas.Id = '{nodeFilters[i].Id}'  ) )  ",
                        HierarchyLevel.Tenant => "",
                        HierarchyLevel.Device => "",
                        _ => "",
                    };
                }
            }
            #endregion Node Filters


            #region Grid Filters
            if (filters != null && filters.Any()) sqlWhereClaue = " WHERE 1 = 1 ";
            foreach (var filter in filters)
            {
                if (filter.Field.ToLower() == "devicestatusid")
                {
                    sqlWhereClaue += filter.Operator switch
                    {
                        FilterEnum.Eq => $" AND FilteredQuery.DeviceStatusId = {Int32.Parse(filter.Value)}",
                        FilterEnum.Neq => $" AND FilteredQuery.DeviceStatusId != {Int32.Parse(filter.Value)}",
                        _ => throw new NotImplementedException(),
                    };
                }

                if (filter.Field.ToLower() == "macaddress")
                {
                    sqlWhereClaue += filter.Operator switch
                    {
                        FilterEnum.Eq => $" AND FilteredQuery.ConnectionInfo_MacAddress = '{filter.Value}' ",
                        FilterEnum.Neq => $" AND FilteredQuery.ConnectionInfo_MacAddress != '{filter.Value}' ",
                        FilterEnum.StartsWith => $" AND FilteredQuery.ConnectionInfo_MacAddress LIKE  '{filter.Value}%' ",
                        FilterEnum.Contains => $" AND FilteredQuery.ConnectionInfo_MacAddress LIKE  '%{filter.Value}%' ",
                        FilterEnum.EndsWith => $" AND FilteredQuery.ConnectionInfo_MacAddress LIKE  '%{filter.Value}' "
                    };
                }

                if (filter.Field.ToLower() == "ipaddress")
                {
                    sqlWhereClaue += filter.Operator switch
                    {
                        FilterEnum.Eq => $" AND FilteredQuery.ConnectionInfo_IpAddress = '{filter.Value}' ",
                        FilterEnum.Neq => $" AND FilteredQuery.ConnectionInfo_IpAddress != '{filter.Value}' ",
                        FilterEnum.StartsWith => $" AND FilteredQuery.ConnectionInfo_IpAddress LIKE  '{filter.Value}%' ",
                        FilterEnum.Contains => $" AND FilteredQuery.ConnectionInfo_IpAddress LIKE  '%{filter.Value}%' ",
                        FilterEnum.EndsWith => $" AND FilteredQuery.ConnectionInfo_IpAddress LIKE  '%{filter.Value}' "
                    };
                }

                if (filter.Field.ToLower() == "name")
                {
                    sqlWhereClaue += filter.Operator switch
                    {
                        FilterEnum.Eq => $" AND FilteredQuery.Name = '{filter.Value}' ",
                        FilterEnum.Neq => $" AND FilteredQuery.Name != '{filter.Value}' ",
                        FilterEnum.StartsWith => $" AND FilteredQuery.Name LIKE  '{filter.Value}%' ",
                        FilterEnum.Contains => $" AND FilteredQuery.Name LIKE  '%{filter.Value}%' ",
                        FilterEnum.EndsWith => $" AND FilteredQuery.Name LIKE  '%{filter.Value}' "
                    };
                }

                if (filter.Field.ToLower() == "modelname")
                {
                    sqlWhereClaue += filter.Operator switch
                    {
                        FilterEnum.Eq => $" AND FilteredQuery.ModelName = '{filter.Value}' ",
                        FilterEnum.Neq => $" AND FilteredQuery.ModelName != '{filter.Value}' ",
                        FilterEnum.StartsWith => $" AND FilteredQuery.ModelName LIKE  '{filter.Value}%' ",
                        FilterEnum.Contains => $" AND FilteredQuery.ModelName LIKE  '%{filter.Value}%' ",
                        FilterEnum.EndsWith => $" AND FilteredQuery.ModelName LIKE  '%{filter.Value}' "
                    };
                }

                if (filter.Field.ToLower() == "firmware")
                {
                    sqlWhereClaue += filter.Operator switch
                    {
                        FilterEnum.Eq => $" AND FilteredQuery.Firmware = '{filter.Value}' ",
                        FilterEnum.Neq => $" AND FilteredQuery.Firmware != '{filter.Value}' ",
                        FilterEnum.StartsWith => $" AND FilteredQuery.Firmware LIKE  '{filter.Value}%' ",
                        FilterEnum.Contains => $" AND FilteredQuery.Firmware LIKE  '%{filter.Value}%' ",
                        FilterEnum.EndsWith => $" AND FilteredQuery.Firmware LIKE  '%{filter.Value}' "
                    };
                }

                if (filter.Field.ToLower() == "brandname")
                {
                    sqlWhereClaue += filter.Operator switch
                    {
                        FilterEnum.Eq => $" AND FilteredQuery.BrandName = '{filter.Value}' ",
                        FilterEnum.Neq => $" AND FilteredQuery.BrandName != '{filter.Value}' ",
                        FilterEnum.StartsWith => $" AND FilteredQuery.BrandName LIKE  '{filter.Value}%' ",
                        FilterEnum.Contains => $" AND FilteredQuery.BrandName LIKE  '%{filter.Value}%' ",
                        FilterEnum.EndsWith => $" AND FilteredQuery.BrandName LIKE  '%{filter.Value}' "
                    };
                }

                if (filter.Field.ToLower() == "isactive")
                {
                    sqlWhereClaue += filter.Operator switch
                    {
                        FilterEnum.Eq => $" AND FilteredQuery.IsActive = {Int32.Parse(filter.Value)}",
                        FilterEnum.Neq => $" AND FilteredQuery.IsActive != {Int32.Parse(filter.Value)}",
                        _ => throw new NotImplementedException(),
                    };
                }
            }
            #endregion Grid Filters


            #region Grid Sorts
            if (sorts != null && sorts.Any())
            {
                sqlSortQuery = "  ";
                var sortOrderFiled = sorts.First();

                if (sortOrderFiled.Field.ToLower() == "ipaddress")
                {
                    sqlSortQuery = sortOrderFiled.Direction switch
                    {
                        Direction.Asc => " ORDER BY FilteredQuery.ConnectionInfo_IpAddress ASC ",
                        Direction.Desc => " ORDER BY FilteredQuery.ConnectionInfo_IpAddress DESC "
                    };
                }
                if (sortOrderFiled.Field.ToLower() == "macaddress")
                {
                    sqlSortQuery = sortOrderFiled.Direction switch
                    {
                        Direction.Asc => " ORDER BY FilteredQuery.ConnectionInfo_MacAddress ASC ",
                        Direction.Desc => " ORDER BY FilteredQuery.ConnectionInfo_MacAddress DESC "
                    };
                }

                if (sortOrderFiled.Field.ToLower() == "devicestatusid")
                {
                    sqlSortQuery = sortOrderFiled.Direction switch
                    {
                        Direction.Asc => " ORDER BY FilteredQuery.DeviceStatusId ASC ",
                        Direction.Desc => " ORDER BY FilteredQuery.DeviceStatusId DESC "
                    };
                }

                if (sortOrderFiled.Field.ToLower() == "name")
                {
                    sqlSortQuery = sortOrderFiled.Direction switch
                    {
                        Direction.Asc => " ORDER BY FilteredQuery.Name ASC ",
                        Direction.Desc => " ORDER BY FilteredQuery.Name DESC "
                    };
                }

                if (sortOrderFiled.Field.ToLower() == "modelname")
                {
                    sqlSortQuery = sortOrderFiled.Direction switch
                    {
                        Direction.Asc => " ORDER BY FilteredQuery.ModelName ASC ",
                        Direction.Desc => " ORDER BY FilteredQuery.ModelName DESC "
                    };
                }

                if (sortOrderFiled.Field.ToLower() == "brandname")
                {
                    sqlSortQuery = sortOrderFiled.Direction switch
                    {
                        Direction.Asc => " ORDER BY FilteredQuery.BrandName ASC ",
                        Direction.Desc => " ORDER BY FilteredQuery.BrandName DESC "
                    };
                }

                if (sortOrderFiled.Field.ToLower() == "firmware")
                {
                    sqlSortQuery = sortOrderFiled.Direction switch
                    {
                        Direction.Asc => " ORDER BY FilteredQuery.Firmware ASC ",
                        Direction.Desc => " ORDER BY FilteredQuery.Firmware DESC "
                    };
                }


                if (sortOrderFiled.Field.ToLower() == "isactive")
                {
                    sqlSortQuery = sortOrderFiled.Direction switch
                    {
                        Direction.Asc => " ORDER BY FilteredQuery.IsActive ASC ",
                        Direction.Desc => " ORDER BY FilteredQuery.IsActive DESC "
                    };
                }
            }
            #endregion Grid Sorts


            #region Grid Paging
            string sqlOffsetQuery = " OFFSET @_pageSize *(@_pageNumber - 1) ROWS FETCH NEXT @_pageSize ROWS ONLY ";
            #endregion Grid Paging



            var result = (await _context.Database.GetDbConnection().QueryAsync<DeviceFilteredData>(sqlSelectBegin + sqlFilteredQuery + sqlPrefix + sqlSelectEnd + sqlWhereClaue + sqlSortQuery + sqlOffsetQuery, new
            {
                @TenantId = tenantId,
                @_pageSize = paging.Limit,
                @_pageNumber = paging.Page
            })).ToList();


            var total = await _context.Database.GetDbConnection().ExecuteScalarAsync<int>("SELECT COUNT(*) FROM " + sqlFilteredQuery + sqlPrefix + sqlSelectEnd + sqlWhereClaue, new 
            {
                @TenantId = tenantId 
            });

            return new QueryablePagingValue<DeviceFilteredData>(result, total);
        }

        public async Task<DeviceHierarchyCte> GetDeviceHierarchyAsync(Guid id, Guid tenantId)
        {
            var sql = @"SELECT SubAreas.CompanyId AS CompanyId, 
                               SubAreas.CompanyName AS CompanyName,
                               SubAreas.CountryId AS CountryId, 
	                           SubAreas.CountryName AS CountryName, 
	                           SubAreas.RegionId AS RegionId, 
	                           SubAreas.RegionName AS RegionName, 
	                           SubAreas.AreaId AS AreaId, 
	                           SubAreas.AreaName  AS AreaName, 
	                           SubAreas.Id AS SubAreaId, 
	                           SubAreas.Name AS SubAreaName
	                        FROM [devices].[Devices] AS Devices
	                        INNER JOIN [devices].[SubAreas] AS SubAreas ON Devices.SubAreaId = SubAreas.Id
		                        WHERE Devices.Id = @Id
		                          AND Devices.TenantId = @tenantId 
	                              AND Devices.AuditIsDeleted = 0";

            var connection = _context.Database.GetDbConnection();

            var data = await connection.QueryFirstOrDefaultAsync<DeviceHierarchyCte>(sql, new
            {
                @TenantId = tenantId,
                @Id = id
            });

            return data;
        }

        public async Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName)
        {
            var sql = @"SELECT 
		                SubAreas.CompanyId AS CopmanyId, 
		                SubAreas.CompanyName AS CompanyName, 
		                SubAreas.CountryId  AS CountryId, 
		                SubAreas.CountryName AS CountryName,
		                SubAreas.RegionId AS RegionId, 
		                SubAreas.RegionName AS RegionName,
		                SubAreas.AreaId  AS AreaId,
		                SubAreas.AreaName AS AreaName,
		                SubAreas.Id  AS SubAreaId,
		                SubAreas.Name AS SubAreaName,
		                Devices.Id AS DeviceId,
		                Devices.Name AS DeviceName
                FROM [devices].[Devices] AS Devices
                INNER JOIN [devices].[SubAreas]  AS SubAreas ON Devices.SubAreaId = SubAreas.Id
                WHERE Devices.AuditIsDeleted = 0 AND Devices.TenantId = @TenantId AND Devices.Id = @Id";

            var connection = _context.Database.GetDbConnection();

            var data = await connection.QueryFirstOrDefaultAsync<HierarchyCteData>(sql, new
            {
                @TenantId = tenantId,
                @Id = id
            });

            var response = new List<Hierarchy>();

            response.Add(new Hierarchy() { Id = tenantId, Name = tenantName, HierarchyLevel = HierarchyLevel.Tenant });
            response.Add(new Hierarchy() { Id = data.CompanyId, Name = data.CompanyName, HierarchyLevel = HierarchyLevel.Company });
            response.Add(new Hierarchy() { Id = data.CountryId, Name = data.CountryName, HierarchyLevel = HierarchyLevel.Country });
            response.Add(new Hierarchy() { Id = data.RegionId, Name = data.RegionName, HierarchyLevel = HierarchyLevel.Region });
            response.Add(new Hierarchy() { Id = data.AreaId, Name = data.AreaName, HierarchyLevel = HierarchyLevel.Area });
            response.Add(new Hierarchy() { Id = data.SubAreaId, Name = data.SubAreaName, HierarchyLevel = HierarchyLevel.SubArea });
            response.Add(new Hierarchy() { Id = data.DeviceId, Name = data.DeviceName, HierarchyLevel = HierarchyLevel.SubArea });
            return response;
        }
    }
}
