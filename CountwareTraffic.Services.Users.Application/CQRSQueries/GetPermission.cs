﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetPermission : IQuery<IdentityPermissionResponse>
    {
        public Guid Id { get; set; }
        public string Language { get; set; }
    }
}
