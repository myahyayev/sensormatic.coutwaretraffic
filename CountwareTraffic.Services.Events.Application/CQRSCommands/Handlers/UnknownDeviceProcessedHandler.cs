﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class UnknownDeviceProcessedHandler : ICommandHandler<UnknownDeviceProcessed>
    {
        private readonly IUnitOfWork _unitOfWork;
        public UnknownDeviceProcessedHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task HandleAsync(UnknownDeviceProcessed command)
        {
            var unknownDeviceRepository = _unitOfWork.GetRepository<IUnknownDeviceRepository>();

            var unknownDevice = await unknownDeviceRepository.GetByIdAsync(command.Id);

            unknownDevice.WhenProcessed();

            await _unitOfWork.CommitAsync();
        }
    }
}
