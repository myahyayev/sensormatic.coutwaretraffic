﻿namespace CountwareTraffic.Services.Events.Application
{
    public class CountOfVisitorsInsideDto
    {
        public int CountOfVisitorsInside { get; set; }
    }
}
