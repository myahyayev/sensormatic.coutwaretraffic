﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class GetSubAreaRequest : SensormaticRequestValidate
    {
        internal Guid _SubAreaId
        {
            get
            {
                if (Guid.TryParse(subAreaId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.subAreaId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_SubAreaId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1326 SubAreaId cannot be null", nameof(_SubAreaId)));
        }
    }
}
