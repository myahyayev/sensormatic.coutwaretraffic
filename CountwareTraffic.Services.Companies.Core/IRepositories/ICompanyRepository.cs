﻿using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Core
{
    public interface ICompanyRepository : IRepository<Company>, IScopedDependency
    {
        Task<Company> GetAsync(Guid id);
        Task<Company> GetAsync(string name);
        Task<bool> ExistsAsync(string name);
        Task<QueryablePagingValue<Company>> GetAllAsync(PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts);
        Task<bool> ExistsAsync(Guid id);
        Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName);
        Task<TenantHierarchyEntityDto> GetTenantHierarchyAsync(Guid tenantId, string tenantName);
        Task<List<KeyValue>> GetAllKeyValueAsync();
    }
}