﻿using CountwareTraffic.Services.Companies.Grpc;
using Web.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class DistrictService : IScopedSelfDependency
    {
        private readonly District.DistrictClient _districtClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;
        public DistrictService(District.DistrictClient districtClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _districtClient = districtClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetDistrictDetails> GetDistrictByIdAsync(Guid districtId)
        {
            GetDistrictRequest grpcRequest = new() { DistrictId = districtId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_districtClient.GetDistrictByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetDistrictDetails
            {
                Id = new Guid(grpcResponse.DistrictDetail.Id),
                CityId = new Guid(grpcResponse.DistrictDetail.CityId),
                Name = grpcResponse.DistrictDetail.Name,
                AuditCreateBy = new Guid(grpcResponse.DistrictDetail.Audit.AuditCreateBy),
                AuditCreateDate = grpcResponse.DistrictDetail.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(grpcResponse.DistrictDetail.Audit.AuditModifiedBy),
                AuditModifiedDate = grpcResponse.DistrictDetail.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime
            };
        }

        public async Task<ApiPagedResponse<GetDistrictDetails>> GetDistrictsAsync(Guid cityId, DataSourceApiRequest paging)
        {
            GetDistrictsRequest grpcRequest = new()
            {
                CityId = cityId.ToString(),
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Companies.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Companies.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_districtClient.GetDistrictsAsync, grpcRequest, hasClientSideLog: false);

            var districts = grpcResponse.DistrictDetails.Select(district => new GetDistrictDetails
            {
                Id = new Guid(district.Id),
                Name = district.Name,
                CityId = new Guid(district.CityId),
                AuditCreateBy = new Guid(district.Audit.AuditCreateBy),
                AuditCreateDate = district.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(district.Audit.AuditModifiedBy),
                AuditModifiedDate = district.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
            });

            return new ApiPagedResponse<GetDistrictDetails>(districts, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task AddDistrictAsync(Guid cityId, AddDistrictApiRequest request)
        {
            CreateDistrictRequest grpcRequest = new()
            {
                CityId = cityId.ToString(),
                Name = request.Name
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_districtClient.AddDistrictAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task ChangeDistrictAsync(Guid districtId, ChangeDistrictApiRequest request)
        {
            UpdateDistrictRequest grpcRequest = new()
            {
                DistrictId = districtId.ToString(),
                Name = request.Name
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_districtClient.ChangeDistrictAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task DeleteDistrictAsync(Guid districtId)
        {
            DeleteDistrictRequest grpcRequest = new() { DistrictId = districtId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_districtClient.DeleteDistrictAsync, grpcRequest, hasClientSideLog: false);
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
