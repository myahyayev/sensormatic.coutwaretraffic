﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class DistrictLookup : Entity<Guid>
    {
        private string _name;
        private Guid _cityId;

        public string Name => _name;
        public Guid CityId => _cityId;

        public DistrictLookup()
        {
            _id = SequentialGuid.Current.Next(null);
        }
    }
}
