﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRSubAreaDeletedSuccessfullyConsumer : IConsumer<Sensormatic.Tool.QueueModel.SubAreaDeletedSuccessfully>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRSubAreaDeletedSuccessfullyConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.SubAreaDeletedSuccessfully queueEvent)
        {
            await _commandDispatcher.SendAsync(new SubAreaDeletedSuccessfully
            {
                SubAreaId = queueEvent.SubAreaId,
                IsDeleted = queueEvent.IsDeleted,
                UserId = queueEvent.UserId,
                UserName = queueEvent.UserName,
                TenantId = queueEvent.TenantId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
