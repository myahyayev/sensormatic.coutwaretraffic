﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeleteDeviceHandler : ICommandHandler<DeleteDevice>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public DeleteDeviceHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(DeleteDevice command)
        {
            var deviceRepository = _unitOfWork.GetRepository<IDeviceRepository>();

            var device = await deviceRepository.GetAsync(command.DeviceId, _currentTenant.Id);

            if (device is null) throw new DeviceNotFoundException(command.DeviceId);

            deviceRepository.Remove(device);

            device.WhenDeleted(command.DeviceId);

            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            var subArea = await subAreaRepository.GetAsync(device.SubAreaId);

            device.WhenHierarchyDeleted(_currentTenant.Id, subArea.CompanyId, subArea.CountryId, subArea.RegionId, subArea.AreaId, subArea.Id, device.Id, device.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
