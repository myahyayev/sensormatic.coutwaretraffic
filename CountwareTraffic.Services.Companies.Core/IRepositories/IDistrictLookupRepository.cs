﻿using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Core
{
    public interface IDistrictLookupRepository : IRepository<DistrictLookup>, IScopedDependency
    {
        Task<IEnumerable<DistrictLookup>> GetAllAsync(Guid cityId);
        Task<DistrictHierarchyData> GetDistrictHierarchy(Guid districtId);
    }
}
