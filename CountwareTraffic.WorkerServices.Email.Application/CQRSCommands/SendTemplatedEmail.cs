﻿using Convey.CQRS.Commands;
using Sensormatic.Tool.Common;
using System;

namespace CountwareTraffic.WorkerServices.Email.Application
{
    [Contract]
    public class SendTemplatedEmail : ICommand
    {
        public IEmailTemplate Template { get; set; }
        public Guid TenantId { get; set; }
    }
}
