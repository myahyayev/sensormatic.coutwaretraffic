﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public abstract class ReportBuilder : IDisposable, IReportBuilder
    {
        protected ScheduleReport Report;
      
        protected readonly IServiceProvider ServiceProvider;

        protected readonly IOptions<TimeZoneSettings> TimeZoneSettings;

        public ReportBuilder(IServiceProvider serviceProvider, IOptions<TimeZoneSettings> timeZoneSettings)
        {
            ServiceProvider = serviceProvider;
            TimeZoneSettings = timeZoneSettings;
        }

        public virtual IReportBuilder WithCreator(string userName)
        {
            Report.ReportCreator = userName;
            return this;
        }

        public virtual IReportBuilder WithPeriodType(ReportPeriodType reportPeriodType)
        {
            Report.ReportPeriodType = reportPeriodType;
            return this;
        }

        public virtual IReportBuilder WithId(Guid reportId)
        {
            Report.ReportId = reportId;
            return this;
        }

        public virtual IReportBuilder WithName(string reportName)
        {
            Report.ReportName = reportName;
            return this;
        }

        public virtual IReportBuilder WithDatasource(object dataSource)
        {
            Report.ReportData = dataSource;
            return this;
        }

        public virtual IReportBuilder WithExportFormat(ReportExportype expportFormat)
        {
            Report.ExportFormat = expportFormat;
            return this;
        }

        public virtual IReportBuilder WithFrequencyType(ReportFrequencyType frequencyType)
        {
            Report.FrequencyType = frequencyType;
            return this;
        }

        public virtual IReportBuilder WithEmailBody(string emailBody)
        {
            Report.EmailBody = emailBody;
            return this;
        }

        public virtual IReportBuilder WithEmailSubject(string emailSubject)
        {
            Report.EmailSubject = emailSubject;
            return this;
        }

        public virtual IReportBuilder WithEmailTo(string to)
        {
            Report.EmailTo = to;
            return this;
        }

        public virtual IReportBuilder WithTenantId(Guid tenantId)
        {
            Report.TenantId = tenantId;
            return this;
        }

        public virtual IReportBuilder WithRepeatOn(string repeatOn)
        {
            Report.RepeatOn = repeatOn;
            return this;
        }

        public virtual IReportBuilder WithLanguage(ReportLanguage language)
        {
            Report.Language = language;
            return this;
        }

        public virtual IReportBuilder WithParameters(List<Location> locations, DateTime startdate, DateTime endDate)
        {
            Report.ReportLocations = locations;
            Report.StartDate = startdate;
            Report.EndDate = endDate;
            return this;
        }

        public virtual async Task<IReportBuilder> WithLoadingDataSourceFromDataBaseAsync()
        {
            await Report.LoadDataSourceFromDataBaseAsync();
            return this;
        }

        public IScheduleReport Build() => Report;

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
