﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Engine.TrafficReport;
using DevExpress.XtraReports.UI;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public partial class MonthlyTrafficReport : ScheduleReport, ITrafficReport
    {
        private readonly ITrafficReportElasticSearchRepository _trafficReportElasticSearchRepository;

        public MonthlyTrafficReport() : base()
            => InitializeComponent();

        public MonthlyTrafficReport(IServiceProvider serviceProvider, IOptions<TimeZoneSettings> timeZoneSettings) : base(serviceProvider, timeZoneSettings)
        {
            _trafficReportElasticSearchRepository = serviceProvider.GetRequiredService<ITrafficReportElasticSearchRepository>();
            InitializeComponent();
        }

        public override void RefreshData()
        {
            if (ReportData is not null)
            {
                objectDataSource2.DataSource = ReportData as MonthlyTrafficReportData;

                FillDataSources(false);
                SetResources();
            }
        }

        public override void SetResources()
        {
            var reportData = ReportData as MonthlyTrafficReportData;

            List<XRTableCell> tablesHeaderlist = new();

            tablesHeaderlist.AddRange(ComparedStoresSummaryTableHeaderRow.Cells);
           // tablesHeaderlist.AddRange(ComparedStoresSummaryByDayTableHeaderRow.Cells);
            tablesHeaderlist.AddRange(SummaryTableHeaderRow.Cells);
            tablesHeaderlist.AddRange(SummaryByStoreByDayTableHeaderRow.Cells);

            tablesHeaderlist
                .ForEach(cell =>
                {
                    if (cell.Tag != null)
                        cell.Text = GetResouceContentByName(cell.Tag.ToString());
                });

            ComparedStoresSummaryTitle.Text = reportData.ComparedStoresSummary.Title;
            ComparedStoresSummarySubTitle.Text = reportData.ComparedStoresSummary.SubTitle;
            ComparedStoresSummaryFooterTitle.Text = reportData.ComparedStoresSummary.FooterTitle;

            SummaryByStoreTitle.Text = reportData.SummaryByStore.Title;
            SummaryByStoreSubTitle.Text = reportData.SummaryByStore.SubTitle;
            SummaryByStoreFooterTitle.Text = reportData.SummaryByStore.FooterTitle;
            SummaryByStoreFooterTotal.Text = GetResouceContentByName("Total");

            TrafficByDayChartsTitle.Text = reportData.TrafficByDayChart.Title;
            TrafficByDayChartsSubTitle.Text = reportData.TrafficByDayChart.SubTitle;
            TrafficByDayChartsFooterText.Text = reportData.TrafficByDayChart.FooterTitle;

            //ComparedStoresSummaryByDayTitle.Text = reportData.ComparedStoresSummaryByDay.Title;
            //ComparedStoresSummaryByDaySubTitle.Text = reportData.ComparedStoresSummaryByDay.SubTitle;
            //ComparedStoresSummaryByDayFooterTitle.Text = reportData.ComparedStoresSummaryByDay.FooterTitle;

            TrafficByStoreChartsTitle.Text = reportData.TrafficByStoreChart.Title;
            TrafficByStoreChartsSubTitle.Text = reportData.TrafficByStoreChart.SubTitle;
            TrafficByStoreChartsFooter.Text = reportData.TrafficByStoreChart.FooterTitle;

            SummaryByStoreByDayTitle.Text = reportData.SummaryByStoreByDay.Title;
            SummaryByStoreByDaySubTitle.Text = reportData.SummaryByStoreByDay.SubTitle;
            SummaryByStoreByDayFooter.Text = reportData.SummaryByStoreByDay.FooterTitle;
            SummaryByStoreByDayFooterTotal.Text = GetResouceContentByName("Total");

            TrafficByStoreChartsTitle.Text = reportData.TrafficByStoreChart.Title;
            TrafficByStoreChartsSubTitle.Text = reportData.TrafficByStoreChart.SubTitle;
            TrafficByStoreChartsFooter.Text = reportData.TrafficByStoreChart.FooterTitle;
        }
        public override async Task LoadDataSourceFromDataBaseAsync()
        {
            var trafficByDayChart = await TrafficByDayChartAsync();
            ReportData = new MonthlyTrafficReportData
            {
                ComparedStoresSummary = await TrafficComparedStoresSummaryAsync(),
                SummaryByStore = await TrafficSummaryByStoreAsync(),
                TrafficByStoreChart = await TrafficByStoreChartAsync(),
                //ComparedStoresSummaryByDay = trafficByDayChart,  //there is duplicate
                TrafficByDayChart = trafficByDayChart,           //there is duplicate
                SummaryByStoreByDay = await TrafficSummaryByStoreAndDayAsync(),
                Title = TitleValue,
                SubTitle = SubTitleValue,
                CreatedBy = ReportCreator,
                CreatedDate = ReportCreatedDate
            };

            await base.LoadDataSourceFromDataBaseAsync();
        }


        public override string ResourceKey => $"{GetType().FullName}";

        #region private methods
        private async Task<BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary>> TrafficComparedStoresSummaryAsync()
            => new()
            {
                Content = await _trafficReportElasticSearchRepository
                    .TrafficComparedStoresSummaryAsync(ReportLocations, StartDate, EndDate, ReportPeriodType.Monthly, TenantId, Language.Id),
                Title = GetResouceContentByName("ComparedStoresSummaryTitle"),
                SubTitle = GetResouceContentByName("ComparedStoresSummarySubTitle"),
                FooterTitle = $"{GetResouceContentByName("ComparedStoresSummaryFooterTitle")} : {ReportDateRange}"
            };


        private async Task<BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary>> TrafficSummaryByStoreAsync()
            => new()
            {   //todo:weather data will be injected to the model here
                Content = await _trafficReportElasticSearchRepository
                    .TrafficSummaryByStoreAsync(ReportLocations, StartDate, EndDate, ReportPeriodType.Monthly, TenantId, Language.Id),
                Title = GetResouceContentByName("SummaryByStoreTitle"),
                FooterTitle = $"{GetResouceContentByName("SummaryByStoreFooterTitle")} : {ReportDateRange}"
            };


        private async Task<BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary>> TrafficByDayChartAsync()
            => new()
            {
                Content = await _trafficReportElasticSearchRepository
                    .TrafficByTimePeriodChartAsync(ReportLocations, StartDate, EndDate, ReportPeriodType.Monthly, DateRangeType.ByDate, TenantId, Language.ShortName, Language.WindowsTimeZone, Language.LinuxTimeZone),
                Title = GetResouceContentByName("TrafficByDayChartsTitle"),
                FooterTitle = $"{GetResouceContentByName("TrafficByDayChartsFooterText")} : {ReportDateRange}"
            };


        private async Task<BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary>> TrafficByStoreChartAsync()
            => new()
            {
                Content = await _trafficReportElasticSearchRepository.TrafficByStoreChartAsync(ReportLocations, StartDate, EndDate, TenantId, Language.ShortName, Language.WindowsTimeZone, Language.LinuxTimeZone),
                Title = GetResouceContentByName("TrafficByStoreChartsTitle"),
                FooterTitle = $"{GetResouceContentByName("TrafficByStoreChartsFooter")} : {ReportDateRange}"
            };

        private async Task<BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary>> TrafficSummaryByStoreAndDayAsync()
           => new()
           {
               Content = await _trafficReportElasticSearchRepository.TrafficSummaryByStoreAndTimePeriodAsync(ReportLocations, StartDate, EndDate, DateRangeType.ByDate, TenantId, Language.ShortName, Language.WindowsTimeZone, Language.LinuxTimeZone),
               Title = GetResouceContentByName("TrafficSummaryByStoreAndDayFooterTitle"),
               FooterTitle = $"{GetResouceContentByName("TrafficSummaryByStoreAndDayFooter")} : {ReportDateRange}"
           };

        #endregion private methods

    }
}
