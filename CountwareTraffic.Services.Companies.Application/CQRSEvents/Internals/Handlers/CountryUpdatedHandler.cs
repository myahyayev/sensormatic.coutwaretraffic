﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CountryUpdatedHandler : IEventHandler<CountryUpdated>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public CountryUpdatedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(CountryUpdated command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId
            };

            CountryElasticDto countryElasticDto = new()
            {
                Id = command.CountryId,
                Name = command.CountryName
            };

            await _tenantHierarchyElasticSearchRepository.UpdateCountryAsync(tenantElasticDto, companyElasticDto, countryElasticDto);
        }
    }
}

