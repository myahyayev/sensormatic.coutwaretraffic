﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantPaginationHandler : IQueryHandler<GetTenantPagination, IdentityTenantPaginationResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetTenantPaginationHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityTenantPaginationResponse> HandleAsync(GetTenantPagination query)
        {
            return await _identityServerService.GetTenantPaginationAsync(new IdentityTenantPaginationRequest
            {
                Paging = query.Paging,
                Filters = query.Filters,
                Sorts = query.Sorts
            });
        }
    }
}
