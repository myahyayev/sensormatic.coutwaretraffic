﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Reporting.Grpc
{
    [ServiceLog]
    public sealed partial class GetReportTemplateViewRequest : SensormaticRequestValidate
    {
        internal Guid _ReportTemplateId
        {
            get
            {
                if (Guid.TryParse(reportTemplateId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { reportTemplateId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_ReportTemplateId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1535 ReportTemplateId cannot be null", nameof(_ReportTemplateId)));
        }
    }
}