﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Events.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    public class UnknownDeviceCreatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.UnknownDeviceCreated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        public UnknownDeviceCreatedConsumer(IEventDispatcher eventDispatcher) => _eventDispatcher = eventDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.UnknownDeviceCreated queueEvent)
        {
            UnknownDeviceCreated unknownDeviceCreated = new()
            {
                DeviceName = queueEvent.Name,
                HttpPort = queueEvent.HttpPort,
                HttpsPort = queueEvent.HttpsPort,
                IpAddress = queueEvent.IpAddress,
                MacAddress = queueEvent.MacAddress,
                SerialNumber = queueEvent.SerialNumber
            };

            await _eventDispatcher.PublishAsync(unknownDeviceCreated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}