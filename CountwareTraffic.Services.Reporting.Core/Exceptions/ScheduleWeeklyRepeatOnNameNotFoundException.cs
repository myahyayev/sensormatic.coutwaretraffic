﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleWeeklyRepeatOnNameNotFoundException : DomainException
    {
        IEnumerable<ScheduleWeeklyRepeatOn> ScheduleWeeklyRepeatOns { get; }
        public string Name { get; }

        public ScheduleWeeklyRepeatOnNameNotFoundException(IEnumerable<ScheduleWeeklyRepeatOn> scheduleWeeklyRepeatOns, string name)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ScheduleWeeklyRepeatOn Name: {String.Join(",", scheduleWeeklyRepeatOns.Select(s => s.Name))}") }, 400, ResponseMessageType.Error)
        {
            ScheduleWeeklyRepeatOns = scheduleWeeklyRepeatOns;
            Name = name;
        }
    }
}
