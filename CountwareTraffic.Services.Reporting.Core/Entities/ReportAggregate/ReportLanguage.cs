﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportLanguage : AggregateRoot, IDateable, IDeletable, ITraceable
    {
        private string _name;
        private string _description;
        private string _shortName;
        private string _windowsTimeZone;
        private string _linuxTimeZone;


        public string Name => _name;
        public string Description => _description;
        public string ShortName => _shortName;
        public string WindowsTimeZone => _windowsTimeZone;
        public string LinuxTimeZone => _linuxTimeZone;

        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public bool AuditIsDeleted { get; set; }
        #endregion


        private ReportLanguage() { }

        public static ReportLanguage Create(string name, string description,  string shortName,  string windowsTimeZone, string linuxTimeZone)
        {
            return new ReportLanguage
            {
                _name = name,
                _description = description,
                _shortName = shortName,
                _windowsTimeZone = windowsTimeZone,
                _linuxTimeZone = linuxTimeZone
            };
        }
    }
}
