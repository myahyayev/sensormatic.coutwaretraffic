﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class GetDeviceByMac : Convey.CQRS.Queries.IQuery<DeviceDto>
    {
        public string MacAddress { get; set; }
    }
}
