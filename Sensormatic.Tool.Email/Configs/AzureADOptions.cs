﻿using Sensormatic.Tool.Ioc;

namespace Sensormatic.Tool.Email.Configs
{
    public class AzureADOptions : IConfigurationOptions
    {
        public string? Instance { get; set; }
        public string? TenantId { get; set; }
        public string? ClientId { get; set; }
        public string? ClientSecret { get; set; }
    }
}
