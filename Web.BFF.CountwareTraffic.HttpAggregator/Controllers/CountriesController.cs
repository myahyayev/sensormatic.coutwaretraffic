﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class CountriesController : BaseController
    {
        private readonly CountryService _countryService;
        public CountriesController(IServiceProvider provider, ILogger<CountriesController> logger, IHttpContextAccessor contextAccessor, CountryService countryService) 
            : base(provider, logger, contextAccessor.HttpContext) => _countryService = countryService;


        [HttpGet("{countryId}/detail"), ServiceLog]
        public async Task<ApiResponse<GetCountryDetails>> GetCountryDetailAsync(Guid countryId)
            => Response(await _countryService.GetCountryByIdAsync(countryId), ResultMessage.SuccessResultMessage);


        [HttpPost("{companyId}/details"), ServiceLog]
        public async Task<ApiPagedResponse<GetCountryDetails>> GetCountryDetailsAsync(Guid companyId, [FromBody] DataSourceApiRequest paging)
            => await _countryService.GetCountriesAsync(companyId, paging);
        

        [HttpPost("{companyId}/add"), ServiceLog]
        public async Task<ApiResponse<AddApiResponse>> AddCountryAsync(Guid companyId, [FromForm] AddCountryApiRequest request)
        {
            await _countryService.AddCountryAsync(companyId, request);
            return Response(new AddApiResponse { Created = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpPut("{countryId}/change"), ServiceLog]
        public async Task<ApiResponse<ChangeApiResponse>> ChangeCountryAsync(Guid countryId, [FromForm] ChangeCountryApiRequest request)
        {
            await _countryService.ChangeCountryAsync(countryId, request);
            return Response(new ChangeApiResponse { Updated = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpDelete("{countryId}/delete"), ServiceLog]
        public async Task<ApiResponse<DeleteApiResponse>> DeleteCountryAsync(Guid countryId)
        {
            await _countryService.DeleteCountryAsync(countryId);
            return Response(new DeleteApiResponse { Deleted = "Success" }, ResultMessage.SuccessResultMessage);
        }
    }
}
