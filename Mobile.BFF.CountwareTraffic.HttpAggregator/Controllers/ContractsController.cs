﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Enums;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer;
using Sensormatic.Tool.Api;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class ContractsController : BaseController
    {
        private readonly UserService _userService;

        public ContractsController(IServiceProvider provider,
                                   ILogger<ContractsController> logger,
                                   UserService userService,
                                   IHttpContextAccessor httpContextAccessor)
            : base(provider, logger, httpContextAccessor)
        {
            _userService = userService;
        }

        [AllowAnonymous, HttpGet("{id}")]
        public async Task<ApiResponse<GetContractByIdApiResponse>> GetContractByIdAsync(Guid id)
        {
            var (result, status, message) = await _userService.GetContractByIdAsync(id);

            return Response(result, status == 200 ? new MessageResponse("#S1057")
                                                  : new MessageResponse(message));
        }

        [AllowAnonymous, HttpGet("{contractType}/{language}/list")]
        public async Task<ApiResponse<List<GetContractsByParametersItemApiResponse>>> GetContractsByParametersAsync(
                                                                                                    string contractType,
                                                                                                    string language)
        {
            var response = await _userService.GetContractsByParametersAsync(new GetContractsByParametersApiRequest
            {
                AppType = (int)AppType.CwTraffic,
                ContractType = contractType,
                Language = language
            });

            return Response(response.Data, response.Status == 200 ? new MessageResponse("#S1058")
                                                                  : new MessageResponse(response.Message));
        }

        [HttpPost]
        [Permissions(Permissions = new[] { Permissions.AdminContractAdd })]
        public async Task<ApiResponse<CreateContractApiResponse>> CreateContractAsync([FromBody] CreateContractApiRequest request)
        {
            var response = await _userService.CreateContractAsync(request);

            return Response(response, response.Status == 200 ? new MessageResponse("#S1059")
                                                             : new MessageResponse(response.Message));
        }

        [HttpPut]
        [Permissions(Permissions = new[] { Permissions.AdminContractEdit })]
        public async Task<ApiResponse<UpdateContractApiResponse>> UpdateContractAsync([FromBody] UpdateContractApiRequest request)
        {
            var response = await _userService.UpdateContractAsync(request);

            return Response(response, response.Status == 200 ? new MessageResponse("#S1060")
                                                             : new MessageResponse(response.Message));
        }

        [HttpGet("{contractId}/confirm-contract")]
        public async Task<ApiResponse<bool>> ConfirmContractAsync(Guid contractId)
        {
            var response = await _userService.ConfirmContractAsync(contractId);

            return Response(response, response ? new MessageResponse("#S1144") : new MessageResponse(""));
        }

        [HttpGet("check-contracts")]
        public async Task<ApiResponse<List<CheckContractsApiResponse>>> CheckContractsAsync()
        {
            var language = _httpContextAccessor.GetLanguage();
            var response = await _userService.CheckContractsAsync(language);

            return Response(response, response != null ? new MessageResponse("#S1145") : new MessageResponse(""));
        }
    }
}
