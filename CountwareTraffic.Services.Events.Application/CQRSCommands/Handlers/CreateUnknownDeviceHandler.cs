﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Core;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application.Commands
{
    public class CreateUnknownDeviceHandler : ICommandHandler<CreateUnknownDevice>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<CreateUnknownDeviceHandler> _logger;
        public CreateUnknownDeviceHandler(IUnitOfWork unitOfWork, ILogger<CreateUnknownDeviceHandler> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task HandleAsync(CreateUnknownDevice command)
        {
            var unknownDeviceRepository = _unitOfWork.GetRepository<IUnknownDeviceRepository>();

            if (await unknownDeviceRepository.ExistsAsync(command.IpAddress, command.MacAddress))
            {
                _logger.LogInformation($"UnknownDevice includes current record. Record IpAddress: {command.IpAddress}, Record MacAddress: {command.MacAddress}");
                return;
            }

            var unknownDevice = UnknownDevice.Create(command.Name, command.HttpPort, command.HttpsPort, command.IpAddress, command.MacAddress, command.SerialNumber);

            await unknownDeviceRepository.AddAsync(unknownDevice);

            unknownDevice.WhenCreated();

            await _unitOfWork.CommitAsync();
        }
    }
}
