﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record AreaHDeleted : MessageEnvelope, IQueueEvent
    {
        public AreaHDeleted(Guid areaId, string areaName, Guid regionId, Guid countryId,  Guid companyId,  Guid recordId, Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            AreaId = areaId;
            AreaName = areaName;
            RegionId = regionId;
            CountryId = countryId;
            CompanyId = companyId;
            RecordId = recordId;
        }
        public Guid AreaId { get; }
        public string AreaName { get; }
        public Guid RegionId { get; }
        public Guid CountryId { get; }
        public Guid CompanyId { get; }
        public Guid RecordId { get; init; }
    }
}
