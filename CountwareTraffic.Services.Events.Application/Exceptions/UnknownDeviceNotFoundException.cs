﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public class UnknownDeviceNotFoundException : AppException
    {
        public Guid Id { get; }
        public UnknownDeviceNotFoundException(Guid id)
            : base(new List<ErrorResult>() { new ErrorResult($"Unknown Device with id: {id} was not found") }, 404, ResponseMessageType.Error)
        {
            Id = id;
        }
    }
}
