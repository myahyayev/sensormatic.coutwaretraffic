﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application
{
    public class HttpRequestFailureException : AppException
    {
        public HttpRequestFailureException(IList<ErrorResult> errorResults, int code, ResponseMessageType responseMessageType)
            : base(errorResults, code, responseMessageType)
        {
        }
    }
}
