﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer
{
    public class RoleAdminPaginationApiRequest
    {
        public PagingQuery Paging { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }

        public string Language { get; set; }

        public RoleAdminPaginationApiRequest()
        {
            Paging = new PagingQuery();
            Filters = new List<GridFilter>();
            Sorts = new List<SortDescriptor>();
        }
    }

}