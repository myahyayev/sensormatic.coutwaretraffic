﻿using Microsoft.EntityFrameworkCore.Migrations;
using Nest;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class OutboxMessagesStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"IF EXISTS (SELECT * FROM sys.indexes  WHERE name = 'NC_IX_IsTryFromQueue_OccurredOn_ProcessedDate' AND object_id = OBJECT_ID('[area.app].[OutboxMessages]')) DROP INDEX NC_IX_IsTryFromQueue_OccurredOn_ProcessedDate ON[area.app].[OutboxMessages]");

            migrationBuilder.DropColumn(
                name: "IsTryFromQueue",
                schema: "area.app",
                table: "OutboxMessages");

            migrationBuilder.AddColumn<byte>(
                name: "Status",
                schema: "area.app",
                table: "OutboxMessages",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

           
            migrationBuilder.Sql(@"CREATE INDEX [NC_IX_Status_OccurredOn] ON [area.app].[OutboxMessages] ([Status] ASC,[OccurredOn] ASC)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"DROP INDEX [NC_IX_Status_OccurredOn] ON [area.app].[OutboxMessages]");

            migrationBuilder.DropColumn(
                name: "Status",
                schema: "area.app",
                table: "OutboxMessages");

            migrationBuilder.AddColumn<bool>(
                name: "IsTryFromQueue",
                schema: "area.app",
                table: "OutboxMessages",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
