﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ResetPasswordApiResponse
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
