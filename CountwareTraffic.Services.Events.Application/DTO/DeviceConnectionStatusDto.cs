﻿using CountwareTraffic.Services.Events.Core;
using System;

namespace CountwareTraffic.Services.Events.Application
{
    public record DeviceConnectionStatusDto
    {
        public DeviceConnectionStatusDto()
        {
            ChangeTime = DateTime.UtcNow;
        }
        public Guid DeviceId { get; set; }
        public DateTime ExpireTime { get; set; }
        public DateTime ChangeTime { get; init; }
        public DeviceStatus Status
        {
            get;init;
        }
    }
}
