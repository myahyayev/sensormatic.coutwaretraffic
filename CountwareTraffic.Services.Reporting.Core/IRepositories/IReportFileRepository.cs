﻿using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Core
{
    public interface IReportFileRepository : IRepository<ReportFile>, IScopedDependency
    {
        Task<QueryablePagingValue<ReportFile>> GetByReportId(Guid reportId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts);
        Task<ReportFile> GetByIdWithReportIdAsync(Guid id, Guid reportId);
        Task<ReportFile> GetFirstByReportId(Guid reportId);
    }
}
