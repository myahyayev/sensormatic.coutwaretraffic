﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class CreateTenantApiRequest
    {
        [FromForm(Name = "name")]
        public string Name { get; set; }

        [FromForm(Name = "description")]
        public string Description { get; set; }

        [FromForm(Name = "erpCode")]
        public string ErpCode { get; set; }

        [FromForm(Name = "erpGroupName")]
        public string ErpGroupName { get; set; }

        [FromForm(Name = "erpStartDate")]
        public DateTime ErpStartDate { get; set; }

        [FromForm(Name = "erpEndDate")]
        public DateTime ErpEndDate { get; set; }

        [FromForm(Name = "tenantGroupId")]
        public Guid TenantGroupId { get; set; }
    }
}
