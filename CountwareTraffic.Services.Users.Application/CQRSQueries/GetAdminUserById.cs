﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries
{
    public class GetAdminUserById : IQuery<IdentityGetAdminUserByIdResponse>
    {
        public Guid Id { get; set; }
    }
}
