﻿using Convey;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Infrastructure;
using CountwareTraffic.Services.Devices.Grpc;
using CountwareTraffic.Services.Users.Grpc;
using Grpc.Net.Client.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Quartz;
using Sensormatic.Tool.ApplicationInsights;
using Sensormatic.Tool.Grpc.Client;
using Sensormatic.Tool.Scheduler;
using System;
using System.Net.Http;

namespace CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler
{

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            Sensormatic.Tool.Ioc.IoCGenerator.DoTNet.Current.Start(services, Configuration);

            services.AddApplicationInsights(Configuration);

            services.AddDbContext<AreaDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("AreaDbConnection"), x => x.UseNetTopologySuite().EnableRetryOnFailure()));


            services.AddConvey()
                    .AddApplication()
                    .Build();

            var assembly = typeof(Sensormatic.Tool.Scheduler.Controllers.HomeController).Assembly;

            services.AddControllersWithViews()
               .AddApplicationPart(assembly)
               .AddRazorRuntimeCompilation();


            services.Configure<MvcRazorRuntimeCompilationOptions>(options => { options.FileProviders.Add(new EmbeddedFileProvider(assembly)); });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSignalR();
            services.AddGrpcServices();
            services.RegisterQuartzServices(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection()
               .UseStaticFiles(new StaticFileOptions { FileProvider = new PhysicalFileProvider(AppDomain.CurrentDomain.BaseDirectory + "/wwwroot") })
               .UseRouting()
               .UseAuthorization()
               .UseGrpcWeb()
               .UseEndpoints(endpoints => { endpoints.MapHub<JobHub>("/signalr"); })
               .UseEndpoints(endpoints =>
               {
                   endpoints.MapControllerRoute(
                       name: "default",
                       pattern: "{controller=Home}/{action=Index}/{id?}");
               });
        }
    }
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterQuartzServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<QuartzOptions>(configuration.GetSection("Quartz"));

            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();

                q.AddJobListener<JobListener>();
                q.AddSchedulerListener<SchedulerListener>();
                q.AddTriggerListener<TriggerListener>();

                q.ScheduleJob<ElasticSearchTenantsHierarchyIndexRepairJob>(trigger => trigger
                    .WithIdentity("EventsIndexRepairTrigger", "Elastic")
                    .StartNow()
                    .WithDescription("ever day at 00:00")
                    .WithCronSchedule("0 0 0 * * ?"));

            });

            services.AddQuartzServer(options =>
            {
                options.WaitForJobsToComplete = true;
            });

            return services;
        }

        public static IServiceCollection AddGrpcServices(this IServiceCollection services)
        {
            var urlsConfig = services.BuildServiceProvider().GetRequiredService<IOptions<UrlsConfig>>().Value;

            #region grpcDevice microservice
            services.AddGrpcClient<Device.DeviceClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcDevice);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
                .RegisterClientInterceptors();
            #endregion grpcDevice microservice



            #region grpcUser microservice
            services.AddGrpcClient<User.UserClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcUser);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
            .RegisterClientInterceptors();
            #endregion grpcUser microservice


            return services;
        }

    }
}