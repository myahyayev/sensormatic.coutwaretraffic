﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record SubAreaDeleted : MessageEnvelope, IQueueEvent
    {
        public SubAreaDeleted(Guid subAreaId, Guid recordId, Guid userId, string correlationId, Guid tenantId)
           : base(userId, correlationId, tenantId)
        {
            SubAreaId = subAreaId;
            RecordId = recordId;
        }
        public Guid SubAreaId { get; }
        public Guid RecordId { get; init; }
    }
}
