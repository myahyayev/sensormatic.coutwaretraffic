﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public record ReportTemplateType : IDeletableValueObject
    {
        private int _typeId;
        private Guid _reportTemplateId;
        public int TypeId => _typeId;
        public Guid ReportTemplateId => _reportTemplateId;

        private ReportTemplateType() { }

        public static ReportTemplateType Create(int typeId, Guid reportTemplateId)
        {
            return new ReportTemplateType
            {
                _typeId = typeId,
                _reportTemplateId = reportTemplateId
            };
        }

        public object[] GetKeys()
        {
            return new object[] { _typeId, _reportTemplateId };
        }
    }
}
