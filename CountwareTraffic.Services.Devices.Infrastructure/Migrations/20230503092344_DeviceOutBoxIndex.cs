﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Devices.Infrastructure.Migrations
{
    public partial class DeviceOutBoxIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF NOT EXISTS (SELECT * FROM sys.indexes  WHERE name = 'NC_IX_EventRecordId' AND object_id = OBJECT_ID('[device.app].[OutboxMessages]')) CREATE NONCLUSTERED INDEX [NC_IX_EventRecordId] ON [device.app].[OutboxMessages]([EventRecordId] ASC);");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF EXISTS (SELECT * FROM sys.indexes  WHERE name = 'NC_IX_EventRecordId' AND object_id = OBJECT_ID('[device.app].[OutboxMessages]')) DROP INDEX [NC_IX_EventRecordId] ON [device.app].[OutboxMessages]");
        }
    }
}
