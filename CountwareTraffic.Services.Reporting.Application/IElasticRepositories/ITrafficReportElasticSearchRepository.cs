﻿using CountwareTraffic.Services.Reporting.Core;
using CountwareTraffic.Services.Reporting.Engine;
using CountwareTraffic.Services.Reporting.Engine.TrafficReport;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Application
{
    public interface ITrafficReportElasticSearchRepository : IScopedDependency
    {
        Task<List<ComparedStoreSummary>> TrafficComparedStoresSummaryAsync(List<Location> locations, DateTime startDate, DateTime endDate, ReportPeriodType reportPeriodType, Guid tenantId, Guid languageId);

        Task<List<ComparedStoreSummary>> TrafficSummaryByStoreAsync(List<Location> locations, DateTime startDate, DateTime endDate, ReportPeriodType reportPeriodType, Guid tenantId, Guid languageId);

        Task<List<ComparedStoreSummary>> TrafficByTimePeriodChartAsync(List<Location> locations, DateTime startDate, DateTime endDate, ReportPeriodType reportPeriodType, DateRangeType dateRangeType, Guid tenantId, string languageShortName, string languageWindowsTimeZone, string languageLinuxTimeZone);

        Task<List<ComparedStoreSummary>> TrafficByStoreChartAsync(List<Location> locations, DateTime startDate, DateTime endDate, Guid tenantId, string languageShortName, string languageWindowsTimeZone, string languageLinuxTimeZone);

        Task<List<ComparedStoreSummary>> TrafficSummaryByStoreAndTimePeriodAsync(List<Location> locations, DateTime startDate, DateTime endDate, DateRangeType dateRangeType, Guid tenantId, string languageShortName, string languageWindowsTimeZone, string languageLinuxTimeZone);
    }
}
