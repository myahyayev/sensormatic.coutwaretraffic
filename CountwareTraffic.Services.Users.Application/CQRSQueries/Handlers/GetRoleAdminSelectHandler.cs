﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class GetRoleAdminSelectHandler : IQueryHandler<GetRoleAdminSelect, IdentityGetRoleAdminSelectResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetRoleAdminSelectHandler(IIdentityServerService identityServerService)
        {
            _identityServerService = identityServerService;
        }

        public async Task<IdentityGetRoleAdminSelectResponse> HandleAsync(GetRoleAdminSelect query)
        {
            return await _identityServerService.GetRoleAdminSelectResponse();
        }
    }
}
