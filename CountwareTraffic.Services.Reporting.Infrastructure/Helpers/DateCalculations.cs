﻿using CountwareTraffic.Services.Reporting.Engine;
using Sensormatic.Tool.Common;
using System;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public static class DateCalculations
    {
        public static DateRange ToDateRange(DateTime from, DateTime to, ReportPeriodType reportPeriodType, SummaryCompareType summaryCompareType)
        {
            DateRange calculatedDateRange = null;

            switch (reportPeriodType)
            {
                // Daily
                case ReportPeriodType.Daily:

                    switch (summaryCompareType)
                    {
                        case SummaryCompareType.ToLastWeek:
                            calculatedDateRange = new()
                            {
                                Type = DateRangeType.ByHour,
                                Caption = "Last Week",
                                From = from.AddDays(-7),
                                To = to.AddDays(-7)
                            };
                            break;

                        case SummaryCompareType.ToLastYear:
                            calculatedDateRange = new()
                            {
                                Type = DateRangeType.ByHour,
                                Caption = "Last Year",
                                From = from.AddYears(-1),
                                To = to.AddYears(-1)
                            };
                            break;

                        case SummaryCompareType.ToLastLastYear:
                            calculatedDateRange = new()
                            {
                                Type = DateRangeType.ByHour,
                                Caption = "Last Last Year",
                                From = from.AddYears(-2),
                                To = to.AddYears(-2)
                            };
                            break;

                        default:
                            break;
                    }
                    break;

                // Weekly
                case ReportPeriodType.Weekly:
                    switch (summaryCompareType)
                    {
                        case SummaryCompareType.ToLastWeek:
                            calculatedDateRange = new()
                            {
                                Caption = "Last Week",
                                From = DateTime.Today.AddDays(-(int)DateTime.Now.DayOfWeek - 6),
                                To = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday)
                            };
                            break;

                        case SummaryCompareType.ToLastYear:
                            calculatedDateRange = new()
                            {
                                Caption = "Last Year",
                                From = from.AddYears(-1).StartOfWeek(DayOfWeek.Monday),
                                To = from.AddYears(-1).StartOfWeek(DayOfWeek.Monday).AddDays(6)
                            };
                            break;

                        case SummaryCompareType.ToLastLastYear:
                            calculatedDateRange = new()
                            {
                                Caption = "Last Last Year",
                                From = from.AddYears(-2).StartOfWeek(DayOfWeek.Monday),
                                To = from.AddYears(-2).StartOfWeek(DayOfWeek.Monday).AddDays(6)
                            };
                            break;

                        default:
                            break;
                    }
                    break;

                // Monthly
                case ReportPeriodType.Monthly:
                    switch (summaryCompareType)
                    {
                        case SummaryCompareType.ToLastMonth:

                            calculatedDateRange = new()
                            {
                                Caption = "Last Month",
                                From = new DateTime(from.AddMonths(-1).Year, from.AddMonths(-1).Month, 1),
                                To = new DateTime(from.AddMonths(-1).Year, from.AddMonths(-1).Month, 1).AddMonths(1).AddDays(-1)
                            };
                            break;

                        case SummaryCompareType.ToLastYear:
                            calculatedDateRange = new()
                            {
                                Caption = "Last Last Year",
                                From = new DateTime(from.AddYears(-1).Year, from.AddYears(-1).Month, 1),
                                To = new DateTime(from.AddYears(-1).Year, from.AddYears(-1).Month, 1).AddMonths(1).AddDays(-1)
                            };
                            break;

                        case SummaryCompareType.ToLastLastYear:
                            calculatedDateRange = new()
                            {
                                Caption = "Last Last Year",
                                From = new DateTime(from.AddYears(-2).Year, from.AddYears(-2).Month, 1),
                                To = new DateTime(from.AddYears(-2).Year, from.AddYears(-2).Month, 1).AddMonths(1).AddDays(-1)
                            };
                            break;

                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
            return calculatedDateRange;
        }
    }
}
