﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries
{
    public class GetRoleAdminSelect : IQuery<IdentityGetRoleAdminSelectResponse>
    {
    }
}
