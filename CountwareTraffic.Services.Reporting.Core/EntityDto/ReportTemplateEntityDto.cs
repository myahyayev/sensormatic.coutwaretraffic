﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportTemplateEntityDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AssemblyQualifiedName { get; set; }
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }

        public IList<TemplateTypeEntityDto> TemplateTypeEntityDtos { get; set; }
    }
}

