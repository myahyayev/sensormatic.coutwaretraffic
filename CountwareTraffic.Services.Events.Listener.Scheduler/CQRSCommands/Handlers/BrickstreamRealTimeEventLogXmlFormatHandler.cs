﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Queue;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class BrickstreamRealTimeEventLogXmlFormatHandler : EventLogXmlFormatHandlerBase<BrickstreamRealTimeEventLogXmlFormatHandler>, ICommandHandler<BrickstreamRealTimeEventLogXmlFormat>
    {


        public BrickstreamRealTimeEventLogXmlFormatHandler(ILogger<BrickstreamRealTimeEventLogXmlFormatHandler> logger, IOptions<TimeZoneSettings> timeZoneSettings, IIdentityService identityService, IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher, ICache redisCacheService, IQueueService queueService, IDeviceConnectionStatusProcessorService deviceConnectionStatusProcessorService) : base(logger,timeZoneSettings, identityService, queryDispatcher, commandDispatcher, redisCacheService, queueService,deviceConnectionStatusProcessorService)
        {

        }

        public async Task HandleAsync(BrickstreamRealTimeEventLogXmlFormat eventLog)
        {
            var xml = HttpUtility.UrlDecode(eventLog.Xml);

            if (xml.IsNullOrWhiteSpace())
            {
                _logger.LogError("xml file is empty!");
                return;
            }
            //todo:realtime events will be served to panel via signalR and wont stored
            //var objectFromXml = RealTimeXmlEventParse(xml);

            //if (objectFromXml != null)
            //{
            //var currentDevice =
            //        await _queryDispatcher.QueryAsync(new GetDeviceByMac() { MacAddress = objectFromXml.MacAddress });

            //await IsDeviceValid(currentDevice, objectFromXml);

            //    await ProcessDeviceConnectionStatus(currentDevice);
            //    var redisKey = string.Format(CacheKeys.DeviceRealTimeLatestEvent, currentDevice.Id);
            //    if (objectFromXml.Events != null && objectFromXml.Events.Count > 0)
            //    {
            //        foreach (var deviceEvent in objectFromXml.Events)
            //        {

            //            var latestEvent = await _redisCacheService.GetAsync<LatestEventDto>(redisKey);
            //            var splittedEventsList = new List<DeviceEventsListener>();

            //            var onTimeIOCount = 0;
            //            var directionType = DirectionType.Unknown;

            //            //if redis doesn't have last event and device counts have been reseted
            //            if (!(latestEvent != null && (latestEvent.EnterCount <= deviceEvent.EnterCount || latestEvent.ExitCount <= deviceEvent.ExitCount)))
            //            {
            //                latestEvent = new LatestEventDto()
            //                {
            //                    DeviceId = currentDevice.Id,
            //                    ExitCount = 0,
            //                    EnterCount = 0
            //                };
            //            }

            //            //there is outward event
            //            if (latestEvent.EnterCount == deviceEvent.EnterCount && latestEvent.ExitCount != deviceEvent.ExitCount)
            //            {
            //                directionType = DirectionType.Outward;
            //                onTimeIOCount = deviceEvent.ExitCount - latestEvent.ExitCount;
            //            }
            //            //there is inward event
            //            else if (latestEvent.EnterCount != deviceEvent.EnterCount && latestEvent.ExitCount == deviceEvent.ExitCount)
            //            {
            //                directionType = DirectionType.Inwards;
            //                onTimeIOCount = deviceEvent.EnterCount - latestEvent.EnterCount;
            //            }
            //            //there is outward event and inward event
            //            else if (latestEvent.EnterCount != deviceEvent.EnterCount && latestEvent.ExitCount != deviceEvent.ExitCount)
            //            {
            //                int onTimeInwards = deviceEvent.EnterCount - latestEvent.EnterCount;
            //                int onTimeOutwards = deviceEvent.ExitCount - latestEvent.ExitCount;

            //                if (onTimeInwards > onTimeOutwards)
            //                {
            //                    onTimeIOCount = onTimeInwards - onTimeOutwards;
            //                    directionType = DirectionType.Inwards;
            //                }
            //                else if (onTimeOutwards > onTimeInwards)
            //                {
            //                    onTimeIOCount = onTimeOutwards - onTimeInwards;
            //                    directionType = DirectionType.Outward;
            //                }
            //                else
            //                {

            //                    //some devices like Hikvision doesn't split event into sepreted events in this case,so we spilt it!

            //                    //make Outward event
            //                    DeviceEventsListener tmp = new DeviceEventsListener
            //                    {
            //                        DeviceId = currentDevice.Id,
            //                        OnTimeIOCount = onTimeOutwards,
            //                        EnterCount = deviceEvent.EnterCount,
            //                        ExitCount = deviceEvent.ExitCount,
            //                        UserId = _identityService.UserId,
            //                        RecordId = Guid.NewGuid(),
            //                        EventDate = deviceEvent.StartDate,
            //                        EventStartDate = deviceEvent.StartDate,
            //                        EventEndDate = deviceEvent.EndDate,
            //                        DeviceName = currentDevice.Name,
            //                        DirectionTypeId = DirectionType.Outward.Id,
            //                        TenantId = currentDevice.TenantId,
            //                        Description = $"Splitted Realtime Event = DeviceName: {currentDevice.Name}, EnterCount: {deviceEvent.EnterCount}, ExitCount: {deviceEvent.ExitCount}, CountDiffrent: {deviceEvent.CountDiffrent}, OnTime{DirectionType.Outward.Name}Count: {onTimeOutwards}",
            //                    };

            //                    splittedEventsList.Add(tmp);

            //                    //make Inward event from Outward event
            //                    splittedEventsList.Add(
            //                        tmp with
            //                        {
            //                            RecordId = Guid.NewGuid(),
            //                            DirectionTypeId = DirectionType.Inwards.Id,
            //                            OnTimeIOCount = onTimeInwards,
            //                            Description = $"Splitted Realtime Event = DeviceName: {currentDevice.Name}, EnterCount: {deviceEvent.EnterCount}, ExitCount: {deviceEvent.ExitCount}, CountDiffrent: {deviceEvent.CountDiffrent}, OnTime{DirectionType.Inwards.Name}Count: {onTimeInwards}"
            //                        }
            //                    );

            //                }
            //            }

            //            await _redisCacheService.AddAsync(redisKey, new LatestEventDto
            //            {
            //                EnterCount = deviceEvent.EnterCount,
            //                ExitCount = deviceEvent.ExitCount,
            //                DeviceId = currentDevice.Id
            //            }, DateTime.Now.AddDays(1) - DateTime.Now, @override: true);

            //            if (splittedEventsList.Any())
            //            {
            //                foreach (var currentSplittedEvent in splittedEventsList)
            //                {
            //                    await _commandDispatcher.SendAsync(currentSplittedEvent);
            //                }
            //            }
            //            else
            //            {
            //                await _commandDispatcher.SendAsync(new DeviceEventsListener
            //                {
            //                    DeviceId = currentDevice.Id,
            //                    OnTimeIOCount = onTimeIOCount,
            //                    EnterCount = deviceEvent.EnterCount,
            //                    ExitCount = deviceEvent.ExitCount,
            //                    UserId = _identityService.UserId,
            //                    RecordId = Guid.NewGuid(),
            //                    EventDate = deviceEvent.StartDate,
            //                    EventStartDate = deviceEvent.StartDate,
            //                    EventEndDate = deviceEvent.EndDate,
            //                    DeviceName = currentDevice.Name,
            //                    DirectionTypeId = directionType.Id,
            //                    TenantId = currentDevice.TenantId,
            //                    Description = $"DeviceName: {currentDevice.Name}, EnterCount: {deviceEvent.EnterCount}, ExitCount: {deviceEvent.ExitCount}, CountDiffrent: {deviceEvent.CountDiffrent}, OnTime{directionType.Name}Count: {onTimeIOCount}",
            //                });
            //            }

            //        }
            //    }
            //}
            //else
            //{
            //    _logger.LogError("BrickstreamTimeRangeEventLogXmlFormatHandler xml format is not valid!");
            //    throw new Exception("xml format is not valid!");
            //}


        }

        public override DeviceDto ParseDeviceEventXml(string xml)
        {
            DeviceDto deviceEventInfo = null;

            var xmlDocuemnt = XDocument.Parse(xml.Trim());
            var metrics = xmlDocuemnt.Descendants("RealTimeMetrics");

            if (metrics != null && metrics.Count() > 0)
            {
                var metric = metrics.First();
                var properties = metric.Descendants("Properties").FirstOrDefault();

                deviceEventInfo = new DeviceDto()
                {
                    MacAddress = properties.Descendants("MacAddress").FirstOrDefault()?.Value,
                    IpAddress = properties.Descendants("IpAddress").FirstOrDefault()?.Value,
                    SerialNumber = properties.Descendants("SerialNumber").FirstOrDefault()?.Value,
                    HttpPort = properties.Descendants("HttpPort").FirstOrDefault()?.Value,
                    HttpsPort = properties.Descendants("HttpsPort").FirstOrDefault()?.Value,
                    ModelName = properties.Descendants("HwPlatform").FirstOrDefault()?.Value,
                    FirmWare = properties.Descendants("SwRelease").FirstOrDefault()?.Value,
                    BrandName = "Brickstream"
                };

                //string timeZone = properties.Descendants("Timezone").FirstOrDefault()?.Value;
                //string timeZoneName = properties.Descendants("TimezoneName").FirstOrDefault()?.Value;

                foreach (XElement reportData in metric.Descendants("RTReport"))
                {
                    DateTime eventDate = DateTime.ParseExact(reportData.Attribute("Date").Value, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);

                    DateTime dt = DateTime.UtcNow;

                    foreach (XElement report in reportData.Descendants("RTObject"))
                    {
                        deviceEventInfo.DeviceId = report.Attribute("DeviceId").Value;
                        deviceEventInfo.DeviceName = report.Attribute("Devicename").Value;

                        foreach (XElement reportItem in report.Descendants("RTCount")) //real time
                        {
                            int enter = Int32.Parse(reportItem.Attribute("TotalEnters").Value);
                            int exit = Int32.Parse(reportItem.Attribute("TotalExits").Value);

                            deviceEventInfo.Events.Add(new EventDto
                            {
                                StartDate = dt,
                                EndDate = dt,
                                EnterCount = enter,
                                ExitCount = exit,
                            });
                        }
                    }
                }
            }

            return deviceEventInfo;
        }

    }
}



