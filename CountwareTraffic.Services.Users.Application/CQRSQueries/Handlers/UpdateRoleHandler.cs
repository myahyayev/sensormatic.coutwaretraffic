﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class UpdateRoleHandler : IQueryHandler<UpdateRole, IdentityUpdateRoleResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public UpdateRoleHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityUpdateRoleResponse> HandleAsync(UpdateRole query)
        {
            return await _identityServerService.UpdateRoleAsync(new IdentityUpdateRoleRequest
            {
                Id = query.Id.ToString(),
                Name = query.Name,
                Description = query.Description,
                Permissions = query.Permissions
            });
        }
    }
}
