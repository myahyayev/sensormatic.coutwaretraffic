﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class RefreshTokenHandler : IQueryHandler<RefreshToken, IdentityRefreshTokenResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public RefreshTokenHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<IdentityRefreshTokenResponse> HandleAsync(RefreshToken query)
        {
            return await _identityServerService.RefreshTokenAsync(new IdentityRefreshTokenRequest
            {
                AccessToken = query.AccessToken,
                RefreshToken = query.RefreshTokenKey
            });
        }
    }
}
