﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSCommands.Handlers
{
    public class CreateContractHandler : IQueryHandler<CreateContract, IdentityCreateContractResponse>
    {
        private readonly IIdentityServerService _identityService;

        public CreateContractHandler(IIdentityServerService identityService)
        {
            _identityService = identityService;
        }

        public async Task<IdentityCreateContractResponse> HandleAsync(CreateContract query)
        {
            return await _identityService.CreateContractAsync(
                  new Objects.Requests.IdentityCreateContractRequest
                  {
                      AppType = query.AppType,
                      ContractBody = query.ContractBody,
                      ContractType = query.ContractType,
                      Language = query.Language,
                      Name = query.Name,
                      ReleaseDate = query.ReleaseDate,
                      Version = query.Version,
                  }).ConfigureAwait(false);
        }
    }
}
