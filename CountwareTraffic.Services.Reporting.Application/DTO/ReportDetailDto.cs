﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportDetailDto : ReportDto
    {
        public bool NotifyWhenComplete { get; set; }
        public string EmailTo { get; set; }
        public string EmailBody { get; set; }
        public string EmailSubject { get; set; }
        public Guid LanguageId { get; set; }
        public string LanguageName { get; set; }
        public Guid ScheduleId { get; set; }
        public int ScheduleFrequencyId { get; set; }
        public string ScheduleFrequencyName { get; set; }
        public int ScheduleDeliveryTimeId { get; set; }
        public string ScheduleDeliveryTimeName { get; set; }
        public DateTime? StartsOn { get; set; }
        public DateTime? EndsOn { get; set; }
        public string AtSetTime { get; set; }
        public string AtSetTimeZone { get; set; }
        public string RepeatOn { get; set; }
        public DateTime DateRangeEndDate { get; set; }
        public DateTime DateRangeStartDate { get; set; }
    }
}
