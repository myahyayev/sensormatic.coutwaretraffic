﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class CompanyOutBoxIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF NOT EXISTS (SELECT * FROM sys.indexes  WHERE name = 'NC_IX_EventRecordId' AND object_id = OBJECT_ID('[area.app].[OutboxMessages]')) CREATE NONCLUSTERED INDEX [NC_IX_EventRecordId] ON [area.app].[OutboxMessages]([EventRecordId] ASC);");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF EXISTS (SELECT * FROM sys.indexes  WHERE name = 'NC_IX_EventRecordId' AND object_id = OBJECT_ID('[area.app].[OutboxMessages]')) DROP INDEX [NC_IX_EventRecordId] ON [area.app].[OutboxMessages]");
        }
    }
}
