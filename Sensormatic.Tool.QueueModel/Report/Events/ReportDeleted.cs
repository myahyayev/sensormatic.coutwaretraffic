﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record ReportDeleted : MessageEnvelope, IQueueEvent
    {
        public ReportDeleted(Guid reportId, Guid recordId, Guid userId, string correlationId, Guid tenantId)
          : base(userId, correlationId, tenantId)
        {
            ReportId = reportId;
            RecordId = recordId;
        }

        public Guid ReportId { get; }
        public Guid RecordId { get; init; }
    }
}
