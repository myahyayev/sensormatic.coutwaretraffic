﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class GetCountriesRequest : SensormaticRequestValidate
    {
        internal Guid _CompanyId
        {
            get
            {
                if (Guid.TryParse(companyId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.companyId_ = value.ToString(); }
        }


        internal DataSourceRequest _DataSourceRequest
        {
            get
            {
                if (this.dataSourceRequest_.PagingRequest == null)
                    this.dataSourceRequest_.PagingRequest = new PagingRequest() { Limit = 10, Page = 1 };

                if (this.dataSourceRequest_.PagingRequest.Limit < 1)
                    this.dataSourceRequest_.PagingRequest.Limit = 10;

                if (this.dataSourceRequest_.PagingRequest.Page < 1)
                    this.dataSourceRequest_.PagingRequest.Page = 1;

                return this.dataSourceRequest_;
            }
            set { this.dataSourceRequest_ = value; }
        }

        public override void Validate()
        {
            if (_CompanyId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1317 CompanyId cannot be null", nameof(_CompanyId)));
        }
    }
}

