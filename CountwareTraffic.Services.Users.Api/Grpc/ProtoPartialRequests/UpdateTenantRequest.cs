﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace CountwareTraffic.Services.Users.Grpc
{
    [ServiceLog]
    public sealed partial class UpdateTenantRequest : SensormaticRequestValidate
    {
        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1474 Tenant Name cannot be null", nameof(Name)));
            if (!Name.IsValidSubdomain())
                ValidateResults.Add(new ErrorResult($"#E1475 Tenant Name is invalid", nameof(Name)));
            if (Name.Length < 2 || Name.Length > 50)
                ValidateResults.Add(new ErrorResult($"#E1476 Tenant Name length must be 2-50 character", nameof(Name)));
        }
    }
}