﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class CreateDeviceHandler : ICommandHandler<CreateDevice>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public CreateDeviceHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(CreateDevice command)
        {
            var deviceRepository = _unitOfWork.GetRepository<IDeviceRepository>();

            if (await deviceRepository.GetByIPAddressAndMacAsync(command.IpAddress,command.MacAddress) !=default)
                throw new DeviceAlreadyExistsException(command.IpAddress,command.MacAddress);

            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            var subArea = await subAreaRepository.GetAsync(command.SubAreaId);

            if (subArea == null)
                command.SubAreaId = System.Guid.Empty;

            var brandName = (await _unitOfWork.GetRepository<IBrandLookupRepository>().GetByIdAsync(command.BrandId)).Name;
            var modelName = (await _unitOfWork.GetRepository<IBrandModelLookupRepository>().GetByIdAsync(command.ModelId)).Name;

            var connectionInfo = DeviceConnectionInfo.Create(command.IpAddress, command.Port, command.Identity, command.Password, command.UniqueId, command.MacAddress);

            var device = Device.Create(command.Name, command.Description, command.Firmware, command.ControlFrequency, command.BrandId, brandName, command.ModelId, modelName, command.Note, command.SubAreaId, connectionInfo, command.DeviceTypeId);

            await deviceRepository.AddAsync(device);

            device.WhenCreated(device.Id, device.Name, connectionInfo.MacAddress, connectionInfo.IpAddress, subArea.Id, subArea.Name, subArea.AreaId, subArea.AreaName, subArea.RegionId, subArea.RegionName, subArea.CountryId, subArea.CountryName, subArea.CompanyId, subArea.CompanyName, subArea.CountryLookupId, subArea.CityId, subArea.CityName, subArea.DistrictId, subArea.DistrictName, device.ControlFrequency);

            device.WhenHierarchyCreated(_currentTenant.Id, subArea.CompanyId, subArea.CountryId, subArea.RegionId, subArea.AreaId, subArea.Id, device.Id, device.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
