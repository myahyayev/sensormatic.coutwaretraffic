﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportStatus : Enumeration
    {
        public static ReportStatus Unknown = new(1, nameof(Unknown));
        public static ReportStatus Generating = new(2, nameof(Generating));
        public static ReportStatus Ready = new(3, nameof(Ready));
        public static ReportStatus Scheduled = new(4, nameof(Scheduled));
        public static ReportStatus Failed = new(5, nameof(Failed));
        public ReportStatus(int id, string name) : base(id, name) { }

        public static IEnumerable<ReportStatus> List() => new[] { Unknown, Generating, Ready, Scheduled, Failed };


        public static ReportStatus FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
                throw new ReportStatusNameNotFoundException(List(), name);

            return state;
        }

        public static ReportStatus From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
                throw new ReportStatusIdNotFoundException(List(), id);

            return state;
        }
    }
}
