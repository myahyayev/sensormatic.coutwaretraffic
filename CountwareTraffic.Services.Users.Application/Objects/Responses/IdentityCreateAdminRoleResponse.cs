﻿namespace CountwareTraffic.Services.Users.Application.Objects.Responses
{
    public class IdentityCreateAdminRoleResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
