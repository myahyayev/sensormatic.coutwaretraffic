﻿using CountwareTraffic.Services.Users.Application.Objects.Requests;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public interface IIdentityServerService : IScopedDependency
    {
        Task<IdentityTokenResponse> GetTokenAsync(IdentityTokenRequest tokenRequest);
        Task<IdentityResetPasswordWithTokenResponse> ResetPasswordWithTokenAsync(IdentityResetPasswordWithTokenRequest resetPasswordWithTokenReuqest);
        Task<IdentityChangePasswordResponse> ChangePasswordAsync(IdentityChangePasswordRequest resetPasswordRequest);
        Task<IdentityTenantsResponse> GetTenantsAsync();
        Task<IdentityRefreshTokenResponse> RefreshTokenAsync(IdentityRefreshTokenRequest refreshTokenRequest);

        Task<IdentityUserGridResponse> GetUserGridAsync(IdentityUserGridRequest identityUserGridRequest);
        Task<IdentityUserResponse> GetUserAsync();
        Task<IdentityCreateUserResponse> CreateUserAsync(IdentityCreateUserRequest identityCreateUserRequest);
        Task<IdentityUpdateUserResponse> UpdateUserAsync(IdentityUpdateUserRequest identityUpdateUserRequest);

        Task<IdentityGetUserIdResponse> GetUserIdAsync(Guid id);
        Task<IdentityDeleteUserResponse> DeleteUserAsync(Guid id);
        Task<IdentityUserResponse> GetUserUserNameAsync(string userName);

        Task<IdentityUpdateUserResponse> CreateUserWithDefaultPasswordAsync(IdentityUpdateUserRequest identityUpdateUserRequest);
        Task<IdentityUserNameExistResponse> GetUserNameExistAsync(string userName);
        Task<IdentityUserNameExistResponse> GetEmailExistAsync(string email);
        Task<IdentityResetPasswordResponse> ResetPasswordAsync(string userName);
        Task<IdentityUserChangeStatusResponse> GetUserChangeStatusAsync(string id);
        Task<bool> GetEmailExistsAsync(string email);
        Task<bool> GetUserAvatarAsync(string id);
        Task<bool> UserResetAsync(string id);
        Task<IdentityTenantGroupPaginationResponse> GetTenantGroupPaginationAsync(IdentityTenantGroupPaginationRequest identityTenantGroupPaginationRequest);
        Task<IdentityTenantGroupResponse> GetTenantGroupAsync();
        Task<IdentityCreateTenantGroupResponse> CreateTenantGroupAsync(IdentityCreateTenantGroupRequest identityCreateTenantGroupRequest);
        Task<IdentityUpdateTenantGroupResponse> UpdateTenantGroupAsync(IdentityUpdateTenantGroupRequest identityUpdateTenantGroupRequest);
        Task<IdentityDeleteTenantGroupResponse> DeleteTenantGroupAsync(Guid id);
        Task<IdentityTenantGroupIdResponse> GetTenantGroupAsync(Guid id);
        Task<IdentityTenantGroupResponse> GetTenantGroupSelectAsync();
        Task<IdentityCreateTenantResponse> CreateTenantAsync(IdentityCreateTenantRequest identityCreateTenantRequest);
        Task<IdentityUpdateTenantResponse> UpdateTenantAsync(IdentityUpdateTenantRequest identityUpdateTenantRequest);
        Task<IdentityTenantSelectResponse> GetTenantSelectAsync();
        Task<IdentityRolePaginationResponse> GetRolePaginationAsync(IdentityRolePaginationRequest identityRolePaginationRequest);
        Task<List<IdentityRolesResponse>> GetRoleAsync();
        Task<IdentityCreateRoleResponse> CreateRoleAsync(IdentityCreateRoleRequest identityCreateRoleRequest);
        Task<IdentityUpdateRoleResponse> UpdateRoleAsync(IdentityUpdateRoleRequest identityUpdateRoleRequest);
        Task<IdentityRoleIdResponse> GetRoleIdAsync(Guid id, string language);
        Task<IdentityDeleteRoleResponse> DeleteRoleAsync(Guid id);
        Task<IdentityAddRoleToUserResponse> AddRoleToUserAsync(Guid userId, string roleName);
        Task<IdentityRemoveRoleFromUserResponse> RemoveRoleFromUserAsync(Guid userId, string roleName);
        Task<IdentityAssignRoleToUserResponse> AssignRolesToUserAsync(string userName);
        Task<IdentityRoleSelectResponse> GetRoleSelectAsync();
        Task<IdentityPermissionPaginationResponse> GetPermissionPaginationAsync(IdentityPermissionPaginationRequest identityPermissionPaginationRequest, string language);
        Task<IdentityPermissionResponse> GetPermissionAsync(Guid id, string language);
        Task<IdentityDeletePermissionResponse> DeletePermissionAsync(Guid id);
        Task<IdentityCreatePermissionResponse> CreatePermissionAsync(IdentityCreatePermissionsRequest identityCreatePermissionsRequest);
        Task<IdentityUpdatePermissionResponse> UpdatePermissionAsync(IdentityUpdatePermissionsRequest identityUpdatePermissionsRequest);
        Task<IdentityCreateRolePermissionResponse> RolePermissionAsync(IdentityCreateRolePermissionRequest identityCreateRolePermissionRequest);
        Task<IdentityCreateRolePermissionsResponse> RolePermissionsAsync(IdentityCreateRolePermissionsRequest identityCreateRolePermissionsRequest);
        Task<IdentityRemovePermissionRoleResponse> RemovePermissionRoleAsync(IdentityRemovePermissionRoleRequest identityRemovePermissionRoleRequest);
        Task<IdentityUpdatePermissionRoleResponse> UpdatePermissionRoleAsync(IdentityUpdatePermissionRoleRequest identityUpdatePermissionRoleRequest);
        Task<IdentityPermissionSelectResponse> GetPermissionSelectAsync();
        Task<IdentityGroupResponse> GetGroupAsync(Guid id);
        Task<IdentityDeleteGroupResponse> DeleteGroupAsync(Guid id);
        Task<IdentityCreateGroupResponse> CreateGroupAsync(IdentityCreateGroupRequest identityCreateGroupRequest);
        Task<IdentityUpdateGroupResponse> UpdateGroupAsync(IdentityUpdateGroupRequest identityUpdateGroupRequest);
        Task<IdentityGroupAddUserResponse> GroupAddUserAsync(IdentityGroupAddUserRequest identityGroupAddUserRequest);
        Task<IdentityGroupSelectResponse> GetGroupSelectAsync();
        Task<IdentityDeleteTokenResponse> DeleteTokenAsync(Guid id);
        Task<IdentityRoleUsersResponse> GetRoleUsersAsync(Guid roleId);
        Task<IdentityUserRolesResponse> GetUserRolesAsync(string userName);
        Task<IdentityUserPermissionsResponse> GetUserPermissionsAsync(Guid userId, string language);
        Task<bool> ControlTokenAsync(string token);
        Task<IdentityForgotPasswordResponse> ForgotPasswordAsync(string userName, string domain, string language);
        Task<IdentityForgotPasswordResponse> ForgotPasswordTokenAsync(IdentityResetPasswordWithTokenRequest token);
        Task<IdentityTokenResponse> ControlOtpTokenAsync(IdentityTokenRequest tokenRequest);
        Task<IdentityGetTenantIdResponse> GetTenantIdAsync(Guid tenantId);
        Task<IdentityTenantPaginationResponse> GetTenantPaginationAsync(IdentityTenantPaginationRequest identityTenantPaginationRequest);

        Task<IdentityAdminPermissionPaginationResponse> AdminPermissionPaginationRequestAsync(IdentityAdminPermissionPaginationRequest request, string language);

        Task<GetAdminPermissionSelectResponse> GetAdminPermissionSelectAsync();
        Task<IdentityGetRoleAdminSelectResponse> GetRoleAdminSelectResponse();
        Task<IdentityCreateAdminRoleResponse> CreateAdminRole(IdentityCreateAdminRoleRequest request);
        Task<IdentityAdminGridResponse> GetAdminGridAsync(IdentityAdminGridRequest request);
        Task<IdentityRoleAdminPaginationResponse> RoleAdminPermissionPaginationRequestAsync(IdentityRoleAdminPaginationRequest request, string language);
        Task<IdentityGetAdminUserByIdResponse> GetAdminUserByIdAsync(IdentityGetAdminUserByIdRequest request);
        Task<IdentityCreateAdminUserWithDefaultPasswordResponse> CreateAdminUserWithDefaultPasswordAsync(IdentityCreateAdminUserWithDefaultPasswordRequest request, string language);
        Task<IdentityUpdateAdminUserResponse> UpdateAdminUserAsync(IdentityUpdateAdminUserRequest request, string language);
        Task<List<IdentityCheckContractsResponse>> CheckContractsAsync(string language);
        Task<IdentityConfirmContractResponse> ConfirmContractAsync(Guid contractId);

        #region Contracts
        Task<IdentityGetContractByIdResponse> GetContractByIdAsync(
                                                IdentityGetContractByIdRequest getContractByIdRequest);
        Task<IdentityCreateContractResponse> CreateContractAsync(IdentityCreateContractRequest createContractRequest);
        Task<IdentityUpdateContractResponse> UpdateContractAsync(IdentityUpdateContractRequest createContractRequest);

        Task<IdentityGetContractsByParametersResponse> GetContractsByParametersAsync(IdentityGetContractsByParametersRequest request);
        #endregion
    }
}
