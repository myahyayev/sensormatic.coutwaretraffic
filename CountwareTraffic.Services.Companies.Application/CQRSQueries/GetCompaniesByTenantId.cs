﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application.DTO;
using System;

namespace CountwareTraffic.Services.Companies.Application.CQRSQueries
{
    public class GetCompaniesByTenantId : IQuery<GetCompaniesByTenantIdDto>
    {
        public Guid TenantId { get; set; }
    }
}
