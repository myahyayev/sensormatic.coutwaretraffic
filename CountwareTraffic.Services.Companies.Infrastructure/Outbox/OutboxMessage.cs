﻿using Sensormatic.Tool.Common;
using System;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class OutboxMessage
    {
        public Guid Id { get; init; }
        public Guid EventRecordId { get; set; }
        public DateTime OccurredOn { get; init; }
        public string Type { get; init; }
        public string Data { get; init; }
        public DateTime? ProcessedDate { get; set; }
        public string LastException { get; set; }
        public OperationStatus Status { get; set; }
        private OutboxMessage() { }
        public OutboxMessage(DateTime occurredOn, string type, string data, Guid eventRecordId)
        {
            Id = Guid.NewGuid();
            OccurredOn = occurredOn;
            Type = type;
            Data = data;
            EventRecordId = eventRecordId;
            Status = OperationStatus.Processing;
        }
    }
}
