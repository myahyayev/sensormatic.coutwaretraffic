﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportFormat : Enumeration
    {
        public static ReportFormat Unknown = new(1, nameof(Unknown));
        public static ReportFormat PDF = new(2, nameof(PDF));
        public static ReportFormat XLS = new(3, nameof(XLS));
        public static ReportFormat HTML = new(4, nameof(HTML));
        public ReportFormat(int id, string name) : base(id, name) { }

        public static IEnumerable<ReportFormat> List() => new[] { Unknown, PDF, XLS, HTML };


        public static ReportFormat FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
                throw new ReportFormatNameNotFoundException(List(), name);

            return state;
        }

        public static ReportFormat From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
                throw new ReportFormatIdNotFoundException(List(), id);

            return state;
        }
    }
}
