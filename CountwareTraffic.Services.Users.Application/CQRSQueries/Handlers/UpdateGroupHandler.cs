﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class UpdateGroupHandler : IQueryHandler<UpdateGroup, IdentityUpdateGroupResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public UpdateGroupHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityUpdateGroupResponse> HandleAsync(UpdateGroup query)
        {
            return await _identityServerService.UpdateGroupAsync(new IdentityUpdateGroupRequest
            {
                Id = query.Id,
                Name = query.Name,
                Description = query.Description
            });
        }
    }
}
