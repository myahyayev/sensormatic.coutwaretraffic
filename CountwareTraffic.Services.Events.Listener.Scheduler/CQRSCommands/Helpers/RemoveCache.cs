﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Application;
using System;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    [Contract]
    public class RemoveCache : ICommand
    {
        public Guid DeviceId { get; set; }

        public RemoveCache(Guid deviceId)
        {
            DeviceId = deviceId;
        }
    }
}
