﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityPermissionPaginationResponse
    {
        public List<IdentityPermissionPaginationData> Data { get; set; }
        public int TotalCount { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
        public int HasNextPage { get; set; }
        public int Next { get; set; }
        public int Prev { get; set; }
    }

    public class IdentityPermissionPaginationData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public string DescriptionLabel { get; set; }
    }
}
