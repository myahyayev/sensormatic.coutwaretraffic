﻿namespace CountwareTraffic.Services.Reporting.Engine
{
    public enum SummaryCompareType : byte
    {
        Unknown = 1,
        ToLastWeek = 2,
        ToLastMonth = 3,
        ToLastYear = 4,
        ToLastLastYear = 5,
    }
}
