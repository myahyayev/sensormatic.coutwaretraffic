﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetUnknownDevicesHandler : IQueryHandler<GetUnknownDevices, List<UnknownDeviceDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetUnknownDevicesHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<UnknownDeviceDetailsDto>> HandleAsync(GetUnknownDevices query)
        {
            var unknownDevices = await _unitOfWork.GetRepository<IUnknownDeviceRepository>().GetUnusedAsync();

            if (unknownDevices == null) return null;

            return unknownDevices.Select(unknownDevice => new UnknownDeviceDetailsDto
            {
                Id = unknownDevice.Id,
                Name = unknownDevice.Name,
                HttpPort = unknownDevice.HttpPort,
                HttpsPort = unknownDevice.HttpsPort,
                IpAddress = unknownDevice.IpAddress,
                MacAddress = unknownDevice.MacAddress,
                OccurredOn = unknownDevice.OccurredOn,
                SerialNumber = unknownDevice.SerialNumber
            }).ToList();
        }
    }
}
