using Countware.Traffic.CrossCC.Observability;
using Countware.Traffic.CrossCC.PolicyProvider;
using CountwareTraffic.Services.Companies.Grpc;
using CountwareTraffic.Services.Devices.Grpc;
using CountwareTraffic.Services.Events.Grpc;
using CountwareTraffic.Services.Reporting.Grpc;
using CountwareTraffic.Services.Users.Grpc;
using Grpc.Net.Client.Web;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.ApplicationInsights;
using Sensormatic.Tool.Grpc.Client;
using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Text;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;

            Env = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            Sensormatic.Tool.Ioc.IoCGenerator.DoTNet.Current.Start(services, Configuration);

            services.AddApplicationInsights(Configuration)
                    .AddControllers();

            services.AddCors(o =>
            {
                o.AddPolicy("Gateway", builder =>
                {
                    builder.SetIsOriginAllowed((host) => true);
                    builder.AllowAnyMethod();
                    builder.AllowAnyHeader();
                    builder.AllowCredentials();
                });
            });

            if (!Env.IsProduction())
            {
                services.AddSwagger();
            }

            services.AddGrpcServices();
            services.ConfigureAuthService();

            services.AddSingleton<IAuthorizationPolicyProvider, PermissionsPolicyProvider>();
            services.AddSingleton<IAuthorizationHandler, PermissionsAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, RolesAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, ScopesAuthorizationHandler>();

            services.AddTransient<HttpClientAuthorizationDelegatingHandler>();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            if (!env.IsProduction())
            {
                app.UseSwagger()
                   .UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mobile.BFF.CountwareTraffic.HttpAggregator v1"));
            }

            app.UseHttpsRedirection()
               .UseRouting()
               .UseCors("Gateway")
               .UseAuthentication()
               .UseAuthorization()
               .UseGrpcWeb()
               .UserCorrelationId()
               .UseSensormaticExceptionMiddleware()
               .UseEndpoints(endpoints =>
               {
                   endpoints.MapControllers();
                   endpoints.MapGet("/", async context =>
                   {
                       context.Response.ContentType = "text/html;charset=utf-8";
                       await context.Response.WriteAsync($"<h1>{Assembly.GetExecutingAssembly().GetName().Name} v{Assembly.GetExecutingAssembly().GetName().Version.ToString()} is running</h1>");
                   });

               });
        }
    }

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddGrpcServices(this IServiceCollection services)
        {
            var urlsConfig = services.BuildServiceProvider().GetRequiredService<IOptions<UrlsConfig>>().Value;

            #region grpcArea microservice
            services.AddGrpcClient<Subarea.SubareaClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcArea);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
                .RegisterClientInterceptors();


            services.AddGrpcClient<Company.CompanyClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcArea);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });

            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
                .RegisterClientInterceptors();


            services.AddGrpcClient<Area.AreaClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcArea);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
                .RegisterClientInterceptors();


            services.AddGrpcClient<Country.CountryClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcArea);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
                .RegisterClientInterceptors();

            services.AddGrpcClient<Lookup.LookupClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcArea);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
               .RegisterClientInterceptors();

            services.AddGrpcClient<Region.RegionClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcArea);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
              .RegisterClientInterceptors();

            services.AddGrpcClient<AllHierarchy.AllHierarchyClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcArea);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
            .RegisterClientInterceptors();
            #endregion grpcArea microservice


            #region grpcDevice microservice
            services.AddGrpcClient<Device.DeviceClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcDevice);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
                .RegisterClientInterceptors();
            #endregion grpcDevice microservice


            #region grpcUser microservice
            services.AddGrpcClient<User.UserClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcUser);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
            .RegisterClientInterceptors();
            #endregion grpcUser microservice


            #region grpcEvent microservice
            services.AddGrpcClient<UnknownDevice.UnknownDeviceClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcEvent);
                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
               .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
               {
                   HttpVersion = new Version(1, 1)
               })
               .RegisterClientInterceptors();

            services.AddGrpcClient<Traffic.TrafficClient>(options => { options.Address = new Uri(urlsConfig.GrpcEvent); })
              .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
              {
                  HttpVersion = new Version(1, 1)
              })
              .RegisterClientInterceptors();
            #endregion grpcEvent microservice


            #region grpcReport microservice
            services.AddGrpcClient<Report.ReportClient>(options =>
            {
                options.Address = new Uri(urlsConfig.GrpcReport);

                options.ChannelOptionsActions.Add(c =>
                {
                    c.MaxReceiveMessageSize = int.MaxValue;
                    c.MaxSendMessageSize = int.MaxValue;
                });
            })
            .ConfigurePrimaryHttpMessageHandler(() => new GrpcWebHandler(GrpcWebMode.GrpcWebText, new HttpClientHandler())
            {
                HttpVersion = new Version(1, 1)
            })
               .RegisterClientInterceptors();
            #endregion grpcReport microservice

            return services;
        }

        public static void ConfigureAuthService(this IServiceCollection services)
        {
            var authenticationConfig = services.BuildServiceProvider().GetRequiredService<IOptions<AuthenticationConfig>>().Value;

            services
                  .AddAuthentication(options =>
                  {
                      options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                      options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                  })
                   .AddJwtBearer(cfg =>
                   {
                       cfg.RequireHttpsMetadata = true;
                       cfg.SaveToken = true;
                       cfg.TokenValidationParameters = new TokenValidationParameters()
                       {
                           IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authenticationConfig.SignKey)),
                           ValidateAudience = false,
                           ValidateIssuer = false,
                           ValidateLifetime = true,
                           RequireExpirationTime = true,
                           ClockSkew = TimeSpan.Zero,
                           ValidateIssuerSigningKey = true
                       };
                   });

            services.AddAuthorization();
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(swagger =>
            {
                swagger.MapType<TimeSpan>(() => new OpenApiSchema { Type = "string", Example = new OpenApiString("00:00:00") });
                swagger.MapType<TimeSpan?>(() => new OpenApiSchema { Type = "string", Example = new OpenApiString("00:00:00") });
                swagger.SwaggerDoc("v1", new OpenApiInfo { Title = "Mobile.BFF.CountwareTraffic.HttpAggregator", Version = "v1" });

                var xmlCommentsFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlCommentsFile);
                swagger.IncludeXmlComments(xmlPath, true);

                swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });
                swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            new string[] {}
                    }
                });
            });
        }
    }
}
