﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CreateAreaHandler : ICommandHandler<CreateArea>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public CreateAreaHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(CreateArea command)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();
            var regionRepository = _unitOfWork.GetRepository<IRegionRepository>();

            if (!await regionRepository.ExistsAsync(command.RegionId))
                throw new RegionNotFoundException(command.RegionId);

            if (await areaRepository.ExistsAsync(command.Name))
                throw new AreaAlreadyExistsException(command.Name);

            var areaAddress = AreaAddress.Create(command.Street, command.Latitude, command.Longitude);

            var areaContract = AreaContact.Create(command.GsmNumber, command.GsmDialCode, command.GsmCountryCode, command.PhoneNumber, command.PhoneDialCode, command.PhoneCountryCode, command.EmailAddress);

            var area = Area.Create(command.Name, command.Description, command.DistrictId, command.CityId, command.AreaTypeId, command.RegionId, command.ManagerName, command.ManagerId, areaAddress, areaContract, command.WorkingHoursStart, command.WorkingHoursEnd, command.WorkingTimeZone);

            await areaRepository.AddAsync(area);

            var hierarchyData = await regionRepository.GetHierarchyAsync(area.RegionId, _currentTenant.Id);

            area.WhenHierarchyCreated(_currentTenant.Id, hierarchyData.CopmanyId, hierarchyData.CountryId, hierarchyData.RegionId, area.Id, area.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}


