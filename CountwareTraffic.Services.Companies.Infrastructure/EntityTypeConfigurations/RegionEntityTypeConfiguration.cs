﻿using CountwareTraffic.Services.Companies.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class RegionEntityTypeConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.ToTable("Regions", SchemaNames.Areas);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
              .Property(x => x.Description)
              .HasField("_description")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .HasMaxLength(2500);

            builder
             .Property(x => x.ManagerId)
             .HasField("_managerId")
             .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
            .Property(x => x.ManagerName)
            .HasField("_managerName")
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasMaxLength(100);

            builder.HasOne<Country>().WithMany().HasForeignKey(x => x.CountryId);
        }
    }
}
