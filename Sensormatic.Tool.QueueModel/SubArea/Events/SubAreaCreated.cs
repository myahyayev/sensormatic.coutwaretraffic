﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record SubAreaCreated : MessageEnvelope, IQueueEvent
    {
        public SubAreaCreated(Guid subAreaId, string name, Guid areaId, string areaName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid countryLookupId, Guid cityId, string cityName, Guid districtId, string districtName, Guid recordId, Guid userId, string correlationId, Guid tenantId)

             : base(userId, correlationId, tenantId)
        {
            SubAreaId = subAreaId;
            Name = name;
            RecordId = recordId;
            AreaName = areaName;
            AreaId = areaId;
            RegionId = regionId;
            RegionName = regionName;
            CountryId = countryId;
            CountryName = countryName;
            CompanyId = companyId;
            CompanyName = companyName;
            CountryLookupId = countryLookupId;
            CityId = cityId;
            CityName = cityName;
            DistrictId = districtId;
            DistrictName = districtName;
        }
        public Guid SubAreaId { get; }
        public string Name { get; }
        public Guid AreaId { get; }
        public string AreaName { get; }
        public Guid RecordId { get; init; }
        public Guid RegionId { get; }
        public string RegionName { get; }
        public Guid CountryId { get; }
        public string CountryName { get; }
        public Guid CompanyId { get; }
        public string CompanyName { get; }
        public Guid CountryLookupId { get; }
        public Guid CityId { get; }
        public string CityName { get; }
        public Guid DistrictId { get; }
        public string DistrictName { get; }
    }
}
