﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Sensormatic.Tool.Efcore
{
    internal class DeletableValueObjectInterceptor : InterceptorGenerator<IDeletableValueObject>
    {
        public override void OnAfterError(string execptionMessage) { }
        public override void OnAfterInsert() { }
        public override void OnBeforeDelete(IDeletableValueObject item, EntityEntry entityEntry, DbContext dbContext) { }
        public override void OnBeforeInsert(IDeletableValueObject item, EntityEntry entityEntry, DbContext dbContext) { }
        public override void OnBeforeUpdate(IDeletableValueObject item, EntityEntry entityEntry, DbContext dbContext) { }
    }
}
