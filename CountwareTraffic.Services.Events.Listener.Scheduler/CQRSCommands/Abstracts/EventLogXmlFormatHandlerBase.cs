﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public abstract class EventLogXmlFormatHandlerBase<T> where T : class
    {
        protected readonly ILogger<T> _logger;
        protected readonly IQueryDispatcher _queryDispatcher;
        protected readonly ICommandDispatcher _commandDispatcher;
        protected readonly ICache _redisCacheService;
        protected readonly IQueueService _queueService;
        protected readonly IIdentityService _identityService;
        protected readonly IDeviceConnectionStatusProcessorService _deviceConnectionStatusProcessorService;
        protected readonly TimeZoneSettings _timeZoneSettings;
        protected EventLogXmlFormatHandlerBase(ILogger<T> logger, IOptions<TimeZoneSettings> timeZoneSettings, IIdentityService identityService, IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher, ICache redisCacheService, IQueueService queueService, IDeviceConnectionStatusProcessorService deviceConnectionStatusProcessorService)
        {
            _logger = logger;
            _queryDispatcher = queryDispatcher;
            _commandDispatcher = commandDispatcher;
            _redisCacheService = redisCacheService;
            _queueService = queueService;
            _identityService = identityService;
            _timeZoneSettings = timeZoneSettings.Value;
            _deviceConnectionStatusProcessorService = deviceConnectionStatusProcessorService;
        }

        public abstract DeviceDto ParseDeviceEventXml(string xml);
        public virtual async Task ProcessUnknownDeviceEvent(DeviceDto device)
        {
            //if (device.IpAddress.Equals("0.0.0.0"))
            //   return;

            await _commandDispatcher.SendAsync(new CreateUnknownDevice
            {
                //todo:name was setted temproray
                //please refer to CWTR-127 issue for more detailes
                Name = $"{device.BrandName}-{device.ModelName}-{device.FirmWare}-{device.DeviceName?[..Math.Min(100, device.DeviceName.Length)]}",
                HttpPort = device.HttpPort.IsNullOrWhiteSpace() ? 80 : Convert.ToInt32(device.HttpPort),
                HttpsPort = device.HttpsPort.IsNullOrWhiteSpace() ? 443 : Convert.ToInt32(device.HttpsPort),
                IpAddress = device.IpAddress ?? string.Empty,
                MacAddress = device.MacAddress ?? string.Empty,
                SerialNumber = device.SerialNumber ?? string.Empty
            });
        }
        public virtual async Task ProcessDeviceConnectionStatus(Application.DeviceDto device)
        {
            await _deviceConnectionStatusProcessorService?.ProcessDeviceConnectionStatus(device);
        }

        /// <summary>
        ///  It will validate device wether found/active or not
        /// </summary>
        /// <param name="validDeviceFromDb">device got from Database</param>
        /// <param name="clientSentDevice">device usersent</param>
        /// <param name="ShouldProcessUnknownDeviceEvent">it will ProcessUnknownDevice to send notification to users about new device if setted to true</param>
        /// <returns>true</returns>
        /// <exception cref="DeviceNotRegisteredException"></exception>
        /// <exception cref="DeviceIsNotActiveException"></exception>
        public virtual async Task<bool> IsDeviceValid(Application.DeviceDto validDeviceFromDb,DeviceDto clientSentDevice,bool ShouldProcessUnknownDeviceEvent=true)
        {
            if (validDeviceFromDb is null)
            {
                _logger.LogError($"Device not found MacAddress: {clientSentDevice.MacAddress}");
                
                if(ShouldProcessUnknownDeviceEvent)
                    await this.ProcessUnknownDeviceEvent(clientSentDevice);

                throw new DeviceNotRegisteredException("-", clientSentDevice.MacAddress);
            }
            else if (!validDeviceFromDb.IsActive)
            {
                _logger.LogError($"Device :{validDeviceFromDb.Name}-{validDeviceFromDb.Id} might is not in active status!");

                throw new DeviceIsNotActiveException(validDeviceFromDb.Name, validDeviceFromDb.Id);
            }

            return true;
        }
        public virtual bool IsEventAreaWorkingTimeZoneValid(Application.DeviceDto device, DateTime eventStartDateTime, DateTime eventEndDateTime)
        {
            if (device.AreaWorkingTimeZone != null)
            {
                TimeSpan AreaWorkingHoursStartOndevice = device.AreaWorkingHoursStart.GetValueOrDefault();
                TimeSpan AreaWorkingHoursEndOndevice = device.AreaWorkingHoursEnd.GetValueOrDefault();
                if (AreaWorkingHoursStartOndevice == default && AreaWorkingHoursEndOndevice == default)
                {
                    return true;
                }

                TimeZoneInfo timeZoneInfo = null;
                TimeZoneInfo.ClearCachedData();

                if (_timeZoneSettings.ProvideByLinux)
                    timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneExtensions.TimeZoneChangeWindowsToLinux(device.AreaWorkingTimeZone.Value));
                else
                    timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(device.AreaWorkingTimeZone.Value);


                DateTime AreaWorkingHoursStart = eventStartDateTime
                                                .Date
                                                .Add(AreaWorkingHoursStartOndevice);

                DateTime AreaWorkingHoursEnd = eventEndDateTime
                                                .Date
                                                .Add(AreaWorkingHoursEndOndevice);


                AreaWorkingHoursStart = TimeZoneInfo.ConvertTime(AreaWorkingHoursStart, timeZoneInfo, TimeZoneInfo.Utc);

                AreaWorkingHoursEnd = TimeZoneInfo.ConvertTime(AreaWorkingHoursEnd, timeZoneInfo, TimeZoneInfo.Utc);



                if (DateTimeExtensions.HasTimePeriodOverlap(eventStartDateTime, eventEndDateTime, AreaWorkingHoursStart, AreaWorkingHoursEnd))
                {
                    return true;
                }

                _logger.LogWarning($"Device:{device.Name} , {device.Id} [{eventStartDateTime}-{eventEndDateTime}] UTC event isn't in [{AreaWorkingHoursStart}-{AreaWorkingHoursEnd}] UTC AreaWorkingHours range,so event skipped!");
            }
            else
            {

                _logger.LogWarning($"Device:{device.Name} , {device.Id} located AreaWorkingTimeZone is null so event skipped!");
            }

            return false;
        }
    }
}

