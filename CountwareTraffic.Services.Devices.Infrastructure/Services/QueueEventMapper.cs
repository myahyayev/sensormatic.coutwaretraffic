﻿using CountwareTraffic.Services.Devices.Application;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class QueueEventMapper : IQueueEventMapper
    {
        public List<Sensormatic.Tool.Queue.IQueueEvent> MapAll(IEnumerable<Sensormatic.Tool.Efcore.IDomainEvent> events, Guid userId, string correlationId, Guid tenantId)
             => events.Select(e => Map(e, userId, correlationId, tenantId)).ToList();

        public Sensormatic.Tool.Queue.IQueueEvent Map(Sensormatic.Tool.Efcore.IDomainEvent @event, Guid userId, string correlationId, Guid tenantId)
        {
            switch (@event)
            {
                case Core.DeviceCreated e:
                    return new Sensormatic.Tool.QueueModel.DeviceCreated(
                        e.DeviceId,
                        e.Name,
                        e.MacAddress,
                        e.IpAddress,
                        e.SubAreaId,
                        e.SubAreaName,
                        e.AreaId,
                        e.AreaName,
                        e.RegionId,
                        e.RegionName,
                        e.CountryId,
                        e.CountryName,
                        e.CompanyId,
                        e.CompanyName,
                        e.CountryLookupId,
                        e.CityId,
                        e.CityName,
                        e.DistrictId,
                        e.DistrictName,
                        e.ControlFrequency,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );


                case Core.DeviceChanged e:
                    return new Sensormatic.Tool.QueueModel.DeviceChanged(
                        e.DeviceId,
                        e.Name,
                        e.MacAddress,
                        e.IpAddress,
                        e.IsActive,
                        e.OldName,
                        e.ControlFrequency,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );


                case Core.DeviceDeleted e:
                    return new Sensormatic.Tool.QueueModel.DeviceDeleted(
                        e.DeviceId,
                        e.Name,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );


                case Core.DeviceHierarchyDeleted e:
                    return new Sensormatic.Tool.QueueModel.DeviceHDeleted(
                        e.DeviceId,
                        e.DeviceName,
                        e.SubAreaId,
                        e.AreaId,
                        e.RegionId,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.DeviceHierarchyUpdated e:
                    return new Sensormatic.Tool.QueueModel.DeviceHUpdated(
                        e.DeviceId,
                        e.DeviceName,
                        e.SubAreaId,
                        e.AreaId,
                        e.RegionId,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.DeviceHierarchyCreated e:
                    return new Sensormatic.Tool.QueueModel.DeviceHCreated(
                        e.DeviceId,
                        e.DeviceName,
                        e.SubAreaId,
                        e.AreaId,
                        e.RegionId,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );
            }

            return null;
        }
        public void Dispose() => GC.SuppressFinalize(this);
    }
}
