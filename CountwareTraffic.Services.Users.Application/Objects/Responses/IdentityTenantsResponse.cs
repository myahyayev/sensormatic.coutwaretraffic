﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityTenantsResponse
    {
        public List<Tenant> Tenants { get; set; }
    }

    public class Tenant
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
