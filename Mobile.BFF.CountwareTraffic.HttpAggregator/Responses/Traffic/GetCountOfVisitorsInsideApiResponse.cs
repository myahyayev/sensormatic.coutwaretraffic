﻿namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetCountOfVisitorsInsideApiResponse
    {
        public int CountOfVisitorsInside { get; set; }
    }
}
