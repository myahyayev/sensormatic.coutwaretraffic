﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class ElasticHierarchyChangedHandler : ICommandHandler<ElasticHierarchyChanged>
    {
        private readonly IHubContext<ElasticHierarchyNotificationsHub, IElasticHierarchyNotificationsHub> _hubContext;
        private readonly ILogger<ElasticHierarchyChangedHandler> _logger;
        public ElasticHierarchyChangedHandler(IHubContext<ElasticHierarchyNotificationsHub, IElasticHierarchyNotificationsHub> hubContext, ILogger<ElasticHierarchyChangedHandler> logger)
        {
            _hubContext = hubContext;
            _logger = logger;
        }


        public async Task HandleAsync(ElasticHierarchyChanged command)
        {
            _logger.LogInformation($"ElasticHierarchyChangedHandler : {command.TenantId}");

            await _hubContext.Clients
                                       .Groups(command.TenantId.ToString())
                                       .ElasticHierarchiesChangedReceived(command);
        }
    }
}


