﻿using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Core
{
    public interface ICountryRepository : IRepository<Country>, IScopedDependency
    {
        Task<Country> GetAsync(Guid id);
        Task<bool> ExistsAsync(string name,Guid companyId);

        Task<Country> GetAsync(string name, Guid companyId);
        Task<QueryablePagingValue<Country>> GetAllAsync(Guid companyId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts);
        Task<bool> ExistsAsync(Guid id);
        Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName);
        Task<HierarchyCteData> GetHierarchyAsync(Guid id, Guid tenantId);
        Task<List<KeyValue>> GetAllKeyValueAsync(Guid parentId);
    }
}
