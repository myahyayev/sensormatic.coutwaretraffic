﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public enum UserType
    {
        SuperAdmin = 0,
        Admin = 1,
        ClientAdmin = 2,
        User = 3
    }
}
