﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class AddCompanyApiRequest : SensormaticRequestValidate
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string GsmNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string FaxNumber { get; set; }

        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"Description invalid format", nameof(Description)));

            if (!EmailAddress.IsNullOrWhiteSpace() && (!EmailValidator.EmailIsValid(EmailAddress) || EmailAddress.Length > 120))
                ValidateResults.Add(new ErrorResult($"Email address invalid format", nameof(EmailAddress)));

            if (!City.IsNullOrWhiteSpace() && City.Length > 100)
                ValidateResults.Add(new ErrorResult($"City invalid format", nameof(City)));

            if (!Street.IsNullOrWhiteSpace() && Street.Length > 250)
                ValidateResults.Add(new ErrorResult($"Street invalid format", nameof(Street)));

            if (!State.IsNullOrWhiteSpace() && State.Length > 250)
                ValidateResults.Add(new ErrorResult($"State invalid format", nameof(State)));

            if (!Country.IsNullOrWhiteSpace() && Country.Length > 100)
                ValidateResults.Add(new ErrorResult($"Country invalid format", nameof(Country)));

            if (!ZipCode.IsNullOrWhiteSpace() && ZipCode.Length > 20)
                ValidateResults.Add(new ErrorResult($"ZipCode invalid format", nameof(ZipCode)));

            if (!PhoneNumber.IsNullOrWhiteSpace() && PhoneNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"PhoneNumber invalid format", nameof(PhoneNumber)));

            if (!GsmNumber.IsNullOrWhiteSpace() && GsmNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"GsmNumber invalid format", nameof(GsmNumber)));

            if (!FaxNumber.IsNullOrWhiteSpace() && FaxNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"FaxNumber invalid format", nameof(FaxNumber)));
        }
    }
}
