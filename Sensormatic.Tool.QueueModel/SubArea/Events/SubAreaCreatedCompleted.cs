﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record SubAreaCreatedCompleted : MessageEnvelope, IQueueEvent
    {
        public SubAreaCreatedCompleted(Guid subAreaId, Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            SubAreaId = subAreaId;
        }
        public Guid SubAreaId { get; }
        public Guid RecordId { get; init; }
    }
}
