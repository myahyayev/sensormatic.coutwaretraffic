﻿using Countware.Traffic.CrossCC.Observability;
using Grpc.Core;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Grpc.Common;
using Sensormatic.Tool.Ioc;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Sensormatic.Tool.Common;
using CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Delegates;
using CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Extensions;

namespace CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc
{
    public class AsyncUnaryCallHandler : ITransientSelfDependency
    {
        private static string HasClientsideLog = "HasClientsideLog";

        public async Task<TResponse> CallMethodAsync<TRequest, TResponse>(UnaryCallgRPCServiceMethodHandler<TRequest, TResponse> asyncUnaryCallHandler, TRequest request, Metadata metadata = null, DateTime? deadline = null, CancellationToken cancellationToken = default, bool hasClientSideLog = false)
            where TRequest : class
            where TResponse : class
        {
            try
            {
                if (metadata == null)
                    metadata = new Metadata();

                if (hasClientSideLog)
                    metadata.Add(HasClientsideLog, "true");

                return await asyncUnaryCallHandler?.Invoke(request, metadata, deadline, cancellationToken).ResponseAsync;

            }
            catch (Exception ex)
            {
                if (GrpcExceptionHandler.TryGetErrorModel(ex, out ErrorModel model))
                    model.ThrowClientGrpcxception();
                throw;
            }
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
