﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries
{
    public class CheckContracts : IQuery<List<IdentityCheckContractsResponse>>
    {
        public string Language { get; set; }
    }
}