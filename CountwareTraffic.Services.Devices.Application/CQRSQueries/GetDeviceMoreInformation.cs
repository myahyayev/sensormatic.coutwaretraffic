﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class GetDeviceMoreInformation : IQuery<DeviceMoreInformationDto>
    {
        public Guid DeviceId { get; set; }
    }
}