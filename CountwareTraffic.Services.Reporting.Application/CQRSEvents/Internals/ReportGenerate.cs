﻿using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportGenerate : Convey.CQRS.Events.IEvent
    {
        public Guid ReportId { get; set; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
        public string CorrelationId { get; set; }
        public string UserName { get; set; }
    }
}

