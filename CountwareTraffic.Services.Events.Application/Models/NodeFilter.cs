﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class NodeFilter
    {
        public Guid Id { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }

    public enum HierarchyLevel : byte
    {
        Tenant = 1,
        Company = 2,
        Country = 3,
        Region = 4,
        Area = 5,
        SubArea = 6,
        Device = 7,
    }
}
