﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class UserNameExistsApiResponse
    {
        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("errors")]
        public List<UserNameExistsApiResponseError> Errors { get; set; }
    }

    public class UserNameExistsApiResponseError
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
