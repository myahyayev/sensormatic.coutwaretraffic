﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetRoleId : IQuery<IdentityRoleIdResponse>
    {
        public Guid Id { get; set; }
        public string Language { get; set; }
    }
}
