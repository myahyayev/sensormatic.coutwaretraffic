﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Devices.Grpc
{
    [ServiceLog]
    public sealed partial class GetHierarchyRequest : SensormaticRequestValidate
    {
        internal Guid _Id
        {
            get
            {
                if (Guid.TryParse(id_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.id_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_Id == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1506 Id cannot be null", nameof(_Id)));
        }
    }
}
