﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class AreaWorkingHours : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AreaStatus",
                schema: "areas",
                table: "Areas",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "WorkingHoursEnd",
                schema: "areas",
                table: "Areas",
                type: "time",
                nullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "WorkingHoursStart",
                schema: "areas",
                table: "Areas",
                type: "time",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AreaStatuses",
                schema: "areas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaStatuses", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Areas_AreaStatus",
                schema: "areas",
                table: "Areas",
                column: "AreaStatus");

            migrationBuilder.Sql(@"INSERT INTO [areas].[AreaStatuses] ( Id, Name ) VALUES
                                                       (1, 'Created'), (2, 'Completed'), (3, 'Rejected')");

            migrationBuilder.AddForeignKey(
                name: "FK_Areas_AreaStatuses_AreaStatus",
                schema: "areas",
                table: "Areas",
                column: "AreaStatus",
                principalSchema: "areas",
                principalTable: "AreaStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Areas_AreaStatuses_AreaStatus",
                schema: "areas",
                table: "Areas");

            migrationBuilder.DropTable(
                name: "AreaStatuses",
                schema: "areas");

            migrationBuilder.DropIndex(
                name: "IX_Areas_AreaStatus",
                schema: "areas",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "AreaStatus",
                schema: "areas",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "WorkingHoursEnd",
                schema: "areas",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "WorkingHoursStart",
                schema: "areas",
                table: "Areas");
        }
    }
}
