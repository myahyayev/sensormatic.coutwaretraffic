﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class DevicesController : BaseController
    {
        private readonly DeviceService _deviceService;
        private readonly UnknownDeviceService _unknownDeviceService;
        private readonly UserService _userService;
        
        public DevicesController(IServiceProvider provider, ILogger<DevicesController> logger, IHttpContextAccessor contextAccessor, DeviceService deviceService, UnknownDeviceService unknownDeviceService, UserService userService)
            : base(provider, logger, contextAccessor) 
        {
            _deviceService = deviceService;
            _unknownDeviceService = unknownDeviceService;
            _userService = userService;

        }


        [HttpGet("{deviceId}/detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceView, Permissions.DeviceView })]
        public async Task<ApiResponse<GetDeviceDetails>> GetDeviceDetailAsync(Guid deviceId)
            => Response(await _deviceService.GetDeviceByIdAsync(deviceId), new ResultMessage("#S1074"));


        [HttpPost("{subAreaId}/details"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceList })]
        public async Task<ApiPagedResponse<GetDeviceDetails>> GetDeviceDetailsAsync(Guid subAreaId, [FromBody] DataSourceApiRequest paging)
            => await _deviceService.GetDevicesAsync(subAreaId, paging);


        [HttpPost("filter/details")]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceList, Permissions.DeviceList })]
        public async Task<ApiPagedResponse<GetDeviceDetails>> GetDeviceDetailsAsync([FromBody] GetDeviceFilterApiRequest request)
        {
            request.HierarchyTreeSelectFilters = await AreaControl(request.HierarchyTreeSelectFilters);

            var response = await _deviceService.GetDevicesAsync(request);

            return new ApiPagedResponse<GetDeviceDetails>(response.Data, response.TotalCount, response.Page, response.Limit, response.HasNextPage);
        }


        [HttpPost("details")]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceList, Permissions.DeviceList })]
        public async Task<ApiPagedResponse<GetDeviceDetails>> GetDeviceDetailsAsync([FromBody] DataSourceApiRequest paging)
        {
            var response = await _deviceService.GetDevicesAsync(paging);

            return new ApiPagedResponse<GetDeviceDetails>(response.Data, response.TotalCount, response.Page, response.Limit, response.HasNextPage);
        }

        [HttpGet("statuses"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceList, Permissions.DeviceList })]
        public async Task<ApiResponse<IEnumerable<GetDeviceStatus>>> GetDeviceStatusesAsync()
        {
            var response = await _deviceService.GetDeviceStatusesAsync();

            return Response(response, new ResultMessage("#S1075"));
        }

        [HttpGet("types"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceList, Permissions.DeviceList })]
        public async Task<ApiResponse<IEnumerable<GetDeviceType>>> GetDeviceTypesAsync()
            => Response(await _deviceService.GetDeviceTypesAsync(), new ResultMessage("#S1076"));


        [HttpPost("{subAreaId}/add"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceAdd })]
        public async Task<ApiResponse<CrudApiResponse>> AddDeviceAsync(Guid subAreaId, [FromForm] AddDeviceApiRequest request)
        {
            await _deviceService.AddDeviceAsync(subAreaId, request);

            if (request.UnknownDeviceId.HasValue)
            {
                await _unknownDeviceService.UnknownDeviceProcessedAsync(request.UnknownDeviceId.Value);
            }

            return Response(new CrudApiResponse {  IsSuccess = true, OperationType = OperationType.Create }, new ResultMessage("#S1077"));
        }


        [HttpPut("{deviceId}/change"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceEdit })]
        public async Task<ApiResponse<CrudApiResponse>> ChangeDeviceAsync(Guid deviceId, [FromForm] ChangeDeviceApiRequest request)
        {
            await _deviceService.ChangeDeviceAsync(deviceId, request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Create }, new ResultMessage("#S1078"));
        }


        [HttpDelete("{deviceId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceDelete })]
        public async Task<ApiResponse<CrudApiResponse>> DeleteCountryAsync(Guid deviceId)
        {
            await _deviceService.DeleteDeviceAsync(deviceId);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Delete }, new ResultMessage("#S1079"));
        }


        [HttpGet("{deviceId}/more-information"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceView })]
        public async Task<ApiResponse<GetDeviceMoreInformation>> GetDeviceMoreInformationAsync(Guid deviceId)
            => Response(await _deviceService.GetDeviceMoreInformationAsync(deviceId), new ResultMessage("#S1080"));


        private async Task<List<HierarchyTreeSelectFilterRequest>> AreaControl(List<HierarchyTreeSelectFilterRequest> hierarchyTreeSelectFilters)
        {
            var user = await GetUserArea();
            if (!(user.UserType == (int)UserType.SuperAdmin) && !(user.UserType == (int)UserType.Admin))
            {
                if (hierarchyTreeSelectFilters == null || hierarchyTreeSelectFilters.Count == 0)
                {
                    hierarchyTreeSelectFilters = new List<HierarchyTreeSelectFilterRequest>();
                    user.Areas.ForEach(area =>
                        hierarchyTreeSelectFilters.Add(new HierarchyTreeSelectFilterRequest { Id = area.Id, HierarchyLevel = (HierarchyLevel)area.Level })
                    );
                }
            }
            return hierarchyTreeSelectFilters;
        }

        private async Task<GetUserIdApiResponse> GetUserArea()
        {
            Guid userId = UserExtensions.GetUserId((ClaimsIdentity)User.Identity);
            var user = await _userService.GetUserIdAsync(userId);
            return user;
        }
    }
}
