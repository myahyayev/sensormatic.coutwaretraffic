﻿using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class DistrictHierarchyData
    {
        public Guid DistrictId { get; set; }
		public string DistrictName { get; set; }
		public Guid CityId { get; set; }
		public string CityName { get; set; }
		public Guid CountryId { get; set; }
		public string CountryName { get; set; }
	}
}
