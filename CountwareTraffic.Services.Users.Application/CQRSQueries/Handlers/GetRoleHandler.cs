﻿using Convey.CQRS.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetRoleHandler : IQueryHandler<GetRole, List<IdentityRolesResponse>>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetRoleHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<List<IdentityRolesResponse>> HandleAsync(GetRole query)
        {
            return await _identityServerService.GetRoleAsync();
        }
    }
}
