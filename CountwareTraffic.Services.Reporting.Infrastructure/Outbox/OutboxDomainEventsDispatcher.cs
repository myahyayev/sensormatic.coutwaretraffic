﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Infrastructure;
using Newtonsoft.Json;
using Sensormatic.Tool.Efcore;
using Sensormatic.Tool.Ioc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public interface IOutboxIDomainEventsDispatcher : IScopedDependency
    {
        Task DispatchEventsAsync();
    }

    public class OutboxDomainEventsDispatcher : IOutboxIDomainEventsDispatcher
    {
        private readonly ReportDbContext _deviceDbContext;
        private readonly IQueueEventMapper _queueEventMapper;
        private readonly IIdentityService _identityService;
        private readonly ICorrelationService _correlationService;
        private readonly ICurrentTenant _currentTenant;
        public OutboxDomainEventsDispatcher(ReportDbContext reportDbContext, IQueueEventMapper queueEventMapper, IIdentityService identityService, ICorrelationService correlationService, ICurrentTenant currentTenant)
        {
            _deviceDbContext = reportDbContext;
            _queueEventMapper = queueEventMapper;
            _identityService = identityService;
            _correlationService = correlationService;
            _currentTenant = currentTenant;
        }

        public Task DispatchEventsAsync()
        {
            var domainEntities = _deviceDbContext.ChangeTracker
                .Entries<IDomainEventRaisable>()
                .Where(x => x.Entity.Events != null && x.Entity.Events.Any()).ToList();

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.Events)
                .ToList();

            if (domainEvents.Count > 0)
            {
                var mappedEvents = _queueEventMapper.MapAll(domainEvents, _identityService.UserName, _identityService.UserId, _correlationService.CorrelationId, _currentTenant.Id);

                foreach (var queueEvent in mappedEvents)
                {
                    OutboxMessage outboxMessage = new(DateTime.Now, queueEvent.GetType().FullName, JsonConvert.SerializeObject(queueEvent), queueEvent.RecordId);
                    _deviceDbContext.OutboxMessages.Add(outboxMessage);
                }
            }

            return Task.CompletedTask;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
