﻿using CountwareTraffic.Services.Events.Application;
using Newtonsoft.Json;
using Sensormatic.Tool.Efcore;
using Sensormatic.Tool.Ioc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public interface IOutboxIDomainEventsDispatcher : IScopedDependency
    {
        Task DispatchEventsAsync();
    }

    public class OutboxDomainEventsDispatcher : IOutboxIDomainEventsDispatcher
    {
        private readonly EventDbContext _eventDbContext;
        private readonly IQueueEventMapper _queueEventMapper;
        private readonly IIdentityService _identityService;
        private readonly ICorrelationService _correlationService;
        public OutboxDomainEventsDispatcher(EventDbContext deviceDbContext, IQueueEventMapper queueEventMapper, IIdentityService identityService, ICorrelationService correlationService)
        {
            _eventDbContext = deviceDbContext;
            _queueEventMapper = queueEventMapper;
            _identityService = identityService;
            _correlationService = correlationService;
        }

        public Task DispatchEventsAsync()
        {
            var domainEntities = _eventDbContext.ChangeTracker
                .Entries<IDomainEventRaisable>()
                .Where(x => x.Entity.Events != null && x.Entity.Events.Any()).ToList();

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.Events)
                .ToList();

            if (domainEntities.Count > 0)
            {
                var mappedEvents = _queueEventMapper.MapAll(domainEvents, _identityService.UserId, _correlationService.CorrelationId);

                foreach (var queueEvent in mappedEvents)
                {
                    OutboxMessage outboxMessage = new(DateTime.Now, queueEvent.GetType().FullName, JsonConvert.SerializeObject(queueEvent), queueEvent.RecordId);
                    _eventDbContext.OutboxMessages.Add(outboxMessage);
                }
            }

            return Task.CompletedTask;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
