﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetGetReportByIdApiResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TemplateName { get; set; }
        public int TemplateTypeId { get; set; }
        public string TemplateTypeName { get; set; }
        public int ReportFormatId { get; set; }
        public string ReportFormatName { get; set; }
        public int ReportStatusId { get; set; }
        public string ReportStatusName { get; set; }
        public bool IsActive { get; set; }
        public List<string> Locations { get; set; }
        public bool NotifyWhenComplete { get; set; }
        public string EmailTo { get; set; }
        public string EmailBody { get; set; }
        public string EmailSubject { get; set; }
        public Guid LanguageId { get; set; }
        public string LanguageName { get; set; }
        public Guid ScheduleId { get; set; }
        public int ScheduleFrequencyId { get; set; }
        public string ScheduleFrequencyName { get; set; }
        public int ScheduleDeliveryTimeId { get; set; }
        public string ScheduleDeliveryTimeName { get; set; }
        public string AtSetTime { get; set; }
        public string AtSetTimeZone { get; set; }
        public string RepeatOn { get; set; }
        public DateTime? StartsOn { get; set; }
        public DateTime? EndsOn { get; set; }
        public DateTime DateRangeEndDate { get; set; }
        public DateTime DateRangeStartDate { get; set; }
        public Guid TemplateId { get; set; }

        public List<ReportFile> ReportFiles { get; set; }
    }
}
