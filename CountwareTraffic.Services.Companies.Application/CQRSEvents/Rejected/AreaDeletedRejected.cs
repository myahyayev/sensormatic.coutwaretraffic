﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public record AreaDeletedRejected : Convey.CQRS.Events.IEvent
    {
        public Guid AreaId { get; set; }
        public Guid RecordId { get; set; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
    }
}
