﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Settings.Core
{
    public class NoteCreated : IDomainEvent
    {
        public Guid TenantId { get; init; }
        public bool IsAffectToReport { get; init; }
        public bool IsVisible { get; init; }
        public DateTime BeginDate { get; init; }
        public DateTime EndDate { get; init; }
        public string Subject { get; init; }
        public string Content { get; init; }
        public Guid RecordId { get; init; }
        public DateTime CreateDate { get; init; }
        public DateTime ModifiedDate { get; init; }
        public Guid CreateBy { get; init; }
        public Guid ModifiedBy { get; init; }

        public NoteCreated(string subject, string content, DateTime begindate, DateTime endDate, bool isAffectToReport, bool isVisible, Guid tenantId, DateTime createdDate, Guid createdBy, DateTime modifiedDate, Guid modifiedBy)
        {
            TenantId = tenantId;
            IsAffectToReport = isAffectToReport;
            IsVisible = isVisible;
            BeginDate = begindate;
            EndDate = endDate;
            Subject = subject;
            Content = content;
            CreateDate = createdDate;
            ModifiedDate = modifiedDate;
            CreateBy = createdBy;
            ModifiedBy = modifiedBy;
            RecordId = Guid.NewGuid();
        }
    }
}
