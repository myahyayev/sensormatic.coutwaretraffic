﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class BrandAlreadyExistsException : AppException
    {
        public string BrandName { get; }

        public BrandAlreadyExistsException(string name)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1550 Brand with name: {name} already exists.") }, 409, ResponseMessageType.Error)
        {
            BrandName = name;
        }
    }
}
