﻿using CountwareTraffic.Services.Users.Application.Objects;
using CountwareTraffic.Services.Users.Application.Objects.Requests;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityServerService : IIdentityServerService
    {
        private readonly ILogger<IdentityServerService> _logger;
        private readonly IOptions<AuthenticationConfig> _authConfig;
        private readonly HttpClient _httpClient;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public IdentityServerService(ILogger<IdentityServerService> logger, IOptions<AuthenticationConfig> authConfig, IHttpClientFactory clientFactory, IHttpContextAccessor httpContextAccessor)
        {
            _logger = logger;
            _authConfig = authConfig;
            _httpClient = clientFactory.CreateClient();
            _httpContextAccessor = httpContextAccessor;
        }
        public void Dispose() => GC.SuppressFinalize(this);
        public async Task<IdentityTenantsResponse> GetTenantsAsync()
        {

            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/tenant";

            var response = await _httpClient.GetAsync(requestUri);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"Token Result: {jsonResult}  RequestUri: {requestUri}");


            var tenants = TextJsonExtensions.Deserialize<List<Tenant>>(jsonResult);

            return new IdentityTenantsResponse
            {
                Tenants = tenants
            };
        }

        public async Task<IdentityTokenResponse> GetTokenAsync(IdentityTokenRequest tokenRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Account";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(tokenRequest);

            request.Content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"Token Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityTokenResponse>(jsonResult);

        }

        public async Task<IdentityResetPasswordWithTokenResponse> ResetPasswordWithTokenAsync(IdentityResetPasswordWithTokenRequest resetPasswordWithTokenReuqest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/ResetPasswordWithToken";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(resetPasswordWithTokenReuqest);

            request.Content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"ResetPasswordWithToken Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityResetPasswordWithTokenResponse>(await response.Content.ReadAsStringAsync());

        }

        public async Task<IdentityRefreshTokenResponse> RefreshTokenAsync(IdentityRefreshTokenRequest refreshTokenRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Account/refresh-token";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(refreshTokenRequest);

            request.Content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"RefreshToken Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityRefreshTokenResponse>(await response.Content.ReadAsStringAsync());
        }

        public async Task<IdentityChangePasswordResponse> ChangePasswordAsync(IdentityChangePasswordRequest changePasswordRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/ChangePassword";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(changePasswordRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"ResetPasswordWithToken Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityChangePasswordResponse>(jsonResult);

        }

        public async Task<IdentityUserGridResponse> GetUserGridAsync(IdentityUserGridRequest identityUserGridRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/Grid";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityUserGridRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetUserGridAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUserGridResponse>(jsonResult);
        }

        public async Task<IdentityUserResponse> GetUserAsync()
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User";

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetUser Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<List<IdentityUserResponse>>(jsonResult).FirstOrDefault();
        }

        public async Task<IdentityCreateUserResponse> CreateUserAsync(IdentityCreateUserRequest identityCreateUserRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityCreateUserRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"CreateUserAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityCreateUserResponse>(jsonResult);
        }
        public async Task<IdentityUpdateUserResponse> UpdateUserAsync(IdentityUpdateUserRequest identityUpdateUserRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User";

            HttpRequestMessage request = new(HttpMethod.Put, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityUpdateUserRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"CreateUserAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUpdateUserResponse>(jsonResult);
        }

        public async Task<IdentityGetUserIdResponse> GetUserIdAsync(Guid id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/" + id;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetUser Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityGetUserIdResponse>(jsonResult);
        }
        public async Task<IdentityDeleteUserResponse> DeleteUserAsync(Guid id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/" + id;

            HttpRequestMessage request = new(HttpMethod.Delete, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"DeleteUserAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityDeleteUserResponse>(jsonResult);
        }
        public async Task<IdentityUserResponse> GetUserUserNameAsync(string userName)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/userName/" + userName;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetUser Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUserResponse>(jsonResult);
        }

        public async Task<IdentityUpdateUserResponse> CreateUserWithDefaultPasswordAsync(IdentityUpdateUserRequest identityUpdateUserRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/CreateUserWithDefaultPassword";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityUpdateUserRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"CreateUserWithDefaultPasswordAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUpdateUserResponse>(jsonResult);
        }

        public async Task<IdentityUserNameExistResponse> GetUserNameExistAsync(string userName)
        {

            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/UserNameExist/" + userName;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetUserNameExistAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUserNameExistResponse>(jsonResult);
        }

        public async Task<IdentityUserNameExistResponse> GetEmailExistAsync(string email)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/EmailExist/" + email;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetEmailExistAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUserNameExistResponse>(jsonResult);
        }

        public async Task<IdentityResetPasswordResponse> ResetPasswordAsync(string userName)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/ResetPassword";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(userName);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"ResetPasswordAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityResetPasswordResponse>(jsonResult);

        }

        public async Task<IdentityUserChangeStatusResponse> GetUserChangeStatusAsync(string id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/UserChangeStatus/" + id;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetUserChangeStatusAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUserChangeStatusResponse>(jsonResult);
        }

        public async Task<bool> GetEmailExistsAsync(string email)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/EmailExist/" + email;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetEmailExistsAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<bool>(jsonResult);
        }
        public async Task<bool> GetUserAvatarAsync(string id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/Avatar/" + id;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetUserAvatarAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<bool>(jsonResult);
        }

        public async Task<bool> UserResetAsync(string id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/reset/" + id;

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(id);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"UserResetAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<bool>(jsonResult);

        }

        public async Task<IdentityTenantGroupPaginationResponse> GetTenantGroupPaginationAsync(IdentityTenantGroupPaginationRequest identityTenantGroupPaginationRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/TenantGroup/Pagination";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityTenantGroupPaginationRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetTenantGroupPaginationAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityTenantGroupPaginationResponse>(jsonResult);
        }

        public async Task<IdentityTenantGroupResponse> GetTenantGroupAsync()
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/TenantGroup";

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetTenantGroupAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityTenantGroupResponse>(jsonResult);
        }

        public async Task<IdentityCreateTenantGroupResponse> CreateTenantGroupAsync(IdentityCreateTenantGroupRequest identityCreateTenantGroupRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/TenantGroup";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityCreateTenantGroupRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"CreateTenantGroupAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityCreateTenantGroupResponse>(jsonResult);
        }

        public async Task<IdentityUpdateTenantGroupResponse> UpdateTenantGroupAsync(IdentityUpdateTenantGroupRequest identityUpdateTenantGroupRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/TenantGroup";

            HttpRequestMessage request = new(HttpMethod.Put, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityUpdateTenantGroupRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"UpdateTenantGroupAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUpdateTenantGroupResponse>(jsonResult);
        }

        public async Task<IdentityDeleteTenantGroupResponse> DeleteTenantGroupAsync(Guid id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/TenantGroup/" + id;

            HttpRequestMessage request = new(HttpMethod.Delete, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"DeleteTenantGroupAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityDeleteTenantGroupResponse>(jsonResult);
        }

        public async Task<IdentityTenantGroupIdResponse> GetTenantGroupAsync(Guid id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/TenantGroup/" + id;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetTenantGroupAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityTenantGroupIdResponse>(jsonResult);
        }

        public async Task<IdentityTenantGroupResponse> GetTenantGroupSelectAsync()
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/TenantGroup/Select";

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetTenantGroupSelectAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityTenantGroupResponse>(jsonResult);
        }

        public async Task<IdentityCreateTenantResponse> CreateTenantAsync(IdentityCreateTenantRequest identityCreateTenantRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Tenant";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityCreateTenantRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"CreateTenantAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityCreateTenantResponse>(jsonResult);
        }

        public async Task<IdentityUpdateTenantResponse> UpdateTenantAsync(IdentityUpdateTenantRequest identityUpdateTenantRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Tenant";

            HttpRequestMessage request = new(HttpMethod.Put, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityUpdateTenantRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"UpdateTenantAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUpdateTenantResponse>(jsonResult);
        }

        public async Task<IdentityTenantSelectResponse> GetTenantSelectAsync()
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Tenant/Select";

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetTenantSelectAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityTenantSelectResponse>(jsonResult);
        }

        public async Task<IdentityRolePaginationResponse> GetRolePaginationAsync(IdentityRolePaginationRequest identityRolePaginationRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Role/Pagination";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityRolePaginationRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            request.Headers.AcceptLanguage.Add(new StringWithQualityHeaderValue(identityRolePaginationRequest.Language));
            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetRolePaginationAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityRolePaginationResponse>(jsonResult);
        }

        public async Task<List<IdentityRolesResponse>> GetRoleAsync()
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Role";

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetRoleAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<List<IdentityRolesResponse>>(jsonResult);

        }

        public async Task<IdentityCreateRoleResponse> CreateRoleAsync(IdentityCreateRoleRequest identityCreateRoleRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Role";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityCreateRoleRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"CreateRoleAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityCreateRoleResponse>(jsonResult);
        }

        public async Task<IdentityUpdateRoleResponse> UpdateRoleAsync(IdentityUpdateRoleRequest identityUpdateRoleRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Role";

            HttpRequestMessage request = new(HttpMethod.Put, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityUpdateRoleRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"UpdateRoleAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUpdateRoleResponse>(jsonResult);
        }

        public async Task<IdentityRoleIdResponse> GetRoleIdAsync(Guid id, string language)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Role/" + id;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            request.Headers.AcceptLanguage.Add(new StringWithQualityHeaderValue(language));

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetRoleIdAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityRoleIdResponse>(jsonResult);
        }

        public async Task<IdentityDeleteRoleResponse> DeleteRoleAsync(Guid id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Role/" + id;

            HttpRequestMessage request = new(HttpMethod.Delete, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"DeleteRoleAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityDeleteRoleResponse>(jsonResult);
        }

        public async Task<IdentityAddRoleToUserResponse> AddRoleToUserAsync(Guid userId, string roleName)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/AddRoleToUser/" + userId + "/" + roleName;

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(roleName);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"AddRoleToUserAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityAddRoleToUserResponse>(jsonResult);
        }

        public async Task<IdentityRemoveRoleFromUserResponse> RemoveRoleFromUserAsync(Guid userId, string roleName)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/RemoveRoleFromUser/" + userId + "/" + roleName;

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(roleName);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"RemoveRoleFromUserAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityRemoveRoleFromUserResponse>(jsonResult);
        }

        public async Task<IdentityAssignRoleToUserResponse> AssignRolesToUserAsync(string userName)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/AssignRolesToUser/" + userName;

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(userName);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"AssignRolesToUserAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityAssignRoleToUserResponse>(jsonResult);
        }

        public async Task<IdentityRoleSelectResponse> GetRoleSelectAsync()
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Role/Select";

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetTenantSelectAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityRoleSelectResponse>(jsonResult);
        }

        public async Task<IdentityPermissionPaginationResponse> GetPermissionPaginationAsync(IdentityPermissionPaginationRequest identityPermissionPaginationRequest, string language)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission/Pagination";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityPermissionPaginationRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            request.Headers.AcceptLanguage.Add(new StringWithQualityHeaderValue(language));

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetPermissionPaginationAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityPermissionPaginationResponse>(jsonResult);
        }

        public async Task<IdentityPermissionResponse> GetPermissionAsync(Guid id, string language)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission/" + id;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            request.Headers.AcceptLanguage.Add(new StringWithQualityHeaderValue(language));

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetPermissionAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityPermissionResponse>(jsonResult);
        }

        public async Task<IdentityDeletePermissionResponse> DeletePermissionAsync(Guid id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission/" + id;

            HttpRequestMessage request = new(HttpMethod.Delete, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"DeletePermissionAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityDeletePermissionResponse>(jsonResult);
        }

        public async Task<IdentityCreatePermissionResponse> CreatePermissionAsync(IdentityCreatePermissionsRequest identityCreatePermissionsRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityCreatePermissionsRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"CreatePermissionAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityCreatePermissionResponse>(jsonResult);
        }

        public async Task<IdentityUpdatePermissionResponse> UpdatePermissionAsync(IdentityUpdatePermissionsRequest identityUpdatePermissionsRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission";

            HttpRequestMessage request = new(HttpMethod.Put, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityUpdatePermissionsRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"UpdatePermissionAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUpdatePermissionResponse>(jsonResult);
        }

        public async Task<IdentityCreateRolePermissionResponse> RolePermissionAsync(IdentityCreateRolePermissionRequest identityCreateRolePermissionRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission/RolePermission";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityCreateRolePermissionRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"RolePermissionAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityCreateRolePermissionResponse>(jsonResult);
        }

        public async Task<IdentityCreateRolePermissionsResponse> RolePermissionsAsync(IdentityCreateRolePermissionsRequest identityCreateRolePermissionsRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission/RolePermissions";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityCreateRolePermissionsRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"RolePermissionsAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityCreateRolePermissionsResponse>(jsonResult);
        }

        public async Task<IdentityRemovePermissionRoleResponse> RemovePermissionRoleAsync(IdentityRemovePermissionRoleRequest identityRemovePermissionRoleRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission/RemovePermissionRole";

            HttpRequestMessage request = new(HttpMethod.Delete, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityRemovePermissionRoleRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"RemovePermissionRoleAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityRemovePermissionRoleResponse>(jsonResult);
        }

        public async Task<IdentityUpdatePermissionRoleResponse> UpdatePermissionRoleAsync(IdentityUpdatePermissionRoleRequest identityUpdatePermissionRoleRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission/UpdateRolePermission";

            HttpRequestMessage request = new(HttpMethod.Put, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityUpdatePermissionRoleRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"UpdatePermissionRoleAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUpdatePermissionRoleResponse>(jsonResult);
        }

        public async Task<IdentityPermissionSelectResponse> GetPermissionSelectAsync()
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Pemission/Select";

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetPermissionSelectAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityPermissionSelectResponse>(jsonResult);
        }

        public async Task<IdentityGroupResponse> GetGroupAsync(Guid id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Group/" + id;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetGroupAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityGroupResponse>(jsonResult);
        }

        public async Task<IdentityDeleteGroupResponse> DeleteGroupAsync(Guid id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Group/" + id;

            HttpRequestMessage request = new(HttpMethod.Delete, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"DeleteGroupAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityDeleteGroupResponse>(jsonResult);
        }

        public async Task<IdentityCreateGroupResponse> CreateGroupAsync(IdentityCreateGroupRequest identityCreateGroupRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Group/Pagination";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityCreateGroupRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"CreateGroupAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityCreateGroupResponse>(jsonResult);
        }

        public async Task<IdentityUpdateGroupResponse> UpdateGroupAsync(IdentityUpdateGroupRequest identityUpdateGroupRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Group";

            HttpRequestMessage request = new(HttpMethod.Put, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityUpdateGroupRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"UpdateGroupAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUpdateGroupResponse>(jsonResult);
        }

        public async Task<IdentityGroupAddUserResponse> GroupAddUserAsync(IdentityGroupAddUserRequest identityGroupAddUserRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Group/AddUser";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityGroupAddUserRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GroupAddUserAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityGroupAddUserResponse>(jsonResult);
        }

        public async Task<IdentityGroupSelectResponse> GetGroupSelectAsync()
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Group/Select";

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetGroupSelectAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityGroupSelectResponse>(jsonResult);
        }

        public async Task<IdentityDeleteTokenResponse> DeleteTokenAsync(Guid id)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Account/" + id;

            HttpRequestMessage request = new(HttpMethod.Delete, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"DeleteTokenAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityDeleteTokenResponse>(jsonResult);
        }

        public async Task<IdentityRoleUsersResponse> GetRoleUsersAsync(Guid roleId)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Role/Users/" + roleId;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetRoleUsersAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityRoleUsersResponse>(jsonResult);
        }

        public async Task<IdentityUserRolesResponse> GetUserRolesAsync(string userName)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/Roles/" + userName;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetUserRolesAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUserRolesResponse>(jsonResult);
        }

        public async Task<IdentityUserPermissionsResponse> GetUserPermissionsAsync(Guid userId, string language)
        {

            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Role/UserPermissions/" + userId;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            request.Headers.AcceptLanguage.Add(new StringWithQualityHeaderValue(language));

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetUserPermissionsAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityUserPermissionsResponse>(jsonResult);
        }

        public async Task<bool> ControlTokenAsync(string token)
        {

            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Account/control-token";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(token);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"ControlToken Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<bool>(jsonResult);
        }

        public async Task<IdentityForgotPasswordResponse> ForgotPasswordAsync(string userName, string domain, string Language)
        {

            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/forgot-password/" + userName + "/" + Language;

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(domain);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            request.Content = content;

            //request.Headers.Add("Content-Language", Language);

            var response = await _httpClient.SendAsync(request);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"ForgotPasswordAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityForgotPasswordResponse>(jsonResult);
        }

        public async Task<IdentityForgotPasswordResponse> ForgotPasswordTokenAsync(IdentityResetPasswordWithTokenRequest token)
        {

            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/User/forgot-password-token";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(token);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"ForgotPasswordTokenAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityForgotPasswordResponse>(jsonResult);
        }

        public async Task<IdentityTokenResponse> ControlOtpTokenAsync(IdentityTokenRequest tokenRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Account";

            HttpRequestMessage request = new(HttpMethod.Put, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(tokenRequest);

            request.Content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"ControlOtpToken Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityTokenResponse>(jsonResult);

        }

        public async Task<IdentityGetTenantIdResponse> GetTenantIdAsync(Guid tenantId)
        {

            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Tenant/GetId/" + tenantId;

            HttpRequestMessage request = new(HttpMethod.Get, requestUri);

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetTenantId Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityGetTenantIdResponse>(jsonResult);
        }

        public async Task<IdentityTenantPaginationResponse> GetTenantPaginationAsync(IdentityTenantPaginationRequest identityTenantPaginationRequest)
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");

            string requestUri = _authConfig.Value?.Authority + "/api/Tenant/Pagination";

            HttpRequestMessage request = new(HttpMethod.Post, requestUri);

            var jsonRequest = TextJsonExtensions.Serialize(identityTenantPaginationRequest);

            var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");

            string accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            request.Content = content;

            var response = await _httpClient.SendAsync(request);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation($"GetTenantPaginationAsync Result: {jsonResult}  RequestUri: {requestUri}");

            return TextJsonExtensions.Deserialize<IdentityTenantPaginationResponse>(jsonResult);
        }

        public async Task<IdentityGetContractByIdResponse> GetContractByIdAsync(IdentityGetContractByIdRequest getContractByIdRequest)
        {
            EnsureValidAuthority();

            Guid id = getContractByIdRequest.Id;

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                                        .AddMethod(HttpMethod.Get)
                                                        .AddRequestUri($"api/Contracts/{id}")
                                                        .AddAccessToken(_httpContextAccessor.HttpContext)
                                                        .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage).ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation("GetContractById Request: {@jsonRequest}  Response: {@jsonResponse}",
                                           getContractByIdRequest, jsonResult);

            return TextJsonExtensions.Deserialize<IdentityGetContractByIdResponse>(jsonResult);
        }

        public async Task<IdentityCreateContractResponse> CreateContractAsync(IdentityCreateContractRequest createContractRequest)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                        .AddMethod(HttpMethod.Post)
                                        .AddRequestUri("api/Contracts")
                                        .AddContent(createContractRequest, Encoding.UTF8)
                                        .AddAccessToken(_httpContextAccessor.HttpContext)
                                        .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation("CreateContract Request: {@jsonRequest} Response: {@jsonResponse}",
                                           createContractRequest, jsonResult);

            return TextJsonExtensions.Deserialize<IdentityCreateContractResponse>(jsonResult);
        }

        public async Task<IdentityUpdateContractResponse> UpdateContractAsync(IdentityUpdateContractRequest createContractRequest)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                        .AddMethod(HttpMethod.Put)
                                        .AddRequestUri("api/Contracts")
                                        .AddContent(createContractRequest, Encoding.UTF8)
                                        .AddAccessToken(_httpContextAccessor.HttpContext)
                                        .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation("UpdateContract Request: {@jsonRequest} Response: {@jsonResponse}",
                                           createContractRequest, jsonResult);

            return TextJsonExtensions.Deserialize<IdentityUpdateContractResponse>(jsonResult);
        }

        public async Task<IdentityGetContractsByParametersResponse> GetContractsByParametersAsync(IdentityGetContractsByParametersRequest request)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                        .AddMethod(HttpMethod.Get)
                                        .AddRequestUri(
                            $"api/Contracts/List/{request.Language}/{request.AppType}/{request.ContractType}")
                                        .AddAccessToken(_httpContextAccessor.HttpContext)
                                        .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation("GetContractsByParameters Request: {@jsonRequest} Response: {@jsonResponse}",
                                    request, jsonResult);

            return TextJsonExtensions.Deserialize<IdentityGetContractsByParametersResponse>(jsonResult);
        }

        public async Task<IdentityAdminPermissionPaginationResponse> AdminPermissionPaginationRequestAsync(IdentityAdminPermissionPaginationRequest request, string language)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Post)
                                      .AddRequestUri("api/Pemission/AdminPagination")
                                      .AddAcceptLanguage(language)
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .AddContent(request, Encoding.UTF8)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "IdentityServerPermissionPaginationRequest Request: {@jsonRequest} Response: {@jsonResponse}",
                       request, jsonResult
                       );

            return TextJsonExtensions.Deserialize<IdentityAdminPermissionPaginationResponse>(jsonResult);
        }

        public async Task<GetAdminPermissionSelectResponse> GetAdminPermissionSelectAsync()
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Get)
                                      .AddRequestUri("api/Pemission/AdminSelect")
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "GetAdminPermissionSelectAsync Response: {@jsonResponse}", jsonResult
                );

            return TextJsonExtensions.Deserialize<GetAdminPermissionSelectResponse>(jsonResult);
        }

        public async Task<IdentityGetRoleAdminSelectResponse> GetRoleAdminSelectResponse()
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Get)
                                      .AddRequestUri("api/Role/AdminSelect")
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "GetRoleAdminSelectResponse Response: {@jsonResponse}", jsonResult
                );

            return TextJsonExtensions.Deserialize<IdentityGetRoleAdminSelectResponse>(jsonResult);
        }

        public async Task<IdentityRoleAdminPaginationResponse> RoleAdminPermissionPaginationRequestAsync(IdentityRoleAdminPaginationRequest request, string language)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Post)
                                      .AddRequestUri("api/Role/AdminPagination")
                                      .AddAcceptLanguage(language)
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .AddContent(request, Encoding.UTF8)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "RoleAdminPermissionPaginationRequestAsync Request: {@jsonRequest} Response: {@jsonResponse}",
                       request, jsonResult
                       );

            return TextJsonExtensions.Deserialize<IdentityRoleAdminPaginationResponse>(jsonResult);
        }

        public async Task<IdentityCreateAdminRoleResponse> CreateAdminRole(IdentityCreateAdminRoleRequest request)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Post)
                                      .AddRequestUri("api/Role/AdminRole")
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .AddContent(request, Encoding.UTF8)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                           .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "CreateAdminRole Request: {@jsonRequest} Response: {@jsonResponse}",
                       request, jsonResult
                       );

            return TextJsonExtensions.Deserialize<IdentityCreateAdminRoleResponse>(jsonResult);
        }

        public async Task<IdentityAdminGridResponse> GetAdminGridAsync(IdentityAdminGridRequest request)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Post)
                                      .AddRequestUri("api/User/AdminGrid")
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .AddContent(request, Encoding.UTF8)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "GetAdminGridAsync Request: {@jsonRequest} Response: {@jsonResponse}",
                       request, jsonResult
                       );

            return TextJsonExtensions.Deserialize<IdentityAdminGridResponse>(jsonResult);
        }

        public async Task<IdentityGetAdminUserByIdResponse> GetAdminUserByIdAsync(IdentityGetAdminUserByIdRequest request)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Get)
                                      .AddRequestUri($"api/User/GetAdminUser/{request.Id}")
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "GetAdminUserById Request: {@jsonRequest} Response: {@jsonResponse}",
                       request, jsonResult
                       );

            return TextJsonExtensions.Deserialize<IdentityGetAdminUserByIdResponse>(jsonResult);
        }

        public async Task<IdentityCreateAdminUserWithDefaultPasswordResponse> CreateAdminUserWithDefaultPasswordAsync(IdentityCreateAdminUserWithDefaultPasswordRequest request, string language)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Post)
                                      .AddRequestUri("api/User/CreateAdminUserWithDefaultPassword")
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .AddAcceptLanguage(language)
                                      .AddContent(request, Encoding.UTF8)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "CreateAdminUserWithDefaultPasswordAsync Request: {@jsonRequest} Response: {@jsonResponse}",
                       request, jsonResult
                       );

            return TextJsonExtensions.Deserialize<IdentityCreateAdminUserWithDefaultPasswordResponse>(jsonResult);
        }

        public async Task<IdentityUpdateAdminUserResponse> UpdateAdminUserAsync(IdentityUpdateAdminUserRequest request, string language)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Put)
                                      .AddRequestUri("api/User/UpdateAdminUser")
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .AddAcceptLanguage(language)
                                      .AddContent(request, Encoding.UTF8)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "UpdateAdminUserAsync Request: {@jsonRequest} Response: {@jsonResponse}",
                       request, jsonResult
                       );

            return TextJsonExtensions.Deserialize<IdentityUpdateAdminUserResponse>(jsonResult);
        }

        public async Task<List<IdentityCheckContractsResponse>> CheckContractsAsync(string language)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Get)
                                      .AddRequestUri("api/Contracts/check-contracts/1")
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .AddAcceptLanguage(language)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "UpdateAdminUserAsync Request: {@jsonRequest} Response: {@jsonResponse}",
                       "", jsonResult
                       );

            var resp = TextJsonExtensions.Deserialize<IdentityCheckContractsServiceResponse>(jsonResult);
            return resp.Data;
        }

        public async Task<IdentityConfirmContractResponse> ConfirmContractAsync(Guid contractId)
        {
            EnsureValidAuthority();

            var httpRequestMessage = new HttpRequestMessageBuilder(_authConfig)
                                      .AddMethod(HttpMethod.Get)
                                      .AddRequestUri("api/Contracts/confirm-contract/" + contractId)
                                      .AddAccessToken(_httpContextAccessor.HttpContext)
                                      .Build();

            var response = await _httpClient.SendAsync(httpRequestMessage)
                                            .ConfigureAwait(false);

            await CheckStatusCode(response);

            string jsonResult = await response.Content.ReadAsStringAsync();

            _logger.LogInformation(
                "ConfirmContractAsync Request: {@jsonRequest} Response: {@jsonResponse}",
                       contractId, jsonResult
                       );

            var resp = TextJsonExtensions.Deserialize<IdentityConfirmContractServiceResponse>(jsonResult);
            return resp.Status == 200 ? new IdentityConfirmContractResponse { Response = true } : new IdentityConfirmContractResponse { Response = false };
        }

        #region private methods

        private void EnsureValidAuthority()
        {
            if (_authConfig.Value == null || _authConfig.Value.Authority.IsNullOrWhiteSpace())
                throw new System.Exception("#E1003 Authority value is null");
        }

        private async Task CheckStatusCode(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                object obj = response.StatusCode switch
                {
                    HttpStatusCode.ServiceUnavailable => throw new IdentityServiceCannotBeAccessedException(),
                    _ => throw await response.GetException()
                };
            }
        }
        #endregion
    }
}


























































////realda olmasi gerekenler
//public async Task<IdentityTokenResponse> GetTokenAsync_New(IdentityTokenRequest identityTokenRequest)
//{
//    //var requestUrl = _authConfig.Value.Authority + "connect/token";
//    //var request = new HttpRequestMessage(HttpMethod.Post, requestUrl);

//    //var keyValues = new List<KeyValuePair<string, string>>();
//    //keyValues.Add(new KeyValuePair<string, string>("grant_type", identityTokenRequest.GrantType));
//    //keyValues.Add(new KeyValuePair<string, string>("client_id", identityTokenRequest.ClientId));
//    //keyValues.Add(new KeyValuePair<string, string>("client_secret", identityTokenRequest.ClientSecret));
//    //keyValues.Add(new KeyValuePair<string, string>("username", identityTokenRequest.Username));
//    //keyValues.Add(new KeyValuePair<string, string>("password", identityTokenRequest.Password));

//    //if (identityTokenRequest.GrantType == "refresh_token")
//    //    keyValues.Add(new KeyValuePair<string, string>("refresh_token", identityTokenRequest.RefreshToken));

//    //if (identityTokenRequest.UserIdentifierType != IdentityUserIdentifierType.UserName)
//    //    keyValues.Add(new KeyValuePair<string, string>("identifier_type", ((int)identityTokenRequest.UserIdentifierType).ToString()));

//    //var content = new FormUrlEncodedContent(keyValues);
//    //content.Headers.Clear();
//    //content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

//    //request.Content = content;

//    //var response = await _httpClient.SendAsync(request);

//    //if (response.IsSuccessStatusCode)
//    //{
//    //    var rsp = await response.Content.ReadAsStringAsync();
//    //    return Jil.JSON.Deserialize<IdentityTokenResponse>(rsp, _options);
//    //}

//    //throw new GetTokenException(identityTokenRequest.Username);

//    return null;
//}


//public Task<IdentityCheckUserResponse> CheckUserAsync(string accessToken, IdentityCheckUserRequest checkRequest)
//{
//    throw new NotImplementedException();
//}

//public Task<Guid> CreateUserAsync(string accessToken, IdentityCreateUserRequest identityCreateUserRequest)
//{
//    throw new NotImplementedException();
//}

//public Task DeleteUserAsync(string accessToken, IdentityDeleteUserRequest identityDeleteUserRequest)
//{
//    throw new NotImplementedException();
//}

//public Task<string> GenerateUsernameAsync(string accessToken, string name, string surname)
//{
//    throw new NotImplementedException();
//}

//public Task<IdentityResetPasswordResponse> InitiateResetPasswordAsync(string accessToken, string identifier, IdentityResetPasswordRequest identityResetPasswordRequest)
//{
//    throw new NotImplementedException();
//}

//public Task PatchUserAsync(string accessToken, IdentityPatchUserRequest identityUpdatePersonalInformationRequest)
//{
//    throw new NotImplementedException();
//}

//public Task ResetPasswordAsync(string accessToken, string identifier, IdentityResetPasswordChangeRequest identityResetPasswordChangeRequest)
//{
//    throw new NotImplementedException();
//}

//public Task UpdatePasswordAsync(string accessToken, IdentityUpdatePasswordRequest identityUpdatePasswordRequest)
//{
//    throw new NotImplementedException();
//}

//public Task UpdateUserClaimsAsync(string accessToken, Guid userId, IdentityUpdateUserClaimsRequest identityUpdateUserClaimsRequest)
//{
//    throw new NotImplementedException();
//}