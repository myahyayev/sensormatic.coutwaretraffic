﻿using Sensormatic.Tool.Core;
using System;
using Sensormatic.Tool.Common;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class CreateSubAreaRequest : SensormaticRequestValidate
    {
        internal Guid _AreaId
        {
            get
            {
                if (Guid.TryParse(areaId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.areaId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_AreaId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1303 AreaId cannot be null", nameof(_AreaId)));

            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1304 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1305 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1306 Description invalid format", nameof(Description)));
        }
    }
}
