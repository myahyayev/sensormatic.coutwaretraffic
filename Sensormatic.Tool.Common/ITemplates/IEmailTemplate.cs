﻿using System;
using System.Collections.Generic;

namespace Sensormatic.Tool.Common
{
    public interface IEmailTemplate
    {
        public string TemplateName { get; }
        public List<Guid> UserIds { get; set; }
    }
}
