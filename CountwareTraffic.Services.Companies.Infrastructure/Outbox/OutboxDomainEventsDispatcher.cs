﻿using CountwareTraffic.Services.Companies.Application;
using Newtonsoft.Json;
using Sensormatic.Tool.Efcore;
using Sensormatic.Tool.Ioc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public interface IOutboxIDomainEventsDispatcher : IScopedDependency 
    {
        Task DispatchEventsAsync();
    }

    public class OutboxDomainEventsDispatcher : IOutboxIDomainEventsDispatcher
    {
        private readonly AreaDbContext _areaDbContext;
        private readonly IQueueEventMapper _queueEventMapper;
        private readonly IIdentityService _identityService;
        private readonly ICorrelationService _correlationService;
        private readonly ICurrentTenant _currentTenant;
        public OutboxDomainEventsDispatcher(AreaDbContext areaDbContext, IQueueEventMapper queueEventMapper, IIdentityService identityService, ICorrelationService correlationService, ICurrentTenant currentTenant)
        {
            _areaDbContext = areaDbContext;
            _queueEventMapper = queueEventMapper;
            _identityService = identityService;
            _correlationService = correlationService;
            _currentTenant = currentTenant;
        }

        public Task DispatchEventsAsync()
        {
            var domainEntities = _areaDbContext.ChangeTracker
                .Entries<IDomainEventRaisable>()
                .Where(x => x.Entity.Events != null && x.Entity.Events.Any()).ToList();

            var domainEvents = domainEntities
                .SelectMany(x => x.Entity.Events)
                .ToList();

            if (domainEvents.Count > 0)
            {
                var mappedEvents = _queueEventMapper.MapAll(domainEvents, _identityService.UserId, _correlationService.CorrelationId, _currentTenant.Id);

                foreach (var queueEvent in mappedEvents)
                {
                    if (queueEvent != null)
                    {
                        OutboxMessage outboxMessage = new(DateTime.Now, queueEvent.GetType().FullName, JsonConvert.SerializeObject(queueEvent), queueEvent.RecordId);
                        _areaDbContext.OutboxMessages.Add(outboxMessage);
                    }
                }
            }

            return Task.CompletedTask;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
