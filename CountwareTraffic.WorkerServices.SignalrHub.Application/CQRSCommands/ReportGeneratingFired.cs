﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    [Contract]
    public class ReportGeneratingFired : ICommand
    {
        public Guid ReportId { get; init; }
        public int ReportStatus { get; init; }
        public Guid TenantId { get; set; }
    }
}