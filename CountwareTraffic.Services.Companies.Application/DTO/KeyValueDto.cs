﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class KeyValueDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
