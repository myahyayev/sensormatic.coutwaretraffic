﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaCreatedCompletedHandler : IEventHandler<AreaCreatedCompleted>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public AreaCreatedCompletedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(AreaCreatedCompleted command)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var area = await areaRepository.GetAsync(command.AreaId, command.TenantId);

            area.WhenCreatedCompleted();

            await _unitOfWork.CommitAsync();

            _queueService.Publish(new AreaCreatedSuccessfully
            {
                RecordId = Guid.NewGuid(),
                AreaId = command.AreaId,
                AreaStatus = AreaStatus.Completed.Name,
                UserId = command.UserId,
                UserName = String.Empty,
                TenantId = command.TenantId
            });
        }
    }
}

