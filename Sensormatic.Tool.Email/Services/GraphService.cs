﻿using Microsoft.Extensions.Options;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using Sensormatic.Tool.Email.Configs;
using Sensormatic.Tool.Ioc;
using System;
using System.Net.Http.Headers;


namespace Sensormatic.Tool.Email
{
    public interface IGraphService
    {
        GraphServiceClient GetAuthenticatedGraphClient();
    }

    public class GraphService: IGraphService
    {
        private readonly string _clientId;
        private readonly string _tenantId;
        private readonly string _clientSecret;

        public GraphService(IOptions<AzureADOptions> appSettings)
        {
            _clientId = appSettings.Value?.ClientId!;
            _tenantId = appSettings.Value?.TenantId!;
            _clientSecret = appSettings.Value?.ClientSecret!;
        }

        public GraphServiceClient GetAuthenticatedGraphClient()
        {
            var app = ConfidentialClientApplicationBuilder
                        .Create(_clientId)
                        .WithClientSecret(_clientSecret)
                        .WithAuthority($"https://login.microsoftonline.com/{_tenantId}")
                        .Build();

            var scopes = new[] { "https://graph.microsoft.com/.default" };
            var authenticationResult = app.AcquireTokenForClient(scopes).ExecuteAsync().Result;
            var graphClient = new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
            {
                requestMessage.Headers.Authorization =
                    new AuthenticationHeaderValue("Bearer", authenticationResult.AccessToken);
            }));

            return graphClient;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
