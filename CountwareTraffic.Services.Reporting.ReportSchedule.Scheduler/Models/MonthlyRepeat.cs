﻿using Newtonsoft.Json;

namespace CountwareTraffic.Services.Reporting.ReportSchedule.Scheduler
{
    public class MonthlyRepeat
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
