﻿namespace CountwareTraffic.Services.Devices.Application
{
    public class BrandLookupDetailsDto : BrandLookupDto
    {
        public string Name { get; set; }
    }
}
