﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record AreaDeletedFailed : IQueueEvent
    {
        public Guid AreaId { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public bool IsDeleted { get; } = false;
        public Guid RecordId { get; init; }
        public Guid TenantId { get; set; }
    }
}
