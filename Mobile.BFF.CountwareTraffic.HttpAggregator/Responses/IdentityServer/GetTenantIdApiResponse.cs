﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetTenantIdApiResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ErpCode { get; set; }
        public string ErpGroupName { get; set; }
        public TenantGroupApiData TenantGroup { get; set; }
        public DateTime ErpStartDate { get; set; }
        public DateTime ErpEndDate { get; set; }
    }
}
