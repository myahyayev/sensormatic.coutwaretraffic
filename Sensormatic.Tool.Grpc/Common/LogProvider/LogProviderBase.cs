﻿using Sensormatic.Tool.Grpc.Common;
using System.Threading.Tasks;

namespace Sensormatic.Tool.Grpc.Common
{
    public class LogProviderBase : ILogProvider
    {
        public virtual Task WriteLogAsync(WebServiceLog webServiceLog)
        {
            //Istersen logla

            return Task.CompletedTask;
        }
    }
}
