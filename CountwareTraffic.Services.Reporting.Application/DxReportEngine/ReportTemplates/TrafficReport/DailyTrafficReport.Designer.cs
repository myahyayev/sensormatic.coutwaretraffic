﻿namespace CountwareTraffic.Services.Reporting.Engine
{
    partial class DailyTrafficReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView1 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.XYDiagram xyDiagram2 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SeriesKeyColorColorizer seriesKeyColorColorizer1 = new DevExpress.XtraCharts.SeriesKeyColorColorizer();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DailyTrafficReport));
            this.objectDataSource2 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.MainDetail = new DevExpress.XtraReports.UI.DetailBand();
            this.SummaryByStore = new DevExpress.XtraReports.UI.DetailReportBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.SummaryByStoreHeaderTable = new DevExpress.XtraReports.UI.XRTable();
            this.SummaryTableHeaderRow = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.table3 = new DevExpress.XtraReports.UI.XRTable();
            this.tableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.weatherIcon = new DevExpress.XtraReports.UI.XRPictureBox();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.SummaryByStoreFooterTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.Total = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.SummaryByStoreSubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.SummaryByStoreTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupCaption2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupData2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailCaption2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailData2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupFooterBackground3 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailData3_Odd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ComparedStoresSummary = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.ComparedStoresSummarySubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.ComparedStoresSummaryTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.ComparedStoresSummaryHeaderTable = new DevExpress.XtraReports.UI.XRTable();
            this.ComparedStoresSummaryTableHeaderRow = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.ComparedStoresSummaryFooterTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByHourCharts = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TrafficByHourChartsTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByHourChartsSubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByHourChart = new DevExpress.XtraReports.UI.XRChart();
            this.ReportFooter2 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.TrafficByHourChartsFooterText = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByStoreCharts = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.TrafficByStoreChartsTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByStoreChartsSubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficbyStoreChart = new DevExpress.XtraReports.UI.XRChart();
            this.ReportFooter3 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.TrafficByStoreChartsFooter = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.CreatedDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.CorporationLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.tip = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.SubTitleLbl = new DevExpress.XtraReports.UI.XRLabel();
            this.TitleLbl = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.ApplicationLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryByStoreHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComparedStoresSummaryHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficByHourChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficbyStoreChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // objectDataSource2
            // 
            this.objectDataSource2.DataSource = typeof(global::CountwareTraffic.Services.Reporting.Engine.TrafficReport.DailyTrafficReportData);
            this.objectDataSource2.Name = "objectDataSource2";
            // 
            // TopMargin
            // 
            this.TopMargin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.StylePriority.UseBackColor = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.BackColor = System.Drawing.Color.Empty;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.StylePriority.UseBackColor = false;
            // 
            // xrLabel111
            // 
            this.xrLabel111.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CreatedBy]")});
            this.xrLabel111.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel111.ForeColor = System.Drawing.Color.Gray;
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69.16666F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.SizeF = new System.Drawing.SizeF(300.9584F, 24.19433F);
            this.xrLabel111.StylePriority.UseFont = false;
            this.xrLabel111.StylePriority.UseForeColor = false;
            this.xrLabel111.Text = "Note:this is footer text";
            // 
            // MainDetail
            // 
            this.MainDetail.Expanded = false;
            this.MainDetail.HeightF = 12.70832F;
            this.MainDetail.KeepTogether = true;
            this.MainDetail.Name = "MainDetail";
            this.MainDetail.Visible = false;
            // 
            // SummaryByStore
            // 
            this.SummaryByStore.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.GroupHeader1,
            this.Detail1,
            this.GroupFooter1,
            this.GroupHeader3});
            this.SummaryByStore.DataMember = "SummaryByStore.Content";
            this.SummaryByStore.DataSource = this.objectDataSource2;
            this.SummaryByStore.Expanded = false;
            this.SummaryByStore.Level = 3;
            this.SummaryByStore.Name = "SummaryByStore";
            this.SummaryByStore.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.SummaryByStoreHeaderTable});
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("StoreId", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 40F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // SummaryByStoreHeaderTable
            // 
            this.SummaryByStoreHeaderTable.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.SummaryByStoreHeaderTable.Name = "SummaryByStoreHeaderTable";
            this.SummaryByStoreHeaderTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.SummaryTableHeaderRow});
            this.SummaryByStoreHeaderTable.SizeF = new System.Drawing.SizeF(1130.109F, 40F);
            // 
            // SummaryTableHeaderRow
            // 
            this.SummaryTableHeaderRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell4,
            this.tableCell5,
            this.tableCell8,
            this.xrTableCell5,
            this.tableCell9});
            this.SummaryTableHeaderRow.Name = "SummaryTableHeaderRow";
            this.SummaryTableHeaderRow.Weight = 1D;
            // 
            // tableCell4
            // 
            this.tableCell4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell4.Name = "tableCell4";
            this.tableCell4.StyleName = "DetailCaption2";
            this.tableCell4.StylePriority.UseBorderColor = false;
            this.tableCell4.StylePriority.UseTextAlignment = false;
            this.tableCell4.Tag = "StoreName";
            this.tableCell4.Text = "Store Name";
            this.tableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell4.Weight = 0.05193371125697218D;
            // 
            // tableCell5
            // 
            this.tableCell5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell5.Name = "tableCell5";
            this.tableCell5.StyleName = "DetailCaption2";
            this.tableCell5.StylePriority.UseBorderColor = false;
            this.tableCell5.StylePriority.UseTextAlignment = false;
            this.tableCell5.Tag = "Date";
            this.tableCell5.Text = "Date";
            this.tableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell5.Weight = 0.039344092081952005D;
            // 
            // tableCell8
            // 
            this.tableCell8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell8.Name = "tableCell8";
            this.tableCell8.StyleName = "DetailCaption2";
            this.tableCell8.StylePriority.UseBorderColor = false;
            this.tableCell8.Tag = "TrafficInBound";
            this.tableCell8.Text = "Traffic InBound";
            this.tableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell8.Weight = 0.028247043993393072D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StyleName = "DetailCaption2";
            this.xrTableCell5.StylePriority.UseBorderColor = false;
            this.xrTableCell5.Tag = "TrafficOutBound";
            this.xrTableCell5.Text = "Traffic OutBound";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.028247044226006695D;
            // 
            // tableCell9
            // 
            this.tableCell9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell9.Name = "tableCell9";
            this.tableCell9.StyleName = "DetailCaption2";
            this.tableCell9.StylePriority.UseBorderColor = false;
            this.tableCell9.StylePriority.UseTextAlignment = false;
            this.tableCell9.Tag = "Weather";
            this.tableCell9.Text = "Weather";
            this.tableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell9.Weight = 0.037935905283581969D;
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table3});
            this.Detail1.HeightF = 40F;
            this.Detail1.KeepTogether = true;
            this.Detail1.Name = "Detail1";
            this.Detail1.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("order", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            // 
            // table3
            // 
            this.table3.BorderColor = System.Drawing.Color.Black;
            this.table3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.table3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.table3.BorderWidth = 1F;
            this.table3.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.table3.Name = "table3";
            this.table3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow3});
            this.table3.SizeF = new System.Drawing.SizeF(1130.109F, 40F);
            this.table3.StylePriority.UseBorderColor = false;
            this.table3.StylePriority.UseBorderWidth = false;
            // 
            // tableRow3
            // 
            this.tableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell29,
            this.tableCell30,
            this.tableCell33,
            this.xrTableCell6,
            this.tableCell34,
            this.xrTableCell2});
            this.tableRow3.Name = "tableRow3";
            this.tableRow3.Weight = 11.5D;
            // 
            // tableCell29
            // 
            this.tableCell29.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell29.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.tableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.tableCell29.BorderWidth = 1F;
            this.tableCell29.EvenStyleName = "DetailData2";
            this.tableCell29.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StoreName]")});
            this.tableCell29.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableCell29.KeepTogether = true;
            this.tableCell29.Name = "tableCell29";
            this.tableCell29.OddStyleName = "DetailData3_Odd";
            this.tableCell29.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Merge;
            this.tableCell29.StylePriority.UseBackColor = false;
            this.tableCell29.StylePriority.UseBorderDashStyle = false;
            this.tableCell29.StylePriority.UseBorders = false;
            this.tableCell29.StylePriority.UseBorderWidth = false;
            this.tableCell29.StylePriority.UseFont = false;
            this.tableCell29.StylePriority.UseTextAlignment = false;
            this.tableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell29.Weight = 0.047723838198863354D;
            // 
            // tableCell30
            // 
            this.tableCell30.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell30.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.tableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableCell30.BorderWidth = 1F;
            this.tableCell30.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CurrentPeriod].[Caption]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]")});
            this.tableCell30.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableCell30.Name = "tableCell30";
            this.tableCell30.OddStyleName = "DetailData3_Odd";
            this.tableCell30.StyleName = "DetailData2";
            this.tableCell30.StylePriority.UseFont = false;
            this.tableCell30.StylePriority.UseTextAlignment = false;
            this.tableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell30.Weight = 0.036154763114691131D;
            // 
            // tableCell33
            // 
            this.tableCell33.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell33.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.tableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableCell33.BorderWidth = 1F;
            this.tableCell33.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[InBound].[Value],[Traffic].[InBound].[Unit],[Traffic].[InBound]" +
                    ".[Indicator])\n"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[InBound].[ForeColor].[IsEmpty], false,[Traffic].[InBound].[ForeCol" +
                    "or])")});
            this.tableCell33.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableCell33.Name = "tableCell33";
            this.tableCell33.OddStyleName = "DetailData3_Odd";
            this.tableCell33.StyleName = "DetailData2";
            this.tableCell33.StylePriority.UseBackColor = false;
            this.tableCell33.StylePriority.UseFont = false;
            this.tableCell33.StylePriority.UseForeColor = false;
            this.tableCell33.StylePriority.UseTextAlignment = false;
            this.tableCell33.Weight = 0.02595726881878431D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell6.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.BorderWidth = 1F;
            this.xrTableCell6.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[OutBound].[Value],[Traffic].[OutBound].[Unit],[Traffic].[OutBou" +
                    "nd].[Indicator])\n"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[OutBound].[ForeColor].[IsEmpty], false,[Traffic].[OutBound].[ForeC" +
                    "olor] )\n")});
            this.xrTableCell6.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.OddStyleName = "DetailData3_Odd";
            this.xrTableCell6.StyleName = "DetailData2";
            this.xrTableCell6.StylePriority.UseBackColor = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseForeColor = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 0.025957268400665454D;
            // 
            // tableCell34
            // 
            this.tableCell34.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell34.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.tableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableCell34.BorderWidth = 1F;
            this.tableCell34.CanShrink = true;
            this.tableCell34.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Weather].[Description],\' \',[Weather].[Temperature].[Value],\' \',[Weather]." +
                    "[Temperature].[Unit])")});
            this.tableCell34.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableCell34.Name = "tableCell34";
            this.tableCell34.NullValueText = "-";
            this.tableCell34.OddStyleName = "DetailData3_Odd";
            this.tableCell34.RowSpan = 2;
            this.tableCell34.StyleName = "DetailData2";
            this.tableCell34.StylePriority.UseFont = false;
            this.tableCell34.StylePriority.UseTextAlignment = false;
            this.tableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell34.Weight = 0.026479693842125757D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell2.BorderWidth = 1F;
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.weatherIcon});
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.OddStyleName = "DetailData3_Odd";
            this.xrTableCell2.StyleName = "DetailData2";
            this.xrTableCell2.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseBorderWidth = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.0083810372676652D;
            // 
            // weatherIcon
            // 
            this.weatherIcon.BorderWidth = 0F;
            this.weatherIcon.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ImageSource", "[Weather].[Icon]")});
            this.weatherIcon.LocationFloat = new DevExpress.Utils.PointFloat(2.5F, 0F);
            this.weatherIcon.Name = "weatherIcon";
            this.weatherIcon.SizeF = new System.Drawing.SizeF(40F, 40F);
            this.weatherIcon.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.weatherIcon.StylePriority.UseBorderWidth = false;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.SummaryByStoreFooterTitle,
            this.xrLabel3,
            this.xrLabel1,
            this.Total});
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 77.68239F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.GroupFooter1.StylePriority.UseBorders = false;
            // 
            // SummaryByStoreFooterTitle
            // 
            this.SummaryByStoreFooterTitle.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreFooterTitle.ForeColor = System.Drawing.Color.Gray;
            this.SummaryByStoreFooterTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 42.24382F);
            this.SummaryByStoreFooterTitle.Name = "SummaryByStoreFooterTitle";
            this.SummaryByStoreFooterTitle.SizeF = new System.Drawing.SizeF(1130.109F, 24.19433F);
            this.SummaryByStoreFooterTitle.StyleName = "tip";
            this.SummaryByStoreFooterTitle.Text = "Footertitle";
            this.SummaryByStoreFooterTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum(Iif([Traffic].[InBound].[IsComparedRow],0 ,[Traffic].[InBound].[Value]))")});
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(575.4635F, 4.999938F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(171.8951F, 23F);
            this.xrLabel3.StyleName = "Title";
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseFont = false;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel3.Summary = xrSummary1;
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum(Iif([Traffic].[OutBound].[IsComparedRow],0 ,[Traffic].[OutBound].[Value]))" +
                    "\n")});
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(747.3586F, 4.999938F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(171.895F, 23F);
            this.xrLabel1.StyleName = "Title";
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel1.Summary = xrSummary2;
            this.xrLabel1.Text = "xrLabel3";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Total
            // 
            this.Total.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.Total.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Total.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Total.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.Total.Name = "Total";
            this.Total.Padding = new DevExpress.XtraPrinting.PaddingInfo(415, 0, 0, 0, 100F);
            this.Total.SizeF = new System.Drawing.SizeF(1130.109F, 29.19439F);
            this.Total.StyleName = "Title";
            this.Total.StylePriority.UseBorderColor = false;
            this.Total.StylePriority.UseBorders = false;
            this.Total.StylePriority.UseFont = false;
            this.Total.StylePriority.UsePadding = false;
            this.Total.StylePriority.UseTextAlignment = false;
            this.Total.Tag = "Total";
            this.Total.Text = "Total";
            this.Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.SummaryByStoreSubTitle,
            this.SummaryByStoreTitle});
            this.GroupHeader3.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader3.HeightF = 114.5963F;
            this.GroupHeader3.KeepTogether = true;
            this.GroupHeader3.Level = 1;
            this.GroupHeader3.Name = "GroupHeader3";
            this.GroupHeader3.RepeatEveryPage = true;
            // 
            // xrLabel4
            // 
            this.xrLabel4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StoreName]\n")});
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 76.42603F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.SizeF = new System.Drawing.SizeF(1133.78F, 24.19433F);
            this.xrLabel4.StyleName = "Title";
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Summary by Store";
            // 
            // SummaryByStoreSubTitle
            // 
            this.SummaryByStoreSubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreSubTitle.LocationFloat = new DevExpress.Utils.PointFloat(621.0416F, 76.42606F);
            this.SummaryByStoreSubTitle.Name = "SummaryByStoreSubTitle";
            this.SummaryByStoreSubTitle.SizeF = new System.Drawing.SizeF(155.67F, 24.19433F);
            this.SummaryByStoreSubTitle.StyleName = "Title";
            this.SummaryByStoreSubTitle.StylePriority.UseFont = false;
            this.SummaryByStoreSubTitle.Text = "Summary by Store";
            this.SummaryByStoreSubTitle.Visible = false;
            // 
            // SummaryByStoreTitle
            // 
            this.SummaryByStoreTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 40.38433F);
            this.SummaryByStoreTitle.Name = "SummaryByStoreTitle";
            this.SummaryByStoreTitle.SizeF = new System.Drawing.SizeF(1133.78F, 24.19432F);
            this.SummaryByStoreTitle.StyleName = "Title";
            this.SummaryByStoreTitle.StylePriority.UseFont = false;
            this.SummaryByStoreTitle.Text = "Summary by Store";
            // 
            // Title
            // 
            this.Title.BackColor = System.Drawing.Color.Transparent;
            this.Title.BorderColor = System.Drawing.Color.Black;
            this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Title.BorderWidth = 1F;
            this.Title.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.Title.Name = "Title";
            this.Title.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // GroupCaption2
            // 
            this.GroupCaption2.BackColor = System.Drawing.Color.White;
            this.GroupCaption2.BorderColor = System.Drawing.Color.Black;
            this.GroupCaption2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GroupCaption2.BorderWidth = 1F;
            this.GroupCaption2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupCaption2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.GroupCaption2.Name = "GroupCaption2";
            this.GroupCaption2.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GroupCaption2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupData2
            // 
            this.GroupData2.BackColor = System.Drawing.Color.White;
            this.GroupData2.BorderColor = System.Drawing.Color.White;
            this.GroupData2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GroupData2.BorderWidth = 2F;
            this.GroupData2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupData2.ForeColor = System.Drawing.Color.Black;
            this.GroupData2.Name = "GroupData2";
            this.GroupData2.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GroupData2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailCaption2
            // 
            this.DetailCaption2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.DetailCaption2.BorderColor = System.Drawing.Color.Transparent;
            this.DetailCaption2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DetailCaption2.BorderWidth = 0F;
            this.DetailCaption2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailCaption2.ForeColor = System.Drawing.Color.White;
            this.DetailCaption2.Name = "DetailCaption2";
            this.DetailCaption2.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
            this.DetailCaption2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailData2
            // 
            this.DetailData2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.DetailData2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.DetailData2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DetailData2.BorderWidth = 0F;
            this.DetailData2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailData2.ForeColor = System.Drawing.Color.Black;
            this.DetailData2.Name = "DetailData2";
            this.DetailData2.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 6, 6, 100F);
            this.DetailData2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupFooterBackground3
            // 
            this.GroupFooterBackground3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(117)))), ((int)(((byte)(129)))));
            this.GroupFooterBackground3.BorderColor = System.Drawing.Color.White;
            this.GroupFooterBackground3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GroupFooterBackground3.BorderWidth = 2F;
            this.GroupFooterBackground3.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupFooterBackground3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(228)))), ((int)(((byte)(228)))));
            this.GroupFooterBackground3.Name = "GroupFooterBackground3";
            this.GroupFooterBackground3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GroupFooterBackground3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailData3_Odd
            // 
            this.DetailData3_Odd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.DetailData3_Odd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.DetailData3_Odd.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.DetailData3_Odd.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DetailData3_Odd.BorderWidth = 0F;
            this.DetailData3_Odd.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailData3_Odd.ForeColor = System.Drawing.Color.Black;
            this.DetailData3_Odd.Name = "DetailData3_Odd";
            this.DetailData3_Odd.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 6, 6, 100F);
            this.DetailData3_Odd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageInfo
            // 
            this.PageInfo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PageInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(70)))), ((int)(((byte)(80)))));
            this.PageInfo.Name = "PageInfo";
            this.PageInfo.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
            // 
            // ComparedStoresSummary
            // 
            this.ComparedStoresSummary.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.ReportHeader1,
            this.ReportFooter1});
            this.ComparedStoresSummary.DataMember = "ComparedStoresSummary.Content";
            this.ComparedStoresSummary.DataSource = this.objectDataSource2;
            this.ComparedStoresSummary.Expanded = false;
            this.ComparedStoresSummary.Level = 0;
            this.ComparedStoresSummary.Name = "ComparedStoresSummary";
            // 
            // Detail4
            // 
            this.Detail4.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.Detail4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail4.HeightF = 40F;
            this.Detail4.Name = "Detail4";
            this.Detail4.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("order", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail4.StylePriority.UseBorderDashStyle = false;
            this.Detail4.StylePriority.UseBorders = false;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.Color.Black;
            this.xrTable1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable1.BorderWidth = 1F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1130.11F, 40F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell11,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow1.Weight = 11.5D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell1.BorderWidth = 1F;
            this.xrTableCell1.EvenStyleName = "DetailData2";
            this.xrTableCell1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CurrentPeriod].[Caption]")});
            this.xrTableCell1.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.OddStyleName = "DetailData3_Odd";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseBorderWidth = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "xrTableCell1";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.072545136183890135D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BorderWidth = 0F;
            this.xrTableCell11.EvenStyleName = "DetailData2";
            this.xrTableCell11.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[InBound].[Value],[Traffic].[InBound].[Unit],[Traffic].[InBound]" +
                    ".[Indicator])"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[InBound].[ForeColor].[IsEmpty], false,[Traffic].[InBound].[ForeCol" +
                    "or] )\n")});
            this.xrTableCell11.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.OddStyleName = "DetailData3_Odd";
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseForeColor = false;
            this.xrTableCell11.Text = "xrTableCell11";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.097560702240805139D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell3.BorderWidth = 1F;
            this.xrTableCell3.EvenStyleName = "DetailData2";
            this.xrTableCell3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[OutBound].[Value],[Traffic].[OutBound].[Unit],[Traffic].[OutBou" +
                    "nd].[Indicator])\n"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[OutBound].[ForeColor].[IsEmpty], false,[Traffic].[OutBound].[ForeC" +
                    "olor])\n")});
            this.xrTableCell3.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.OddStyleName = "DetailData3_Odd";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseBorderWidth = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseForeColor = false;
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.097560706144588866D;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ComparedStoresSummarySubTitle,
            this.ComparedStoresSummaryTitle,
            this.ComparedStoresSummaryHeaderTable});
            this.ReportHeader1.HeightF = 153.69F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // ComparedStoresSummarySubTitle
            // 
            this.ComparedStoresSummarySubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComparedStoresSummarySubTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 73.49567F);
            this.ComparedStoresSummarySubTitle.Name = "ComparedStoresSummarySubTitle";
            this.ComparedStoresSummarySubTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.ComparedStoresSummarySubTitle.StyleName = "Title";
            this.ComparedStoresSummarySubTitle.StylePriority.UseFont = false;
            this.ComparedStoresSummarySubTitle.Text = "LLY Comp Stores";
            // 
            // ComparedStoresSummaryTitle
            // 
            this.ComparedStoresSummaryTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComparedStoresSummaryTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 37.45403F);
            this.ComparedStoresSummaryTitle.Name = "ComparedStoresSummaryTitle";
            this.ComparedStoresSummaryTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.ComparedStoresSummaryTitle.StyleName = "Title";
            this.ComparedStoresSummaryTitle.StylePriority.UseFont = false;
            this.ComparedStoresSummaryTitle.Text = "Summary";
            // 
            // ComparedStoresSummaryHeaderTable
            // 
            this.ComparedStoresSummaryHeaderTable.LocationFloat = new DevExpress.Utils.PointFloat(20F, 113.69F);
            this.ComparedStoresSummaryHeaderTable.Name = "ComparedStoresSummaryHeaderTable";
            this.ComparedStoresSummaryHeaderTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.ComparedStoresSummaryTableHeaderRow});
            this.ComparedStoresSummaryHeaderTable.SizeF = new System.Drawing.SizeF(1130.11F, 39.99999F);
            // 
            // ComparedStoresSummaryTableHeaderRow
            // 
            this.ComparedStoresSummaryTableHeaderRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell45,
            this.xrTableCell4});
            this.ComparedStoresSummaryTableHeaderRow.Name = "ComparedStoresSummaryTableHeaderRow";
            this.ComparedStoresSummaryTableHeaderRow.Weight = 1D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell42.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StyleName = "DetailCaption2";
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.Tag = "Date";
            this.xrTableCell42.Text = "Date";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell42.Weight = 0.027895164578078582D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StyleName = "DetailCaption2";
            this.xrTableCell45.StylePriority.UseBorderColor = false;
            this.xrTableCell45.Tag = "TrafficInBound";
            this.xrTableCell45.Text = "Traffic InBound";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.03751418711465454D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StyleName = "DetailCaption2";
            this.xrTableCell4.StylePriority.UseBorderColor = false;
            this.xrTableCell4.Tag = "TrafficOutBound";
            this.xrTableCell4.Text = "Traffic OutBound";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell4.Weight = 0.037514184862537055D;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.ComparedStoresSummaryFooterTitle});
            this.ReportFooter1.HeightF = 34.31646F;
            this.ReportFooter1.KeepTogether = true;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrLine2
            // 
            this.xrLine2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(1130.11F, 2F);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // ComparedStoresSummaryFooterTitle
            // 
            this.ComparedStoresSummaryFooterTitle.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComparedStoresSummaryFooterTitle.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComparedStoresSummaryFooterTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 10F);
            this.ComparedStoresSummaryFooterTitle.Name = "ComparedStoresSummaryFooterTitle";
            this.ComparedStoresSummaryFooterTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.ComparedStoresSummaryFooterTitle.StyleName = "tip";
            this.ComparedStoresSummaryFooterTitle.Text = "Note:this is footer text";
            this.ComparedStoresSummaryFooterTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TrafficByHourCharts
            // 
            this.TrafficByHourCharts.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportFooter2});
            this.TrafficByHourCharts.Expanded = false;
            this.TrafficByHourCharts.Level = 1;
            this.TrafficByHourCharts.Name = "TrafficByHourCharts";
            this.TrafficByHourCharts.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TrafficByHourChartsTitle,
            this.TrafficByHourChartsSubTitle,
            this.TrafficByHourChart});
            this.Detail.HeightF = 615.5027F;
            this.Detail.Name = "Detail";
            // 
            // TrafficByHourChartsTitle
            // 
            this.TrafficByHourChartsTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByHourChartsTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 41.00004F);
            this.TrafficByHourChartsTitle.Name = "TrafficByHourChartsTitle";
            this.TrafficByHourChartsTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.TrafficByHourChartsTitle.StyleName = "Title";
            this.TrafficByHourChartsTitle.StylePriority.UseFont = false;
            this.TrafficByHourChartsTitle.Text = "Traffic by Hour";
            // 
            // TrafficByHourChartsSubTitle
            // 
            this.TrafficByHourChartsSubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByHourChartsSubTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 77.04175F);
            this.TrafficByHourChartsSubTitle.Name = "TrafficByHourChartsSubTitle";
            this.TrafficByHourChartsSubTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TrafficByHourChartsSubTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.TrafficByHourChartsSubTitle.StyleName = "Title";
            this.TrafficByHourChartsSubTitle.StylePriority.UseFont = false;
            this.TrafficByHourChartsSubTitle.StylePriority.UsePadding = false;
            this.TrafficByHourChartsSubTitle.Text = "Traffic by Hour";
            // 
            // TrafficByHourChart
            // 
            this.TrafficByHourChart.BorderColor = System.Drawing.Color.Black;
            this.TrafficByHourChart.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TrafficByHourChart.DataSource = this.objectDataSource2;
            xyDiagram1.AxisX.GridLines.Visible = true;
            xyDiagram1.AxisX.Label.Alignment = DevExpress.XtraCharts.AxisLabelAlignment.Center;
            xyDiagram1.AxisX.Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram1.AxisX.Label.ResolveOverlappingOptions.AllowHide = false;
            xyDiagram1.AxisX.LabelVisibilityMode = DevExpress.XtraCharts.AxisLabelVisibilityMode.AutoGeneratedAndCustom;
            xyDiagram1.AxisX.Title.Font = new System.Drawing.Font("Arial", 12F);
            xyDiagram1.AxisX.Visibility = DevExpress.Utils.DefaultBoolean.True;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram1.AxisY.Title.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisXZooming = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYZooming = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.PaneLayout.Direction = DevExpress.XtraCharts.PaneLayoutDirection.Horizontal;
            xyDiagram1.Rotated = true;
            this.TrafficByHourChart.Diagram = xyDiagram1;
            this.TrafficByHourChart.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Gradient;
            this.TrafficByHourChart.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.TrafficByHourChart.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.TrafficByHourChart.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.TrafficByHourChart.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 112.6666F);
            this.TrafficByHourChart.Name = "TrafficByHourChart";
            this.TrafficByHourChart.PaletteName = "CountwareChartPalleteColor";
            this.TrafficByHourChart.PaletteRepository.Add("CountwareChartPalleteColor", new DevExpress.XtraCharts.Palette("CountwareChartPalleteColor", DevExpress.XtraCharts.PaletteScaleMode.Extrapolate, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(39)))), ((int)(((byte)(63))))), System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(39)))), ((int)(((byte)(63)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(95)))), ((int)(((byte)(255))))), System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(95)))), ((int)(((byte)(255)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(31)))), ((int)(((byte)(48))))), System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(31)))), ((int)(((byte)(48)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(141)))), ((int)(((byte)(163))))), System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(141)))), ((int)(((byte)(163)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(194)))), ((int)(((byte)(221))))), System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(194)))), ((int)(((byte)(221)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(99)))), ((int)(((byte)(140))))), System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(99)))), ((int)(((byte)(140)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(180)))), ((int)(((byte)(252))))), System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(180)))), ((int)(((byte)(252)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158))))), System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))), System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(34)))), ((int)(((byte)(76))))), System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(34)))), ((int)(((byte)(76)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(86)))), ((int)(((byte)(90))))), System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(86)))), ((int)(((byte)(90)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160))))), System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(216)))), ((int)(((byte)(214))))), System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(216)))), ((int)(((byte)(214)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(24)))), ((int)(((byte)(52))))), System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(24)))), ((int)(((byte)(52)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238))))), System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))), System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))))}));
            this.TrafficByHourChart.SeriesDataMember = "TrafficByHourChart.Content.CurrentPeriod.Caption";
            series1.ArgumentDataMember = "TrafficByHourChart.Content.CurrentPeriod.Caption";
            series1.DataSorted = true;
            sideBySideBarSeriesLabel1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sideBySideBarSeriesLabel1.Position = DevExpress.XtraCharts.BarSeriesLabelPosition.Top;
            series1.Label = sideBySideBarSeriesLabel1;
            series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.Name = "InBound";
            series1.ValueDataMembersSerializable = "TrafficByHourChart.Content.Traffic.InBound.Value";
            sideBySideBarSeriesView1.BarWidth = 0.2D;
            sideBySideBarSeriesView1.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            sideBySideBarSeriesView1.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            series1.View = sideBySideBarSeriesView1;
            series2.ArgumentDataMember = "TrafficByHourChart.Content.CurrentPeriod.Caption";
            sideBySideBarSeriesLabel2.Position = DevExpress.XtraCharts.BarSeriesLabelPosition.TopInside;
            series2.Label = sideBySideBarSeriesLabel2;
            series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series2.Name = "OUT";
            series2.ValueDataMembersSerializable = "TrafficByHourChart.Content.Traffic.OutBound.Value";
            series2.Visible = false;
            this.TrafficByHourChart.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            this.TrafficByHourChart.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            this.TrafficByHourChart.SeriesTemplate.SeriesDataMember = "TrafficByHourChart.Content.CurrentPeriod.Caption";
            this.TrafficByHourChart.SeriesTemplate.ValueDataMembersSerializable = "TrafficByHourChart.Content.Traffic.InBound.Value";
            this.TrafficByHourChart.SeriesTemplate.Visible = false;
            this.TrafficByHourChart.SizeF = new System.Drawing.SizeF(1130.11F, 492.8361F);
            // 
            // ReportFooter2
            // 
            this.ReportFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TrafficByHourChartsFooterText});
            this.ReportFooter2.HeightF = 33.41071F;
            this.ReportFooter2.KeepTogether = true;
            this.ReportFooter2.Name = "ReportFooter2";
            // 
            // TrafficByHourChartsFooterText
            // 
            this.TrafficByHourChartsFooterText.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByHourChartsFooterText.ForeColor = System.Drawing.Color.Gray;
            this.TrafficByHourChartsFooterText.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 9.216384F);
            this.TrafficByHourChartsFooterText.Name = "TrafficByHourChartsFooterText";
            this.TrafficByHourChartsFooterText.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.TrafficByHourChartsFooterText.StyleName = "tip";
            this.TrafficByHourChartsFooterText.Text = "Note:this is footer text";
            this.TrafficByHourChartsFooterText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TrafficByStoreCharts
            // 
            this.TrafficByStoreCharts.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportFooter3});
            this.TrafficByStoreCharts.Expanded = false;
            this.TrafficByStoreCharts.Level = 2;
            this.TrafficByStoreCharts.Name = "TrafficByStoreCharts";
            this.TrafficByStoreCharts.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TrafficByStoreChartsTitle,
            this.TrafficByStoreChartsSubTitle,
            this.TrafficbyStoreChart});
            this.Detail2.HeightF = 604.364F;
            this.Detail2.Name = "Detail2";
            // 
            // TrafficByStoreChartsTitle
            // 
            this.TrafficByStoreChartsTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByStoreChartsTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 38.99999F);
            this.TrafficByStoreChartsTitle.Name = "TrafficByStoreChartsTitle";
            this.TrafficByStoreChartsTitle.SizeF = new System.Drawing.SizeF(1130.109F, 24.19432F);
            this.TrafficByStoreChartsTitle.StyleName = "Title";
            this.TrafficByStoreChartsTitle.StylePriority.UseFont = false;
            this.TrafficByStoreChartsTitle.Text = "Traffic by Store";
            // 
            // TrafficByStoreChartsSubTitle
            // 
            this.TrafficByStoreChartsSubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByStoreChartsSubTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 75.04174F);
            this.TrafficByStoreChartsSubTitle.Name = "TrafficByStoreChartsSubTitle";
            this.TrafficByStoreChartsSubTitle.SizeF = new System.Drawing.SizeF(1130.109F, 24.19433F);
            this.TrafficByStoreChartsSubTitle.StyleName = "Title";
            this.TrafficByStoreChartsSubTitle.StylePriority.UseFont = false;
            this.TrafficByStoreChartsSubTitle.Text = "Traffic by Store";
            // 
            // TrafficbyStoreChart
            // 
            this.TrafficbyStoreChart.AutoLayout = true;
            this.TrafficbyStoreChart.BorderColor = System.Drawing.Color.Black;
            this.TrafficbyStoreChart.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TrafficbyStoreChart.DataMember = "TrafficByStoreChart.Content";
            this.TrafficbyStoreChart.DataSource = this.objectDataSource2;
            xyDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisY.Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram2.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram2.DefaultPane.EnableAxisXZooming = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram2.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram2.DefaultPane.EnableAxisYZooming = DevExpress.Utils.DefaultBoolean.False;
            this.TrafficbyStoreChart.Diagram = xyDiagram2;
            this.TrafficbyStoreChart.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Gradient;
            this.TrafficbyStoreChart.IndicatorsPaletteName = "Equity";
            this.TrafficbyStoreChart.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.TrafficbyStoreChart.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.TrafficbyStoreChart.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.TrafficbyStoreChart.Legend.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficbyStoreChart.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 108.236F);
            this.TrafficbyStoreChart.Name = "TrafficbyStoreChart";
            this.TrafficbyStoreChart.PaletteName = "Palette 1";
            this.TrafficbyStoreChart.PaletteRepository.Add("Palette 1", new DevExpress.XtraCharts.Palette("Palette 1", DevExpress.XtraCharts.PaletteScaleMode.Extrapolate, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(39)))), ((int)(((byte)(63))))), System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(39)))), ((int)(((byte)(63)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(95)))), ((int)(((byte)(255))))), System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(95)))), ((int)(((byte)(255)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(31)))), ((int)(((byte)(48))))), System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(31)))), ((int)(((byte)(48)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(141)))), ((int)(((byte)(163))))), System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(141)))), ((int)(((byte)(163)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(194)))), ((int)(((byte)(221))))), System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(194)))), ((int)(((byte)(221)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(99)))), ((int)(((byte)(140))))), System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(99)))), ((int)(((byte)(140)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(180)))), ((int)(((byte)(252))))), System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(180)))), ((int)(((byte)(252)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158))))), System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))), System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(34)))), ((int)(((byte)(76))))), System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(34)))), ((int)(((byte)(76)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(86)))), ((int)(((byte)(90))))), System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(86)))), ((int)(((byte)(90)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160))))), System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(216)))), ((int)(((byte)(214))))), System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(216)))), ((int)(((byte)(214)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(24)))), ((int)(((byte)(52))))), System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(24)))), ((int)(((byte)(52)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238))))), System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))), System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))))}));
            this.TrafficbyStoreChart.SeriesDataMember = "StoreName";
            this.TrafficbyStoreChart.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.TrafficbyStoreChart.SeriesTemplate.ArgumentDataMember = "StoreName";
            sideBySideBarSeriesLabel3.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Gradient;
            sideBySideBarSeriesLabel3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sideBySideBarSeriesLabel3.MaxWidth = 100;
            sideBySideBarSeriesLabel3.Position = DevExpress.XtraCharts.BarSeriesLabelPosition.Top;
            this.TrafficbyStoreChart.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.TrafficbyStoreChart.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.TrafficbyStoreChart.SeriesTemplate.SeriesColorizer = seriesKeyColorColorizer1;
            this.TrafficbyStoreChart.SeriesTemplate.SeriesDataMember = "StoreName";
            this.TrafficbyStoreChart.SeriesTemplate.ValueDataMembersSerializable = "Traffic.InBound.Value";
            this.TrafficbyStoreChart.SizeF = new System.Drawing.SizeF(1130.11F, 458.8132F);
            // 
            // ReportFooter3
            // 
            this.ReportFooter3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TrafficByStoreChartsFooter});
            this.ReportFooter3.HeightF = 34.59072F;
            this.ReportFooter3.Name = "ReportFooter3";
            // 
            // TrafficByStoreChartsFooter
            // 
            this.TrafficByStoreChartsFooter.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByStoreChartsFooter.ForeColor = System.Drawing.Color.Gray;
            this.TrafficByStoreChartsFooter.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 9.39632F);
            this.TrafficByStoreChartsFooter.Name = "TrafficByStoreChartsFooter";
            this.TrafficByStoreChartsFooter.SizeF = new System.Drawing.SizeF(1130.109F, 24.19433F);
            this.TrafficByStoreChartsFooter.StyleName = "tip";
            this.TrafficByStoreChartsFooter.Text = "Note:this is footer text";
            this.TrafficByStoreChartsFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.PageFooter.HeightF = 75F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.StylePriority.UseBackColor = false;
            // 
            // xrPanel1
            // 
            this.xrPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.CreatedDate,
            this.xrLabel2,
            this.pageInfo2,
            this.CorporationLogo});
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1169F, 75F);
            this.xrPanel1.StylePriority.UseBackColor = false;
            // 
            // CreatedDate
            // 
            this.CreatedDate.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CreatedDate]")});
            this.CreatedDate.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatedDate.ForeColor = System.Drawing.Color.Gray;
            this.CreatedDate.LocationFloat = new DevExpress.Utils.PointFloat(961.7339F, 42.21865F);
            this.CreatedDate.Name = "CreatedDate";
            this.CreatedDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.CreatedDate.SizeF = new System.Drawing.SizeF(188.3751F, 24.19434F);
            this.CreatedDate.StyleName = "PageInfo";
            this.CreatedDate.StylePriority.UsePadding = false;
            this.CreatedDate.Text = "created date";
            this.CreatedDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CreatedBy]")});
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.Gray;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(961.7339F, 15.07733F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.SizeF = new System.Drawing.SizeF(188.3751F, 24.19434F);
            this.xrLabel2.StyleName = "Title";
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Note:this is footer text";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // pageInfo2
            // 
            this.pageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pageInfo2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(556.2967F, 42.00002F);
            this.pageInfo2.Name = "pageInfo2";
            this.pageInfo2.SizeF = new System.Drawing.SizeF(133.9902F, 23.00001F);
            this.pageInfo2.StyleName = "PageInfo";
            this.pageInfo2.StylePriority.UseBorders = false;
            this.pageInfo2.StylePriority.UseFont = false;
            this.pageInfo2.StylePriority.UseTextAlignment = false;
            this.pageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.pageInfo2.TextFormatString = "{0}/{1}";
            // 
            // CorporationLogo
            // 
            this.CorporationLogo.BackColor = System.Drawing.Color.Transparent;
            this.CorporationLogo.ImageSource = new DevExpress.XtraPrinting.Drawing.ImageSource("img", resources.GetString("CorporationLogo.ImageSource"));
            this.CorporationLogo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12.53866F);
            this.CorporationLogo.Name = "CorporationLogo";
            this.CorporationLogo.NavigateUrl = "https://www.securitastechnology.com.tr/";
            this.CorporationLogo.SizeF = new System.Drawing.SizeF(136F, 52.68F);
            this.CorporationLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.CorporationLogo.StylePriority.UseBackColor = false;
            // 
            // tip
            // 
            this.tip.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tip.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.tip.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.tip.BorderWidth = 2F;
            this.tip.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tip.ForeColor = System.Drawing.SystemColors.GrayText;
            this.tip.Name = "tip";
            this.tip.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel2});
            this.PageHeader.HeightF = 75F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrPanel2
            // 
            this.xrPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.SubTitleLbl,
            this.TitleLbl,
            this.xrLine1,
            this.ApplicationLogo});
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(1169F, 75F);
            this.xrPanel2.StylePriority.UseBackColor = false;
            // 
            // SubTitleLbl
            // 
            this.SubTitleLbl.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SubTitle]")});
            this.SubTitleLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubTitleLbl.LocationFloat = new DevExpress.Utils.PointFloat(188.17F, 38.70831F);
            this.SubTitleLbl.Name = "SubTitleLbl";
            this.SubTitleLbl.SizeF = new System.Drawing.SizeF(961.9391F, 31.29169F);
            this.SubTitleLbl.StylePriority.UseFont = false;
            this.SubTitleLbl.StylePriority.UseTextAlignment = false;
            this.SubTitleLbl.Text = "titleLbl";
            this.SubTitleLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TitleLbl
            // 
            this.TitleLbl.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Title]")});
            this.TitleLbl.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLbl.LocationFloat = new DevExpress.Utils.PointFloat(188.17F, 3.916668F);
            this.TitleLbl.Name = "TitleLbl";
            this.TitleLbl.SizeF = new System.Drawing.SizeF(961.94F, 34.79166F);
            this.TitleLbl.StyleName = "Title";
            this.TitleLbl.StylePriority.UseFont = false;
            this.TitleLbl.StylePriority.UseTextAlignment = false;
            this.TitleLbl.Text = "TrafficSummaryReport";
            this.TitleLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.BorderColor = System.Drawing.Color.Transparent;
            this.xrLine1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160)))));
            this.xrLine1.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine1.LineWidth = 2F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(171.0867F, 8.083355F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(2.083344F, 52.68497F);
            this.xrLine1.StylePriority.UseBorderColor = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // ApplicationLogo
            // 
            this.ApplicationLogo.BackColor = System.Drawing.Color.Transparent;
            this.ApplicationLogo.ImageSource = new DevExpress.XtraPrinting.Drawing.ImageSource("img", resources.GetString("ApplicationLogo.ImageSource"));
            this.ApplicationLogo.LocationFloat = new DevExpress.Utils.PointFloat(20F, 10.08335F);
            this.ApplicationLogo.Name = "ApplicationLogo";
            this.ApplicationLogo.NavigateUrl = "https://Traffic.countware.net/";
            this.ApplicationLogo.SizeF = new System.Drawing.SizeF(136F, 52.68497F);
            this.ApplicationLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.ApplicationLogo.StylePriority.UseBackColor = false;
            this.ApplicationLogo.StylePriority.UseBorderColor = false;
            // 
            // DailyTrafficReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.BottomMargin,
            this.MainDetail,
            this.SummaryByStore,
            this.ComparedStoresSummary,
            this.TrafficByHourCharts,
            this.TrafficByStoreCharts,
            this.PageFooter,
            this.PageHeader});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource2});
            this.DataSource = this.objectDataSource2;
            this.DrawWatermark = true;
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.GroupCaption2,
            this.GroupData2,
            this.DetailCaption2,
            this.DetailData2,
            this.GroupFooterBackground3,
            this.DetailData3_Odd,
            this.PageInfo,
            this.tip});
            this.Version = "22.1";
            this.Watermark.Font = new System.Drawing.Font("Calibri", 120F);
            this.Watermark.TextTransparency = 193;
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryByStoreHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComparedStoresSummaryHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficByHourChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficbyStoreChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin; 
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.DetailBand MainDetail;
        private DevExpress.XtraReports.UI.DetailReportBand SummaryByStore;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRControlStyle Title;
        private DevExpress.XtraReports.UI.XRControlStyle GroupCaption2;
        private DevExpress.XtraReports.UI.XRControlStyle GroupData2;
        private DevExpress.XtraReports.UI.XRControlStyle DetailCaption2;
        private DevExpress.XtraReports.UI.XRControlStyle DetailData2;
        private DevExpress.XtraReports.UI.XRControlStyle GroupFooterBackground3;
        private DevExpress.XtraReports.UI.XRControlStyle DetailData3_Odd;
        private DevExpress.XtraReports.UI.XRControlStyle PageInfo;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource2;
        private DevExpress.XtraReports.UI.DetailReportBand ComparedStoresSummary;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable ComparedStoresSummaryHeaderTable;
        private DevExpress.XtraReports.UI.XRTableRow ComparedStoresSummaryTableHeaderRow;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRLabel ComparedStoresSummaryTitle;
        private DevExpress.XtraReports.UI.XRLabel ComparedStoresSummarySubTitle;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRPictureBox weatherIcon;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreFooterTitle;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRLabel ComparedStoresSummaryFooterTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.DetailReportBand TrafficByHourCharts;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRChart TrafficByHourChart;
        private DevExpress.XtraReports.UI.DetailReportBand TrafficByStoreCharts;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRLabel TrafficByHourChartsTitle;
        private DevExpress.XtraReports.UI.XRLabel TrafficByHourChartsSubTitle;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter2;
        private DevExpress.XtraReports.UI.XRLabel TrafficByHourChartsFooterText;
        private DevExpress.XtraReports.UI.XRChart TrafficbyStoreChart;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel TrafficByStoreChartsTitle;
        private DevExpress.XtraReports.UI.XRLabel TrafficByStoreChartsSubTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreSubTitle;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreTitle;
        private DevExpress.XtraReports.UI.XRTable SummaryByStoreHeaderTable;
        private DevExpress.XtraReports.UI.XRTableRow SummaryTableHeaderRow;
        private DevExpress.XtraReports.UI.XRTableCell tableCell4;
        private DevExpress.XtraReports.UI.XRTableCell tableCell5;
        private DevExpress.XtraReports.UI.XRTableCell tableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell tableCell9;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRPageInfo pageInfo2;
        private DevExpress.XtraReports.UI.XRPictureBox CorporationLogo;
        private DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRLabel SubTitleLbl;
        private DevExpress.XtraReports.UI.XRLabel TitleLbl;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRPictureBox ApplicationLogo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRControlStyle tip;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter3;
        private DevExpress.XtraReports.UI.XRLabel TrafficByStoreChartsFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRTable table3;
        private DevExpress.XtraReports.UI.XRTableRow tableRow3;
        private DevExpress.XtraReports.UI.XRTableCell tableCell29;
        private DevExpress.XtraReports.UI.XRTableCell tableCell30;
        private DevExpress.XtraReports.UI.XRTableCell tableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell tableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel CreatedDate;
        private DevExpress.XtraReports.UI.XRLabel Total;
    }
}
