﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;

namespace CountwareTraffic.Services.Devices.Core
{
    public record DeviceConnectionInfo : IDomainEventRaisable,  IDeletableValueObject
    {
        private string _ipAddress;
        private int _port;
        private string _identity;
        private string _password;
        private string _uniqueId;
        private string _macAddress;

        public string IpAddress => _ipAddress;
        public int Port => _port;
        public string Identity => _identity;
        public string Password => _password;
        public string UniqueId => _uniqueId;
        public string MacAddress => _macAddress;

        [NotMapped]
        public IEnumerable<IDomainEvent> Events { get; set; }
        public void AddEvent(IDomainEvent @event)
            => ((List<IDomainEvent>)Events).Add(@event);

        public void ClearEvents()
             => ((List<IDomainEvent>)Events).Clear();

        private DeviceConnectionInfo() { Events = new List<IDomainEvent>(); }

        public static DeviceConnectionInfo Create(string ipAddress, int port, string identity, string password, string uniqueId, string macAddress)
        {
            if (!IPAddress.TryParse(ipAddress, out IPAddress ipA))
                throw new IpAddressIsEmptyOrWrongFormatException(ipAddress);

            if (ipAddress.Length > 15 )
                throw new ArgumentException(nameof(ipAddress));

            if (macAddress.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(macAddress));

            if (macAddress.Length > 17)
                throw new ArgumentException(nameof(macAddress));

            if (port < 1 || port > 65535)
                throw new InvalidPortNumberException(port);


            if (!identity.IsNullOrWhiteSpace() && identity.Length > 50)
                throw new ArgumentException(nameof(identity));

            if (!password.IsNullOrWhiteSpace() && password.Length > 16)
                throw new ArgumentException(nameof(password));

            if (!uniqueId.IsNullOrWhiteSpace() && uniqueId.Length > 75)
                throw new ArgumentException(nameof(uniqueId));

            return new DeviceConnectionInfo
            {
                _ipAddress = ipAddress,
                _port = port,
                _identity = identity,
                _password = password,
                _uniqueId = uniqueId,
                _macAddress = macAddress
            };
        }

        public void WhenChanged(string ipAddress, int port, string identity, string password, string uniqueId, string macAddress)
        {
            if (!IPAddress.TryParse(ipAddress, out IPAddress ipA))
                throw new IpAddressIsEmptyOrWrongFormatException(ipAddress);

            if (ipAddress.Length > 15)
                throw new ArgumentException(nameof(ipAddress));

            if (macAddress.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(macAddress));

            if (macAddress.Length > 17)
                throw new ArgumentException(nameof(macAddress));

            if (port < 1 || port > 65535)
                throw new InvalidPortNumberException(port);


            if (!identity.IsNullOrWhiteSpace() && identity.Length > 50)
                throw new ArgumentException(nameof(identity));

            if (!password.IsNullOrWhiteSpace() && password.Length > 16)
                throw new ArgumentException(nameof(password));

            if (!uniqueId.IsNullOrWhiteSpace() && uniqueId.Length > 75)
                throw new ArgumentException(nameof(uniqueId));

            _ipAddress = ipAddress;
            _port = port;
            _identity = identity;
            _password = password;
            _uniqueId = uniqueId;
            _macAddress = macAddress;
        }

        public void WhenChangedCompleted(Guid deviceId, string name, string odlName, bool isActive,  string macAddress, string ipAddress, int controlFrequency)
        { 
            AddEvent(new DeviceChanged(deviceId, name, macAddress, ipAddress, isActive, odlName, controlFrequency));
        }
    }
}

