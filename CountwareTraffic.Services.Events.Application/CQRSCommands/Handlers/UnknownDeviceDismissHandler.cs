﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class UnknownDeviceDismissHandler : ICommandHandler<UnknownDeviceDismiss>
    {
        private readonly IUnitOfWork _unitOfWork;
        public UnknownDeviceDismissHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task HandleAsync(UnknownDeviceDismiss command)
        {
            var unknownDeviceRepository = _unitOfWork.GetRepository<IUnknownDeviceRepository>();

            var unknownDevice = await unknownDeviceRepository.GetByIdAsync(command.Id);

            unknownDevice.AuditIsDeleted = true;

            await _unitOfWork.CommitAsync();
        }
    }
}
