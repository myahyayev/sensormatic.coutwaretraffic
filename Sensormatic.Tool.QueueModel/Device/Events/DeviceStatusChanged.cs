﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceStatusChanged : MessageEnvelope, IQueueEvent
    {
        public DeviceStatusChanged(Guid deviceId, int deviceStatus, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            DeviceId = deviceId;
            DeviceStatus = deviceStatus;
            RecordId = Guid.NewGuid();
        }
        public Guid DeviceId { get; }
        public int DeviceStatus { get; }
        public Guid RecordId { get; init; }
    }
}
