﻿using CountwareTraffic.Services.Reporting.Application;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Sensormatic.Tool.MongoDb;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ResourceMongoDbRepository : MongoDbRepository<Resource>, IResourceMongoDbRepository
    {
        public ResourceMongoDbRepository(IConfiguration configuration) : base(configuration, "ReportResources", "Resources") { }

        public async new Task AddAsync(Resource resource) => await base.AddAsync(resource);

        public async Task<ResourceDto> GetAsync(Guid languageId, string key)
        {
            var resource = await Collection.Find(x => x.LanguageId == languageId.ToString() && x.ResouceKey == key)
                 .Project(r => new ResourceDto
                 {
                     LanguageId = r.LanguageId,
                     ResouceKey = r.ResouceKey,
                     Content = r.Content,
                 })
                 .FirstOrDefaultAsync();
            return resource;
        }

        public async Task<List<ResourceDto>> GetByAssemblyQualifiedNameAsync(string assemblyQualifiedName)
        {
            var resource = await Collection.Find(x=> x.AssemblyQualifiedName == assemblyQualifiedName)
                     .Project(r => new ResourceDto
                     {
                         LanguageId = r.LanguageId,
                         ResouceKey = r.ResouceKey,
                         Content = r.Content,
                     })
                     .ToListAsync();
            return resource;
        }
    }
}
