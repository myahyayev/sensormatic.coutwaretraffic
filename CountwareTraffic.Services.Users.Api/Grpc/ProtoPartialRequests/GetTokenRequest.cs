﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Users.Grpc
{
    [ServiceLog]
    public sealed partial class GetTokenRequest : SensormaticRequestValidate
    {
        internal Guid? _TenantId
        {
            get
            {
                if (Guid.TryParse(tenantId_, out Guid id))
                    return id;

                return null;
            }
            set { tenantId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (UserName.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(UserName)));

            if (Password.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(Password)));
        }
    }
}
