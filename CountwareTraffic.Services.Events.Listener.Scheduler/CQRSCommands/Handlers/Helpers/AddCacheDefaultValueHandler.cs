﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Cache;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class AddCacheDefaultValueHandler : ICommandHandler<AddCacheDefaultValue>
    {
        private readonly ILogger<AddCacheDefaultValueHandler> _logger;
        private readonly ICache _redisCacheService;

        public AddCacheDefaultValueHandler(ILogger<AddCacheDefaultValueHandler> logger, ICache redisCacheService)
        {
            _logger = logger;
            _redisCacheService = redisCacheService;
        }

        public async Task HandleAsync(AddCacheDefaultValue command)
        {
            var redisKey = string.Format(CacheKeys.DeviceRealTimeLatestEvent, command.DeviceId);

            var result = await _redisCacheService.AddAsync(redisKey, new LatestEventDto
            {
                TotalEnterCount = command.TotalEnterCount,
                TotalExitCount = command.TotalExitCount,
                DeviceId = command.DeviceId
            }, expireTime: null, @override: true);

            if (result)
            {
                _logger.LogInformation($"{command.DeviceId} cache data has been added!");
            }
        }
    }
}
