﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer
{
    public class GetContractsByParametersApiResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<GetContractsByParametersItemApiResponse> Data { get; set; }
    }

    public class GetContractsByParametersItemApiResponse
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("version")]
        public string Version { get; set; }

        [JsonPropertyName("releaseDate")]
        public DateTime ReleaseDate { get; set; }

        [JsonPropertyName("contractType")]
        public string ContractType { get; set; }

        [JsonPropertyName("appType")]
        public int AppType { get; set; }
    }
}
