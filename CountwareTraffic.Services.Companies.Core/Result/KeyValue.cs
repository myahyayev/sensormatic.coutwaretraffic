﻿using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class KeyValue
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
