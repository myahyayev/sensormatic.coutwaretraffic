﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteGroupHandler : IQueryHandler<DeleteGroup, IdentityDeleteGroupResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public DeleteGroupHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityDeleteGroupResponse> HandleAsync(DeleteGroup query)
        {
            return await _identityServerService.DeleteGroupAsync(query.Id);
        }
    }
}
