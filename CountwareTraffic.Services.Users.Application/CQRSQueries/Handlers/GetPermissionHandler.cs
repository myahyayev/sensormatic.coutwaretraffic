﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetPermissionHandler : IQueryHandler<GetPermission, IdentityPermissionResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetPermissionHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityPermissionResponse> HandleAsync(GetPermission query)
        {
            return await _identityServerService.GetPermissionAsync(query.Id, query.Language);
        }
    }
}
