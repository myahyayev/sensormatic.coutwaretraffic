﻿using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Engine.TrafficReport
{
    public class DailyTrafficReportData : BaseReportData
    {
        public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> ComparedStoresSummary { get; set; }
        public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> SummaryByStore { get; set; }
        public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> TrafficByHourChart { get; set; }
        public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> TrafficByStoreChart { get; set; }
    }
}
