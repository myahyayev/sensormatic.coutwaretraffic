﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record SubAreaCreatedRejected : MessageEnvelope, IQueueEvent
    {
        public SubAreaCreatedRejected(Guid subAreaId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            SubAreaId = subAreaId;
        }
        public Guid SubAreaId { get; }
        public Guid RecordId { get; init; }
    }
}
