﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetRegionCountryIdHandler : IQueryHandler<GetRegionCountryId, Guid>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetRegionCountryIdHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<Guid> HandleAsync(GetRegionCountryId query)
        {
            var region = await _unitOfWork.GetRepository<IRegionRepository>().GetByIdAsync(query.RegionId);

            var country = await _unitOfWork.GetRepository<ICountryRepository>().GetByIdAsync(region.CountryId);

            return country.CountryLookupId;
        }
    }
}
