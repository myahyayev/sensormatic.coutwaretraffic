﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Reporting.Infrastructure.Migrations
{
    public partial class ReportLanguageAddShortName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ShortName",
                schema: "reports",
                table: "ReportLanguages",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShortName",
                schema: "reports",
                table: "ReportLanguages");
        }
    }
}
