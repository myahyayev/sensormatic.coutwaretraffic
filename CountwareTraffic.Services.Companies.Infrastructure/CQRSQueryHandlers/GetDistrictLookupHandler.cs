﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetDistrictLookupHandler : IQueryHandler<GetDistrictLookup, DistrictLookupDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetDistrictLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<DistrictLookupDetailsDto> HandleAsync(GetDistrictLookup query)
        {
            var districtLookup = await _unitOfWork.GetRepository<IDistrictLookupRepository>().GetByIdAsync(query.DistrictId);

            if (districtLookup == null)
                throw new DistrictLookupNotFoundException(query.DistrictId);

            return new DistrictLookupDetailsDto
            {
                Id = districtLookup.Id,
                Name = districtLookup.Name,
                CityId = districtLookup.CityId,
            };
        }
    }
}
