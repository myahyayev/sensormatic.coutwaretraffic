﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Devices.Core
{
    public class BrandModelLookup : Entity<Guid>
    {
        private string _name;
        private Guid _brandId;

        public string Name => _name;
        public Guid BrandId => _brandId;

        private BrandModelLookup() => _id = SequentialGuid.Current.Next(null);

        public static BrandModelLookup Create(string name, Guid brandId)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(name));

            if (name.Length > 150)
                throw new ArgumentException(nameof(name));

            return new BrandModelLookup
            {
                _name = name,
                _brandId = brandId
            };
        }

        public void Change(string name)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(name));

            if (name.Length > 150)
                throw new ArgumentException(nameof(name));

            _name = name;
        }
    }
}
