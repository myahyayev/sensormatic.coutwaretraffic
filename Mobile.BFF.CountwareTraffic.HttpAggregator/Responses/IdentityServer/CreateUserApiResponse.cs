﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class CreateUserApiResponse
    {
        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("status")]
        public int Status { get; set; }

        [JsonPropertyName("traceId")]
        public string TraceId { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("errors")]
        public CreateUserApiErrors Errors { get; set; }
    }

    public class CreateUserApiErrors
    {

        [JsonPropertyName("email")]
        public List<string> Email { get; set; }

        [JsonPropertyName("firstName")]
        public List<string> FirstName { get; set; }

        [JsonPropertyName("lastName")]
        public List<string> LastName { get; set; }

        [JsonPropertyName("userName")]
        public List<string> UserName { get; set; }

        [JsonPropertyName("phoneNumber")]
        public List<string> PhoneNumber { get; set; }

        [JsonPropertyName("password")]
        public List<string> Password { get; set; }

        [JsonPropertyName("confirmPassword")]
        public List<string> ConfirmPassword { get; set; }
    }
}
