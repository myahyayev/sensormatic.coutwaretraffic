﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetPerformingByTypeApiRequest
    {
        public List<HierarchyTreeSelectFilterRequest> HierarchyTreeSelectFilters { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Direction Direction { get; set; }
        public PerformType PerformType { get; set; }
        public int Size { get; set; }
    }
}
