﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportGeneratingFailed : IDomainEvent
    {
        public Guid ReportId { get; init; }
        public Guid FileId { get; init; }
        public Guid RecordId { get; init; }
        public int ReportStatus { get; init; }
        public Guid TenantId { get; init; }

        public ReportGeneratingFailed(Guid reportId, Guid fileId, int reportStatus, Guid tenantId)
        {
            ReportId = reportId;
            FileId = fileId;
            ReportStatus = reportStatus;
            RecordId = Guid.NewGuid();
            TenantId = tenantId;
        }
    }
}
