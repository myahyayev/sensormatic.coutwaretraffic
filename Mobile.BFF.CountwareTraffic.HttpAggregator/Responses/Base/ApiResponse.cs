﻿using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ApiResponse<T>
    {
        public T Data { get; set; }
        public MessageResponse Message { get; set; }
    }

    public class MessageResponse
    {
        public MessageResponse()
        {
        }

        public MessageResponse(string title)
        {
            this.Title = title;
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public List<CallToAction> CallToActions { get; set; }
        public CallToActionType? CallToActionType { get; set; }
    }
    public class ResultMessage
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public CallToActionType? CallToActionType { get; set; }
        public static ResultMessage SuccessResultMessage
            => new ResultMessage
            {
                Description = "Success",
                Title = "#S1001 Success"
            };

        public ResultMessage()
        {

        }

        public ResultMessage(string successCode)
        {
            this.Description = "Success";
            this.Title = successCode + " Success";
        }
    }
}
