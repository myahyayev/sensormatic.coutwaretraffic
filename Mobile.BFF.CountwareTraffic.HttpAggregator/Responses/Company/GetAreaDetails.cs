﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetAreaDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid RegionId { get; set; }
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public string AreaTypeName { get; set; }
        public int AreaTypeId { get; set; }
        public string Street { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public string ManagerName { get; set; }
        public Guid? ManagerId { get; set; }

        //contact
        public string GsmNumber { get; set; }
        public string GsmCountryCode { get; set; }
        public string GsmDialCode { get; set; }
        public string PhoneDialCode { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneCountryCode { get; set; }
        public string EmailAddress { get; set; }
        public Guid DistrictId { get; set; }
        public Guid CityId { get; set; }
        public Guid CountryId { get; set; }
        public string WorkingHoursStart { get; set; }
        public string WorkingHoursEnd { get; set; }
        public string WorkingTimeZone { get; set; }
    }
}
