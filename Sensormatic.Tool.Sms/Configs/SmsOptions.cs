﻿using Sensormatic.Tool.Ioc;

namespace Sensormatic.Tool.Sms
{
    public class SmsOptions : IConfigurationOptions
    {
        public string Url { get; set; }
        public int PlatformId { get; set; }
        public int ChnCode { get; set; }
        public string User { get; set; }
        public string Pw { get; set; }
    }
}