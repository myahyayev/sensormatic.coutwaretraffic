﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]

    //ControllerBase was used instead of BaseController due to log traffic,use it in testing.
    public class HikvisionController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly ILogger<HikvisionController> _logger;
        public HikvisionController(
            ILogger<HikvisionController> logger,
            ICommandDispatcher commandDispatcher)
        {
            _commandDispatcher = commandDispatcher;
            _logger = logger;
        }

        [HttpPost("realtime/events/xml")]
        public async Task<OkObjectResult> CreateEventAsync()
        {
            // _logger.LogInformation($" Hikvision realtime start to read xml body.....");
            using (StreamReader sReader = new StreamReader(Request.Body))
            {
                string xmlBody = await sReader.ReadToEndAsync();

                _logger.LogInformation($"{xmlBody}");

                await _commandDispatcher.SendAsync(new HikvisionRealTimeEventLogXmlFormat()
                {
                    Xml = xmlBody
                });
            }
            return Ok(new { Success = "Success" });
        }

        [HttpPost("timerange/events/xml")]
        public async Task<OkObjectResult> CreateTimeRangeEventAsync()
        {
            //_logger.LogInformation($" Hikvision TimeRange start to read xml body.....");
            using (StreamReader sReader = new StreamReader(Request.Body))
            {
                string xmlBody = await sReader.ReadToEndAsync();

                _logger.LogInformation($"{xmlBody}");

                await _commandDispatcher.SendAsync(new HikvisionTimeRangeEventLogXmlFormat()
                {
                    Xml = xmlBody
                });
            }
            return Ok(new { Success = "Success" });
        }



    }

}





























































//[HttpPost("realtime/events/xml"), ServiceLog]
//public async Task<ApiResponse<CrudApiResponse>> CreateEventAsync()
//{
//    try
//    {
//        Request.EnableBuffering();

//        _logger.LogInformation(Request.ContentType);

//        var reader = new StreamReader(Request?.Body, Encoding.UTF8);

//        if (reader != null)
//        {
//            reader.BaseStream.Seek(0, SeekOrigin.Begin);

//            var payload = reader.ReadToEnd();

//            _logger.LogInformation($"Hikvision xml file : {payload}");

//            await _commandDispatcher.SendAsync(new HikvisionTimeRangeEventLogXmlFormat()
//            {
//                Xml = payload
//            });

//            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Create }, ResultMessage.SuccessResultMessage);
//        }
//        else
//        {
//            _logger.LogError("Hikvision xml file is empty!");
//            return Response(new CrudApiResponse { IsSuccess = false, OperationType = OperationType.Create }, ResultMessage.FailedResultMessage);
//        }
//    }
//    catch (Exception e)
//    {
//        _logger.LogError($"{e.GetType().FullName}  {e.Message}   {e.StackTrace}");
//        throw e;
//    }
//}