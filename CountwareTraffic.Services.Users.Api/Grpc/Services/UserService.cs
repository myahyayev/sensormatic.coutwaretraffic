﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application;
using CountwareTraffic.Services.Users.Application.CQRSCommands;
using CountwareTraffic.Services.Users.Application.CQRSQueries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Sensormatic.Tool.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Grpc
{
    [Authorize]
    public class UserService : User.UserBase
    {
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly ICommandDispatcher _commandDispatcher;
        public UserService(IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher)
        {
            _queryDispatcher = queryDispatcher;
            _commandDispatcher = commandDispatcher;
        }

        [AllowAnonymous]
        public override async Task<GetTenantsResponse> GetTenants(Empty request, ServerCallContext context)
        {

            var response = await _queryDispatcher.QueryAsync(new GetTenants());

            GetTenantsResponse grpcResponse = new();

            response.Tenants.ForEach(u => grpcResponse.Tenants.Add(new Tenant
            {
                TenantId = u.Id.ToString(),
                Name = u.Name,
                Description = u.Description
            }));

            return grpcResponse;
        }

        [AllowAnonymous]
        public async override Task<GetTokenResponse> GetToken(GetTokenRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new GetToken
            {
                UserName = request.UserName,
                Password = request.Password,
                TenantId = request._TenantId,
                Language = request.Language
            });

            GetTokenResponse grpcResponse = new()
            {
                LoginStatus = response.LoginStatus switch
                {
                    Application.LoginStatus.Unknown => LoginStatus.Unknown,
                    Application.LoginStatus.FirstLogin => LoginStatus.FirstLogin,
                    Application.LoginStatus.AlreadyLogged => LoginStatus.AlreadyLogged,
                    Application.LoginStatus.AuthorityNull => LoginStatus.AuthorityNull,
                    Application.LoginStatus.VerifyToken => LoginStatus.VerifyToken,
                    _ => throw new System.NotImplementedException(),
                }
            };


            grpcResponse.AuthorityNull = response.AuthorityNull != null
                ?
                new AuthorityNullModel
                {
                    AuthToken = response.AuthorityNull.AuthToken
                }
                : null;


            grpcResponse.FirstLogin = response.FirstLogin != null
                ?
                new FirstLoginModel
                {
                    Message = response.FirstLogin.Message
                }
                : null;


            grpcResponse.AlreadyLogged = response.AlreadyLogged != null
                ?
                new AlreadyLoggedModel
                {
                    AccessToken = response.AlreadyLogged.AccessToken,
                    Email = response.AlreadyLogged.Email,
                    ExpireTime = Timestamp.FromDateTimeOffset(response.AlreadyLogged.ExpireTime),
                    FamilyName = response.AlreadyLogged.FamilyName,
                    PhoneNumber = response.AlreadyLogged.PhoneNumber,
                    GivenName = response.AlreadyLogged.GivenName,
                    UniqueName = response.AlreadyLogged.UniqueName,
                    RefreshToken = response.AlreadyLogged.RefreshToken,
                    Gender = response.AlreadyLogged.Gender switch
                    {
                        Application.Gender.Male => Gender.Male,
                        Application.Gender.Femail => Gender.Femail,
                        _ => throw new System.NotImplementedException(),
                    }
                }
                : null;


            grpcResponse.VerifyToken = response.VerifyToken != null
                ?
                new VerifyTokenModel()
                {
                    Authority = response.VerifyToken.Authority,
                    VerifyToken = response.VerifyToken.VerifyToken,
                    TimeOut = response.VerifyToken.TimeOut
                }
                : null;

            if (grpcResponse.VerifyToken?.Parameters != null)
                FillRepeatedData(response.VerifyToken.Parameters, grpcResponse.VerifyToken.Parameters);

            grpcResponse.OtpDetail = response.OtpDetail != null
                ?
                new OtpDetailModel()
                {
                    ReferenceText = response.OtpDetail.ReferenceText
                }
                : null;

            static void FillRepeatedData(string[] data, Google.Protobuf.Collections.RepeatedField<string> repeatedField) => data.ToList().ForEach(u => repeatedField.Add(u));

            return grpcResponse;
        }

        [AllowAnonymous]
        public async override Task<ResetPasswordWithTokenResponse> ResetPasswordWithToken(ResetPasswordWithTokenRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new ResetPasswordWithToken
            {
                Token = request.Token,
                UserName = request.UserName,
                Password = request.Password,
                ConfirmPassword = request.ConfirmPassword
            });

            return new ResetPasswordWithTokenResponse
            {
                Message = response.Message
            };
        }

        public override async Task<ChangePasswordResponse> ChangePassword(ChangePasswordRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new ChangePassword
            {
                UserName = request.UserName,
                CurrentPassword = request.CurrentPassword,
                NewPassword = request.NewPassword,
                ConfirmPassword = request.ConfirmPassword
            });

            return new ChangePasswordResponse
            {
                Message = response.Message
            };
        }

        [AllowAnonymous]
        public override async Task<RefreshTokenResponse> RefreshToken(RefreshTokenRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new RefreshToken
            {
                AccessToken = request.AccessToken,
                RefreshTokenKey = request.RefreshTokenKey
            });

            return new RefreshTokenResponse
            {
                AccessToken = response.AccessToken,
                RefreshTokenKey = response.RefreshToken
            };
        }

        public override async Task<UserGridResponse> GetUserGrid(UserGridRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetUserGrid
            {
                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            var response = new UserGridResponse
            {
                TotalCount = tempResponse.TotalCount,
                Page = tempResponse.Page,
                Limit = tempResponse.Limit,
                HasNextPage = tempResponse.HasNextPage,
                Next = tempResponse.Next,
                Prev = tempResponse.Prev
            };

            tempResponse.Data.ForEach(data => response.UserGridData.Add(new UserGridData
            {
                Id = data.Id.ToString(),
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                PhoneNumber = data.PhoneNumber,
                UserName = data.UserName,
                Status = data.Status,
                TwoFactorEnabled = data.TwoFactorEnabled,
                Gender = data.Gender,
                Birthdate = Timestamp.FromDateTimeOffset(data.Birthdate),
                Address = data.Address,
                AllowSms = data.AllowSms,
                AllowEmail = data.AllowEmail,
                ProfilePic = data.ProfilePic
            }));

            return response;
        }

        public override async Task<UserResponse> GetUser(Empty request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new GetUser { });

            return new UserResponse
            {
                Id = response.Id.ToString(),
                FirstName = response.FirstName,
                LastName = response.LastName,
                Email = response.Email,
                PhoneNumber = response.PhoneNumber,
                UserName = response.UserName,
                Status = response.Status,
                TwoFactorEnabled = response.TwoFactorEnabled,
                Gender = response.Gender,
                Birthdate = Timestamp.FromDateTimeOffset(response.Birthdate),
                Address = response.Address,
                AllowSms = response.AllowSms,
                AllowEmail = response.AllowEmail,
                ProfilePic = response.ProfilePic
            };
        }

        public override async Task<CreateUserResponse> CreateUser(CreateUserRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new CreateUser
            {
                Id = Guid.Parse(request.Id),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                UserName = request.UserName,
                Status = request.Status.HasValue ? request.Status.Value : false,
                TwoFactorEnabled = request.TwoFactorEnabled.HasValue ? request.Status.Value : false,
                Gender = request.Gender,
                Birthdate = request.Birthdate.ToDateTime(),
                UserType = request.UserType,
                Address = request.Address,
                AllowSms = request.AllowSms.HasValue ? request.AllowSms.Value : false,
                AllowEmail = request.AllowEmail.HasValue ? request.AllowEmail.Value : false,
                ProfilePic = request.ProfilePic,
                Areas = request.Areas.Select(area => new Application.AreaDetail { Id = Guid.Parse(area.Id), Level = area.Level.ToString() }).ToList(),
                Roles = request.Roles.ToList(),
                Password = request.Password,
                ConfirmPassword = request.ConfirmPassword
            });

            var response = new CreateUserResponse
            {
                Type = tempResponse.Type,
                Title = tempResponse.Title,
                Status = tempResponse.Status,
                TraceId = tempResponse.TraceId,
                Message = tempResponse.Message
            };

            response.Errors = new CreateUserError { };

            if (tempResponse.Errors != null)
            {
                if (tempResponse.Errors.Email != null)
                    tempResponse.Errors.Email.ForEach(email => response.Errors.Email.Add(email));
                if (tempResponse.Errors.FirstName != null)
                    tempResponse.Errors.FirstName.ForEach(firstName => response.Errors.FirstName.Add(firstName));
                if (tempResponse.Errors.LastName != null)
                    tempResponse.Errors.LastName.ForEach(lastName => response.Errors.LastName.Add(lastName));
                if (tempResponse.Errors.UserName != null)
                    tempResponse.Errors.UserName.ForEach(userName => response.Errors.UserName.Add(userName));
                if (tempResponse.Errors.PhoneNumber != null)
                    tempResponse.Errors.PhoneNumber.ForEach(phoneNumber => response.Errors.PhoneNumber.Add(phoneNumber));
                if (tempResponse.Errors.Password != null)
                    tempResponse.Errors.Password.ForEach(password => response.Errors.Password.Add(password));
                if (tempResponse.Errors.ConfirmPassword != null)
                    tempResponse.Errors.ConfirmPassword.ForEach(confirmPassword => response.Errors.ConfirmPassword.Add(confirmPassword));
            }

            return response;
        }

        public override async Task<UpdateUserResponse> UpdateUser(UpdateUserRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new UpdateUser
            {
                Id = Guid.Parse(request.Id),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                UserName = request.UserName,
                Status = request.Status.HasValue ? request.Status.Value : false,
                TwoFactorEnabled = request.TwoFactorEnabled.HasValue ? request.TwoFactorEnabled.Value : false,
                Gender = request.Gender,
                Birthdate = request.Birthdate.ToDateTime(),
                UserType = request.UserType,
                Address = request.Address,
                AllowSms = request.AllowSms.HasValue ? request.AllowSms.Value : false,
                AllowEmail = request.AllowEmail.HasValue ? request.AllowEmail.Value : false,
                ProfilePic = request.ProfilePic,
                Areas = request.Areas.Select(area => new Application.AreaDetail { Id = Guid.Parse(area.Id), Level = area.Level.ToString() }).ToList(),
                Roles = request.Roles.ToList()
            });

            var response = new UpdateUserResponse
            {
                Type = tempResponse.Type,
                Title = tempResponse.Title,
                Status = tempResponse.Status,
                TraceId = tempResponse.TraceId,
                Message = tempResponse.Message
            };

            response.Errors = new UpdateUserError { };

            if (tempResponse.Errors != null)
            {
                if (tempResponse.Errors.Email != null)
                    tempResponse.Errors.Email.ForEach(email => response.Errors.Email.Add(email));
                if (tempResponse.Errors.FirstName != null)
                    tempResponse.Errors.FirstName.ForEach(firstName => response.Errors.FirstName.Add(firstName));
                if (tempResponse.Errors.LastName != null)
                    tempResponse.Errors.LastName.ForEach(lastName => response.Errors.LastName.Add(lastName));
                if (tempResponse.Errors.UserName != null)
                    tempResponse.Errors.UserName.ForEach(userName => response.Errors.UserName.Add(userName));
                if (tempResponse.Errors.PhoneNumber != null)
                    tempResponse.Errors.PhoneNumber.ForEach(phoneNumber => response.Errors.PhoneNumber.Add(phoneNumber));
            }

            return response;
        }

        public override async Task<GetUserIdResponse> GetUserId(UserIdRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetUserId
            {
                Id = Guid.Parse(request.Id)
            });

            var response = new GetUserIdResponse
            {
                Id = tempResponse.Id.ToString(),
                FirstName = tempResponse.FirstName,
                LastName = tempResponse.LastName,
                Email = tempResponse.Email,
                PhoneNumber = tempResponse.PhoneNumber,
                UserName = tempResponse.UserName,
                Status = tempResponse.Status,
                TwoFactorEnabled = tempResponse.TwoFactorEnabled,
                Gender = tempResponse.Gender,
                Birthdate = Timestamp.FromDateTimeOffset(tempResponse.Birthdate),
                UserType = tempResponse.UserType,
                Address = tempResponse.Address,
                AllowSms = tempResponse.AllowSms,
                AllowEmail = tempResponse.AllowEmail,
                ProfilePic = tempResponse.ProfilePic

            };
            tempResponse.Areas.ForEach(area => response.Areas.Add(new AreaDetail { Id = area.Id.ToString(), Level = area.Level }));
            tempResponse.Roles.ForEach(role => response.Roles.Add(new RoleDetail { Id = role.Id.ToString(), Name = role.Name }));

            return response;
        }

        public override async Task<DeleteUserResponse> DeleteUser(UserIdRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new DeleteUser
            {
                Id = Guid.Parse(request.Id)
            });

            return new DeleteUserResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<UserResponse> GetUserUserName(UserNameRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new GetUserUserName
            {
                UserName = request.UserName
            });

            return new UserResponse
            {
                Id = response.Id.ToString(),
                FirstName = response.FirstName,
                LastName = response.LastName,
                Email = response.Email,
                PhoneNumber = response.PhoneNumber,
                UserName = response.UserName,
                Status = response.Status,
                TwoFactorEnabled = response.TwoFactorEnabled,
                Gender = response.Gender,
                Birthdate = Timestamp.FromDateTimeOffset(response.Birthdate),
                Address = response.Address,
                AllowSms = response.AllowSms,
                AllowEmail = response.AllowEmail,
                ProfilePic = response.ProfilePic
            };
        }

        public override async Task<UpdateUserResponse> CreateUserWithDefaultPassword(UpdateUserRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new CreateUserWithDefaultPassword
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                UserName = request.UserName,
                Status = request.Status.HasValue ? request.Status.Value : false,
                TwoFactorEnabled = request.TwoFactorEnabled.HasValue ? request.TwoFactorEnabled.Value : false,
                Gender = request.Gender,
                Birthdate = request.Birthdate.ToDateTime(),
                UserType = request.UserType,
                Address = request.Address,
                AllowSms = request.AllowSms.HasValue ? request.AllowSms.Value : false,
                AllowEmail = request.AllowEmail.HasValue ? request.AllowEmail.Value : false,
                ProfilePic = request.ProfilePic,
                Areas = request.Areas.Select(area => new Application.AreaDetail { Id = Guid.Parse(area.Id), Level = area.Level.ToString() }).ToList(),
                Roles = request.Roles.ToList(),
                Language = request.Language,
                Domain = request.Domain
            });

            var response = new UpdateUserResponse
            {
                Type = tempResponse.Type,
                Title = tempResponse.Title,
                Status = tempResponse.Status,
                TraceId = tempResponse.TraceId,
                Message = tempResponse.Message
            };

            response.Errors = new UpdateUserError { };
            if (tempResponse.Errors != null)
            {
                if (tempResponse.Errors.Email != null)
                    tempResponse.Errors.Email.ForEach(email => response.Errors.Email.Add(email));
                if (tempResponse.Errors.FirstName != null)
                    tempResponse.Errors.FirstName.ForEach(firstName => response.Errors.FirstName.Add(firstName));
                if (tempResponse.Errors.LastName != null)
                    tempResponse.Errors.LastName.ForEach(lastName => response.Errors.LastName.Add(lastName));
                if (tempResponse.Errors.UserName != null)
                    tempResponse.Errors.UserName.ForEach(userName => response.Errors.UserName.Add(userName));
                if (tempResponse.Errors.PhoneNumber != null)
                    tempResponse.Errors.PhoneNumber.ForEach(phoneNumber => response.Errors.PhoneNumber.Add(phoneNumber));
            }

            return response;
        }

        public override async Task<UserNameExistResponse> GetUserNameExist(UserNameExistRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new GetUserNameExist
            {
                UserName = request.UserName
            });

            return new UserNameExistResponse
            {
                Status = response.Status,
                Message = response.Message,
                Data = response.Data
            };
        }

        public override async Task<EmailExistResponse> GetEmailExist(EmailExistRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new GetEmailExist
            {
                Email = request.Email
            });

            return new EmailExistResponse
            {
                Status = response.Status,
                Message = response.Message,
                Data = response.Data
            };
        }

        public override async Task<ResetPasswordResponse> ResetPassword(ResetPasswordRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new ResetPassword
            {
                UserName = request.UserName
            });

            return new ResetPasswordResponse
            {
                Message = response.Message
            };
        }

        public override async Task<UserChangeStatusResponse> GetUserChangeStatus(UserChangeStatusRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetUserChangeStatus
            {
                Id = request.Id
            });

            var response = new UserChangeStatusResponse
            {
                Type = tempResponse.Type,
                Message = tempResponse.Message
            };
            tempResponse.Errors.ForEach(error => response.Errors.Add(new UserChangeStatusError { Message = error.Message }));
            return response;
        }


        public override async Task<EmailExistsResponse> GetEmailExists(EmailExistsRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new GetEmailExists
            {
                Email = request.Email
            });

            return new EmailExistsResponse
            {
                Response = response
            };
        }

        public override async Task<UserAvatarResponse> GetUserAvatar(UserAvatarRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new GetUserAvatar
            {
                Id = request.Id
            });

            return new UserAvatarResponse
            {
                Response = response
            };
        }

        public override async Task<UserResetResponse> UserReset(UserResetRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new UserReset
            {
                Id = request.Id
            });

            return new UserResetResponse
            {
                Response = response
            };
        }

        public override async Task<TenantGroupPaginationResponse> GetTenantGroupPagination(TenantGroupPaginationRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetTenantGroupPagination
            {
                Paging = new(request.DataSourceRequest.PagingRequest.Page, request.DataSourceRequest.PagingRequest.Limit),
                Sorts = request.DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),
                Filters = request.DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            var response = new TenantGroupPaginationResponse
            {
                TotalCount = tempResponse.TotalCount,
                Page = tempResponse.Page,
                Limit = tempResponse.Limit,
                HasNextPage = tempResponse.HasNextPage,
                Next = tempResponse.Next,
                Prev = tempResponse.Prev
            };

            tempResponse.Data.ToList().ForEach(data => response.Data.Add(new Tenant
            {
                TenantId = data.Id.ToString(),
                Name = data.Name,
                Description = data.Description
            }));

            return response;
        }

        public override async Task<TenantGroupResponse> GetTenantGroup(Empty request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetTenantGroup { });

            var response = new TenantGroupResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };

            tempResponse.Data.ToList().ForEach(data => response.Data.Add(new TenantGroupData
            {
                Id = data.Id.ToString(),
                Name = data.Name,
                Description = data.Description
            }));

            return response;
        }

        public override async Task<CreateTenantGroupResponse> CreateTenantGroup(CreateTenantGroupRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new CreateTenantGroup
            {
                Name = request.Name,
                Description = request.Description
            });

            return new CreateTenantGroupResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<UpdateTenantGroupResponse> UpdateTenantGroup(UpdateTenantGroupRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new UpdateTenantGroup
            {
                Id = Guid.Parse(request.Id),
                Name = request.Name,
                Description = request.Description
            });

            return new UpdateTenantGroupResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<DeleteTenantGroupResponse> DeleteTenantGroup(DeleteTenantGroupRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new DeleteTenantGroup
            {
                Id = Guid.Parse(request.Id)
            });

            var response = new DeleteTenantGroupResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };
            return response;
        }

        public override async Task<TenantGroupIdResponse> GetTenantGroupId(TenantGroupIdRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetTenantGroupId { Id = Guid.Parse(request.Id) });

            var response = new TenantGroupIdResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message,
                Data = new TenantGroupData
                {
                    Id = tempResponse.Data.Id.ToString(),
                    Name = tempResponse.Data.Name,
                    Description = tempResponse.Data.Description
                }
            };

            return response;
        }

        public override async Task<TenantGroupResponse> GetTenantGroupSelect(Empty request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetTenantGroupSelect { });

            var response = new TenantGroupResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };

            tempResponse.Data.ForEach(data => response.Data.Add(new TenantGroupData
            {
                Id = data.Id.ToString(),
                Name = data.Name,
                Description = data.Description
            }));

            return response;
        }

        public override async Task<CreateTenantResponse> CreateTenant(CreateTenantRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new CreateTenant
            {
                Name = request.Name,
                Description = request.Description,
                ErpCode = request.ErpCode,
                ErpGroupName = request.ErpGroupName,
                ErpStartDate = request.ErpStartDate.ToDateTime(),
                ErpEndDate = request.ErpEndDate.ToDateTime(),
                TenantGroupId = Guid.Parse(request.TenantGroupId)
            });

            return new CreateTenantResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<UpdateTenantResponse> UpdateTenant(UpdateTenantRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new UpdateTenant
            {
                Id = Guid.Parse(request.Id),
                Name = request.Name,
                Description = request.Description,
                ErpCode = request.ErpCode,
                ErpGroupName = request.ErpGroupName,
                ErpStartDate = request.ErpStartDate.ToDateTime(),
                ErpEndDate = request.ErpEndDate.ToDateTime(),
                TenantGroupId = Guid.Parse(request.TenantGroupId)
            });

            return new UpdateTenantResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<TenantSelectResponse> GetTenantSelect(Empty request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetTenantSelect { });

            var response = new TenantSelectResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };

            tempResponse.Data.ToList().ForEach(data => response.Data.Add(new TenantSelectData
            {
                Id = data.Id.ToString(),
                Name = data.Name
            }));

            return response;
        }

        public override async Task<GetRolePaginationResponse> GetRolePagination(GetRolePaginationRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetRolePagination
            {
                Paging = new(request.DataSourceRequest.PagingRequest.Page, request.DataSourceRequest.PagingRequest.Limit),
                Sorts = request.DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),
                Filters = request.DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList(),
                Language = request.Language
            });

            var response = new GetRolePaginationResponse
            {
                TotalCount = tempResponse.TotalCount,
                Page = tempResponse.Page,
                Limit = tempResponse.Limit,
                HasNextPage = tempResponse.HasNextPage,
                Next = tempResponse.Next,
                Prev = tempResponse.Prev
            };

            foreach (var data in tempResponse.Data)
            {
                var RolePaginationData = new RolePaginationData
                {
                    Id = data.Id.ToString(),
                    Name = data.Name,
                    Description = data.Description
                };
                data.Permissions.ForEach(permission => RolePaginationData.Permissions.Add(
                    new RolePaginationPermissionData
                    {
                        Id = permission.Id.ToString(),
                        Name = permission.Name,
                        Description = permission.Description,
                        Label = permission.Label,
                        DescriptionLabel = permission.DescriptionLabel
                    }
                ));
                response.Data.Add(RolePaginationData);
            }
            return response;
        }

        public override async Task<RolesResponse> GetRole(Empty request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetRole { });

            var response = new RolesResponse { };

            foreach (var data in tempResponse)
            {
                var roleResponse = new RoleData
                {
                    Id = data.Id.ToString(),
                    Name = data.Name,
                    Description = data.Description
                };
                response.Data.Add(roleResponse);
            }
            return response;
        }

        public override async Task<CreateRoleResponse> CreateRole(CreateRoleRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new CreateRole
            {
                Name = request.Name,
                Description = request.Description,
                Permissions = request.Permissions.ToList()
            });

            return new CreateRoleResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<UpdateRoleResponse> UpdateRole(UpdateRoleRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new UpdateRole
            {
                Id = Guid.Parse(request.Id),
                Name = request.Name,
                Description = request.Description,
                Permissions = request.Permissions.ToList()
            });

            return new UpdateRoleResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<RoleIdResponse> GetRoleId(RoleIdRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetRoleId
            {
                Id = Guid.Parse(request.Id),
                Language = request.Language
            });

            var response = new RoleIdResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message,
                Data = new RoleData
                {
                    Id = tempResponse.Data.Id.ToString(),
                    Name = tempResponse.Data.Name,
                    Description = tempResponse.Data.Description
                }
            };
            tempResponse.Data.Permissions.ForEach(permission => response.Data.Permissions.Add(new RolePermissionData
            {
                Id = permission.PermissionId.ToString(),
                Name = permission.Name,
                Description = permission.Description,
                Label = permission.Label,
                DescriptionLabel = permission.DescriptionLabel
            }));

            return response;
        }

        public override async Task<DeleteRoleResponse> DeleteRole(DeleteRoleRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new DeleteRole
            {
                Id = Guid.Parse(request.Id)
            });

            return new DeleteRoleResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<AddRoleToUserResponse> AddRoleToUser(AddRoleToUserRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new AddRoleToUser
            {
                UserId = Guid.Parse(request.UserId),
                RoleName = request.RoleName
            });

            return new AddRoleToUserResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<RemoveRoleFromUserResponse> RemoveRoleFromUser(RemoveRoleFromUserRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new RemoveRoleFromUser
            {
                UserId = Guid.Parse(request.UserId),
                RoleName = request.RoleName
            });

            return new RemoveRoleFromUserResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<AssignRolesToUserResponse> AssignRolesToUser(AssignRolesToUserRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new AssignRolesToUser
            {
                UserName = request.UserName
            });

            return new AssignRolesToUserResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<GetRoleSelectResponse> GetRoleSelect(Empty request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetRoleSelect { });

            var response = new GetRoleSelectResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };
            tempResponse.Data.ForEach(data => response.Data.Add(new GetRoleSelectData
            {
                Id = data.Id.ToString(),
                Name = data.Name
            }));
            return response;
        }

        public override async Task<GetPermissionPaginationResponse> GetPermissionPagination(GetPermissionPaginationRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetPermissionPagination
            {
                Paging = new(request.DataSourceRequest.PagingRequest.Page, request.DataSourceRequest.PagingRequest.Limit),
                Sorts = request.DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),
                Filters = request.DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList(),
                Language = request.Language
            });

            var response = new GetPermissionPaginationResponse
            {
                TotalCount = tempResponse.TotalCount,
                Page = tempResponse.Page,
                Limit = tempResponse.Limit,
                HasNextPage = tempResponse.HasNextPage,
                Next = tempResponse.Next,
                Prev = tempResponse.Prev
            };
            tempResponse.Data.ForEach(data => response.Data.Add(new PermissionData
            {
                Id = data.Id.ToString(),
                Description = data.Description,
                Name = data.Name,
                Label = data.Label,
                DescriptionLabel = data.DescriptionLabel
            }));

            return response;
        }

        public override async Task<GetPermissionResponse> GetPermission(GetPermissionRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetPermission
            {
                Id = Guid.Parse(request.Id),
                Language = request.Language
            });

            var response = new GetPermissionResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };
            response.Data = new PermissionData
            {
                Id = tempResponse.Data.Id.ToString(),
                Description = tempResponse.Data.Description,
                Name = tempResponse.Data.Name,
                Label = tempResponse.Data.Label,
                DescriptionLabel = tempResponse.Data.DescriptionLabel
            };

            return response;
        }

        public override async Task<DeletePermissionResponse> DeletePermission(DeletePermissionRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new DeletePermission
            {
                Id = Guid.Parse(request.Id)
            });

            return new DeletePermissionResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<CreatePermissionResponse> CreatePermission(CreatePermissionRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new CreatePermission
            {
                Name = request.Name,
                Description = request.Description
            });

            return new CreatePermissionResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<UpdatePermissionResponse> UpdatePermission(UpdatePermissionRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new UpdatePermission
            {
                Id = Guid.Parse(request.Id),
                Name = request.Name,
                Description = request.Description
            });

            return new UpdatePermissionResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<RolePermissionResponse> RolePermission(RolePermissionRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new RolePermission
            {
                PermissionId = Guid.Parse(request.PermissionId),
                RoleId = request.RoleId,
                StartDate = request.StartDate.ToDateTime(),
                EndDate = request.EndDate.ToDateTime()
            });

            return new RolePermissionResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<RolePermissionsResponse> RolePermissions(RolePermissionsRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new RolePermissions
            {
                PermissionIds = request.PermissionIds.ToList().Select(permission => Guid.Parse(permission)).ToList(),
                RoleId = request.RoleId,
                StartDate = request.StartDate.ToDateTime(),
                EndDate = request.EndDate.ToDateTime()
            });

            return new RolePermissionsResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<RemovePermissionRoleResponse> RemovePermissionRole(RemovePermissionRoleRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new RemovePermissionRole
            {
                Id = Guid.Parse(request.Id),
                PermissionId = Guid.Parse(request.PermissionId),
                RoleId = Guid.Parse(request.RoleId)
            });

            return new RemovePermissionRoleResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<UpdatePermissionRoleResponse> UpdatePermissionRole(UpdatePermissionRoleRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new UpdatePermissionRole
            {
                Id = Guid.Parse(request.Id),
                PermissionId = Guid.Parse(request.PermissionId),
                RoleId = Guid.Parse(request.RoleId),
                StartDate = request.StartDate.ToDateTime(),
                EndDate = request.EndDate.ToDateTime()
            });

            return new UpdatePermissionRoleResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<GetPermissionSelectResponse> GetPermissionSelect(Empty request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetPermissionSelect { });

            var response = new GetPermissionSelectResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };
            tempResponse.Data.ForEach(data => response.Data.Add(new GetPermissionSelectData
            {
                Id = data.Id.ToString(),
                Name = data.Name
            }));

            return response;
        }

        public override async Task<GetGroupResponse> GetGroup(GetGroupRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetGroup
            {
                Id = Guid.Parse(request.Id)
            });

            var response = new GetGroupResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };
            tempResponse.Data.ForEach(data => response.Data.Add(new GetGroupData
            {
                Id = data.Id.ToString(),
                Name = data.Name,
                Description = data.Description
            }));

            return response;
        }

        public override async Task<DeleteGroupResponse> DeleteGroup(DeleteGroupRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new DeleteGroup
            {
                Id = Guid.Parse(request.Id)
            });

            return new DeleteGroupResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<CreateGroupResponse> CreateGroup(CreateGroupRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new CreateGroup
            {
                Name = request.Name,
                Description = request.Description
            });

            return new CreateGroupResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<UpdateGroupResponse> UpdateGroup(UpdateGroupRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new UpdateGroup
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description
            });

            return new UpdateGroupResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<GroupAddUserResponse> GroupAddUser(GroupAddUserRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new GroupAddUser
            {
                UserId = Guid.Parse(request.UserId),
                GroupId = Guid.Parse(request.GroupId),
                StartDate = request.StartDate.ToDateTime(),
                EndDate = request.EndDate.ToDateTime()
            });

            return new GroupAddUserResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<GetGroupSelectResponse> GetGroupSelect(Empty request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetGroupSelect { });

            var response = new GetGroupSelectResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };
            tempResponse.Data.ForEach(data => response.Data.Add(new GetGroupSelectData
            {
                Id = data.Id.ToString(),
                Name = data.Name
            }));

            return response;
        }

        public override async Task<DeleteTokenResponse> DeleteToken(DeleteTokenRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new DeleteToken
            {
                Id = Guid.Parse(request.Id)
            });

            return new DeleteTokenResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        public override async Task<GetRoleUsersResponse> GetRoleUsers(GetRoleUsersRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetRoleUsers
            {
                RoleId = Guid.Parse(request.RoleId)
            });

            var response = new GetRoleUsersResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };
            tempResponse.Data.ForEach(data => response.Data.Add(data));

            return response;
        }

        public override async Task<GetUserRolesResponse> GetUserRoles(GetUserRolesRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetUserRoles
            {
                UserName = request.UserName
            });

            var response = new GetUserRolesResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };
            tempResponse.Data.ForEach(data => response.Data.Add(data));

            return response;
        }

        public override async Task<GetUserPermissionsResponse> GetUserPermissions(GetUserPermissionsRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetUserPermissions
            {
                UserId = Guid.Parse(request.UserId),
                Language = request.Language
            });

            var response = new GetUserPermissionsResponse
            {
                Status = tempResponse.Status,
                Message = tempResponse.Message
            };
            tempResponse.Data.ForEach(data => response.Data.Add(new UserPermissionDetail
            {
                Id = data.Id.ToString(),
                Name = data.Name,
                Label = data.Label,
                Description = data.Description,
                DescriptionLabel = data.DescriptionLabel
            }));

            return response;
        }

        [AllowAnonymous]
        public override async Task<ControlTokenResponse> ControlToken(ControlTokenRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new ControlToken
            {
                Token = request.Token
            });

            return new ControlTokenResponse
            {
                Response = response
            };
        }

        [AllowAnonymous]
        public override async Task<ForgotPasswordResponse> ForgotPassword(ForgotPasswordRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new ForgotPassword
            {
                UserName = request.UserName,
                Domain = request.Domain,
                Language = request.Language
            });

            return new ForgotPasswordResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        [AllowAnonymous]
        public override async Task<ForgotPasswordTokenResponse> ForgotPasswordToken(ForgotPasswordTokenRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new ForgotPasswordToken
            {
                UserName = request.UserName,
                Token = request.Token,
                Password = request.Password,
                ConfirmPassword = request.ConfirmPassword
            });

            return new ForgotPasswordTokenResponse
            {
                Status = response.Status,
                Message = response.Message
            };
        }

        [AllowAnonymous]
        public async override Task<GetTokenResponse> ControlOtpToken(ControlOtpTokenRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new ControlOtpToken
            {
                UserName = request.UserName,
                Password = request.Password,
                TenantId = Guid.Parse(request.TenantId),
                Language = request.Language,
                Token = request.Token,
                Otp = request.Otp
            });

            GetTokenResponse grpcResponse = new()
            {
                LoginStatus = response.LoginStatus switch
                {
                    Application.LoginStatus.Unknown => LoginStatus.Unknown,
                    Application.LoginStatus.FirstLogin => LoginStatus.FirstLogin,
                    Application.LoginStatus.AlreadyLogged => LoginStatus.AlreadyLogged,
                    Application.LoginStatus.AuthorityNull => LoginStatus.AuthorityNull,
                    Application.LoginStatus.VerifyToken => LoginStatus.VerifyToken,
                    _ => throw new System.NotImplementedException(),
                }
            };


            grpcResponse.AuthorityNull = response.AuthorityNull != null
                ?
                new AuthorityNullModel
                {
                    AuthToken = response.AuthorityNull.AuthToken
                }
                : null;


            grpcResponse.FirstLogin = response.FirstLogin != null
                ?
                new FirstLoginModel
                {
                    Message = response.FirstLogin.Message
                }
                : null;


            grpcResponse.AlreadyLogged = response.AlreadyLogged != null
                ?
                new AlreadyLoggedModel
                {
                    AccessToken = response.AlreadyLogged.AccessToken,
                    Email = response.AlreadyLogged.Email,
                    ExpireTime = Timestamp.FromDateTimeOffset(response.AlreadyLogged.ExpireTime),
                    FamilyName = response.AlreadyLogged.FamilyName,
                    PhoneNumber = response.AlreadyLogged.PhoneNumber,
                    GivenName = response.AlreadyLogged.GivenName,
                    UniqueName = response.AlreadyLogged.UniqueName,
                    RefreshToken = response.AlreadyLogged.RefreshToken,
                    Gender = response.AlreadyLogged.Gender switch
                    {
                        Application.Gender.Male => Gender.Male,
                        Application.Gender.Femail => Gender.Femail,
                        _ => throw new System.NotImplementedException(),
                    }
                }
                : null;


            grpcResponse.VerifyToken = response.VerifyToken != null
                ?
                new VerifyTokenModel()
                {
                    Authority = response.VerifyToken.Authority,
                    VerifyToken = response.VerifyToken.VerifyToken,
                    TimeOut = response.VerifyToken.TimeOut
                }
                : null;

            if (grpcResponse.VerifyToken?.Parameters != null)
                FillRepeatedData(response.VerifyToken.Parameters, grpcResponse.VerifyToken.Parameters);

            static void FillRepeatedData(string[] data, Google.Protobuf.Collections.RepeatedField<string> repeatedField) => data.ToList().ForEach(u => repeatedField.Add(u));

            return grpcResponse;
        }

        public override async Task<GetTenantIdResponse> GetTenantId(GetTenantIdRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetTenantId
            {
                Id = Guid.Parse(request.Id)
            });

            var response = new GetTenantIdResponse
            {
                Id = tempResponse.Id.ToString(),
                Name = tempResponse.Name,
                Description = tempResponse.Description,
                ErpCode = tempResponse.ErpCode,
                ErpGroupName = tempResponse.ErpGroupName,
                ErpStartDate = Timestamp.FromDateTimeOffset(tempResponse.ErpStartDate),
                ErpEndDate = Timestamp.FromDateTimeOffset(tempResponse.ErpEndDate),
                TenantGroup = new TenantGroupData
                {
                    Id = tempResponse.TenantGroup.Id.ToString(),
                    Name = tempResponse.TenantGroup.Name,
                    Description = tempResponse.TenantGroup.Description
                }
            };

            return response;
        }

        public override async Task<TenantPaginationResponse> GetTenantPagination(TenantPaginationRequest request, ServerCallContext context)
        {
            var tempResponse = await _queryDispatcher.QueryAsync(new GetTenantPagination
            {
                Paging = new(request.DataSourceRequest.PagingRequest.Page, request.DataSourceRequest.PagingRequest.Limit),
                Sorts = request.DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),
                Filters = request.DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            var response = new TenantPaginationResponse
            {
                TotalCount = tempResponse.TotalCount,
                Page = tempResponse.Page,
                Limit = tempResponse.Limit,
                HasNextPage = tempResponse.HasNextPage,
                Next = tempResponse.Next,
                Prev = tempResponse.Prev
            };

            tempResponse.Data.ToList().ForEach(data => response.Data.Add(new GetTenantIdResponse
            {
                Id = data.Id.ToString(),
                Name = data.Name,
                Description = data.Description,
                ErpCode = data.ErpCode,
                ErpGroupName = data.ErpGroupName,
                ErpStartDate = Timestamp.FromDateTimeOffset(data.ErpStartDate),
                ErpEndDate = Timestamp.FromDateTimeOffset(data.ErpEndDate),
                TenantGroup = new TenantGroupData
                {
                    Id = data.TenantGroup.Id.ToString(),
                    Name = data.TenantGroup.Name,
                    Description = data.TenantGroup.Description
                }
            }));

            return response;
        }


        [AllowAnonymous]
        public override async Task<GetContractsByIdResponse> GetContractsById(GetContractsByIdRequest request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new GetContractById
            {
                Id = Guid.Parse(request.Id)
            });

            if (result.Data is null)
            {
                return new GetContractsByIdResponse
                {
                    Status = result.Status,
                    Message = result.Message
                };
            }

            return new GetContractsByIdResponse
            {
                Id = result.Data.Id.ToString(),
                AppType = result.Data.AppType,
                ContractBody = result.Data.ContractBody,
                ContractType = result.Data.ContractType,
                Language = result.Data.Language,
                Name = result.Data.Name,
                ReleaseDate = Timestamp.FromDateTimeOffset(result.Data.ReleaseDate),
                Version = result.Data.Version,
                Message = result.Message,
                Status = result.Status
            };
        }

        public override async Task<CreateContractResponse> CreateContract(CreateContractRequest request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new CreateContract
            {
                AppType = request.AppType,
                Version = request.Version,
                ReleaseDate = request.ReleaseDate.ToDateTime(),
                ContractBody = request.ContractBody,
                ContractType = request.ContractType,
                Language = request.Language,
                Name = request.Name,
            });

            return new CreateContractResponse
            {
                Message = result.Message,
                Status = result.Status
            };
        }

        public override async Task<UpdateContractResponse> UpdateContract(UpdateContractRequest request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new UpdateContract
            {
                Id = Guid.Parse(request.Id),
                AppType = request.AppType,
                Version = request.Version,
                ReleaseDate = request.ReleaseDate.ToDateTime(),
                ContractBody = request.ContractBody,
                ContractType = request.ContractType,
                Language = request.Language,
                Name = request.Name,
            });

            return new UpdateContractResponse
            {
                Message = result.Message,
                Status = result.Status
            };
        }

        [AllowAnonymous]
        public override async Task<GetContractsByParametersResponse> GetContractsByParameters(GetContractsByParametersRequest request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new GetContractsByParameters
            {
                AppType = request.AppType,
                ContractType = request.ContractType,
                Language = request.Language,
            });

            var model = new GetContractsByParametersResponse
            {
                Message = result.Message,
                Status = result.Status,
            };

            foreach (var contract in result.Data)
            {
                model.Data.Add(new GetContractsByIdResponse
                {
                    Id = contract.Id.ToString(),
                    AppType = contract.AppType,
                    ContractType = contract.ContractType,
                    Name = contract.Name,
                    ReleaseDate = Timestamp.FromDateTimeOffset(contract.ReleaseDate),
                    Version = contract.Version
                });
            }

            return model;
        }

        public override async Task<AdminPermissionPaginationResponse> AdminPermissionPagination(AdminPermissionPaginationRequest request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new AdminPermissionPagination
            {
                Paging = new(request.DataSourceRequest.PagingRequest.Page, request.DataSourceRequest.PagingRequest.Limit),
                Sorts = request.DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),
                Filters = request.DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList(),
                Language = request.Language
            });

            var model = new AdminPermissionPaginationResponse
            {
                TotalCount = result.TotalCount,
                Page = result.Page,
                Limit = result.Limit,
                HasNextPage = result.HasNextPage,
                Next = result.Next,
                Prev = result.Prev
            };

            foreach (var item in result.Data)
            {
                model.Data.Add(new AdminPermissionPaginationResponseItem
                {
                    Description = item.Description,
                    DescriptionLabel = item.DescriptionLabel,
                    Id = item.Id.ToString(),
                    Label = item.Label,
                    Name = item.Name
                });
            }

            return model;
        }

        public override async Task<AdminPermissionSelectResponse> GetAdminPermissionSelect(Empty request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new GetAdminPermissionSelect());

            var model = new AdminPermissionSelectResponse
            {
                Message = result.Message,
                Status = result.Status,
            };

            foreach (var item in result.Data)
            {
                model.Data.Add(new AdminPermissionDetail
                {
                    Id = item.Id.ToString(),
                    Name = item.Name
                });
            }

            return model;
        }

        public override async Task<RoleAdminSelectResponse> GetRoleAdminPermissionSelect(Empty request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new GetRoleAdminSelect());

            var model = new RoleAdminSelectResponse
            {
                Message = result.Message,
                Status = result.Status,
            };

            foreach (var item in result.Data)
            {
                model.Data.Add(new RoleAdminSelectDetail
                {
                    Id = item.Id.ToString(),
                    Name = item.Name
                });
            }

            return model;
        }

        public override async Task<CreateAdminRoleResponse> CreateAdminRole(CreateAdminRoleRequest request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new CreateAdminRole()
            {
                Name = request.Name,
                Description = request.Description,
                Permissions = request.Permissions.Select(x => x).ToList()
            });

            return new CreateAdminRoleResponse
            {
                Message = result.Message,
                Status = result.Status,
            };
        }

        public override async Task<AdminGridResponse> GetAdminGrid(AdminGridRequest request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new GetAdminGridRequest
            {
                PagingQuery = new(request.DataSourceRequest.PagingRequest.Page, request.DataSourceRequest.PagingRequest.Limit),
                Sorts = request.DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),
                Filters = request.DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList(),
            });

            var model = new AdminGridResponse
            {
                TotalCount = result.TotalCount,
                Page = result.Page,
                Limit = result.Limit,
                HasNextPage = result.HasNextPage,
                Next = result.Next,
                Prev = result.Prev
            };

            foreach (var data in result.Data)
            {
                var item = new AdminGridData
                {
                    Id = data.Id.ToString(),
                    FirstName = data.FirstName,
                    LastName = data.LastName,
                    Email = data.Email,
                    PhoneNumber = data.PhoneNumber,
                    UserName = data.UserName,
                    Status = data.Status,
                    TwoFactorEnabled = data.TwoFactorEnabled,
                    Gender = data.Gender,
                    Birthdate = Timestamp.FromDateTimeOffset(data.Birthdate),
                    Address = data.Address,
                    AllowSms = data.AllowSms,
                    AllowEmail = data.AllowEmail,
                    ProfilePic = data.ProfilePic,
                    Domain = data.Domain,
                    Language = data.Language,
                    UserType = data.UserType
                };

                data.Areas?.ForEach(a => item.Areas.Add(new AreaDetail
                {
                    Id = a.Id.ToString(),
                    Level = a.Level
                }));

                data.Roles?.ForEach(r => item.Roles.Add(r));

                model.Data.Add(item);
            }

            return model;
        }

        public override async Task<RoleAdminPaginationResponse> RoleAdminPagination(RoleAdminPaginationRequest request, ServerCallContext context)
        {
            var result = await _queryDispatcher.QueryAsync(new RoleAdminPagination
            {
                Paging = new(request.DataSourceRequest.PagingRequest.Page, request.DataSourceRequest.PagingRequest.Limit),
                Sorts = request.DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Users.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),
                Filters = request.DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList(),
                Language = request.Language
            });

            var model = new RoleAdminPaginationResponse
            {
                TotalCount = result.TotalCount,
                Page = result.Page,
                Limit = result.Limit,
                HasNextPage = result.HasNextPage,
                Next = result.Next,
                Prev = result.Prev
            };

            result.Data?.ForEach(permission =>
            {
                var response = new RoleAdminPaginationResponseData
                {
                    Description = permission.Description,
                    Id = permission.Id.ToString(),
                    Name = permission.Name
                };

                permission.Permissions?.ForEach(item =>
                {
                    response.Data.Add(new RoleAdminPaginationResponseItem
                    {
                        Description = item.Description,
                        DescriptionLabel = item.DescriptionLabel,
                        Id = item.Id.ToString(),
                        Label = item.Label,
                        Name = item.Name
                    });
                });

                model.Data.Add(response);
            });

            return model;
        }

        public override async Task<GetAdminUserByIdResponse> GetAdminUserById(GetAdminUserByIdRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new GetAdminUserById
            {
                Id = request.Id.ToGuid()
            });

            var model = new GetAdminUserByIdResponse
            {
                Id = response.Id.ToString(),
                Address = response.Address,
                AllowEmail = response.AllowEmail,
                AllowSms = response.AllowSms,
                Birthdate = Timestamp.FromDateTimeOffset(response.Birthdate),
                Email = response.Email,
                FirstName = response.FirstName,
                LastName = response.LastName,
                Gender = response.Gender,
                PhoneNumber = response.PhoneNumber,
                ProfilePic = response.ProfilePic,
                Status = response.Status,
                TwoFactorEnabled = response.TwoFactorEnabled,
                UserName = response.UserName,
                UserType = response.UserType,
            };

            response.Tenants.ForEach(tenant => model.Tenants.Add(new GetAdminUserByIdTenant
            {
                Id = tenant.Id.ToString(),
                Name = tenant.Name
            }));

            response.Roles.ForEach(role => model.Roles.Add(new GetAdminUserByIdRoleDetail
            {
                Id = role.Id.ToString(),
                Name = role.Name
            }));

            return model;
        }

        public override async Task<CreateAdminUserWithDefaultPasswordResponse> CreateAdminUserWithDefaultPassword(CreateAdminUserWithDefaultPasswordRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new CreateAdminUserWithDefaultPassword
            {
                Address = request.Address,
                AllowEmail = request.AllowEmail.HasValue && request.AllowEmail.Value,
                AllowSms = request.AllowSms.HasValue && request.AllowEmail.Value,
                Birthdate = request.Birthdate.ToDateTime(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Gender = request.Gender,
                PhoneNumber = request.PhoneNumber,
                ProfilePic = request.ProfilePic,
                Status = request.Status.HasValue && request.Status.Value,
                TwoFactorEnabled = request.TwoFactorEnabled.HasValue && request.TwoFactorEnabled.Value,
                UserName = request.UserName,
                UserType = request.UserType,
                Language = request.Language,
                Domain = request.Domain,
                Roles = request.Roles.Select(x => x).ToList(),
                TenantIds = request.Tenants.Select(x => x).ToList()
            });

            return new CreateAdminUserWithDefaultPasswordResponse
            {
                Message = response.Message,
                Status = response.Status
            };
        }

        public override async Task<UpdateAdminUserResponse> UpdateAdminUser(UpdateAdminUserRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new UpdateAdminUser
            {
                Id = request.Id.ToGuid(),
                Address = request.Address,
                AllowEmail = request.AllowEmail.HasValue && request.AllowEmail.Value,
                AllowSms = request.AllowSms.HasValue && request.AllowEmail.Value,
                Birthdate = request.Birthdate.ToDateTime(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Gender = request.Gender,
                PhoneNumber = request.PhoneNumber,
                ProfilePic = request.ProfilePic,
                Status = request.Status.HasValue && request.Status.Value,
                TwoFactorEnabled = request.TwoFactorEnabled.HasValue && request.TwoFactorEnabled.Value,
                UserName = request.UserName,
                UserType = request.UserType,
                Language = request.Language,
                Domain = request.Domain,
                Roles = request.Roles.Select(x => x).ToList(),
                TenantIds = request.Tenants.Select(x => x).ToList()
            });

            return new UpdateAdminUserResponse
            {
                Message = response.Message,
                Status = response.Status
            };
        }

        public override async Task<CheckContractsResponse> CheckContracts(CheckContractsRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new CheckContracts { 
                Language = request.Language
            });
            var responseObj = new CheckContractsResponse();
            response.ForEach(contracts => responseObj.Data.Add(new CheckContractsResponseData
            {
                ContractId = contracts.ContractId.ToString(),
                ContractType = contracts.ContractType,
                Version = contracts.Version
            }));

            return responseObj;
        }

        public override async Task<ConfirmContractResponse> ConfirmContract(ConfirmContractRequest request, ServerCallContext context)
        {
            var response = await _queryDispatcher.QueryAsync(new ConfirmContract { 
                ContractId = Guid.Parse(request.ContractId)
            });

            return new ConfirmContractResponse
            {
                Response = response.Response
            };
        }
    }
}
