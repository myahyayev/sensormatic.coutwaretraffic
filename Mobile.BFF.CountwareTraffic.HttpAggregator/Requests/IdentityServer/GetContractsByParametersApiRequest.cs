﻿using Microsoft.AspNetCore.Mvc;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer
{
    public class GetContractsByParametersApiRequest
    {
        [FromForm(Name = "appType")]
        public int AppType { get; set; }

        [FromForm(Name = "contractType")]
        public string ContractType { get; set; }

        [FromForm(Name = "language")]
        public string Language { get; set; }
    }
}
