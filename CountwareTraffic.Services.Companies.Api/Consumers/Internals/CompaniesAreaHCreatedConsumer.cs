﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesAreaHCreatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaHCreated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesAreaHCreatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaHCreated queuEvent)
        {
            var areaCreated = _eventMapper.Map(queuEvent) as AreaHCreated;
            await _eventDispatcher.PublishAsync(areaCreated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
