﻿using Convey.CQRS.Queries;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTokenHandler : IQueryHandler<GetToken, IdentityTokenResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetTokenHandler(IIdentityServerService identityServerService)
            =>_identityServerService = identityServerService;


        public async Task<IdentityTokenResponse> HandleAsync(GetToken query)
        {
            return await _identityServerService.GetTokenAsync(new IdentityTokenRequest
            {
                Payload = new Payload
                {
                    Password = query.Password,
                    UserName = query.UserName,
                    TenantId = query.TenantId,
                    Language = query.Language
                }
            });
        }

        private bool IsValidMobilePhone(string text)
        {
            if (text.Length != 10)
                return false;

            var regexTrMobilePhone = new Regex(@"^[5][0,3,4,5,6][0-9][0-9]{7}$", RegexOptions.IgnoreCase);
            var matchTrMobilePhone = regexTrMobilePhone.Match(text);

            return matchTrMobilePhone.Success;
        }
    }
}
