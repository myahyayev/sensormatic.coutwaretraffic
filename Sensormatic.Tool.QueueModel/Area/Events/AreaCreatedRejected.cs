﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record AreaCreatedRejected : MessageEnvelope, IQueueEvent
    {
        public AreaCreatedRejected(Guid areaId, string name, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            AreaId = areaId;
            Name = name;
        }
        public Guid AreaId { get; }
        public string Name { get; }
        public Guid RecordId { get; init; }
    }
}
