﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using Sensormatic.Tool.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetCompaniesHandler : IQueryHandler<GetCompanies, PagingResult<CompanyDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetCompaniesHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<PagingResult<CompanyDetailsDto>> HandleAsync(GetCompanies query)
        {
            int page = query.PagingQuery.Page;
            int limit = query.PagingQuery.Limit;

            var qres = await _unitOfWork.GetRepository<ICompanyRepository>()
                                        .GetAllAsync(query.PagingQuery, query.Filters, query.Sorts);

            if (qres == null)
                return PagingResult<CompanyDetailsDto>.Empty;

            return new PagingResult<CompanyDetailsDto>(qres.Entities.Select(company => new CompanyDetailsDto
            {
                Id = company.Id,
                Name = company.Name,
                Description = company.Description,
                AuditCreateBy = company.AuditCreateBy,
                AuditCreateDate = company.AuditCreateDate,
                AuditModifiedBy = company.AuditModifiedBy,
                AuditModifiedDate = company.AuditModifiedDate,
                City = company.Address?.City,
                Country = company.Address?.Country,
                Latitude = company.Address?.Location?.Y,
                Longitude = company.Address?.Location?.X,
                State = company.Address?.State,
                Street = company.Address?.Street,
                ZipCode = company.Address?.ZipCode,
                EmailAddress = company.Contact?.EmailAddress,
                EmailAddressSecondary = company.Contact?.EmailAddressSecondary,
                GsmDialCode = company.Contact?.GsmDialCode,
                GsmDialCodeSecondary = company.Contact?.GsmDialCodeSecondary,
                GsmNumberSecondary = company.Contact?.GsmNumberSecondary,
                PersonName = company.Contact?.PersonName,
                PersonNameSecondary = company.Contact?.PersonNameSecondary,
                PhoneDialCode = company.Contact?.PhoneDialCode,
                PhoneDialCodeSecondary = company.Contact?.PhoneDialCodeSecondary,
                PhoneNumberSecondary = company.Contact?.PhoneNumberSecondary,
                GsmNumber = company.Contact?.GsmNumber,
                PhoneNumber = company.Contact?.PhoneNumber,
                GsmCountryCode = company.Contact?.GsmCountryCode,
                GsmCountryCodeSecondary = company.Contact?.GsmCountryCodeSecondary,
                PhoneCountryCodeSecondary = company.Contact?.PhoneCountryCodeSecondary,
                PhoneCountryCode = company.Contact?.PhoneCountryCode,

            }), qres.Total, page, limit, qres.Total > (limit * (page - 1)) + qres.Entities.Count);
        }
    }
}