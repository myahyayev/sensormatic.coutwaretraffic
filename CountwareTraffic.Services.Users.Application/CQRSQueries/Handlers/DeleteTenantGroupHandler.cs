﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteTenantGroupHandler : IQueryHandler<DeleteTenantGroup, IdentityDeleteTenantGroupResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public DeleteTenantGroupHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityDeleteTenantGroupResponse> HandleAsync(DeleteTenantGroup query)
        {
            return await _identityServerService.DeleteTenantGroupAsync(query.Id);
        }
    }
}
