﻿using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using StackExchange.Redis;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class RedisExpiredKeysListenerService : BackgroundService
    {
        private readonly ILogger<RedisExpiredKeysListenerService> _logger;
        private readonly ICache _redisCacheService;
        private readonly IServiceProvider _serviceProvicer;
        public RedisExpiredKeysListenerService(ILogger<RedisExpiredKeysListenerService> logger, ICache redisCacheService, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _redisCacheService = redisCacheService;
            _serviceProvicer = serviceProvider;

        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            string EXPIRED_KEYS_CHANNEL = "__keyevent@0__:expired";

            var connection = _redisCacheService.GetConnection<ConnectionMultiplexer>();

            ISubscriber subscriber = connection.GetSubscriber();

            subscriber.Subscribe(EXPIRED_KEYS_CHANNEL, async (channel, key) =>
            {


                //Only DeviceConnectedStatus:{0}:{1}  =>> {0} deviceId, {1} tenantId
                var splittedArray = key.ToString().Split(":");

                if (splittedArray[0] == nameof(CacheKeys.DeviceConnectedStatus))
                {
                    // Try to acquire a lock on the key
                    var lockKey = $"lock:{key}";
                    if (await _redisCacheService.LockTakeAsync(lockKey, "1", TimeSpan.FromSeconds(5)))
                    {
                        var deviceConnectionStatusProcessorService = _serviceProvicer.GetService<IDeviceConnectionStatusProcessorService>();

                        Guid deviceId = new Guid(splittedArray[1]);
                        Guid tenantId = new Guid(splittedArray[2]);

                        Application.DeviceDto device = new() { TenantId = tenantId, Id = deviceId, ControlFrequency = -1 };

                        await deviceConnectionStatusProcessorService.ProcessDeviceConnectionStatus(device);


                    }
                }
            });
        }

        public async override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Redis Expired Keys Listener Service forcing to stop...");

            await Task.Delay(5000);

            await base.StopAsync(cancellationToken);
        }
    }
}
