﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Devices.Grpc
{
    [ServiceLog]
    public sealed partial class CreateDeviceBrandModelRequest : SensormaticRequestValidate
    {
        internal Guid _BrandId
        {
            get
            {
                if (Guid.TryParse(brandId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { brandId_ = value.ToString(); }
        }


        public override void Validate()
        {
            if (_BrandId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1493 BrandId cannot be null", nameof(_BrandId)));

            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1494 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1495 Name invalid format", nameof(Name)));
        }
    }
}


