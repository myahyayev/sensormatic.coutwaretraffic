﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Events.Infrastructure.Migrations
{
    public partial class ChangCompanyLookupCountry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CompanyLookupId",
                schema: "events",
                table: "Devices",
                newName: "CountryLookupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CountryLookupId",
                schema: "events",
                table: "Devices",
                newName: "CompanyLookupId");
        }
    }
}
