﻿using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class ElasticDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class TenantElasticDto : ElasticDto { }
    public class CompanyElasticDto : ElasticDto { }
    public class CountryElasticDto : ElasticDto { }
    public class RegionElasticDto : ElasticDto { }
    public class AreaElasticDto : ElasticDto { }
    public class SubAreaElasticDto : ElasticDto { }
    public class DeviceElasticDto : ElasticDto { }
}
