﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreateUserHandler : IQueryHandler<CreateUser, IdentityCreateUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public CreateUserHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityCreateUserResponse> HandleAsync(CreateUser query)
        {
            return await _identityServerService.CreateUserAsync(new IdentityCreateUserRequest
            {
                Id = query.Id,
                FirstName = query.FirstName,
                LastName = query.LastName,
                Email = query.Email,
                PhoneNumber = query.PhoneNumber,
                UserName = query.UserName,
                Status = query.Status,
                TwoFactorEnabled = query.TwoFactorEnabled,
                Gender = query.Gender,
                Birthdate = query.Birthdate,
                UserType =query.UserType,
                Address = query.Address,
                AllowSms = query.AllowSms,
                AllowEmail = query.AllowEmail,
                ProfilePic = query.ProfilePic,
                Areas = query.Areas,
                Roles = query.Roles,
                Password = query.Password,
                ConfirmPassword = query.ConfirmPassword
            });
        }
    }
}
