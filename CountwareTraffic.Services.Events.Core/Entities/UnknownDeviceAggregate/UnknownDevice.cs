﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;
using System;
using System.Net;

namespace CountwareTraffic.Services.Events.Core
{
    public class UnknownDevice : AggregateRoot, IDeletable
    {
        private string _name;
        private int _httpPort;
        private int _httpsPort;
        private string _ipAddress;
        private string _macAddress;
        private string _serialNumber;
        private DateTime _occurredOn;
        private DateTime? _processedDate;
        private bool _isProcessed;

        public string Name => _name;
        public int HttpPort => _httpPort;
        public int HttpsPort => _httpsPort;
        public string IpAddress => _ipAddress;
        public string MacAddress => _macAddress;
        public string SerialNumber => _serialNumber;
        public DateTime OccurredOn => _occurredOn;
        public DateTime? ProcessedDate => _processedDate;
        public bool IsProcessed => _isProcessed;

        public bool AuditIsDeleted { get; set; }

        private UnknownDevice() { }

        public static UnknownDevice Create(string name, int httpPort, int httpsPort, string ipAddress, string macAddress, string serialNumber)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            if (name.Length > 150)
                throw new ArgumentException(nameof(name));

            if (name.Length > 130)
                throw new ArgumentException(nameof(name));

            if (!IPAddress.TryParse(ipAddress, out IPAddress ipA))
                throw new IpAddressIsEmptyOrWrongFormatException(ipAddress);

            if (ipAddress.Length > 15)
                throw new ArgumentException(nameof(ipAddress));

            if (macAddress.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(macAddress));

            if (macAddress.Length > 17)
                throw new ArgumentException(nameof(macAddress));

            if (!serialNumber.IsNullOrWhiteSpace() && serialNumber.Length > 50)
                throw new ArgumentException(nameof(serialNumber));

            return new UnknownDevice
            {
                _name = name,
                _httpPort = httpPort,
                _httpsPort = httpsPort,
                _ipAddress = ipAddress,
                _macAddress = macAddress,
                _serialNumber = serialNumber,
                _isProcessed = false,
                _occurredOn = DateTime.Now
            };
        }

        public void WhenProcessed()
        {
            _isProcessed = true;
            _processedDate = DateTime.Now;
        }

        public void WhenCreated()
            => AddEvent(new UnknownDeviceCreateComplated(_name, _httpPort, _httpsPort, _ipAddress, _macAddress, _serialNumber));
    }
}
