﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class AreaChangedRejectedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaChangedRejected>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public AreaChangedRejectedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaChangedRejected queuEvent)
        {
            var areaChangedRejected = _eventMapper.Map(queuEvent) as AreaChangedRejected;
            await _eventDispatcher.PublishAsync(areaChangedRejected);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
