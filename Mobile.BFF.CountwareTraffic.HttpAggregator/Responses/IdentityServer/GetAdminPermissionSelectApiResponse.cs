﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer
{
    public class GetAdminPermissionSelectApiResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<GetAdminPermissionSelectApiResponseItem> Data { get; set; }
    }

    public class GetAdminPermissionSelectApiResponseItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
