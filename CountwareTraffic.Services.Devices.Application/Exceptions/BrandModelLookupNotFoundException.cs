﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class BrandModelLookupNotFoundException : AppException
    {
        public Guid Id { get; }
        public BrandModelLookupNotFoundException(Guid id)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1553 BrandModelLookup with id: {id} was not found") }, 404, ResponseMessageType.Error)
        {
            Id = id;
        }
    }
}
