﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetRegion : IQuery<RegionDetailsDto>
    {
        public Guid RegionId { get; set; }
    }
}
