﻿using CountwareTraffic.Services.Settings.Application;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Settings.Infrastructure
{
    public class QueueEventMapper : IQueueEventMapper
    {
        public List<Sensormatic.Tool.Queue.IQueueEvent> MapAll(IEnumerable<Sensormatic.Tool.Efcore.IDomainEvent> events, Guid userId, string correlationId, Guid tenantId)
             => events.Select(e => Map(e, userId, correlationId, tenantId)).ToList();

        public Sensormatic.Tool.Queue.IQueueEvent Map(Sensormatic.Tool.Efcore.IDomainEvent @event, Guid userId, string correlationId, Guid tenantId)
        {
            switch (@event)
            {
                
            }

            return null;
        }
        public void Dispose() => GC.SuppressFinalize(this);
    }
}
