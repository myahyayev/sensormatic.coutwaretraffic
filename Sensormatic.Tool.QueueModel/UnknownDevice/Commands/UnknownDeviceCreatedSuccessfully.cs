﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record UnknownDeviceCreatedSuccessfully : IQueueCommand
    {
        public UnknownDeviceCreatedSuccessfully(string name, string macAddress, string ipAddress, string serialNumber, int httpPort, int httpsPort)
        {
            Name = name;
            MacAddress = macAddress;
            IpAddress = ipAddress;
            SerialNumber = serialNumber;
            HttpPort = httpPort;
            HttpsPort = httpsPort;
        }
        public string MacAddress { get; }
        public string IpAddress { get; }
        public string Name { get; }
        public string SerialNumber { get; }
        public int HttpPort { get; }
        public int HttpsPort { get; }
        public Guid RecordId { get; init; }
    }
}
