﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    internal sealed class SchemaNames
    {
        internal const string Application = "area.app";
        internal const string Areas = "areas";
    }
}
