﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetGroup : IQuery<IdentityGroupResponse>
    {
        public Guid Id { get; set; }
    }
}
