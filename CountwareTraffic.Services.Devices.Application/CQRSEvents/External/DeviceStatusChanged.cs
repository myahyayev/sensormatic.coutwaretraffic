﻿using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeviceStatusChanged : Convey.CQRS.Events.IEvent
    {
        public Guid DeviceId { get; init; }
        public int DeviceStatus { get; set; }
        public Guid RecordId { get; init; }
        public string CorrelationId { get; set; }
        public Guid TenantId { get; set; }
    }
}
