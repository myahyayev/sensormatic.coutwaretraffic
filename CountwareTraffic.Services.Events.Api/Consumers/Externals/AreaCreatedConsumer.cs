﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    public class AreaCreatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaCreated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        private readonly ILogger<AreaCreatedConsumer> _logger;
        public AreaCreatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper, ILogger<AreaCreatedConsumer> logger)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
            _logger = logger;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaCreated queuEvent)
        {
            _logger.LogInformation($"Consumed created Area with name {queuEvent.AreaName} with correlationId {queuEvent.CorrelationId}");
            var areaCreated = _eventMapper.Map(queuEvent) as AreaCreated;
            await _eventDispatcher.PublishAsync(areaCreated);
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
