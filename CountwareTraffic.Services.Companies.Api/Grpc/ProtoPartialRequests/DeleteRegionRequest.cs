﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class DeleteRegionRequest : SensormaticRequestValidate
    {
        internal Guid _RegionId
        {
            get
            {
                if (Guid.TryParse(regionId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.regionId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_RegionId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1310 AreaId cannot be null", nameof(_RegionId)));
        }
    }
}
