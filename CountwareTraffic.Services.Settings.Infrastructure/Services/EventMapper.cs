﻿using CountwareTraffic.Services.Settings.Application;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Settings.Infrastructure
{
    public class EventMapper : IEventMapper
    {
        public IEnumerable<Convey.CQRS.Events.IEvent> MapAll(IEnumerable<Sensormatic.Tool.Queue.IQueueEvent> events)
              => events.Select(Map);

        public Convey.CQRS.Events.IEvent Map(Sensormatic.Tool.Queue.IQueueEvent @event)
        {
            switch (@event)
            {
               
            }


            return null;
        }
    }
}
