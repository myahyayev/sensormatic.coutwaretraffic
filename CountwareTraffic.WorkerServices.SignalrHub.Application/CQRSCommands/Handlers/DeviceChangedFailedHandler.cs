﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class DeviceChangedFailedHandler : ICommandHandler<DeviceChangedFailed>
    {
        private readonly IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> _hubContext;
        public DeviceChangedFailedHandler(IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> hubContext) 
            => _hubContext = hubContext;

        public async Task HandleAsync(DeviceChangedFailed command) 
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .DeviceChangedFailedReceived(command);
    }
}
