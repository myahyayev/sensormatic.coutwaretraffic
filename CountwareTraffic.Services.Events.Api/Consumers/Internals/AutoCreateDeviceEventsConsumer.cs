﻿using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    public class AutoCreateDeviceEventsConsumer : IConsumer<Sensormatic.Tool.QueueModel.DeviceCreatedCompleted>, ITransientSelfDependency
    {
        private readonly IQueueService _queueService;
        public AutoCreateDeviceEventsConsumer(IQueueService queueService) => _queueService = queueService;

        public Task ConsumeAsync(Sensormatic.Tool.QueueModel.DeviceCreatedCompleted queuEvent)
        {
            var template = new QueueConfigTemplate
            {
                PrefetchCount = 5,
                RetryCount = 1440,//one day by 60 RetryIntervalSeconds
                RetryIntervalSeconds = 60,
                ExcludeExceptions = new List<Type> { typeof(Exception), typeof(HttpRequestException),typeof (ArgumentNullException) ,typeof(DbUpdateException)
            },//for avoid retry for NC_IX_AuditIsDeleted_TenantId_EventStartDate_EventEndDate_DirectionTypeId_DeviceId index
                AutoScale = true,
                ScaleUpTo = 20
            };

            //device event queue
            _queueService.Subscribe<DeviceEventsListenerConsumer>(String.Format(Queues.CountwareTrafficEventsDeviceEventsListener, queuEvent.DeviceId), template);

            //device event migration queue
            _queueService.Subscribe<DeviceEventsRangeListenerConsumer>(String.Format(Queues.CountwareTrafficEventsDeviceMigrationEventsListener, queuEvent.DeviceId), template);

            return Task.CompletedTask;
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
