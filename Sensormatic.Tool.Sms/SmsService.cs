﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Ioc;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Sensormatic.Tool.Sms
{
    public interface ISmsService : IScopedDependency
    {
        Task<SmsResponse> SendAsync(SmsRequest request);
    }

    public class SmsService : ISmsService
    {
        private readonly SmsOptions _smsOptions;
        private readonly ILogger<SmsService> _logger;
        private readonly HttpClient _httpClient;
        public SmsService(IOptions<SmsOptions> smsOptions, ILogger<SmsService> logger, IHttpClientFactory clientFactory)
        {
            _smsOptions = smsOptions.Value;
            _logger = logger;
            _httpClient = clientFactory.CreateClient();
        }
        public async Task<SmsResponse> SendAsync(SmsRequest request)
        {
            string to = string.Join(',', request.PhoneNumbers);

            var uriBuilder = new UriBuilder(_smsOptions.Url);
            var nameValueCollection = HttpUtility.ParseQueryString(uriBuilder.Query);

            nameValueCollection["Platformid"] = _smsOptions.PlatformId.ToString();
            nameValueCollection["chncode"] = _smsOptions.ChnCode.ToString();
            nameValueCollection["user"] = _smsOptions.User;
            nameValueCollection["pw"] = _smsOptions.Pw;
            nameValueCollection["to"] = to;
            nameValueCollection["text"] = request.Message;

            uriBuilder.Query = nameValueCollection.ToString();

            var httpResponseMessage = await _httpClient.GetAsync(uriBuilder.ToString());

            await CheckStatusCode(httpResponseMessage);

            var httpContent = await httpResponseMessage.Content.ReadAsStringAsync();

            if (httpContent.Contains("NOK"))
            {
                _logger.LogError($"Sms send failed: To {{To}} Message: {{Message}}  Error: {{Error}}", to, request.Message, httpContent);
                return new SmsResponse { IsSuccess = false, Message = httpContent };
            }
            else
            {
                _logger.LogInformation($"Sms send successfully: To {{To}} Message: {{Message}}", to, request.Message);
                return new SmsResponse { IsSuccess = true, Message = "Success" };
            }
        }

        private async Task CheckStatusCode(HttpResponseMessage httpResponseMessage)
        {
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                object obj = httpResponseMessage.StatusCode switch
                {
                    HttpStatusCode.ServiceUnavailable => throw new SmsServiceCannotBeAccessedException(),
                    _ => throw await httpResponseMessage.GetException()
                };
            }
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
