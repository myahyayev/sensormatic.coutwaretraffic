﻿using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeviceHDeleted : Convey.CQRS.Events.IEvent
    {
        public Guid DeviceId { get; set; }
        public string DeviceName { get; set; }
        public Guid SubAreaId { get; set; }
        public Guid AreaId { get; set; }
        public Guid RegionId { get; set; }
        public Guid CountryId { get; set; }
        public Guid ComapnyId { get; init; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
    }
}
