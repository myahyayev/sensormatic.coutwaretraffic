﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace Sensormatic.Tool.Sms
{
    public class SmsServiceCannotBeAccessedException : AppException
    {
        public SmsServiceCannotBeAccessedException()
            : base(new List<ErrorResult>() { new ErrorResult($"Sms Service cannot be accessed or there is a system error.") }, 500, ResponseMessageType.Error) { }
    }
}
