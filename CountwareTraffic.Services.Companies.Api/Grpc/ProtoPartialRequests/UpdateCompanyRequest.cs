﻿using CountwareTraffic.Services.Companies.Api;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class UpdateCompanyRequest : SensormaticRequestValidate
    {
        internal Guid _CompanyId
        {
            get
            {
                if (Guid.TryParse(companyId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.companyId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_CompanyId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1345 CompanyId cannot be null", nameof(_CompanyId)));

            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1346 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1347 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1348 Description invalid format", nameof(Description)));

            if (!City.IsNullOrWhiteSpace() && City.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1349 City invalid format", nameof(City)));

            if (!Street.IsNullOrWhiteSpace() && Street.Length > 250)
                ValidateResults.Add(new ErrorResult($"#E1350 Street invalid format", nameof(Street)));

            if (!State.IsNullOrWhiteSpace() && State.Length > 250)
                ValidateResults.Add(new ErrorResult($"#E1351 State invalid format", nameof(State)));

            if (!Country.IsNullOrWhiteSpace() && Country.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1352 Country invalid format", nameof(Country)));

            if (!ZipCode.IsNullOrWhiteSpace() && ZipCode.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1353 ZipCode invalid format", nameof(ZipCode)));

            if (!EmailAddress.IsNullOrWhiteSpace() && (!EmailValidator.EmailIsValid(EmailAddress) || EmailAddress.Length > 120))
                ValidateResults.Add(new ErrorResult($"#E1354 Email address invalid format", nameof(EmailAddress)));

            if (!PhoneNumber.IsNullOrWhiteSpace() && PhoneNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1355 PhoneNumber invalid format", nameof(PhoneNumber)));

            if (!GsmNumber.IsNullOrWhiteSpace() && GsmNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1356 GsmNumber invalid format", nameof(GsmNumber)));

            if (!GsmDialCode.IsNullOrWhiteSpace() && GsmDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1357 GsmDialCode invalid format", nameof(GsmDialCode)));

            if (!PhoneDialCode.IsNullOrWhiteSpace() && PhoneDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1358 PhoneDialCode invalid format", nameof(PhoneDialCode)));

            if (!PersonName.IsNullOrWhiteSpace() && PersonName.Length > 120)
                ValidateResults.Add(new ErrorResult($"#E1359 PersonName invalid format", nameof(PersonName)));

            if (!GsmDialCodeSecondary.IsNullOrWhiteSpace() && GsmDialCodeSecondary.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1360 GsmDialCodeSecondary invalid format", nameof(GsmDialCodeSecondary)));

            if (!GsmNumberSecondary.IsNullOrWhiteSpace() && GsmNumberSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1361 GsmNumberSecondary invalid format", nameof(GsmNumberSecondary)));

            if (!PhoneDialCodeSecondary.IsNullOrWhiteSpace() && PhoneDialCodeSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1362 PhoneDialCodeSecondary invalid format", nameof(PhoneDialCodeSecondary)));

            if (!PhoneNumberSecondary.IsNullOrWhiteSpace() && PhoneNumberSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1363 PhoneNumberSecondary invalid format", nameof(PhoneNumberSecondary)));

            if (!EmailAddressSecondary.IsNullOrWhiteSpace() && (!EmailValidator.EmailIsValid(EmailAddressSecondary) || EmailAddressSecondary.Length > 120))
                ValidateResults.Add(new ErrorResult($"#E1364 Email address secondary invalid format", nameof(EmailAddressSecondary)));

            if (!PersonNameSecondary.IsNullOrWhiteSpace() && PersonNameSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1365 PersonNameSecondary invalid format", nameof(PersonNameSecondary)));

            if (!GsmCountryCode.IsNullOrWhiteSpace() && GsmCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1366 GsmCountryCode invalid format", nameof(GsmCountryCode)));

            if (!PhoneCountryCode.IsNullOrWhiteSpace() && PhoneCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1367 PhoneCountryCode invalid format", nameof(PhoneCountryCode)));

            if (!GsmCountryCodeSecondary.IsNullOrWhiteSpace() && GsmCountryCodeSecondary.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1368 GsmCountryCodeSecondary invalid format", nameof(GsmCountryCodeSecondary)));

            if (!PhoneCountryCodeSecondary.IsNullOrWhiteSpace() && PhoneCountryCodeSecondary.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1369 PhoneCountryCodeSecondary invalid format", nameof(PhoneCountryCodeSecondary)));
        }
    }
}