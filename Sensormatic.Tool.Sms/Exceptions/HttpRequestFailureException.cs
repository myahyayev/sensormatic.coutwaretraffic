﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace Sensormatic.Tool.Sms
{
    public class HttpRequestFailureException : AppException
    {
        public HttpRequestFailureException(IList<ErrorResult> errorResults, int code, ResponseMessageType responseMessageType)
            : base(errorResults, code, responseMessageType)
        {
        }
    }
}
