﻿using Sensormatic.Tool.Efcore;
using System;
using System.Text.Json;

namespace CountwareTraffic.Services.Events.Core
{
    public class Event : AggregateRoot, IDeletable
    {
        private string _description;
        private DateTime _eventDate;
        private DateTime _eventStartDate;
        private DateTime _eventEndDate;
        private Guid _deviceId;
        private int _directionTypeId;
        private int _onTimeIOCount;

        public string Description => _description;
        public int OnTimeIOCount => _onTimeIOCount;
        public DateTime EventDate => _eventDate;
        public DateTime EventStartDate => _eventStartDate;
        public DateTime EventEndDate => _eventEndDate;
        public Guid DeviceId => _deviceId;
        public DirectionType DirectionType { get; private set; }
        public Guid TenantId { get; set; }
        public bool AuditIsDeleted { get; set; }


        private Event() { }

        public static Event Create(string description, int onTimeIOCount, DateTime eventDate, DateTime eventStartDate, DateTime eventEndDate, Guid deviceId, int directionTypeId, Guid tenantId)
        {
            CheckRule(new EventMustHaveDeviceRule(deviceId));

            return new Event
            {
                _eventDate = eventDate,
                _eventStartDate = eventStartDate,
                _eventEndDate = eventEndDate,
                _onTimeIOCount = onTimeIOCount,
                _description = description,
                _deviceId = deviceId,
                _directionTypeId = directionTypeId,
                TenantId = tenantId
            };
        }

        public void CompleteCreation(string deviceName, Guid subAreaId, string subAreaName, Guid areaId, string areaName, Guid cityId, string cityName, Guid districtId, string districtName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName)
        {
            var drType = DirectionType.From(_directionTypeId);

            AddEvent(new EventCreateCompleted(_id, drType, _eventDate, _eventStartDate, _eventEndDate, _onTimeIOCount, _deviceId, deviceName, subAreaId, subAreaName, areaId, areaName, cityId, cityName, districtId, districtName, regionId, regionName, countryId, countryName, companyId, companyName, TenantId));
        }
    }
}
