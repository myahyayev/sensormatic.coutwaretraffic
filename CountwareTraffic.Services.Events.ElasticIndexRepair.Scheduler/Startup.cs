﻿using Convey;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Quartz;
using Sensormatic.Tool.ApplicationInsights;
using Sensormatic.Tool.Scheduler;
using System;


namespace CountwareTraffic.Services.Events.ElasticIndexRepair.Scheduler
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            Sensormatic.Tool.Ioc.IoCGenerator.DoTNet.Current.Start(services, Configuration);

            services.AddApplicationInsights(Configuration);

            services.AddDbContext<EventDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("EventDbConnection"), x => x.UseNetTopologySuite().EnableRetryOnFailure()));


            services.AddConvey()
                    .AddApplication()
                    .Build();

            var assembly = typeof(Sensormatic.Tool.Scheduler.Controllers.HomeController).Assembly;

            services.AddControllersWithViews()
               .AddApplicationPart(assembly)
               .AddRazorRuntimeCompilation();


            services.Configure<MvcRazorRuntimeCompilationOptions>(options => { options.FileProviders.Add(new EmbeddedFileProvider(assembly)); });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSignalR();

            services.RegisterQuartzServices(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection()
               .UseStaticFiles(new StaticFileOptions { FileProvider = new PhysicalFileProvider(AppDomain.CurrentDomain.BaseDirectory + "/wwwroot") })
               .UseRouting()
               .UseAuthorization()
               .UseEndpoints(endpoints => { endpoints.MapHub<JobHub>("/signalr"); })
               .UseEndpoints(endpoints =>
               {
                   endpoints.MapControllerRoute(
                       name: "default",
                       pattern: "{controller=Home}/{action=Index}/{id?}");
               });
        }
    }
    public static class QuartzRegistration
    {
        public static IServiceCollection RegisterQuartzServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<QuartzOptions>(configuration.GetSection("Quartz"));

            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();

                q.AddJobListener<JobListener>();
                q.AddSchedulerListener<SchedulerListener>();
                q.AddTriggerListener<TriggerListener>();

                q.ScheduleJob<ElasticSearchEventIndexRepairJob>(trigger => trigger
                    .WithIdentity("EventsIndexRepairTrigger", "Elastic")
                    .StartNow()
                    .WithDescription("ever day at 00:00")
                    .WithCronSchedule("0 0 0 * * ?"));
            });

            services.AddQuartzServer(options =>
            {
                options.WaitForJobsToComplete = true;
            });

            return services;
        }
    }
}
