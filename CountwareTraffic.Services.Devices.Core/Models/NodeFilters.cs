﻿using System;

namespace CountwareTraffic.Services.Devices.Core
{
    public class NodeFilter
    {
        public Guid Id { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }
}
