﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Reporting.Infrastructure.Migrations
{
    public partial class Report_FirstInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "reports");

            migrationBuilder.CreateTable(
                name: "ReportFormats",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportFormats", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReportLanguages",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    AuditCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditCreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportLanguages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReportStatuses",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReportTemplates",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    AssemblyQualifiedName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    AuditCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditCreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScheduleDeliveryTimes",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleDeliveryTimes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScheduleFrequencies",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleFrequencies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScheduleWeeklyRepeatOns",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleWeeklyRepeatOns", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TemplateTypes",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemplateTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Reports",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    NotifyWhenComplete = table.Column<bool>(type: "bit", nullable: false),
                    TemplateId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FormatId = table.Column<int>(type: "int", nullable: false),
                    LanguageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Parameters = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email_To = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    Email_Subject = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Email_Body = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    AuditCreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditModifiedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    AuditCreateBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AuditModifiedBy = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reports_ReportFormats_FormatId",
                        column: x => x.FormatId,
                        principalSchema: "reports",
                        principalTable: "ReportFormats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reports_ReportLanguages_LanguageId",
                        column: x => x.LanguageId,
                        principalSchema: "reports",
                        principalTable: "ReportLanguages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reports_ReportStatuses_Status",
                        column: x => x.Status,
                        principalSchema: "reports",
                        principalTable: "ReportStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reports_ReportTemplates_TemplateId",
                        column: x => x.TemplateId,
                        principalSchema: "reports",
                        principalTable: "ReportTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReportTemplateTypes",
                schema: "reports",
                columns: table => new
                {
                    TypeId = table.Column<int>(type: "int", nullable: false),
                    ReportTemplateId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportTemplateTypes", x => new { x.TypeId, x.ReportTemplateId });
                    table.ForeignKey(
                        name: "FK_ReportTemplateTypes_ReportTemplates_ReportTemplateId",
                        column: x => x.ReportTemplateId,
                        principalSchema: "reports",
                        principalTable: "ReportTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReportTemplateTypes_TemplateTypes_TypeId",
                        column: x => x.TypeId,
                        principalSchema: "reports",
                        principalTable: "TemplateTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReportSchedules",
                schema: "reports",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ReportId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Frequency = table.Column<int>(type: "int", nullable: false),
                    TemplateTypeId = table.Column<int>(type: "int", nullable: false),
                    DeliveryTime = table.Column<int>(type: "int", nullable: false),
                    StartsOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndsOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    AtSetTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    AtSetTimeZone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RepeatOn = table.Column<string>(type: "nvarchar(2500)", maxLength: 2500, nullable: true),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportSchedules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReportSchedules_Reports_ReportId",
                        column: x => x.ReportId,
                        principalSchema: "reports",
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReportSchedules_ScheduleDeliveryTimes_DeliveryTime",
                        column: x => x.DeliveryTime,
                        principalSchema: "reports",
                        principalTable: "ScheduleDeliveryTimes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReportSchedules_ScheduleFrequencies_Frequency",
                        column: x => x.Frequency,
                        principalSchema: "reports",
                        principalTable: "ScheduleFrequencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReportSchedules_TemplateTypes_TemplateTypeId",
                        column: x => x.TemplateTypeId,
                        principalSchema: "reports",
                        principalTable: "TemplateTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reports_FormatId",
                schema: "reports",
                table: "Reports",
                column: "FormatId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_LanguageId",
                schema: "reports",
                table: "Reports",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_Status",
                schema: "reports",
                table: "Reports",
                column: "Status");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TemplateId",
                schema: "reports",
                table: "Reports",
                column: "TemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportSchedules_DeliveryTime",
                schema: "reports",
                table: "ReportSchedules",
                column: "DeliveryTime");

            migrationBuilder.CreateIndex(
                name: "IX_ReportSchedules_Frequency",
                schema: "reports",
                table: "ReportSchedules",
                column: "Frequency");

            migrationBuilder.CreateIndex(
                name: "IX_ReportSchedules_ReportId",
                schema: "reports",
                table: "ReportSchedules",
                column: "ReportId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportSchedules_TemplateTypeId",
                schema: "reports",
                table: "ReportSchedules",
                column: "TemplateTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportTemplateTypes_ReportTemplateId",
                schema: "reports",
                table: "ReportTemplateTypes",
                column: "ReportTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportTemplateTypes_TypeId_ReportTemplateId",
                schema: "reports",
                table: "ReportTemplateTypes",
                columns: new[] { "TypeId", "ReportTemplateId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReportSchedules",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "ReportTemplateTypes",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "ScheduleWeeklyRepeatOns",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "Reports",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "ScheduleDeliveryTimes",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "ScheduleFrequencies",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "TemplateTypes",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "ReportFormats",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "ReportLanguages",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "ReportStatuses",
                schema: "reports");

            migrationBuilder.DropTable(
                name: "ReportTemplates",
                schema: "reports");
        }
    }
}
