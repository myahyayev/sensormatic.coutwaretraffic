﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class UpdateRegionHandler : ICommandHandler<UpdateRegion>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public UpdateRegionHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(UpdateRegion command)
        {
            var regionRepository = _unitOfWork.GetRepository<IRegionRepository>();

            var region = await regionRepository.GetAsync(command.RegionId);

            if (region is null)
                throw new RegionNotFoundException(command.RegionId);

            var updateControl = await regionRepository.GetAsync(command.Name, command.CountryId);
            if (updateControl != null && updateControl.Id != command.RegionId)
                throw new RegionAlreadyExistsException(command.Name);

            region.CompleteChange(command.Name, command.Description,command.ManagerId, command.ManagerName);

            var countryRepositiry = _unitOfWork.GetRepository<ICountryRepository>();

            var hierarchyData = await countryRepositiry.GetHierarchyAsync(region.CountryId, _currentTenant.Id);

            region.WhenChanged(_currentTenant.Id, hierarchyData.CopmanyId, hierarchyData.CountryId,  region.Id, region.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
