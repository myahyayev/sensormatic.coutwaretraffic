﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Devices.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Api
{
    public class DevicesDeviceHDeletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.DeviceHDeleted>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public DevicesDeviceHDeletedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.DeviceHDeleted queuEvent)
        {
            var deviceHDeleted = _eventMapper.Map(queuEvent) as DeviceHDeleted;
            await _eventDispatcher.PublishAsync(deviceHDeleted);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
