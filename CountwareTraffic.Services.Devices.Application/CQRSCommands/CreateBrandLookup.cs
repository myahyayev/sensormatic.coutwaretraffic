﻿using Convey.CQRS.Commands;

namespace CountwareTraffic.Services.Devices.Application
{
    [Contract]
    public class CreateBrandLookup : ICommand
    {
        public string Name { get; set; }
    }
}
