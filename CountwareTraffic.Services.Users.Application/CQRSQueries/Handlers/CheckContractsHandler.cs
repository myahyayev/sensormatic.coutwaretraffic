﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.CQRSQueries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class CheckContractsHandler : IQueryHandler<CheckContracts, List<IdentityCheckContractsResponse>>
    {
        private readonly IIdentityServerService _identityServerService;

        public CheckContractsHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<List<IdentityCheckContractsResponse>> HandleAsync(CheckContracts query)
        {
            return await _identityServerService.CheckContractsAsync(query.Language);
        }
    }
}
