﻿using Sensormatic.Tool.Ioc;
using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    public interface IDateTimeProvider : ISingletonDependency
    {
        DateTime Now { get; }
    }
}
