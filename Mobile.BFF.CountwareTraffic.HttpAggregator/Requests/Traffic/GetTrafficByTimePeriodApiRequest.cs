﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetTrafficByTimePeriodApiRequest
    {
        public List<HierarchyTreeSelectFilterRequest> HierarchyTreeSelectFilters { get; set; }
        public DateTime CurrentStartDate { get; set; }
        public DateTime CurrentEndDate { get; set; }
        public DateTime PreviusEndDate { get; set; }
        public DateTime PreviusStartDate { get; set; }
        public TimePeriod TimePeriod { get; set; }
    }
}
