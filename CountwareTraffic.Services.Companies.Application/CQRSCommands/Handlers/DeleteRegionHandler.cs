﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class DeleteRegionHandler : ICommandHandler<DeleteRegion>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public DeleteRegionHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }
       
        public async Task HandleAsync(DeleteRegion command)
        {
            var regionRepository = _unitOfWork.GetRepository<IRegionRepository>();

            var region = await regionRepository.GetAsync(command.RegionId);

            if (region is null) throw new RegionNotFoundException(command.RegionId);

            regionRepository.Remove(region);

            var countryRepositiry = _unitOfWork.GetRepository<ICountryRepository>();

            var hierarchyData = await countryRepositiry.GetHierarchyAsync(region.CountryId, _currentTenant.Id);

            region.WhenDeleted(_currentTenant.Id,  hierarchyData.CopmanyId,  hierarchyData.CountryId,  region.Id, region.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
