﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Listener.Scheduler.CQRSCommands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Sensormatic.Tool.Cache;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Listener.Scheduler.Controllers.System
{
    // [Authorize]
    public class SystemController : BaseController
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly ILogger<SystemController> _logger;
        public SystemController(IServiceProvider provider, ILogger<SystemController> logger, IHttpContextAccessor contextAccessor, ICommandDispatcher commandDispatcher)
            : base(provider, logger, contextAccessor.HttpContext)
        {
            _commandDispatcher = commandDispatcher;
            _logger = logger;
        }

        [HttpDelete("device/realTime-latestevent-remove"), ServiceLog]
        public async Task<OkObjectResult> RealTimeEventSetRemoveAsync(Guid deviceId)
        {
            await _commandDispatcher.SendAsync(new RemoveCache(deviceId));
            return Ok(new { Success = "Success" });
        }


        [HttpPost("device/realTime-latestevent-defaultvalue-add"), ServiceLog]
        public async Task<OkObjectResult> RealTimeEventSetAddAsync([FromBody] AddCacheDefaultValue request)
        {
            await _commandDispatcher.SendAsync(request);
            return Ok(new { Success = "Success" });
        }

        [HttpDelete("device/TimerangeEventSet-remove"), ServiceLog]
        public async Task<OkObjectResult> TimerangeEventSetRemoveAsync(Guid deviceId, [FromServices] ICache _redisCacheService)
        {
            var redisKey = string.Format(CacheKeys.DeviceTimeRangeEventSet, deviceId);
            var res = await _redisCacheService.DeleteAsync(redisKey);
            return Ok(new { Success = res ? "Success" : "Failed" });
        }

        [HttpDelete("migration/migrationEventSet-remove"), ServiceLog]
        public async Task<OkObjectResult> MigrationEventSetRemoveAsync(Guid deviceId, [FromServices] ICache _redisCacheService)
        {
            var redisKey = string.Format(CacheKeys.DeviceMigrationEventSet, deviceId);
            var res = await _redisCacheService.DeleteAsync(redisKey);
            return Ok(new { Success = res ? "Success" : "Failed" });
        }
        /// <summary>
        /// Inject events from old system to new one device by device,
        /// </summary>
        /// <param name="input">ExitCount , EnterCount , EventDate,Id  of event is required</param>
        [HttpPost("migration/create")]
        public async Task<ActionResult> CreateEventAsync(EventInjectorCommand input)
        {

            _logger.LogInformation($" migration/create called.....");

            await _commandDispatcher.SendAsync(input);

            return Ok(new { Success = "migration started,please waite, consumers are working." });
        }
    }
}