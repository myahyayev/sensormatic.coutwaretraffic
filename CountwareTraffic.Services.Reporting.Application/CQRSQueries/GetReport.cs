﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class GetReport : IQuery<ReportDetailDto>
    {
        public Guid ReportId { get; set; }
    }
}
