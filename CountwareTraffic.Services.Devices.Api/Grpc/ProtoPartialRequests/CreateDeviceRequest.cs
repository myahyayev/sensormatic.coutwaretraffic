﻿using Sensormatic.Tool.Core;
using System;
using System.Net;
using Sensormatic.Tool.Common;

namespace CountwareTraffic.Services.Devices.Grpc
{
    [ServiceLog]
    public sealed partial class CreateDeviceRequest : SensormaticRequestValidate
    {
        internal Guid _SubAreaId
        {
            get
            {
                if (Guid.TryParse(subAreaId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { subAreaId_ = value.ToString(); }
        }

        internal Guid _BrandId
        {
            get
            {
                if (Guid.TryParse(brandId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { brandId_ = value.ToString(); }
        }

        internal Guid _ModelId
        {
            get
            {
                if (Guid.TryParse(modelId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { modelId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_SubAreaId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1477 SubAreaId cannot be null", nameof(_SubAreaId)));

            if (_ModelId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1478 ModelId cannot be null", nameof(_ModelId)));

            if (_BrandId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1479 BrandId cannot be null", nameof(_BrandId)));

            if (!Firmware.IsNullOrWhiteSpace() && Firmware.Length > 50)
                ValidateResults.Add(new ErrorResult($"#E1480 Firmware invalid format", nameof(Firmware)));

            if (ControlFrequency < 60 || ControlFrequency > 1440)
                ValidateResults.Add(new ErrorResult($"#E1481 ControlFrequency with value: {ControlFrequency} wrong format", nameof(ControlFrequency)));

            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1482 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1483 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1484 Description invalid format", nameof(Description)));

            if (DeviceTypeId < 1)
                ValidateResults.Add(new ErrorResult($"#E1485 DeviceTypeId with id: {DeviceTypeId} invalid format", nameof(DeviceTypeId)));

            if (!IPAddress.TryParse(IpAddress, out IPAddress ipA))
                ValidateResults.Add(new ErrorResult($"#E1486 IpAddress with value: {IpAddress} wrong format", nameof(IpAddress)));

            if (MacAddress.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1487 MacAddress Value cannot be null", nameof(MacAddress)));

            if (!MacAddress.IsNullOrWhiteSpace() && MacAddress.Length > 17)
                ValidateResults.Add(new ErrorResult($"#E1488 MacAddress invalid format", nameof(MacAddress)));

            if (!Identity.IsNullOrWhiteSpace() && Identity.Length > 17)
                ValidateResults.Add(new ErrorResult($"#E1489 Identity invalid format", nameof(Identity)));

            if (!Password.IsNullOrWhiteSpace() && Password.Length > 16)
                ValidateResults.Add(new ErrorResult($"#E1490 Password invalid format", nameof(Password)));

            if (!UniqueId.IsNullOrWhiteSpace() && UniqueId.Length > 75)
                ValidateResults.Add(new ErrorResult($"#E1491 UniqueId invalid format", nameof(UniqueId)));

            if (Port < 1 || Port > 65535)
                ValidateResults.Add(new ErrorResult($"#E1492 Port with value: {Port} wrong format", nameof(Port)));
        }
    }
}


