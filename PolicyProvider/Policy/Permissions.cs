﻿namespace Countware.Traffic.CrossCC.PolicyProvider
{

    public class Permissions
    {
        //public const string AreaReadData = "area:area:read-data";
        //public const string AreaFilterData = "area:*:filter-data";
        //public const string AreaAdd = "area:*:add";
        //public const string AreaChange = "area:*:change";
        //public const string AreaDelete = "area:*:delete";

        public const string UserList = "User.List";
        public const string ReportEdit="Report.Edit";
        public const string DeviceView="Device.View";
        public const string UserView="User.View";
        public const string RoleEdit="Role.Edit";
        public const string RoleAdd="Role.Add";
        public const string OrganizationChartView="OrganizationChart.View";
        public const string ReportAdd="Report.Add";
        public const string DashboardTrafficView="Dashboard.Traffic.View";
        public const string ReportList="Report.List";
        public const string RoleList="Role.List";
        public const string RoleView="Role.View";
        public const string PermissionList="Permission.List";
        public const string ReportView="Report.View";
        public const string DeviceList="Device.List";
        public const string UserAdd="User.Add";
        public const string ReportDelete="Report.Delete";
        public const string UserEdit="User.Edit";
        public const string UserDelete="User.Delete";
        public const string CompanyView = "Company.View";
        public const string CountryView = "Country.View";
        public const string RegionView = "Region.View";
        public const string AreaView = "Area.View";
        public const string SubAreaView = "SubArea.View";

        //silinecek yetkiler
        public const string AdminUserReset = "Admin.User.Reset";
        public const string AdminPermissionView = "Admin.Permission.View";
        public const string AdminPermissionDelete = "Admin.Permission.Delete";
        public const string AdminPermissionAdd = "Admin.Permission.Add";
        public const string AdminPermissionEdit = "Admin.Permission.Edit";
        public const string AdminGroupView = "Admin.Group.View";
        public const string AdminGroupDelete = "Admin.Group.Delete";
        public const string AdminGroupAdd = "Admin.Group.Add";
        public const string AdminGroupEdit = "Admin.Group.Edit";
        public const string AdminGroupAddUser = "Admin.Group.AddUser";
        public const string AdminGroupList = "Admin.Group.List";
        public const string AdminTokenDelete = "Admin.Token.Delete";
        public const string AdminCityView = "Admin.City.View";
        public const string AdminCityList = "Admin.City.List";
        public const string AdminDistrictView = "Admin.District.View";
        public const string AdminDistrictList = "Admin.District.List";
        public const string AdminUnknownDeviceView = "Admin.UnknownDevice.View";

        public const string AdminTenantGroupList = "Admin.TenantGroup.List";
        public const string AdminTenantGroupView = "Admin.TenantGroup.View";
        public const string AdminTenantGroupAdd = "Admin.TenantGroup.Add";
        public const string AdminTenantGroupEdit = "Admin.TenantGroup.Edit";
        public const string AdminTenantGroupDelete = "Admin.TenantGroup.Delete";
        public const string AdminTenantAdd = "Admin.Tenant.Add";
        public const string AdminTenantEdit = "Admin.Tenant.Edit";
        public const string AdminTenantList = "Admin.Tenant.List";
        public const string AdminTenantView = "Admin.Tenant.View";
        public const string AdminCompanyView = "Admin.Company.View";
        public const string AdminCompanyList = "Admin.Company.List";
        public const string AdminCompanyAdd = "Admin.Company.Add";
        public const string AdminCompanyEdit = "Admin.Company.Edit";
        public const string AdminCompanyDelete = "Admin.Company.Delete";
        public const string AdminAreaView = "Admin.Area.View";
        public const string AdminAreaList = "Admin.Area.List";
        public const string AdminAreaAdd = "Admin.Area.Add";
        public const string AdminAreaEdit = "Admin.Area.Edit";
        public const string AdminAreaDelete = "Admin.Area.Delete";
        public const string AdminCountryView = "Admin.Country.View";
        public const string AdminCountryList = "Admin.Country.List";
        public const string AdminCountryAdd = "Admin.Country.Add";
        public const string AdminCountryEdit = "Admin.Country.Edit";
        public const string AdminCountryDelete = "Admin.Country.Delete";
        public const string AdminDeviceView = "Admin.Device.View";
        public const string AdminDeviceList = "Admin.Device.List";
        public const string AdminDeviceAdd = "Admin.Device.Add";
        public const string AdminDeviceEdit = "Admin.Device.Edit";
        public const string AdminDeviceDelete = "Admin.Device.Delete";
        public const string AdminOrganizationChartView = "Admin.OrganizationChart.View";
        public const string AdminDeviceBrandList = "Admin.DeviceBrand.List";
        public const string AdminDeviceBrandView = "Admin.DeviceBrand.View";
        public const string AdminDeviceBrandAdd = "Admin.DeviceBrand.Add";
        public const string AdminDeviceBrandEdit = "Admin.DeviceBrand.Edit";
        public const string AdminDeviceBrandDelete = "Admin.DeviceBrand.Delete";
        public const string AdminRegionView = "Admin.Region.View";
        public const string AdminRegionList = "Admin.Region.List";
        public const string AdminRegionAdd = "Admin.Region.Add";
        public const string AdminRegionDelete = "Admin.Region.Delete";
        public const string AdminRegionEdit = "Admin.Region.Edit";
        public const string AdminSubAreaView = "Admin.SubArea.View";
        public const string AdminSubAreaList = "Admin.SubArea.List";
        public const string AdminSubAreaAdd = "Admin.SubArea.Add";
        public const string AdminSubAreaChange = "Admin.SubArea.Edit";
        public const string AdminSubAreaDelete = "Admin.SubArea.Delete";
        public const string AdminUnknownDeviceList = "Admin.UnknownDevice.List";
        public const string AdminUserEdit = "Admin.User.Edit";
        public const string AdminContractAdd = "Admin.Contract.Add";
        public const string AdminContractEdit = "Admin.Contract.Edit";
        public const string AdminUnknownDeviceDelete = "Admin.UnknownDevice.Delete";

    }
}
