﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class AreaCreatedRejectedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaCreatedRejected>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public AreaCreatedRejectedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaCreatedRejected queuEvent)
        {
            var areaCreatedRejected = _eventMapper.Map(queuEvent) as AreaCreatedRejected;
            await _eventDispatcher.PublishAsync(areaCreatedRejected);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
