﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityRemovePermissionRoleRequest
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("permissionId")]
        public Guid PermissionId { get; set; }

        [JsonPropertyName("roleId")]
        public Guid RoleId { get; set; }
    }
}
