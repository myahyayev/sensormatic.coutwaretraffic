﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ResetPasswordApiRequest
    {
        [FromForm(Name = "userName")]
        public string UserName { get; set; }
    }
}
