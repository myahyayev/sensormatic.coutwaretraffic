﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleDeliveryTimeNameNotFoundException : DomainException
    {
        IEnumerable<ScheduleDeliveryTime> ScheduleDeliveryTimes { get; }
        public string Name { get; }

        public ScheduleDeliveryTimeNameNotFoundException(IEnumerable<ScheduleDeliveryTime> scheduleDeliveryTimes, string name)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ScheduleDeliveryTime Name: {String.Join(",", scheduleDeliveryTimes.Select(s => s.Name))}") }, 400, ResponseMessageType.Error)
        {
            ScheduleDeliveryTimes = scheduleDeliveryTimes;
            Name = name;
        }
    }
}
