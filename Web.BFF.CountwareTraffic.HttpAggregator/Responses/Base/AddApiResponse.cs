﻿using System;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class AddApiResponse
    {
        public Guid Id { get; set; }
        public string Created { get; set; }
    }
}
