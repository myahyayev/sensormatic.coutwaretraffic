﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class SubAreasController : BaseController
    {
        private readonly SubAreaService _subAreaService;
        private readonly DeviceService _deviceService;
        public SubAreasController(IServiceProvider provider, ILogger<SubAreasController> logger, IHttpContextAccessor contextAccessor, SubAreaService subAreaService, DeviceService deviceService) 
            : base(provider, logger, contextAccessor.HttpContext)
        {
            _subAreaService = subAreaService;
            _deviceService = deviceService;
        }

        [HttpGet("{subAreaId}/detail"), ServiceLog]
        public async Task<ApiResponse<GetSubAreaDetails>> GetSubAreaDetailAsync(Guid subAreaId)
            => Response(await _subAreaService.GetSubAreaByIdAsync(subAreaId), ResultMessage.SuccessResultMessage);
           

        [HttpPost("{areaId}/details"), ServiceLog]
        public async Task<ApiPagedResponse<GetSubAreaDetails>> GetSubAreaDetailsAsync(Guid areaId, [FromBody] DataSourceApiRequest paging)
            => await _subAreaService.GetSubAreasAsync(areaId, paging);


        [HttpPost("{areaId}/add"), ServiceLog]
        public async Task<ApiResponse<AddApiResponse>> AddSubAreaAsync(Guid areaId, [FromForm] AddSubAreaApiRequest request)
        {
            await _subAreaService.AddSubAreaAsync(areaId, request);

            return Response(new AddApiResponse { Created = "Success" }, new ResultMessage()
            {
                Description = $"Subarea has been created.",
                Title = "Subarea has been created.",
                 ResponseMessageType = ApiResponseMessageType.Info
            });
        }

        [HttpPut("{subAreaId}/change"), ServiceLog]
        public async Task<ApiResponse<ChangeApiResponse>> ChangeSubAreaAsync(Guid subAreaId, [FromForm] ChangeSubAreaApiRequest request)
        {
            await _subAreaService.ChangeSubAreaAsync(subAreaId, request);

            return Response(new ChangeApiResponse { Updated = "Success", Id = subAreaId }, new ResultMessage()
            {
                ResponseMessageType = ApiResponseMessageType.Info,
                Description = $"Subarea with {subAreaId} id has been updated.",
                Title = "Subarea with {subAreaId} id has been updated."
            });
        }


        [HttpDelete("{subAreaId}/delete"), ServiceLog]
        public async Task<ApiResponse<DeleteApiResponse>> DeleteSubAreaAsync(Guid subAreaId)
        {
            var subAreaDeviceControl = await _deviceService.GetDevicesAsync(subAreaId, new DataSourceApiRequest());

            if (subAreaDeviceControl.TotalCount > 0)
            {
                return Response(new DeleteApiResponse { Deleted = "Failed", Id = subAreaId }, new ResultMessage()
                {
                    ResponseMessageType = ApiResponseMessageType.Warning,
                    Description = $"A device has been defined in the SubaArea with id {subAreaId}.",
                    Title = $"SubArea with id {subAreaId} cannot be deleted."
                });
            }

            await _subAreaService.DeleteSubAreaAsync(subAreaId);

            return Response(new DeleteApiResponse { Deleted = "Success", Id = subAreaId }, new ResultMessage()
            {
                ResponseMessageType = ApiResponseMessageType.Info,
                Description = $"Subarea with {subAreaId} id has been deleted.",
                Title = $"Subarea with {subAreaId} id has been deleted."
            });
        }
    }
}
