﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetSubAreaHandler : IQueryHandler<GetSubArea, SubAreaDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GetSubAreaHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<SubAreaDetailsDto> HandleAsync(GetSubArea query)
        {
            var subArea = await _unitOfWork.GetRepository<ISubAreaRepository>()
                .GetAsync(query.SubAreaId, _currentTenant.Id);

            if (subArea == null)
                throw new SubAreaNotFoundException(query.SubAreaId);

            return new SubAreaDetailsDto
            {
                Id = subArea.Id,
                Name = subArea.Name,
                AuditCreateBy = subArea.AuditCreateBy,
                AuditCreateDate = subArea.AuditCreateDate,
                AuditModifiedBy = subArea.AuditModifiedBy,
                AuditModifiedDate = subArea.AuditModifiedDate,
                AreaId = subArea.AreaId,
                Description = subArea.Description
            };
        }
    }
}
