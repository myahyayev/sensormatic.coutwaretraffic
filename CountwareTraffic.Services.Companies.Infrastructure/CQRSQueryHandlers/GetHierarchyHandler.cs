﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GeCompanyHierarchyHandler : IQueryHandler<GetCompanyHierarchy, List<HierarchyDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GeCompanyHierarchyHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<List<HierarchyDto>> HandleAsync(GetCompanyHierarchy query)
        {
            var comapnyRepository = _unitOfWork.GetRepository<ICompanyRepository>();

            var hierarchies = await comapnyRepository.GetHierarchyAsync(query.Id, _currentTenant.Id, _currentTenant.Name);

            return hierarchies.Select(u => new HierarchyDto
            {
                HierarchyLevel = u.HierarchyLevel,
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }


    public class GeCountryHierarchyHandler : IQueryHandler<GetCountryHierarchy, List<HierarchyDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GeCountryHierarchyHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<List<HierarchyDto>> HandleAsync(GetCountryHierarchy query)
        {
            var countryRepository = _unitOfWork.GetRepository<ICountryRepository>();

            var hierarchies = await countryRepository.GetHierarchyAsync(query.Id, _currentTenant.Id, _currentTenant.Name);

            return hierarchies.Select(u => new HierarchyDto
            {
                HierarchyLevel = u.HierarchyLevel,
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }

    public class GetRegionHierarchyHandler : IQueryHandler<GetRegionHierarchy, List<HierarchyDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GetRegionHierarchyHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<List<HierarchyDto>> HandleAsync(GetRegionHierarchy query)
        {
            var regionRepository = _unitOfWork.GetRepository<IRegionRepository>();

            var hierarchies =  await regionRepository.GetHierarchyAsync(query.Id, _currentTenant.Id, _currentTenant.Name);

            return hierarchies.Select(u => new HierarchyDto
            {
                HierarchyLevel = u.HierarchyLevel,
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }

    public class GetAreaHierarchyHandler : IQueryHandler<GetAreaHierarchy, List<HierarchyDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GetAreaHierarchyHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<List<HierarchyDto>> HandleAsync(GetAreaHierarchy query)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var hierarchies = await areaRepository.GetHierarchyAsync(query.Id, _currentTenant.Id, _currentTenant.Name);

            return hierarchies.Select(u => new HierarchyDto
            {
                HierarchyLevel = u.HierarchyLevel,
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }

    public class GetSubAreaHierarchyHandler : IQueryHandler<GetSubAreaHierarchy, List<HierarchyDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GetSubAreaHierarchyHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<List<HierarchyDto>> HandleAsync(GetSubAreaHierarchy query)
        {
            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            var hierarchies = await subAreaRepository.GetHierarchyAsync(query.Id, _currentTenant.Id, _currentTenant.Name);

            return hierarchies.Select(u => new HierarchyDto
            {
                HierarchyLevel = u.HierarchyLevel,
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }
}
