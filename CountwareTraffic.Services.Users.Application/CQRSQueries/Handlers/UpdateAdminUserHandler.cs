﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class UpdateAdminUserHandler : IQueryHandler<UpdateAdminUser, IdentityUpdateAdminUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public UpdateAdminUserHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public Task<IdentityUpdateAdminUserResponse> HandleAsync(UpdateAdminUser query)
        {
            return _identityServerService.UpdateAdminUserAsync(new Objects.Requests.IdentityUpdateAdminUserRequest
            {
                Id = query.Id,
                Address = query.Address,
                AllowEmail = query.AllowEmail,
                AllowSms = query.AllowSms,
                Birthdate = query.Birthdate,
                Domain = query.Domain,
                Email = query.Email,
                FirstName = query.FirstName,
                Gender = query.Gender,
                LastName = query.LastName,
                PhoneNumber = query.PhoneNumber,
                ProfilePic = query.ProfilePic,
                Roles = query.Roles,
                Status = query.Status,
                TenantIds = query.TenantIds,
                TwoFactorEnabled = query.TwoFactorEnabled,
                UserName = query.UserName,
                UserType = query.UserType
            }, query.Language);
        }
    }
}
