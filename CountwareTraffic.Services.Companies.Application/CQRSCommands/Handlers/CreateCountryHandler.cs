﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CreateCountryHandler : ICommandHandler<CreateCountry>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public CreateCountryHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(CreateCountry command)
        {
            var countryRepository = _unitOfWork.GetRepository<ICountryRepository>();

            var companyRepository = _unitOfWork.GetRepository<ICompanyRepository>();
            var company = await companyRepository.GetByIdAsync(command.CompanyId);

            if (company == null)
                throw new CompanyNotFoundException(command.CompanyId);

            if ((await countryRepository.ExistsAsync(command.Name, command.CompanyId)))
                throw new CountryAlreadyExistsException(command.Name);

            var country = Country.Create(command.CountryLookupId, command.Iso, command.Iso3, command.IsoNumeric, command.Name, command.Capital, command.ContinentCode, command.CurrencyCode, command.CompanyId);

            await countryRepository.AddAsync(country);

            country.WhenCreated(_currentTenant.Id,  company.Id, country.Id, country.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
