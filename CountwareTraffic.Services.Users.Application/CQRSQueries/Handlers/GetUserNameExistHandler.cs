﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserNameExistHandler : IQueryHandler<GetUserNameExist, IdentityUserNameExistResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetUserNameExistHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<IdentityUserNameExistResponse> HandleAsync(GetUserNameExist query)
        {
            return await _identityServerService.GetUserNameExistAsync(query.UserName);
        }
    }
}
