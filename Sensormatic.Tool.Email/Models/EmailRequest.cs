﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Sensormatic.Tool.Email
{
    public class EmailRequest
    {
        public IList<string> To { get; set; } = new List<string>();
        public IList<string> Cc { get; set; } = new List<string>();
        public IList<string> Bc { get; set; } = new List<string>();
        public string Subject { get; set; }
        public string Body { get; set; }
        public IList<IFormFile> Attachments { get; set; } = new List<IFormFile>();
        public bool IsHtml { get; set; }
    }
}
