﻿using CountwareTraffic.Services.Events.Application;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class EventMapper : IEventMapper
    {
        public IEnumerable<Convey.CQRS.Events.IEvent> MapAll(IEnumerable<Sensormatic.Tool.Queue.IQueueEvent> events)
              => events.Select(Map);

        public Convey.CQRS.Events.IEvent Map(Sensormatic.Tool.Queue.IQueueEvent @event)
        {
            switch (@event)
            {
                case Sensormatic.Tool.QueueModel.AreaCreated e:
                    return new AreaCreated
                    {
                        TenantId = e.TenantId,
                        AreaId = e.AreaId,
                        AreaName = e.AreaName,
                        RecordId = e.RecordId,
                        WorkingHoursStart = e.WorkingHoursStart,
                        WorkingHoursEnd = e.WorkingHoursEnd,
                        WorkingTimeZone = e.WorkingTimeZone,
                        UserId = e.UserId
                    };
                case Sensormatic.Tool.QueueModel.AreaChanged e:
                    return new AreaChanged
                    {
                        TenantId = e.TenantId,
                        AreaId = e.AreaId,
                        AreaName = e.AreaName,
                        OldAreaName = e.OldAreaName,
                        RecordId = e.RecordId,
                        WorkingHoursStart = e.WorkingHoursStart,
                        OldWorkingHoursStart = e.OldWorkingHoursStart,
                        WorkingHoursEnd = e.WorkingHoursEnd,
                        OldWorkingHoursEnd = e.OldWorkingHoursEnd,
                        WorkingTimeZone = e.WorkingTimeZone,
                        OldWorkingTimeZone = e.OldWorkingTimeZone,
                        UserId = e.UserId
                    };
                case Sensormatic.Tool.QueueModel.AreaDeleted e:
                    return new AreaDeleted
                    {
                        TenantId = e.TenantId,
                        AreaId = e.AreaId,
                        RecordId = e.RecordId,
                        UserId = e.UserId
                    };
                case Sensormatic.Tool.QueueModel.DeviceCreated e:
                    return new DeviceCreated
                    {
                        Name = e.Name,
                        DeviceId = e.DeviceId,
                        MacAddress = e.MacAddress,
                        IpAddress = e.IpAddress,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        SubAreaName = e.SubAreaName,
                        SubAreaId = e.SubAreaId,
                        AreaId = e.AreaId,
                        AreaName = e.AreaName,
                        RegionId = e.RegionId,
                        RegionName = e.RegionName,
                        CityId = e.CityId,
                        CityName = e.CityName,
                        CountryId = e.CountryId,
                        CountryName = e.CountryName,
                        CompanyId = e.CompanyId,
                        DistrictId = e.DistrictId,
                        DistrictName = e.DistrictName,
                        CompanyName = e.CompanyName,
                        CountryLookupId = e.CountryLookupId,
                        ControlFrequency = e.ControlFrequency
                    };


                case Sensormatic.Tool.QueueModel.DeviceChanged e:
                    return new DeviceChanged
                    {
                        Name = e.Name,
                        IpAddress = e.IpAddress,
                        MacAddress = e.MacAddress,
                        DeviceId = e.DeviceId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        IsActive = e.IsActive,
                        ControlFrequency = e.ControlFrequency
                    };


                case Sensormatic.Tool.QueueModel.DeviceDeleted e:
                    return new DeviceDeleted
                    {
                        DeviceId = e.DeviceId,
                        UserId = e.UserId,
                        Name = e.Name,
                        TenantId = e.TenantId
                    };


                case Sensormatic.Tool.QueueModel.EventCreated e:

                    Core.DirectionType directionType = e.DirectionType switch
                    {
                        Sensormatic.Tool.QueueModel.DirectionType.Unknown => Core.DirectionType.Unknown,
                        Sensormatic.Tool.QueueModel.DirectionType.Inwards => Core.DirectionType.Inwards,
                        Sensormatic.Tool.QueueModel.DirectionType.Outward => Core.DirectionType.Outward,
                        _ => Core.DirectionType.Unknown
                    };

                    return new EventCreated
                    {
                        DeviceId = e.DeviceId,
                        EventId = e.EventId,
                        EventDate = e.EventDate,
                        EventStartDate = e.EventStartDate,
                        EventEndtDate =e.EventEndDate,
                        DirectionType = directionType,
                        DeviceName = e.DeviceName,
                        AreaId = e.AreaId,
                        AreaName = e.AreaName,
                        CityId = e.CityId,
                        CityName = e.CityName,
                        CompanyId = e.CompanyId,
                        CompanyName = e.CompanyName,
                        CountryId = e.CountryId,
                        CountryName = e.CountryName,
                        DistrictId = e.DistrictId,
                        DistrictName = e.DistrictName,
                        OnTimeIOCount = e.OnTimeIOCount,
                        RegionId = e.RegionId,
                        RegionName = e.RegionName,
                        SubAreaId = e.SubAreaId,
                        SubAreaName = e.SubAreaName,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };
            }


            return null;
        }
    }
}
