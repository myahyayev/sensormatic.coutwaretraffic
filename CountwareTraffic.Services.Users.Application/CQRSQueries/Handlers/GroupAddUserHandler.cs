﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GroupAddUserHandler : IQueryHandler<GroupAddUser, IdentityGroupAddUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GroupAddUserHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityGroupAddUserResponse> HandleAsync(GroupAddUser query)
        {
            return await _identityServerService.GroupAddUserAsync(new IdentityGroupAddUserRequest
            {
                UserId = query.UserId,
                GroupId = query.GroupId,
                StartDate = query.StartDate,
                EndDate = query.EndDate
            });
        }
    }
}
