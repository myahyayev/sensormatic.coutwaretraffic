﻿using CountwareTraffic.Services.Events.Application;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class QueueEventMapper : IQueueEventMapper
    {
        public List<Sensormatic.Tool.Queue.IQueueEvent> MapAll(IEnumerable<Sensormatic.Tool.Efcore.IDomainEvent> events, Guid userId, string correlationId)
               => events.Select(e => Map(e, userId, correlationId)).ToList();

        public Sensormatic.Tool.Queue.IQueueEvent Map(Sensormatic.Tool.Efcore.IDomainEvent @event, Guid userId, string correlationId)
        {
            switch (@event)
            {
                case Core.EventCreateCompleted e:

                    Sensormatic.Tool.QueueModel.DirectionType directionType = e.DirectionType.Name switch
                    {
                        "Unknown" => Sensormatic.Tool.QueueModel.DirectionType.Unknown,
                        "Inwards" => Sensormatic.Tool.QueueModel.DirectionType.Inwards,
                        "Outward" => Sensormatic.Tool.QueueModel.DirectionType.Outward,
                        _ => Sensormatic.Tool.QueueModel.DirectionType.Unknown,
                    };

                    return new Sensormatic.Tool.QueueModel.EventCreated(
                        e.EventId,
                        directionType,
                        e.EventDate,
                        e.EventStartDate,
                        e.EventEndDate,
                        e.OnTimeIOCount,
                        e.DeviceId,
                        e.DeviceName,
                        e.SubAreaId,
                        e.SubAreaName,
                        e.AreaId,
                        e.AreaName,
                        e.CityId,
                        e.CityName,
                        e.DistrictId,
                        e.DistrictName,
                        e.RegionId,
                        e.RegionName,
                        e.CountryId,
                        e.CountryName,
                        e.CompanyId,
                        e.CompanyName,
                        e.RecordId,
                        userId,
                        correlationId,
                        e.TenantId
                    );

                case Core.UnknownDeviceCreateComplated e:
                    return new Sensormatic.Tool.QueueModel.UnknownDeviceCreated(
                        e.Name,
                        e.MacAddress,
                        e.IpAddress,
                        e.SerialNumber,
                        e.HttpsPort,
                        e.HttpsPort,
                        e.RecordId,
                        userId,
                        correlationId,
                        Guid.NewGuid()
                    );
            }

            return null;
        }
        public void Dispose() => GC.SuppressFinalize(this);
    }
}
