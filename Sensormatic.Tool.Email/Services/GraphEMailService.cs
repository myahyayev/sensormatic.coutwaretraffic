﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sensormatic.Tool.Email
{

    public class GraphEMailService : IEmailService
    {
        private readonly IGraphService _graphService;
        private readonly GraphEmailOptions _appSettings;
        private readonly ILogger<GraphEMailService> _logger;
        public GraphEMailService(IOptions<GraphEmailOptions> appSettings, IGraphService graphService, ILogger<GraphEMailService> logger)
        {
            _logger = logger;
            _graphService = graphService;
            _appSettings = appSettings.Value;
        }
        public async Task<EmailResponse> SendAsync(EmailRequest request)
        {

            var message = new Message
            {
                Subject = request.Subject,
                Body = new ItemBody
                {
                    ContentType = request.IsHtml ? BodyType.Html : BodyType.Text,
                    Content = request.Body
                },
                ToRecipients = request
                                .To?
                                .Select(i => new Recipient { EmailAddress = new EmailAddress { Address = i } })
                                .ToList<Recipient>()
                ,
                ReplyTo = new List<Recipient>()
                ,
                CcRecipients = request
                                .Cc?
                                .Select(i => new Recipient { EmailAddress = new EmailAddress { Address = i } })
                                .ToList<Recipient>()
                ,
                BccRecipients = request
                                .Bc?
                                .Select(i => new Recipient { EmailAddress = new EmailAddress { Address = i } })
                                .ToList<Recipient>()
                ,
                Attachments = new MessageAttachmentsCollectionPage()
                ,
                IsReadReceiptRequested = _appSettings?.IsReadReceiptRequested
            };

            if (request.Attachments != null)
            {

                foreach (var file in request.Attachments)
                {
                    if (file.Length > 0)
                    {

                        var fileAttachment = new FileAttachment
                        {
                            ODataType = "#microsoft.graph.fileAttachment",
                            Name = Path.GetFileName(file.Name),
                            ContentBytes = System.IO.File.ReadAllBytes(Path.GetFullPath(file.Name))
                        };

                        message.Attachments.Add(fileAttachment);

                    }
                }
            }

            var client = _graphService.GetAuthenticatedGraphClient();

            await client.Users[_appSettings?.FromMailAddress]
                .SendMail(message, _appSettings?.SaveSentItems)
                .Request()
                .PostAsync();

            _logger.LogInformation($"Email successfully send to {{EmailAddresses}} Email: {{EmailBody}}", string.Join(',', request.To), request.Body);

            return new EmailResponse { IsSuccess = true, Message = "Success" };
        }
        public void Dispose() => GC.SuppressFinalize(this);
    }
}
