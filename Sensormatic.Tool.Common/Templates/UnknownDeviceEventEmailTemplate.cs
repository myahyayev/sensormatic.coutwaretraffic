﻿using System;
using System.Collections.Generic;

namespace Sensormatic.Tool.Common
{
    public class UnknownDeviceEventEmailTemplate : IEmailTemplate
    {
        public string TemplateName => EmailTemplates.CountwareUnknownDeviceEventEmail;
        public List<Guid> UserIds { get; set; }
        public string MacAddress { get; init; }
        public string IpAddress { get; init; }
        public string DeviceName { get; init; }
        public string SerialNumber { get; init; }
        public string HttpPort { get; init; }
        public string HttpsPort { get; init; }
    }
}
