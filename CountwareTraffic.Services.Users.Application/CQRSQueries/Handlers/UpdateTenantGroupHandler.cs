﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class UpdateTenantGroupHandler : IQueryHandler<UpdateTenantGroup, IdentityUpdateTenantGroupResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public UpdateTenantGroupHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityUpdateTenantGroupResponse> HandleAsync(UpdateTenantGroup query)
        {
            return await _identityServerService.UpdateTenantGroupAsync(new IdentityUpdateTenantGroupRequest
            {
                Id = query.Id,
                Name = query.Name,
                Description = query.Description
            });
        }
    }
}
