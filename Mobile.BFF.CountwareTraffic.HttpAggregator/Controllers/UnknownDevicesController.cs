﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class UnknownDevicesController : BaseController
    {
        private readonly UnknownDeviceService _unknownDeviceService;
        public UnknownDevicesController(IServiceProvider provider, ILogger<UnknownDevicesController> logger, IHttpContextAccessor contextAccessor, UnknownDeviceService unknownDeviceService)
            : base(provider, logger, contextAccessor) => _unknownDeviceService = unknownDeviceService;


        [HttpGet("{unknownDeviceId}/detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminUnknownDeviceView })]
        public async Task<ApiResponse<GetUnknownDeviceDetails>> GetUnknowDeviceDetailAsync(Guid unknownDeviceId)
            => Response(await _unknownDeviceService.GetUnknownDeviceByIdAsync(unknownDeviceId), new ResultMessage("#S1132"));


        [HttpGet("details")]
        [Permissions(Permissions = new[] { Permissions.AdminUnknownDeviceList })]
        public async Task<ApiResponse<IEnumerable<GetUnknownDeviceDetails>>> GetUnknownDeviceDetailsAsync()
           => Response(await _unknownDeviceService.GetUnknownDevicesAsync(), new ResultMessage("#S1133"));

        [HttpGet("dismiss")]
        [Permissions(Permissions = new[] { Permissions.AdminUnknownDeviceDelete })]
        public async Task<ApiResponse<UpdateSuccessApiResponse>> UnknownDeviceDismissAsync(Guid unknownDeviceId)
           => Response(await _unknownDeviceService.UnknownDeviceDismissAsync(unknownDeviceId), new ResultMessage("#S1142"));
    }
}
