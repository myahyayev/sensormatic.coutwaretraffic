﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class HierarchyTree
    {
        public Parent Parent { get; set; }
    }

    public class Parent
    {
        public bool Expanded { get; set; } = true;
        public string Label { get; set; }
        public Data Data { get; set; }
        public List<Child> Children { get; set; }
    }

    public class Data
    {
        public Guid Id { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }

    public class Child
    {
        public bool Expanded { get; set; } = true;
        public string Label { get; set; }
        public Data Data { get; set; }
        public List<Child> Children { get; set; }
    }
}


