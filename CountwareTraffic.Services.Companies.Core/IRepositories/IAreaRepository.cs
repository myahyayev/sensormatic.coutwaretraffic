﻿using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Core
{
    public interface IAreaRepository : IRepository<Area>, IScopedDependency
    {
        Task<Area> GetAsync(Guid id, Guid tenantId);
        Task<Area> GetAsync(string name);
        Task<QueryablePagingValue<Area>> GetAllAsync(Guid regionId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts);
        Task<bool> ExistsAsync(string name);
        Task<bool> ExistsAsync(Guid id);
        Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName);
        Task<HierarchyCteData> GetHierarchyAsync(Guid id, Guid tenantId);
        Task<List<KeyValue>> GetAllKeyValueAsync(Guid parentId);
    }
}
