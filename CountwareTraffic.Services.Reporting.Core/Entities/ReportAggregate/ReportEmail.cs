﻿using Sensormatic.Tool.Efcore;

namespace CountwareTraffic.Services.Reporting.Core
{
    public record ReportEmail :IDeletableValueObject
    {
        private string _to;
        private string _subject;
        private string _body;

        public string To => _to;
        public string Subject => _subject;
        public string Body => _body;

        private ReportEmail() { }

        public static ReportEmail Create(string to, string subject,  string body)
        {
            return new ReportEmail
            {
                _to = to,
                _subject = subject,
                _body = body
            };
        }
    }
}

