﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record ReportGeneratingFailed : MessageEnvelope, IQueueEvent
    {
        public ReportGeneratingFailed(Guid reportId, Guid fileId, int reportStatus, Guid recordId, Guid userId, string correlationId, Guid tenantId)
           : base(userId, correlationId, tenantId)
        {
            ReportId = reportId;
            ReportStatus = reportStatus;
            RecordId = recordId;
            FileId = fileId;
        }

        public Guid FileId { get; init; }
        public Guid ReportId { get; init; }
        public int ReportStatus { get; init; }
        public Guid RecordId { get; init; }
    }
}
