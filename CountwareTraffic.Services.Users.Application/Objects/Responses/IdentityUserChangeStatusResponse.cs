﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityUserChangeStatusResponse
    {
        public string Type { get; set; }
        public string Message { get; set; }

        public List<IdentityUserchangeStatusResponseError> Errors { get; set; }
    }

    public class IdentityUserchangeStatusResponseError
    {
        public string Message { get; set; }
    }
}
