﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record AreaCreated : MessageEnvelope, IQueueEvent
    {
        public AreaCreated(Guid areaId, string areaName, Guid recordId,
            TimeSpan? workingHoursStart, TimeSpan? workingHoursEnd, string workingTimeZone, 
            Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            AreaId = areaId;
            AreaName = areaName;
            RecordId = recordId;
            WorkingHoursStart = workingHoursStart;
            WorkingHoursEnd = workingHoursEnd;
            WorkingTimeZone = workingTimeZone;
        }
        public Guid AreaId { get; }
        public string AreaName { get; }
        public Guid RecordId { get; init; }
        public TimeSpan? WorkingHoursStart { get; }
        public TimeSpan? WorkingHoursEnd { get; }
        public string WorkingTimeZone { get; set; }
    }
}
