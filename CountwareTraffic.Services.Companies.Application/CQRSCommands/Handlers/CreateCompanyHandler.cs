﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CreateCompanyHandler : ICommandHandler<CreateCompany>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public CreateCompanyHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(CreateCompany command)
        {
            var companyRepository = _unitOfWork.GetRepository<ICompanyRepository>();

            if (await companyRepository.ExistsAsync(command.Name))
                throw new CompanyAlreadyExistsException(command.Name);

            var address = CompanyAddress.Create(command.Street, command.City, command.State, command.Country, command.ZipCode, command.Latitude, command.Longitude);

            var contract = CompanyContact.Create(command.GsmDialCode, command.GsmNumber, command.PhoneDialCode, command.PhoneNumber, command.EmailAddress, command.PersonName, command.GsmDialCodeSecondary, command.GsmNumberSecondary, command.PhoneDialCodeSecondary, command.PhoneNumberSecondary, command.EmailAddressSecondary, command.PersonNameSecondary, command.GsmCountryCode, command.PhoneCountryCode, command.GsmCountryCodeSecondary, command.PhoneCountryCodeSecondary);

            var company = Company.Create(command.Name, command.Description, address, contract);

            await companyRepository.AddAsync(company);

            company.WhenCreated(_currentTenant.Id, _currentTenant.Name, company.Id, company.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
