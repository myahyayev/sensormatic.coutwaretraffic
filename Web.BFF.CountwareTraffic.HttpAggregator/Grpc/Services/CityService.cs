﻿using CountwareTraffic.Services.Companies.Grpc;
using Web.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class CityService : IScopedSelfDependency
    {
        private readonly City.CityClient _cityClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;
        public CityService(City.CityClient cityClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _cityClient = cityClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetCityDetails> GetCityByIdAsync(Guid cityId)
        {
            GetCityRequest grpcRequest = new() {  CityId = cityId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_cityClient.GetCityByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetCityDetails
            {
                Id = new Guid(grpcResponse.CityDetail.Id),
                CountryId = new Guid(grpcResponse.CityDetail.CountryId),
                Name = grpcResponse.CityDetail.Name,
                AuditCreateBy = new Guid(grpcResponse.CityDetail.Audit.AuditCreateBy),
                AuditCreateDate = grpcResponse.CityDetail.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(grpcResponse.CityDetail.Audit.AuditModifiedBy),
                AuditModifiedDate = grpcResponse.CityDetail.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime
            };
        }

        public async Task<ApiPagedResponse<GetCityDetails>> GetCitiesAsync(Guid countryId, DataSourceApiRequest paging)
        {
            GetCitiesRequest grpcRequest = new()
            {
                CountryId = countryId.ToString(),
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Companies.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Companies.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_cityClient.GetCitiesAsync, grpcRequest, hasClientSideLog: false);

            var cities = grpcResponse.CityDetails.Select(city => new GetCityDetails
            {
                Id = new Guid(city.Id),
                Name = city.Name,
                CountryId = new Guid(city.CountryId),
                AuditCreateBy = new Guid(city.Audit.AuditCreateBy),
                AuditCreateDate = city.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(city.Audit.AuditModifiedBy),
                AuditModifiedDate = city.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
            });

            return new ApiPagedResponse<GetCityDetails>(cities, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task AddCityAsync(Guid countryId, AddCityApiRequest request)
        {
            CreateCityRequest grpcRequest = new()
            {
                CountryId = countryId.ToString(),
                Name = request.Name
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_cityClient.AddCityAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task ChangeCityAsync(Guid cityId, ChangeCityApiRequest request)
        {
            UpdateCityRequest grpcRequest = new()
            {
                CityId = cityId.ToString(),
                Name = request.Name
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_cityClient.ChangeCityAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task DeleteCityAsync(Guid cityId)
        {
            DeleteCityRequest grpcRequest = new() { CityId = cityId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_cityClient.DeleteCityAsync, grpcRequest, hasClientSideLog: false);
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
