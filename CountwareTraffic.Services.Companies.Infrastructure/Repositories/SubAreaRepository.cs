﻿using CountwareTraffic.Services.Companies.Core;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class SubAreaRepository : Repository<SubArea>, ISubAreaRepository
    {
        private readonly new AreaDbContext _context;
        public SubAreaRepository(AreaDbContext context) : base(context) => _context = context;

        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible

        public async Task<SubArea> GetAsync(Guid id, Guid tenantId) => await base.GetQuery(null).SingleOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);

        public async Task<bool> ExistsAsync(string name, Guid tenantId) 
            => await base.GetQuery(null).AnyAsync(u => u.Name == name && u.AuditIsDeleted == false && u.TenantId == tenantId);

        public async Task<SubArea> GetDeletedAsync(Guid id, Guid tenantId) 
            => await _context.SubAreas.Where(u => u.AuditIsDeleted).SingleOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);

        public async Task<QueryablePagingValue<SubArea>> GetAllAsync(Guid areaId, Guid tenantId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts)
        {
            var query = base.GetQuery(null);

            query = query.ApplyFilter(filters);

            query = query.ApplyOrderBy(sorts);

            var total = await query.CountAsync(x => x.AreaId == areaId && x.TenantId == tenantId);

            if (total > 0)
            {
                query = query.Where(u => u.AreaId == areaId && u.TenantId == tenantId)
                             .Skip((paging.Page - 1) * paging.Limit)
                             .Take(paging.Limit);

                var result = await query.ToListAsync();

                return new QueryablePagingValue<SubArea>(result, total);
            }
            return null;
        }

        public async Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName)
        {
            var sql = @"WITH Companies (Id, Name) AS
                                (
                                   SELECT
                                      Id,
                                      Name
                                      FROM [areas].[Companies] AS Companies WHERE AuditIsDeleted = 0 AND TenantId = @TenantId
                                ),

                                Countries (Id, CompanyId, Name) AS
                                (
                                   SELECT
                                      Id,
                                      CompanyId,
                                      Name
	                                  FROM [areas].[Countries] AS Countries  WHERE AuditIsDeleted = 0  AND TenantId = @TenantId
                                ),

                                Regions (Id, CountryId, Name) AS
                                (
                                   SELECT
                                      Id,
                                      CountryId,
                                      Name
	                                  FROM [areas].[Regions] AS Regions  WHERE  AuditIsDeleted = 0  AND TenantId = @TenantId
                                ),

                                Areas (Id, RegionId, Name) AS
                                (
                                   SELECT
                                      Id,
                                      RegionId,
                                      Name
	                                  FROM [areas].[Areas] AS Regions  WHERE AuditIsDeleted = 0  AND TenantId = @TenantId
                                ),

                                SubAreas(Id, AreaId, Name) AS
                                (
                                  SELECT
                                      Id,
                                      AreaId,
                                      Name
	                                  FROM [areas].[SubAreas] AS SubAreas  WHERE Id = @Id AND AuditIsDeleted = 0  AND TenantId = @TenantId
                                )

                                SELECT 
	                                Companies.Id AS CopmanyId, 
	                                Companies.Name AS CopmanyName, 
	                                Countries.Id  AS CountryId, 
	                                Countries.Name AS CountryName,
	                                Regions.Id AS RegionId, 
	                                Regions.Name AS RegionName,
	                                Areas.Id  AS AreaId,
	                                Areas.Name AS AreaName,
	                                SubAreas.Id  AS SubAreaId,
	                                SubAreas.Name AS SubAreaName
	                                FROM SubAreas
	                                INNER JOIn Areas      ON SubAreas.AreaId  = Areas.Id
	                                INNER JOIN Regions    ON Areas.RegionId  = Regions.Id
                                    INNER JOIN Countries  ON Regions.CountryId = Countries.Id
	                                INNER JOIN Companies  ON Countries.CompanyId = Companies.Id";

            var connection = _context.Database.GetDbConnection();

            var data = await connection.QueryFirstOrDefaultAsync<HierarchyCteData>(sql, new
            {
                @TenantId = tenantId,
                @Id = id
            });

            var response = new List<Hierarchy>();

            response.Add(new Hierarchy() { Id = tenantId, Name = tenantName, HierarchyLevel = HierarchyLevel.Tenant });
            response.Add(new Hierarchy() { Id = data.CopmanyId, Name = data.CopmanyName, HierarchyLevel = HierarchyLevel.Company });
            response.Add(new Hierarchy() { Id = data.CountryId, Name = data.CountryName, HierarchyLevel = HierarchyLevel.Country });
            response.Add(new Hierarchy() { Id = data.RegionId, Name = data.RegionName, HierarchyLevel = HierarchyLevel.Region });
            response.Add(new Hierarchy() { Id = data.AreaId, Name = data.AreaName, HierarchyLevel = HierarchyLevel.Area });
            response.Add(new Hierarchy() { Id = data.SubAreaId, Name = data.SubAreaName, HierarchyLevel = HierarchyLevel.SubArea });
            return response;
        }

        public async Task<List<KeyValue>> GetAllKeyValueAsync(Guid parentId)
        {
            var query = base.GetQuery(null);

            return await query.Where(u => u.AreaId == parentId).Select(x => new KeyValue { Id = x.Id, Name = x.Name }).ToListAsync();
        }
    }
}
