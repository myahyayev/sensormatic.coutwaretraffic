﻿using NetTopologySuite.Geometries;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;

namespace CountwareTraffic.Services.Companies.Core
{
    public record AreaAddress : IDeletableValueObject
    {
        private string _street;
        private Point _location;

        public string Street => _street;
        public Point Location => _location;

        private AreaAddress() { }

        public static AreaAddress Create(string street, double latitude, double longitude)
        {
            if (!street.IsNullOrWhiteSpace() && street.Length > 250)
                throw new InvalidIAddressException(nameof(street), street);

            return new AreaAddress
            {
                _street = street,
                _location = CreateLoaction(latitude, longitude)
            };
        }

        public void WhenChanged(string street, double latitude, double longitude)
        {
            if (!street.IsNullOrWhiteSpace() && street.Length > 250)
                throw new InvalidIAddressException(nameof(street), street);

            _street = street;
            _location = CreateLoaction(latitude, longitude);
        }

        private static Point CreateLoaction(double latitude, double longitude)
        {
            Coordinate coordinate = new() { X = longitude, Y = latitude };

            Point point = new(coordinate);

            point.SRID = 4326;

            return point;
        }
    }
}
