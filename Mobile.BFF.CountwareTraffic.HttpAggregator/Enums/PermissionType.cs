﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public enum PermissionType
    {
        Client = 1,
        Admin = 2
    }
}
