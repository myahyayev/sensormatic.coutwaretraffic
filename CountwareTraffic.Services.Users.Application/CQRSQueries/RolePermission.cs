﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class RolePermission : IQuery<IdentityCreateRolePermissionResponse>
    {
        public Guid PermissionId { get; set; }
        public string RoleId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
