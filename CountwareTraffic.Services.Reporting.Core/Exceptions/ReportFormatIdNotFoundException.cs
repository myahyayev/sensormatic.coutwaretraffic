﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportFormatIdNotFoundException : DomainException
    {
        IEnumerable<ReportFormat> ReportFormats { get; }
        public int Id { get; }

        public ReportFormatIdNotFoundException(IEnumerable<ReportFormat> reportFormats, int id)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ReportFormat Id: {String.Join(",", reportFormats.Select(s => s.Id))}") }, 400, ResponseMessageType.Error)
        {
            ReportFormats = reportFormats;
            Id = id;
        }
    }
}
