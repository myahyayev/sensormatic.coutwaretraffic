﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class DeviceChanged : Convey.CQRS.Events.IEvent
    {
        public Guid DeviceId { get; init; }
        public string Name { get; init; }
        public string OldName { get; set; }
        public Guid UserId { get; init; }
        public string IpAddress { get; set; }
        public string  MacAddress { get; set; }
        public Guid TenantId { get; set; }
        public bool IsActive { get; set; }
        public int ControlFrequency { get; set; }
    }
}
