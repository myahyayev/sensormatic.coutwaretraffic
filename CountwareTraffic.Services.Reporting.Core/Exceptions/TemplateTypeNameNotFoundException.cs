﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class TemplateTypeNameNotFoundException : DomainException
    {
        IEnumerable<TemplateType> TemplateTypes { get; }
        public string Name { get; }

        public TemplateTypeNameNotFoundException(IEnumerable<TemplateType> templateTypes, string name)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for TemplateType Name: {String.Join(",", templateTypes.Select(s => s.Name))}") }, 400, ResponseMessageType.Error)
        {
            TemplateTypes = templateTypes;
            Name = name;
        }
    }
}
