﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Devices.Application
{
    [Contract]
    public class DeleteBrandLookup : ICommand
    {
        public Guid BrandId { get; set; }
    }
}
