﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetDistrictLookupDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid CityId { get; set; }
    }
}
