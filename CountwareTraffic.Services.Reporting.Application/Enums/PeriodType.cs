﻿namespace CountwareTraffic.Services.Reporting.Application
{
    public enum PeriodType : byte
    {
        Today = 1,
        Yesterday = 2,
        ThisWeek = 3,
        ThisMonth = 4
    }
}
