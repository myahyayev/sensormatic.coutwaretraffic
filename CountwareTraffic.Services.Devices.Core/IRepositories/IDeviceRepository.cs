﻿using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Core
{
    public interface IDeviceRepository : IRepository<Device>, IScopedDependency
    {
        Task<Device> GetByMacAsync(string macAddress);
        Task<Device> GetByIPAddressAndMacAsync(string ipAddress, string macAddress);
        Task<Device> GetAsync(Guid id, Guid tenatId);
        Task<QueryablePagingValue<Device>> GetAllAsync(Guid? subAreaId, Guid tenantId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts);
        Task<bool> ExistsAsync(string name, Guid tenatId);
        Task<Device> GetDeletedAsync(Guid id, Guid tenatId);
        Task<DeviceHierarchyCte> GetDeviceHierarchyAsync(Guid id, Guid tenantId);
        Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName);
        Task<QueryablePagingValue<DeviceFilteredData>> GetDevicesTreeSelectAsync(List<NodeFilter> nodeFilters, Guid tenantId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts);
        Task<Device> GetAsync(string name);
    }
}
