﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteTenantGroup : IQuery<IdentityDeleteTenantGroupResponse>
    {
        public Guid Id { get; set; }
    }
}
