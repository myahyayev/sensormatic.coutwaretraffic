﻿using CountwareTraffic.Services.Events.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class UnknownDeviceRepository : Repository<UnknownDevice>, IUnknownDeviceRepository
    {
        private readonly new EventDbContext _context;

        public UnknownDeviceRepository(EventDbContext context) : base(context)
        {
            _context = context;
        }

        #region dispose
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion dispose

        public async Task<UnknownDevice> GetByIPAddressAndMacAsync(string ipAddress, string macAddress)
            => await base.GetQuery(Guid.Empty).SingleOrDefaultAsync(x => x.IpAddress == ipAddress && x.MacAddress == macAddress && !x.IsProcessed);

        public async Task<bool> ExistsAsync(string ipAddress, string macAddress)
            => await base.GetQuery(Guid.Empty).AnyAsync(x => x.IpAddress == ipAddress && x.MacAddress == macAddress && !x.IsProcessed);

        public async Task<IEnumerable<UnknownDevice>> GetUnusedAsync()
            => await base.GetQuery(Guid.Empty).Where(u => u.IsProcessed == false).OrderBy(u => u.OccurredOn).ToListAsync();

    }
}
