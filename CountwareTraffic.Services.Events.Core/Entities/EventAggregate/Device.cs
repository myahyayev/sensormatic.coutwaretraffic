﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Events.Core
{
    public class Device : IEntity, IDeletable
    {
        private Guid _id;
        private string _name;
        private string _macAddress;
        private string _ipAddress;
        private bool _isActive;
        private Guid _subAreaId;
        private string _subAreaName;
        private Guid _areaId;
        private string _areaName;
        private Guid _regionId;
        private string _regionName;
        private Guid _countryId;
        private string _countryName;
        private Guid _companyId;
        private string _companyName;
        private Guid _countryLookupId;
        private Guid _cityId;
        private string _cityName;
        private Guid _districtId;
        private string _districtName;
        private int _controlFrequency;

        public Guid Id => _id;
        public string Name => _name;
        public string MacAddress => _macAddress;
        public string IpAddress => _ipAddress;
        public bool IsActive => _isActive;
        public Guid SubAreaId => _subAreaId;
        public string SubAreaName => _subAreaName;
        public Guid AreaId => _areaId;
        public string AreaName => _areaName;
        public Guid RegionId => _regionId;
        public string RegionName => _regionName;
        public Guid CountryId => _countryId;
        public string CountryName => _countryName;
        public Guid CompanyId => _companyId;
        public string CompanyName => _companyName;
        public Guid CountryLookupId => _countryLookupId;
        public Guid CityId => _cityId;
        public string CityName => _cityName;
        public Guid DistrictId => _districtId;
        public string DistrictName => _districtName;
        public Guid TenantId { get; set; }
        public bool AuditIsDeleted { get; set ; }
        public int ControlFrequency => _controlFrequency;

        public static Device Create(Guid id, string name, string macAddress, string ipAddress, Guid subAreaId, string subAreaName, Guid areaId, string areaName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid countryLookupId, Guid cityId, string cityName, Guid districtId, string districtName, Guid tennantId, int controlFrequency)
        {
            return new Device
            {
                _id = id,
                _name = name,
                _macAddress = macAddress,
                _ipAddress = ipAddress,
                _subAreaName = subAreaName,
                _subAreaId = subAreaId,
                _areaId = areaId,
                _areaName = areaName,
                _regionId = regionId,
                _regionName = regionName,
                _countryId = countryId,
                _countryName = countryName,
                _companyId = companyId,
                _companyName = companyName,
                _countryLookupId = countryLookupId,
                _cityId = cityId,
                _cityName = cityName,
                _districtId = districtId,
                _districtName = districtName,
                _isActive = true,
                _controlFrequency = controlFrequency,
                TenantId = tennantId
            };
        }

        public void Change(string name, string ipAddress, string macAddress, bool isActive, int controlFrequency)
        {
            _name = name;
            _ipAddress = ipAddress;
            _macAddress = macAddress;
            _isActive = isActive;
            _controlFrequency = controlFrequency;
        }
    }
}
