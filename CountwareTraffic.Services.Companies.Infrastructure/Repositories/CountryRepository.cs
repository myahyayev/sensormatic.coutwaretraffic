﻿using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure.Repositories
{
    public class CountryRepository : Repository<Country>, ICountryRepository
    {
        private readonly new AreaDbContext _context;
        private readonly ICurrentTenant _currentTenant;
        public CountryRepository(AreaDbContext context, ICurrentTenant currentTenant) : base(context)
        {
            _context = context;
            _currentTenant = currentTenant;
        }


        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible

        public async Task<bool> ExistsAsync(string name,  Guid companyId) => await base.GetQuery(_currentTenant.Id).AnyAsync(u => u.Name == name && u.CompanyId == companyId);

        public async Task<Country> GetAsync(Guid id) => await base.GetQuery(_currentTenant.Id).SingleOrDefaultAsync(x => x.Id == id);

        public async Task<Country> GetAsync(string name, Guid companyId) => await base.GetQuery(_currentTenant.Id).SingleOrDefaultAsync(x => x.Name == name && x.CompanyId == companyId);

        public async Task<bool> ExistsAsync(Guid id) => await base.GetQuery(_currentTenant.Id).AnyAsync(u => u.Id == id);

        public async Task<QueryablePagingValue<Country>> GetAllAsync(Guid companyId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts)
        {
            var query = base.GetQuery(_currentTenant.Id);

            query = query.ApplyFilter(filters);
            query = query.ApplyOrderBy(sorts);

            var total = await query.CountAsync(u => u.CompanyId == companyId);

            if (total > 0)
            {
                query = query.Where(u => u.CompanyId == companyId)
                             .Skip((paging.Page - 1) * paging.Limit)
                             .Take(paging.Limit);

               

                var result = await query.ToListAsync();

                return new QueryablePagingValue<Country>(result, total);
            }

            return null;
        }

        public async Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName)
        {
            var data = await GetHierarchyAsync(id, tenantId);

            var response = new List<Hierarchy>();

            response.Add(new Hierarchy() { Id = tenantId, Name = tenantName, HierarchyLevel = HierarchyLevel.Tenant });
            response.Add(new Hierarchy() { Id = data.CopmanyId, Name = data.CopmanyName, HierarchyLevel = HierarchyLevel.Company });
            response.Add(new Hierarchy() { Id = data.CountryId, Name = data.CountryName, HierarchyLevel = HierarchyLevel.Country });
            return response;
        }

        public async Task<HierarchyCteData> GetHierarchyAsync(Guid id, Guid tenantId)
        {
            var sql = @"WITH Companies (Id, Name) AS
                        (
                           SELECT
                              Id,
                              Name
                              FROM [areas].[Companies] AS Companies WHERE AuditIsDeleted = 0 AND TenantId = @TenantId
                        ),

                        Countries (Id, CompanyId, Name) AS
                        (
                           SELECT
                              Id,
                              CompanyId,
                              Name
	                          FROM [areas].[Countries] AS Countries  WHERE Id = @Id AND AuditIsDeleted = 0  AND TenantId = @TenantId
                        )
                        SELECT 
	                        Companies.Id AS CopmanyId, 
	                        Companies.Name AS CopmanyName, 
	                        Countries.Id  AS CountryId, 
	                        Countries.Name AS CountryName
	                        FROM Countries
	                        INNER JOIN Companies  ON Countries.CompanyId = Companies.Id";

            var connection = _context.Database.GetDbConnection();

            var data = await connection.QueryFirstOrDefaultAsync<HierarchyCteData>(sql, new
            {
                @TenantId = tenantId,
                @Id = id
            });

            return data;
        }

        public async Task<List<KeyValue>> GetAllKeyValueAsync(Guid parentId)
        {
            var query = base.GetQuery(_currentTenant.Id);

            return await query.Where(u => u.CompanyId == parentId).Select(x => new KeyValue { Id = x.Id, Name = x.Name }).ToListAsync();
        }
    }
}
