﻿using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class TemplateTypeDto
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
    }
}
