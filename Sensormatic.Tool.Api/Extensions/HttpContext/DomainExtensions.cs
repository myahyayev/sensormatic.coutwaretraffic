﻿using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;

namespace Sensormatic.Tool.Api
{
    public static class DomainExtensions
    {
        public static string GetSubDomain(this IHttpContextAccessor context)
        {
            var subDomain = string.Empty;
            var host = context.HttpContext.Request.Headers["Referer"].ToString().ToLower();
            host = host.Replace("http://", "");
            host = host.Replace("https://", "");
            if (!string.IsNullOrWhiteSpace(host))
            {
                subDomain = host.Split('.')[0];
                subDomain = subDomain.Split(':')[0];
            }

            return subDomain.Trim().ToLower();
        }

        public static string GetDomain(this IHttpContextAccessor context)
        {
            var host = context.HttpContext.Request.Headers["Referer"].ToString();
            host = Regex.Match(host, @"(http:|https:)\/\/(.*?)\/").ToString();
            return host;
        }

    }
}
