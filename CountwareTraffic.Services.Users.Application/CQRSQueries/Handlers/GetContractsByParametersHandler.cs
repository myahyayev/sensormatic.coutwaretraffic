﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class GetContractsByParametersHandler
        : IQueryHandler<GetContractsByParameters, IdentityGetContractsByParametersResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetContractsByParametersHandler(IIdentityServerService identityServerService)
        {
            _identityServerService = identityServerService;
        }

        public async Task<IdentityGetContractsByParametersResponse> HandleAsync(GetContractsByParameters query)
        {
            return await _identityServerService.GetContractsByParametersAsync(new Objects.Requests.IdentityGetContractsByParametersRequest
            {
                AppType = query.AppType,
                ContractType = query.ContractType,
                Language = query.Language,
            }).ConfigureAwait(false);
        }
    }
}
