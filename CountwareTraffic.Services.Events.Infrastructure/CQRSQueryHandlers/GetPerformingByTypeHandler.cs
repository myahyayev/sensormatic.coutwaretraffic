﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetPerformingByTypeHandler : IQueryHandler<GetPerformingByType, List<PerformingByTypeDto>>
    {
        private readonly IEventElasticSearchRepository _eventElasticSearchRepository;
        private readonly ILogger<GetPerformingByTypeHandler> _logger;
        public GetPerformingByTypeHandler(IEventElasticSearchRepository eventElasticSearchRepository, ILogger<GetPerformingByTypeHandler> logger)
        {
            _eventElasticSearchRepository = eventElasticSearchRepository;
            _logger = logger;
        }

        public async Task<List<PerformingByTypeDto>> HandleAsync(GetPerformingByType query)
            => await _eventElasticSearchRepository.GetPerformingByTypeAsync(query.NodeFilters, query.StartDate, query.EndDate, query.Direction, query.PerformType, query.Size);
    }
}
