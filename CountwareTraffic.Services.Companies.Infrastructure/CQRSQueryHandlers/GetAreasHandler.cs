﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using Sensormatic.Tool.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetAreasHandler : IQueryHandler<GetAreas, PagingResult<AreaDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetAreasHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<PagingResult<AreaDetailsDto>> HandleAsync(GetAreas query)
        {
            int page = query.PagingQuery.Page;
            int limit = query.PagingQuery.Limit;

            var qres = await _unitOfWork.GetRepository<IAreaRepository>()
                                        .GetAllAsync(query.RegionId, query.PagingQuery, query.Filters, query.Sorts);

            if (qres == null)
                return PagingResult<AreaDetailsDto>.Empty;

            var region = await _unitOfWork.GetRepository<IRegionRepository>().GetByIdAsync(query.RegionId);

            var country = await _unitOfWork.GetRepository<ICountryRepository>().GetByIdAsync(region.CountryId);

            return new PagingResult<AreaDetailsDto>(qres.Entities.Select(area => new AreaDetailsDto
            {
                Id = area.Id,
                AreaTypeName = area.AreaType.Name,
                AreaTypeId = area.AreaType.Id,
                Name = area.Name,
                CityId = area.CityId,
                RegionId = area.RegionId,
                Description = area.Description,
                DistrictId = area.DistrictId,
                AuditCreateBy = area.AuditCreateBy,
                AuditCreateDate = area.AuditCreateDate,
                AuditModifiedBy = area.AuditModifiedBy,
                AuditModifiedDate = area.AuditModifiedDate,
                EmailAddress = area.Contact?.EmailAddress,
                GsmNumber = area.Contact?.GsmNumber,
                PhoneNumber = area.Contact?.PhoneNumber,
                GsmCountryCode = area.Contact?.GsmCountryCode,
                GsmDialCode = area.Contact?.GsmDialCode,
                ManagerId = area.ManagerId,
                ManagerName = area.ManagerName,
                PhoneCountryCode = area.Contact?.PhoneCountryCode,
                PhoneDialCode = area.Contact?.PhoneDialCode,
                Latitude = area.Address?.Location?.Y,
                Longitude = area.Address?.Location?.X,
                Street = area.Address?.Street,
                CountryLookupId = country.CountryLookupId,
                WorkingHoursStart = area.WorkingHoursStart,
                WorkingHoursEnd = area.WorkingHoursEnd,
                WorkingTimeZone = area.WorkingTimeZone
            }), qres.Total, page, limit, qres.Total > (limit * (page - 1)) + qres.Entities.Count);
        }
    }
}
