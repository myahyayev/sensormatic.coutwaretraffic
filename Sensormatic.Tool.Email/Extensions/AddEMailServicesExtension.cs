﻿
using Microsoft.Extensions.DependencyInjection;


namespace Sensormatic.Tool.Email
{
    public static class AddEMailServicesExtension
    {
        public static void AddGraphMailService(this IServiceCollection services)
        {
            services.AddSingleton<IGraphService, GraphService>();
            services.AddScoped<IEmailService, GraphEMailService>();
        }

        public static void AddSMtpMailService(this IServiceCollection services)
        {
            services.AddScoped<IEmailService, SMTPEmailService>();
        }

    }
}
