﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Events.Infrastructure.Migrations
{
    public partial class EventsIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF NOT EXISTS (SELECT * FROM sys.indexes  WHERE name = 'NC_IX_EventDate_AuditIsDeleted' AND object_id = OBJECT_ID('[events].[Events]')) 
                CREATE NONCLUSTERED INDEX [NC_IX_EventDate_AuditIsDeleted] ON [events].[Events]([EventDate] ASC, [AuditIsDeleted] ASC) INCLUDE([DeviceId],[DirectionTypeId],[TenantId])");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP INDEX [NC_IX_EventDate_AuditIsDeleted] ON [events].[Events]");
        }
    }
}
