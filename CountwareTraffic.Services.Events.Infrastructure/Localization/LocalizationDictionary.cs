﻿using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Infrastructure.Localization
{
    public static class LocalizationDictionary
    {
        public static Dictionary<string, Dictionary<string, string>> Localizations => values;

        private static readonly Dictionary<string, Dictionary<string, string>> values =
            new()
            {
                {
                    "tr-TR",
                    new Dictionary<string, string>
                    {
                        {"12 AM", "00"},
                        {"01 AM", "01"},
                        {"02 AM", "02"},
                        {"03 AM", "03"},
                        {"04 AM", "04"},
                        {"05 AM", "05"},
                        {"06 AM", "06"},
                        {"07 AM", "07"},
                        {"08 AM", "08"},
                        {"09 AM", "09"},
                        {"10 AM", "10"},
                        {"11 AM", "11"},
                        {"12 PM", "12"},
                        {"01 PM", "13"},
                        {"02 PM", "14"},
                        {"03 PM", "15"},
                        {"04 PM", "16"},
                        {"05 PM", "17"},
                        {"06 PM", "18"},
                        {"07 PM", "19"},
                        {"08 PM", "20"},
                        {"09 PM", "21"},
                        {"10 PM", "22"},
                        {"11 PM", "23"},

                        {"January", "Ocak"},
                        {"February", "Şubat"},
                        {"March", "Mart"},
                        {"April", "Nisan"},
                        {"May", "Mayıs"},
                        {"June", "Haziran"},
                        {"July", "Temmuz"},
                        {"August", "Ağustos"},
                        {"September", "Eylül"},
                        {"October", "Ekim"},
                        {"November", "Kasım"},
                        {"December", "Aralık"},

                        {"Monday", "Pazartesi"},
                        {"Tuesday", "Salı"},
                        {"Wednesday", "Çarşamba"},
                        {"Thursday", "Perşembe"},
                        {"Friday", "Cuma"},
                        {"Saturday", "Cumartesi"},
                        {"Sunday", "Pazar"},
                    }
                },
                {
                    "en-GB",
                    new Dictionary<string, string>
                    {
                        {"12 AM", "12 AM"},
                        {"01 AM", "01 AM"},
                        {"02 AM", "02 AM"},
                        {"03 AM", "03 AM"},
                        {"04 AM", "04 AM"},
                        {"05 AM", "05 AM"},
                        {"06 AM", "06 AM"},
                        {"07 AM", "07 AM"},
                        {"08 AM", "08 AM"},
                        {"09 AM", "09 AM"},
                        {"10 AM", "10 AM"},
                        {"11 AM", "11 AM"},
                        {"12 PM", "12 PM"},
                        {"01 PM", "01 PM"},
                        {"02 PM", "02 PM"},
                        {"03 PM", "03 PM"},
                        {"04 PM", "04 PM"},
                        {"05 PM", "05 PM"},
                        {"06 PM", "06 PM"},
                        {"07 PM", "07 PM"},
                        {"08 PM", "08 PM"},
                        {"09 PM", "09 PM"},
                        {"10 PM", "10 PM"},
                        {"11 PM", "11 PM"},

                        {"January", "January"},
                        {"February", "February"},
                        {"March", "March"},
                        {"April", "April"},
                        {"May", "May"},
                        {"June", "June"},
                        {"July", "July"},
                        {"August", "August"},
                        {"September", "September"},
                        {"October", "October"},
                        {"November", "November"},
                        {"December", "December"},

                        {"Monday", "Monday"},
                        {"Tuesday", "Tuesday"},
                        {"Wednesday", "Wednesday"},
                        {"Thursday", "Thursday"},
                        {"Friday", "Friday"},
                        {"Saturday", "Saturday"},
                        {"Sunday", "Sunday"},
                    }
                }
            };
    }
}
