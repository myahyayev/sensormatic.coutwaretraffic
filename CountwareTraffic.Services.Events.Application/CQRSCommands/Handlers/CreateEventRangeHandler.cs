﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class CreateEventRangeHandler : ICommandHandler<CreateEventRange>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateEventRangeHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task HandleAsync(CreateEventRange command)
        {
            var eventRepository = _unitOfWork.GetRepository<IEventRepository>();
            var deviceRepository = _unitOfWork.GetRepository<IDeviceRepository>();

            var device = await deviceRepository.GetAsync(command.DeviceId);

            foreach (var eventItem in command.Items)
            {
                if (device == null) continue;

                var @event = Event.Create(eventItem.Description, eventItem.OnTimeIOCount, eventItem.EventDate.ToUniversalTime(), eventItem.EventStartDate.ToUniversalTime(), eventItem.EventEndDate.ToUniversalTime(), eventItem.DeviceId, eventItem.DirectionTypeId, eventItem.TenantId);
                //if add was success Eventdbprovider will call @event.CompleteCreation as OnDomainEventRaiseAfterSuccess and this event will publish to eventcreate queue, this queue will be consumed by CreateEventInElasticConsumer for feeding elastic

                //bool isThereAlready = eventRepository
                //    .GetQuery(eventItem.TenantId)
                //    .Where(e => e.EventStartDate == eventItem.EventStartDate)
                //    .Where(e => e.EventEndDate == eventItem.EventEndDate)
                //    .Where(e => e.DirectionType.Id == eventItem.DirectionTypeId)
                //    .Where(e => e.DeviceId == eventItem.DeviceId)
                //    .Any();

                //if (isThereAlready) continue;
                await eventRepository.AddAsync(@event);

                @event.CompleteCreation(device.Name, device.SubAreaId, device.SubAreaName, device.AreaId, device.AreaName, device.CityId, device.CityName, device.DistrictId, device.DistrictName, device.RegionId, device.RegionName, device.CountryId, device.CountryName, device.CompanyId, device.CompanyName);
            }
            await _unitOfWork.CommitAsync();
        }
    }
}
