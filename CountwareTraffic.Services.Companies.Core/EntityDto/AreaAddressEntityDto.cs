﻿namespace CountwareTraffic.Services.Companies.Core
{
    public class AreaAddressEntityDto
    {
        public string Street { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
