﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class UsersController : BaseController
    {
        private readonly UserService _userService;
        public UsersController(IServiceProvider provider, ILogger<UsersController> logger, IHttpContextAccessor contextAccessor, UserService userService)
            : base(provider, logger, contextAccessor.HttpContext)
            => _userService = userService;



        [AllowAnonymous]
        [HttpGet("tenants"), ServiceLog]
        public async Task<ApiResponse<IEnumerable<GetTenantDetails>>> GetTenantsAsync()
             => Response(await _userService.GetTenantsAsync(), ResultMessage.SuccessResultMessage);

        [AllowAnonymous]
        [HttpPost("token"), ServiceLog]
        public async Task<ApiResponse<TokenApiResponse>> GetTokenAsync([FromForm] TokenApiRequest request)  //username: admin  password: A1w2e3r4t5***
        {
            var tokenResponse = await _userService.GetTokenAsync(request);

            if (tokenResponse.LoginStatus == LoginStatus.FirstLogin)
            {
                return Response(tokenResponse, new ResultMessage()
                {
                    ResponseMessageType = ApiResponseMessageType.Warning,
                    Description = $"You must change your password as it is the first login.",
                    Title = $"You must change your password as it is the first login.",
                });
            }

            return Response(tokenResponse, ResultMessage.SuccessResultMessage);
        }


        [AllowAnonymous]
        [HttpPatch("{userName}/{token}/password-reset"), ServiceLog]
        public async Task<ApiResponse<ResetPasswordApiResponse>> ResetPasswordWithTokenAsync(string userName, string token, [FromForm] ResetPasswordWithTokenApiRequest request)
            => Response(await _userService.ResetPasswordWithTokenAsync(userName, token, request), ResultMessage.SuccessResultMessage);


        [HttpPatch("{userName}/password-reset"), ServiceLog]
        public async Task<ApiResponse<ResetPasswordApiResponse>> ResetPasswordAsync(string userName, [FromForm] ResetPasswordApiRequest request)
            => Response(await _userService.ResetPasswordAsync(userName, request), ResultMessage.SuccessResultMessage);
    }
}
