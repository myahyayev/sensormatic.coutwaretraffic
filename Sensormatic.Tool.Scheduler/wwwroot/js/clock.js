﻿function startTime() {
    let now = new Date();
    let utcString = now.toISOString().slice(11, 19) + " UTC";
    document.getElementById('clock').innerHTML = utcString;

    setTimeout(startTime, 1000);
}
