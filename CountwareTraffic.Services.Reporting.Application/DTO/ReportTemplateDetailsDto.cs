﻿using System.Collections;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportTemplateDetailsDto : ReportTemplateDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string AssemblyQualifiedName { get; set; }
        public IEnumerable<TemplateTypeDto> TemplateTypeDtos { get; set; }
    }
}
