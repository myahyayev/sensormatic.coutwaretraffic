﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public record LatestEventDto
    {
        public Guid DeviceId { get; set; }
        public int TotalEnterCount { get; set; }
        public int TotalExitCount { get; set; }
    }
}
