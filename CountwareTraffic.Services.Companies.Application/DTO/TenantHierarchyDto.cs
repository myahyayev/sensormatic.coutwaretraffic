﻿using CountwareTraffic.Services.Companies.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class TenantHierarchyDto 
    {
        public Parent Parent { get; set; }
    }

    public class Parent
    {
        public Guid Id { get; set; }
        public Data Data { get; set; }
        public List<Child> Children { get; set; }
    }

    public class Data
    {
        public string Name { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }

    public class Child
    {
        public Guid Id { get; set; }
        public Data Data { get; set; }
        public List<Child> Children { get; set; }
    }
}
