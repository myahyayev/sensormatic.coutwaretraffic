﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Grpc
{
    [Authorize]
    public class ReportService : Report.ReportBase
    {
        private readonly ILogger<ReportService> _logger;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        public ReportService(ILogger<ReportService> logger, ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }

        public override async Task<GetReportTemplatesResponse> GetReportTemplates(Empty request, ServerCallContext context)
        {
            var reportTemplates = await _queryDispatcher.QueryAsync(new GetReportTemplates { });

            GetReportTemplatesResponse grpcResponse = new();

            foreach (var item in reportTemplates)
            {
                ReportTemplate reportTemplate = new()
                {
                    AuditCreateBy = item.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(item.AuditCreateDate),
                    AuditModifiedBy = item.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(item.AuditModifiedDate),
                    Description = item.Description,
                    Id = item.Id.ToString(),
                    Name = item.Name,
                };

                item.TemplateTypeDtos.ToList().ForEach(u => reportTemplate.TemplateTypes.Add(new TemplateType
                {
                    Id = u.TypeId,
                    Name = u.Name
                }));

                grpcResponse.ReportTemplates.Add(reportTemplate);
            }

            return grpcResponse;
        }

        public override async Task<GetReportLookupsResponse> GetReportLookups(Empty request, ServerCallContext context)
        {
            var reportLookups = await _queryDispatcher.QueryAsync(new GetReportLookups { });

            GetReportLookupsResponse grpcResponse = new();

            reportLookups.ReportFormats.ForEach(u => grpcResponse.ReportFormats.Add(new EnumResponse
            {
                IsVisible = u.IsVisible,
                Key = u.Key,
                Name = u.Name
            }));

            reportLookups.ScheduleDeliveryTimes.ForEach(u => grpcResponse.ScheduleDeliveryTimes.Add(new EnumResponse
            {
                IsVisible = u.IsVisible,
                Key = u.Key,
                Name = u.Name
            }));

            reportLookups.ScheduleFrequencies.ForEach(u => grpcResponse.ScheduleFrequencies.Add(new EnumResponse
            {
                IsVisible = u.IsVisible,
                Key = u.Key,
                Name = u.Name
            }));

            reportLookups.ScheduleWeeklyRepeatOns.ForEach(u => grpcResponse.ScheduleWeeklyRepeatOns.Add(new EnumResponse
            {
                IsVisible = u.IsVisible,
                Key = u.Key,
                Name = u.Name
            }));

            return grpcResponse;
        }

        public override async Task<GetReportsResponse> GetReports(GetReportsRequest request, ServerCallContext context)
        {
            var pagingReports = await _queryDispatcher.QueryAsync(new GetReports()
            {
                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Reporting.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Reporting.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Reporting.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            GetReportsResponse response = new()
            {
                TotalCount = pagingReports.TotalCount,
                HasNextPage = pagingReports.HasNextPage,
                Page = pagingReports.Page,
                Limit = pagingReports.Limit,
                Next = pagingReports.Next,
                Prev = pagingReports.Prev
            };

            foreach (var report in pagingReports.Data)
            {
                ReportDetail reportDetail = new()
                {
                    Id = report.Id.ToString(),
                    Name = report.Name,
                    Description = report.Description,
                    IsActive = report.IsActive,
                    ReportFormatId = report.FormatId,
                    ReportFormatName = report.FormatName,
                    ReportStatusId = report.StatusId,
                    ReportStatusName = report.StatusName,
                    TemplateName = report.TemplateName,
                    TemplateTypeId = report.TemplateTypeId,
                    TemplateTypeName = report.TemplateTypeName,
                    TemplateId = report.TemplateId.ToString(),
                    FrequencyId = report.ScheduleFrequencyId,
                    FrequencyName = report.ScheduleFrequencyName
                };

                report.Locations.ForEach(u => reportDetail.Locations.Add(u));

                if (report.ReportFiles != null)
                    report.ReportFiles.ForEach(u => reportDetail.ReportFiles.Add(new ReportFile
                    {
                        Id = u.Id.ToString(),
                        CreatedDate = Timestamp.FromDateTimeOffset(u.CreatedDate),
                        FilePath = u.FilePath,
                        DateRangeStartDate = Timestamp.FromDateTimeOffset(u.DateRangeStartDate),
                        DateRangeEndDate = Timestamp.FromDateTimeOffset(u.DateRangeEndDate)
                    }));
                response.ReportDetails.Add(reportDetail);
            }
            return response;
        }

        public override async Task<GetReportByIdResponse> GetReportById(GetReportByIdRequest request, ServerCallContext context)
        {
            var report = await _queryDispatcher.QueryAsync(new GetReport() { ReportId = request._ReportId });

            GetReportByIdResponse grpcResponse = new()
            {
                Id = report.Id.ToString(),
                Name = report.Name,
                Description = report.Description,
                IsActive = report.IsActive,
                ReportFormatId = report.FormatId,
                ReportFormatName = report.FormatName,
                ReportStatusId = report.StatusId,
                ReportStatusName = report.StatusName,
                TemplateName = report.TemplateName,
                TemplateTypeId = report.TemplateTypeId,
                TemplateTypeName = report.TemplateTypeName,
                NotifyWhenComplete = report.NotifyWhenComplete,
                EmailTo = report.EmailTo,
                EmailBody = report.EmailBody,
                EmailSubject = report.EmailSubject,
                LanguageId = report.LanguageId.ToString(),
                LanguageName = report.LanguageName,
                ScheduleId = report.ScheduleId.ToString(),
                ScheduleFrequencyId = report.ScheduleFrequencyId,
                ScheduleFrequencyName = report.ScheduleFrequencyName,
                ScheduleDeliveryTimeId = report.ScheduleDeliveryTimeId,
                ScheduleDeliveryTimeName = report.ScheduleDeliveryTimeName,
                AtSetTime = report.AtSetTime,
                AtSetTimeZone = report.AtSetTimeZone,
                RepeatOn = report.RepeatOn,
                StartsOn = report.StartsOn.HasValue ? Timestamp.FromDateTimeOffset(report.StartsOn.Value) : null,
                EndsOn = report.EndsOn.HasValue ? Timestamp.FromDateTimeOffset(report.EndsOn.Value) : null,
                DateRangeEndDate = Timestamp.FromDateTimeOffset(report.DateRangeEndDate),
                DateRangeStartDate = Timestamp.FromDateTimeOffset(report.DateRangeStartDate),
                TemplateId = report.TemplateId.ToString(),
            };

            report.Locations.ForEach(u => grpcResponse.Locations.Add(u));


            if (report.ReportFiles != null)
                report.ReportFiles.ForEach(u => grpcResponse.ReportFiles.Add(new ReportFile
                {
                    Id = u.Id.ToString(),
                    CreatedDate = Timestamp.FromDateTimeOffset(u.CreatedDate),
                    FilePath = u.FilePath
                }));

            return grpcResponse;
        }

        public override async Task<GetReportLanguagesResponse> GetReportLanguages(Empty request, ServerCallContext context)
        {
            var reportLanguages = await _queryDispatcher.QueryAsync(new GetReportLanguages() { });

            GetReportLanguagesResponse grpcResponse = new();

            reportLanguages.ToList().ForEach(r => grpcResponse.ReportLanguages.Add(new ReportLanguage
            {
                Id = r.Id.ToString(),
                Name = r.Name,
                Description = r.Description
            }));

            return grpcResponse;
        }

        public override async Task<DeleteSuccessResponse> DeleteReport(DeleteReportRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new DeleteReport { ReportId = request._ReportId });
            return new DeleteSuccessResponse { Deleted = "Deleted" };
        }

        public override async Task<CreateSuccessResponse> CreateReport(CreateReportRequest request, ServerCallContext context)
        {
            CreateReport createReport = new()
            {
                Name = request.Name,
                TemplateId = new Guid(request.TemplateId),
                Description = request.Description,
                FormatId = request.FormatId,
                TemplateTypeId = request.TemplateTypeId,
                NotifyWhenComplete = request.NotifyWhenComplete,
                FrequencyId = request.FrequencyId,
                StartsOn = request.StartsOn?.ToDateTimeOffset().LocalDateTime,
                EndsOn = request.EndsOn?.ToDateTimeOffset().LocalDateTime,
                RepeatOn = request.RepeatOn,
                DeliveryTimeId = request.DeliveryTimeId,
                AtSetTime = request.AtSetTime,
                AtSetTimeZone = request.AtSetTimeZone,
                EmailTo = request.EmailTo,
                EmailSubject = request.EmailSubject,
                EmailBody = request.EmailBody,
                LanguageId = new Guid(request.LanguageId),
                Locations = request.Locations.Select(u => new Core.Location
                {
                    Id = new Guid(u.Id),
                    Name = u.Name,
                    LocationLevel = u.LocationLevel switch
                    {
                        LocationLevel.Default => Core.LocationLevel.Tenant,
                        LocationLevel.Tenant => Core.LocationLevel.Tenant,
                        LocationLevel.Company => Core.LocationLevel.Company,
                        LocationLevel.Country => Core.LocationLevel.Country,
                        LocationLevel.Region => Core.LocationLevel.Region,
                        LocationLevel.Area => Core.LocationLevel.Area,
                        LocationLevel.SubArea => Core.LocationLevel.SubArea,
                        LocationLevel.Device => Core.LocationLevel.Device,
                        _ => throw new NotImplementedException(),
                    }

                }).ToList(),
                DateRangeStartDate = request.DateRangeStartDate.ToDateTimeOffset().LocalDateTime,
                DateRangeEndDate = request.DateRangeEndDate.ToDateTimeOffset().LocalDateTime
            };

            await _commandDispatcher.SendAsync(createReport);
            return new CreateSuccessResponse { Created = "Created" };
        }

        public override async Task<GetReportTemplateViewResponse> GetReportTemplateView(GetReportTemplateViewRequest request, ServerCallContext context)
        {
            var view = await _queryDispatcher.QueryAsync(new GetReportTemplateView() { ReportTemplateId = request._ReportTemplateId });

            return new GetReportTemplateViewResponse
            {
                Id = view.Id.ToString(),
                FilePath = view.FilePath
            };
        }

        public override async Task<GetReportFilesResponse> GetReportFiles(GetReportFilesRequest request, ServerCallContext context)
        {
            var pagingReportFiles = await _queryDispatcher.QueryAsync(new GetReportFiles
            {
                ReportId = request._ReportId,
                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Reporting.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Reporting.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Reporting.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            GetReportFilesResponse response = new()
            {
                TotalCount = pagingReportFiles.TotalCount,
                HasNextPage = pagingReportFiles.HasNextPage,
                Page = pagingReportFiles.Page,
                Limit = pagingReportFiles.Limit,
                Next = pagingReportFiles.Next,
                Prev = pagingReportFiles.Prev
            };

            pagingReportFiles.Data.ToList().ForEach(reportFile => response.ReportFiles.Add(new ReportFile
            {
                CreatedDate = Timestamp.FromDateTimeOffset(reportFile.CreatedDate),
                FilePath = reportFile.FilePath,
                Id = reportFile.Id.ToString(),
                DateRangeStartDate = Timestamp.FromDateTimeOffset(reportFile.DateRangeStartDate),
                DateRangeEndDate = Timestamp.FromDateTimeOffset(reportFile.DateRangeEndDate)
            }));

            return response;
        }

        public override async Task<RetryReportGeneratingResponse> RetryReportGenerating(RetryReportGeneratingRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new RetryReportGeneration
            {
                ReportId = request._ReportId,
                ReportFileId = request._ReportFileId,
                DateRangeEndDate = request.DateRangeEndDate?.ToDateTimeOffset().LocalDateTime,
                DateRangeStartDate = request.DateRangeStartDate?.ToDateTimeOffset().LocalDateTime,
            });

            return new RetryReportGeneratingResponse { IsSuccess = true };
        }
    }
}
