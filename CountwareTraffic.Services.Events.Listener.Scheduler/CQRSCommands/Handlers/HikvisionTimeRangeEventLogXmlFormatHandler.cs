﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using static Grpc.Core.Metadata;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class HikvisionTimeRangeEventLogXmlFormatHandler : EventLogXmlFormatHandlerBase<HikvisionTimeRangeEventLogXmlFormatHandler>, ICommandHandler<HikvisionTimeRangeEventLogXmlFormat>
    {


        public HikvisionTimeRangeEventLogXmlFormatHandler(ILogger<HikvisionTimeRangeEventLogXmlFormatHandler> logger, IOptions<TimeZoneSettings> timeZoneSettings, IIdentityService identityService, IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher, ICache redisCacheService, IQueueService queueService, IDeviceConnectionStatusProcessorService deviceConnectionStatusProcessorService) : base(logger, timeZoneSettings, identityService, queryDispatcher, commandDispatcher, redisCacheService, queueService, deviceConnectionStatusProcessorService)
        {

        }
        public async Task HandleAsync(HikvisionTimeRangeEventLogXmlFormat eventLog)
        {
            var xml = HttpUtility.UrlDecode(eventLog.Xml);

            if (xml.IsNullOrWhiteSpace())
            {
                _logger.LogError("xml file is empty!");
                return;
            }


            var objectFromXml = ParseDeviceEventXml(xml);

            if (objectFromXml != null)
            {
                var currentDevice =
                    await _queryDispatcher.QueryAsync(new GetDeviceByMac() { MacAddress = objectFromXml.MacAddress });

                await IsDeviceValid(currentDevice, objectFromXml);

                await ProcessDeviceConnectionStatus(currentDevice);

                DateTime expiryTime = DateTime.Today.AddHours(23).AddMinutes(59);
                TimeSpan timeRemaining = expiryTime - DateTime.Now;

                //skip duplicate device events
                var redisKey = string.Format(CacheKeys.DeviceTimeRangeEventSet, currentDevice.Id);
                if (objectFromXml.Events != null && objectFromXml.Events.Count > 0)
                {

                    foreach (var deviceEvent in objectFromXml.Events)
                    {
                        if (deviceEvent.ExitCount == 0 && deviceEvent.EnterCount == 0)
                            continue;

                        if (!IsEventAreaWorkingTimeZoneValid(currentDevice, deviceEvent.StartDate, deviceEvent.EndDate))
                            continue;

                        var redisValue = string.Format("{0}_{1}", deviceEvent.StartDate.ToString("MM_dd_yyyy_HH_mm_ss"), deviceEvent.EndDate.ToString("MM_dd_yyyy_HH_mm_ss"));

                        if (!await _redisCacheService.AddSetAsync(redisKey, redisValue, null, timeRemaining))
                        {
                            continue;
                        }

                        var splittedEventsList = new List<DeviceEventsListener>();

                        //initial default values for event
                        DeviceEventsListener tmp = new DeviceEventsListener
                        {
                            DeviceId = currentDevice.Id,
                            TotalEnterCount = deviceEvent.EnterCount,
                            TotalExitCount = deviceEvent.ExitCount,
                            UserId = _identityService.UserId,
                            EventDate = deviceEvent.EventDate,
                            EventStartDate = deviceEvent.StartDate,
                            EventEndDate = deviceEvent.EndDate,
                            DeviceName = currentDevice.Name,
                            DirectionTypeId = DirectionType.Unknown.Id,
                            TenantId = currentDevice.TenantId,
                        };

                        //make Outward event
                        if (deviceEvent.ExitCount > 0)
                        {

                            splittedEventsList
                                .Add(tmp with
                                {
                                    OnTimeIOCount = deviceEvent.ExitCount,
                                    RecordId = Guid.NewGuid(),
                                    DirectionTypeId = DirectionType.Outward.Id,
                                    Description = $"TimeRange Event = DeviceName: {currentDevice.Name}",
                                }
                                    );

                        }

                        //make Inward event
                        if (deviceEvent.EnterCount > 0)
                        {

                            splittedEventsList
                                .Add(tmp with
                                {
                                    OnTimeIOCount = deviceEvent.EnterCount,
                                    RecordId = Guid.NewGuid(),
                                    DirectionTypeId = DirectionType.Inwards.Id,
                                    Description = $"TimeRange Event = DeviceName: {currentDevice.Name}",
                                }
                                    );
                        }





                        if (splittedEventsList.Any())
                        {
                            foreach (var currentSplittedEvent in splittedEventsList)
                            {
                                await _commandDispatcher.SendAsync(currentSplittedEvent);
                            }
                        }


                    }
                }


            }
            else
            {
                _logger.LogError("xml format is not valid!");
                throw new Exception("xml format is not valid!");
            }
        }

        public override DeviceDto ParseDeviceEventXml(string xml)
        {
            DeviceDto deviceEventInfo = null;

            XNamespace xmlns = string.Empty;
            XNamespace psiallianceXmlns = "urn:psialliance-org";
            XNamespace isapiXmlns = "http://www.isapi.org/ver20/XMLSchema";

            if (xml.Contains(psiallianceXmlns.NamespaceName))
            {
                xmlns = psiallianceXmlns;
            }
            else if (xml.Contains(isapiXmlns.NamespaceName))
            {
                xmlns = isapiXmlns;
            }
            else
            {
                throw new Exception("xml namespace is not defined!");
            }

            var xmlDocuemnt = XDocument.Parse(xml.Trim());

            var metrics = xmlDocuemnt.Descendants(xmlns + "EventNotificationAlert");

            if (metrics != null && metrics.Count() > 0)
            {
                var metric = metrics.First();

                var staticticMethod = metric.Descendants(xmlns + "statisticalMethods").FirstOrDefault()?.Value;

                deviceEventInfo = new DeviceDto()
                {
                    MacAddress = metric.Descendants(xmlns + "macAddress").FirstOrDefault()?.Value,
                    IpAddress = metric.Descendants(xmlns + "ipAddress").FirstOrDefault()?.Value,
                    SerialNumber = "",
                    HttpPort = metric.Descendants(xmlns + "portNo").FirstOrDefault()?.Value ?? "80",
                    HttpsPort = "443",
                    DeviceName = metric.Descendants(xmlns + "channelName").FirstOrDefault()?.Value,
                    DeviceId = "",
                    BrandName = "Hikvision"
                };

                foreach (XElement reportData in metric.Descendants(xmlns + "peopleCounting"))
                {
                    int enter = Convert.ToInt32(reportData.Descendants(xmlns + "enter").FirstOrDefault().Value);
                    int exit = Convert.ToInt32(reportData.Descendants(xmlns + "exit").FirstOrDefault().Value);


                    if (staticticMethod == "timeRange")
                    {
                        var TimeRange = reportData.Descendants(xmlns + "TimeRange").FirstOrDefault();

                        DateTime startTime = DateTimeOffset.Parse(TimeRange.Descendants(xmlns + "startTime").FirstOrDefault().Value.Replace(" ", "+")).ToUniversalTime().DateTime;

                        DateTime endTime = DateTimeOffset.Parse(TimeRange.Descendants(xmlns + "endTime").FirstOrDefault().Value.Replace(" ", "+")).ToUniversalTime().DateTime;


                        deviceEventInfo.Events.Add(new EventDto
                        {
                            EventDate = startTime,
                            StartDate = startTime,
                            EndDate = endTime,
                            EnterCount = enter,
                            ExitCount = exit,
                        });

                    }

                }
            }
            return deviceEventInfo;
        }

    }
}


