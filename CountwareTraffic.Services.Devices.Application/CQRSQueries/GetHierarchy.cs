﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class GetDeviceHierarchy : IQuery<List<HierarchyDto>>
    {
        public Guid Id { get; set; }
    }

    public class GetAllHierarchy : IQuery<string>
    {
    
    }
}
