﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class UpdateRegionRequest : SensormaticRequestValidate
    {
        internal Guid _RegionId
        {
            get
            {
                if (Guid.TryParse(regionId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.regionId_ = value.ToString(); }
        }

        internal Guid? _ManagerId
        {
            get
            {
                if (Guid.TryParse(managerId_, out Guid id))
                    return id;

                return null;
            }
            set { this.managerId_ = value.ToString(); }
        }

        internal Guid _CountryId
        {
            get
            {
                if (Guid.TryParse(countryId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.countryId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_CountryId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1380 CountryId cannot be null", nameof(_CountryId)));

            if (_RegionId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1381 RegionId cannot be null", nameof(_RegionId)));

            if (String.IsNullOrEmpty(this.Name))
                ValidateResults.Add(new ErrorResult($"#E1382 Name Value cannot be null", nameof(Name)));

            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1383 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1384 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1385 Description invalid format", nameof(Description)));

            if (!ManagerName.IsNullOrWhiteSpace() && ManagerName.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1386 ManagerName invalid format", nameof(ManagerName)));
        }
    }
}