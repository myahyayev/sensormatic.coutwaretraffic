﻿
namespace CountwareTraffic.Services.Users.Application.Objects.Responses
{
    public class IdentityCreateContractResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
