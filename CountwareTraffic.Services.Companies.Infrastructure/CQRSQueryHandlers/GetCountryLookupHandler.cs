﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetCountryLookupHandler : IQueryHandler<GetCountryLookup, CountryLookupDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetCountryLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<CountryLookupDetailsDto> HandleAsync(GetCountryLookup query)
        {
            var countryLookup = await _unitOfWork.GetRepository<ICountryLookupRepository>().GetByIdAsync(query.CountryId);

            if (countryLookup == null)
                throw new CountryLookupNotFoundException(query.CountryId);

            return new CountryLookupDetailsDto
            {
                Id = countryLookup.Id,
                Name = countryLookup.Name,
                Capital = countryLookup.Capital,
                ContinentCode = countryLookup.ContinentCode,
                CurrencyCode = countryLookup.CurrencyCode,
                Iso = countryLookup.Iso,
                Iso3 = countryLookup.Iso3,
                IsoNumeric = countryLookup.IsoNumeric
            };
        }
    }
}
