﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    public class DeviceEventsListenerConsumer : IConsumer<Sensormatic.Tool.QueueModel.DeviceEventsListener>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;
        public DeviceEventsListenerConsumer(ICommandDispatcher commandDispatcher) => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.DeviceEventsListener queueCommand)
        {
            CreateEvent createEvent = new()
            {
                Description = queueCommand.Description,
                DeviceId = queueCommand.DeviceId,
                DirectionTypeId = queueCommand.DirectionTypeId,
                EventDate = queueCommand.EventDate,
                EventStartDate=queueCommand.EventStartDate,
                EventEndDate =queueCommand.EventEndDate,
                TenantId = queueCommand.TenantId,
                OnTimeIOCount = queueCommand.OnTimeIOCount,
            };

            await _commandDispatcher.SendAsync(createEvent);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
