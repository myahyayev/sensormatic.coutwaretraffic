﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using Sensormatic.Tool.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetSubAreasHandler : IQueryHandler<GetSubAreas, PagingResult<SubAreaDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GetSubAreasHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<PagingResult<SubAreaDetailsDto>> HandleAsync(GetSubAreas query)
        {
            int page = query.PagingQuery.Page;
            int limit = query.PagingQuery.Limit;

            var qres = await _unitOfWork.GetRepository<ISubAreaRepository>()
                                        .GetAllAsync(query.AreaId, _currentTenant.Id, query.PagingQuery, query.Filters, query.Sorts);

            if (qres == null)
                return PagingResult<SubAreaDetailsDto>.Empty;

            return new PagingResult<SubAreaDetailsDto>(qres.Entities.Select(x => new SubAreaDetailsDto
            {
                Id = x.Id,
                Name = x.Name,
                AreaId = x.AreaId,
                AuditCreateBy = x.AuditCreateBy,
                AuditCreateDate = x.AuditCreateDate,
                AuditModifiedBy = x.AuditModifiedBy,
                AuditModifiedDate = x.AuditModifiedDate,
                Description = x.Description
            }), qres.Total, page, limit, qres.Total > (limit * (page - 1)) + qres.Entities.Count);
        }
    }
}
