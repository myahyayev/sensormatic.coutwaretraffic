﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetAreaKeyValueHandler : IQueryHandler<GetAreaKeyValue, List<KeyValueDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetAreaKeyValueHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<KeyValueDto>> HandleAsync(GetAreaKeyValue query)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var keyValues = await areaRepository.GetAllKeyValueAsync(query.ParentId);

            return keyValues.Select(u => new KeyValueDto
            {
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }
}


