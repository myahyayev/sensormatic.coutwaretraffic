﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreateTenant : IQuery<IdentityCreateTenantResponse>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ErpCode { get; set; }
        public string ErpGroupName { get; set; }
        public DateTime ErpStartDate { get; set; }
        public DateTime ErpEndDate { get; set; }
        public Guid TenantGroupId { get; set; }
    }
}
