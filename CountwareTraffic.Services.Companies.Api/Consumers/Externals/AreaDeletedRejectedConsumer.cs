﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class AreaDeletedRejectedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaDeletedRejected>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public AreaDeletedRejectedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaDeletedRejected queuEvent)
        {
            var areaDeletedRejected = _eventMapper.Map(queuEvent) as AreaDeletedRejected;
            await _eventDispatcher.PublishAsync(areaDeletedRejected);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
