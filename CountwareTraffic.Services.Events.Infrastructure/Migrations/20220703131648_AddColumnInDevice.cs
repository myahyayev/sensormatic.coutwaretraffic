﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Events.Infrastructure.Migrations
{
    public partial class AddColumnInDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IpAddress",
                schema: "events",
                table: "Devices",
                type: "nvarchar(15)",
                maxLength: 15,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                schema: "events",
                table: "Devices",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "MacAddress",
                schema: "events",
                table: "Devices",
                type: "nvarchar(17)",
                maxLength: 17,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IpAddress",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "IsActive",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "MacAddress",
                schema: "events",
                table: "Devices");
        }
    }
}
