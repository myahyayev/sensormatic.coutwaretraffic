﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application.Objects.Responses
{
    public class IdentityGetRoleAdminSelectResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<RoleAdminSelectItem> Data { get; set; }
    }

    public class RoleAdminSelectItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
