﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRReportGeneratigFailedConsumer : IConsumer<Sensormatic.Tool.QueueModel.ReportGeneratingFailed>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRReportGeneratigFailedConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.ReportGeneratingFailed queueEvent)
        {
            await _commandDispatcher.SendAsync(new ReportGeneratingFailed
            {
                ReportId = queueEvent.ReportId,
                ReportStatus = queueEvent.ReportStatus,
                TenantId = queueEvent.TenantId,
                FileId = queueEvent.FileId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
