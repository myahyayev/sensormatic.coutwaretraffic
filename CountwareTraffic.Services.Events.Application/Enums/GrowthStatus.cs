﻿namespace CountwareTraffic.Services.Events.Application
{
    public enum GrowthStatus : byte
    {
        Equality = 1,
        Increase = 2,
        Decrease = 3
    }
}
