﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    [Contract]
    public class ElasticHierarchyChanged : ICommand
    {
        public Guid TenantId { get; set; }
    }
}