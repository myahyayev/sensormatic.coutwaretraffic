﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    public class AreaDeletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaDeleted>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        private readonly ILogger<AreaDeletedConsumer> _logger;
        public AreaDeletedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper, ILogger<AreaDeletedConsumer> logger)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
            _logger = logger;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaDeleted queuEvent)
        {
            _logger.LogInformation($"Consumed deleted Area with id {queuEvent.AreaId} with correlationId {queuEvent.CorrelationId}");
            var areaDeleted = _eventMapper.Map(queuEvent) as AreaDeleted;
            await _eventDispatcher.PublishAsync(areaDeleted);
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
