﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class UpdateDeviceHandler : ICommandHandler<UpdateDevice>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public UpdateDeviceHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(UpdateDevice command)
        {
            var deviceRepository = _unitOfWork.GetRepository<IDeviceRepository>();

            var device = await deviceRepository.GetAsync(command.DeviceId, _currentTenant.Id);

            if (device is null)
                throw new DeviceNotFoundException(command.DeviceId);
            //check entity existence with new properties
            var updateControl = await deviceRepository.GetByIPAddressAndMacAsync(command.IpAddress,command.MacAddress);

            if (updateControl != default && updateControl.Id != command.DeviceId)
                throw new DeviceAlreadyExistsException(command.IpAddress,command.MacAddress);

            var brandName = (await _unitOfWork.GetRepository<IBrandLookupRepository>().GetByIdAsync(command.BrandId)).Name;
            var modelName = (await _unitOfWork.GetRepository<IBrandModelLookupRepository>().GetByIdAsync(command.ModelId)).Name;

            device.WhenChanged(command.Name, command.IsActive, command.Description, command.Firmware, command.ControlFrequency, command.BrandId, brandName, command.ModelId, modelName, command.Note, command.DeviceTypeId, new DeviceConnectionInfoEntityDto
            {
                Identity = command.Identity,
                IpAddress = command.IpAddress,
                MacAddress = command.MacAddress,
                Password = command.Password,
                Port = command.Port,
                UniqueId = command.UniqueId
            });

            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            var subArea = await subAreaRepository.GetAsync(device.SubAreaId);

            device.WhenHierarchyChanged(_currentTenant.Id, subArea.CompanyId, subArea.CountryId, subArea.RegionId, subArea.AreaId, subArea.Id, device.Id, device.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
