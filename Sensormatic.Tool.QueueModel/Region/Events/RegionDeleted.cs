﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record RegionDeleted : MessageEnvelope, IQueueEvent
    {
        public RegionDeleted(Guid regionId, string regionName, Guid countryId,  Guid companyId,  Guid recordId, Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            RegionId = regionId;
            RegionName = regionName;
            CountryId = countryId;
            CompanyId = companyId;
            RecordId = recordId;
        }

        public Guid RegionId { get; }
        public string RegionName { get; }
        public Guid CountryId { get; }
        public Guid CompanyId { get; }
        public Guid RecordId { get; init; }
    }
}
