﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportEntityDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
        public bool NotifyWhenComplete { get; set; }
        public bool IsActive { get; set; }
        public TemplateType TemplateType { get; set; }
        public ReportFormat Format { get; set; }
        public ReportStatus Status { get; set; }
        public ReportTemplateEntityDto ReportTemplate { get; set; }
        public string Parameters { get; set; }
        public ReportFileEntityDto ReportFile { get; set; }
        public ReportEmailEntityDto ReportEmail { get; set; }
        public ReportLanguageEntityDto ReportLanguage { get; set; }
        public Guid TenantId { get; set; }

        public List<Location> Location
        {
            get
            {
                var parameters = JsonConvert.DeserializeObject<ReportParameters>(Parameters);
                if (parameters is not null && parameters.Locations is not null)
                    return parameters.Locations;

                return null;
            }
        }

        public DateTime DateRangeStartDate
        {
            get
            {
                var parameters = JsonConvert.DeserializeObject<ReportParameters>(Parameters);

                if (parameters is not null)

                    return parameters.DateRangeStartDate;

                return DateTime.Now;
            }
        }

        public DateTime DateRangeEndDate
        {
            get
            {
                var parameters = JsonConvert.DeserializeObject<ReportParameters>(Parameters);

                if (parameters is not null)

                    return parameters.DateRangeEndDate;

                return DateTime.Now;
            }
        }
    }
}
