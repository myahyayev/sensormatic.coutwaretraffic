﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class TemplateType : Enumeration
    {
        public static TemplateType Unknown = new(1, nameof(Unknown));
        public static TemplateType Daily = new(2, nameof(Daily));
        public static TemplateType Weekly = new(3, nameof(Weekly));
        public static TemplateType Monthly = new(4, nameof(Monthly));

        public TemplateType(int id, string name) : base(id, name) { }

        public static IEnumerable<TemplateType> List() => new[] { Unknown, Daily, Weekly, Monthly };


        public static TemplateType FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
                throw new TemplateTypeNameNotFoundException(List(), name);

            return state;
        }

        public static TemplateType From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
                throw new TemplateTypeIdNotFoundException(List(), id);

            return state;
        }
    }
}
