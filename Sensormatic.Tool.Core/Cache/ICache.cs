﻿using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sensormatic.Tool.Core
{
    public interface ICache: ISingletonDependency
    {
        bool KeyExists(string key);
        Task<bool> KeyExistsAsync(string key);
        TimeSpan GetKeyTTL(string key);
        Task<TimeSpan> GetKeyTTLAsync(string key);
        T Get<T>(string key);
        Task<T> GetOrAddAsync<T>(string key, T value, TimeSpan? expireTime = null, bool @override = true);
        T GetOrAdd<T>(string key, T value, TimeSpan? expireTime = null, bool @override = true);
        Task<T> GetAsync<T>(string key);
        Task<bool> AddSetAsync<T>(string key, T value, Action callback, TimeSpan? expireTime = default);
        bool AddSet<T>(string key, T value, Action callback, TimeSpan? expireTime = default);
        bool Add<T>(string key, T value, TimeSpan? expireTime = null, bool @override = true);
        Task<bool> AddAsync<T>(string key, T value, TimeSpan? expireTime = null, bool @override = true);
        bool Delete(string key);
        Task<bool> DeleteAsync(string key);
        List<T> GetList<T>(string key, long start = 0, long stop = -1);
        Task<List<T>> GetListAsync<T>(string key, long start = 0, long stop = -1);
        T GetListItem<T>(string key, long index);
        Task<T> GetListItemAsync<T>(string key, long index);
        void AddList<T>(string key, T value);
        Task AddListAsync<T>(string key, T value);
        void AddRangeList<T>(string key, List<T> values);
        Task AddRangeListAsync<T>(string key, List<T> values);
        void DeleteListItem<T>(string key, T value);
        Task DeleteListItemAsync<T>(string key, T value);
        long IncrementString(string key, int value = 1);
        Task<long> IncrementStringAsync(string key, int value = 1);
        long DecrementString(string key, int value = 1);
        Task<long> DecrementStringAsync(string key, int value = 1);
        bool LockTake(string key, string value, TimeSpan expiryTime);
        Task<bool> LockTakeAsync(string key, string value, TimeSpan expiryTime);
        bool LockRelease(string key, string value);
        Task<bool> LockReleaseAsync(string key, string value);
        T GetConnection<T>();
    }
}