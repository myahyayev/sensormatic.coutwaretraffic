﻿using Sensormatic.Tool.Ioc;

namespace CountwareTraffic.Services.Companies.Core
{
    public interface ICountryLookupRepository : IRepository<CountryLookup>, IScopedDependency
    {
    }
}
