﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.Objects.Responses
{
    public class IdentityCheckContractsServiceResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<IdentityCheckContractsResponse> Data { get; set; }
    }
}
