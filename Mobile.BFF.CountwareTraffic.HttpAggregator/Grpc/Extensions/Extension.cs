﻿using Grpc.Core;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Grpc.Common;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc
{
    public static class Extension
    {
        public static void ThrowClientGrpcxception(this ErrorModel model)
        {
            int statusCode = (model.Code >= 100) ? model.Code : (int)StatusCodeConverter.ConvertToHttpStatusCode((StatusCode)model.Code);
            throw new ClientGrpcException(model.ErrorResults, statusCode, Enum.Parse<ResponseMessageType>(model.Type));
        }

        public static ClientGrpcException CreateClientGrpcException(this ErrorModel model)
        {
            int statusCode = (model.Code >= 100) ? model.Code : (int)StatusCodeConverter.ConvertToHttpStatusCode((StatusCode)model.Code);
            return new ClientGrpcException(model.ErrorResults, statusCode, Enum.Parse<ResponseMessageType>(model.Type));
        }
    }
}
