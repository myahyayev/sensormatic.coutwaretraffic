﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportLookupsDto
    {
        public List<EnumResponse> ReportFormats { get; set; }
        public List<EnumResponse> ScheduleFrequencies { get; set; }
        public List<EnumResponse> ScheduleWeeklyRepeatOns { get; set; }
        public List<EnumResponse> ScheduleDeliveryTimes { get; set; }
    }
}
