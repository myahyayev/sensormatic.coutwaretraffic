﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityPermissionSelectResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }

        public List<IdentityPermissionSelectData> Data { get; set; }
    }

    public class IdentityPermissionSelectData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
