﻿namespace CountwareTraffic.Services.Reporting.Engine
{
    public enum ReportFrequencyType : byte
    {
        Unknown = 1,
        OneTime = 2,
        Repeat = 3
    }
}
