﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetCustomerHandler : IQueryHandler<GetCompany, CompanyDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetCustomerHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<CompanyDetailsDto> HandleAsync(GetCompany query)
        {
            var company = await _unitOfWork.GetRepository<ICompanyRepository>()
                .GetAsync(query.CompanyId);

            if (company == null)
                throw new CompanyNotFoundException(query.CompanyId);

            return new CompanyDetailsDto
            {
                Id = company.Id,
                Name = company.Name,
                Description = company.Description,
                AuditCreateBy = company.AuditCreateBy,
                AuditCreateDate = company.AuditCreateDate,
                AuditModifiedBy = company.AuditModifiedBy,
                AuditModifiedDate = company.AuditModifiedDate,
                EmailAddress = company.Contact?.EmailAddress,
                EmailAddressSecondary = company.Contact?.EmailAddressSecondary,
                GsmDialCode = company.Contact?.GsmDialCode,
                GsmDialCodeSecondary = company.Contact?.GsmDialCodeSecondary,
                GsmNumberSecondary = company.Contact?.GsmNumberSecondary,
                PersonName = company.Contact?.PersonName,
                PersonNameSecondary = company.Contact?.PersonNameSecondary,
                PhoneDialCode = company.Contact?.PhoneDialCode,
                PhoneDialCodeSecondary = company.Contact?.PhoneDialCodeSecondary,
                PhoneNumberSecondary = company.Contact?.PhoneNumberSecondary,
                GsmCountryCode = company.Contact?.GsmCountryCode,
                GsmCountryCodeSecondary = company.Contact?.GsmCountryCodeSecondary,
                PhoneCountryCodeSecondary = company.Contact?.PhoneCountryCodeSecondary,
                PhoneCountryCode = company.Contact?.PhoneCountryCode,
                GsmNumber = company.Contact?.GsmNumber,
                PhoneNumber = company.Contact?.PhoneNumber,
                City = company.Address?.City,
                Latitude = company.Address?.Location?.Y,
                Longitude = company.Address?.Location?.X,
                Country = company.Address?.Country,
                State = company.Address?.State,
                Street = company.Address?.Street,
                ZipCode = company.Address?.ZipCode
            };
        }
    }
}
