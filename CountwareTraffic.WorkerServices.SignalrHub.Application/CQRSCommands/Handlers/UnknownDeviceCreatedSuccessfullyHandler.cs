﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class UnknownDeviceCreatedSuccessfullyHandler : ICommandHandler<UnknownDeviceCreatedSuccessfully>
    {
        private readonly IHubContext<UnknownDeviceNotificationsHub, IUnknownDeviceNotificationsHub> _hubContext;
        public UnknownDeviceCreatedSuccessfullyHandler(IHubContext<UnknownDeviceNotificationsHub, IUnknownDeviceNotificationsHub> hubContext)
            => _hubContext = hubContext;

        public async Task HandleAsync(UnknownDeviceCreatedSuccessfully command)
            => await _hubContext.Clients
                                        .All
                                        .UnknownDeviceCreatedSuccessfullyReceived(command);
    }
}
