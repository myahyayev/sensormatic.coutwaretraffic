﻿using Convey.CQRS.Commands;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class DeviceEventsMigrationListenerHandler : ICommandHandler<DeviceMigrationEventsListener>
    {
        private readonly IQueueService _queueService;
        public DeviceEventsMigrationListenerHandler(IQueueService queueService) => _queueService = queueService;

        public async Task HandleAsync(DeviceMigrationEventsListener command)
        {
            string queueName = string.Format(String.Format(Queues.CountwareTrafficEventsDeviceMigrationEventsListener, command.DeviceId));

            Sensormatic.Tool.QueueModel.DeviceEventsRangeListener deviceEventsRangeListener = new(Guid.Empty, String.Empty, command.TenantId);

            command.Items.ForEach(item =>
            {
                deviceEventsRangeListener.Items.Add(new Sensormatic.Tool.QueueModel.DeviceEventsRangeElement(item.Description, item.DeviceId, item.DirectionTypeId, item.OnTimeIOCount, item.EventDate, item.EventStartDate, item.EventEndDate, item.UserId, String.Empty, item.TenantId));
            });

            _queueService.Send(queueName, deviceEventsRangeListener);
        }
    }
}
