﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    [Contract]
    public class RetryReportGeneration : ICommand
    {
        public Guid ReportId { get; set; }
        public DateTime? DateRangeStartDate { get; set; }
        public DateTime? DateRangeEndDate { get; set; }
        public Guid? ReportFileId { get; set; }
    }
}
