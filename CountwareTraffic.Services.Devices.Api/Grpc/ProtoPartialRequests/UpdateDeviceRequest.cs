﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;
using System.Net;

namespace CountwareTraffic.Services.Devices.Grpc
{
    [ServiceLog]
    public sealed partial class UpdateDeviceRequest : SensormaticRequestValidate
    {
        internal Guid _DeviceId
        {
            get
            {
                if (Guid.TryParse(deviceId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { deviceId_ = value.ToString(); }
        }


        internal Guid _BrandId
        {
            get
            {
                if (Guid.TryParse(brandId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { brandId_ = value.ToString(); }
        }

        internal Guid _ModelId
        {
            get
            {
                if (Guid.TryParse(modelId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { modelId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_DeviceId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1513 DeviceId cannot be null", nameof(_DeviceId)));

            if (_ModelId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1514 ModelId cannot be null", nameof(_ModelId)));

            if (_BrandId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1515 BrandId cannot be null", nameof(_BrandId)));

            if (!Firmware.IsNullOrWhiteSpace() && Firmware.Length > 50)
                ValidateResults.Add(new ErrorResult($"#E1516 FirmWare invalid format", nameof(Firmware)));

            if (ControlFrequency < 60 || ControlFrequency > 1440)
                ValidateResults.Add(new ErrorResult($"#E1517 ControlFrequency with value: {ControlFrequency} wrong format", nameof(ControlFrequency)));

            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1518 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1519 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1520 Description invalid format", nameof(Description)));

            if (DeviceTypeId < 1)
                ValidateResults.Add(new ErrorResult($"#E1521 DeviceTypeId with id: {DeviceTypeId} invalid format", nameof(DeviceTypeId)));

            if (!IPAddress.TryParse(IpAddress, out IPAddress ipA))
                ValidateResults.Add(new ErrorResult($"#E1522 IpAddress with value: {IpAddress} wrong format", nameof(IpAddress)));

            if (MacAddress.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1523 MacAddress Value cannot be null", nameof(MacAddress)));

            if (!MacAddress.IsNullOrWhiteSpace() && MacAddress.Length > 17)
                ValidateResults.Add(new ErrorResult($"#E1524 MacAddress invalid format", nameof(MacAddress)));

            if (!Identity.IsNullOrWhiteSpace() && Identity.Length > 17)
                ValidateResults.Add(new ErrorResult($"#E1525 Identity invalid format", nameof(Identity)));

            if (!Password.IsNullOrWhiteSpace() && Password.Length > 16)
                ValidateResults.Add(new ErrorResult($"#E1526 Password invalid format", nameof(Password)));

            if (!UniqueId.IsNullOrWhiteSpace() && UniqueId.Length > 75)
                ValidateResults.Add(new ErrorResult($"#E1527 UniqueId invalid format", nameof(UniqueId)));

            if (Port < 1 || Port > 65535)
                ValidateResults.Add(new ErrorResult($"#E1528 Port with value: {Port} wrong format", nameof(Port)));
        }
    }
}