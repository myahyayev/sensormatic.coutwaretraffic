﻿
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Core
{
    public interface IRegionRepository : IRepository<Region>, IScopedDependency
    {
        Task<Region> GetAsync(Guid id);
        Task<bool> ExistsAsync(string name, Guid countryId);
        Task<Region> GetAsync(string name, Guid countryId);
        Task<QueryablePagingValue<Region>> GetAllAsync(Guid countryId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts);
        Task<bool> ExistsAsync(Guid id);
        Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName);
        Task<HierarchyCteData> GetHierarchyAsync(Guid id, Guid tenantId);
        Task<List<KeyValue>> GetAllKeyValueAsync(Guid parentId);
    }
}

