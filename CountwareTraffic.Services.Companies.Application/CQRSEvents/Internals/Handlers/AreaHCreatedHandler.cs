﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaHCreatedHandler : IEventHandler<AreaHCreated>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public AreaHCreatedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(AreaHCreated command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId
            };

            CountryElasticDto countryElasticDto = new()
            {
                Id = command.CountryId
            };

            RegionElasticDto regionElasticDto = new()
            {
                Id = command.RegionId
            };

            AreaElasticDto areaElasticDto = new()
            {
                Id = command.AreaId,
                Name = command.AreaName
            };

            await _tenantHierarchyElasticSearchRepository.AddAreaAsync(tenantElasticDto, companyElasticDto, countryElasticDto, regionElasticDto, areaElasticDto);
        }
    }
}

