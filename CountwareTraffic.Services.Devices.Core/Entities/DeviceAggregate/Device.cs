﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Devices.Core
{
    public class Device : AggregateRoot, IAuditable, IDateable, IDeletable, ITraceable, IMultiTenant
    {
        private string _name;
        private string _description;
        private Guid _subAreaId;
        private DeviceConnectionInfo _connectionInfo;
        private int _deviceStatusId;
        private int _deviceTypeId;
        private int _deviceCreationStatus;
        private string _firmWare;
        private Guid _brandId;
        private Guid _modelId;
        private string _brandName;
        private string _modelName;
        private string _note;
        private bool _isActive;
        private int _controlFrequency;

        public string Name => _name;
        public string Description => _description;
        public string FirmWare => _firmWare;
        public Guid BrandId => _brandId;
        public Guid ModelId => _modelId;
        public string BrandName => _brandName;
        public string ModelName => _modelName;
        public string Note => _note;
        public bool IsActive => _isActive;
        public int ControlFrequency => _controlFrequency;
        public Guid SubAreaId => _subAreaId;
        public DeviceConnectionInfo ConnectionInfo => _connectionInfo;
        public DeviceStatus DeviceStatus { get; private set; }
        public DeviceType DeviceType { get; private set; }
        public DeviceCreationStatus DeviceCreationStatus { get; private set; }
        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public bool AuditIsDeleted { get; set; }
        public Guid TenantId { get; set; }
        #endregion


        private Device() { }

        public static Device Create(string name, string description, string firmware, int controlFrequency, Guid brandId, string brandName, Guid modelId, string modelName, string note, Guid subAreaId, DeviceConnectionInfo connectionInfo, int deviceTypeId)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            if (name.Length > 150)
                throw new ArgumentException(nameof(name));

            if (string.IsNullOrWhiteSpace(modelName))
                throw new ArgumentNullException(nameof(modelName));

            if (string.IsNullOrWhiteSpace(brandName))
                throw new ArgumentNullException(nameof(brandName));

            if (brandId == Guid.Empty)
                throw new ArgumentNullException(nameof(brandId));

            if (modelId == Guid.Empty)
                throw new ArgumentNullException(nameof(modelId));

            if (!firmware.IsNullOrWhiteSpace() && firmware.Length > 50)
                throw new ArgumentException(nameof(firmware));

            if (!description.IsNullOrWhiteSpace() && description.Length > 2500)
                throw new ArgumentException(nameof(description));

            if (!DeviceType.TryParse(deviceTypeId, out DeviceType deviceType))
                throw new DeviceTypeIdNotFoundException(DeviceType.List(), deviceTypeId);

            if (controlFrequency < 70 || controlFrequency > 1440)
                throw new ArgumentException(nameof(controlFrequency));

            CheckRule(new DeviceMustHaveSubAreaRule(subAreaId));

            return new Device
            {
                _name = name,
                _firmWare = firmware,
                _controlFrequency = controlFrequency,
                _brandId = brandId,
                _brandName = brandName,
                _modelId = modelId,
                _modelName = modelName,
                _note = note,
                _isActive = true,
                _description = description,
                _subAreaId = subAreaId,
                _connectionInfo = connectionInfo,
                _deviceStatusId = DeviceStatus.Unknown.Id,
                _deviceTypeId = deviceTypeId,
                _deviceCreationStatus = DeviceCreationStatus.Created.Id
            };
        }

        public void WhenCreated(Guid deviceId, string name, string macAddress, string ipAddress, Guid subAreaId, string subAreaName, Guid areaId, string areaName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid countryLookupId, Guid cityId, string cityName, Guid districtId, string districtName, int controlFrequency)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            if (string.IsNullOrWhiteSpace(ipAddress))
                throw new ArgumentNullException(nameof(ipAddress));

            if (string.IsNullOrWhiteSpace(macAddress))
                throw new ArgumentNullException(nameof(macAddress));

            if (deviceId == Guid.Empty)
                throw new ArgumentNullException(nameof(deviceId));

            AddEvent(new DeviceCreated(deviceId, name, macAddress, ipAddress, subAreaId, subAreaName, areaId, areaName, regionId, regionName, countryId, countryName, companyId, companyName, countryLookupId, cityId, cityName, districtId, districtName, controlFrequency));
        }

        public void WhenChanged(string name, bool isActive, string description, string firmware, int controlFrequency, Guid brandId, string brandName, Guid modelId, string modelName, string note, int deviceTypeId, DeviceConnectionInfoEntityDto connectionInfoEntityDto)
        {
            bool hasEvent = false;
            string oldName = _name;

            if (name != _name || isActive != _isActive || controlFrequency != _controlFrequency)
            {
                AddEvent(new DeviceChanged(_id, name, _connectionInfo.MacAddress, _connectionInfo.IpAddress, isActive, _name, controlFrequency));
                hasEvent = true;
            }

            _deviceTypeId = deviceTypeId;
            _name = name;
            _description = description;
            _isActive = isActive;
            _firmWare = firmware;
            _controlFrequency = controlFrequency;
            _brandId = brandId;
            _brandName = brandName;
            _modelId = modelId;
            _modelName = modelName;
            _note = note;

            if (_connectionInfo == null)
                _connectionInfo = DeviceConnectionInfo.Create(connectionInfoEntityDto.IpAddress, connectionInfoEntityDto.Port, connectionInfoEntityDto.Identity, connectionInfoEntityDto.Password, connectionInfoEntityDto.UniqueId, connectionInfoEntityDto.MacAddress);
            else
                _connectionInfo.WhenChanged(connectionInfoEntityDto.IpAddress, connectionInfoEntityDto.Port, connectionInfoEntityDto.Identity, connectionInfoEntityDto.Password, connectionInfoEntityDto.UniqueId, connectionInfoEntityDto.MacAddress);

            if (!hasEvent)
                _connectionInfo.WhenChangedCompleted(_id, _name, oldName, _isActive, _connectionInfo.MacAddress, _connectionInfo.IpAddress, _controlFrequency);
        }

        public void WhenDeleted(Guid deviceId)
        {
            if (deviceId == Guid.Empty)
                throw new ArgumentNullException(nameof(deviceId));

            AddEvent(new DeviceDeleted(deviceId, _name));
        }

        public void WhenChangeStatus(int deviceStatusId)
        {
            if (!DeviceStatus.TryParse(deviceStatusId, out DeviceStatus deviceStatus))
                throw new DeviceStatusIdNotFoundException(DeviceStatus.List(), deviceStatusId);

            _deviceStatusId = deviceStatusId;
        }

        public void WhenDeviceConnected() => _deviceStatusId = DeviceStatus.Connected.Id;

        public void WhenDeviceDisConnected() => _deviceStatusId = DeviceStatus.DisConnected.Id;

        public void WhenDeviceConnecttionBroken() => _deviceStatusId = DeviceStatus.Broken.Id;

        public void WhenDeviceConnecttionUnknown() => _deviceStatusId = DeviceStatus.Unknown.Id;

        public void WhenCreatedCompleted() => _deviceCreationStatus = DeviceCreationStatus.Completed.Id;

        public void WhenCreatedRejected() => _deviceCreationStatus = DeviceCreationStatus.Rejected.Id;

        public void WhenChangedRejected(string oldName) => _name = oldName;

        public void WhenDeletedRejected() => AuditIsDeleted = false;

        public void WhenHierarchyCreated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, Guid deviceId, string deviceName) => AddEvent(new DeviceHierarchyCreated(tenantId, companyId, countryId, regionId, areaId, subAreaId, deviceId, deviceName));

        public void WhenHierarchyChanged(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, Guid deviceId, string deviceName) => AddEvent(new DeviceHierarchyUpdated(tenantId, companyId, countryId, regionId, areaId, subAreaId, deviceId, deviceName));

        public void WhenHierarchyDeleted(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, Guid deviceId, string deviceName) => AddEvent(new DeviceHierarchyDeleted(tenantId, companyId, countryId, regionId, areaId, subAreaId, deviceId, deviceName));
    }
}
