﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application
{
    public class RolePermissions : IQuery<IdentityCreateRolePermissionsResponse>
    {
        public List<Guid> PermissionIds { get; set; }
        public string RoleId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
