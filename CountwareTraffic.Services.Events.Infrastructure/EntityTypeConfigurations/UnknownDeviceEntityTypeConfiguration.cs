﻿using CountwareTraffic.Services.Events.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class UnknownDeviceEntityTypeConfiguration : IEntityTypeConfiguration<UnknownDevice>
    {
        public void Configure(EntityTypeBuilder<UnknownDevice> builder)
        {
            builder.ToTable("UnknownDevices", SchemaNames.Events);

            builder
                 .Property(x => x.Id)
                 .HasField("_id")
                 .UsePropertyAccessMode(PropertyAccessMode.Field)
                 .IsRequired();

            builder
                 .Property(x => x.Name)
                 .HasField("_name")
                 .UsePropertyAccessMode(PropertyAccessMode.Field)
                 .IsRequired()
                 .HasMaxLength(150);

            builder
                 .Property(x => x.IpAddress)
                 .HasField("_ipAddress")
                 .UsePropertyAccessMode(PropertyAccessMode.Field)
                 .IsRequired()
                 .HasMaxLength(15);

            builder
                .Property(x => x.MacAddress)
                .HasField("_macAddress")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired()
                .HasMaxLength(17);

            builder
                .Property(x => x.HttpPort)
                .HasField("_httpPort")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired();

            builder
                .Property(x => x.HttpsPort)
                .HasField("_httpsPort")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired();

            builder
                 .Property(x => x.SerialNumber)
                 .HasField("_serialNumber")
                 .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.OccurredOn)
               .HasField("_occurredOn")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired();

            builder
              .Property(x => x.IsProcessed)
              .HasField("_isProcessed")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .IsRequired();

            builder
                .Property(x => x.ProcessedDate)
                .HasField("_processedDate")
                .UsePropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}