﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Ioc;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Sensormatic.Tool.StorageService
{
    public class AzureStorageService : ISingletonSelfDependency
    {
        private readonly BlobContainerClient _blobContainer;
        public AzureStorageService(IConfiguration configuration, IOptions<AzureBlobContainerOptions> azureBlobContainerOptions)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _blobContainer = new BlobContainerClient(configuration.GetConnectionString("AzureBlob"), azureBlobContainerOptions.Value.Name);

            _blobContainer.CreateIfNotExists();
        }

        public async Task DeleteAsync(string fileUrl)
        {
            var blob = _blobContainer.GetBlobClient(fileUrl);

            await blob.DeleteIfExistsAsync();
        }

        public async Task<Stream> DownloadAsync(string fileUrl)
        {
            var blob = _blobContainer.GetBlobClient(fileUrl);

            var file = await blob.DownloadAsync();

            return file.Value.Content;
        }

        public async Task MoveAsync(string source, string destination)
        {
            var blob = _blobContainer.GetBlobClient(source);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            await blob.DownloadToAsync(destination);

            await DeleteAsync(source);
        }

        public async Task<string> UploadAsync(string base64String, string fileUrl, bool overrideFile = true)
        {
            base64String = base64String.Substring(base64String.IndexOf(',') + 1);

            byte[] bytes = Convert.FromBase64String(base64String);

            Stream stream = new MemoryStream(bytes);

            var blob = _blobContainer.GetBlobClient(fileUrl);

            await blob.UploadAsync(stream, overrideFile);

            return blob.Uri.AbsoluteUri;
        }

        public async Task<string> UploadAsync(IFormFile documentFile, string fileUrl, bool overrideFile = true)
        {
            var blob = _blobContainer.GetBlobClient(fileUrl);

            using var stream = documentFile.OpenReadStream();

            await blob.UploadAsync(stream, overrideFile);
            return blob.Uri.AbsoluteUri;
        }

        public async Task<string> UploadAsync(Stream stream, string fileUrl, bool overrideFile = true)
        {
            var blob = _blobContainer.GetBlobClient(fileUrl);

            stream.Position = 0;

            await blob.UploadAsync(stream, overrideFile);

            return blob.Uri.AbsoluteUri;
        }

        public string CreateFilePath(string fileUnique, DateTime createdDate, string fileName)
        {
            var extension = Path.GetExtension(fileName);

            if (string.IsNullOrEmpty(extension))
                throw new UnknownFileNameFormatException(fileName);

            return $"Report_{fileUnique}_{createdDate.ToString("dd/MM/yyyy")}{extension}";
        }
    }
}
