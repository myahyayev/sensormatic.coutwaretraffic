﻿using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;

namespace Sensormatic.Tool.QueueModel
{
    public record SendEmail : IQueueCommand
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsHtml { get; set; }
        public List<string> To { get; set; } = new List<string>();
        public List<string> Cc { get; set; } = new List<string>();
        public List<string> Bc { get; set; } = new List<string>();
        public List<Guid> UserIds { get; set; } = new List<Guid>();
        public Guid TenantId { get; set; }
        public Guid RecordId { get; init; }
    }
}
