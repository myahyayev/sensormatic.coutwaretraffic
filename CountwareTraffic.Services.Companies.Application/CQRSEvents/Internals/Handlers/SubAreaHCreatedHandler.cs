﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class SubAreaHCreatedHandler : IEventHandler<SubAreaHCreated>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public SubAreaHCreatedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(SubAreaHCreated command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId
            };

            CountryElasticDto countryElasticDto = new()
            {
                Id = command.CountryId
            };

            RegionElasticDto regionElasticDto = new()
            {
                Id = command.RegionId
            };

            AreaElasticDto areaElasticDto = new()
            {
                Id = command.AreaId
            };

            SubAreaElasticDto subAreaElasticDto = new()
            {
                Id = command.SubAreaId,
                Name = command.SubAreaName
            };

            await _tenantHierarchyElasticSearchRepository.AddSubAreaAsync(tenantElasticDto, companyElasticDto, countryElasticDto, regionElasticDto, areaElasticDto, subAreaElasticDto);
        }
    }
}

