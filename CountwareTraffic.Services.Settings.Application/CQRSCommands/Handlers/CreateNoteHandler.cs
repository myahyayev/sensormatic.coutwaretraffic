﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Settings.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Settings.Application
{
    public class CreateNoteHandler : ICommandHandler<CreateNote>
    {
        private readonly IUnitOfWork _unitOfWork;
        public CreateNoteHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task HandleAsync(CreateNote command)
        {
            var noteRepository = _unitOfWork.GetRepository<INoteRepository>();

            var note = Note.Create(command.Subject, command.Content, command.BeginDate, command.EndDate, command.IsAffectToReport, command.IsVisible);

            await noteRepository.AddAsync(note);

            note.WhenCreated();

            await _unitOfWork.CommitAsync();
        }
    }
}
