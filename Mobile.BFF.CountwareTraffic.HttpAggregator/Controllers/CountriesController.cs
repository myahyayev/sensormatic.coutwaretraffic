﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class CountriesController : BaseController
    {
        private readonly CountryService _countryService;
        private readonly RegionService _regionService;
        public CountriesController(IServiceProvider provider, ILogger<CountriesController> logger, IHttpContextAccessor contextAccessor, CountryService countryService, RegionService regionService)
            : base(provider, logger, contextAccessor)
        {
            _regionService = regionService;
            _countryService = countryService;
        }


        [HttpGet("{countryId}/detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCountryView, Permissions.CountryView })]
        public async Task<ApiResponse<GetCountryDetails>> GetCountryDetailAsync(Guid countryId)
            => Response(await _countryService.GetCountryByIdAsync(countryId), new ResultMessage("#S1137"));


        [HttpPost("{companyId}/details"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCountryList })]
        public async Task<ApiPagedResponse<GetCountryDetails>> GetCountryDetailsAsync(Guid companyId, [FromBody] DataSourceApiRequest paging)
            => await _countryService.GetCountriesAsync(companyId, paging);
        

        [HttpPost("{companyId}/add"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCountryAdd })]
        public async Task<ApiResponse<CrudApiResponse>> AddCountryAsync(Guid companyId, [FromForm] AddCountryApiRequest request)
        {
            await _countryService.AddCountryAsync(companyId, request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Create }, new ResultMessage("#S1138"));
        }


        [HttpPut("{countryId}/change"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCountryEdit })]
        public async Task<ApiResponse<CrudApiResponse>> ChangeCountryAsync(Guid countryId, [FromForm] ChangeCountryApiRequest request)
        {
            await _countryService.ChangeCountryAsync(countryId, request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Update }, new ResultMessage("#S1139"));
        }


        [HttpDelete("{countryId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminCountryDelete })]
        public async Task<ApiResponse<CrudApiResponse>> DeleteCountryAsync(Guid countryId)
        {
            var regionControl = await _regionService.GetRegionsAsync(countryId, new DataSourceApiRequest());

            if (regionControl.TotalCount > 0)
            {
                return Response(new CrudApiResponse { IsSuccess = false, OperationType = OperationType.Delete, Id = countryId }, new ResultMessage()
                {
                    CallToActionType = CallToActionType.GoToRegions,
                    Description = $"#W1028 A region has been defined in the Country with id {countryId}.",
                    Title = $"#W1028 Country with id {countryId} cannot be deleted."
                });
            }

            await _countryService.DeleteCountryAsync(countryId);

            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Delete, Id = countryId }, new ResultMessage("#S1140")
            {
                CallToActionType = CallToActionType.Ok,
                Description = $"#S1073 Country with {countryId} id has been deleted.",
                Title = $"#S1073 Country with {countryId} id has been deleted."
            });
        }
    }
}
