﻿using CountwareTraffic.Services.Settings.Core;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Settings.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly SettingsDbContext _context;

        public UnitOfWork(SettingsDbContext context, IServiceProvider serviceProvider)
        {
            _context = context;
            _serviceProvider = serviceProvider;
        }

        public T GetRepository<T>() where T : IRepository => _serviceProvider.GetService<T>();

        public int Commit()=> _context.SaveChanges();

        public async Task<int> CommitAsync() => await _context.SaveChangesAsync();


        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible
    }
}
