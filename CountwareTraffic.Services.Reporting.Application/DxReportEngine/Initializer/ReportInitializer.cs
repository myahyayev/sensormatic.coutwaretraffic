﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.StorageService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public class ReportInitializer : IScopedSelfDependency
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IOptions<TimeZoneSettings> _timeZoneSettings;
        private readonly AzureStorageService _azureStorageService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<ReportInitializer> _logger;
        public ReportInitializer(IServiceProvider serviceProvider, IOptions<TimeZoneSettings> timeZoneSettings, AzureStorageService azureStorageService, IUnitOfWork unitOfWork, ILogger<ReportInitializer> logger)
        {
            _serviceProvider = serviceProvider;
            _timeZoneSettings = timeZoneSettings;
            _azureStorageService = azureStorageService;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public static IDictionary<Type, Func<IServiceProvider, IOptions<TimeZoneSettings>, ReportBuilder>> Builders 
            => new Dictionary<Type, Func<IServiceProvider, IOptions<TimeZoneSettings>, ReportBuilder>>
        {
             { typeof(ITrafficReport), (serviceProvider, timeZoneSettings) => serviceProvider.GetRequiredService<TrafficReportBuilder>() },
             { typeof(ICompherensiveTrafficReport), (serviceProvider, timeZoneSettings) => serviceProvider.GetRequiredService<CompherensiveTrafficReportBuilder>() },
        };

        public async Task<MemoryStream> GenerateReportFileAsync(string assemblyQualifiedName, Guid reportId, string reportName, ReportPeriodType reportPeriodType, ReportExportype reportExportype, ReportFrequencyType reportFrequencyType, List<Location> reportLocations, DateTime reportStartDate, DateTime reportEndDate, Guid reportTenantId, string reportUserName, string reportEmailBody, string reportEmailTo, string reportEmailSubject, ReportLanguage language)
        {
            if (reportFrequencyType == ReportFrequencyType.Repeat)
            {
                (await _unitOfWork.GetRepository<IReportRepository>()
                    .GetByIdAsync(reportId))
                    .WhenReportStartToGenerate();

                await _unitOfWork.CommitAsync();
            }
            var builder =
                 await GenerateBuilder(assemblyQualifiedName)
                 .WithPeriodType(reportPeriodType)
                     .WithFrequencyType(reportFrequencyType)
                     .WithId(reportId)
                     .WithTenantId(reportTenantId)
                     .WithLanguage(language)
                     .WithParameters(reportLocations, reportStartDate, reportEndDate)
                     .WithName(reportName)
                     .WithExportFormat(reportExportype)
                     .WithCreator(reportUserName)
                     .WithEmailBody(reportEmailBody)
                     .WithEmailTo(reportEmailTo)
                     .WithEmailSubject(reportEmailSubject)
                     .WithLoadingDataSourceFromDataBaseAsync(); ;
            return await builder
                .Build()
                .ExportAsync();
        }

        public async Task GenerateReportFileAndSaveContainerAsync(string assemblyQualifiedName, Guid reportId, string reportName, ReportPeriodType reportPeriodType, ReportExportype reportExportype, ReportFrequencyType reportFrequencyType, List<Location> reportLocations, DateTime reportStartDate, DateTime reportEndDate, string reportRepeatOn, Guid reportTenantId, string reportUserName, string reportEmailBody, string reportEmailTo, string reportEmailSubject, ReportLanguage language)
        {
            var createdDate = DateTime.Now;

            if (reportFrequencyType == ReportFrequencyType.Repeat)
            {
                (await _unitOfWork.GetRepository<IReportRepository>()
                    .GetByIdAsync(reportId))
                    .WhenReportStartToGenerate();

                await _unitOfWork.CommitAsync();
            }
            try
            {
                var builder =
                     await GenerateBuilder(assemblyQualifiedName)
                     .WithPeriodType(reportPeriodType)
                     .WithFrequencyType(reportFrequencyType)
                     .WithId(reportId)
                     .WithTenantId(reportTenantId)
                     .WithLanguage(language)
                     .WithParameters(reportLocations, reportStartDate, reportEndDate)
                     .WithRepeatOn(reportRepeatOn)
                     .WithName(reportName)
                     .WithExportFormat(reportExportype)
                     .WithCreator(reportUserName)
                     .WithEmailBody(reportEmailBody)
                     .WithEmailTo(reportEmailTo)
                     .WithEmailSubject(reportEmailSubject)
                     .WithLoadingDataSourceFromDataBaseAsync();

                var report = builder.Build();

                var stream = await report.ExportAsync();

                string filePath = _azureStorageService
                    .CreateFilePath
                    (
                        fileUnique: reportId.ToString(),
                        createdDate: createdDate,
                        fileName: $"{reportId}.{reportExportype.ToString().ToLower()}"
                    );
               
                string storageFilePath = await _azureStorageService
                    .UploadAsync(stream, filePath, overrideFile: true);

                var reportFile = ReportFile.Create(reportId, createdDate, storageFilePath, reportStartDate.ToUniversalTime(), reportEndDate.ToUniversalTime());

                await _unitOfWork.GetRepository<IReportFileRepository>()
                    .AddAsync(reportFile);

                (
                    await _unitOfWork.GetRepository<IReportScheduleRepository>()
                     .GetByReportIdAsync(reportId)
                )
                .SetLastRunTime(DateTime.Now);


                (
                    await _unitOfWork.GetRepository<IReportRepository>()
                    .GetByIdAsync(reportId)
                )
                .WhenReportGeneratingCompleted(storageFilePath, reportFile.Id, (int)reportFrequencyType, report.ReportCreatedDate, reportExportype.ToString(), report.TitleValue, report.SubTitleValue, report.ReportDateRange, reportEmailTo, reportEmailBody, reportEmailSubject, language.ShortName, reportRepeatOn,false
                );


                await _unitOfWork.CommitAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"ReportId {{reportId}}", reportId);

                var reportFile = ReportFile.Create(reportId, createdDate, OperationStatus.Failed.ToString(), reportStartDate.ToUniversalTime(), reportEndDate.ToUniversalTime());

                await _unitOfWork.GetRepository<IReportFileRepository>()
                 .AddAsync(reportFile);

                (
                    await _unitOfWork.GetRepository<IReportRepository>()
                    .GetByIdAsync(reportId)
                )
                .WhenReportGeneratingFailed((int)reportFrequencyType, reportFile.Id, reportRepeatOn,false);

                await _unitOfWork.CommitAsync();
            }
        }

        public async Task RetryGenerateReportFileAndSaveContainerAsync(string assemblyQualifiedName, Guid reportId, string reportName, ReportPeriodType reportPeriodType, ReportExportype reportExportype, ReportFrequencyType reportFrequencyType, List<Location> reportLocations, DateTime reportStartDate, DateTime reportEndDate, string reportRepeatOn, Guid reportTenantId, string reportUserName, string reportEmailBody, string reportEmailTo, string reportEmailSubject, ReportLanguage language, Guid? reportFileId)
        {
            (
                await _unitOfWork.GetRepository<IReportRepository>()
                    .GetByIdAsync(reportId)
            )
            .WhenReportStartToGenerate();

            await _unitOfWork.CommitAsync();

            var fileId = Guid.Empty;

            try
            {
                var builder =
                     await GenerateBuilder(assemblyQualifiedName)
                     .WithPeriodType(reportPeriodType)
                     .WithFrequencyType(reportFrequencyType)
                     .WithId(reportId)
                     .WithTenantId(reportTenantId)
                     .WithLanguage(language)
                     .WithParameters(reportLocations, reportStartDate, reportEndDate)
                     .WithRepeatOn(reportRepeatOn)
                     .WithName(reportName)
                     .WithExportFormat(reportExportype)
                     .WithCreator(reportUserName)
                     .WithEmailBody(reportEmailBody)
                     .WithEmailTo(reportEmailTo)
                     .WithEmailSubject(reportEmailSubject)
                     .WithLoadingDataSourceFromDataBaseAsync();

                var report = builder.Build();

                

                var reportFileRepository = _unitOfWork.GetRepository<IReportFileRepository>();
                ReportFile reportFile = null;

                if (reportFileId.HasValue)
                    reportFile = await reportFileRepository.GetByIdAsync(reportFileId.Value);

                else
                    reportFile = await reportFileRepository.GetFirstByReportId(reportId);


                var stream = await report.ExportAsync();

                string filePath = _azureStorageService
                    .CreateFilePath
                    (
                        fileUnique: reportId.ToString(),
                        createdDate: reportFile.CreatedDate, //Ayni tarhide azure blob file olusturmasii ve failed ezmesi icin gelistirilmisdir.
                        fileName: $"{reportId}.{reportExportype.ToString().ToLower()}"
                    );

                string storageFilePath = await _azureStorageService
                    .UploadAsync(stream, filePath, overrideFile: true);
               
                
                fileId = reportFile.Id;
                reportFile.SetFilePath(storageFilePath);


                (
                    await _unitOfWork.GetRepository<IReportScheduleRepository>()
                     .GetByReportIdAsync(reportId)
                )
                .SetLastRunTime(DateTime.Now);


                (
                    await _unitOfWork.GetRepository<IReportRepository>()
                    .GetByIdAsync(reportId)
                )
                .WhenReportGeneratingCompleted(storageFilePath, fileId, (int)reportFrequencyType, report.ReportCreatedDate, reportExportype.ToString(), report.TitleValue, report.SubTitleValue, report.ReportDateRange, reportEmailTo, reportEmailBody, reportEmailSubject, language.ShortName, reportRepeatOn, wasRetry: true);


                await _unitOfWork.CommitAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"ReportId {{reportId}}", reportId);

                if (reportFileId.HasValue)
                    fileId = reportFileId.Value;
                else
                    fileId = (await _unitOfWork.GetRepository<IReportFileRepository>().GetFirstByReportId(reportId)).Id;

                (
                    await _unitOfWork.GetRepository<IReportRepository>()
                    .GetByIdAsync(reportId)
                )
                .WhenReportGeneratingFailed((int)reportFrequencyType, fileId, reportRepeatOn, wasRetry: true);

                await _unitOfWork.CommitAsync();
            }
        }

        private ReportBuilder GenerateBuilder(string assemblyQualifiedName)
        {
            Builders
                .TryGetValue(Type.GetType(assemblyQualifiedName), out Func<IServiceProvider, IOptions<TimeZoneSettings>, ReportBuilder> pickBuilder);

            var builder = pickBuilder(_serviceProvider, _timeZoneSettings);

            return builder;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
