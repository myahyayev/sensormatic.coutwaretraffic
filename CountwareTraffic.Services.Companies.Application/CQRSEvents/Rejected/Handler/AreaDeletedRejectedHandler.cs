﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaDeletedRejectedHandler : IEventHandler<AreaDeletedRejected>
    {
        private readonly IQueueService _queueService;
        private readonly IUnitOfWork _unitOfWork;
        public AreaDeletedRejectedHandler(IQueueService queueService, IUnitOfWork unitOfWork)
        {
            _queueService = queueService;
            _unitOfWork = unitOfWork;
        }


        public async Task HandleAsync(AreaDeletedRejected command)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var area = await areaRepository.GetAsync(command.AreaId, command.TenantId);

            area.WhenDeletedRejected();

            await _unitOfWork.CommitAsync();

            _queueService.Publish(new AreaDeletedFailed
            {
                RecordId = Guid.NewGuid(),
                AreaId = command.AreaId,
                UserId = command.UserId,
                UserName = String.Empty,
                TenantId = command.TenantId
            });
        }
    }
}
