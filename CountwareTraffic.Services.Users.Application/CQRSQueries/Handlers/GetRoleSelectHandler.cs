﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetRoleSelectHandler : IQueryHandler<GetRoleSelect, IdentityRoleSelectResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetRoleSelectHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityRoleSelectResponse> HandleAsync(GetRoleSelect query)
        {
            return await _identityServerService.GetRoleSelectAsync();
        }
    }
}
