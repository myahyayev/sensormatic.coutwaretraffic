﻿namespace Sensormatic.Tool.Core
{
    public class SortDescriptor
    {
        public string Field { get; set; }
        public Direction Direction { get; set; }
    }
    public enum Direction
    {
        [EnumMemberInfo("Artan", "Artan", "46969600-8916-4B96-9636-FC3F77B4BD84")]
        Asc = 1,

        [EnumMemberInfo("Azalan", "Azalan", "F3014953-6161-4AF2-9630-EDCE025AD3B8")]
        Desc = 2
    }
}
