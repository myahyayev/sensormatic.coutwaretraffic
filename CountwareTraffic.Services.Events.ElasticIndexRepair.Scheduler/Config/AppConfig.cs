﻿using Sensormatic.Tool.Ioc;

namespace CountwareTraffic.Services.Events.ElasticIndexRepair.Scheduler 
{
    public class AppConfig : IConfigurationOptions
    {
        public int StartDays { get; set; }
        public int BatchCount { get; set; }
    }
}
