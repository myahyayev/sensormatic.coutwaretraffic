﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ResetPasswordHandler : IQueryHandler<ResetPassword, IdentityResetPasswordResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public ResetPasswordHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<IdentityResetPasswordResponse> HandleAsync(ResetPassword query)
        {
            return await _identityServerService.ResetPasswordAsync(query.UserName);
        }
    }
}
