﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class RolePermissionsHandler : IQueryHandler<RolePermissions, IdentityCreateRolePermissionsResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public RolePermissionsHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityCreateRolePermissionsResponse> HandleAsync(RolePermissions query)
        {
            return await _identityServerService.RolePermissionsAsync(new IdentityCreateRolePermissionsRequest
            {
                PermissionIds = query.PermissionIds,
                RoleId = query.RoleId,
                StartDate = query.StartDate,
                EndDate = query.EndDate
            });
        }
    }
}
