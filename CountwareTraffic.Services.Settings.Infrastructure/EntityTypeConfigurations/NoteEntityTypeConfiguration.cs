﻿using CountwareTraffic.Services.Settings.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CountwareTraffic.Services.Settings.Infrastructure
{
    public class NoteEntityTypeConfiguration : IEntityTypeConfiguration<Note>
    {
        private Guid _tenantId;
        public NoteEntityTypeConfiguration(Guid tenantId) => _tenantId = tenantId;

        public void Configure(EntityTypeBuilder<Note> builder)
        {
            builder.ToTable("Notes", SchemaNames.Settings);

            builder.HasQueryFilter(a => a.TenantId == _tenantId);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);


            builder
               .Property(x => x.Subject)
               .HasField("_subject")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(250);

            builder
              .Property(x => x.BeginDate)
              .HasField("_beginDate")
              .UsePropertyAccessMode(PropertyAccessMode.Field);


            builder
             .Property(x => x.EndDate)
             .HasField("_endDate")
             .UsePropertyAccessMode(PropertyAccessMode.Field);


            builder
               .Property(x => x.Content)
               .HasField("_content")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(2500);


            builder
             .Property(x => x.IsAffectToReport)
             .HasField("_isAffectToReport")
             .UsePropertyAccessMode(PropertyAccessMode.Field);


            builder
            .Property(x => x.IsVisible)
            .HasField("_isVisible")
            .UsePropertyAccessMode(PropertyAccessMode.Field);

        }
    }
}
