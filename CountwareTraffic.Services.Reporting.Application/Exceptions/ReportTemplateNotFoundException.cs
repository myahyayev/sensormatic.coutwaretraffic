﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportTemplateNotFoundException : AppException
    {
        public Guid Id { get; }
        public ReportTemplateNotFoundException(Guid id)
            : base(new List<ErrorResult>() { new ErrorResult($"ReportTemplate with id: {id} was not found") }, 404, ResponseMessageType.Error)
        {
            Id = id;
        }
    }
}
