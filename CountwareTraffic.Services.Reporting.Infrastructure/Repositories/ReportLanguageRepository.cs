﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportLanguageRepository : Repository<ReportLanguage>, IReportLanguageRepository
    {
        private readonly new ReportDbContext _context;

        public ReportLanguageRepository(ReportDbContext context) : base(context) => _context = context;

        public async Task<IEnumerable<ReportLanguage>> GetAllReportLanguagesAsync() => await base.GetQuery(null).OrderBy(u => u.Name).ToListAsync();

        #region dispose
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion dispose
    }
}
