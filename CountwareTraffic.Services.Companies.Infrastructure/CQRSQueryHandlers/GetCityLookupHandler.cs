﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetCityLookupHandler : IQueryHandler<GetCityLookup, CityLookupDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetCityLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<CityLookupDetailsDto> HandleAsync(GetCityLookup query)
        {
            var cityLookup = await _unitOfWork.GetRepository<ICityLookupRepository>().GetByIdAsync(query.CityId);

            if (cityLookup == null)
                throw new CityLookupNotFoundException(query.CityId);

            return new CityLookupDetailsDto
            {
                Id = cityLookup.Id,
                Name = cityLookup.Name,
                CountryId = cityLookup.CountryId,
            };
        }
    }
}
