﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRUnknownDeviceCreatedSuccessfullyConsumer : IConsumer<Sensormatic.Tool.QueueModel.UnknownDeviceCreatedSuccessfully>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRUnknownDeviceCreatedSuccessfullyConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.UnknownDeviceCreatedSuccessfully queueCommand)
        {
            await _commandDispatcher.SendAsync(new UnknownDeviceCreatedSuccessfully
            {
                IpAddress = queueCommand.IpAddress,
                MacAddress = queueCommand.MacAddress,
                Name = queueCommand.Name,
                SerialNumber = queueCommand.SerialNumber,
                HttpPort = queueCommand.HttpPort,
                HttpsPort = queueCommand.HttpsPort
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
