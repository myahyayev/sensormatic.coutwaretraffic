﻿using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class Hierarchy
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }

    public class HierarchyCteData
    {
        public Guid TenantId { get; set; }
        public Guid CopmanyId { get; set; }
        public Guid CountryId { get; set; }
        public Guid RegionId { get; set; }
        public Guid AreaId { get; set; }
        public Guid SubAreaId { get; set; }
        public string TenantName { get; set; }
        public string CopmanyName { get; set; }
        public string CountryName { get; set; }
        public string RegionName { get; set; }
        public string AreaName { get; set; }
        public string SubAreaName { get; set; }

    }

    public enum HierarchyLevel
    {
        Tenant = 1,
        Company = 2,
        Country = 3,
        Region = 4,
        Area = 5,
        SubArea = 6,
        Device = 7,
    }
}
