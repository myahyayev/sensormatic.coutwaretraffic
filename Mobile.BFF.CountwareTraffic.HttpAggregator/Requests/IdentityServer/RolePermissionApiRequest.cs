﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class RolePermissionApiRequest
    {
        [FromForm(Name = "permissionId")]
        public Guid PermissionId { get; set; }

        [FromForm(Name = "roleId")]
        public string RoleId { get; set; }

        [FromForm(Name = "startDate")]
        public DateTime StartDate { get; set; }

        [FromForm(Name = "endDate")]
        public DateTime EndDate { get; set; }
    }
}
