﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{

    public class UpdateBrandLookupHandler : ICommandHandler<UpdateBrandLookup>
    {
        private readonly IUnitOfWork _unitOfWork;
        public UpdateBrandLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task HandleAsync(UpdateBrandLookup command)
        {
            var brandRepository = _unitOfWork.GetRepository<IBrandLookupRepository>();

            var brand = await brandRepository.GetByIdAsync(command.BrandId);

            if (brand is null)
                throw new BrandLookupNotFoundException(command.BrandId);

            var updateControl = await brandRepository.GetByNameAsync(command.Name);

            if (updateControl != null && updateControl.Id != command.BrandId)
                throw new BrandAlreadyExistsException(command.Name);

            brand.Change(command.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
