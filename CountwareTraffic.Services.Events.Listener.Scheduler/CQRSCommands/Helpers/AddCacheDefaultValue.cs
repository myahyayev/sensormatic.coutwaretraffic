﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Application;
using System;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    [Contract]
    public class AddCacheDefaultValue : ICommand
    {
        public Guid DeviceId { get; set; }
        public int TotalExitCount { get; set; }
        public int TotalEnterCount { get; set; }
    }
}