﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class AreaChangedFailedHandler : ICommandHandler<AreaChangedFailed>
    {
        private readonly IHubContext<AreaNotificationsHub, IAreaNotificationsHub> _hubContext;
        public AreaChangedFailedHandler(IHubContext<AreaNotificationsHub, IAreaNotificationsHub> hubContext)
            => _hubContext = hubContext;


        public async Task HandleAsync(AreaChangedFailed command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .AreaChangedFailedReceived(command);
    }
}
