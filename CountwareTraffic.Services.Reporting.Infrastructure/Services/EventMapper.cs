﻿using CountwareTraffic.Services.Reporting.Application;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class EventMapper : IEventMapper
    {
        public IEnumerable<Convey.CQRS.Events.IEvent> MapAll(IEnumerable<Sensormatic.Tool.Queue.IQueueEvent> events)
              => events.Select(Map);

        public Convey.CQRS.Events.IEvent Map(Sensormatic.Tool.Queue.IQueueEvent @event)
        {
            switch (@event)
            {
                case Sensormatic.Tool.QueueModel.OneTimeReportGenerate e:
                    return new ReportGenerate
                    {
                        ReportId = e.ReportId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CorrelationId = e.CorrelationId,
                        UserName = e.UserName
                    };
            }

            return null;
        }
    }
}
