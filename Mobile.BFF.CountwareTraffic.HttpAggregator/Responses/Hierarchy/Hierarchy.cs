﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class Hierarchy
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }
}
