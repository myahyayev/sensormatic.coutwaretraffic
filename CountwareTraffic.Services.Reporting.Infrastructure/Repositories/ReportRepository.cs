﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportRepository : Repository<Report>, IReportRepository
    {
        private readonly new ReportDbContext _context;
        private readonly ICurrentTenant _currentTenant;

        public ReportRepository(ReportDbContext context, ICurrentTenant currentTenant) : base(context)
        {
            _context = context;
            _currentTenant = currentTenant;
        }

        public async Task<QueryablePagingValue<ReportDetailEntityDto>> GetAllReports(PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts)
        {
            var query = base.GetQuery(_currentTenant.Id);

            query = query.ApplyFilter(filters.Where(u => u.Field.ToLower() != "isactive").ToList());

            foreach (var filter in filters)
            {
                if (filter.Field.ToLower() == "templatetypeid")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.TemplateType.Id == Int32.Parse(filter.Value)),
                        FilterEnum.Neq => query.Where(u => u.TemplateType.Id != Int32.Parse(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }

                if (filter.Field.ToLower() == "reportformatid")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.ReportFormat.Id == Int32.Parse(filter.Value)),
                        FilterEnum.Neq => query.Where(u => u.ReportFormat.Id != Int32.Parse(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }

                if (filter.Field.ToLower() == "reportstatusid")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.ReportStatus.Id == Int32.Parse(filter.Value)),
                        FilterEnum.Neq => query.Where(u => u.ReportStatus.Id != Int32.Parse(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }

                if (filter.Field.ToLower() == "isactive")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.IsActive == (filter.Value == "0") ? false : true),
                        FilterEnum.Neq => query.Where(u => u.IsActive != (filter.Value == "0") ? false : true),
                    };
                }
            }

            query = query.ApplyOrderBy(sorts);

            foreach (var sort in sorts)
            {
                if (sort.Field.ToLower() == "templatetypeid")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.TemplateType.Id),
                        Direction.Desc => query.OrderByDescending(u => u.TemplateType.Id),
                        _ => throw new NotImplementedException(),
                    };
                }
                if (sort.Field.ToLower() == "reportformatid")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.ReportFormat.Id),
                        Direction.Desc => query.OrderByDescending(u => u.ReportFormat.Id),
                        _ => throw new NotImplementedException(),
                    };
                }

                if (sort.Field.ToLower() == "reportstatusid")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.ReportStatus.Id),
                        Direction.Desc => query.OrderByDescending(u => u.ReportStatus.Id),
                        _ => throw new NotImplementedException(),
                    };
                }
            }

            var total = await query.CountAsync();

            if (total > 0)
            {
                var result = await query
                   .Include(i => i.TemplateType)
                   .Include(i => i.ReportStatus)
                   .Include(i => i.ReportFormat)
                   .Skip((paging.Page - 1) * paging.Limit)
                   .Take(paging.Limit)
                   .Select(s => new ReportDetailEntityDto
                   {
                       Id = s.Id,
                       Name = s.Name,
                       Description = s.Description,
                       IsActive = s.IsActive,
                       TenantId = s.TenantId,
                       Status = s.ReportStatus,
                       Format = s.ReportFormat,
                       Parameters = s.Parameters,
                       TemplateType = s.TemplateType,
                       ReportFile = (from reportFile in _context.Set<ReportFile>().OrderByDescending(U => U.CreatedDate)
                                     where reportFile.ReportId == s.Id
                                     select new ReportFileEntityDto
                                     {
                                         Id = reportFile.Id,
                                         CreatedDate = reportFile.CreatedDate,
                                         FilePath = reportFile.FilePath,
                                         DateRangeStartDate = reportFile.DateRangeStartDate,
                                         DateRangeEndDate = reportFile.DateRangeEndDate,

                                     }).FirstOrDefault(),
                       ReportTemplate = (from reportTemplate in _context.Set<ReportTemplate>()
                                         where reportTemplate.Id == s.TemplateId
                                         select new ReportTemplateEntityDto
                                         {
                                             Id = reportTemplate.Id,
                                             Name = reportTemplate.Name,
                                             AssemblyQualifiedName = reportTemplate.AssemblyQualifiedName
                                         }
                          ).FirstOrDefault(),
                       ReportSchedule = (from reportSchedule in _context.Set<ReportSchedule>()
                                        .Include(i => i.ScheduleFrequency)
                                         where reportSchedule.ReportId == s.Id && !reportSchedule.AuditIsDeleted
                                         select new ReportScheduleEntityDto
                                         {
                                             Frequency = reportSchedule.ScheduleFrequency,
                                         }).FirstOrDefault(),

                   }).ToListAsync();

                return new QueryablePagingValue<ReportDetailEntityDto>(result, total);
            }

            return null;
        }

        public async Task<ReportDetailEntityDto> GetReportByIdAsync(Guid reportId, Guid? tenantId = null, bool includeIsActive = true)
        {
            var query = base.GetQuery(tenantId == null? _currentTenant.Id : tenantId.Value);

            if (!includeIsActive)
                query = query.Where(u => u.IsActive == true); 

            return await  query
                .Include(i => i.TemplateType)
                .Include(i => i.ReportStatus)
                .Include(i => i.ReportFormat)
                .Include(i => i.Email)
                .Where(w => w.Id == reportId)
                .Select(s => new ReportDetailEntityDto
                {
                    Id = s.Id,
                    Name = s.Name,
                    Description = s.Description,
                    IsActive = s.IsActive,
                    Status = s.ReportStatus,
                    Format = s.ReportFormat,
                     TenantId = s.TenantId,
                    Parameters = s.Parameters,
                    TemplateType = s.TemplateType,
                    NotifyWhenComplete = s.NotifyWhenComplete,
                    ReportEmail = new ReportEmailEntityDto
                    {
                        Body = s.Email.Body,
                        Subject = s.Email.Subject,
                        To = s.Email.To
                    },
                    ReportTemplate = (from reportTemplate in _context.Set<ReportTemplate>()
                                      where reportTemplate.Id == s.TemplateId
                                      select new ReportTemplateEntityDto { Id = reportTemplate.Id, Name = reportTemplate.Name,  AssemblyQualifiedName = reportTemplate.AssemblyQualifiedName }).FirstOrDefault(),

                    ReportLanguage = (from reportLanguage in _context.Set<ReportLanguage>()
                                      where reportLanguage.Id == s.LanguageId
                                      select new ReportLanguageEntityDto
                                      {
                                          Id = reportLanguage.Id,
                                          Name = reportLanguage.Name,
                                          ShortName = reportLanguage.ShortName
                                      }).FirstOrDefault(),

                    ReportSchedule = (from reportSchedule in _context.Set<ReportSchedule>()
                                      .Include(i => i.ScheduleFrequency)
                                      .Include(i => i.ScheduleDeliveryTime)
                                      where reportSchedule.ReportId == s.Id && !reportSchedule.AuditIsDeleted
                                      select new ReportScheduleEntityDto
                                      {
                                          Id = reportSchedule.Id,
                                          AtSetTime = reportSchedule.AtSetTime,
                                          AtSetTimeZone = reportSchedule.AtSetTimeZone,
                                          EndsOn = reportSchedule.EndsOn,
                                          DeliveryTime = reportSchedule.ScheduleDeliveryTime,
                                          Frequency = reportSchedule.ScheduleFrequency,
                                          RepeatOn = reportSchedule.RepeatOn,
                                          StartsOn = reportSchedule.StartsOn

                                      }).FirstOrDefault(),
                    ReportFile = (from reportFile in _context.Set<ReportFile>().OrderByDescending(U => U.CreatedDate)
                                   where reportFile.ReportId == s.Id
                                   select new ReportFileEntityDto
                                   {
                                       Id = reportFile.Id,
                                       CreatedDate = reportFile.CreatedDate,
                                       FilePath = reportFile.FilePath
                                   }).FirstOrDefault(),
                }).FirstOrDefaultAsync();
        }

        public async Task<ReportDetailEntityDto> GetReportByIdAsync(Guid reportId)
        {
            return await _context.Reports
                .Include(i => i.TemplateType)
                .Include(i => i.ReportStatus)
                .Include(i => i.ReportFormat)
                .Include(i => i.Email)
                .Where(w => w.Id == reportId && w.IsActive && !w.AuditIsDeleted)
                .Select(s => new ReportDetailEntityDto
                {
                    Id = s.Id,
                    Name = s.Name,
                    Description = s.Description,
                    IsActive = s.IsActive,
                    Status = s.ReportStatus,
                    Format = s.ReportFormat,
                    Parameters = s.Parameters,
                    UserName = s.UserName,
                    TemplateType = s.TemplateType,
                    NotifyWhenComplete = s.NotifyWhenComplete,
                    TenantId = s.TenantId,
                    ReportEmail = new ReportEmailEntityDto
                    {
                        Body = s.Email.Body,
                        Subject = s.Email.Subject,
                        To = s.Email.To
                    },
                    ReportTemplate = (from reportTemplate in _context.Set<ReportTemplate>()
                                      where reportTemplate.Id == s.TemplateId
                                      select new ReportTemplateEntityDto { Id = reportTemplate.Id, Name = reportTemplate.Name, AssemblyQualifiedName = reportTemplate.AssemblyQualifiedName }).FirstOrDefault(),

                    ReportLanguage = (from reportLanguage in _context.Set<ReportLanguage>()
                                      where reportLanguage.Id == s.LanguageId
                                      select new ReportLanguageEntityDto
                                      {
                                          Id = reportLanguage.Id,
                                          Name = reportLanguage.Name,
                                          ShortName = reportLanguage.ShortName,
                                          LinuxTimeZone = reportLanguage.LinuxTimeZone,
                                          WindowsTimeZone = reportLanguage.WindowsTimeZone
                                      }).FirstOrDefault(),

                    ReportSchedule = (from reportSchedule in _context.Set<ReportSchedule>()
                                      .Include(i => i.ScheduleFrequency)
                                      .Include(i => i.ScheduleDeliveryTime)
                                      where reportSchedule.ReportId == s.Id && !reportSchedule.AuditIsDeleted
                                      select new ReportScheduleEntityDto
                                      {
                                          Id = reportSchedule.Id,
                                          AtSetTime = reportSchedule.AtSetTime,
                                          AtSetTimeZone = reportSchedule.AtSetTimeZone,
                                          EndsOn = reportSchedule.EndsOn,
                                          DeliveryTime = reportSchedule.ScheduleDeliveryTime,
                                          Frequency = reportSchedule.ScheduleFrequency,
                                          RepeatOn = reportSchedule.RepeatOn,
                                          StartsOn = reportSchedule.StartsOn

                                      }).FirstOrDefault(),
                    ReportFile = (from reportFile in _context.Set<ReportFile>().OrderByDescending(U => U.CreatedDate)
                                  where reportFile.ReportId == s.Id && !reportFile.AuditIsDeleted
                                  select new ReportFileEntityDto
                                  {
                                      Id = reportFile.Id,
                                      CreatedDate = reportFile.CreatedDate,
                                      FilePath = reportFile.FilePath,
                                      DateRangeStartDate = reportFile.DateRangeStartDate,
                                      DateRangeEndDate = reportFile.DateRangeEndDate
                                  }).FirstOrDefault(),
                }).FirstOrDefaultAsync();
        }

        public async Task<Report> GetAsync(Guid id)
            => await base.GetQuery(_currentTenant.Id).SingleOrDefaultAsync(x => x.Id == id);

      

        #region dispose
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion dispose
    }
}
