﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;
using System;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace CountwareTraffic.Services.Companies.Core
{
    public class Area : AggregateRoot, IAuditable, IDateable, IDeletable, ITraceable, IMultiTenant
    {
        private string _name;
        private string _description;
        private int _areaStatus;
        private int _areaTypeId;
        private Guid _regionId;
        private Guid _cityId;
        private Guid _districtId;
        private AreaAddress _address;
        private AreaContact _contact;
        private string _managerName;
        private Guid? _managerId;
        private TimeSpan? _workingHoursStart;
        private TimeSpan? _workingHoursEnd;
        private string _workingTimeZone;

        public string Name => _name;
        public string Description => _description;
        public AreaStatus AreaStatus { get; private set; }
        public Guid DistrictId => _districtId;
        public AreaType AreaType { get; private set; }
        public Guid RegionId => _regionId;
        public AreaAddress Address => _address;
        public AreaContact Contact => _contact;
        public Guid CityId => _cityId;
        public string ManagerName => _managerName;
        public Guid? ManagerId => _managerId;
        public TimeSpan? WorkingHoursStart => _workingHoursStart;
        public TimeSpan? WorkingHoursEnd => _workingHoursEnd;
        public string WorkingTimeZone => _workingTimeZone;

        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public bool AuditIsDeleted { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public Guid TenantId { get; set; }
        #endregion default properties

        private Area() { }

        public static Area Create(string name, string description, Guid districtId, Guid cityId, int areaTypeId, Guid regionId, string managerName, Guid? managerId, AreaAddress address, AreaContact contact, TimeSpan? workingHoursStart, TimeSpan? workingHoursEnd, string workingTimeZone)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1405 " + nameof(name));

            if (name.Length > 150)
                throw new ArgumentException("#E1406 " + nameof(name));

            if (areaTypeId < 1)
                throw new InvalidAreaTypeException(areaTypeId);

            if (!description.IsNullOrWhiteSpace() && description.Length > 2500)
                throw new ArgumentException("#E1408 " + nameof(description));

            if (!managerName.IsNullOrWhiteSpace() && managerName.Length > 100)
                throw new ArgumentException("#E1409 " + nameof(managerName));

            var area = new Area
            {
                _districtId = districtId,
                _name = name,
                _areaTypeId = areaTypeId,
                _description = description,
                _regionId = regionId,
                _address = address,
                _contact = contact,
                _cityId = cityId,
                _managerName = managerName,
                _managerId = managerId,
                _workingHoursStart = workingHoursStart,
                _workingHoursEnd = workingHoursEnd,
                _workingTimeZone = workingTimeZone,
                _areaStatus = SubAreaStatus.Created.Id
            };

            area.WhenCreated(area.Id, area.Name, area.WorkingHoursStart, area.WorkingHoursEnd, area.WorkingTimeZone);
            return area;
        }

        public void CompleteChange(string name, string description, Guid districtId, Guid cityId, int areaTypeId, string managerName, Guid? managerId, AreaAddressEntityDto addressEntityDto, AreaContactEntityDto contactEntityDto, TimeSpan? workingHoursStart, TimeSpan? workingHoursEnd, string workingTimeZone)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("#E1410 " + nameof(name));

            if (areaTypeId < 1)
                throw new InvalidAreaTypeException(areaTypeId);

            if (!description.IsNullOrWhiteSpace() && description.Length > 2500)
                throw new ArgumentException("#E1411 " + nameof(description));

            if (!managerName.IsNullOrWhiteSpace() && managerName.Length > 100)
                throw new ArgumentException("#E1412 " + nameof(managerName));

            WhenChanged(_id, name, _name, workingHoursStart, _workingHoursStart, workingHoursEnd, _workingHoursEnd, workingTimeZone, _workingTimeZone);

            if (areaTypeId != _areaTypeId)
            {
                ChangeAreaType(areaTypeId);
                _areaTypeId = areaTypeId;
            }
            if (_name != name) _name = name;
            if (_description != description) _description = description;

            _districtId = districtId;
            _cityId = cityId;
            _managerName = managerName;
            _managerId = managerId;

            _workingHoursStart = workingHoursStart;
            _workingHoursEnd = workingHoursEnd;
            _workingTimeZone = workingTimeZone;

            if (_contact == null)
                _contact = AreaContact.Create(contactEntityDto.GsmNumber, contactEntityDto.GsmDialCode, contactEntityDto.GsmCountryCode, contactEntityDto.PhoneNumber, contactEntityDto.PhoneDialCode, contactEntityDto.PhoneCountryCode, contactEntityDto.EmailAddress);
            else
                _contact.WhenChanged(contactEntityDto.GsmNumber, contactEntityDto.GsmDialCode, contactEntityDto.GsmCountryCode, contactEntityDto.PhoneNumber, contactEntityDto.PhoneDialCode, contactEntityDto.PhoneCountryCode, contactEntityDto.EmailAddress);

            if (_address == null)
                _address = AreaAddress.Create(addressEntityDto.Street, addressEntityDto.Latitude, addressEntityDto.Longitude);

            else
                _address.WhenChanged(addressEntityDto.Street, addressEntityDto.Latitude, addressEntityDto.Longitude);

        }

        public void ChangeAreaType(int areaTypeId)
        {
            if (areaTypeId < 1)
                throw new InvalidAreaTypeException(areaTypeId);

            AddEvent(new AreaTypeChanged(this, areaTypeId));
        }

        public void WhenCreated(Guid areaId, string areaName, TimeSpan? workingHoursStart, TimeSpan? workingHoursEnd, string workingTimeZone)
            => AddEvent(new AreaCreated(areaId, areaName, workingHoursStart, workingHoursEnd, workingTimeZone));
        public void WhenChanged(Guid areaId, string areaName, string oldAreaName, TimeSpan? workingHoursStart, TimeSpan? oldWorkingHoursStart, TimeSpan? workingHoursEnd, TimeSpan? oldWorkingHoursEnd, string workingTimeZone, string oldWorkingTimeZone)
            => AddEvent(new AreaChanged(areaId, areaName, oldAreaName, workingHoursStart, oldWorkingHoursStart, workingHoursEnd, oldWorkingHoursEnd, workingTimeZone, oldWorkingTimeZone));
        public void WhenDeleted(Guid areaId)
            => AddEvent(new AreaDeleted(areaId));

        public void WhenCreatedCompleted() => _areaStatus = AreaStatus.Completed.Id;

        public void WhenCreatedRejected() => _areaStatus = AreaStatus.Rejected.Id;

        public void WhenChangedRejected(string oldName) => _name = oldName;

        public void WhenDeletedRejected() => AuditIsDeleted = false;

        public void WhenHierarchyCreated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, string areaName)
            => AddEvent(new AreaHierarchyCreated(tenantId, companyId, countryId, regionId, areaId, areaName));

        public void WhenHierarchyChanged(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, string areaName)
            => AddEvent(new AreaHierarchyUpdated(tenantId, companyId, countryId, regionId, areaId, areaName));

        public void WhenHierarchyDeleted(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, string areaName)
            => AddEvent(new AreaHierarchyDeleted(tenantId, companyId, countryId, regionId, areaId, areaName));
    }
}
