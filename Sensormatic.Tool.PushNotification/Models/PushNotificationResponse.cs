﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sensormatic.Tool.PushNotification
{
    public class PushNotificationResponse
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
