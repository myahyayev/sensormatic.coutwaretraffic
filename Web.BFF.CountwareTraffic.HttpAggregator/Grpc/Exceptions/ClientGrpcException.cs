﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;
namespace Web.BFF.CountwareTraffic.HttpAggregator.Grpc
{
    public class ClientGrpcException : BaseException
    {
        public ClientGrpcException(ICollection<ErrorResult> errorResults, int statusCode, Sensormatic.Tool.Core.ResponseMessageType responseMessageType) 
            : base(errorResults, statusCode, responseMessageType) { }
    }
}
