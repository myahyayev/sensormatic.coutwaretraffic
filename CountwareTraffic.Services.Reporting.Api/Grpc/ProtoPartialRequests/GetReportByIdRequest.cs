﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Reporting.Grpc
{
    [ServiceLog]
    public sealed partial class GetReportByIdRequest : SensormaticRequestValidate
    {
        internal Guid _ReportId
        {
            get
            {
                if (Guid.TryParse(reportId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { reportId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_ReportId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1533 ReportId cannot be null", nameof(_ReportId)));
        }
    }
}
