﻿using Convey.CQRS.Queries;
using System.ComponentModel.DataAnnotations;

namespace CountwareTraffic.Services.Users.Application
{
    public class RefreshToken : IQuery<IdentityRefreshTokenResponse>
    {
        [Required]
        public string AccessToken { get; set; }
        [Required]

        public string RefreshTokenKey { get; set; }
    }
}
