﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
namespace CountwareTraffic.Services.Companies.Grpc
{
    [Authorize]
    public class LookupService : Lookup.LookupBase
    {
        private readonly ILogger<LookupService> _logger;
        private readonly IQueryDispatcher _queryDispatcher;
        public LookupService(ILogger<LookupService> logger, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _queryDispatcher = queryDispatcher;
        }

        public override async Task<GetCountryLookupDetailResponse> GetCountryLookupById(GetCountryLookupRequest request, ServerCallContext context)
        {
            var countryLookup = await _queryDispatcher.QueryAsync(new GetCountryLookup
            {
                CountryId = request._CountryId
            });

            GetCountryLookupDetailResponse response = new();

            response.CountryLookupDetail = new()
            {
                Capital = countryLookup.Capital,
                Id = countryLookup.Id.ToString(),
                ContinentCode = countryLookup.ContinentCode,
                CurrencyCode = countryLookup.CurrencyCode,
                Iso = countryLookup.Iso,
                Iso3 = countryLookup.Iso3,
                IsoNumeric = countryLookup.IsoNumeric,
                Name = countryLookup.Name
            };

            return response;
        }

        public override async Task<GetCountryLookupsDetailResponse> GetCountryLookups(Empty request, ServerCallContext context)
        {
            var countryLookups = await _queryDispatcher.QueryAsync(new GetCountryLookups());

            GetCountryLookupsDetailResponse response = new();

            countryLookups.ForEach(countryLookup => response.CountryLookupDetails.Add(new CountryLookupDetail
            {
                Capital = countryLookup.Capital,
                Id = countryLookup.Id.ToString(),
                ContinentCode = countryLookup.ContinentCode,
                CurrencyCode = countryLookup.CurrencyCode,
                Iso = countryLookup.Iso,
                Iso3 = countryLookup.Iso3,
                IsoNumeric = countryLookup.IsoNumeric,
                Name = countryLookup.Name
            }));
            return response;
        }

        public override async Task<GetCityLookupDetailResponse> GetCityLookupById(GetCityLookupRequest request, ServerCallContext context)
        {
            var cityLookup = await _queryDispatcher.QueryAsync(new GetCityLookup
            {
                 CityId = request._CityId
            });

            GetCityLookupDetailResponse response = new();

            response.CityLookupDetail = new()
            {
                Id = cityLookup.Id.ToString(),
                Name = cityLookup.Name,
                CountryId = cityLookup.CountryId.ToString()
            };

            return response;
        }

        public override async Task<GetCityLookupsDetailResponse> GetCityLookups(GetCityLookupsRequest request, ServerCallContext context)
        {
            var cityLookups = await _queryDispatcher.QueryAsync(new GetCityLookups { CountryId = request._CountryId });

            GetCityLookupsDetailResponse response = new();

            cityLookups.ForEach(cityLookup => response.CityLookupDetails.Add(new CityLookupDetail
            {
                Id = cityLookup.Id.ToString(),
                Name = cityLookup.Name,
                CountryId = cityLookup.CountryId.ToString()
            }));
            return response;
        }

        public override async Task<GetDistrictLookupDetailResponse> GetDistrictLookupById(GetDistrictLookupRequest request, ServerCallContext context)
        {
            var districtLookup = await _queryDispatcher.QueryAsync(new GetDistrictLookup
            {
                 DistrictId = request._DistrictId
            });

            GetDistrictLookupDetailResponse response = new();

            response.DistrictLookupDetail = new()
            {
                Id = districtLookup.Id.ToString(),
                Name = districtLookup.Name,
                CityId = districtLookup.CityId.ToString()
            };

            return response;
        }

        public override async Task<GetDistrictLookupsDetailResponse> GetDistrictLookups(GetDistrictLookupsRequest request, ServerCallContext context)
        {
            var districtLookups = await _queryDispatcher.QueryAsync(new GetDistrictLookups { CityId = request._CityId});

            GetDistrictLookupsDetailResponse response = new();

            districtLookups.ForEach(districtLookup => response.DistrictLookupDetail.Add(new DistrictLookupDetail
            {
                Id = districtLookup.Id.ToString(),
                Name = districtLookup.Name,
                CityId = districtLookup.CityId.ToString()
            }));
            return response;
        }
    }
}
