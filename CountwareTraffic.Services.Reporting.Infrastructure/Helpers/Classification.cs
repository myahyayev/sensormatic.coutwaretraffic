﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public abstract class Classification : IComparable, IResourceable
    {
        public string Name { get; private set; }
        public int Id { get; private set; }
        public string DisplayName { get; private set; }
        public string ShortName { get; set; }
        public abstract string ResourceKey {get;}
        
        protected Classification(int id, string name, string displayName, string shortName) 
            => (Id, Name, DisplayName, ShortName) = (id, name, displayName, shortName);

        public override string ToString() => Name;

        public static IEnumerable<T> GetAll<T>() where T : Classification =>
            typeof(T).GetFields(BindingFlags.Public |
                                BindingFlags.Static |
                                BindingFlags.DeclaredOnly)
                        .Select(f => f.GetValue(null))
                        .Cast<T>();

        public override bool Equals(object obj)
        {
            if (obj is not Classification otherValue)
            {
                return false;
            }

            var typeMatches = GetType().Equals(obj.GetType());
            var valueMatches = Id.Equals(otherValue.Id);

            return typeMatches && valueMatches;
        } 

        public override int GetHashCode() => Id.GetHashCode();

        public static int AbsoluteDifference(Classification firstValue, Classification secondValue)
        {
            var absoluteDifference = Math.Abs(firstValue.Id - secondValue.Id);
            return absoluteDifference;
        }

        public static T FromValue<T>(int value) where T : Classification
        {
            var matchingItem = Parse<T, int>(value, "value", item => item.Id == value);
            return matchingItem;
        }

        public static T FromName<T>(string displayName) where T : Classification
        {
            var matchingItem = Parse<T, string>(displayName, "display name", item => item.Name == displayName);
            return matchingItem;
        }

        private static T Parse<T, K>(K value, string description, Func<T, bool> predicate) where T : Classification
        {
            var matchingItem = GetAll<T>().FirstOrDefault(predicate);

            if (matchingItem == null)
                throw new InvalidOperationException($"'{value}' is not a valid {description} in {typeof(T)}");

            return matchingItem;
        }

        public int CompareTo(object other) => Id.CompareTo(((Classification)other).Id);
    }
}
