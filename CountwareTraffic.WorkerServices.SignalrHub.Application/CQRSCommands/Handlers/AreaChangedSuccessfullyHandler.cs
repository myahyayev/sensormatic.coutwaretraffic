﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class AreaChangedSuccessfullyHandler : ICommandHandler<AreaChangedSuccessfully>
    {
        private readonly IHubContext<AreaNotificationsHub, IAreaNotificationsHub> _hubContext;
        public AreaChangedSuccessfullyHandler(IHubContext<AreaNotificationsHub, IAreaNotificationsHub> hubContext)
            => _hubContext = hubContext;


        public async Task HandleAsync(AreaChangedSuccessfully command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .AreaChangedSuccessfullyReceived(command);
    }
}
