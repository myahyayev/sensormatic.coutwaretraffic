
![Logo](https://cwreportingprod.blob.core.windows.net/reportimages/countware-traffic-logo.png)


# CountwareTraffic








## Installation

Run on Docker:

Make docker config & Run:

	1) wsl -d docker-desktop
	2) sysctl -w vm.max_map_count=262144
	3) docker-compose -f .\docker-compose.yml -f .\docker-compose.override.yml up -d


	
## URL list of Services

#### Prodcution

http://admin.traffic.countware.net/

http://tenantName.traffic.countware.net/

http://company-outbox.traffic.countware.net/

http://company-elk-repair-scheduler.traffic.countware.net/

http://traffic.countware.net/

http://device-outbox.traffic.countware.net/

http://elsk.traffic.countware.net/

http://event-outbox.traffic.countware.net/

http://event-elk-repair-scheduler.traffic.countware.net/

http://graylog.traffic.countware.net/

http://identity-server.traffic.countware.net/

http://mobile-bff.traffic.countware.net/

http://grafana.traffic.countware.net/

http://prometheus.traffic.countware.net/

http://reporting-api.traffic.countware.net/

http://reporting-outbox.traffic.countware.net/

http://reporting-scheduler.traffic.countware.net/

http://signalrhub.traffic.countware.net/

https://elk-app-countwaretraffic-prod.kb.westeurope.azure.elastic-cloud.com:9243

#### Development / Test

[not implemented]

## Authors

- [@Mahmud Yahyayev](https://github.com/mahmudyahyayev)
- [@Mahdi Rostami](https://www.github.com/m3hdiRostami/)
- [@Onur Cevik]()