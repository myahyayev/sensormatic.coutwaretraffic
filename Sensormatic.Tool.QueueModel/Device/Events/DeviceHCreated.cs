﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceHCreated : MessageEnvelope, IQueueEvent
    {
        public DeviceHCreated(Guid deviceId, string deviceName, Guid subAreaId, Guid areaId, Guid regionId, Guid countryId, Guid companyId, Guid recordId, Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            DeviceId = deviceId;
            DeviceName = deviceName;
            SubAreaId = subAreaId;
            AreaId = areaId;
            RegionId = regionId;
            CountryId = countryId;
            CompanyId = companyId;
            RecordId = recordId;
        }

        public Guid DeviceId { get; }
        public string DeviceName { get; }
        public Guid SubAreaId { get; }
        public Guid AreaId { get; }
        public Guid RegionId { get; }
        public Guid CountryId { get; }
        public Guid CompanyId { get; }
        public Guid RecordId { get; init; }
    }
}
