﻿using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public class TrafficByTimePeriodDto
    {
        public List<string> Categories { get; set; }
        public CurrentSeries CurrentSeries { get; set; }
        public PreviusSeries PreviusSeries { get; set; }
        public TrafficByTimePeriodDto()
        {
            CurrentSeries = new();
            PreviusSeries = new();
            Categories = new();
        }
    }

    public class Series
    {
        public string Name { get; set; }
        public List<int> Data { get; set; }
    }

    public class CurrentSeries : Series
    {
        public CurrentSeries()
        {
            Name = "Current";
            Data = new();
        }
    }

    public class PreviusSeries : Series
    {
        public PreviusSeries()
        {
            Name = "Previus";
            Data = new();
        }
    }
}
