﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantSelectHandler : IQueryHandler<GetTenantSelect, IdentityTenantSelectResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetTenantSelectHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityTenantSelectResponse> HandleAsync(GetTenantSelect query)
        {
            return await _identityServerService.GetTenantSelectAsync();
        }
    }
}
