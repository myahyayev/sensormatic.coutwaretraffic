﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class DeleteTenantGroupApiResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }

        public List<DeleteTenantGroupApiErrorResult> ErrorResults { get; set; }
    }

    public class DeleteTenantGroupApiErrorResult
    {
        public string Message { get; set; }
    }
}
