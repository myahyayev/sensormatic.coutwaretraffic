﻿using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using Microsoft.EntityFrameworkCore;
using System;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class AreaDbContext : DbContext
    {
        private readonly AreaDbProvider _provider;
        public AreaDbContext(DbContextOptions options)
            : base(options) { }

        public AreaDbContext(DbContextOptions options, AreaDbProvider provider) : base(options) => _provider = provider;

        #region DBSETs
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<AreaType> AreaTypes { get; set; }
        public DbSet<AreaStatus> AreaStatuses { get; set; }
        public virtual DbSet<SubArea> SubAreas { get; set; }
        public DbSet<SubAreaStatus> SubAreaStatuses { get; set; }
        public DbSet<OutboxMessage> OutboxMessages { get; set; }
        public virtual DbSet<CountryLookup> CountryLookups { get; set; }
        public virtual DbSet<CityLookup> CityLookups { get; set; }
        public virtual DbSet<DistrictLookup> DistrictLookups { get; set; }

        #endregion DBSETs

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies(false);

            if (_provider != null)
                optionsBuilder.AddInterceptors(new SaveChangesInterceptor(_provider));
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new CompanyEntityTypeConfiguration());
            builder.ApplyConfiguration(new CountryEntityTypeConfiguration());
            builder.ApplyConfiguration(new RegionEntityTypeConfiguration());
            builder.ApplyConfiguration(new AreaEntityTypeConfiguration());

            builder.ApplyConfiguration(new CountryLookupEntityTypeConfiguration());
            builder.ApplyConfiguration(new CityLookupEntityTypeConfiguration());
            builder.ApplyConfiguration(new DistrictLookupEntityTypeConfiguration());

            builder.ApplyConfiguration(new SubAreaEntityTypeConfiguration());
            builder.ApplyConfiguration(new SubAreaStatusEntityTypeConfiguration());
            builder.ApplyConfiguration(new AreaTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new AreaStatusEntityTypeConfiguration());
            builder.ApplyConfiguration(new OutboxMessageEntityTypeConfiguration());
        }
    }
}
