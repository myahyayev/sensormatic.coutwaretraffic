﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class UpdateTenantHandler : IQueryHandler<UpdateTenant, IdentityUpdateTenantResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public UpdateTenantHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityUpdateTenantResponse> HandleAsync(UpdateTenant query)
        {
            return await _identityServerService.UpdateTenantAsync(new IdentityUpdateTenantRequest
            {
                Id = query.Id,
                Name = query.Name,
                Description = query.Description,
                ErpCode = query.ErpCode,
                ErpGroupName = query.ErpGroupName,
                ErpStartDate = query.ErpStartDate,
                ErpEndDate = query.ErpEndDate,
                TenantGroupId = query.TenantGroupId
            });
        }
    }
}
