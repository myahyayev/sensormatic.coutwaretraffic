﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class ReportGeneratingFailedHandler : ICommandHandler<ReportGeneratingFailed>
    {
        private readonly IHubContext<ReportNotificationsHub, IReportNotificationsHub> _hubContext;
        private readonly ILogger<ReportGeneratingFailedHandler> _logger;
        public ReportGeneratingFailedHandler(IHubContext<ReportNotificationsHub, IReportNotificationsHub> hubContext, ILogger<ReportGeneratingFailedHandler> logger)
        {
            _logger = logger;
            _hubContext = hubContext;
        }

        public async Task HandleAsync(ReportGeneratingFailed command)
        {
            _logger.LogInformation($"ReportGeneratingFailedHandler : {command.TenantId}");

            await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .ReportGeneratingFailedReceived(command);
        }
    }
}

