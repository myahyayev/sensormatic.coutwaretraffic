﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ChangeRegionApiRequest : SensormaticRequestValidate
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? ManagerId { get; set; }
        public string ManagerName { get; set; }
        public Guid CountryId { get; set; }

        public override void Validate()
        {
            if (CountryId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1190 CountryId cannot be null", nameof(CountryId)));

            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1191 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1192 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1193 Description invalid format", nameof(Description)));

            if (!ManagerName.IsNullOrWhiteSpace() && ManagerName.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1194 ManagerName invalid format", nameof(ManagerName)));
        }
    }
}
