﻿using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class Location
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public LocationLevel LocationLevel { get; set; }
    }

    public enum LocationLevel : byte
    {
        Tenant = 1,
        Company = 2,
        Country = 3,
        Region = 4,
        Area = 5,
        SubArea = 6,
        Device = 7,
    }
}
