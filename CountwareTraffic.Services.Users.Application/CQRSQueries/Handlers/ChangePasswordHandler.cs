﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ChangePasswordHandler : IQueryHandler<ChangePassword, IdentityChangePasswordResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public ChangePasswordHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<IdentityChangePasswordResponse> HandleAsync(ChangePassword query)
        {
            return await _identityServerService.ChangePasswordAsync(new IdentityChangePasswordRequest
            {
                UserName = query.UserName,
                CurrentPassword = query.CurrentPassword,
                NewPassword = query.NewPassword,

                ConfirmPassword = query.ConfirmPassword
            });
        }
    }
}
