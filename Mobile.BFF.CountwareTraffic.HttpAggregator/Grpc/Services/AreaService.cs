﻿using CountwareTraffic.Services.Companies.Grpc;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Grpc.Common;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AreaService : IScopedSelfDependency
    {
        private readonly Area.AreaClient _areaClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;

        public AreaService(Area.AreaClient areaClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _areaClient = areaClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetAreaDetails> GetAreaByIdAsync(Guid areaId)
        {
            GetAreaRequest grpcRequest = new() { AreaId = areaId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_areaClient.GetAreaByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetAreaDetails
            {
                Id = new Guid(grpcResponse.AreaDetail.Id),
                Name = grpcResponse.AreaDetail.Name,
                Description = grpcResponse.AreaDetail.Description,
                DistrictId = new Guid(grpcResponse.AreaDetail.DistrictId),
                AuditCreateBy = new Guid(grpcResponse.AreaDetail.Audit.AuditCreateBy),
                AuditCreateDate = grpcResponse.AreaDetail.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(grpcResponse.AreaDetail.Audit.AuditModifiedBy),
                AuditModifiedDate = grpcResponse.AreaDetail.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                AreaTypeName = grpcResponse.AreaDetail.AreaTypeName,
                EmailAddress = grpcResponse.AreaDetail.EmailAddress,
                RegionId = new Guid(grpcResponse.AreaDetail.RegionId),
                GsmNumber = grpcResponse.AreaDetail.GsmNumber,
                Latitude = grpcResponse.AreaDetail.Latitude,
                Longitude = grpcResponse.AreaDetail.Longitude,
                PhoneNumber = grpcResponse.AreaDetail.PhoneNumber,
                Street = grpcResponse.AreaDetail.Street,
                AreaTypeId = grpcResponse.AreaDetail.AreaTypeId,
                CityId = new Guid(grpcResponse.AreaDetail.CityId),
                GsmCountryCode = grpcResponse.AreaDetail.GsmCountryCode,
                GsmDialCode = grpcResponse.AreaDetail.GsmDialCode,
                PhoneDialCode = grpcResponse.AreaDetail.PhoneDialCode,
                PhoneCountryCode = grpcResponse.AreaDetail.PhoneCountryCode,
                ManagerName = grpcResponse.AreaDetail.ManagerName,
                CountryId = new Guid(grpcResponse.AreaDetail.CountryLookupId),
                ManagerId = grpcResponse.AreaDetail.ManagerId == null ? null : new Guid(grpcResponse.AreaDetail.ManagerId),
                WorkingHoursStart = grpcResponse.AreaDetail.WorkingHoursStart != null ? TimeSpan.FromTicks(grpcResponse.AreaDetail.WorkingHoursStart.Value).ToString() : null,
                WorkingHoursEnd = grpcResponse.AreaDetail.WorkingHoursEnd != null ? TimeSpan.FromTicks(grpcResponse.AreaDetail.WorkingHoursEnd.Value).ToString() : null,
                WorkingTimeZone = grpcResponse.AreaDetail.WorkingTimeZone
            };
        }

        public async Task<ApiPagedResponse<GetAreaDetails>> GetAreasAsync(Guid regionId, DataSourceApiRequest paging)
        {
            GetAreasRequest grpcRequest = new()
            {
                RegionId = regionId.ToString(),
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };


            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Companies.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Companies.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_areaClient.GetAreasAsync, grpcRequest, hasClientSideLog: false);

            var areas = grpcResponse.AreaDetails.Select(area => new GetAreaDetails
            {
                Id = new Guid(area.Id),
                Name = area.Name,
                Description = area.Description,
                DistrictId = new Guid(area.DistrictId),
                AuditCreateBy = new Guid(area.Audit.AuditCreateBy),
                AuditCreateDate = area.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(area.Audit.AuditModifiedBy),
                AuditModifiedDate = area.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                AreaTypeName = area.AreaTypeName,
                EmailAddress = area.EmailAddress,
                RegionId = new Guid(area.RegionId),
                GsmNumber = area.GsmNumber,
                Latitude = area.Latitude,
                Longitude = area.Longitude,
                PhoneNumber = area.PhoneNumber,
                Street = area.Street,
                AreaTypeId = area.AreaTypeId,
                CityId = new Guid(area.CityId),
                GsmCountryCode = area.GsmCountryCode,
                GsmDialCode = area.GsmDialCode,
                PhoneDialCode = area.PhoneDialCode,
                PhoneCountryCode = area.PhoneCountryCode,
                ManagerName = area.ManagerName,
                ManagerId = area.ManagerId == null ? null : new Guid(area.ManagerId),
                CountryId = new Guid(area.CountryLookupId),
                WorkingHoursStart = area.WorkingHoursStart != null ? TimeSpan.FromTicks(area.WorkingHoursStart.Value).ToString() : null,
                WorkingHoursEnd = area.WorkingHoursEnd != null ? TimeSpan.FromTicks(area.WorkingHoursEnd.Value).ToString() : null,
                WorkingTimeZone = area.WorkingTimeZone,
            });

            return new ApiPagedResponse<GetAreaDetails>(areas, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task AddAreaAsync(Guid regionId, AddAreaApiRequest request)
        {
            CreateAreaRequest grpcRequest = new()
            {
                RegionId = regionId.ToString(),
                Description = request.Description,
                Name = request.Name,
                AreaTypeId = request.AreaTypeId,
                Street = request.Street,
                CityId = request.CityId.ToString(),
                GsmCountryCode = request.GsmCountryCode,
                GsmDialCode = request.GsmDialCode,
                ManagerId = request.ManagerId.HasValue ? request.ManagerId.ToString() : null,
                EmailAddress = request.EmailAddress,
                ManagerName = request.ManagerName,
                PhoneCountryCode = request.PhoneCountryCode,
                PhoneDialCode = request.PhoneDialCode,
                DistrictId = request.DistrictId.ToString(),
                GsmNumber = request.GsmNumber,
                Latitude = request.Latitude,
                Longitude = request.Longitude,
                PhoneNumber = request.PhoneNumber,
                WorkingHoursStart = request.WorkingHoursStart?.Ticks,
                WorkingHoursEnd = request.WorkingHoursEnd?.Ticks,
                WorkingTimeZone = request.WorkingTimeZone,
    };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_areaClient.AddAreaAsync, grpcRequest, hasClientSideLog: true);
        }

        public async Task ChangeAreaAsync(Guid areaId, ChangeAreaApiRequest request)
        {
            UpdateAreaRequest grpcRequest = new()
            {
                AreaId = areaId.ToString(),
                Description = request.Description,
                Name = request.Name,
                AreaTypeId = request.AreaTypeId,
                Street = request.Street,
                CityId = request.CityId.ToString(),
                GsmCountryCode = request.GsmCountryCode,
                GsmDialCode = request.GsmDialCode,
                ManagerId = request.ManagerId.HasValue ? request.ManagerId.ToString() : null,
                EmailAddress = request.EmailAddress,
                ManagerName = request.ManagerName,
                PhoneCountryCode = request.PhoneCountryCode,
                PhoneDialCode = request.PhoneDialCode,
                DistrictId = request.DistrictId.ToString(),
                GsmNumber = request.GsmNumber,
                Latitude = request.Latitude,
                Longitude = request.Longitude,
                PhoneNumber = request.PhoneNumber,
                WorkingHoursStart = request.WorkingHoursStart?.Ticks,
                WorkingHoursEnd = request.WorkingHoursEnd?.Ticks,
                WorkingTimeZone = request.WorkingTimeZone
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_areaClient.ChangeAreaAsync, grpcRequest, hasClientSideLog: true);
        }

        public async Task CancelAreaAsync(Guid areaId)
        {
            DeleteAreaRequest grpcRequest = new() { AreaId = areaId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_areaClient.DeleteAreaAsync, grpcRequest, hasClientSideLog: true);
        }

        public async Task<IEnumerable<GetAreaType>> GetAreaTypesAsync()
        {
            GetAreaTypesRequest grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
              .CallMethodAsync(_areaClient.GetAreaTypesAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.AreaTypes.Select(type => new GetAreaType
            {
                Id = type.Id,
                Name = type.Name
            });
        }

        public async Task<IEnumerable<Hierarchy>> GetHierarchyAsync(Guid areaId)
        {
            GetHierarchyRequest grpcRequest = new() { Id = areaId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_areaClient.GetHierarchyAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.Hierarchies.Select(hierarchy => new Hierarchy
            {
                Id = new Guid(hierarchy.Id),
                Name = hierarchy.Name,
                HierarchyLevel = hierarchy.HierarchyLevel switch
                {
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Default => throw new NotImplementedException(),
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Tenant => HierarchyLevel.Tenant,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Company => HierarchyLevel.Company,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Country => HierarchyLevel.Country,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Region => HierarchyLevel.Region,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Area => HierarchyLevel.Area,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.SubArea => HierarchyLevel.SubArea,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Device => HierarchyLevel.Device,
                    _ => throw new NotImplementedException(),
                }
            });
        }

        public async Task<List<Parent>> GetDeviceFilterAsync(Guid parentId)
        {
            KeyValueRequest grpcRequest = new KeyValueRequest
            {
                ParentId = parentId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_areaClient.GetAreasKeyValueAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.KeyValues.Select(u => new Parent
            {
                Children = null,
                Expanded = true,
                Label = u.Name,
                Data = new Data

                {
                    HierarchyLevel = HierarchyLevel.Area,
                    Id = new Guid(u.Id)
                }
            }).ToList();
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
