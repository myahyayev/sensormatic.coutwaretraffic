﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetReportLanguagesApiResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
