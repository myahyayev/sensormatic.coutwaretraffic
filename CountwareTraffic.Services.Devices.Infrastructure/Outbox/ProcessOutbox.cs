﻿using Convey.CQRS.Commands;
using Nest;
using Sensormatic.Tool.Common;
using System;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class ProcessOutbox : ICommand
    {
        public Guid RecordId { get; set; }
        public string Exception { get; set; }
        public OperationStatus Status
        {
            get
            {
                return Exception == null ? OperationStatus.Success : OperationStatus.Failed;
            }
        }
    }
}
