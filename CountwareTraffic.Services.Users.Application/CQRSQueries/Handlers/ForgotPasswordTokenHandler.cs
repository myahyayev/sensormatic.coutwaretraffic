﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ForgotPasswordTokenHandler : IQueryHandler<ForgotPasswordToken, IdentityForgotPasswordResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public ForgotPasswordTokenHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityForgotPasswordResponse> HandleAsync(ForgotPasswordToken query)
        {
            return await _identityServerService.ForgotPasswordTokenAsync(new IdentityResetPasswordWithTokenRequest
            {
                Token = query.Token,
                UserName = query.UserName,
                Password = query.Password,
                ConfirmPassword = query.ConfirmPassword
            });
        }
    }
}
