﻿using CountwareTraffic.Services.Companies.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Dapper;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class DistrictLookupRepository : Repository<DistrictLookup>, IDistrictLookupRepository
    {
        private readonly new AreaDbContext _context;

        public DistrictLookupRepository(AreaDbContext context) : base(context) => _context = context;

        public async Task<IEnumerable<DistrictLookup>> GetAllAsync(Guid cityId) => await _context.DistrictLookups.Where(u => u.CityId == cityId).OrderBy(u => u.Name).ToListAsync();

        public async Task<DistrictHierarchyData> GetDistrictHierarchy(Guid districtId)
        {
            var sql = @"SELECT Districts.Id AS DistrictId,
	                       Districts.Name AS DistrictName,
	                       Cities.Id AS CityId,
	                       Cities.Name AS CityName,
	                       Countries.Id AS CountryId,
	                       Countries.Name AS CountryName
                    FROM [areas].[DistrictLookups] AS Districts
                            INNER JOIN [areas].[CityLookups] AS Cities ON Districts.CityId = Cities.Id
                            INNER JOIN [areas].[CountryLookups] AS Countries ON Cities.CountryId = Countries.Id
                                    WHERE Districts.Id = @Id";

            var connection = _context.Database.GetDbConnection();

            var data = await connection.QueryFirstOrDefaultAsync<DistrictHierarchyData>(sql, new
            {
                @Id = districtId
            });

            return data;
        }

        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible
    }
}
