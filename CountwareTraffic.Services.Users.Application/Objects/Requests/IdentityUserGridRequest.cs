﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityUserGridRequest
    {
        public PagingQuery Paging { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
        public IdentityUserGridRequest()
        {
            Paging = new PagingQuery();
            Filters = new List<GridFilter>();
            Sorts = new List<SortDescriptor>();
        }
    }
}
