﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class UpdatePermission : IQuery<IdentityUpdatePermissionResponse>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
