﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetCityLookupDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid CountryId { get; set; }
    }
}
