﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using CountwareTraffic.Services.Reporting.Engine;
using CountwareTraffic.Services.Reporting.Engine.TrafficReport;
using Elasticsearch.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.ElasticSearch;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class TrafficReportElasticSearchRepository : ElasticRepository<EventCreateElasticData>, ITrafficReportElasticSearchRepository
    {
        private readonly ILogger<EventCreateElasticData> _logger;
        private readonly IResourceMongoDbRepository _resourceMongoDbRepository;
        private readonly ComparedStoreSummaryCalculations _comparedStoreSummaryCalculations;
        private readonly TimeZoneSettings _timeZoneSettings;
        public TrafficReportElasticSearchRepository(IConfiguration configuration, ILogger<EventCreateElasticData> logger, IResourceMongoDbRepository resourceMongoDbRepository, ComparedStoreSummaryCalculations comparedStoreSummaryCalculations, IOptions<TimeZoneSettings> timeZoneSettings)
            : base(configuration, logger, ElasticsearchKeys.CountwareTrafficEventsEvent)
        {
            _logger = logger;
            _resourceMongoDbRepository = resourceMongoDbRepository;
            _comparedStoreSummaryCalculations = comparedStoreSummaryCalculations;
            _timeZoneSettings = timeZoneSettings.Value;
        }
        

        public async Task<List<ComparedStoreSummary>> TrafficComparedStoresSummaryAsync(List<Location> locations, DateTime startDate, DateTime endDate, ReportPeriodType reportPeriod, Guid tenantId, Guid languageId)
        {
            await this.CheckElasticIndexAsync();

            List<ComparedStoreSummary> response = new();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
                .Must(must)
                .MustNot(mustNot)
                .Filter(filter));

            must.Add(m => m
              .Term(t => t.Field(f => f.TenantId).Value(tenantId)));

            mustNot.Add(m => m
               .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Unknown.Id)));

            this.GenerateLocationQuery(locations, ref must);

            var dateRangeExpressions = this.GetDateRangeExpressions(startDate, endDate, reportPeriod);

            searchDescriptor
               .Size(0)
               .Index(_index)
                   .Query(u => u.Bool(boolContainerGlobal))
                   .Aggregations(agg => agg
                      .MultiTerms("group_by", mt => mt
                        .CollectMode(TermsAggregationCollectMode.BreadthFirst)
                        .Terms(t => t.Field(f => f.TenantId), t => t.Field(f => f.DirectionType.Id))
                        .Size(int.MaxValue)
                        .Aggregations(agg1 => agg1
                            .DateRange("date_range", dRange => dRange
                                .Field(f => f.EventDate)
                                .Ranges(dateRangeExpressions.ToArray())
                                .Aggregations(agg2 => agg2
                                   .Sum("sum_traffic", t => t.Field(f => f.OnTimeIOCount)))))));

#if DEBUG
            var json = this.SearchDescriptorToJson(searchDescriptor);
#endif
            var result = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(result);

            var groupedItems = ((Nest.BucketAggregate)result.Aggregations.First(u => u.Key == "group_by").Value).Items;

            foreach (Nest.KeyedBucket<object> keyedBucket in groupedItems)
            {
                var directionType = DirectionType.FromValue<DirectionType>(Convert.ToInt32(keyedBucket.KeyAsString.Split("|")[1]));

                ComparedStoreSummary cp;

                if (directionType == DirectionType.Inwards)
                {
                    foreach (var dateRange in keyedBucket.DateRange("date_range").Buckets)
                    {
                        cp = new ComparedStoreSummary();
                        cp.Order = DateRangeClassification.FromName(dateRange.Key).Id;
                        cp.CurrentPeriod.Caption = (await _resourceMongoDbRepository.GetAsync(languageId, DateRangeClassification.FromName(dateRange.Key).ResourceKey)).Content;
                        cp.CurrentPeriod.CaptionKey = dateRange.Key;
                        cp.Traffic.InBound.Value = (int)dateRange.Sum("sum_traffic").Value;
                       
                        var founditem = response.FindIndex(m => m.CurrentPeriod.CaptionKey == dateRange.Key);
                        if (founditem >= 0)
                            response[founditem].Traffic.InBound = cp.Traffic.InBound;
                        else
                            response.Add(cp);
                    }
                }
                else if (directionType == DirectionType.Outward)
                {
                    foreach (var dateRange in keyedBucket.DateRange("date_range").Buckets)
                    {
                        cp = new ComparedStoreSummary();
                        cp.Order = DateRangeClassification.FromName(dateRange.Key).Id;
                        cp.CurrentPeriod.Caption = (await _resourceMongoDbRepository.GetAsync(languageId, DateRangeClassification.FromName(dateRange.Key).ResourceKey)).Content;
                        cp.CurrentPeriod.CaptionKey = dateRange.Key;
                        cp.Traffic.OutBound.Value = (int)dateRange.Sum("sum_traffic").Value;

                        var founditem = response.FindIndex(m => m.CurrentPeriod.CaptionKey == dateRange.Key);
                        if (founditem >= 0)
                            response[founditem].Traffic.OutBound = cp.Traffic.OutBound;
                        else
                            response.Add(cp);
                    }
                }
            }

            var compareMapping = this.GetCompareMapping();

            var thisItem = response.FirstOrDefault(m => m.CurrentPeriod.CaptionKey == compareMapping[reportPeriod]);

            var compareCandidates = response.Except(new List<ComparedStoreSummary>() { thisItem });

            response.AddRange(await _comparedStoreSummaryCalculations.GenerateComparedStoreSummaries(compareCandidates, thisItem, tenantId, "", languageId));

            return response.OrderBy(u => u.Order).ToList();
        }

        public async Task<List<ComparedStoreSummary>> TrafficSummaryByStoreAsync(List<Location> locations, DateTime startDate, DateTime endDate, ReportPeriodType reportPeriodType, Guid tenantId, Guid languageId)
        {
            await this.CheckElasticIndexAsync();

            List<ComparedStoreSummary> response = new();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
               .Must(must)
               .MustNot(mustNot)
               .Filter(filter));

            must.Add(m => m
                .Term(t => t.Field(f => f.TenantId).Value(tenantId)));

            mustNot.Add(m => m
                .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Unknown.Id)));

            this.GenerateLocationQuery(locations, ref must);

            var dateRangeExpressions = this.GetDateRangeExpressions(startDate, endDate, reportPeriodType);

            searchDescriptor
              .Size(0)
              .Index(_index)
                  .Query(u => u.Bool(boolContainerGlobal))
                  .Aggregations(agg => agg
                     .MultiTerms("group_by", mt => mt
                        .CollectMode(TermsAggregationCollectMode.BreadthFirst)
                        .Terms(t => t.Field(f => f.AreaId), t => t.Field(f => f.DirectionType.Id))
                        .Size(int.MaxValue)
                        .Aggregations(agg1 => agg1
                        .TopHits("top_hit", t => t
                            .Size(1)
                            .Source(s => s
                            .Includes(i => i.Field(f => f.AreaName))))
                        .DateRange("date_range", dRange => dRange
                            .Field(f => f.EventDate)
                                .Ranges(dateRangeExpressions)
                                .Aggregations(agg2 => agg2
                                    .Sum("sum_traffic", t => t.Field(f => f.OnTimeIOCount))
                                    )))));

#if DEBUG
            var json = this.SearchDescriptorToJson(searchDescriptor);
#endif
            var result = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(result);

            var groupedItems = ((Nest.BucketAggregate)result.Aggregations.First(u => u.Key == "group_by").Value).Items;

            var compareMapping = this.GetCompareMapping();

            List<ComparedStoreSummary> qres = new();

            foreach (Nest.KeyedBucket<object> keyedBucket in groupedItems)
            {
                var storeId = new Guid(keyedBucket.KeyAsString.Split("|")[0]);
                var directionType = DirectionType.FromValue<DirectionType>(Convert.ToInt32(keyedBucket.KeyAsString.Split("|")[1]));
                var storeName = keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().AreaName;

                ComparedStoreSummary cp;
                if (directionType == DirectionType.Inwards)
                {
                    foreach (var dateRange in keyedBucket.DateRange("date_range").Buckets)
                    {
                        cp = new ComparedStoreSummary();
                        cp.Traffic.InBound.Value = (int)dateRange.Sum("sum_traffic").Value;
                        cp.Order = DateRangeClassification.FromName(dateRange.Key).Id;
                        cp.CurrentPeriod.Caption = (await _resourceMongoDbRepository.GetAsync(languageId, DateRangeClassification.FromName(dateRange.Key).ResourceKey)).Content;
                        cp.CurrentPeriod.CaptionKey = dateRange.Key;
                        cp.StoreId = storeId;
                        cp.StoreName = storeName;

                        var founditem = qres.FindIndex(m => m.CurrentPeriod.CaptionKey == dateRange.Key && m.StoreId == storeId);
                        if (founditem >= 0)
                            qres[founditem].Traffic.InBound = cp.Traffic.InBound;
                        else
                            qres.Add(cp);
                    }
                }
                else if (directionType == DirectionType.Outward)
                {
                    foreach (var dateRange in keyedBucket.DateRange("date_range").Buckets)
                    {
                        cp = new ComparedStoreSummary();
                        cp.Traffic.OutBound.Value = (int)dateRange.Sum("sum_traffic").Value; ;
                        cp.Order = DateRangeClassification.FromName(dateRange.Key).Id;
                        cp.CurrentPeriod.Caption = (await _resourceMongoDbRepository.GetAsync(languageId, DateRangeClassification.FromName(dateRange.Key).ResourceKey)).Content;
                        cp.CurrentPeriod.CaptionKey = dateRange.Key;
                        cp.StoreId = storeId;
                        cp.StoreName = storeName;

                        var founditem = qres.FindIndex(m => m.CurrentPeriod.CaptionKey == dateRange.Key && m.StoreId == storeId);
                        if (founditem >= 0)
                            qres[founditem].Traffic.OutBound = cp.Traffic.OutBound;
                        else
                            qres.Add(cp);
                    }
                }
            }

            var query = qres.GroupBy(data => data.StoreId).Select(group => 
                new 
                {
                    Id = group.Key,
                    ComparedStoreSummaries = group.OrderByDescending(x => x.StoreName)
                })
                .OrderBy(group => group.ComparedStoreSummaries.First().StoreName);

            foreach (var item in query)
            {
                response.AddRange(item.ComparedStoreSummaries);

                var thisItem = item.ComparedStoreSummaries.FirstOrDefault(m => m.CurrentPeriod.CaptionKey == compareMapping[reportPeriodType]);
                var compareCandidates = item.ComparedStoreSummaries.Except(new List<ComparedStoreSummary>() { thisItem });
                response.AddRange(await _comparedStoreSummaryCalculations.GenerateComparedStoreSummaries(compareCandidates, thisItem, item.Id, thisItem.StoreName, languageId));
            }

            return response;
        }

        public async Task<List<ComparedStoreSummary>> TrafficByTimePeriodChartAsync(List<Location> locations, DateTime startDate, DateTime endDate, ReportPeriodType reportPeriod, DateRangeType dateRangeType, Guid tenantId, string languageShortName, string languageWindowsTimeZone, string languageLinuxTimeZone)
        {
            await this.CheckElasticIndexAsync();

            List<ComparedStoreSummary> response = new();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            string calendar_interval = reportPeriod switch
            {
                ReportPeriodType.Unknown => throw new NotImplementedException(),
                ReportPeriodType.Weekly => "1D",
                ReportPeriodType.Daily => "1H",
                ReportPeriodType.Monthly => "1D",
                _ => throw new NotImplementedException(),
            };

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
               .Must(must)
               .MustNot(mustNot)
               .Filter(filter));

            must.Add(m => m
              .Term(t => t.Field(f => f.TenantId).Value(tenantId)));

            mustNot.Add(m => m
                .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Outward.Id)));

            this.GenerateLocationQuery(locations, ref must);

            filter.Add(f =>
               f.DateRange(d => d.Field(f => f.EventDate)
                   .GreaterThanOrEquals(startDate.ToUniversalTime())
                   .LessThanOrEquals(endDate.ToUniversalTime())));

            searchDescriptor
               .Size(0)
               .Index(_index)
                   .Query(u => u.Bool(boolContainerGlobal))
                   .Aggregations(a => a
                        .DateHistogram("per_timeperiod", u => u.Field(f => f.EventDate)
                            .Order(HistogramOrder.KeyAscending)
                            .CalendarInterval(new DateMathTime(calendar_interval))
                            .Aggregations(aa => aa
                                .Sum("sum_traffic", t => t.Field(f => f.OnTimeIOCount)))));
#if DEBUG
            var json = this.SearchDescriptorToJson(searchDescriptor);
#endif

            var result = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(result);

            var items = ((Nest.BucketAggregate)result.Aggregations.First(u => u.Key == "per_timeperiod").Value).Items;

            System.Globalization.CultureInfo cultureInfo = new(languageShortName);

            foreach (DateHistogramBucket dateHistogramBucket in items)
            {
                var itemDate = DateTime.Parse(dateHistogramBucket.KeyAsString);

                ComparedStoreSummary comparedStoreSummary = new()
                {
                    CurrentPeriod = new Engine.DateRange()
                    {
                        Type = dateRangeType,
                        From = itemDate,
                        To = reportPeriod switch
                        {
                            ReportPeriodType.Unknown => throw new NotImplementedException(),
                            ReportPeriodType.Daily => itemDate.AddHours(1),
                            ReportPeriodType.Weekly => itemDate.AddDays(1),
                            ReportPeriodType.Monthly => itemDate.AddDays(1),
                            _ => throw new NotImplementedException(),
                        },
                        TimeZoneSettings = _timeZoneSettings,
                        CultureInfo = cultureInfo,
                        WindowsTimeZone = languageWindowsTimeZone,
                        LinuxTimeZone = languageLinuxTimeZone,
                        Caption = null
                    },
                    Traffic = new Traffic
                    {
                        InBound = new ValueObject<double>() { Value = (double)((Nest.ValueAggregate)dateHistogramBucket.Values.First()).Value }
                    }

                };
                response.Add(comparedStoreSummary);
            }

            return response; 
        }

        public async Task<List<ComparedStoreSummary>> TrafficByStoreChartAsync(List<Location> locations, DateTime startDate, DateTime endDate, Guid tenantId, string languageShortName, string languageWindowsTimeZone, string languageLinuxTimeZone)
        {
            await this.CheckElasticIndexAsync();

            List<ComparedStoreSummary> response = new();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
               .Must(must)
               .MustNot(mustNot)
               .Filter(filter));

            must.Add(m => m
                .Term(t => t.Field(f => f.TenantId).Value(tenantId)));

            mustNot.Add(m => m
                .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Outward.Id)));

            this.GenerateLocationQuery(locations, ref must);

            filter.Add(f =>
                f.DateRange(d => d.Field(f => f.EventDate)
                    .GreaterThanOrEquals(startDate.ToUniversalTime())
                    .LessThanOrEquals(endDate.ToUniversalTime())));

            TermsOrderDescriptor<EventCreateElasticData> termsOrderDescriptor = new();
            termsOrderDescriptor.Ascending("sum_traffic");

            searchDescriptor
                .Index(_index)
                .Size(0)
                .Query(u => u.Bool(boolContainerGlobal))
                .Aggregations(a => a
                    .Terms("group_by", t => t.Field(f => f.AreaId)
                        .Size(int.MaxValue)
                        .Order(o => termsOrderDescriptor)
                        .Aggregations(aa => aa
                            .Sum("sum_traffic", t => t.Field(f => f.OnTimeIOCount))
                            .TopHits("top_hit", t => t
                                .Size(1)
                                .Source(s => s
                                    .Includes(i => i.Field(f => f.AreaName)))))));

#if DEBUG
            var json = this.SearchDescriptorToJson(searchDescriptor);
#endif
            var result = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(result);

            var items = ((Nest.BucketAggregate)result.Aggregations.First(u => u.Key == "group_by").Value).Items;

            System.Globalization.CultureInfo cultureInfo = new(languageShortName);

            foreach (Nest.KeyedBucket<object> keyedBucket in items)
            {
                ComparedStoreSummary comparedStoreSummary = new()
                {
                    CurrentPeriod = new Engine.DateRange()
                    {
                        Type = DateRangeType.ByDate,
                        From = startDate,
                        To = endDate,
                        TimeZoneSettings = _timeZoneSettings,
                        CultureInfo = cultureInfo,
                        WindowsTimeZone = languageWindowsTimeZone,
                        LinuxTimeZone = languageLinuxTimeZone,
                    },
                    Traffic = new Traffic
                    {
                        InBound = new ValueObject<double>() { Value = (double)keyedBucket.Sum("sum_traffic").Value }
                    },
                    StoreName = keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().AreaName,
                    StoreId = new Guid(keyedBucket.Key.ToString()),
                };

                response.Add(comparedStoreSummary);
            }
            return response;
        }

        public async Task<List<ComparedStoreSummary>> TrafficSummaryByStoreAndTimePeriodAsync(List<Location> locations, DateTime startDate, DateTime endDate, DateRangeType dateRangeType, Guid tenantId, string languageShortName, string languageWindowsTimeZone, string languageLinuxTimeZone)
        {
            await this.CheckElasticIndexAsync();

            List<ComparedStoreSummary> response = new();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
               .Must(must)
               .MustNot(mustNot)
               .Filter(filter));

            must.Add(m => m
                .Term(t => t.Field(f => f.TenantId).Value(tenantId)));

            mustNot.Add(m => m
                .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Outward.Id)));

            this.GenerateLocationQuery(locations, ref must);

            filter.Add(f =>
                f.DateRange(d => d.Field(f => f.EventDate)
                    .GreaterThanOrEquals(startDate.ToUniversalTime())
                    .LessThanOrEquals(endDate.ToUniversalTime())));

            TermsOrderDescriptor<EventCreateElasticData> termsOrderDescriptor = new();
            termsOrderDescriptor.Ascending("sum_traffic");

            searchDescriptor
                .Index(_index)
                .Size(0)
                .Query(u => u.Bool(boolContainerGlobal))
                .Aggregations(a => a
                    .Terms("group_by", t => t.Field(f => f.AreaId)
                    .Size(int.MaxValue)
                        .Aggregations(aa => aa
                             .TopHits("top_hit", t => t
                                    .Size(1)
                                    .Source(s => s
                                        .Includes(i => i.Field(f => f.AreaName))))
                            .DateHistogram("per_timeperiod", u => u.Field(f => f.EventDate)
                            .CalendarInterval(new DateMathTime("1D"))
                                .Aggregations(aaa => aaa
                                    .Sum("sum_traffic", t => t.Field(f => f.OnTimeIOCount))
                                   ))))
                   );

#if DEBUG
            var json = this.SearchDescriptorToJson(searchDescriptor);
#endif
            var result = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(result);

            var items = ((Nest.BucketAggregate)result.Aggregations.First(u => u.Key == "group_by").Value).Items;

            System.Globalization.CultureInfo cultureInfo = new(languageShortName);

            foreach (Nest.KeyedBucket<object> keyedBucket in items)
            {
                var storeId = new Guid(keyedBucket.Key.ToString());
                var storeName = keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().AreaName;

                foreach (DateHistogramBucket dateHistogramBucket in keyedBucket.DateHistogram("per_timeperiod").Buckets)
                {
                    var itemDate = DateTime.Parse(dateHistogramBucket.KeyAsString);

                    var inbound = (double)dateHistogramBucket.Sum("sum_traffic").Value;

                    ComparedStoreSummary comparedStoreSummary = new()
                    {
                        CurrentPeriod = new Engine.DateRange()
                        {
                            Type = DateRangeType.ByDate,
                            From = itemDate,
                            To = itemDate.AddDays(1), //Suanda kullanilmiyor rapor kisminda
                            TimeZoneSettings = _timeZoneSettings,
                            CultureInfo = cultureInfo,
                            WindowsTimeZone = languageWindowsTimeZone,
                            LinuxTimeZone = languageLinuxTimeZone,
                        },
                        Traffic = new Traffic
                        {
                            InBound = new ValueObject<double>() { Value = inbound }
                        },
                        StoreName = storeName,
                        StoreId = storeId,
                    };

                    response.Add(comparedStoreSummary);
                }
            }

            return response;
        }



        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {

                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion



        #region private methods
        private async Task CheckElasticIndexAsync()
        {
            if (!(await _client.Indices.ExistsAsync(_index)).Exists)
                throw new ElasticIndexNotFoundException(_index);
        }

        private string SearchDescriptorToJson(SearchDescriptor<EventCreateElasticData> searchDescriptor)
        {
            string jsonQuery = _client.RequestResponseSerializer.SerializeToString(searchDescriptor);

            _logger.LogInformation($"Elastic Query: {jsonQuery}");
            return jsonQuery;
        }

        private void GenerateLocationQuery(List<Location> locations, ref List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>> must)
        {
            var should = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerInner = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
                .Should(should));

            foreach (var location in locations)
            {
                switch (location.LocationLevel)
                {
                    case (LocationLevel.Tenant):
                        break;

                    case (LocationLevel.Company):
                        should.Add(s => s.Term(new Field("companyId"), location.Id));
                        break;

                    case (LocationLevel.Country):
                        should.Add(s => s.Term(new Field("countryId"), location.Id));
                        break;

                    case (LocationLevel.Region):
                        should.Add(s => s.Term(new Field("regionId"), location.Id));
                        break;

                    case (LocationLevel.Area):
                        should.Add(s => s.Term(new Field("areaId"), location.Id));
                        break;

                    case (LocationLevel.SubArea):
                        should.Add(s => s.Term(new Field("subAreaId"), location.Id));
                        break;

                    case (LocationLevel.Device):
                        should.Add(s => s.Term(new Field("deviceId"), location.Id));
                        break;
                }
            }

            must.Add(m => m.Bool(boolContainerInner));
        }

        private void ValidateElasticResult(ISearchResponse<EventCreateElasticData> response)
        {
            if (!response.IsValid)
                throw new ElasticSearchQueryException(_index, response.ServerError.Status, response.ServerError.Error.Type, response.OriginalException);
        }

        private  IDateRangeExpression[] GetDateRangeExpressions(DateTime startDate, DateTime endDate, ReportPeriodType reportPeriodType)
        {
            var lastYearRange = DateCalculations.ToDateRange(startDate, endDate, reportPeriodType, SummaryCompareType.ToLastYear);
            var lastLastYearRange = DateCalculations.ToDateRange(startDate, endDate, reportPeriodType, SummaryCompareType.ToLastLastYear);

            List<IDateRangeExpression> dateRangeExpressions = new()
            {
                new DateRangeExpression()
                {
                    Key = DateRangeClassification.LastYear.Name,
                    From = lastYearRange.From.ToUniversalTime(),
                    To = lastYearRange.To.ToUniversalTime()
                },
                new DateRangeExpression()
                {
                    Key = DateRangeClassification.LastLastYear.Name,
                    From = lastLastYearRange.From.ToUniversalTime(),
                    To = lastLastYearRange.To.ToUniversalTime()
                }
            };

            if (reportPeriodType == ReportPeriodType.Daily)
            {
                var lastWeekRange = DateCalculations.ToDateRange(startDate, endDate, reportPeriodType, SummaryCompareType.ToLastWeek);

                List<IDateRangeExpression> DailyExtendedDateRangeExpressions = new()
                {
                    new DateRangeExpression()
                    {
                        Key = DateRangeClassification.Today.Name,
                        From = startDate.ToUniversalTime(),
                        To = endDate.ToUniversalTime()
                    },
                    new DateRangeExpression()
                    {
                        Key = DateRangeClassification.LastWeek.Name,
                        From = lastWeekRange.From.ToUniversalTime(),
                        To = lastWeekRange.To.ToUniversalTime()
                    },
                };
                dateRangeExpressions.AddRange(DailyExtendedDateRangeExpressions);

            }
            else if (reportPeriodType == ReportPeriodType.Weekly)
            {
                var lastWeekRange = DateCalculations.ToDateRange(startDate, endDate, reportPeriodType, SummaryCompareType.ToLastWeek);

                List<IDateRangeExpression> WeeklyExtendedDateRangeExpressions = new()
                {
                    new DateRangeExpression()
                    {
                        Key = DateRangeClassification.ThisWeek.Name,
                        From = startDate.ToUniversalTime(),
                        To = endDate.ToUniversalTime()
                    },
                    new DateRangeExpression()
                    {
                        Key = DateRangeClassification.LastWeek.Name,
                        From = lastWeekRange.From.ToUniversalTime(),
                        To = lastWeekRange.To.ToUniversalTime()
                    },
                };
                dateRangeExpressions.AddRange(WeeklyExtendedDateRangeExpressions);

            }
            else if (reportPeriodType == ReportPeriodType.Monthly)
            {
                var lastMonthRange = DateCalculations.ToDateRange(startDate, endDate, reportPeriodType, SummaryCompareType.ToLastMonth);
                List<IDateRangeExpression> MonthlyExtendedDateRangeExpressions = new()
                {
                    new DateRangeExpression()
                    {
                        Key = DateRangeClassification.ThisMonth.Name,
                        From = startDate.ToUniversalTime(),
                        To = endDate.ToUniversalTime()
                    },
                    new DateRangeExpression()
                    {
                        Key = DateRangeClassification.LastMonth.Name,
                        From = lastMonthRange.From.ToUniversalTime(),
                        To = lastMonthRange.To.ToUniversalTime()
                    }
                };
                dateRangeExpressions.AddRange(MonthlyExtendedDateRangeExpressions);
            }

            return dateRangeExpressions.ToArray();
        }

        private Dictionary<ReportPeriodType, string> GetCompareMapping()
        {
            return new Dictionary<ReportPeriodType, string>()
            {
                {ReportPeriodType.Daily, DateRangeClassification.Today.Name },
                {ReportPeriodType.Weekly,DateRangeClassification.ThisWeek.Name },
                {ReportPeriodType.Monthly, DateRangeClassification.ThisMonth.Name },
            };
        }

        #endregion private methods
    }
}
