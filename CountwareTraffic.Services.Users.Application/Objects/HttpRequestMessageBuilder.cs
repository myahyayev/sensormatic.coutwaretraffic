﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.Objects
{
    public class HttpRequestMessageBuilder
    {
        private readonly HttpRequestMessage _httpRequestMessage;
        private readonly IOptions<AuthenticationConfig> _options;

        private const string ACCESS_TOKEN = "access_token";
        private const string AUTHENTICATION_HEADER = "Bearer";

        public HttpRequestMessageBuilder(IOptions<AuthenticationConfig> options)
        {
            _httpRequestMessage = new HttpRequestMessage();
            _options = options;
        }

        public HttpRequestMessageBuilder AddMethod(HttpMethod httpMethod)
        {
            _httpRequestMessage.Method = httpMethod;

            return this;
        }

        public HttpRequestMessageBuilder AddRequestUri(string requestUri)
        {
            var uriBuilder = new StringBuilder();
            uriBuilder.Append(_options.Value?.Authority);
            uriBuilder.Append('/');
            uriBuilder.Append(requestUri);

            _httpRequestMessage.RequestUri = new System.Uri(uriBuilder.ToString());

            return this;
        }

        public HttpRequestMessageBuilder AddAcceptLanguage(string language)
        {
            _httpRequestMessage.Headers.AcceptLanguage.Add(new StringWithQualityHeaderValue(language));

            return this;
        }

        public HttpRequestMessageBuilder AddContent(object @object,
                                                    Encoding encoding,
                                                    string mediaType = "application/json")
        {
            var jsonRequest = TextJsonExtensions.Serialize(@object);

            var content = new StringContent(jsonRequest, encoding, mediaType);

            _httpRequestMessage.Content = content;

            return this;
        }

        public HttpRequestMessageBuilder AddAccessToken(HttpContext httpContext)
        {
            string accessToken = httpContext.GetTokenAsync(ACCESS_TOKEN).GetAwaiter().GetResult();

            _httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue(AUTHENTICATION_HEADER,
                                                                                       accessToken);

            return this;
        }

        public HttpRequestMessage Build()
        {
            return _httpRequestMessage;
        }
    }
}
