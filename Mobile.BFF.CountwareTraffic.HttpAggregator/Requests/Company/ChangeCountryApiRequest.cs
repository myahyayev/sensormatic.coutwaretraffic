﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ChangeCountryApiRequest : SensormaticRequestValidate
    {
        public Guid CompanyId { get; set; }
        public Guid CountryLookupId { get; set; }
        public string Iso { get; set; }
        public string Iso3 { get; set; }
        public int IsoNumeric { get; set; }
        public string Capital { get; set; }
        public string ContinentCode { get; set; }
        public string CurrencyCode { get; set; }
        public string Name { get; set; }
        public override void Validate()
        {
            if (CompanyId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1181 CompanyId cannot be null", nameof(CompanyId)));

            if (CountryLookupId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1182 CountryLookupId cannot be null", nameof(CountryLookupId)));

            if (Name.IsNullOrWhiteSpace() || Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1183 Name invalid format", nameof(Name)));

            if (Iso.IsNullOrWhiteSpace() || Iso.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1184 Iso invalid format", nameof(Iso)));

            if (Iso3.IsNullOrWhiteSpace() || Iso3.Length != 3)
                ValidateResults.Add(new ErrorResult($"#E1185 Iso3 invalid format", nameof(Iso3)));

            if (IsoNumeric < 1)
                ValidateResults.Add(new ErrorResult($"#E1186 IsoNumeric must be above zero.", nameof(IsoNumeric)));

            if (Capital.IsNullOrWhiteSpace() || Capital.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1187 Capital invalid format", nameof(Capital)));

            if (!ContinentCode.IsNullOrWhiteSpace() && ContinentCode.Length > 5)
                ValidateResults.Add(new ErrorResult($"#E1188 ContinentCode invalid format", nameof(ContinentCode)));

            if (!CurrencyCode.IsNullOrWhiteSpace() && CurrencyCode.Length != 3)
                ValidateResults.Add(new ErrorResult($"#E1189 CurrencyCode invalid format", nameof(CurrencyCode)));
        }
    }
}
