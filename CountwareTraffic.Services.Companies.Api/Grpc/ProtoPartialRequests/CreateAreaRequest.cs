﻿using CountwareTraffic.Services.Companies.Api;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class CreateAreaRequest : SensormaticRequestValidate
    {
        internal Guid _RegionId
        {
            get
            {
                if (Guid.TryParse(regionId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.regionId_ = value.ToString(); }
        }

        internal Guid? _ManagerId
        {
            get
            {
                if (Guid.TryParse(managerId_, out Guid id))
                    return id;

                return null;
            }
            set { this.managerId_ = value.ToString(); }
        }

        internal Guid _CityId
        {
            get
            {
                if (Guid.TryParse(cityId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.cityId_ = value.ToString(); }
        }

        internal Guid _DistrictId
        {
            get
            {
                if (Guid.TryParse(districtId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.districtId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_RegionId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1251 RegionId cannot be null", nameof(_RegionId)));

            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1252 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1253 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1254 Description invalid format", nameof(Description)));

            if (!ManagerName.IsNullOrWhiteSpace() && ManagerName.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1255 ManagerName invalid format", nameof(ManagerName)));

            if ( AreaTypeId < 1)
                ValidateResults.Add(new ErrorResult($"#E1256 AreaTypeId invalid format", nameof(AreaTypeId)));

            if (!Street.IsNullOrWhiteSpace() && Street.Length > 250)
                ValidateResults.Add(new ErrorResult($"#E1257 Street invalid format", nameof(Street)));

            if (!EmailAddress.IsNullOrWhiteSpace() && (!EmailValidator.EmailIsValid(EmailAddress) || EmailAddress.Length > 120))
                ValidateResults.Add(new ErrorResult($"#E1258 Email address invalid format", nameof(EmailAddress)));

            if (!PhoneNumber.IsNullOrWhiteSpace() && PhoneNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1259 PhoneNumber invalid format", nameof(PhoneNumber)));

            if (!GsmNumber.IsNullOrWhiteSpace() && GsmNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1260 GsmNumber invalid format", nameof(GsmNumber)));

            if (!GsmDialCode.IsNullOrWhiteSpace() && GsmDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1261 GsmDialCode invalid format", nameof(GsmDialCode)));

            if (!PhoneDialCode.IsNullOrWhiteSpace() && PhoneDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1262 PhoneDialCode invalid format", nameof(PhoneDialCode)));

            if (!GsmCountryCode.IsNullOrWhiteSpace() && GsmCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1263 GsmCountryCode invalid format", nameof(GsmCountryCode)));

            if (!PhoneCountryCode.IsNullOrWhiteSpace() && PhoneCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1264 PhoneCountryCode invalid format", nameof(PhoneCountryCode)));
        }
    }
}
