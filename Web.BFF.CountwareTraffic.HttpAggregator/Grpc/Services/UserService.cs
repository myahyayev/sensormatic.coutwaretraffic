﻿using CountwareTraffic.Services.Users.Grpc;
using Web.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class UserService : IScopedSelfDependency
    {
        private readonly User.UserClient _userClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;

        public UserService(User.UserClient userClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _userClient = userClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<IEnumerable<GetTenantDetails>> GetTenantsAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_userClient.GetTenantsAsync, grpcRequest, hasClientSideLog: false);


            return grpcResponse.Tenants.Select(tenant => new GetTenantDetails
            {
                TenantId = new Guid(tenant.TenantId),
                Name = tenant.Name,
                Description = tenant.Description
            });

        }

        public async Task<TokenApiResponse> GetTokenAsync(TokenApiRequest request)
        {
            GetTokenRequest grpcRequest = new()
            {
                Password = request.Password,
                UserName = request.UserName
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_userClient.GetTokenAsync, grpcRequest, hasClientSideLog: false);

            TokenApiResponse tokenResponse = new()
            {
                LoginStatus = grpcResponse.LoginStatus switch
                {
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.Unknown => LoginStatus.Unknown,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.FirstLogin => LoginStatus.FirstLogin,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.AlreadyLogged => LoginStatus.AlreadyLogged,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.AuthorityNull => LoginStatus.AuthorityNull,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.VerifyToken => LoginStatus.VerifyToken,
                    _ => throw new NotImplementedException(),
                }
            };


            tokenResponse.VerifyToken = grpcResponse.VerifyToken != null
                ?
                new VerifyTokenModel
                {
                    Authority = grpcResponse.VerifyToken.Authority,
                    VerifyToken = grpcResponse.VerifyToken.VerifyToken,
                    TimeOut = grpcResponse.VerifyToken.TimeOut,
                    Parameters = grpcResponse.VerifyToken.Parameters.Select(u => u).ToArray()
                }
                : null;



            tokenResponse.AuthorityNull = grpcResponse.AuthorityNull != null
                ?
                new AuthorityNullModel
                {
                    AuthToken = grpcResponse.AuthorityNull.AuthToken
                }
                : null;


            tokenResponse.AlreadyLogged = grpcResponse.AlreadyLogged != null
                ?
                new AlreadyLoggedModel
                {
                    AccessToken = grpcResponse.AlreadyLogged.AccessToken,
                    Email = grpcResponse.AlreadyLogged.Email,
                    ExpireTime = grpcResponse.AlreadyLogged.ExpireTime.ToDateTimeOffset().LocalDateTime,
                    FamilyName = grpcResponse.AlreadyLogged.FamilyName,
                    UniqueName = grpcResponse.AlreadyLogged.UniqueName,
                    PhoneNumber = grpcResponse.AlreadyLogged.PhoneNumber,
                    GivenName = grpcResponse.AlreadyLogged.GivenName,
                    Gender = grpcResponse.AlreadyLogged.Gender switch
                    {
                        Gender.Male => (int)Gender.Male,
                        Gender.Femail => (int)Gender.Femail,
                        _ => throw new NotImplementedException()
                    }
                }
                : null;


            tokenResponse.FirstLogin = grpcResponse.FirstLogin != null
                ? new FirstLoginModel
                {
                    Message = grpcResponse.FirstLogin.Message
                }
                : null;

            return tokenResponse;
        }

        public async Task<ResetPasswordApiResponse> ResetPasswordWithTokenAsync(string userName, string token, ResetPasswordWithTokenApiRequest request)
        {
            ResetPasswordWithTokenRequest grpcRequest = new()
            {
                Token = token,
                UserName = userName,
                Password = request.Password,
                ConfirmPassword = request.ConfirmPassword
            };

            var grpcResponse = await _asyncUnaryCallHandler
             .CallMethodAsync(_userClient.ResetPasswordWithTokenAsync, grpcRequest, hasClientSideLog: false);

            return new ResetPasswordApiResponse
            {
                Message = grpcResponse.Message
            };
        }

        public async Task<ResetPasswordApiResponse> ResetPasswordAsync(string userName, ResetPasswordApiRequest request)
        {
            ResetPasswordRequest grpcRequest = new()
            {
                ConfirmPassword = request.ConfirmPassword,
                CurrentPassword = request.CurrentPassword,
                NewPassword = request.NewPassword,
                UserName = userName
            };

            var grpcResponse = await _asyncUnaryCallHandler
             .CallMethodAsync(_userClient.ResetPasswordAsync, grpcRequest, hasClientSideLog: false);

            return new ResetPasswordApiResponse
            {
                Message = grpcResponse.Message
            };
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
