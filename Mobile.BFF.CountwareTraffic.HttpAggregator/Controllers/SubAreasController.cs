﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class SubAreasController : BaseController
    {
        private readonly SubAreaService _subAreaService;
        private readonly DeviceService _deviceService;
        public SubAreasController(IServiceProvider provider, ILogger<SubAreasController> logger, IHttpContextAccessor contextAccessor, SubAreaService subAreaService, DeviceService deviceService) 
            : base(provider, logger, contextAccessor)
        {
            _subAreaService = subAreaService;
            _deviceService = deviceService;
        }

        [HttpGet("{subAreaId}/detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminSubAreaView, Permissions.SubAreaView })]
        public async Task<ApiResponse<GetSubAreaDetails>> GetSubAreaDetailAsync(Guid subAreaId)
            => Response(await _subAreaService.GetSubAreaByIdAsync(subAreaId), new ResultMessage("#S1123"));
           

        [HttpPost("{areaId}/details"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminSubAreaList })]
        public async Task<ApiPagedResponse<GetSubAreaDetails>> GetSubAreaDetailsAsync(Guid areaId, [FromBody] DataSourceApiRequest paging)
            => await _subAreaService.GetSubAreasAsync(areaId, paging);


        [HttpPost("{areaId}/add"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminSubAreaAdd })]
        public async Task<ApiResponse<CrudApiResponse>> AddSubAreaAsync(Guid areaId, [FromForm] AddSubAreaApiRequest request)
        {
            await _subAreaService.AddSubAreaAsync(areaId, request);

            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Create }, new ResultMessage()
            {
                CallToActionType = CallToActionType.Ok,
                Description = $"#S1124 Subarea has been created.",
                Title = "#S1124 Subarea has been created."
            });
        }

        [HttpPut("{subAreaId}/change"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminSubAreaChange })]
        public async Task<ApiResponse<CrudApiResponse>> ChangeSubAreaAsync(Guid subAreaId, [FromForm] ChangeSubAreaApiRequest request)
        {
            await _subAreaService.ChangeSubAreaAsync(subAreaId, request);

            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Update, Id = subAreaId }, new ResultMessage()
            {
                CallToActionType = CallToActionType.Ok,
                Description = $"#S1125 Subarea with {subAreaId} id has been updated.",
                Title = "#S1125 Subarea with {subAreaId} id has been updated."
            });
        }


        [HttpDelete("{subAreaId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminSubAreaDelete })]
        public async Task<ApiResponse<CrudApiResponse>> DeleteSubAreaAsync(Guid subAreaId)
        {
            var subAreaDeviceControl = await _deviceService.GetDevicesAsync(subAreaId, new DataSourceApiRequest());

            if (subAreaDeviceControl.TotalCount > 0)
            {
                return Response(new CrudApiResponse {  IsSuccess = false,  OperationType = OperationType.Delete, Id = subAreaId }, new ResultMessage()
                {
                    CallToActionType = CallToActionType.GoToDevices,
                    Description = $"#W1031 A device has been defined in the SubaArea with id {subAreaId}.",
                    Title = $"#W1031 SubArea with id {subAreaId} cannot be deleted."
                });
            }

            await _subAreaService.DeleteSubAreaAsync(subAreaId);

            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Delete, Id = subAreaId }, new ResultMessage()
            {
                CallToActionType = CallToActionType.Ok,
                Description = $"#S1126 Subarea with {subAreaId} id has been deleted.",
                Title = $"#S1126 Subarea with {subAreaId} id has been deleted."
            });
        }
    }
}
