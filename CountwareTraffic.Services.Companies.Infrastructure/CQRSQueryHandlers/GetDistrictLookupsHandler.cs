﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetDistrictLookupsHandler : IQueryHandler<GetDistrictLookups, List<DistrictLookupDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetDistrictLookupsHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<List<DistrictLookupDetailsDto>> HandleAsync(GetDistrictLookups query)
        {
            var districLookups = await _unitOfWork.GetRepository<IDistrictLookupRepository>().GetAllAsync(query.CityId);

            if (districLookups == null) return null;

            return districLookups.Select(districtLookup => new DistrictLookupDetailsDto
            {
                Id = districtLookup.Id,
                Name = districtLookup.Name,
                CityId = districtLookup.CityId
            }).ToList();
        }
    }
}
