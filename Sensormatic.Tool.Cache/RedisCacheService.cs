﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sensormatic.Tool.Cache
{
    public class RedisCacheService : ICache
    {
        private readonly Lazy<ConnectionMultiplexer> _lazyConnection;
        private readonly ILogger<RedisCacheService> _logger;
        private readonly IDatabase _db;
        public RedisCacheService(IConfiguration configuration, ILogger<RedisCacheService> logger)
        {
            _lazyConnection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(configuration.GetConnectionString("RedisConnection")));
            _logger = logger;
            _db = Connection.GetDatabase();
        }

        private ConnectionMultiplexer Connection => _lazyConnection.Value;

        public T GetConnection<T>()
        {
            return (T)Convert.ChangeType(Connection, typeof(T));
        }

        public T Get<T>(string key)
        {


            string redisValue = _db.StringGet(key);

            if (!string.IsNullOrWhiteSpace(redisValue))
                return redisValue.Deserialize<T>();

            return default;
        }

        public async Task<T> GetAsync<T>(string key)
        {

            string redisValue = await _db.StringGetAsync(key);

            if (!string.IsNullOrWhiteSpace(redisValue))
                return redisValue.Deserialize<T>();

            return default;
        }
        public bool KeyExists(string key)
        {
            return _db.KeyExists(key);
        }
        public async Task<bool> KeyExistsAsync(string key)
        {
            return await _db.KeyExistsAsync(key);
        }
        /// <summary>
        /// Add a value to a set if value doesn't present already
        /// </summary>
        /// <typeparam name="T">value type</typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="callback">invoked if add is success</param>
        /// <param name="expireTime">will be set if key already not exists</param>
        /// <returns>true if set doesn't have value already</returns>
        public async Task<bool> AddSetAsync<T>(string key, T value, Action callback, TimeSpan? expireTime = default)
        {
            bool keyAlreadyExists = await _db.KeyExistsAsync(key);

            // Add the value to the Redis Set with the user ID as the key
            bool added = await _db.SetAddAsync(key, value.Serialize());

            if (added)
            {
                // If the value was added to the set, process the request
                callback?.Invoke();
                // Set the expiration time 
                if (expireTime != default && !keyAlreadyExists)
                    await _db.KeyExpireAsync(key, expireTime);

                return true;
            }
            else
            {
#if DEBUG
                // If the value already exists in the set, ignore the request
                _logger.LogWarning("Duplicate Set item ignored. {0}:{1}", key, value);
#endif
                return false;
            }
        }
        /// <summary>
        /// Add a value to a set if value doesn't present already
        /// </summary>
        /// <typeparam name="T">value type</typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="callback">invoked if add is success</param>
        /// <param name="expireTime">will be set if key already not exists</param>
        /// <returns>true if set doesn't have value already</returns>
        public bool AddSet<T>(string key, T value, Action callback, TimeSpan? expireTime = default)
        {
            bool keyAlreadyExists =  _db.KeyExists(key);
           
            // Add the value to the Redis Set with the user ID as the key
            bool added =  _db.SetAdd(key, value.Serialize());

            if (added)
            {
                // If the value was added to the set, process the request
                callback?.Invoke();
                // Set the expiration time 
                if (expireTime != default && !keyAlreadyExists)
                     _db.KeyExpire(key, expireTime);

                return true;
            }
            else
            {
#if DEBUG
                // If the value already exists in the set, ignore the request
                _logger.LogWarning("Duplicate Set item ignored. {0}:{1}", key, value);
#endif
                return false;
            }
        }
        public async Task<TimeSpan> GetKeyTTLAsync(string key) { return await _db.KeyTimeToLiveAsync(key) ?? default; }
        public TimeSpan GetKeyTTL(string key) { return  _db.KeyTimeToLive(key) ?? default; }
        public bool Add<T>(string key, T value, TimeSpan? expireTime = null, bool @override = true)
        {

            if (value != null)
            {
                var redisValue = value.Serialize();
                return _db.StringSet(key, redisValue, expireTime, @override ? When.Always : When.NotExists);
            }

            return false;
        }

        public async Task<bool> AddAsync<T>(string key, T value, TimeSpan? expireTime = null, bool @override = true)
        {

            if (value != null)
            {
                var redisValue = value.Serialize();
                return await _db.StringSetAsync(key, redisValue, expireTime, @override ? When.Always : When.NotExists);
            }

            return false;
        }
        public async Task<T> GetOrAddAsync<T>(string key, T value, TimeSpan? expireTime = null, bool @override = true)

        {
            var result = await GetAsync<T>(key);

            if (EqualityComparer<T>.Default.Equals(result, default(T)))
            {

                await AddAsync(key, value, expireTime, @override);
            }

            return result;
        }
        public T GetOrAdd<T>(string key, T value, TimeSpan? expireTime = null, bool @override = true)
        {
            var result = Get<T>(key);
            if (EqualityComparer<T>.Default.Equals(result, default(T)))
            {
                Add(key, value, expireTime, @override);
            }
            return result;
        }
        public bool Delete(string key)
        {
            return _db.KeyDelete(key);
        }

        public async Task<bool> DeleteAsync(string key)
        {
            return await _db.KeyDeleteAsync(key);
        }

        public List<T> GetList<T>(string key, long start = 0, long stop = -1)
        {

            var redisValues = _db.ListRange(key, start, stop);

            var result = new List<T>();

            if (redisValues.Length > 0)
            {
                foreach (string redisValue in redisValues)
                {
                    result.Add(redisValue.Deserialize<T>());
                }
            }

            return result;
        }

        public async Task<List<T>> GetListAsync<T>(string key, long start = 0, long stop = -1)
        {

            var redisValues = await _db.ListRangeAsync(key, start, stop);

            var result = new List<T>();

            if (redisValues.Length > 0)
            {
                foreach (string redisValue in redisValues)
                {
                    result.Add(redisValue.Deserialize<T>());
                }
            }

            return result;
        }

        public T GetListItem<T>(string key, long index)
        {

            string redisValue = _db.ListGetByIndex(key, index);

            if (!string.IsNullOrWhiteSpace(redisValue))
                return redisValue.Deserialize<T>();

            return default(T);
        }

        public async Task<T> GetListItemAsync<T>(string key, long index)
        {

            string redisValue = await _db.ListGetByIndexAsync(key, index);

            if (!string.IsNullOrWhiteSpace(redisValue))
                return redisValue.Deserialize<T>();

            return default(T);
        }

        public void AddList<T>(string key, T value)
        {

            if (value != null)
            {
                var redisValue = value.Serialize();
                _db.ListRightPush(key, redisValue);
            }
        }

        public async Task AddListAsync<T>(string key, T value)
        {

            if (value != null)
            {
                var redisValue = value.Serialize();
                await _db.ListRightPushAsync(key, redisValue);
            }
        }

        public void AddRangeList<T>(string key, List<T> values)
        {

            RedisValue[] redisValues = new RedisValue[values.Count];

            if (values != null)
            {
                for (int i = 0; i < values.Count; i++)
                {
                    var redisValue = values[i].Serialize();
                    redisValues[i] = redisValue;
                }

                _db.ListRightPush(key, redisValues);
            }
        }

        public async Task AddRangeListAsync<T>(string key, List<T> values)
        {

            RedisValue[] redisValues = new RedisValue[values.Count];

            if (values != null)
            {
                for (int i = 0; i < values.Count; i++)
                {
                    var redisValue = values[i].Serialize();
                    redisValues[i] = redisValue;
                }

                await _db.ListRightPushAsync(key, redisValues);
            }
        }

        public void DeleteListItem<T>(string key, T value)
        {

            var redisValue = value.Serialize();
            _db.ListRemoveAsync(key, redisValue);
        }

        public async Task DeleteListItemAsync<T>(string key, T value)
        {

            var redisValue = value.Serialize();
            await _db.ListRemoveAsync(key, redisValue);
        }

        public long IncrementString(string key, int value = 1)
        {
            return _db.StringIncrement(key, value);
        }

        public async Task<long> IncrementStringAsync(string key, int value = 1)
        {
            return await _db.StringIncrementAsync(key, value);
        }

        public long DecrementString(string key, int value = 1)
        {
            return _db.StringDecrement(key, value);
        }

        public async Task<long> DecrementStringAsync(string key, int value = 1)
        {
            return await _db.StringDecrementAsync(key, value);
        }

        public bool LockTake(string key, string value, TimeSpan expiryTime)
        {
            return _db.LockTake(key, value, expiryTime);
        }

        public async Task<bool> LockTakeAsync(string key, string value, TimeSpan expiryTime)
        {
            return await _db.LockTakeAsync(key, value, expiryTime);
        }

        public bool LockRelease(string key, string value)
        {
            return _db.LockRelease(key, value);
        }

        public async Task<bool> LockReleaseAsync(string key, string value)
        {
            return await _db.LockReleaseAsync(key, value);
        }
    }
}
