﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public class DeviceIsNotActiveException : AppException
    {
        public string DeviceName { get; }
        public Guid Id { get; }

        /// <summary>
        ///it will be evaluated as 200 ok http code by devices to not send same data again
        /// </summary>
        /// <param name="deviceName"></param>
        /// <param name="deviceId"></param>
        public DeviceIsNotActiveException(string deviceName,Guid deviceId)
            : base(new List<ErrorResult>() { new ErrorResult($"Device :{deviceName}-{deviceId} might is not in active status!") } , 200, ResponseMessageType.Warning)
        {
            DeviceName=deviceName;
            Id=deviceId;
        }
    }
}
