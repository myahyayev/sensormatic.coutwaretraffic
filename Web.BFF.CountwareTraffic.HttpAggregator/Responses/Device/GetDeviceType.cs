﻿namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class GetDeviceType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
