﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetWidgetPerformingHandler : IQueryHandler<GetWidgetPerforming, WidgetPerformingDto>
    {
        private readonly IEventElasticSearchRepository _eventElasticSearchRepository;
        private readonly ILogger<GetWidgetPerformingHandler> _logger;
        public GetWidgetPerformingHandler(IEventElasticSearchRepository eventElasticSearchRepository, ILogger<GetWidgetPerformingHandler> logger)
        {
            _eventElasticSearchRepository = eventElasticSearchRepository;
            _logger = logger;
        }

        public async Task<WidgetPerformingDto> HandleAsync(GetWidgetPerforming query)
            => await _eventElasticSearchRepository.GetWidgetPerformingAsync(query.NodeFilters, query.CurrentStartDate, query.CurrentEndDate, query.PreviusStartDate, query.PreviusEndDate);
    }
}
