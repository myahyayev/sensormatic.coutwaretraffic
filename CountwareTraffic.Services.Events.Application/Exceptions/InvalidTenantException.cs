﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public class InvalidTenantException : AppException
    {
        public string Message { get; set; }
        public InvalidTenantException(string message)
            : base(new List<ErrorResult>() { new ErrorResult(message) }, 500, ResponseMessageType.Error)
        {
            Message = message;
        }
    }
}
