﻿using Sensormatic.Tool.Common;
using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class DeviceDto
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public int ControlFrequency { get; set; }
        public bool IsActive { get; set; }
        public TimeSpan? AreaWorkingHoursStart { get; set; }
        public TimeSpan? AreaWorkingHoursEnd { get; set; }
        public TimeZoneValues AreaWorkingTimeZone { get; set; }
    }
}
