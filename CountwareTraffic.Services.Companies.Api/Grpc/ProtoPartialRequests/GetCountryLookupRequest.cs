﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    public sealed partial class GetCountryLookupRequest : SensormaticRequestValidate
    {
        internal Guid _CountryId
        {
            get
            {
                if (Guid.TryParse(id_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.id_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_CountryId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1318 CountryId cannot be null", nameof(_CountryId)));
        }
    }
}
