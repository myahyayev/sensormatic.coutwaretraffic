﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleWeeklyRepeatOn : Enumeration
    {
        public static ScheduleWeeklyRepeatOn Unknown = new(1, nameof(Unknown));
        public static ScheduleWeeklyRepeatOn Sunday = new(2, nameof(Sunday));
        public static ScheduleWeeklyRepeatOn Monday = new(3, nameof(Monday));
        public static ScheduleWeeklyRepeatOn Tuesday = new(4, nameof(Tuesday));
        public static ScheduleWeeklyRepeatOn Wednesday = new(5, nameof(Wednesday));
        public static ScheduleWeeklyRepeatOn Thursday = new(6, nameof(Thursday));
        public static ScheduleWeeklyRepeatOn Friday = new(7, nameof(Friday));
        public static ScheduleWeeklyRepeatOn Saturday = new(8, nameof(Saturday));
        public ScheduleWeeklyRepeatOn(int id, string name) : base(id, name) { }

        public static IEnumerable<ScheduleWeeklyRepeatOn> List() => new[] { Unknown, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday };


        public static ScheduleWeeklyRepeatOn FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
                throw new ScheduleWeeklyRepeatOnNameNotFoundException(List(), name);

            return state;
        }

        public static ScheduleWeeklyRepeatOn From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
                throw new ScheduleWeeklyRepeatOnIdNotFoundException(List(), id);

            return state;
        }

        public static ScheduleWeeklyRepeatOn Current
            => From(Convert.ToInt32(DateTime.UtcNow.DayOfWeek) + 2);
    }
}
