﻿using Sensormatic.Tool.Ioc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Core
{
    public interface IReportTemplateRepository : IRepository<ReportTemplate>, IScopedDependency
    {
        Task<IEnumerable<ReportTemplateEntityDto>> GetAllTemplatesAsync();
    }
}
