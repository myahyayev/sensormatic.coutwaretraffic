﻿using CountwareTraffic.Services.Companies.Grpc;
using Web.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Grpc.Client;
using Sensormatic.Tool.Ioc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class SubAreaService : IScopedSelfDependency
    {
        private readonly Subarea.SubareaClient _subAreaClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;
       
        public SubAreaService(Subarea.SubareaClient subAreaClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _subAreaClient = subAreaClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetSubAreaDetails> GetSubAreaByIdAsync(Guid subAreaId)
        {
            GetSubAreaRequest grpcRequest = new() { SubAreaId = subAreaId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_subAreaClient.GetSubAreaByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetSubAreaDetails
            {
                Id = new Guid(grpcResponse.SubAreaDetail.Id),
                Name = grpcResponse.SubAreaDetail.Name,
                Description = grpcResponse.SubAreaDetail.Description,
                AreaId = new Guid(grpcResponse.SubAreaDetail.AreaId),
                AuditCreateBy = new Guid(grpcResponse.SubAreaDetail.Audit.AuditCreateBy),
                AuditCreateDate = grpcResponse.SubAreaDetail.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(grpcResponse.SubAreaDetail.Audit.AuditModifiedBy),
                AuditModifiedDate = grpcResponse.SubAreaDetail.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
            };
        }

        public async Task<ApiPagedResponse<GetSubAreaDetails>> GetSubAreasAsync(Guid areaId, DataSourceApiRequest paging)
        {
            GetSubAreasRequest grpcRequest = new()
            {
                AreaId = areaId.ToString(),
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Companies.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Companies.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }


            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_subAreaClient.GetSubAreasAsync, grpcRequest, hasClientSideLog: false);

            var subAreas = grpcResponse.SubAreaDetails.Select(u => new GetSubAreaDetails
            {
                Id = new Guid(u.Id),
                Name = u.Name,
                Description = u.Description,
                AreaId = new Guid(u.AreaId),
                AuditCreateBy = new Guid(u.Audit.AuditCreateBy),
                AuditCreateDate = u.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(u.Audit.AuditModifiedBy),
                AuditModifiedDate = u.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
            });

            return new ApiPagedResponse<GetSubAreaDetails>(subAreas, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task AddSubAreaAsync(Guid areaId, AddSubAreaApiRequest request)
        {
            CreateSubAreaRequest grpcRequest = new()
            {
                AreaId = areaId.ToString(),
                Description = request.Description,
                Name = request.Name
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_subAreaClient.AddSubAreaAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task ChangeSubAreaAsync(Guid subAreaId, ChangeSubAreaApiRequest request)
        {
            UpdateSubAreaRequest grpcRequest = new()
            {
                SubAreaId = subAreaId.ToString(),
                Description = request.Description,
                Name = request.Name
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_subAreaClient.ChangeSubAreaAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task DeleteSubAreaAsync(Guid subAreaId)
        {
            DeleteSubAreaRequest grpcRequest = new() { SubAreaId = subAreaId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_subAreaClient.DeleteSubAreaAsync, grpcRequest, hasClientSideLog: false);
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
