﻿using System.Data;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateDbConnection(string connectionName);
    }
}
