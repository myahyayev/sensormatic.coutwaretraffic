﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class UpdateSuccessApiResponse
    {
        public string Updated { get; set; }
    }
}
