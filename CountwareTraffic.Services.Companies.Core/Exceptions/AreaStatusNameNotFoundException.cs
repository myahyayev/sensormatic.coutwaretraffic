﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Companies.Core
{
    public class AreaStatusNameNotFoundException : DomainException
    {
        IEnumerable<AreaStatus> AreaStatuses { get; }
        public string Name { get; }

        public AreaStatusNameNotFoundException(IEnumerable<AreaStatus> areaStatuses, string name)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1458 Possible values for AreaStatus Name: {String.Join(",", areaStatuses.Select(s => s.Name))}") }, 400, ResponseMessageType.Error)
        {
            AreaStatuses = areaStatuses;
            Name = name;
        }
    }
}
