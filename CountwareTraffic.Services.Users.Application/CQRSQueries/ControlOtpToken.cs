﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ControlOtpToken : IQuery<IdentityTokenResponse>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid? TenantId { get; set; }
        public string Language { get; set; }
        public string Token { get; set; }
        public string Otp { get; set; }
    }
}
