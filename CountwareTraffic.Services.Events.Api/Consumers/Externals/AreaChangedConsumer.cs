﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    public class AreaChangedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaChanged>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        private readonly ILogger<AreaChangedConsumer> _logger;
        public AreaChangedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper, ILogger<AreaChangedConsumer> logger)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
            _logger = logger;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaChanged queuEvent)
        {
            _logger.LogInformation($"Consumed changed Area with name {queuEvent.AreaName} with correlationId {queuEvent.CorrelationId}");
            var areaChanged = _eventMapper.Map(queuEvent) as AreaChanged;
            await _eventDispatcher.PublishAsync(areaChanged);
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
