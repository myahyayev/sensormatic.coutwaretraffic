﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class DistrictsController : BaseController
    {
        private readonly DistrictService _districtService;
        public DistrictsController(IServiceProvider provider, ILogger<DistrictsController> logger, IHttpContextAccessor contextAccessor, DistrictService districtService) 
            : base(provider, logger, contextAccessor.HttpContext) => _districtService = districtService;


        [HttpGet("{districtId}/detail"), ServiceLog]
        public async Task<ApiResponse<GetDistrictDetails>> GetDistrictDetailAsync(Guid districtId)
            => Response(await _districtService.GetDistrictByIdAsync(districtId), ResultMessage.SuccessResultMessage);


        [HttpPost("{cityId}/details"), ServiceLog]
        public async Task<ApiPagedResponse<GetDistrictDetails>> GetDistrictDetailsAsync(Guid cityId, [FromBody] DataSourceApiRequest paging)
            => await _districtService.GetDistrictsAsync(cityId, paging);


        [HttpPost("{cityId}/add"), ServiceLog]
        public async Task<ApiResponse<AddApiResponse>> AddDistrictAsync(Guid cityId, [FromForm] AddDistrictApiRequest request)
        {
            await _districtService.AddDistrictAsync(cityId, request);
            return Response(new AddApiResponse { Created = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpPut("{districtId}/change"), ServiceLog]
        public async Task<ApiResponse<ChangeApiResponse>> ChangeDistrictAsync(Guid districtId, [FromForm] ChangeDistrictApiRequest request)
        {
            await _districtService.ChangeDistrictAsync(districtId, request);
            return Response(new ChangeApiResponse { Updated = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpDelete("{districtId}/delete"), ServiceLog]
        public async Task<ApiResponse<DeleteApiResponse>> DeleteDistrictAsync(Guid districtId)
        {
            await _districtService.DeleteDistrictAsync(districtId);
            return Response(new DeleteApiResponse { Deleted = "Success" }, ResultMessage.SuccessResultMessage);
        }
    }
}
