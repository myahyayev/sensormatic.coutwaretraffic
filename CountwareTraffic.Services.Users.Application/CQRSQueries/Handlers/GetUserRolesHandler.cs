﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserRolesHandler : IQueryHandler<GetUserRoles, IdentityUserRolesResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetUserRolesHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityUserRolesResponse> HandleAsync(GetUserRoles query)
        {
            return await _identityServerService.GetUserRolesAsync(query.UserName);
        }
    }
}
