﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteGroup : IQuery<IdentityDeleteGroupResponse>
    {
        public Guid Id { get; set; }
    }
}
