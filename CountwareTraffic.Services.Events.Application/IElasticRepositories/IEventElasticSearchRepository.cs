﻿using CountwareTraffic.Services.Events.Core;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public interface IEventElasticSearchRepository : IScopedDependency 
    {
        Task<bool> IsIndicesExistsAsync();
        Task AddAsync(EventCreateElasticDto data);
        Task AddRangeAsync(List<EventCreateElasticDto> data);
        Task<QueryablePagingValue<EventDetailsDto>> GetEventsAsync(Guid deviceId, int page, int size, Guid tenantId);
        Task<WidgetPerformingDto> GetWidgetPerformingAsync(List<NodeFilter> nodeFilters, DateTime currentStartDate, DateTime currentEndDate, DateTime previusStartDate, DateTime previusEndDate);
        Task<List<PerformingByTypeDto>> GetPerformingByTypeAsync(List<NodeFilter> nodeFilters, DateTime startDate, DateTime endDate, Direction direction, PerformType performType, int count = 10);
        Task<TrafficByTimePeriodDto> GetTrafficByTimePeriodAsync(List<NodeFilter> nodeFilters, DateTime currentStartDate, DateTime currentEndDate, DateTime previusStartDate, DateTime previusEndDate, TimePeriod timePeriod);
        Task<CountOfVisitorsInsideDto> GetCountOfVisitorsInside(List<NodeFilter> nodeFilters, DateTime currentStartDate);
        Task<VisitorDurationDto> GetVisitorDurationAsync(List<NodeFilter> nodeFilters, DateTime currentStartDate);

        Task<int> GetEventCountAsync(DateTime startDate);
        Task<List<EventsByTimePeriodDto>> GetEventsByTimePeriodAsync(DateTime StartDate, DateTime EndDate);
    }
}
