﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class UpdateBrandModelLookupHandler : ICommandHandler<UpdateBrandModelLookup>
    {
        private readonly IUnitOfWork _unitOfWork;
        public UpdateBrandModelLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task HandleAsync(UpdateBrandModelLookup command)
        {
            var brandModelRepository = _unitOfWork.GetRepository<IBrandModelLookupRepository>();

            var brandModel = await brandModelRepository.GetByIdAsync(command.BrandModelId);

            if (brandModel is null)
                throw new BrandModelLookupNotFoundException(command.BrandModelId);

            var updateControl = await brandModelRepository.GetByNameAsync(command.Name);

            if (updateControl != null && updateControl.Id != command.BrandModelId)
                throw new BrandModelAlreadyExistsException(command.Name);

            brandModel.Change(command.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
