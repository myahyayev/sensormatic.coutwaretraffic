﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityUserPermissionsResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<UserPermissionDetail> Data { get; set; }
    }

    public class UserPermissionDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string DescriptionLabel { get; set; }
    }
}
