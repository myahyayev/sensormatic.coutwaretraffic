﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ScheduleDeliveryTimeEntityTypeConfiguration : IEntityTypeConfiguration<ScheduleDeliveryTime>
    {
        public void Configure(EntityTypeBuilder<ScheduleDeliveryTime> builder)
        {
            builder.ToTable("ScheduleDeliveryTimes", SchemaNames.Reports);

            builder
               .HasKey(x => x.Id);

            builder
                .Property(x => x.Id)
                .HasDefaultValue(1)
                .ValueGeneratedNever()
                .IsRequired();

            builder
                .Property(x => x.Name)
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}
