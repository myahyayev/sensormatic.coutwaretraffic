﻿using CountwareTraffic.Services.Reporting.Application;
using Sensormatic.Tool.Common;
using System;
using System.Globalization;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public class DateRange
    {
        private string _caption { get; set; }
        private string _captionKey { get; set; }

        public DateRangeType Type { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public CultureInfo CultureInfo { get; set; }
        public TimeZoneSettings TimeZoneSettings { get; set; }
        public string WindowsTimeZone { get; set; }
        public string LinuxTimeZone { get; set; }

        public string Caption
        {
            get { return _caption.IsNullOrWhiteSpace() ? ToString() : _caption; }
            set { _caption = value; }
        }

        public string CaptionKey
        {
            get { return _captionKey.IsNullOrWhiteSpace() ? ToString() : _captionKey; }
            set { _captionKey = value; }
        }

        public override string ToString()
        {
            //if (DateTime.Compare(To, From) == 0)
            //    return From.ConvertBySystemTimeZoneId(WindowsTimeZone, LinuxTimeZone, TimeZoneSettings.ProvideByLinux)
            //               .ToString("yyyy-MM-dd HH:mm", CultureInfo);

            if (Type == DateRangeType.ByDate)
            {
                var fromDate = From.ConvertBySystemTimeZoneId(WindowsTimeZone, LinuxTimeZone, TimeZoneSettings.ProvideByLinux);

                return $"{fromDate.ToString("dddd", CultureInfo)} ({fromDate.ToString("dd.MM.yyyy", CultureInfo)})";
            }

            else if (Type == DateRangeType.ByHour)
            {
                var fromDate = From.ConvertBySystemTimeZoneId(WindowsTimeZone, LinuxTimeZone, TimeZoneSettings.ProvideByLinux);

                var toDate = To.ConvertBySystemTimeZoneId(WindowsTimeZone, LinuxTimeZone, TimeZoneSettings.ProvideByLinux);

                return $"{fromDate.ToString("HH", CultureInfo)} ~ {toDate.ToString("HH", CultureInfo)}";
            }

            return DateRangeType.Unknown.ToString();
        }


        public string DateRangeLongName()
        {
            if (DateTime.Compare(To, From) == 0)
                return From.ConvertBySystemTimeZoneId(WindowsTimeZone, LinuxTimeZone, TimeZoneSettings.ProvideByLinux)
                           .ToString("dd.MM.yyyy HH:mm", CultureInfo);

            if (Type == DateRangeType.ByDate)
            {
                var fromStr = From.ConvertBySystemTimeZoneId(WindowsTimeZone, LinuxTimeZone, TimeZoneSettings.ProvideByLinux)
                                  .ToString("dd.MM.yyyy HH:mm", CultureInfo);

                var toStr = To.ConvertBySystemTimeZoneId(WindowsTimeZone, LinuxTimeZone, TimeZoneSettings.ProvideByLinux)
                              .ToString("dd.MM.yyyy HH:mm", CultureInfo);

                return $"{fromStr} ~ {toStr}";
            }

            else if (Type == DateRangeType.ByHour)
            {
                var fromStr = From.ConvertBySystemTimeZoneId(WindowsTimeZone, LinuxTimeZone, TimeZoneSettings.ProvideByLinux)
                                  .ToString("HH", CultureInfo);

                var toStr = To.ConvertBySystemTimeZoneId(WindowsTimeZone, LinuxTimeZone, TimeZoneSettings.ProvideByLinux)
                              .ToString("HH", CultureInfo);

                return $"{fromStr} ~ {toStr}";
            }

            return DateRangeType.Unknown.ToString();
        }
    }
}
