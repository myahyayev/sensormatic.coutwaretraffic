﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteToken : IQuery<IdentityDeleteTokenResponse>
    {
        public Guid Id { get; set; }
    }
}
