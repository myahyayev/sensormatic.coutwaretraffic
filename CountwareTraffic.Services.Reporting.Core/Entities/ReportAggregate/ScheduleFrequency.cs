﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleFrequency : Enumeration
    {
        public static ScheduleFrequency Unknown = new(1, nameof(Unknown));
        public static ScheduleFrequency OneTime = new(2, nameof(OneTime));
        public static ScheduleFrequency Repeat = new(3, nameof(Repeat));
        public ScheduleFrequency(int id, string name) : base(id, name) { }

        public static IEnumerable<ScheduleFrequency> List() => new[] { Unknown, OneTime, Repeat };


        public static ScheduleFrequency FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
                throw new ScheduleTypeNameNotFoundException(List(), name);

            return state;
        }

        public static ScheduleFrequency From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
                throw new ScheduleTypeIdNotFoundException(List(), id);

            return state;
        }
    }
}
