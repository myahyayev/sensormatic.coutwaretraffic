﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Grpc
{
    [Authorize]
    public class UnknownDeviceService : UnknownDevice.UnknownDeviceBase
    {
        private readonly ILogger<UnknownDeviceService> _logger;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        public UnknownDeviceService(ILogger<UnknownDeviceService> logger, ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }

        public override async Task<GetUnknownDeviceDetailResponse> GetUnknownDeviceById(GetUnknownDeviceRequest request, ServerCallContext context)
        {

            var unknownDevice = await _queryDispatcher.QueryAsync(new GetUnknownDevice { Id = request._UnknownDeviceId });

            var response = new GetUnknownDeviceDetailResponse();

            response.UnknownDeviceDetail = new UnknownDeviceDetail
            {
                Id = unknownDevice.Id.ToString(),
                Name = unknownDevice.Name,
                IpAddress = unknownDevice.IpAddress,
                MacAddress = unknownDevice.MacAddress,
                HttpPort = unknownDevice.HttpPort,
                HttpsPort = unknownDevice.HttpsPort,
                OccurredOn = Timestamp.FromDateTimeOffset(unknownDevice.OccurredOn),
                SerialNumber = unknownDevice.SerialNumber,
            };
            return response;
        }

        public override async Task<GetUnknownDevicesDetailResponse> GetUnknownDevices(Empty request, ServerCallContext context)
        {
            var unknownDevices = await _queryDispatcher.QueryAsync(new GetUnknownDevices {});

            var response = new GetUnknownDevicesDetailResponse();

            unknownDevices.ForEach(unknownDevice => response.UnknownDeviceDetails.Add(new UnknownDeviceDetail
            {
                Id = unknownDevice.Id.ToString(),
                Name = unknownDevice.Name,
                IpAddress = unknownDevice.IpAddress,
                MacAddress = unknownDevice.MacAddress,
                HttpPort = unknownDevice.HttpPort,
                HttpsPort = unknownDevice.HttpsPort,
                OccurredOn = Timestamp.FromDateTimeOffset(unknownDevice.OccurredOn),
                SerialNumber = unknownDevice.SerialNumber,
            }));

            return response;
        }

        public override async Task<UpdateSuccessResponse> UnknownDeviceProcessed(UnknownDeviceProcessedRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UnknownDeviceProcessed { Id = request._UnknownDeviceId });

            return new UpdateSuccessResponse { Updated = "Updated" };
        }

        public override async Task<UpdateSuccessResponse> UnknownDeviceDismiss(UnknownDeviceDismissRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UnknownDeviceDismiss { Id = request._UnknownDeviceId });

            return new UpdateSuccessResponse { Updated = "Updated" };
        }
    }
}
