﻿using System.Data;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateDbConnection(string connectionName);
    }
}
