﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRDeviceStatusChangedSuccessfullyConsumer : IConsumer<Sensormatic.Tool.QueueModel.DeviceStatusChangeSuccessfully>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRDeviceStatusChangedSuccessfullyConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.DeviceStatusChangeSuccessfully queueEvent)
        {
            await _commandDispatcher.SendAsync(new DeviceStatusChangeSuccessfully
            {
                DeviceId = queueEvent.DeviceId,
                DeviceStatusId = queueEvent.DeviceStatusId,
                DeviceStatusName = queueEvent.DeviceStatusName,
                Name = queueEvent.Name,
                RecordId = queueEvent.RecordId,
                TenantId = queueEvent.TenantId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
