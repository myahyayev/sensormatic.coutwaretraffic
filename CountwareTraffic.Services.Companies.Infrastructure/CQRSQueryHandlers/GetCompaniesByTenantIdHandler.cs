﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application.CQRSQueries;
using CountwareTraffic.Services.Companies.Application.DTO;
using CountwareTraffic.Services.Companies.Core;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure.CQRSQueryHandlers
{
    public class GetCompaniesByTenantIdHandler : IQueryHandler<GetCompaniesByTenantId, GetCompaniesByTenantIdDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetCompaniesByTenantIdHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<GetCompaniesByTenantIdDto> HandleAsync(GetCompaniesByTenantId query)
        {
            var companyRepository = _unitOfWork.GetRepository<ICompanyRepository>();

            var companies = await companyRepository.GetQuery(query.TenantId)
                                                   .ToListAsync();

            var model = new GetCompaniesByTenantIdDto();

            foreach (var company in companies)
            {
                model.Data.Add(new GetCompaniesByTenantIdItemDto
                {
                    Id = company.Id,
                    Name = company.Name
                });
            }

            return model;
        }
    }
}
