﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class GetReportLanguagesHandler : IQueryHandler<GetReportLanguages, IEnumerable<ReportLanguageDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetReportLanguagesHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<IEnumerable<ReportLanguageDto>> HandleAsync(GetReportLanguages query)
        {
            var repository = _unitOfWork.GetRepository<IReportLanguageRepository>();

            var reportLanguages = await repository.GetAllReportLanguagesAsync();

            if (reportLanguages == null)
                return new List<ReportLanguageDto>();

            return reportLanguages.Select(r => new ReportLanguageDto
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description
            });
        }
    }
}
