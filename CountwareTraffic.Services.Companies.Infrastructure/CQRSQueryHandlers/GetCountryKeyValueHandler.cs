﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetCountryKeyValueHandler : IQueryHandler<GetCountryKeyValue, List<KeyValueDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetCountryKeyValueHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<KeyValueDto>> HandleAsync(GetCountryKeyValue query)
        {
            var countryRepository = _unitOfWork.GetRepository<ICountryRepository>();

            var keyValues = await countryRepository.GetAllKeyValueAsync(query.ParentId);

            return keyValues.Select(u => new KeyValueDto
            {
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }
}


