﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRSubAreaCreatedSuccessfullyConsumer : IConsumer<Sensormatic.Tool.QueueModel.SubAreaCreatedSuccessfully>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRSubAreaCreatedSuccessfullyConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.SubAreaCreatedSuccessfully queueEvent)
        {
            await _commandDispatcher.SendAsync(new SubAreaCreatedSuccessfully
            {
                SubAreaId = queueEvent.SubAreaId,
                SubAreaStatus = queueEvent.SubAreaStatus,
                UserId = queueEvent.UserId,
                UserName = queueEvent.UserName,
                TenantId = queueEvent.TenantId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
