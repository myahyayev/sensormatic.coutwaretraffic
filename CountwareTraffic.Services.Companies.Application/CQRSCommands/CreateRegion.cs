﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    [Contract]
    public class CreateRegion : ICommand
    {
        public Guid CountryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ManagerName { get; set; }
        public Guid? ManagerId { get; set; }
    }
}
