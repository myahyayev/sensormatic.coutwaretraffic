﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantIdHandler : IQueryHandler<GetTenantId, IdentityGetTenantIdResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetTenantIdHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityGetTenantIdResponse> HandleAsync(GetTenantId query)
        {
            return await _identityServerService.GetTenantIdAsync(query.Id);
        }
    }
}
