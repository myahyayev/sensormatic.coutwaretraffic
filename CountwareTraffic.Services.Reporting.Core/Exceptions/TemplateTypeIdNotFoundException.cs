﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class TemplateTypeIdNotFoundException : DomainException
    {
        IEnumerable<TemplateType> TemplateTypes { get; }
        public int Id { get; }

        public TemplateTypeIdNotFoundException(IEnumerable<TemplateType> templateTypes, int id)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for TemplateType Id: {String.Join(",", templateTypes.Select(s => s.Id))}") }, 400, ResponseMessageType.Error)
        {
            TemplateTypes = templateTypes;
            Id = id;
        }
    }
}
