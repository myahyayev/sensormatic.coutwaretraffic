﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{

    public class SubAreaCreatedCompletedHandler : IEventHandler<SubAreaCreatedCompleted>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public SubAreaCreatedCompletedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(SubAreaCreatedCompleted command)
        {
            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            var subArea = await subAreaRepository.GetAsync(command.SubAreaId, command.TenantId);

            subArea.WhenCreatedCompleted();

            await _unitOfWork.CommitAsync();

            _queueService.Publish(new SubAreaCreatedSuccessfully
            {
                RecordId = Guid.NewGuid(),
                SubAreaId = command.SubAreaId,
                SubAreaStatus = SubAreaStatus.Completed.Name,
                UserId = command.UserId,
                UserName = String.Empty,
                TenantId = command.TenantId
            });
        }
    }    
}

