﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportFile : IEntity, IDeletable
    {
        private Guid _id;
        private Guid _reportId;
        private DateTime _createdDate;
        private string _filePath;
        private DateTime _dateRangeStartDate;
        private DateTime _dateRangeEndDate;

        public Guid Id => _id;
        public Guid ReportId => _reportId;
        public DateTime CreatedDate => _createdDate;
        public DateTime DateRangeStartDate => _dateRangeStartDate;
        public DateTime DateRangeEndDate => _dateRangeEndDate;
        public string FilePath => _filePath;
        public bool AuditIsDeleted { get; set; }

        public static ReportFile Create(Guid reportId, DateTime createdDate, string filePath, DateTime dateRangeStartDate, DateTime dateRangeEndDate)
        {
            return new ReportFile
            {
                _createdDate = createdDate,
                _filePath = filePath,
                _reportId = reportId,
                _dateRangeStartDate = dateRangeStartDate,
                _dateRangeEndDate = dateRangeEndDate,
            };
        }

        public void SetFilePath(string filePath)
        {
            _filePath = filePath;
        }
    }
}
