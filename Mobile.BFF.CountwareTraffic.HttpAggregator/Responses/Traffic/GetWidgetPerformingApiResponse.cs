﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetWidgetPerformingApiResponse
    {
        public int InwardsCurrent { get; set; }
        public int InwardsPrevius { get; set; }
    }
}
