﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using Sensormatic.Tool.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class GetReportsHandler : IQueryHandler<GetReports, PagingResult<ReportDetailDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetReportsHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;
        public async Task<PagingResult<ReportDetailDto>> HandleAsync(GetReports query)
        {
            var reportRepository = _unitOfWork.GetRepository<IReportRepository>();

            int page = query.PagingQuery.Page;
            int limit = query.PagingQuery.Limit;

            var qres = await reportRepository.GetAllReports(query.PagingQuery, query.Filters, query.Sorts);

            if (qres == null)
                return PagingResult<ReportDetailDto>.Empty;


            return new PagingResult<ReportDetailDto>(qres.Entities.Select(report => new ReportDetailDto
            {
                Id = report.Id,
                Name = report.Name,
                Description = report.Description,
                FormatId = report.Format.Id,
                FormatName = report.Format.Name,
                IsActive = report.IsActive,
                Locations = report.Location.Select(u => u.Name).ToList(),
                StatusId = report.Status.Id,
                StatusName = report.Status.Name,
                TemplateName = report.ReportTemplate.Name,
                TemplateTypeId = report.TemplateType.Id,
                TemplateTypeName = report.TemplateType.Name,
                TenantId = report.TenantId,
                ReportFiles = report.ReportFile == null ? null : new()
                {
                    new ReportFileDto()
                    {
                        CreatedDate = report.ReportFile.CreatedDate,
                        FilePath = report.ReportFile.FilePath,
                        Id = report.ReportFile.Id,
                        DateRangeStartDate = report.ReportFile.DateRangeStartDate,
                        DateRangeEndDate = report.ReportFile.DateRangeEndDate
                    }
                },

                ScheduleFrequencyId = report.ReportSchedule.Frequency.Id,
                ScheduleFrequencyName = report.ReportSchedule.Frequency.Name,

            }), qres.Total, page, limit, qres.Total > (limit * (page - 1)) + qres.Entities.Count);
        }
    }
}