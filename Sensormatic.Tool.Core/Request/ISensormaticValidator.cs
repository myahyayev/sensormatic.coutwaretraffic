﻿namespace Sensormatic.Tool.Core
{
    public interface ISensormaticValidator : ISensormaticValidate
    {
        void Validate();
    }
}
