﻿namespace CountwareTraffic.Services.Events.Application
{
    public class DeviceStatusDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
