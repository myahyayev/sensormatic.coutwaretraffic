﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserGridHandler : IQueryHandler<GetUserGrid, IdentityUserGridResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetUserGridHandler(IIdentityServerService identityServerService) => _identityServerService = identityServerService;

        public async Task<IdentityUserGridResponse> HandleAsync(GetUserGrid query)
        {
            return await _identityServerService.GetUserGridAsync(new IdentityUserGridRequest
            {
                Paging = query.PagingQuery,
                Sorts = query.Sorts,
                Filters = query.Filters
            });
        }
    }
}
