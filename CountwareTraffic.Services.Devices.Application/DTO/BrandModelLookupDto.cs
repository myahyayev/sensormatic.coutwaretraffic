﻿using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class BrandModelLookupDto
    {
        public Guid Id { get; set; }
    }
}
