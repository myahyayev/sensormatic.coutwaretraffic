﻿using Convey.CQRS.Commands;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    [Contract]
    public class CreateEventRange : ICommand
    {
        public CreateEventRange(Guid deviceId)
        {
            Items = new();
            DeviceId = deviceId;
        }
        public List<CreateEvent> Items { get; set; }
        public Guid DeviceId { get; set; }

     
    }
}
