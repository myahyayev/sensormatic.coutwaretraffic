﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaHUpdatedHandler : IEventHandler<AreaHUpdated>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public AreaHUpdatedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(AreaHUpdated command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.CompanyId
            };

            CountryElasticDto countryElasticDto = new()
            {
                Id = command.CountryId
            };

            RegionElasticDto regionElasticDto = new()
            {
                Id = command.RegionId
            };

            AreaElasticDto areaElasticDto = new()
            {
                Id = command.AreaId,
                Name = command.AreaName
            };

            await _tenantHierarchyElasticSearchRepository.UpdateAreaAsync(tenantElasticDto, companyElasticDto, countryElasticDto, regionElasticDto, areaElasticDto);
        }
    }
}

