﻿using System;

namespace CountwareTraffic.Services.Reporting.Engine.CompherensiveTrafficReport
{
    public record ComparedStoreSummary : Engine.BaseComparedStoreSummary
    {     public ComparedStoreSummary()
        {
            Traffic = new Traffic();
            sales = new();
            SalesTrxs = new();
            ReturnTrxs = new();
            ConvRate = new();
            ATV = new();
            ShopperYield = new();
            CaptureRate = new();
            NewShopper = new();
        }
        public TimeSpan VisitDuration { get; set; }
        public Traffic Traffic { get; set; }
   
        public ValueObject<float> sales { get; set; }
        public ValueObject<float> SalesTrxs { get; set; }
        public ValueObject<float> ReturnTrxs { get; set; }
        public ValueObject<float> ConvRate { get; set; }
        public ValueObject<float> ATV { get; set; }
        public ValueObject<float> ShopperYield { get; set; }
        public ValueObject<float> CaptureRate { get; set; }
        public ValueObject<float> NewShopper { get; set; }
    }
}
