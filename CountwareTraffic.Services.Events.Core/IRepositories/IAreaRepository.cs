﻿using Sensormatic.Tool.Ioc;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Core
{
    public interface IAreaRepository : IRepository<Area>, IScopedDependency
    {
        Task<Area> GetAsync(Guid id);
        Task<bool> ExistsAsync(Guid id);
    }
}