﻿using System;

namespace CountwareTraffic.Services.Users.Application.Objects.Responses
{
    public class IdentityGetContractByIdResponse
    {
        public IdentityGetContractByIdResponse()
        {
            Data = new();
        }

        public int Status { get; set; }
        public string Message { get; set; }
        public IdentityGetContractByIdResponseItem Data { get; set; }
    }

    public class IdentityGetContractByIdResponseItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string ContractBody { get; set; }
        public string ContractType { get; set; }
        public string Language { get; set; }
        public int AppType { get; set; }
    }
}
