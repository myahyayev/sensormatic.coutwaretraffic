syntax = "proto3";

option csharp_namespace = "CountwareTraffic.Services.Reporting.Grpc";

import "google/protobuf/timestamp.proto";
import "google/protobuf/wrappers.proto";
import "google/protobuf/empty.proto";

package report;

service Report {
     rpc GetReportTemplates(google.protobuf.Empty) returns (GetReportTemplatesResponse){}
     rpc GetReportLookups(google.protobuf.Empty) returns (GetReportLookupsResponse){}
     rpc GetReports(GetReportsRequest) returns (GetReportsResponse){}
     rpc GetReportById(GetReportByIdRequest) returns(GetReportByIdResponse){}
     rpc GetReportLanguages(google.protobuf.Empty) returns(GetReportLanguagesResponse){}
     rpc DeleteReport(DeleteReportRequest) returns (DeleteSuccessResponse){}
     rpc CreateReport(CreateReportRequest) returns (CreateSuccessResponse){}
     rpc GetReportTemplateView(GetReportTemplateViewRequest) returns (GetReportTemplateViewResponse){}
     rpc GetReportFiles(GetReportFilesRequest) returns (GetReportFilesResponse){}
     rpc RetryReportGenerating(RetryReportGeneratingRequest) returns (RetryReportGeneratingResponse){}
}


/*REQUESTS*/
message GetReportsRequest{
    DataSourceRequest DataSourceRequest  = 1;
}

message GetReportByIdRequest{
    string ReportId = 1;
}
message DeleteReportRequest{
    string ReportId = 1;
}
message CreateReportRequest{
    string Name = 1;
    string TemplateId = 2;
    google.protobuf.StringValue Description = 3;
    int32  FormatId = 4; 
    int32 TemplateTypeId = 5;   
    bool NotifyWhenComplete = 6;
    int32 FrequencyId = 7;
    google.protobuf.Timestamp StartsOn = 8;   
    google.protobuf.Timestamp EndsOn = 9;        
    google.protobuf.StringValue RepeatOn = 10;      
    int32 DeliveryTimeId  = 11;   
    google.protobuf.StringValue AtSetTime = 12;   
    google.protobuf.StringValue AtSetTimeZone = 13;        
    google.protobuf.StringValue EmailTo = 14;
    google.protobuf.StringValue EmailBody = 15;
    google.protobuf.StringValue EmailSubject = 16;      
    string LanguageId = 17;    
    google.protobuf.Timestamp DateRangeStartDate = 18;   
    google.protobuf.Timestamp DateRangeEndDate = 19;        
    repeated Location Locations = 20;    
}
message GetReportTemplateViewRequest{
    string ReportTemplateId = 1;
}

message GetReportFilesRequest{
    DataSourceRequest DataSourceRequest  = 1;
    string ReportId = 2;
}

message RetryReportGeneratingRequest{
    string ReportId = 1;
    google.protobuf.StringValue ReportFileId = 2; 
    google.protobuf.Timestamp DateRangeStartDate = 3;  
    google.protobuf.Timestamp DateRangeEndDate = 4;  
}

/*RESPONSES*/
message GetReportTemplatesResponse{
    repeated ReportTemplate ReportTemplates = 1;
}

message GetReportLookupsResponse{
    repeated EnumResponse  ReportFormats = 1;
    repeated EnumResponse  ScheduleFrequencies = 2;
    repeated EnumResponse  ScheduleWeeklyRepeatOns = 3;
    repeated EnumResponse  ScheduleDeliveryTimes = 4;
}

message GetReportsResponse{
    int32 TotalCount = 1;
    int32 Page = 2;
    int32 Limit = 3;
    bool HasNextPage = 4;
    google.protobuf.Int32Value Next = 5;
    google.protobuf.Int32Value Prev = 6;
    repeated ReportDetail ReportDetails  = 7;
}

message GetReportByIdResponse{
    string Id = 1;
    string Name = 2;
    google.protobuf.StringValue Description = 3;
    google.protobuf.StringValue TemplateName = 4;
    int32 TemplateTypeId = 5;
    int32 ReportFormatId = 6;
    int32 ReportStatusId = 7;
    bool IsActive = 8;
    repeated google.protobuf.StringValue Locations = 9;
    string TemplateTypeName = 10;
    string ReportFormatName = 11;
    string ReportStatusName = 12;
    bool NotifyWhenComplete = 13;
    google.protobuf.StringValue EmailTo = 14;
    google.protobuf.StringValue EmailBody = 15;
    google.protobuf.StringValue EmailSubject = 16;
    string LanguageId = 17;
    string LanguageName = 18;
    string ScheduleId = 19;
    int32 ScheduleFrequencyId = 20;
    string ScheduleFrequencyName = 21;
    int32 ScheduleDeliveryTimeId = 22;
    string ScheduleDeliveryTimeName = 23;
    google.protobuf.StringValue AtSetTime = 24;
    google.protobuf.StringValue AtSetTimeZone = 25;
    google.protobuf.StringValue RepeatOn = 26;
    google.protobuf.Timestamp StartsOn = 27;   
    google.protobuf.Timestamp EndsOn = 28;  
    google.protobuf.Timestamp DateRangeStartDate = 29;   
    google.protobuf.Timestamp DateRangeEndDate = 30;
    string TemplateId = 31;
    repeated ReportFile ReportFiles = 32;
}

message GetReportLanguagesResponse{
    repeated ReportLanguage ReportLanguages = 1;
}

message GetReportTemplateViewResponse{
    string Id = 1;
    google.protobuf.StringValue FilePath = 2;
}

message GetReportFilesResponse{
    int32 TotalCount = 1;
    int32 Page = 2;
    int32 Limit = 3;
    bool HasNextPage = 4;
    google.protobuf.Int32Value Next = 5;
    google.protobuf.Int32Value Prev = 6;
    repeated ReportFile ReportFiles  = 7;
}

message RetryReportGeneratingResponse{
    bool IsSuccess =  1;
}

/*MODELS*/
message ReportTemplate{
    string Id = 1;
    repeated TemplateType  TemplateTypes = 2;
    string Name = 3;
    google.protobuf.StringValue Description = 4;
    google.protobuf.Timestamp AuditCreateDate = 5;
    google.protobuf.Timestamp AuditModifiedDate = 6;
    string AuditCreateBy = 7;
    string AuditModifiedBy = 8;
}

message ReportDetail{
    string Id = 1;
    string Name = 2;
    google.protobuf.StringValue Description = 3;
    google.protobuf.StringValue TemplateName = 4;
    int32 TemplateTypeId = 5;
    int32 ReportFormatId = 6;
    int32 ReportStatusId = 7;
    bool IsActive = 8;
    repeated google.protobuf.StringValue Locations = 9;
    string TemplateTypeName = 10;
    string ReportFormatName = 11;
    string ReportStatusName = 12;
    string TemplateId = 13;
    repeated ReportFile ReportFiles = 14;
    int32 FrequencyId = 15;
    google.protobuf.StringValue FrequencyName = 16;
}
message EnumResponse{
     int32 Key = 1;
     string Name = 2;
     bool IsVisible = 3;
}

message TemplateType{
    int32 Id = 1;
    string Name = 2;
}

message PagingRequest{
    int32 Limit = 1;
    int32 Page = 2;
}

message GridFilter{
    FilterEnum Operator = 1;
    string Field = 2;
    string Value = 3;
    repeated string GuidValues = 4;
    repeated string StringValues = 5;
}

message SortDescriptor{
    string Field = 1;
    Direction Direction = 2;
}

enum FilterEnum{
    FilterEnumDefault = 0;
    Eq = 1;
    Neq = 2;
    StartsWith = 3;
    Contains = 4;
    EndsWith = 5;
}

enum Direction{
     DirectionDefault = 0;
     Asc = 1;
     Desc = 2;
}

message  DataSourceRequest{
    PagingRequest PagingRequest = 1;
    repeated GridFilter Filters = 2;
    repeated SortDescriptor Sorts = 3;
}

message ReportLanguage{
    string Id = 1;
    string Name = 2;
    google.protobuf.StringValue Description = 3;
}

message UpdateSuccessResponse{
     google.protobuf.StringValue Updated = 1;
}

message DeleteSuccessResponse{
     google.protobuf.StringValue Deleted = 1;
}

message CreateSuccessResponse{
     google.protobuf.StringValue Created = 1;
}

message Location{
     LocationLevel LocationLevel = 1;
     string Id = 2;
     string Name = 3;
}

enum LocationLevel{
    LocationLevelDefault = 0;
    Tenant = 1;
    Company = 2;
    Country = 3;
    Region = 4;
    Area = 5;
    SubArea = 6;
    Device = 7;
}
message ReportFile{
    string Id = 1;
    google.protobuf.StringValue FilePath = 2;
    google.protobuf.Timestamp CreatedDate = 3;
    google.protobuf.Timestamp DateRangeStartDate = 4;
    google.protobuf.Timestamp DateRangeEndDate = 5;
}
