﻿namespace CountwareTraffic.Services.Events.Application
{
    public enum TimePeriod
    {
        Hourly = 1,
        Daily = 2,
        Monthly = 3
    }
}
