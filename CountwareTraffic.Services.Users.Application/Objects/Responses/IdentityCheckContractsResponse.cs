﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityCheckContractsResponse
    {
        public Guid ContractId { get; set; }
        public string ContractType { get; set; }
        public string Version { get; set; }
    }
}
