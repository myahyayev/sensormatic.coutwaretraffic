﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application.Objects.Responses
{
    public class GetAdminPermissionSelectResponse
    {
        public string Message { get; set; }
        public int Status { get; set; }
        public List<AdminSelectResponseDetail> Data { get; set; }
    }

    public class AdminSelectResponseDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
