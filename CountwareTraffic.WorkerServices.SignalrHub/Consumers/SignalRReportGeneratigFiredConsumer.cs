﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRReportGeneratigFiredConsumer : IConsumer<Sensormatic.Tool.QueueModel.ReportGeneratigFired>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRReportGeneratigFiredConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.ReportGeneratigFired queueEvent)
        {
            await _commandDispatcher.SendAsync(new ReportGeneratingFired
            {
                ReportId = queueEvent.ReportId,
                ReportStatus = queueEvent.ReportStatus,
                TenantId = queueEvent.TenantId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
