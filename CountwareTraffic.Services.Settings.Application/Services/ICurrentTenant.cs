﻿using Sensormatic.Tool.Ioc;
using System;

namespace CountwareTraffic.Services.Settings.Application
{
    public interface ICurrentTenant : ITransientDependency
    {
        Guid Id { get; }
    }
}
