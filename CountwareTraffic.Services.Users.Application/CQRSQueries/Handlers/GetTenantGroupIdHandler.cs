﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantGroupIdHandler : IQueryHandler<GetTenantGroupId, IdentityTenantGroupIdResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetTenantGroupIdHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityTenantGroupIdResponse> HandleAsync(GetTenantGroupId query)
        {
            return await _identityServerService.GetTenantGroupAsync(query.Id);
        }
    }
}
