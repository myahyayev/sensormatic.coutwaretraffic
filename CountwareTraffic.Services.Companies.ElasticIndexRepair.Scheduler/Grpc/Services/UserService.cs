﻿using CountwareTraffic.Services.Users.Grpc;
using CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Responses;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Services
{
    public class UserService : IScopedSelfDependency
    {
        private readonly User.UserClient _userClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;

        public UserService(User.UserClient userClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _userClient = userClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<IEnumerable<GetTenantDetails>> GetTenantsAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_userClient.GetTenantsAsync, grpcRequest, hasClientSideLog: false);


            return grpcResponse.Tenants.Select(tenant => new GetTenantDetails
            {
                TenantId = new Guid(tenant.TenantId),
                Name = tenant.Name,
                Description = tenant.Description
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
