﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class UnknownDeviceCreated : Convey.CQRS.Events.IEvent
    {
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string DeviceName { get; set; }
        public string SerialNumber { get; set; }
        public int HttpPort { get; set; }
        public int HttpsPort { get; set; }
    }
}
