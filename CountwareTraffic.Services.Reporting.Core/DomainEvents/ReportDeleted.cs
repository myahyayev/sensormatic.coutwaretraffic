﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportDeleted : IDomainEvent
    {
        public Guid ReportId { get; init; }
        public Guid RecordId { get; init; }

        public ReportDeleted(Guid reportId)
        {
            ReportId = reportId;
            RecordId = Guid.NewGuid();
        }
    }
}
