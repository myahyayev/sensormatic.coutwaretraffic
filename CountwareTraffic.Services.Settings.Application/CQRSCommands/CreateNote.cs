﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Settings.Application
{
    [Contract]
    public class CreateNote : ICommand
    {
        public string Subject { get; set; }
        public string Content { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsAffectToReport { get; set; }
        public bool IsVisible { get; set; }
    }
}
