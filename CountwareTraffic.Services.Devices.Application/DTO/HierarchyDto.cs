﻿using CountwareTraffic.Services.Devices.Core;
using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class HierarchyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }
}
