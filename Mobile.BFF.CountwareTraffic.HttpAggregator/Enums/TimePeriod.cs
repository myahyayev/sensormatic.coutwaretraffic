﻿namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public enum TimePeriod
    {
        Hourly = 1,
        Daily = 2,
        Monthly = 3
    }
}
