﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Enums;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Grpc.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Sensormatic.Tool.Api;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class UsersController : BaseController
    {
        private readonly UserService _userService;

        private readonly CompanyService _companyService;
        private readonly CountryService _countryService;
        private readonly RegionService _regionService;
        private readonly AreaService _areaService;
        private readonly SubAreaService _subAreaService;
        private readonly IConfiguration _configuration;
        public UsersController(IServiceProvider provider, ILogger<UsersController> logger, IHttpContextAccessor httpContextAccessor, CompanyService companyService, CountryService countryService, RegionService regionService, AreaService areaService, SubAreaService subAreaService, UserService userService, IConfiguration configuration)
            : base(provider, logger, httpContextAccessor)

        {
            _userService = userService;
            _companyService = companyService;
            _countryService = countryService;
            _areaService = areaService;
            _regionService = regionService;
            _subAreaService = subAreaService;
            _configuration = configuration;
        }



        [AllowAnonymous]
        [HttpGet("tenants"), ServiceLog]
        public async Task<ApiResponse<IEnumerable<GetTenantDetails>>> GetTenantsAsync()
             => Response(await _userService.GetTenantsAsync(), ResultMessage.SuccessResultMessage);


        [AllowAnonymous]
        [HttpPost("token"), ServiceLog]
        public async Task<ApiResponse<TokenApiResponse>> GetTokenAsync([FromForm] TokenApiRequest request) //todo: client tarafinda subdoin olarak tenant id dbde map tablosundan bulunarak requeste eklenicek.Bu islemi middleware tarafinda yapmaya ozen gosterin.
        {
            var subDomain = _httpContextAccessor.GetSubDomain();

            var tenants = await _userService.GetTenantsAsync();

            var tenant = tenants.Where(tenant => tenant.Name.ToLower() == subDomain.ToLower()).FirstOrDefault();

            if (tenant != null)
                request.TenantId = tenant.TenantId;
            else if (subDomain.ToLower() != "localhost" && subDomain.ToLower() != "mobile-bff" && subDomain.ToLower() != "admin" && subDomain.ToLower() != "")
                throw new Exception("#E1001 " + subDomain + " Subdomain is wrong, tenant couldn't found.");

            var language = _httpContextAccessor.GetLanguage();
            var tokenResponse = await _userService.GetTokenAsync(request, language);

            if (tokenResponse.LoginStatus == LoginStatus.FirstLogin)
            {
                return Response(tokenResponse, new ResultMessage()
                {
                    CallToActionType = CallToActionType.ChangePassword,
                    Description = $"#I1001 You must change your password as it is the first login.",
                    Title = $"#I1001 You must change your password as it is the first login.",
                });
            }
            return Response(tokenResponse, new MessageResponse("#S1002"));
        }


        [AllowAnonymous]
        [HttpPatch("{userName}/token/password-reset"), ServiceLog]
        public async Task<ApiResponse<ChangePasswordApiResponse>> ResetPasswordWithTokenAsync(string userName, [FromForm] ResetPasswordWithTokenApiRequest request)
            => Response(await _userService.ResetPasswordWithTokenAsync(userName, request), new MessageResponse("#S1003"));


        [HttpPatch("{userName}/password-change"), ServiceLog]
        public async Task<ApiResponse<ChangePasswordApiResponse>> ChangePasswordAsync(string userName, [FromForm] ChangePasswordApiRequest request)
            => Response(await _userService.ChangePasswordAsync(userName, request), new MessageResponse("#S1004"));


        [AllowAnonymous]
        [HttpPatch("token-refresh"), ServiceLog]
        public async Task<ApiResponse<RefreshTokenApiResponse>> RefreshTokenAsync([FromForm] RefreshTokenApiRequest request)
            => Response(await _userService.RefreshTokenAsync(request.AccessToken, request.RefreshToken), new MessageResponse("#S1005"));


        [HttpPost("details")]
        [Permissions(Permissions = new[] { Permissions.UserList })]
        public async Task<ApiPagedResponse<UserApiResponse>> GetUserGridAsync([FromBody] DataSourceApiRequest paging)
           => await _userService.GetUserGridAsync(paging);


        [HttpGet("detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserView })]
        public async Task<ApiResponse<GetUserIdApiResponse>> GetUserAsync()
        {
            Guid loginUserId = UserExtensions.GetUserId((ClaimsIdentity)User.Identity);
            return await GetUserIdAsync(loginUserId);
        }


        [HttpPost("create-user"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserAdd })]
        public async Task<ApiResponse<CreateUserApiResponse>> CreateUserAsync([FromBody] CreateUserApiRequest request)
        {
            var response = await _userService.CreateUserAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1006")) : throw new Exception(response.Message);
        }


        [HttpPut("update-user"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserEdit })]
        public async Task<ApiResponse<UpdateUserApiResponse>> UpdateUserAsync([FromBody] UpdateUserApiRequest request)
        {
            var response = await _userService.UpdateUserAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1007")) : throw new Exception(response.Message);
        }


        [HttpGet("{userId}/detail-with-userid"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserView })]
        public async Task<ApiResponse<GetUserIdApiResponse>> GetUserIdAsync(Guid userId)
        {
            var user = await _userService.GetUserIdAsync(userId);

            user.Areas = await SetUserAreasByUserTypeAsync(user, TenantId, TenantName);

            user.Areas = user.Areas.Where(area => area.Name != "").ToList();

            return Response(user, new MessageResponse("#S1008"));
        }

        private async Task<List<AreaDetailApi>> SetUserAreasByUserTypeAsync(GetUserIdApiResponse user, string tenantId, string tenantName)
        {
            Hierarchy defaultHierarchy = new()
            {
                HierarchyLevel = HierarchyLevel.Tenant,
                Id = (!tenantId.IsNullOrWhiteSpace() ? new Guid(tenantId) : Guid.Empty),
                Name = tenantName
            };

            if (user.UserType == (int)UserType.SuperAdmin || user.UserType == (int)UserType.Admin)
            {
                var response = await _companyService.GetCompaniesByTenantIdAsync(tenantId.ToGuid());

                if (response.Data.Any())
                {
                    user.Areas = new();

                    foreach (var company in response.Data)
                    {
                        user.Areas.Add(new AreaDetailApi
                        {
                            Id = company.Id.ToGuid(),
                            Name = company.Name,
                            Level = (int)HierarchyLevel.Company
                        });
                    }
                }
            }
            else
            {
                foreach (var area in user.Areas)
                {
                    switch ((HierarchyLevel)area.Level)
                    {
                        case HierarchyLevel.Tenant:
                            area.Name = defaultHierarchy.Name;
                            break;
                        case HierarchyLevel.Company:
                            {
                                try { area.Name = (await _companyService.GetHierarchyAsync(area.Id)).LastOrDefault()?.Name ?? ""; }
                                catch { area.Name = ""; }
                            }
                            break;
                        case HierarchyLevel.Country:
                            {
                                try { area.Name = (await _countryService.GetHierarchyAsync(area.Id)).LastOrDefault()?.Name ?? ""; }
                                catch { area.Name = ""; }
                            }
                            break;
                        case HierarchyLevel.Region:
                            {
                                try { area.Name = (await _regionService.GetHierarchyAsync(area.Id)).LastOrDefault()?.Name ?? ""; }
                                catch { area.Name = ""; }
                            }
                            break;
                        case HierarchyLevel.Area:
                            {
                                try { area.Name = (await _areaService.GetHierarchyAsync(area.Id)).LastOrDefault()?.Name ?? ""; }
                                catch { area.Name = ""; }
                            }
                            break;
                        case HierarchyLevel.SubArea:
                            {
                                try { area.Name = (await _subAreaService.GetHierarchyAsync(area.Id)).LastOrDefault()?.Name ?? ""; }
                                catch { area.Name = ""; }
                            }
                            break;
                        default:
                            throw new NotImplementedException();

                    }
                }
            }

            return user.Areas;
        }

        [HttpDelete("{userId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserDelete })]
        public async Task<ApiResponse<DeleteUserApiResponse>> DeleteUserAsync(Guid userId)
        {
            var response = await _userService.DeleteUserAsync(userId);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1009")) : throw new Exception(response.Message);
        }


        [HttpGet("{userName}/detail-with-username"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserView })]
        public async Task<ApiResponse<UserApiResponse>> GetUserIdAsync(string userName)
            => Response(await _userService.GetUserUserNameAsync(userName), new MessageResponse("#S1010"));


        [HttpPost("cerate-user-default-password"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserAdd })]
        public async Task<ApiResponse<UpdateUserApiResponse>> CreateUserWithDefaultPasswordAsync([FromBody] UpdateUserApiRequest request)
        {
            var language = _httpContextAccessor.GetLanguage();
            var appUrl = _configuration["appUrl"];
            var tenantName = GetTenantName();
            string domain = String.Format(appUrl,tenantName);
            var response = await _userService.CreateUserWithDefaultPasswordAsync(request, domain, language);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1011")) : throw new Exception(response.Message);
        }


        [HttpGet("{userName}/username-exist"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserAdd })]
        public async Task<ApiResponse<UserNameExistApiResponse>> GetUserNameExistAsync(string userName)
        {
            var response = await _userService.GetUserNameExistAsync(userName);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1012")) : throw new Exception(response.Message);
        }


        [HttpGet("{email}/email-exist"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserAdd })]
        public async Task<ApiResponse<EmailExistApiResponse>> GetEmailExistAsync(string email)
        {
            var response = await _userService.GetEmailExistAsync(email);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1013")) : throw new Exception(response.Message);
        }


        [HttpPost("reset-password"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminUserEdit })]
        public async Task<ApiResponse<ResetPasswordApiResponse>> ResetPasswordAsync([FromBody] ResetPasswordApiRequest request)
            => Response(await _userService.ResetPasswordAsync(request), new MessageResponse("#S1014"));


        [HttpGet("{userId}/change-status"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserEdit })]
        public async Task<ApiResponse<UserChangeStatusApiResponse>> GetUserChangeStatusAsync(Guid userId)
        {
            var response = await _userService.GetUserChangeStatusAsync(userId);
            return Response(response, new MessageResponse(response.Message));
        }


        [HttpGet("{email}/email-exists"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserAdd })]
        public async Task<ApiResponse<EmailExistsApiResponse>> GetEmailExistsAsync(string email)
            => Response(await _userService.GetEmailExistsAsync(email), new MessageResponse("#S1015"));


        [HttpGet("{userId}/user-avatar"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserView })]
        public async Task<ApiResponse<UserAvatarApiResponse>> GetUserAvatarAsync(Guid userId)
            => Response(await _userService.GetUserAvatarAsync(userId), new MessageResponse("#S1016"));


        [HttpGet("{userId}/user-reset"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminUserReset })]
        public async Task<ApiResponse<UserResetApiResponse>> UserResetAsync(Guid userId)
            => Response(await _userService.UserResetAsync(userId), new MessageResponse("#S1017"));

        [HttpPost("tenant-group-details")]
        [Permissions(Permissions = new[] { Permissions.AdminTenantGroupList })]
        public async Task<ApiPagedResponse<TenantGroupPaginationApiResponse>> GetTenantGroupPaginationAsync([FromBody] DataSourceApiRequest paging)
           => await _userService.GetTenantGroupPaginationAsync(paging);


        [HttpGet("tenant-group-detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantGroupView })]
        public async Task<ApiResponse<List<TenantGroupApiData>>> GetTenantGroupAsync()
        {
            var response = await _userService.GetTenantGroupAsync();
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1018")) : throw new Exception(response.Message);
        }


        [HttpPost("create-tenant-group"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantGroupAdd })]
        public async Task<ApiResponse<CreateTenantGroupApiResponse>> CreateTenantGroupAsync(CreateTenantGroupApiRequest request)
        {
            var response = await _userService.CreateTenantGroupAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1019")) : throw new Exception(response.Message);
        }


        [HttpPut("update-tenant-group"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantGroupEdit })]
        public async Task<ApiResponse<UpdateTenantGroupApiResponse>> UpdateTenantGroupAsync(UpdateTenantGroupApiRequest request)
        {
            var response = await _userService.UpdateTenantGroupAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1020")) : throw new Exception(response.Message);
        }


        [HttpDelete("{tenantGroupId}/delete-tenant-group"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantGroupDelete })]
        public async Task<ApiResponse<DeleteTenantGroupApiResponse>> DeleteTenantGroupAsync(Guid tenantGroupId)
        {
            var response = await _userService.DeleteTenantGroupAsync(tenantGroupId);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1141")) : throw new Exception(response.Message);
        }


        [HttpGet("{tenantGroupId}/tenant-group-detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantGroupView })]
        public async Task<ApiResponse<TenantGroupApiData>> GetTenantGroupAsync(Guid tenantGroupId)
        {
            var response = await _userService.GetTenantGroupAsync(tenantGroupId);
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1021")) : throw new Exception(response.Message);
        }


        [HttpGet("tenant-group-detail-select"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantGroupList })]
        public async Task<ApiResponse<List<TenantGroupApiData>>> GetTenantGroupSelectAsync()
        {
            var response = await _userService.GetTenantGroupSelectAsync();
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1022")) : throw new Exception(response.Message);
        }


        [HttpPost("create-tenant"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantAdd })]
        public async Task<ApiResponse<CreateTenantApiResponse>> CreateTenantAsync(CreateTenantApiRequest request)
        {
            var response = await _userService.CreateTenantAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1023")) : throw new Exception(response.Message);
        }


        [HttpPut("update-tenant"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantEdit })]
        public async Task<ApiResponse<UpdateTenantApiResponse>> UpdateTenantAsync(UpdateTenantApiRequest request)
        {
            var response = await _userService.UpdateTenantAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1024")) : throw new Exception(response.Message);
        }


        [HttpGet("tenant-select"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantList })]
        public async Task<ApiResponse<List<TenantSelectApiData>>> GetTenantSelectAsync()
        {
            var response = await _userService.GetTenantSelectAsync();
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1025")) : throw new Exception(response.Message);
        }


        [HttpPost("role-details"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleList })]
        public async Task<ApiPagedResponse<RolePaginationApiResponse>> GetRolePaginationAsync(DataSourceApiRequest paging)
        {
            var language = _httpContextAccessor.GetLanguage();
            return await _userService.GetRolePaginationAsync(paging, language);
        }


        [HttpGet("role-detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleView })]
        public async Task<ApiResponse<RolesApiResponse>> GetRoleAsync()
            => Response(await _userService.GetRoleAsync(), new MessageResponse("#S1026"));


        [HttpPost("create-role"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleAdd })]
        public async Task<ApiResponse<CreateRoleApiResponse>> CreateRoleAsync(CreateRoleApiRequest request)
        {
            var response = await _userService.CreateRoleAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1027")) : throw new Exception(response.Message);
        }


        [HttpPut("update-role"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleEdit })]
        public async Task<ApiResponse<UpdateRoleApiResponse>> UpdateRoleAsync(UpdateRoleApiRequest request)
        {
            var response = await _userService.UpdateRoleAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1028")) : throw new Exception(response.Message);
        }


        [HttpGet("{roleId}/role-detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleView })]
        public async Task<ApiResponse<RoleIdApiResponseData>> GetRoleIdAsync(Guid roleId)
        {
            var language = _httpContextAccessor.GetLanguage();
            var response = await _userService.GetRoleIdAsync(roleId, language);
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1029")) : throw new Exception(response.Message);
        }


        //Role silinmeyeceği için kaldırıldı
        //[HttpDelete("{roleId}/delete-role"), ServiceLog]
        //public async Task<ApiResponse<DeleteRoleApiResponse>> DeleteRoleAsync(Guid roleId)
        //    => Response(await _userService.DeleteRoleAsync(roleId), ResultMessage.SuccessResultMessage);



        [HttpPost("add-role-to-user"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserAdd, Permissions.UserEdit })]
        public async Task<ApiResponse<AddRoleToUserApiResponse>> AddRoleToUserAsync(AddRoleToUserApiRequest request)
        {
            var response = await _userService.AddRoleToUserAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1030")) : throw new Exception(response.Message);
        }


        [HttpPost("remove-role-from-user"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserAdd, Permissions.UserEdit })]
        public async Task<ApiResponse<RemoveRoleFromUserApiResponse>> RemoveRoleFromUserAsync(RemoveRoleFromUserApiRequest request)
        {
            var response = await _userService.RemoveRoleFromUserAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1031")) : throw new Exception(response.Message);
        }


        [HttpGet("{username}/assign-role-to-user"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserAdd, Permissions.UserEdit })]
        public async Task<ApiResponse<AssignRolesToUserApiResponse>> AssignRolesToUserAsync(string userName)
        {
            var response = await _userService.AssignRolesToUserAsync(userName);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1032")) : throw new Exception(response.Message);
        }


        [HttpGet("role-select"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleList })]
        public async Task<ApiResponse<List<RoleSelectApiData>>> GetRoleSelectAsync()
        {
            var response = await _userService.GetRoleSelectAsync();
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1033")) : throw new Exception(response.Message);
        }


        [HttpPost("permissions-details")]
        [Permissions(Permissions = new[] { Permissions.PermissionList })]
        public async Task<ApiPagedResponse<PermissionApiData>> GetPermissionPaginationAsync([FromBody] DataSourceApiRequest paging)
        {
            var language = _httpContextAccessor.GetLanguage();
            return await _userService.GetPermissionPaginationAsync(paging, language);
        }


        [HttpGet("{permissionId}/permission-detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminPermissionView })]
        public async Task<ApiResponse<PermissionApiResponse>> GetPermissionAsync(Guid permissionId)
        {
            var response = await _userService.GetPermissionAsync(permissionId, LanguageExtensions.GetLanguage(_httpContextAccessor));
            return response.Status == 200 ? Response(response, new MessageResponse("#S1034")) : throw new Exception(response.Message);
        }


        [HttpDelete("delete-permission"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminPermissionDelete })]
        public async Task<ApiResponse<DeletePermissionApiResponse>> DeletePermissionAsync(Guid permissionId)
        {
            var response = await _userService.DeletePermissionAsync(permissionId);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1035")) : throw new Exception(response.Message);
        }


        [HttpPost("create-permission"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminPermissionAdd })]
        public async Task<ApiResponse<CreatePermissionApiResponse>> CreatePermissionAsync(CreatePermissionApiRequest request)
        {
            var response = await _userService.CreatePermissionAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1036")) : throw new Exception(response.Message);
        }


        [HttpPut("update-permission"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminPermissionEdit })]
        public async Task<ApiResponse<UpdatePermissionApiResponse>> UpdatePermissionAsync(UpdatePermissionApiRequest request)
        {
            var response = await _userService.UpdatePermissionAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1037")) : throw new Exception(response.Message);
        }


        [HttpPost("role-permission"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleAdd, Permissions.RoleEdit })]
        public async Task<ApiResponse<RolePermissionApiResponse>> RolePermissionAsync(RolePermissionApiRequest request)
        {
            var response = await _userService.RolePermissionAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1038")) : throw new Exception(response.Message);
        }


        [HttpPost("role-permissions"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleAdd, Permissions.RoleEdit })]
        public async Task<ApiResponse<RolePermissionsApiResponse>> RolePermissionsAsync(RolePermissionsApiRequest request)
        {
            var response = await _userService.RolePermissionsAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1039")) : throw new Exception(response.Message);
        }


        [HttpPost("remove-permission-role"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleAdd, Permissions.RoleEdit })]
        public async Task<ApiResponse<RemovePermissionRoleApiResponse>> RemovePermissionRoleAsync(RemovePermissionRoleApiRequest request)
        {
            var response = await _userService.RemovePermissionRoleAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1040")) : throw new Exception(response.Message);
        }


        [HttpPost("update-permission-role"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleAdd, Permissions.RoleEdit })]
        public async Task<ApiResponse<UpdatePermissionRoleApiResponse>> UpdatePermissionRoleAsync(UpdatePermissionRoleApiRequest request)
        {
            var response = await _userService.UpdatePermissionRoleAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1041")) : throw new Exception(response.Message);
        }


        [HttpGet("permission-select"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.PermissionList })]
        public async Task<ApiResponse<List<PermissionSelectApiResponseData>>> GetPermissionSelectAsync()
        {
            var response = await _userService.GetPermissionSelectAsync();
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1042")) : throw new Exception(response.Message);
        }


        [HttpGet("{groupId}/group-detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminGroupView })]
        public async Task<ApiResponse<List<GroupData>>> GetGroupAsync(Guid groupId)
        {
            var response = await _userService.GetGroupAsync(groupId);
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1043")) : throw new Exception(response.Message);
        }


        [HttpDelete("{roleId}/delete-group"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminGroupDelete })]
        public async Task<ApiResponse<DeleteGroupApiResponse>> DeleteGroupAsync(Guid groupId)
        {
            var response = await _userService.DeleteGroupAsync(groupId);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1044")) : throw new Exception(response.Message);
        }


        [HttpPost("create-group"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminGroupAdd })]
        public async Task<ApiResponse<CreateGroupApiResponse>> CreateGroupAsync(CreateGroupApiRequest request)
        {
            var response = await _userService.CreateGroupAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1045")) : throw new Exception(response.Message);
        }


        [HttpPut("update-group"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminGroupEdit })]
        public async Task<ApiResponse<UpdateGroupApiResponse>> UpdateGroupAsync(UpdateGroupApiRequest request)
        {
            var response = await _userService.UpdateGroupAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1046")) : throw new Exception(response.Message);
        }


        [HttpPost("group-add-user"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminGroupAddUser })]
        public async Task<ApiResponse<GroupAddUserApiResponse>> GroupAddUserAsync(GroupAddUserApiRequest request)
        {
            var response = await _userService.GroupAddUserAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1047")) : throw new Exception(response.Message);
        }


        [HttpGet("group-select"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminGroupList })]
        public async Task<ApiResponse<List<GroupSelectApiResponseData>>> GetGroupSelectAsync()
        {
            var response = await _userService.GetGroupSelectAsync();
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1048")) : throw new Exception(response.Message);
        }


        [HttpDelete("delete-token"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTokenDelete })]
        public async Task<ApiResponse<DeleteTokenApiResponse>> DeleteTokenAsync(Guid tokenId)
        {
            var response = await _userService.DeleteTokenAsync(tokenId);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1049")) : throw new Exception(response.Message);
        }


        [HttpGet("{roleId}/role-users"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.RoleView })]
        public async Task<ApiResponse<List<string>>> GetRoleUsersAsync(Guid roleId)
            => Response(await _userService.GetRoleUsersAsync(roleId), new MessageResponse("#S1050"));


        [HttpGet("{userName}/user-roles"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserView })]
        public async Task<ApiResponse<List<string>>> GetUserRolesAsync(string userName)
            => Response(await _userService.GetUserRolesAsync(userName), new MessageResponse("#S1051"));

        [HttpGet("{userId}/user-permissions"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.UserView })]
        public async Task<ApiResponse<List<UserPermissionApiResponse>>> GetUserPermissionsAsync(Guid userId)
        {
            var language =  _httpContextAccessor.GetLanguage();
            return Response(await _userService.GetUserPermissionsAsync(userId, language), new MessageResponse("#S1052"));
        }

        [AllowAnonymous]
        [HttpPost("control-token"), ServiceLog]
        public async Task<bool> ControlToken(string token)
            => await _userService.ControlTokenAsync(token);

        [AllowAnonymous]
        [HttpGet("{userName}/forgot-password"), ServiceLog]
        public async Task<ApiResponse<ForgotPasswordApiResponse>> ForgotPassword(string userName)
        {
            var language =  _httpContextAccessor.GetLanguage();
            string domain = _httpContextAccessor.GetDomain();
            
            var response = await _userService.ForgotPasswordAsync(userName, domain, language);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1053")) : throw new Exception(response.Message);
        }

        [AllowAnonymous]
        [HttpPost("forgot-password-token"), ServiceLog]
        public async Task<ApiResponse<ForgotPasswordApiResponse>> ForgotPasswordToken(ForgotPasswordApiRequest request)
        {
            var response = await _userService.ForgotPasswordTokenAsync(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1054")) : throw new Exception(response.Message);
        }

        [AllowAnonymous]
        [HttpPut("token"), ServiceLog]
        public async Task<ApiResponse<TokenApiResponse>> ControlTokenTokenAsync([FromForm] ControlOtpTokenApiRequest request)
        {
            var subDomain = _httpContextAccessor.GetSubDomain();

            var tenants = await _userService.GetTenantsAsync();

            var tenant = tenants.Where(tenant => tenant.Name.ToLower() == subDomain.ToLower()).FirstOrDefault();

            if (tenant != null)
                request.TenantId = tenant.TenantId;
            else if (subDomain.ToLower() != "localhost" && subDomain.ToLower() != "mobile-bff" && subDomain.ToLower() != "admin" && subDomain.ToLower() != "")
                throw new Exception("#E1002 " + subDomain + " Subdomain is wrong, tenant couldn't found.");

            var language =  _httpContextAccessor.GetLanguage();
            var tokenResponse = await _userService.ControlOtpTokenAsync(request, language);

            if (tokenResponse.LoginStatus == LoginStatus.FirstLogin)
            {
                return Response(tokenResponse, new ResultMessage()
                {
                    CallToActionType = CallToActionType.ChangePassword,
                    Description = $"#I1002 You must change your password as it is the first login.",
                    Title = $"#I1002 You must change your password as it is the first login.",
                });
            }

            return Response(tokenResponse, new MessageResponse("#S1055"));
        }

        [HttpGet("{tenantId}/tenant-id"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminTenantView })]
        public async Task<ApiResponse<GetTenantIdApiResponse>> GetTenantIdAsync(Guid tenantId)
            => Response(await _userService.GetTenantIdAsync(tenantId), new MessageResponse("#S1056"));

        [HttpPost("tenant-details")]
        [Permissions(Permissions = new[] { Permissions.AdminTenantList })]
        public async Task<ApiPagedResponse<TenantPaginationApiResponse>> GetTenantPaginationAsync([FromBody] DataSourceApiRequest paging)
           => await _userService.GetTenantPaginationAsync(paging);

        [HttpPost("admin-permission-pagination")]
        public async Task<ApiPagedResponse<AdminPermissionPaginationApiResponse>> AdminPermissionPagination([FromBody] DataSourceApiRequest paging)
        {
            string language =  _httpContextAccessor.GetLanguage();

            return await _userService.AdminPermissionPaginationAsync(paging, language);
        }

        [HttpGet("admin-permission-select")]
        public async Task<ApiResponse<List<GetAdminPermissionSelectApiResponseItem>>> GetAdminSelect()
        {
            var response = await _userService.GetAdminPermissionSelect();
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1134")) : throw new Exception(response.Message);
        }

        [HttpPost("admin-role")]
        public async Task<ApiResponse<CreateAdminRoleApiResponse>> CreateAdminRole([FromBody] CreateAdminRoleApiRequest request)
        {
            var response = await _userService.CreateAdminRole(request);
            return response.Status == 200 ? Response(response, new MessageResponse("#S1135")) : throw new Exception(response.Message);
        }

        [HttpGet("admin-role-select")]
        public async Task<ApiResponse<List<AdminRoleSelectApiResponseItem>>> GetRoleAdminSelect()
        {
            var response = await _userService.GetRoleAdminSelect();
            return response.Status == 200 ? Response(response.Data, new MessageResponse("#S1136")) : throw new Exception(response.Message);
        }

        [HttpPost("admin-grid")]
        public async Task<ApiPagedResponse<AdminGridApiResponseItem>> AdminGrid([FromBody] DataSourceApiRequest request)
        {
            return await _userService.GetAdminGrid(request);
        }

        [HttpPost("role-admin-pagination")]
        public async Task<ApiPagedResponse<RoleAdminPermissionResponse>> RoleAdminPagination([FromBody] DataSourceApiRequest paging)
        {
            string language =  _httpContextAccessor.GetLanguage();

            return await _userService.RoleAdminPagination(paging, language);
        }

        [HttpGet("admin-user/{userId}")]
        public async Task<ApiResponse<GetAdminUserByIdApiResponse>> GetAdminUserByIdAsync(Guid userId)
        {
            var request = new GetAdminUserByIdApiRequest
            {
                Id = userId
            };

            var response = await _userService.GetAdminUserByIdApiAsync(request);
            return new ApiResponse<GetAdminUserByIdApiResponse>
            {
                Data = response,
                Message = new MessageResponse("#S1061")
            };
        }

        [HttpPost("create-admin-user-default-password")]
        public async Task<ApiResponse<CreateAdminUserWithDefaultPasswordApiResponse>> CreateAdminUserWithDefaultPasswordAsync(
                                                                    [FromBody] CreateAdminUserWithDefaultPasswordApiRequest request)
        {
            string language =  _httpContextAccessor.GetLanguage();
            
            var adminAppUrl = _configuration["adminAppUrl"];

            var response = await _userService.CreateAdminUserWithDefaultPasswordAsync(request, language, adminAppUrl);

            return new ApiResponse<CreateAdminUserWithDefaultPasswordApiResponse>
            {
                Message = response.Status == 200 ? new MessageResponse("#S1062")
                                                 : throw new Exception(response.Message)
            };
        }

        [HttpPut("admin-user")]
        public async Task<ApiResponse<UpdateAdminUserApiResponse>> UpdateAdminUserAsync([FromBody] UpdateAdminUserApiRequest request)
        {
            string language =  _httpContextAccessor.GetLanguage();
            var adminAppUrl = _configuration["adminAppUrl"];

            var response = await _userService.UpdateAdminUserAsync(request, language, adminAppUrl);

            return new ApiResponse<UpdateAdminUserApiResponse>
            {
                Message = response.Status == 200 ? new MessageResponse("#S1063")
                                                 : throw new Exception(response.Message)
            };
        }

        private string GetTenantName()
        {
            var tenantName = _httpContextAccessor.HttpContext.User.Claims
                                            .Where(x => x.Type == "tname")
                                            .Select(x => x.Value)
                                            .FirstOrDefault();
            return tenantName;
        }
    }
}
