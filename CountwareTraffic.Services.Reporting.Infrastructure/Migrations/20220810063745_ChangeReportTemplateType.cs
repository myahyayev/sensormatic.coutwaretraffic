﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Reporting.Infrastructure.Migrations
{
    public partial class ChangeReportTemplateType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ReportSchedules_TemplateTypes_TemplateTypeId",
                schema: "reports",
                table: "ReportSchedules");

            migrationBuilder.DropIndex(
                name: "IX_ReportSchedules_TemplateTypeId",
                schema: "reports",
                table: "ReportSchedules");

            migrationBuilder.DropColumn(
                name: "TemplateTypeId",
                schema: "reports",
                table: "ReportSchedules");

            migrationBuilder.AddColumn<int>(
                name: "TemplateTypeId",
                schema: "reports",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TemplateTypeId",
                schema: "reports",
                table: "Reports",
                column: "TemplateTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_TemplateTypes_TemplateTypeId",
                schema: "reports",
                table: "Reports",
                column: "TemplateTypeId",
                principalSchema: "reports",
                principalTable: "TemplateTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_TemplateTypes_TemplateTypeId",
                schema: "reports",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TemplateTypeId",
                schema: "reports",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "TemplateTypeId",
                schema: "reports",
                table: "Reports");

            migrationBuilder.AddColumn<int>(
                name: "TemplateTypeId",
                schema: "reports",
                table: "ReportSchedules",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ReportSchedules_TemplateTypeId",
                schema: "reports",
                table: "ReportSchedules",
                column: "TemplateTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_ReportSchedules_TemplateTypes_TemplateTypeId",
                schema: "reports",
                table: "ReportSchedules",
                column: "TemplateTypeId",
                principalSchema: "reports",
                principalTable: "TemplateTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
