﻿using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Core
{
    public interface IBrandModelLookupRepository : IRepository<BrandModelLookup>, IScopedDependency
    {
        Task<IEnumerable<BrandModelLookup>> GetAllAsync(Guid brandId);
        Task<BrandModelLookup> GetByNameAsync(string name);
    }
}
