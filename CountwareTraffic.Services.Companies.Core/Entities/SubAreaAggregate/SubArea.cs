﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class SubArea : AggregateRoot, IAuditable, IDateable, IDeletable, ITraceable, IMultiTenant
    {
        private string _name;
        private string _description;
        private Guid _areaId;
        private int _subAreaStatus;

        public string Name => _name;
        public string Description => _description;
        public Guid AreaId => _areaId;
        public SubAreaStatus SubAreaStatus { get; private set; }


        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public bool AuditIsDeleted { get; set; }
        public Guid TenantId { get; set; }
        #endregion default properties

        private SubArea() { }

        public static SubArea Create(string name, string description, Guid areaId)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("#E1447 "+nameof(name));

            if (name.Length > 150)
                throw new ArgumentException("#E1448 " + nameof(name));

            if (!description.IsNullOrWhiteSpace() && description.Length > 2500)
                throw new ArgumentException("#E1449 " + nameof(description));

            SubArea subArea = new()
            {
                _name = name,
                _description = description,
                _areaId = areaId,
                _subAreaStatus = SubAreaStatus.Created.Id
            };

            return subArea;
        }

        public void WhenCreated(Guid subAreaId, string name, Guid areaId, string areaName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid countryLookupId, Guid cityId, string cityName, Guid districtId, string districtName)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("#E1450 " + nameof(name));

            if (subAreaId == Guid.Empty)
                throw new ArgumentNullException("#E1451 " + nameof(subAreaId));

            if (areaId == Guid.Empty)
                throw new ArgumentNullException("#E1452 " + nameof(areaId));

            if (string.IsNullOrWhiteSpace(areaName))
                throw new ArgumentNullException("#E1453 " + nameof(areaName));

            AddEvent(new SubAreaCreated(subAreaId, name, areaId, areaName, regionId, regionName, countryId, countryName, companyId, companyName, countryLookupId, cityId, cityName, districtId, districtName));
        }

        public void WhenChanged(string name, string description)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("#E1454 " + nameof(name));

            if (name.Length > 150)
                throw new ArgumentException("#E1455 " + nameof(name));

            if (!description.IsNullOrWhiteSpace() && description.Length > 2500)
                throw new ArgumentException("#E1456 " + nameof(description));

            if (name != _name)
                AddEvent(new SubAreaChanged(_id, name, _name));

            _name = name;
            _description = description;
        }

        public void WhenDeleted(Guid subAreaId)
        {
            if (subAreaId == Guid.Empty)
                throw new ArgumentNullException("#E1457 " + nameof(subAreaId));

            AddEvent(new SubAreaDeleted(subAreaId));
        }

        public void WhenCreatedCompleted() => _subAreaStatus = SubAreaStatus.Completed.Id;

        public void WhenCreatedRejected() => _subAreaStatus = SubAreaStatus.Rejected.Id;

        public void WhenChangedRejected(string oldName) => _name = oldName;

        public void WhenDeletedRejected() => AuditIsDeleted = false;

        public void WhenHierarchyCreated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, string subAreaName) => AddEvent(new SubAreaHierarchyCreated(tenantId, companyId, countryId, regionId, areaId, subAreaId, subAreaName));

        public void WhenHierarchyChanged(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, string subAreaName) => AddEvent(new SubAreaHierarchyUpdated(tenantId, companyId, countryId, regionId, areaId, subAreaId, subAreaName));

        public void WhenHierarchyDeleted(Guid tenantId, Guid companyId,  Guid countryId,  Guid regionId,  Guid areaId, Guid subAreaId, string subAreaName) => AddEvent(new SubAreaHierarchyDeleted(tenantId, companyId, countryId, regionId, areaId, subAreaId, subAreaName));

    }
}
