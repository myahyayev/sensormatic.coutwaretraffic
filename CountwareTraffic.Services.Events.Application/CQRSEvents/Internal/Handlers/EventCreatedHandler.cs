﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class EventCreatedHandler : IEventHandler<EventCreated>
    {
        private readonly IEventElasticSearchRepository _eventElasticSearchRepository;
        public EventCreatedHandler(IEventElasticSearchRepository eventElasticSearchRepository)
        {
            _eventElasticSearchRepository = eventElasticSearchRepository;
        }

        public async Task HandleAsync(EventCreated e)
        {
            await _eventElasticSearchRepository.AddAsync(new EventCreateElasticDto
            {
                EventId = e.EventId,
                DeviceId = e.DeviceId,
                DeviceName = e.DeviceName,
                EventDate = e.EventDate,
                EventStartDate=e.EventStartDate,
                EventEndtDate =e.EventEndtDate,
                TenantId = e.TenantId,
                AreaId = e.AreaId,
                SubAreaName = e.SubAreaName,
                SubAreaId = e.SubAreaId,
                RegionName = e.RegionName,
                AreaName = e.AreaName,
                CityId = e.CityId,
                CityName = e.CityName,
                CompanyId = e.CompanyId,
                CompanyName = e.CompanyName,
                CountryId = e.CountryId,
                CountryName = e.CountryName,
                DirectionType = e.DirectionType,
                DistrictId = e.DistrictId,
                DistrictName = e.DistrictName,
                OnTimeIOCount = e.OnTimeIOCount,
                RegionId = e.RegionId
            });
        }
    }
}
