﻿using Convey.CQRS.Queries;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreateRole : IQuery<IdentityCreateRoleResponse>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Permissions { get; set; }
    }
}
