﻿using CountwareTraffic.Services.Companies.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class DistrictLookupEntityTypeConfiguration : IEntityTypeConfiguration<DistrictLookup>
    {
        public void Configure(EntityTypeBuilder<DistrictLookup> builder)
        {
            builder.ToTable("DistrictLookups", SchemaNames.Areas);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder.HasOne<CityLookup>().WithMany().HasForeignKey(x => x.CityId);
        }
    }
}
