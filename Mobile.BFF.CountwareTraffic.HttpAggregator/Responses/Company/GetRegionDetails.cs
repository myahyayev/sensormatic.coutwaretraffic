﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetRegionDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ManagerName { get; set; }
        public Guid? ManagerId { get; set; }
        public Guid CountryId { get; set; }
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
    }
}
