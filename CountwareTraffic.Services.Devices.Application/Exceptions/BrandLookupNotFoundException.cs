﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class BrandLookupNotFoundException : AppException
    {
        public Guid Id { get; }
        public BrandLookupNotFoundException(Guid id)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1551 BrandLookup with id: {id} was not found") }, 404, ResponseMessageType.Error)
        {
            Id = id;
        }
    }
}
