﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;

namespace CountwareTraffic.Services.Companies.Core
{
    public record AreaContact : IDeletableValueObject
    {
        private string _gsmNumber;
        private string _gsmDialCode;
        private string _gsmCountryCode;
        private string _phoneDialCode;
        private string _phoneNumber;
        private string _phoneCountryCode;
        private string _emailAddress;

        public string GsmNumber => _gsmNumber;
        public string GsmCountryCode => _gsmCountryCode;
        public string GsmDialCode => _gsmDialCode;
        public string PhoneDialCode => _phoneDialCode;
        public string PhoneNumber => _phoneNumber;
        public string PhoneCountryCode => _phoneCountryCode;

        public string EmailAddress => _emailAddress;

        private AreaContact() { }

        public static AreaContact Create(string gsmNumber, string gsmDialCode, string gsmCountryCode, string phoneNumber, string phoneDialCode, string phoneCountryCode,  string emailAddress)
        {
            if (!gsmDialCode.IsNullOrWhiteSpace() && gsmDialCode.Length > 6)
                throw new InvalidIContactException(nameof(gsmDialCode), gsmDialCode);

            if (!phoneDialCode.IsNullOrWhiteSpace() && phoneDialCode.Length > 6)
                throw new InvalidIContactException(nameof(phoneDialCode), phoneDialCode);

            if (!phoneNumber.IsNullOrWhiteSpace() && phoneNumber.Length > 20)
                throw new InvalidIContactException(nameof(phoneNumber), phoneNumber);

            if (!gsmNumber.IsNullOrWhiteSpace() && gsmNumber.Length > 20)
                throw new InvalidIContactException(nameof(gsmNumber), gsmNumber);

            if (!emailAddress.IsNullOrWhiteSpace() && emailAddress.Length > 120)
                throw new InvalidIContactException(nameof(emailAddress), emailAddress);

            if (!gsmCountryCode.IsNullOrWhiteSpace() && gsmCountryCode.Length != 2)
                throw new InvalidIContactException(nameof(gsmCountryCode), gsmCountryCode);

            if (!phoneCountryCode.IsNullOrWhiteSpace() && phoneCountryCode.Length != 2)
                throw new InvalidIContactException(nameof(phoneCountryCode), phoneCountryCode);

            return new AreaContact
            {
                _gsmNumber = gsmNumber,
                _gsmDialCode = gsmDialCode,
                _gsmCountryCode = gsmCountryCode,
                _phoneDialCode = phoneDialCode,
                _phoneCountryCode = phoneCountryCode,
                _phoneNumber = phoneNumber,
                _emailAddress = emailAddress
            };
        }

        public void WhenChanged(string gsmNumber, string gsmDialCode, string gsmCountryCode, string phoneNumber, string phoneDialCode, string phoneCountryCode, string emailAddress)
        {
            if (!gsmDialCode.IsNullOrWhiteSpace() && gsmDialCode.Length > 6)
                throw new InvalidIContactException(nameof(gsmDialCode), gsmDialCode);

            if (!phoneDialCode.IsNullOrWhiteSpace() && phoneDialCode.Length > 6)
                throw new InvalidIContactException(nameof(phoneDialCode), phoneDialCode);

            if (!phoneNumber.IsNullOrWhiteSpace() && phoneNumber.Length > 20)
                throw new InvalidIContactException(nameof(phoneNumber), phoneNumber);

            if (!gsmNumber.IsNullOrWhiteSpace() && gsmNumber.Length > 20)
                throw new InvalidIContactException(nameof(gsmNumber), gsmNumber);

            if (!emailAddress.IsNullOrWhiteSpace() && emailAddress.Length > 120)
                throw new InvalidIContactException(nameof(emailAddress), emailAddress);

            if (!gsmCountryCode.IsNullOrWhiteSpace() && gsmCountryCode.Length != 2)
                throw new InvalidIContactException(nameof(gsmCountryCode), gsmCountryCode);

            if (!phoneCountryCode.IsNullOrWhiteSpace() && phoneCountryCode.Length != 2)
                throw new InvalidIContactException(nameof(phoneCountryCode), phoneCountryCode);

            _gsmNumber = gsmNumber;
                _gsmDialCode = gsmDialCode;
            _gsmCountryCode = gsmCountryCode;
            _phoneDialCode = phoneDialCode;
            _phoneCountryCode = phoneCountryCode;
            _phoneNumber = phoneNumber;
            _emailAddress = emailAddress;
        }
    }
}

