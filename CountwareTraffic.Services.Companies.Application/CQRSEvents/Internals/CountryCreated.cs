﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CountryCreated : Convey.CQRS.Events.IEvent
    {
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public Guid ComapnyId { get; init; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
    }
}
