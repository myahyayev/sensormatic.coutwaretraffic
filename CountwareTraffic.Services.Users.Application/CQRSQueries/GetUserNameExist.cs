﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserNameExist : IQuery<IdentityUserNameExistResponse>
    {
        public string UserName { get; set; }
    }
}
