﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application.Objects.Responses
{
    public class IdentityRoleAdminPaginationResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int TotalCount { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
        public int HasNextPage { get; set; }
        public int Next { get; set; }
        public int Prev { get; set; }
        public List<IdentityRoleAdminPermission> Data { get; set; }
    }

    public class IdentityRoleAdminPermission
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<IdentityRoleAdminPermissionItem> Permissions { get; set; }
    }

    public class IdentityRoleAdminPermissionItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public string DescriptionLabel { get; set; }
    }
}
