﻿using Sensormatic.Tool.Ioc;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Core
{
    public interface ICityLookupRepository : IRepository<CityLookup>, IScopedDependency
    {
        Task<IEnumerable<CityLookup>> GetAllAsync(Guid countryId);
    }
}
