﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class CreateEventHandler : ICommandHandler<CreateEvent>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateEventHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task HandleAsync(CreateEvent command)
        {
            var eventRepository = _unitOfWork.GetRepository<IEventRepository>();
            var deviceRepository = _unitOfWork.GetRepository<IDeviceRepository>();

            var device = await deviceRepository.GetAsync(command.DeviceId);

            if (device == null) return;

            var @event = Event.Create(command.Description, command.OnTimeIOCount,command.EventDate.ToUniversalTime(),command.EventStartDate.ToUniversalTime(), command.EventEndDate.ToUniversalTime(), command.DeviceId, command.DirectionTypeId, command.TenantId);
            //if add was success Eventdbprovider will call @event.CompleteCreation as OnDomainEventRaiseAfterSuccess and this event will publish to eventcreate queue, this queue will be consumed by CreateEventInElasticConsumer for feeding elastic

            //bool isThereAlready = eventRepository
            //    .GetQuery(command.TenantId)
            //    .Where(e => e.EventStartDate == command.EventStartDate)
            //    .Where(e => e.EventEndDate == command.EventEndDate)
            //    .Where(e => e.DirectionType.Id == command.DirectionTypeId)
            //    .Where(e => e.DeviceId == command.DeviceId)
            //    .Any();

            //if (isThereAlready) return;
            
            await eventRepository.AddAsync(@event);

            @event.CompleteCreation(device.Name, device.SubAreaId, device.SubAreaName, device.AreaId, device.AreaName, device.CityId, device.CityName, device.DistrictId, device.DistrictName, device.RegionId, device.RegionName, device.CountryId, device.CountryName, device.CompanyId, device.CompanyName);

            await _unitOfWork.CommitAsync();

        }
    }
}
