﻿namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public enum ElasticHierarchyLevel
    {
        Tenant = 1,
        Company = 2,
        Country = 3,
        Region = 4,
        Area = 5,
        SubArea = 6,
        Device = 7,
    }
}
