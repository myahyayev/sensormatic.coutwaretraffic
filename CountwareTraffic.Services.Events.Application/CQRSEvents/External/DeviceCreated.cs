﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class DeviceCreated : Convey.CQRS.Events.IEvent
    {
        public Guid DeviceId { get; init; }
        public Guid SubAreaId { get; set; }
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string SubAreaName { get; init; }
        public string Name { get; init; }
        public Guid UserId { get; init; }
        public Guid TenantId { get; set; }
        public string AreaName { get; set; }
        public Guid AreaId { get; set; }
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid CountryLookupId { get; set; }
        public Guid CityId { get; set; }
        public string CityName { get; set; }
        public Guid DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int ControlFrequency { get; set; }
    }
}
