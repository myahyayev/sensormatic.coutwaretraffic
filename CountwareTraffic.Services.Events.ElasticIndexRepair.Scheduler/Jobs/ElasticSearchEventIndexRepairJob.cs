﻿using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.ElasticIndexRepair.Scheduler
{
    public class ElasticSearchEventIndexRepairJob : IJob
    {
        private readonly IEventRepository _eventRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IEventElasticSearchRepository _eventElasticSearchRepository;
        private readonly AppConfig _appConfig;
        private readonly ILogger<ElasticSearchEventIndexRepairJob> _logger;

        public ElasticSearchEventIndexRepairJob(IEventRepository eventRepository,
            IDeviceRepository deviceRepository,
            IEventElasticSearchRepository eventElasticSearchRepository,
            IOptions<AppConfig> appConfig,
            ILogger<ElasticSearchEventIndexRepairJob> logger)
        {
            _eventRepository = eventRepository;
            _deviceRepository = deviceRepository;
            _eventElasticSearchRepository = eventElasticSearchRepository;
            _appConfig = appConfig.Value;
            _logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            _logger.LogInformation("ElasticSearchEventIndexRepairJob starting...");

            await Run();

            _logger.LogInformation("ElasticSearchEventIndexRepairJob ended...");
        }

        private async Task Run()
        {
            var startDate = DateTime.UtcNow.AddDays(_appConfig.StartDays);

            var eventQuery = _eventRepository.GetQuery(null);
            var deviceQuery = _deviceRepository.GetQuery(null);

            var query = eventQuery
                .Include(x => x.DirectionType)
                .Join(deviceQuery, e => e.DeviceId, d => d.Id, (e, d) => new { Event = e, Device = d })
                .Where(x => x.Event.EventDate >= startDate)
                .OrderBy(x => x.Event.EventDate);

            var sqlCount = query.Count();
            var elasticCount = await _eventElasticSearchRepository.GetEventCountAsync(startDate);

            if (sqlCount == elasticCount)
            {
                _logger.LogInformation($"ElasticSearchEventIndexRepairJob Sql data count ({sqlCount}) equals to elastic data count ({elasticCount}), so syncing will not continue!");
                return;
            };

            _logger.LogInformation($"ElasticSearchEventIndexRepairJob Sql data count ({sqlCount}) does not equal to elastic data count ({elasticCount})!");


            var hasNextPage = false;
            var page = 0;
            var pageSize = _appConfig.BatchCount;

            try
            {
                do
                {
                    var elasticDtos = await query
                    .Select(x => new EventCreateElasticDto
                    {

                        EventId = x.Event.Id,
                        DeviceId = x.Device.Id,
                        DeviceName = x.Device.Name,
                        EventDate = x.Event.EventDate,
                        EventStartDate = x.Event.EventStartDate,
                        EventEndtDate = x.Event.EventEndDate,
                        TenantId = x.Event.TenantId,
                        AreaId = x.Device.AreaId,
                        SubAreaName = x.Device.SubAreaName,
                        SubAreaId = x.Device.SubAreaId,
                        RegionName = x.Device.RegionName,
                        AreaName = x.Device.AreaName,
                        CityId = x.Device.CityId,
                        CityName = x.Device.CityName,
                        CompanyId = x.Device.CompanyId,
                        CompanyName = x.Device.CompanyName,
                        CountryId = x.Device.CountryId,
                        CountryName = x.Device.CountryName,
                        DirectionType = x.Event.DirectionType,
                        DistrictId = x.Device.DistrictId,
                        DistrictName = x.Device.DistrictName,
                        OnTimeIOCount = x.Event.OnTimeIOCount,
                        RegionId = x.Device.RegionId
                    })
                    .Skip(page * pageSize)
                    .Take(pageSize)
                    .ToListAsync();

                    page++;
                    hasNextPage = elasticDtos.Any();

                    if (hasNextPage)
                    {
                        await _eventElasticSearchRepository.AddRangeAsync(elasticDtos);

                        _logger.LogInformation($"ElasticSearchEventIndexRepairJob {elasticDtos.Count} records in elasticsearch Synced!");
                    }

                } while (hasNextPage);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ElasticSearchEventIndexRepairJob error!");
            }
        }
    }
}
