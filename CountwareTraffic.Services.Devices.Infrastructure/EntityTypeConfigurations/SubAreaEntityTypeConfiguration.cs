﻿using CountwareTraffic.Services.Devices.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class SubAreaEntityTypeConfiguration : IEntityTypeConfiguration<SubArea>
    {
        public void Configure(EntityTypeBuilder<SubArea> builder)
        {
            builder.ToTable("SubAreas", SchemaNames.Devices);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired();

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
                .Property(x => x.AreaId)
                .HasField("_areaId")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired();

            builder
               .Property(x => x.AreaName)
               .HasField("_areaName")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
                 .Property(x => x.RegionId)
                 .HasField("_regionId")
                 .UsePropertyAccessMode(PropertyAccessMode.Field)
                 .IsRequired();

            builder
               .Property(x => x.RegionName)
               .HasField("_regionName")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
                .Property(x => x.CountryId)
                .HasField("_countryId")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired();

            builder
               .Property(x => x.CountryName)
               .HasField("_countryName")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
               .Property(x => x.CompanyId)
               .HasField("_companyId")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired();

            builder
               .Property(x => x.CompanyName)
               .HasField("_companyName")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
               .Property(x => x.CountryLookupId)
               .HasField("_countryLookupId")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired();

            builder
              .Property(x => x.CityId)
              .HasField("_cityId")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .IsRequired();

            builder
               .Property(x => x.CityName)
               .HasField("_cityName")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
               .Property(x => x.DistrictId)
               .HasField("_districtId")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired();

            builder
               .Property(x => x.DistrictName)
               .HasField("_districtName")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);
        }
    }
}