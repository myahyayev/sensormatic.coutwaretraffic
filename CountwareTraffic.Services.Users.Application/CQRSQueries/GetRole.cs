﻿using Convey.CQRS.Queries;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetRole : IQuery<List<IdentityRolesResponse>>
    {
    }
}
