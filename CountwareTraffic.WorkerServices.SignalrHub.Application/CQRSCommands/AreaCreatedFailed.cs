﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    [Contract]
    public class AreaCreatedFailed : ICommand
    {
        public Guid AreaId { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string AreaStatus { get; set; }
        public Guid TenantId { get; set; }
    }
}
