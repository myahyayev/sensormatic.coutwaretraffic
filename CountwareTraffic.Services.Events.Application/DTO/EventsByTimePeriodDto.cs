﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class EventsByTimePeriodDto
    {
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid SubAreaId { get; set; }
        public string SubAreaName { get; set; }
        public DateTime Date { get; set; }
        public int EnterCount { get; set; }
        public int ExitCount { get; set; }
    }
}
