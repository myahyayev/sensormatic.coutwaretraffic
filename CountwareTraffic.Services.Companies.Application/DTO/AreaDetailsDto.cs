﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaDetailsDto : AreaDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string AreaTypeName { get; set; }
        public Guid RegionId { get; set; }
        public string Street { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int AreaTypeId { get; set; }
        public string ManagerName { get; set; }
        public Guid? ManagerId { get; set; }

        //contact
        public string GsmNumber { get; set; }
        public string GsmCountryCode { get; set; }
        public string GsmDialCode { get; set; }
        public string PhoneDialCode { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneCountryCode { get; set; }
        public string EmailAddress { get; set; }
        public Guid DistrictId { get; set; }
        public Guid CityId { get; set; }
        public Guid CountryLookupId { get; set; }

        public TimeSpan? WorkingHoursStart { get; set; }
        public TimeSpan? WorkingHoursEnd { get; set; }
        public string WorkingTimeZone { get; set; }
    }
}
