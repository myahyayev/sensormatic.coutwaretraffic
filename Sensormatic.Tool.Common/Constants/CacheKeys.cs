﻿namespace Sensormatic.Tool.Common
{
    public static class CacheKeys
    {
        public const string DeviceRealTimeLatestEvent = "DeviceRealTimeLatestEvent:{0}";
        public const string DeviceTimeRangeEventSet = "DeviceTimeRangeEventSet:{0}";
        public const string DeviceMigrationEventSet = "DeviceMigrationEventSet:{0}";
        public const string DeviceConnectedStatus = "DeviceConnectedStatus:{0}:{1}";
        public const string DeviceConnectedStatusShadow = "DeviceConnectedStatusShadow:{0}:{1}";
    }
}
