﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer
{
    public class UpdateContractApiRequest
    {
        [FromForm(Name = "id")]
        public Guid Id { get; set; }

        [FromForm(Name = "name")]
        public string Name { get; set; }

        [FromForm(Name = "version")]
        public string Version { get; set; }

        [FromForm(Name = "releaseDate")]
        public DateTime ReleaseDate { get; set; }

        [FromForm(Name = "contractBody")]

        public string ContractBody { get; set; }

        [FromForm(Name = "contractType")]
        public string ContractType { get; set; }

        [FromForm(Name = "Language")]
        public string Language { get; set; }
    }
}
