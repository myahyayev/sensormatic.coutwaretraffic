﻿using Quartz;
using Quartz.Spi;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.ReportSchedule.Scheduler
{
    public class AutoDeleteReportJobConsumer : IConsumer<Sensormatic.Tool.QueueModel.ReportDeleted>, ITransientSelfDependency
    {
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IJobFactory _jobFactory;
        public AutoDeleteReportJobConsumer(ISchedulerFactory schedulerFactory, IJobFactory jobFactory)
        {
            _schedulerFactory = schedulerFactory;
            _jobFactory = jobFactory;
        }

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.ReportDeleted queuEvent)
        {
            var scheduler = await _schedulerFactory.GetScheduler();

            scheduler.JobFactory = _jobFactory;

            var jobKey = new JobKey(queuEvent.ReportId.ToString(), $"{queuEvent.TenantId}.group");

            await scheduler.DeleteJob(jobKey);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
