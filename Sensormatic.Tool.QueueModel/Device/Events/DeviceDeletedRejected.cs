﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceDeletedRejected : MessageEnvelope, IQueueEvent
    {
        public DeviceDeletedRejected(Guid deviceId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            DeviceId = deviceId;
        }
        public Guid DeviceId { get; }
        public Guid RecordId { get; init; }
    }
}
