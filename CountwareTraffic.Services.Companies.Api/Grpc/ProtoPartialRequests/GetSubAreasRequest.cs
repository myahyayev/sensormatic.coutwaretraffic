﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class GetSubAreasRequest : SensormaticRequestValidate
    {
        internal Guid _AreaId
        {
            get
            {
                if (Guid.TryParse(areaId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.areaId_ = value.ToString(); }
        }


        internal DataSourceRequest _DataSourceRequest
        {
            get
            {
                if (this.dataSourceRequest_.PagingRequest == null)
                    this.dataSourceRequest_.PagingRequest = new PagingRequest() { Limit = 10, Page = 1 };

                if (this.dataSourceRequest_.PagingRequest.Limit < 1)
                    this.dataSourceRequest_.PagingRequest.Limit = 10;

                if (this.dataSourceRequest_.PagingRequest.Page < 1)
                    this.dataSourceRequest_.PagingRequest.Page = 1;

                return this.dataSourceRequest_;
            }
            set { this.dataSourceRequest_ = value; }
        }

        public override void Validate()
        {
            if (_AreaId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1327 AreaId cannot be null", nameof(_AreaId)));
        }
    }
}
