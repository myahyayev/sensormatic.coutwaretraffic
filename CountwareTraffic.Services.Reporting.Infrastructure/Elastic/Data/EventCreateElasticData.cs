﻿using CountwareTraffic.Services.Reporting.Core;
using Sensormatic.Tool.ElasticSearch;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{

    /// <summary>
    /// EventId is representing as Id
    /// </summary>
    public class EventCreateElasticData : ElasticSearchEntity, IMultiTenant
    {
        public EventCreateElasticData(Guid tenantId, Guid eventid)
        {
            Id = eventid;
            TenantId = tenantId;
        }

        //EventInfo
        public DirectionType DirectionType { get; set; }
        public DateTime EventDate { get; set; }
        //todo:no need to start and end date for now
        //public DateTime EventStartDate { get; set; }
        //public DateTime EventEndtDate { get; set; }

        //public int EnterCount { get; set; }
        //public int ExitCount { get; set; }
        public int OnTimeIOCount { get; set; }
        //public int CurrentCount { get; set; }

        //DeviceInfo
        public Guid DeviceId { get; set; }
        public string DeviceName { get; set; }
        public Guid SubAreaId { get; set; }
        public string SubAreaName { get; set; }
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid CityId { get; set; }
        public string CityName { get; set; }
        public Guid DistrictId { get; set; }
        public string DistrictName { get; set; }
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }

        public Guid TenantId { get; set; }
    }


    public class DirectionType : Enumeration
    {
        public static DirectionType Unknown = new(1, nameof(Unknown));
        public static DirectionType Inwards = new(2, nameof(Inwards));
        public static DirectionType Outward = new(3, nameof(Outward));

        public DirectionType(int id, string name) : base(id, name) { }

        public static IEnumerable<DirectionType> List() => new[] { Unknown, Inwards, Outward };


        public static DirectionType FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            return state;
        }

        public static DirectionType From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            return state;
        }
    }
}
