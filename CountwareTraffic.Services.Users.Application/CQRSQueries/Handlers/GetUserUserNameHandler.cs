﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserUserNameHandler : IQueryHandler<GetUserUserName, IdentityUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetUserUserNameHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<IdentityUserResponse> HandleAsync(GetUserUserName query)
        {
            return await _identityServerService.GetUserUserNameAsync(query.UserName);
        }
    }
}
