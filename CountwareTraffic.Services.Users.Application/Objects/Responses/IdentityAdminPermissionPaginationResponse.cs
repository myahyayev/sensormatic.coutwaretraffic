﻿using System.Collections.Generic;
using System;

namespace CountwareTraffic.Services.Users.Application.Objects.Responses
{
    public class IdentityAdminPermissionPaginationResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<IdentityServerPermissionPagination> Data { get; set; }
        public int TotalCount { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
        public int HasNextPage { get; set; }
        public int Next { get; set; }
        public int Prev { get; set; }
    }

    public class IdentityServerPermissionPagination
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public string DescriptionLabel { get; set; }
    }
}
