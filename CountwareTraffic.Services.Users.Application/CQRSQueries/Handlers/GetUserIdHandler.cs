﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserIdHandler : IQueryHandler<GetUserId,IdentityGetUserIdResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetUserIdHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<IdentityGetUserIdResponse> HandleAsync(GetUserId query)
        {
            return await _identityServerService.GetUserIdAsync(query.Id);
        }
    }
}
