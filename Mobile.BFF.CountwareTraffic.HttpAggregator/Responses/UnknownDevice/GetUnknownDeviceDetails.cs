﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetUnknownDeviceDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int HttpPort { get; set; }
        public int HttpsPort { get; set; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
        public string SerialNumber { get; set; }
        public DateTime OccurredOn { get; set; }
    }
}
