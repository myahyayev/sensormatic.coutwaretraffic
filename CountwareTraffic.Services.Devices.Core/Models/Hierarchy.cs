﻿using System;

namespace CountwareTraffic.Services.Devices.Core
{
    public class Hierarchy
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }

    public class HierarchyCteData
    {
        public Guid TenantId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CountryId { get; set; }
        public Guid RegionId { get; set; }
        public Guid AreaId { get; set; }
        public Guid SubAreaId { get; set; }
        public Guid DeviceId { get; set; }
        public string TenantName { get; set; }
        public string CompanyName { get; set; }
        public string CountryName { get; set; }
        public string RegionName { get; set; }
        public string AreaName { get; set; }
        public string SubAreaName { get; set; }
        public string DeviceName { get; set; }
    }
}
