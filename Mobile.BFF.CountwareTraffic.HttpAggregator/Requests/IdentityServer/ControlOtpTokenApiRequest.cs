﻿using Microsoft.AspNetCore.Mvc;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ControlOtpTokenApiRequest : SensormaticRequestValidate
    {
        [FromForm(Name = "username")]
        public string UserName { get; set; }

        [FromForm(Name = "password")]
        public string Password { get; set; }

        [FromForm(Name = "tenantId")]
        public Guid? TenantId { get; set; }

        [FromForm(Name = "token")]
        public string Token { get; set; }

        [FromForm(Name = "otp")]
        public string Otp { get; set; }

        public override void Validate()
        {
            if (UserName.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1242 Value cannot be null", nameof(UserName)));

            if (Password.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1243 Value cannot be null", nameof(Password)));
        }
    }
}
