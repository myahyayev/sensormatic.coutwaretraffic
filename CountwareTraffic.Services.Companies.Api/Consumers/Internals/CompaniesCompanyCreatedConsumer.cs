﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesCompanyCreatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.CompanyCreated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesCompanyCreatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.CompanyCreated queuEvent)
        {
            var companyCreated = _eventMapper.Map(queuEvent) as CompanyCreated;
            await _eventDispatcher.PublishAsync(companyCreated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
