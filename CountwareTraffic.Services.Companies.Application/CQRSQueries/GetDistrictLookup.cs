﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetDistrictLookup : IQuery<DistrictLookupDetailsDto>
    {
        public Guid DistrictId { get; set; }
    }
}
