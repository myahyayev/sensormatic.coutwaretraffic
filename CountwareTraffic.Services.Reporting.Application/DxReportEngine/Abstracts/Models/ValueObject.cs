﻿using System.Drawing;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public class ValueObject<T>
    {
        public T Value { get; set; }
        public string Unit { get; set; }
        public Color ForeColor { get; set; }
        public char? Indicator { get; set; }
        public bool IsComparedRow { get; set; }

        public static ValueObject<T> CreateDefault()
        {
            return new()
            {
                //Indicator = '⇌',
                Unit = " ",
                Value = default(T)
            };
        }
    }
}
