using Convey;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using CountwareTraffic.WorkerServices.SignalrHub.Consumer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Sensormatic.Tool.ApplicationInsights;
using Sensormatic.Tool.Queue;
using StackExchange.Redis;
using System;
using System.Net;
using System.Reflection;
using System.Text;

namespace CountwareTraffic.WorkerServices.SignalrHub
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            Sensormatic.Tool.Ioc.IoCGenerator.DoTNet.Current.Start(services, Configuration);

            services.AddApplicationInsights(Configuration)
                    .AddConvey()
                    .AddApplication()
                    .Build();

            services.AddControllers();

            services.AddHostedService<SignalRSubscriber>();
            services.AddHostedService<AutoScaler>();

            services.AddCors(options =>
             {
                 options.AddPolicy("CorsPolicy",
                     builder => builder
                     .AllowAnyMethod()
                     .AllowAnyHeader()
                     .SetIsOriginAllowed((host) => true)
                     .AllowCredentials());
             });

            services.AddSignalR()
                    .AddStackExchangeRedis(Configuration.GetConnectionString("RedisConnection"));

            services.ConfigureAuthService();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection()
               .UseRouting()
               .UseAuthentication()
               .UseAuthorization()
               .UseCors("CorsPolicy")
               .UseEndpoints(endpoints =>
                {
                    endpoints.MapHub<DeviceNotificationsHub>("/hub/devicenotification");
                    endpoints.MapHub<AreaNotificationsHub>("/hub/areanotification");
                    endpoints.MapHub<SubAreaNotificationsHub>("/hub/subareanotification");
                    endpoints.MapHub<ElasticHierarchyNotificationsHub>("/hub/elastichierarchynotification");
                    endpoints.MapHub<UnknownDeviceNotificationsHub>("/hub/unknowndevicenotification");
                    endpoints.MapHub<ReportNotificationsHub>("/hub/reportnotification");
                    endpoints.MapGet("/", async context =>
                    {
                        context.Response.ContentType = "text/html;charset=utf-8";
                        await context.Response.WriteAsync($"<h1>{Assembly.GetExecutingAssembly().GetName().Name} v{Assembly.GetExecutingAssembly().GetName().Version.ToString()} is running</h1>");
                    });
                });
        }
    }

    public static class ServiceCollectionExtensions
    {
        public static void ConfigureAuthService(this IServiceCollection services)
        {
            var authenticationConfig = services.BuildServiceProvider().GetRequiredService<IOptions<AuthenticationConfig>>().Value;

            services
                  .AddAuthentication(options =>
                  {
                      options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                      options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                  })
                   .AddJwtBearer(cfg =>
                   {
                       cfg.RequireHttpsMetadata = true;
                       cfg.SaveToken = true;
                       cfg.TokenValidationParameters = new TokenValidationParameters()
                       {
                           IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authenticationConfig.SignKey)),
                           ValidateAudience = false,
                           ValidateIssuer = false,
                           ValidateLifetime = true,
                           RequireExpirationTime = true,
                           ClockSkew = TimeSpan.Zero,
                           ValidateIssuerSigningKey = true
                       };
                   });

            services.AddAuthorization();
        }
    }
}
