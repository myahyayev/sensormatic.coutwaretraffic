﻿using Sensormatic.Tool.Ioc;

namespace Sensormatic.Tool.Email
{
    public class SmtpOptions : IConfigurationOptions
    {
        public string Server { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public bool EnableSSL { get; set; }
    }
}
