﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRElasticHierarchyChangedConsumer : IConsumer<Sensormatic.Tool.QueueModel.ElasticHierarchyChanged>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRElasticHierarchyChangedConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.ElasticHierarchyChanged queueCommand)
        {
            await _commandDispatcher.SendAsync(new ElasticHierarchyChanged
            {
                TenantId = queueCommand.TenantId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
