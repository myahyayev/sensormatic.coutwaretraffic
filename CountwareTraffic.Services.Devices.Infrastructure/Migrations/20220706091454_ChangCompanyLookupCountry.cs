﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Devices.Infrastructure.Migrations
{
    public partial class ChangCompanyLookupCountry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CompanyLookupId",
                schema: "devices",
                table: "SubAreas",
                newName: "CountryLookupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CountryLookupId",
                schema: "devices",
                table: "SubAreas",
                newName: "CompanyLookupId");
        }
    }
}
