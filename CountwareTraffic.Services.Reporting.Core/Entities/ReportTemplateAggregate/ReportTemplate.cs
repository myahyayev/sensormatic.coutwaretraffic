﻿using Sensormatic.Tool.Efcore;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportTemplate : AggregateRoot, IDateable, IDeletable, ITraceable
    {
        private string _name;
        private string _description;
        private string _assemblyQualifiedName;
        private string _filePath;

        public string Name => _name;
        public string Description => _description;
        public string AssemblyQualifiedName => _assemblyQualifiedName;
        public Collection<ReportTemplateType> Types { get; set; }
        public string FilePath => _filePath;


        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public bool AuditIsDeleted { get; set; }
        #endregion

        private ReportTemplate() { }

        public static ReportTemplate Create(string name, string description, string assemblyQualifiedName)
        {
            return new ReportTemplate
            {
                _name = name,
                _description = description,
                _assemblyQualifiedName = assemblyQualifiedName,
                Types = new Collection<ReportTemplateType>()
            };
        }

        public void SetName(string name) 
            => _name = name;

        public void AddType(int typeId)
        {
            if (!IsInType(typeId))
                return;

            var reportTemplateType =  ReportTemplateType.Create(typeId, _id);

            Types.Add(reportTemplateType);
        }

        public void RemoveType(int typeId)
        {
            if (!IsInType(typeId))
                return;

            var reportTemplateType = ReportTemplateType.Create(typeId, _id);

            Types.Remove(reportTemplateType);
        }


        private bool IsInType(int typeId) 
            => Types.Any(x => x.TypeId == typeId);
    }
}