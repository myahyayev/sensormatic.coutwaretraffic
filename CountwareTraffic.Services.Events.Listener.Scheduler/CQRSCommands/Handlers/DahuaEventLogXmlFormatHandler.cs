﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Cache;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;
using System.Web;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{

    public class DahuaEventLogXmlFormatHandler : ICommandHandler<DahuaEventLogXmlFormat>
    {
        private readonly ILogger<DahuaEventLogXmlFormatHandler> _logger;
        private readonly IIdentityService _identityService;
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly ICache _redisCacheService;
        private readonly IQueueService _queueService;
        public DahuaEventLogXmlFormatHandler(IIdentityService identityService, IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher, ILogger<DahuaEventLogXmlFormatHandler> logger, ICache redisCacheService, IQueueService queueService)
        {
            _identityService = identityService;
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
            _logger = logger;
            _redisCacheService = redisCacheService;
            _queueService = queueService;
        }

        public async Task HandleAsync(DahuaEventLogXmlFormat eventLog)
        {
            var xml = HttpUtility.UrlDecode(eventLog.Xml);

            if (xml.IsNullOrWhiteSpace())
            {
                _logger.LogError("DahuaEventLogXmlFormatHandler xml file is empty!");
                throw new NotImplementedException("This device is not supported yet!");
            }
        }
    }
}
