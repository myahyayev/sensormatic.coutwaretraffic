﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class HierarchyTreeSelectFilterRequest
    {
        public HierarchyLevel HierarchyLevel { get; set; }
        public Guid Id { get; set; }
        public ParentNodeRequest Parent { get; set; }
    }

    public class ParentNodeRequest
    {
        public HierarchyLevel hierarchyLevel { get; set; }
        public Guid Id { get; set; }
        public ParentNodeRequest Parent { get; set; }
    }
}