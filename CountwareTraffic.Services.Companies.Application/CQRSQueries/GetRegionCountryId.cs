﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetRegionCountryId : IQuery<Guid>
    {
        public Guid RegionId { get; set; }
    }
}
