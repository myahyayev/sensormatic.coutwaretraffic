﻿using Convey.CQRS.Commands;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    [Contract]
    public class UnknownDeviceCreatedSuccessfully : ICommand
    {
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }
        public int HttpPort { get; set; }
        public int HttpsPort { get; set; }
    }
}
