﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Queue;
using System.Threading;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.ReportSchedule.Scheduler
{
    public class AutoCreateReportJobSubscriber : BackgroundService
    {
        private readonly ILogger<AutoCreateReportJobSubscriber> _logger;
        private readonly IQueueService _queueService;

        public IScheduler Scheduler { get; set; }
        public AutoCreateReportJobSubscriber(ILogger<AutoCreateReportJobSubscriber> logger, IQueueService queueService)
        {
            _logger = logger;
            _queueService = queueService;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _queueService.Subscribe<AutoCreateReportJobConsumer>(Queues.CountwareTrafficReportingAutoCreateReportJob, QueueConfigTemplate.Default());
        }

        public async override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Auto Create Report job subscriber forcing to stop...");

            _queueService.StopConsumers();

            await Task.Delay(5000);

            await base.StopAsync(cancellationToken);
        }
    }
}
