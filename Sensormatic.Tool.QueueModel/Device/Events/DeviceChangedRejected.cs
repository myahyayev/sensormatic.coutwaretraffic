﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceChangedRejected : MessageEnvelope, IQueueEvent
    {
        public DeviceChangedRejected(Guid deviceId, string name, string oldName,  Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            DeviceId = deviceId;
            Name = name;
            OldName = oldName;
        }
        public Guid DeviceId { get; }
        public string Name { get; }
        public string OldName { get; }
        public Guid RecordId { get; init; }
    }
}
