﻿
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Sensormatic.Tool.Api
{
    public static class UserExtensions
    {
        public static Guid GetUserId(ClaimsIdentity identity)
        {
            IEnumerable<Claim> claims = identity.Claims;
            var userId = claims.Where(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")
                                 .Select(x => x.Value)
                                 .FirstOrDefault();
            Guid userIdG;
            if (Guid.TryParse(userId, out userIdG))
                return userIdG;
            else
                throw new Exception("#E1092 User Id wrong! This isn't Guid");
        }
        public static string GetTenantId(this IHttpContextAccessor context)
        {
            var tenantId = context.HttpContext.User.Claims
                                             .Where(x => x.Type == "http://schemas.microsoft.com/identity/claims/tenantid")
                                             .Select(x => x.Value)
                                             .FirstOrDefault();

            return tenantId;
        }
        public static string GetTenantName(this IHttpContextAccessor context)
        {
            var tenantName = context.HttpContext.User.Claims
                                             .Where(x => x.Type == "tname")
                                             .Select(x => x.Value)
                                             .FirstOrDefault();

            return tenantName;
        }

    }
}
