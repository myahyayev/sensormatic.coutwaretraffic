﻿namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityTokenResponse
    {
        public LoginStatus LoginStatus { get; set; }
        public FirstLoginModel FirstLogin { get; set; }
        public VerifyTokenModel VerifyToken { get; set; }
        public AlreadyLoggedModel AlreadyLogged { get; set; }
        public AuthorityNullModel AuthorityNull { get; set; }
        public OtpDetailModel OtpDetail { get; set; }
    }
}