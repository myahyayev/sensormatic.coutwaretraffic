﻿using CountwareTraffic.Services.Companies.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class AreaEntityTypeConfiguration : IEntityTypeConfiguration<Area>
    {
        public void Configure(EntityTypeBuilder<Area> builder)
        {
            builder.ToTable("Areas", SchemaNames.Areas);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
               .Property(x => x.Description)
               .HasField("_description")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .HasMaxLength(2500);

            builder
              .Property(x => x.DistrictId)
              .HasField("_districtId")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .IsRequired();

            builder
              .Property(x => x.CityId)
              .HasField("_cityId")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .IsRequired();

            builder
            .Property(x => x.ManagerId)
            .HasField("_managerId")
            .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
            .Property(x => x.ManagerName)
            .HasField("_managerName")
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasMaxLength(100);


            var addressNavigationBuilder = builder.OwnsOne(x => x.Address);
            builder
               .Navigation(x => x.Address).Metadata.SetField("_address");
            addressNavigationBuilder.Property(x => x.Street).HasMaxLength(250);
            addressNavigationBuilder.Property(x => x.Location).IsRequired(false);


            var contactNavigationBuilder = builder.OwnsOne(x => x.Contact);
            builder
              .Navigation(x => x.Contact).Metadata.SetField("_contact");
            contactNavigationBuilder.Property(x => x.GsmNumber).HasMaxLength(20);
            contactNavigationBuilder.Property(x => x.GsmCountryCode).HasMaxLength(2);
            contactNavigationBuilder.Property(x => x.GsmDialCode).HasMaxLength(6);
            contactNavigationBuilder.Property(x => x.PhoneNumber).HasMaxLength(20);
            contactNavigationBuilder.Property(x => x.PhoneCountryCode).HasMaxLength(2);
            contactNavigationBuilder.Property(x => x.PhoneDialCode).HasMaxLength(6);
            contactNavigationBuilder.Property(x => x.EmailAddress).HasMaxLength(120);

            builder.HasOne<Region>().WithMany().HasForeignKey(x => x.RegionId);
            builder
                   .Property<int>("_areaTypeId")
                   .UsePropertyAccessMode(PropertyAccessMode.Field)
                   .HasColumnName("AreaTypeId")
                   .IsRequired();

            builder.HasOne(o => o.AreaType)
                   .WithMany()
                   .HasForeignKey("_areaTypeId");


            builder
                .Property<int>("_areaStatus")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("AreaStatus")
                .IsRequired();

            builder.HasOne(o => o.AreaStatus)
                   .WithMany()
                   .HasForeignKey("_areaStatus");
            builder
                .Property(x => x.WorkingHoursStart)
                .HasField("_workingHoursStart")
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
                .Property(x => x.WorkingHoursEnd)
                .HasField("_workingHoursEnd")
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.WorkingTimeZone)
               .HasField("_workingTimeZone")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .HasMaxLength(255);
        }
    }
}
