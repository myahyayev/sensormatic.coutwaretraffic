﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class SubAreaDeletedSuccessfullyHandler : ICommandHandler<SubAreaDeletedSuccessfully>
    {
        private readonly IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> _hubContext;
        public SubAreaDeletedSuccessfullyHandler(IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> hubContext) 
            => _hubContext = hubContext;

        public async Task HandleAsync(SubAreaDeletedSuccessfully command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .SubAreaDeletedSuccessfullyReceived(command);
    }
}
