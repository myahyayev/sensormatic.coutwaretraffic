﻿using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sensormatic.Tool.QueueModel
{
    public record RetryReportGeneration : IQueueCommand
    {
        public Guid ReportId { get; set; }
        public DateTime? DateRangeStartDate { get; set; }
        public DateTime? DateRangeEndDate { get; set; }
        public Guid? ReportFileId { get; set; }
        public Guid RecordId { get ; init ; }

        public RetryReportGeneration()
        {
            RecordId = Guid.NewGuid();
        }
    }
}
