﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Events.Core
{
    public class UnknownDeviceCreateComplated : IDomainEvent
    {
        public string Name { get; init; }
        public int HttpPort { get; init; }
        public int HttpsPort { get; init; }
        public string IpAddress { get; init; }
        public string MacAddress { get; init; }
        public string SerialNumber { get; init; }
        public Guid RecordId { get; init; }

        public UnknownDeviceCreateComplated(string name, int httpPort, int httpsPort, string ipAddress, string macAddress, string serialNumber)
        {
            Name = name;
            HttpPort = httpPort;
            HttpsPort = httpsPort;
            IpAddress = ipAddress;
            MacAddress = macAddress;
            SerialNumber = serialNumber;
            RecordId = Guid.NewGuid();
        }
    }
}
