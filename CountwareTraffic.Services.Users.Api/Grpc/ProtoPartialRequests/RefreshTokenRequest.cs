﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Users.Grpc
{
    [ServiceLog]
    public sealed partial class RefreshTokenRequest : SensormaticRequestValidate
    {
        public override void Validate()
        {
            if (RefreshTokenKey.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(RefreshTokenKey)));

            if (AccessToken.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(AccessToken)));
        }
    }
}