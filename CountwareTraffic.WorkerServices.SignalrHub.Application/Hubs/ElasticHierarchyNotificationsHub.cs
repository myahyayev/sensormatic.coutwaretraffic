﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public interface IElasticHierarchyNotificationsHub
    {
        Task JoinElasticHierarchiesProcess(string groupName);
        Task LeaveElasticHierarchiesProcess(string groupName);
        Task ElasticHierarchiesChangedReceived(ElasticHierarchyChanged command);
    }

    //[Authorize]
    public class ElasticHierarchyNotificationsHub : Hub<IElasticHierarchyNotificationsHub>
    {
        public async Task JoinElasticHierarchiesProcess(string groupName)
            => await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

        public async Task LeaveElasticHierarchiesProcess(string groupName)
          => await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);

        public async Task ElasticHierarchiesChangedReceived(ElasticHierarchyChanged command)
            => await Clients.Group(command.TenantId.ToString()).ElasticHierarchiesChangedReceived(command);
    }
}


