﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityTenantGroupSelectResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }

        public List<IdentityTenantGroupSelectData> Data { get; set; }
    }

    public class IdentityTenantGroupSelectData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
