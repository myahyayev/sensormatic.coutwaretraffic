﻿using CountwareTraffic.Services.Devices.Core;
using CountwareTraffic.Services.Devices.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class BrandLookupRepository : Repository<BrandLookup>, IBrandLookupRepository
    {
        private readonly new DeviceDbContext _context;

        public BrandLookupRepository(DeviceDbContext context) : base(context) => _context = context;
        public async Task<BrandLookup> GetByNameAsync(string name) => await base.GetQuery().SingleOrDefaultAsync(x => x.Name == name);


        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible
    }
}
