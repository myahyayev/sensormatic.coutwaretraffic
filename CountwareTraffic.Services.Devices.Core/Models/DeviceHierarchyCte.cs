﻿using System;

namespace CountwareTraffic.Services.Devices.Core
{
    public class DeviceHierarchyCte
    {
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid SubAreaId { get; set; }
        public string SubAreaName { get; set; }
    }
}
