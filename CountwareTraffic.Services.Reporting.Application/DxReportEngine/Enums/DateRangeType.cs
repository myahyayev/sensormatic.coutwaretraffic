﻿namespace CountwareTraffic.Services.Reporting.Engine
{
    public enum DateRangeType : byte 
    {
        Unknown = 1,
        ByHour = 2,
        ByDate = 3
    }
}
