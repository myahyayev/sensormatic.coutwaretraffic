﻿using CountwareTraffic.Services.Events.Core;
using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class EventCreated : Convey.CQRS.Events.IEvent
    {

        //EventInfo
        public Guid EventId { get; set; }
        public DirectionType DirectionType { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime EventStartDate { get; set; }
        public DateTime EventEndtDate { get; set; }
        public int TotalEnterCount { get; set; }
        public int TotalExitCount { get; set; }
        public int OnTimeIOCount { get; set; }

        //DeviceInfo
        public Guid DeviceId { get; set; }
        public string DeviceName { get; set; }
        public Guid SubAreaId { get; set; }
        public string SubAreaName { get; set; }
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid CityId { get; set; }
        public string CityName { get; set; }
        public Guid DistrictId { get; set; }
        public string DistrictName { get; set; }
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid UserId { get; init; }
        public Guid TenantId { get; set; }
    }
}
