﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class SubAreaChangedFailedHandler : ICommandHandler<SubAreaChangedFailed>
    {
        private readonly IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> _hubContext;
        public SubAreaChangedFailedHandler(IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> hubContext) 
            => _hubContext = hubContext;


        public async Task HandleAsync(SubAreaChangedFailed command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .SubAreaChangedFailedReceived(command);
    }
}
