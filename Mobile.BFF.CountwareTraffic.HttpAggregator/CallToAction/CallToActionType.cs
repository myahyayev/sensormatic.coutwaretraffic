﻿namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public enum CallToActionType
    {
        AbortCall,
        TryAgain,
        OkCall,
        Ok,
        GoToDevices,
        GoToCountries,
        ChangePassword,
        GoToRegions,
        GoToAreas,
        GoToSubAreas,
        GoToBrandModel
    }
}
