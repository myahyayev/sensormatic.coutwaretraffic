﻿using Convey.CQRS.Queries;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class GetBrandLookups : IQuery<List<BrandLookupDetailsDto>>
    {
    }
}
