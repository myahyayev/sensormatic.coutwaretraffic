﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleTypeNameNotFoundException : DomainException
    {
        IEnumerable<ScheduleFrequency> ScheduleTypes { get; }
        public string Name { get; }

        public ScheduleTypeNameNotFoundException(IEnumerable<ScheduleFrequency> scheduleTypes, string name)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ScheduleType Name: {String.Join(",", scheduleTypes.Select(s => s.Name))}") }, 400, ResponseMessageType.Error)
        {
            ScheduleTypes = scheduleTypes;
            Name = name;
        }
    }
}
