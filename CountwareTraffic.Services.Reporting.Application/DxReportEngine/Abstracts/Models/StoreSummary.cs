﻿using System;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public record BaseComparedStoreSummary : StoreSummary
    {
        public bool IsBold { get; set; }
        public int Order { get; set; }
        public BaseComparedStoreSummary() => CurrentPeriod = new DateRange();
    }
    public record StoreSummary
    {
        public Guid StoreId { get; set; }
        public string StoreName { get; set; }
        public DateRange CurrentPeriod { get; set; }
        public StoreSummary() => CurrentPeriod = new DateRange();
    }
}
