﻿using CountwareTraffic.Services.Companies.Grpc;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class LookupService : IScopedSelfDependency
    {
        private readonly Lookup.LookupClient _lookupClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;

        public LookupService(Lookup.LookupClient lookupClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _lookupClient = lookupClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetCountryLookupDetails> GetCountryLookupByIdAsync(Guid countryLookupId)
        {
            GetCountryLookupRequest grpcRequest = new() { Id = countryLookupId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_lookupClient.GetCountryLookupByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetCountryLookupDetails
            {
                Id = new Guid(grpcResponse.CountryLookupDetail.Id),
                Name = grpcResponse.CountryLookupDetail.Name,
                IsoNumeric = grpcResponse.CountryLookupDetail.IsoNumeric == null ? 0 : grpcResponse.CountryLookupDetail.IsoNumeric.Value,
                Capital = grpcResponse.CountryLookupDetail.Capital,
                ContinentCode = grpcResponse.CountryLookupDetail.ContinentCode,
                CurrencyCode = grpcResponse.CountryLookupDetail.CurrencyCode,
                Iso = grpcResponse.CountryLookupDetail.Iso,
                Iso3 = grpcResponse.CountryLookupDetail.Iso3
            };
        }

        public async Task<IEnumerable<GetCountryLookupDetails>> GetCountryLookupsAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_lookupClient.GetCountryLookupsAsync, grpcRequest, hasClientSideLog: false);


            return grpcResponse.CountryLookupDetails.Select(countryLookup => new GetCountryLookupDetails
            {
                Id = new Guid(countryLookup.Id),
                Name = countryLookup.Name,
                IsoNumeric = countryLookup.IsoNumeric == null ? 0 : countryLookup.IsoNumeric.Value,
                Capital = countryLookup.Capital,
                ContinentCode = countryLookup.ContinentCode,
                CurrencyCode = countryLookup.CurrencyCode,
                Iso = countryLookup.Iso,
                Iso3 = countryLookup.Iso3
            });
        }

        public async Task<GetCityLookupDetails> GetCityLookupByIdAsync(Guid citylookupId)
        {
            GetCityLookupRequest grpcRequest = new() { Id = citylookupId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_lookupClient.GetCityLookupByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetCityLookupDetails
            {
                Id = new Guid(grpcResponse.CityLookupDetail.Id),
                Name = grpcResponse.CityLookupDetail.Name,
                CountryId = new Guid(grpcResponse.CityLookupDetail.CountryId),
            };
        }

        public async Task<IEnumerable<GetCityLookupDetails>> GetCityLookupsAsync(Guid countryId)
        {
            GetCityLookupsRequest grpcRequest = new()
            {
                CountryId = countryId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_lookupClient.GetCityLookupsAsync, grpcRequest, hasClientSideLog: false);


            return grpcResponse.CityLookupDetails.Select(cityLookup => new GetCityLookupDetails
            {
                Id = new Guid(cityLookup.Id),
                Name = cityLookup.Name,
                CountryId = new Guid(cityLookup.CountryId)
            });
        }

        public async Task<GetDistrictLookupDetails> GetDistrictLookupByIdAsync(Guid districtLookupId)
        {
            GetDistrictLookupRequest grpcRequest = new() { Id = districtLookupId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_lookupClient.GetDistrictLookupByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetDistrictLookupDetails
            {
                Id = new Guid(grpcResponse.DistrictLookupDetail.Id),
                Name = grpcResponse.DistrictLookupDetail.Name,
                CityId = new Guid(grpcResponse.DistrictLookupDetail.CityId),
            };
        }

        public async Task<IEnumerable<GetDistrictLookupDetails>> GetDistrictLookupsAsync(Guid cityId)
        {
            GetDistrictLookupsRequest grpcRequest = new()
            {
                 CityId = cityId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_lookupClient.GetDistrictLookupsAsync, grpcRequest, hasClientSideLog: false);


            return grpcResponse.DistrictLookupDetail.Select(districtLookup => new GetDistrictLookupDetails
            {
                Id = new Guid(districtLookup.Id),
                Name = districtLookup.Name,
                CityId = new Guid(districtLookup.CityId)
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
