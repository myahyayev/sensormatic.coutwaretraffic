﻿using System;
using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application.Objects.Requests
{
    public class IdentityGetContractByIdRequest
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
    }
}
