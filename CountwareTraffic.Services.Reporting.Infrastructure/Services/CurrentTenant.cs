﻿using CountwareTraffic.Services.Reporting.Application;
using Microsoft.AspNetCore.Http;
using System;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class CurrentTenant : ICurrentTenant
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CurrentTenant(IHttpContextAccessor httpContextAccessor) => _httpContextAccessor = httpContextAccessor;

        public Guid Id
        {
            get
            {
                if (_httpContextAccessor.HttpContext != null)
                {
                    if (_httpContextAccessor.HttpContext.Request.Headers.TryGetValue("tenant-id", out var tenantId))
                        return new Guid(tenantId);

                    else
                        throw new InvalidTenantException("Invalid tenant id");
                }
                return Guid.Empty;
            }
        }

        public string Name
        {
            get
            {
                if (_httpContextAccessor.HttpContext != null)
                {
                    if (_httpContextAccessor.HttpContext.Request.Headers.TryGetValue("tenant-name", out var tenanName))
                        return tenanName;

                    else
                        throw new InvalidTenantException("Invalid tenant name");
                }
                return string.Empty;
            }
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}