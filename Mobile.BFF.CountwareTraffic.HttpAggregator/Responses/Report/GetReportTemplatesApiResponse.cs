﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetReportTemplatesApiResponse
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid Id { get; set; }
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public IEnumerable<TemplateType> TemplateTypes { get; set; }
    }

    public class TemplateType
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
    }

}
