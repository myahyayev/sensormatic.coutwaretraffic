﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]

    //ControllerBase was used instead of BaseController due to log traffic,use it in testing.
    public class BrickstreamController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly ILogger<BrickstreamController> _logger;
        public BrickstreamController(
            ILogger<BrickstreamController> logger,
            ICommandDispatcher commandDispatcher)
        {
            _commandDispatcher = commandDispatcher;
            _logger = logger;
        }


        [HttpPost("realtime/events/xml")]
        public async Task<OkObjectResult> CreateEventAsync()
        {
            // _logger.LogInformation($" Brickstream realtime start to read xml body.....");
            using (StreamReader sReader = new StreamReader(Request.Body))
            {
                string xmlBody = await sReader.ReadToEndAsync();

                _logger.LogInformation($"{xmlBody}");

                await _commandDispatcher.SendAsync(new BrickstreamRealTimeEventLogXmlFormat()
                {
                    Xml = xmlBody
                });
            }
            return Ok(new { Success = "Success" });
        }

        [HttpPost("timerange/events/xml")]
        public async Task<OkObjectResult> CreateTimeRangeEventAsync()
        {
            //_logger.LogInformation($" Brickstream TimeRange start to read xml body.....");
            using (StreamReader sReader = new StreamReader(Request.Body))
            {
                string xmlBody = await sReader.ReadToEndAsync();

                _logger.LogInformation($"{xmlBody}");

                await _commandDispatcher.SendAsync(new BrickstreamTimeRangeEventLogXmlFormat()
                {
                    Xml = xmlBody
                });
            }
            return Ok(new { Success = "Success" });
        }
    }

}
