﻿using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Engine.TrafficReport
{
    public class WeeklyTrafficReportData : BaseReportData
    {
       public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> ComparedStoresSummary { get; set; }
       public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> ComparedStoresSummaryByDay { get; set; }
       public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> TrafficByDayChart { get; set; }
       public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> TrafficByStoreChart { get; set; }
       public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> SummaryByStore { get; set; }
       public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> SummaryByStoreByDay { get; set; }
    }
}
