﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class CompanyContactCountryCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Contact_GsmCountryCode",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(2)",
                maxLength: 2,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_GsmCountryCodeSecondary",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(2)",
                maxLength: 2,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_PhoneCountryCode",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(2)",
                maxLength: 2,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_PhoneCountryCodeSecondary",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(2)",
                maxLength: 2,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Contact_GsmCountryCode",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_GsmCountryCodeSecondary",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_PhoneCountryCode",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_PhoneCountryCodeSecondary",
                schema: "areas",
                table: "Companies");
        }
    }
}
