﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class UpdateSubAreaHandler : ICommandHandler<UpdateSubArea>
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public UpdateSubAreaHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(UpdateSubArea command)
        {
            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            var subArea = await subAreaRepository.GetAsync(command.SubAreaId, _currentTenant.Id);

            if (subArea is null)
                throw new SubAreaNotFoundException(command.SubAreaId);

            if (subArea.Name == command.Name)
            {
                if (subArea.Description == command.Description)
                    return;

                subArea.WhenChanged(command.Name, command.Description);

                await _unitOfWork.CommitAsync();
                return;
            }
            
                
            if(await subAreaRepository.ExistsAsync(command.Name, _currentTenant.Id))
                throw new SubAreaAlreadyExistsException(command.Name);

          
            subArea.WhenChanged(command.Name, command.Description);

            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var hierarchy = await areaRepository.GetHierarchyAsync(subArea.AreaId, _currentTenant.Id);

            subArea.WhenHierarchyChanged(_currentTenant.Id,  hierarchy.CopmanyId,  hierarchy.CountryId, hierarchy.RegionId, hierarchy.AreaId, subArea.Id, subArea.Name);


            await _unitOfWork.CommitAsync();
        }
    }
}
