﻿using System;

namespace Countware.Traffic.CrossCC.PolicyProvider
{
    public interface IIdentifiable
    {
        Guid Identifier { get; }
    }
}
