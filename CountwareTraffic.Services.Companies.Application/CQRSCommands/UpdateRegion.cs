﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    [Contract]
    public class UpdateRegion : ICommand
    {
        public Guid RegionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ManagerName { get; set; }
        public Guid? ManagerId { get; set; }
        public Guid CountryId { get; set; }
    }
}
