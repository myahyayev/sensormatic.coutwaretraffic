﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetToken : IQuery<IdentityTokenResponse>
    { 
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid? TenantId { get; set; }
        public string Language { get; set; }
    }
}
