﻿using Sensormatic.Tool.Core;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class AddCountryApiRequest : SensormaticRequestValidate
    {
        public string Iso { get; set; }
        public string Iso3 { get; set; }
        public int IsoNumeric { get; set; }
        public string Name { get; set; }
        public string Capital { get; set; }
        public string ContinentCode { get; set; }
        public string CurrencyCode { get; set; }

        public override void Validate()
        {
            if (string.IsNullOrEmpty(Name))
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(Name)));
        }
    }

}
