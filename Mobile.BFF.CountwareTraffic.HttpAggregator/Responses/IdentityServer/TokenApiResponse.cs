﻿using System;
using System.Text.Json.Serialization;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class TokenApiResponse
    {
        [JsonIgnore]
        public LoginStatus LoginStatus { get; set; }
        public AlreadyLoggedModel AlreadyLogged { get; set; }
        public AuthorityNullModel AuthorityNull { get; set; }
        public FirstLoginModel FirstLogin { get; set; }
        public VerifyTokenModel VerifyToken { get; set; }
        public OtpDetailModel OtpDetail { get; set; }
    }

    public class AlreadyLoggedModel
    {
        [JsonPropertyName("access_token")]
        public string AccessToken { get; set; }


        [JsonPropertyName("unique_name")]
        public string UniqueName { get; set; }


        [JsonPropertyName("email")]
        public string Email { get; set; }


        [JsonPropertyName("given_name")]
        public string GivenName { get; set; }


        [JsonPropertyName("family_name")]
        public string FamilyName { get; set; }


        [JsonPropertyName("phone_number")]
        public string PhoneNumber { get; set; }


        [JsonPropertyName("expire_Time")]
        public DateTime ExpireTime { get; set; }


        [JsonPropertyName("gender")]
        public int Gender { get; set; }


        [JsonPropertyName("refresh_token")]
        public string RefreshToken { get; set; }
    }

    public class AuthorityNullModel
    {
        [JsonPropertyName("auth_token")]
        public string AuthToken { get; set; }
    }

    public class FirstLoginModel
    {
        public string Message { get; set; }
    }

    public class VerifyTokenModel
    {
        [JsonPropertyName("verify_token")]
        public string VerifyToken { get; set; }


        [JsonPropertyName("authority")]
        public string Authority { get; set; }


        [JsonPropertyName("parameters")]
        public string[] Parameters { get; set; }


        [JsonPropertyName("timeOut")]
        public int TimeOut { get; set; }
    }
    public class OtpDetailModel
    {
        [JsonPropertyName("reference_text")]
        public string ReferenceText { get; set; }
    }

    public enum LoginStatus : byte
    {
        Unknown = 0,
        FirstLogin = 1,
        AlreadyLogged = 2,
        AuthorityNull = 3,
        VerifyToken = 4,
    }
}
