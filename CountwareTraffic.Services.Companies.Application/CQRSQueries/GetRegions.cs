﻿using Convey.CQRS.Queries;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetRegions : IQuery<PagingResult<RegionDetailsDto>>
    {
        public Guid CountryId { get; set; }
        public PagingQuery PagingQuery { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
    }
}
