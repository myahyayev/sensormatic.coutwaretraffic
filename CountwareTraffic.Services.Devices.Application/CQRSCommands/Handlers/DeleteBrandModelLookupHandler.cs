﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeleteBrandModelLookupHandler : ICommandHandler<DeleteBrandModelLookup>
    {
        private readonly IUnitOfWork _unitOfWork;
        public DeleteBrandModelLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task HandleAsync(DeleteBrandModelLookup command)
        {
            var brandModelRepository = _unitOfWork.GetRepository<IBrandModelLookupRepository>();

            var brand = await brandModelRepository.GetByIdAsync(command.BrandModelId);

            if (brand is null)
                throw new BrandModelLookupNotFoundException(command.BrandModelId);

            brandModelRepository.Remove(brand);

            await _unitOfWork.CommitAsync();
        }
    }
}
