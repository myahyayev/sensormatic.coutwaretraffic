﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportTemplateRepository : Repository<ReportTemplate>, IReportTemplateRepository
    {
        private readonly new ReportDbContext _context;

        public ReportTemplateRepository(ReportDbContext context) : base(context) => _context = context;

        public async Task<IEnumerable<ReportTemplateEntityDto>> GetAllTemplatesAsync()
        {
            var query = base.GetQuery(null);

            return await query.Include(i => i.Types)
                .Select(s => new ReportTemplateEntityDto
                {
                    Id = s.Id,
                    AssemblyQualifiedName = s.AssemblyQualifiedName,
                    AuditCreateBy = s.AuditCreateBy,
                    AuditCreateDate = s.AuditCreateDate,
                    AuditModifiedBy = s.AuditModifiedBy,
                    AuditModifiedDate = s.AuditModifiedDate,
                    Description = s.Description,
                    Name = s.Name,
                    TemplateTypeEntityDtos = (from reportTemplateTypes in s.Types
                                              join templateType in _context.Set<TemplateType>() on reportTemplateTypes.TypeId equals templateType.Id
                                              select new TemplateTypeEntityDto { Name = templateType.Name, TypeId = templateType.Id }).ToList()

                }).ToListAsync();
        }

        #region dispose
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion dispose


    }
}
