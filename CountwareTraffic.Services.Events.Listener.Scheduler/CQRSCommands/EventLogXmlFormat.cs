﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Application;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    [Contract]
    public class BrickstreamTimeRangeEventLogXmlFormat : ICommand 
    {
        public string Xml { get; set; }
    }

    [Contract]
    public class BrickstreamRealTimeEventLogXmlFormat : ICommand
    {
        public string Xml { get; set; }
    }

    [Contract]
    public class HikvisionTimeRangeEventLogXmlFormat : ICommand
    {
        public string Xml { get; set; }
    }
    [Contract]
    public class HikvisionRealTimeEventLogXmlFormat : ICommand
    {
        public string Xml { get; set; }
    }

    [Contract]
    public class DahuaEventLogXmlFormat : ICommand { public string Xml { get; set; } }
}
