﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer
{
    public class GetAdminUserByIdApiRequest
    {
        public Guid Id { get; set; }
    }
}
