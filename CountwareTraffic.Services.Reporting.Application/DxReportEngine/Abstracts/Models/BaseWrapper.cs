﻿using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Engine
{
    /// <summary>
    /// Data wrapper model for report items(detail,chart,etc)
    /// </summary>
    /// <typeparam name="ContentEnumerable"></typeparam>
    /// <typeparam name="ContentEnumerableItemType"></typeparam>
    public class BaseWrapper<ContentEnumerable, ContentEnumerableItemType> where ContentEnumerable : IEnumerable<ContentEnumerableItemType>
    {
        /// <summary>
        /// Record of data
        /// </summary>
        public ContentEnumerable Content { get; set; }
        /// <summary>
        /// Title of report item
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        ///Subtitle of report item
        /// </summary>
        public string SubTitle { get; set; }
        /// <summary>
        /// FooterTitle of report item
        /// </summary>
        public string FooterTitle { get; set; }
    }
}
