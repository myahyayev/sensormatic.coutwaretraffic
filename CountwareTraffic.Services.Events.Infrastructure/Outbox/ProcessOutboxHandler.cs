﻿using Convey.CQRS.Commands;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Retry;
using Sensormatic.Tool.Common;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class ProcessOutboxHandler : ICommandHandler<ProcessOutbox>
    {
        private readonly EventDbContext _context;
        private readonly ILogger<ProcessOutboxHandler> _logger;
        
        public ProcessOutboxHandler(EventDbContext context, ILogger<ProcessOutboxHandler> logger)
        {
            _context = context;
            _logger = logger;
        }
        public async Task HandleAsync(ProcessOutbox command)
        {

            var policy = CreatePolicy(nameof(ProcessOutboxHandler), command.RecordId.ToString());
            await policy.ExecuteAsync(async () =>
            {

                using (var connection = _context.Database.GetDbConnection())
                {

                    const string UpdateProcessedDateQuery = @"UPDATE [event.app].[OutboxMessages] SET [ProcessedDate] = @ProcessedDate, [Status] = @Status, [LastException] = @LastException WHERE [EventRecordId] = @EventRecordId";

                    var parametes = new DynamicParameters();
                    parametes.Add("@ProcessedDate", command.Status == OperationStatus.Success ? DateTime.UtcNow : null);
                    parametes.Add("@Status", command.Status);
                    parametes.Add("@LastException", command.Exception);
                    parametes.Add("@EventRecordId", command.RecordId);

                    if ((await connection.ExecuteAsync(UpdateProcessedDateQuery, parametes)) > 0)
                        _logger.LogTrace($"ProcessOutbox Handled for {command.RecordId} record after Queue.pub");
                }

            });

        }
        private AsyncRetryPolicy CreatePolicy(string prefix, string recordId, int retries = 5, int sleepDurationProviderInSeconds = 300)
        {
            return Policy.Handle<Exception>()
                                            .WaitAndRetryAsync(
                                                retryCount: retries,
                                                sleepDurationProvider: retry => TimeSpan.FromSeconds(sleepDurationProviderInSeconds),
                                                onRetry: (exception, timeSpan, retry, ctx) =>
                                                {
                                                    _logger.LogError(exception, "[{prefix}] Exception {ExceptionType} with message {Message} detected on attempt {retry} of {retries} for recordId:{recordId}", prefix, exception.GetType().Name, exception.Message, retry, retries, recordId);
                                                }
                                            );
        }

    }
}
