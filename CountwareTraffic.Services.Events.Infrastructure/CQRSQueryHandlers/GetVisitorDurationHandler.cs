﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetVisitorDurationHandler : IQueryHandler<GetVisitorDuration, VisitorDurationDto>
    {
        private readonly IEventElasticSearchRepository _eventElasticSearchRepository;
        private readonly ILogger<GetVisitorDurationHandler> _logger;
        public GetVisitorDurationHandler(IEventElasticSearchRepository eventElasticSearchRepository, ILogger<GetVisitorDurationHandler> logger)
        {
            _eventElasticSearchRepository = eventElasticSearchRepository;
            _logger = logger;
        }

        public async Task<VisitorDurationDto> HandleAsync(GetVisitorDuration query)
            => await _eventElasticSearchRepository.GetVisitorDurationAsync(query.NodeFilters, query.CurrentStartDate);
    }
}
