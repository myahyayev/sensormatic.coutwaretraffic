﻿using System;
using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityTokenRequest
    {
        [JsonPropertyName("payload")]
        public Payload Payload { get; set; }

        [JsonPropertyName("token")]
        public string Token { get; set; }
    }

    public class Payload
    {
        [JsonPropertyName("username")]
        public string UserName { get; set; }

        [JsonPropertyName("password")]
        public string Password { get; set; }

        [JsonPropertyName("tenantId")]
        public Guid? TenantId { get; set; }

        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("otp")]
        public string Otp { get; set; }
    }
}
