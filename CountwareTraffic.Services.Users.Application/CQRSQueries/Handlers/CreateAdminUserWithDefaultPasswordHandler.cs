﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class CreateAdminUserWithDefaultPasswordHandler : IQueryHandler<CreateAdminUserWithDefaultPassword, IdentityCreateAdminUserWithDefaultPasswordResponse>
    {
        private readonly IIdentityServerService _identityServerService;
        public CreateAdminUserWithDefaultPasswordHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public Task<IdentityCreateAdminUserWithDefaultPasswordResponse> HandleAsync(CreateAdminUserWithDefaultPassword query)
        {
            return _identityServerService.CreateAdminUserWithDefaultPasswordAsync(new Objects.Requests.IdentityCreateAdminUserWithDefaultPasswordRequest
            {
                Address = query.Address,
                AllowEmail = query.AllowEmail,
                AllowSms = query.AllowSms,
                Birthdate = query.Birthdate,
                Domain = query.Domain,
                Email = query.Email,
                FirstName = query.FirstName,
                Gender = query.Gender,
                LastName = query.LastName,
                PhoneNumber = query.PhoneNumber,
                ProfilePic = query.ProfilePic,
                Roles = query.Roles,
                Status = query.Status,
                TenantIds = query.TenantIds,
                TwoFactorEnabled = query.TwoFactorEnabled,
                UserName = query.UserName,
                UserType = query.UserType,
            }, query.Language);
        }
    }
}
