﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantGroupSelectHandler : IQueryHandler<GetTenantGroupSelect, IdentityTenantGroupResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetTenantGroupSelectHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityTenantGroupResponse> HandleAsync(GetTenantGroupSelect query)
        {
            return await _identityServerService.GetTenantGroupSelectAsync();
        }
    }
}
