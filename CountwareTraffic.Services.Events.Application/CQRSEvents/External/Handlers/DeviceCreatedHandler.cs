﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Events.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class DeviceCreatedHandler : IEventHandler<DeviceCreated>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public DeviceCreatedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(DeviceCreated @event)
        {
            var deviceRepository = _unitOfWork.GetRepository<IDeviceRepository>();

            if (!await deviceRepository.ExistsAsync(@event.DeviceId))
            {
                try
                {
                    var device = Device.Create(@event.DeviceId, @event.Name, @event.MacAddress, @event.IpAddress, @event.SubAreaId, @event.SubAreaName, @event.AreaId, @event.AreaName, @event.RegionId, @event.RegionName, @event.CountryId, @event.CountryName, @event.CompanyId, @event.CompanyName, @event.CountryLookupId, @event.CityId, @event.CityName, @event.DistrictId, @event.DistrictName, @event.TenantId, @event.ControlFrequency);

                    await deviceRepository.AddAsync(device);

                    await _unitOfWork.CommitAsync();

                    _queueService.Publish(new DeviceCreatedCompleted(@event.DeviceId, @event.Name, @event.UserId, "", @event.TenantId));
                }
                catch (Exception ex)
                {
                    _queueService.Publish(new DeviceCreatedRejected(@event.DeviceId, @event.Name, @event.UserId, "", @event.TenantId));
                }
            }
        }
    }
}
