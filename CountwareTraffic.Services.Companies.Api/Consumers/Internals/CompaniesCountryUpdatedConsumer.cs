﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesCountryUpdatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.CountryUpdated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesCountryUpdatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.CountryUpdated queuEvent)
        {
            var countryUpdated = _eventMapper.Map(queuEvent) as CountryUpdated;
            await _eventDispatcher.PublishAsync(countryUpdated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
