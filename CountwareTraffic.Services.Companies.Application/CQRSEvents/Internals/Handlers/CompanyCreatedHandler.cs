﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CompanyCreatedHandler : IEventHandler<CompanyCreated>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public CompanyCreatedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(CompanyCreated command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId,
                Name = command.TenantName
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId,
                Name = command.CompanyName
            };

            await _tenantHierarchyElasticSearchRepository.AddCompanyAsync(tenantElasticDto, companyElasticDto);
        }
    }
}

