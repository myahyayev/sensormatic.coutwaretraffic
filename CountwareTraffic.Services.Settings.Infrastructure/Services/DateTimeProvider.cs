﻿using CountwareTraffic.Services.Settings.Application;
using System;

namespace CountwareTraffic.Services.Settings.Infrastructure
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime Now => DateTime.UtcNow;
    }
}
