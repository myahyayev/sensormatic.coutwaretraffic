﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class RetryReportGeneratingApiRequest
    {
        public DateTime? DateRangeStartDate { get; set; }
        public DateTime? DateRangeEndDate { get; set; }
        public Guid? ReportFileId { get; set; }
    }
}
