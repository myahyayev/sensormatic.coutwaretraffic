﻿using CountwareTraffic.Services.Reporting.Application;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using System;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public partial class MonthlyCompherensiveTrafficReport : ScheduleReport, ICompherensiveTrafficReport
    {

        public MonthlyCompherensiveTrafficReport(IServiceProvider serviceProvider, IOptions<TimeZoneSettings> timeZoneSettings) : base(serviceProvider, timeZoneSettings)
        {
            InitializeComponent();
            RefreshData();
        }
        public override void RefreshData()
        {
            if (DataSource != null)
            {
                objectDataSource2.DataSource = ReportData as CompherensiveTrafficReport.Models.MonthlyCompherensiveTrafficReportData; ;
                FillDataSources(false);
            }
        }

        public override void SetResources()
        {
            var datasource = ReportData as CompherensiveTrafficReport.Models.MonthlyCompherensiveTrafficReportData; ;
            //todo:load from mongo
            //ComparedStoresSummaryTitle.Text = datasource.ComparedStoresSummary.Title;
            //ComparedStoresSummarySubTitle.Text = datasource.ComparedStoresSummary.SubTitle;
            //ComparedStoresSummaryFooterTitle.Text = datasource.ComparedStoresSummary.FooterTitle;

            //SummaryByStoreTitle.Text = datasource.SummaryByStore.Title;
            //SummaryByStoreSubTitle.Text = datasource.SummaryByStore.SubTitle;
            //SummaryByStoreFooterTitle.Text = datasource.SummaryByStore.FooterTitle;
        }
    
    }
}
