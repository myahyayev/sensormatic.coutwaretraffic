﻿using Sensormatic.Tool.Ioc;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Core
{
    public interface IBrandLookupRepository : IRepository<BrandLookup>, IScopedDependency
    {
        Task<BrandLookup> GetByNameAsync(string name);
    }
}
