using Convey;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using CountwareTraffic.Services.Reporting.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Sensormatic.Tool.ApplicationInsights;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.Scheduler;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.ReportSchedule.Scheduler
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            Sensormatic.Tool.Ioc.IoCGenerator.DoTNet.Current.Start(services, Configuration);
            
            services.AddApplicationInsights(Configuration);

            services.AddDbContext<ReportDbContext>(options =>
                                                options.UseSqlServer(Configuration.GetConnectionString("ReportDbConnection"), x => x.UseNetTopologySuite().EnableRetryOnFailure()));

            services.AddConvey()
                    .AddApplication()
                    .Build();

            var assembly = typeof(Sensormatic.Tool.Scheduler.Controllers.HomeController).Assembly;

            services.AddControllersWithViews()
               .AddApplicationPart(assembly)
               .AddRazorRuntimeCompilation();


            services.Configure<MvcRazorRuntimeCompilationOptions>(options => { options.FileProviders.Add(new EmbeddedFileProvider(assembly)); });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddHostedService<AutoCreateReportJobSubscriber>();
            services.AddHostedService<AutoDeleteReportJobSubscriber>();
            services.AddHostedService<AutoScaler>();
            services.AddSignalR();
            services.RegisterQuartzServices(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection()
               .UseStaticFiles(new StaticFileOptions { FileProvider = new PhysicalFileProvider(AppDomain.CurrentDomain.BaseDirectory + "/wwwroot") })
               .UseRouting()
               .UseAuthorization()
               .UseEndpoints(endpoints => { endpoints.MapHub<JobHub>("/signalr"); })
               .UseEndpoints(endpoints =>
               {
                   endpoints.MapControllerRoute(
                       name: "default",
                       pattern: "{controller=Home}/{action=Index}/{id?}");
               });

            var ciTr = new CultureInfo("tr-TR");
            ciTr.NumberFormat.NumberDecimalSeparator = ".";
            ciTr.NumberFormat.CurrencyDecimalSeparator = ".";

            var ciEn = new CultureInfo("en-US");

            // Configure the Localization middleware
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(ciTr),
                SupportedCultures = new List<CultureInfo> { ciTr, ciEn },
                SupportedUICultures = new List<CultureInfo> { ciTr, ciEn }
            });
        }
    }
    public static class QuartzRegistration
    {
        public static void RegisterQuartzServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IJobFactory, JobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<ReportGeneratorJob>();
            services.AddHostedService<QuartzHostedService>();
            services.Configure<QuartzOptions>(configuration.GetSection("Quartz"));

            var serviceProvider = services.BuildServiceProvider();
            var _queryDispatcher = serviceProvider.GetRequiredService<IQueryDispatcher>();

            var scheduledReports = _queryDispatcher.QueryAsync(new GetScheduledReports { }).Result;

            if (scheduledReports != null)

#if DEBUG
                scheduledReports.Where(w => w.Id == new Guid("7A981A81-BB8B-44AF-48D9-08DAB33B52A1")).ToList() //weekly repeat sample
                
#endif

#if !DEBUG
                scheduledReports.ToList()
#endif

                             .ForEach(report => services.AddSingleton(new JobSchedule(
                        jobType: typeof(ReportGeneratorJob),
                        jobName: report.Id.ToString(),
                        description:report.Name,
                        tenantId: report.TenantId,
                        startsOn: report.StartsOn,
                        endsOn: report.EndsOn,
                        atSetTime: report.AtSetTime,
                        atSetTimeZone: report.AtSetTimeZone,
                        repeatOn: report.RepeatOn,
                        templateType: TemplateType.FromValue<TemplateType>(report.TemplateTypeId)
                        )
                    ));

            services.AddQuartz(quartzConfigurator =>
            {
                quartzConfigurator.UseMicrosoftDependencyInjectionJobFactory();
                quartzConfigurator.AddJobListener<JobListener>();
                quartzConfigurator.AddSchedulerListener<SchedulerListener>();
                quartzConfigurator.AddTriggerListener<TriggerListener>();
            });

            services.AddQuartzServer(options =>
            {
                options.WaitForJobsToComplete = true;
            });
        }
    }
}
