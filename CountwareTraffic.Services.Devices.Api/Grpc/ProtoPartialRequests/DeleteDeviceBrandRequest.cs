﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Devices.Grpc
{
    [ServiceLog]
    public sealed partial class DeleteDeviceBrandRequest : SensormaticRequestValidate
    {
        internal Guid _BrandId
        {
            get
            {
                if (Guid.TryParse( brandId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { brandId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_BrandId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1499 BrandId cannot be null", nameof(_BrandId)));
        }
    }
}
