﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetRoleUsers : IQuery<IdentityRoleUsersResponse> 
    {
        public Guid RoleId { get; set; }
    }
}
