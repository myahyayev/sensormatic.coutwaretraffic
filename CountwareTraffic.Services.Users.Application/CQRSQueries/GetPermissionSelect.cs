﻿using Convey.CQRS.Queries;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetPermissionSelect : IQuery<IdentityPermissionSelectResponse>
    {
    }
}
