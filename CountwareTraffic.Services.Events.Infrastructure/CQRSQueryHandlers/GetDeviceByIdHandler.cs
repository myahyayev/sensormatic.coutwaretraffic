﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetDeviceByIdHandler : IQueryHandler<GetDeviceById, DeviceDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<GetDeviceByIdHandler> _logger;
        public GetDeviceByIdHandler(IUnitOfWork unitOfWork, ILogger<GetDeviceByIdHandler> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<DeviceDto> HandleAsync(GetDeviceById query)
        {
            var device = await _unitOfWork
                                        .GetRepository<IDeviceRepository>()
                                        .GetByIdAsync(query.DeviceId);

            if (device == null)
                return default;

            return new DeviceDto
            {
                Id = device.Id,
                Name = device.Name,
                TenantId = device.TenantId,
                ControlFrequency = device.ControlFrequency
            };
        }
    }
}
