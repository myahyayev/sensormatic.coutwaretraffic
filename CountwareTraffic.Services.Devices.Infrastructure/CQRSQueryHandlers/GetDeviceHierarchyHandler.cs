﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class GetDeviceHierarchyHandler : IQueryHandler<GetDeviceHierarchy, List<HierarchyDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GetDeviceHierarchyHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<List<HierarchyDto>> HandleAsync(GetDeviceHierarchy query)
        {
            var deviceRepository = _unitOfWork.GetRepository<IDeviceRepository>();

            var hierarchies = await deviceRepository.GetHierarchyAsync(query.Id, _currentTenant.Id, _currentTenant.Name);

            return hierarchies.Select(u => new HierarchyDto
            {
                HierarchyLevel = u.HierarchyLevel,
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }
}
