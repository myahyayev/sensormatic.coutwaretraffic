﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class GetBrandModelLookupHandler : IQueryHandler<GetBrandModelLookup, BrandModelLookupDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetBrandModelLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<BrandModelLookupDetailsDto> HandleAsync(GetBrandModelLookup query)
        {
            var brandModelLookup = await _unitOfWork.GetRepository<IBrandModelLookupRepository>().GetByIdAsync(query.BrandModelId);

            if (brandModelLookup == null)
                throw new BrandModelLookupNotFoundException(query.BrandModelId);

            return new BrandModelLookupDetailsDto
            {
                Id = brandModelLookup.Id,
                Name = brandModelLookup.Name,
                BrandId = brandModelLookup.BrandId,
            };
        }
    }
}
