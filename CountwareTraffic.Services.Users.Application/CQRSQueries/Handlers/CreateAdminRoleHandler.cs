﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class CreateAdminRoleHandler : IQueryHandler<CreateAdminRole, IdentityCreateAdminRoleResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public CreateAdminRoleHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityCreateAdminRoleResponse> HandleAsync(CreateAdminRole query)
        {
            return await _identityServerService.CreateAdminRole(new Objects.Requests.IdentityCreateAdminRoleRequest
            {
                Name = query.Name,
                Description = query.Description,
                Permissions = query.Permissions
            });

        }
    }
}
