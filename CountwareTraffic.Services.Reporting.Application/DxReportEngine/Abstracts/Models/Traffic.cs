﻿namespace CountwareTraffic.Services.Reporting.Engine
{
    public class Traffic
    {
        public Traffic()
        {
            InBound = new();
            OutBound = new();
        }
        public ValueObject<double> InBound { get; set; }
        public ValueObject<double> OutBound { get; set; }
    }
}