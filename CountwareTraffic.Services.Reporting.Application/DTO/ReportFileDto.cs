﻿using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportFileDto
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string FilePath { get; set; }
        public DateTime DateRangeStartDate { get; set; }
        public DateTime DateRangeEndDate { get; set; }
    }
}
