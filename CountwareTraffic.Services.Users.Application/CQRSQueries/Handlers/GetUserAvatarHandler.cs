﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserAvatarHandler : IQueryHandler<GetUserAvatar, bool>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetUserAvatarHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<bool> HandleAsync(GetUserAvatar query)
        {
            return await _identityServerService.GetUserAvatarAsync(query.Id);
        }
    }
}
