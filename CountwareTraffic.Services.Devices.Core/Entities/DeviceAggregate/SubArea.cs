﻿using Sensormatic.Tool.Efcore;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Core
{
    public class SubArea : IEntity, IDeletable
    {
        private string _name;
        private Guid _id;
        private Guid _areaId;
        private string _areaName;
        private Guid _regionId;
        private string _regionName;
        private Guid _countryId;
        private string _countryName;
        private Guid _companyId;
        private string _companyName;
        private Guid _countryLookupId;
        private Guid _cityId;
        private string _cityName;
        private Guid _districtId;
        private string _districtName;
        public Guid Id => _id;
        public string Name => _name;
        public Guid AreaId => _areaId;
        public string AreaName => _areaName;
        public Guid RegionId => _regionId;
        public string RegionName => _regionName;
        public Guid CountryId => _countryId;
        public string CountryName => _countryName;
        public Guid CompanyId => _companyId;
        public string CompanyName => _companyName;
        public Guid CountryLookupId => _countryLookupId;
        public Guid CityId => _cityId;
        public string CityName => _cityName;
        public Guid DistrictId => _districtId;
        public string DistrictName => _districtName;
        public Guid TenantId { get; set; }
        public bool AuditIsDeleted { get; set; }

        private SubArea() { }
        public static SubArea Create(Guid id, string name, Guid areaId, string areaName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid countryLookupId, Guid cityId, string cityName, Guid districtId, string districtName, Guid tenantId)
        {

            return new SubArea
            {
                _id = id,
                _name = name,
                _areaId = areaId,
                _areaName = areaName,
                _regionId = regionId,
                _regionName = regionName,
                _countryId = countryId,
                _countryName = countryName,
                _companyId  = companyId,
                _companyName = companyName,
                _countryLookupId = countryLookupId,
                _cityId = cityId,
                _cityName = cityName,
                _districtId = districtId,
                _districtName = districtName,
                TenantId = tenantId
            };
        }

        public void Change(string name)
            => _name = name;
    }
}
