﻿using Convey.CQRS.Queries;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class GetScheduledReports : IQuery<IEnumerable<ReportDetailDto>>
    {
    }
}
