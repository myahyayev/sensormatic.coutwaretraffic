﻿using CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Requests.Base;
using CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Responses;
using CountwareTraffic.Services.Devices.Grpc;
using Grpc.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Services
{
    public class DeviceService : IScopedSelfDependency
    {
        private static string TenantId = "tenant-id";
        private static string TenantName = "tenant-name";

        private readonly Device.DeviceClient _deviceClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;
        public DeviceService(Device.DeviceClient deviceClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _deviceClient = deviceClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<ApiPagedResponse<GetDeviceDetails>> GetDevicesAsync(Guid tenantId, DataSourceApiRequest paging)
        {
            Metadata metadata = new() { { TenantId, tenantId.ToString() } };

            GetDevicesRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new CountwareTraffic.Services.Devices.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => CountwareTraffic.Services.Devices.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => CountwareTraffic.Services.Devices.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                CountwareTraffic.Services.Devices.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => CountwareTraffic.Services.Devices.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => CountwareTraffic.Services.Devices.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => CountwareTraffic.Services.Devices.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => CountwareTraffic.Services.Devices.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => CountwareTraffic.Services.Devices.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDevicesAsync, grpcRequest, metadata, hasClientSideLog: false);

            var devices = grpcResponse.DeviceDetails.Select(device => new GetDeviceDetails
            {
                Id = new Guid(device.Id),
                Name = device.Name,
                Description = device.Description,
                AuditCreateBy = new Guid(device.Audit.AuditCreateBy),
                AuditCreateDate = device.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(device.Audit.AuditModifiedBy),
                AuditModifiedDate = device.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                DeviceStatusId = device.DeviceStatusId,
                DeviceStatusName = device.DeviceStatusName,
                DeviceTypeId = device.DeviceTypeId,
                DeviceTypeName = device.DeviceTypeName,
                Identity = device.Identity,
                IpAddress = device.IpAddress,
                MacAddress = device.MacAddress,
                ModelId = new Guid(device.ModelId),
                BrandId = new Guid(device.BrandId),
                BrandName = device.BrandName,
                Note = device.Note,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.Firmware,
                IsActive = device.IsActive,
                ModelName = device.ModelName,
                Password = device.Password,
                Port = device.Port,
                UniqueId = device.UniqueId,
                SubAreaId = new Guid(device.SubAreaId)
            });

            return new ApiPagedResponse<GetDeviceDetails>(devices, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}