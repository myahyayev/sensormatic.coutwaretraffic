﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class BrandModelAlreadyExistsException : AppException
    {
        public string BrandModelName { get; }

        public BrandModelAlreadyExistsException(string name)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1552 Brand Model with name: {name} already exists.") }, 409, ResponseMessageType.Error)
        {
            BrandModelName = name;
        }
    }
}
