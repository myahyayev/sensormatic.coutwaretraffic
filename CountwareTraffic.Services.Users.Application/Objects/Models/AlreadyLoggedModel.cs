﻿using System;
using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application
{
    public class AlreadyLoggedModel
    {
        [JsonPropertyName("auth_token")]
        public string AccessToken { get; set; }


        [JsonPropertyName("unique_name")]
        public string UniqueName { get; set; }


        [JsonPropertyName("email")]
        public string Email { get; set; }


        [JsonPropertyName("given_name")]
        public string GivenName { get; set; }


        [JsonPropertyName("family_name")]
        public string FamilyName { get; set; }


        [JsonPropertyName("phone_number")]
        public string PhoneNumber { get; set; }


        [JsonPropertyName("expire_time")]
        public DateTime ExpireTime { get; set; }


        [JsonPropertyName("gender")]
        public Gender Gender { get; set; }


        [JsonPropertyName("refresh_token")]
        public string RefreshToken { get; set; }
        
    }
}
