﻿using Newtonsoft.Json;
using Sensormatic.Tool.Efcore;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class Report : AggregateRoot, IAuditable, IDateable, IDeletable, ITraceable, IMultiTenant
    {
        private string _name;
        private string _description;
        private bool _notifyWhenComplete;
        private Guid _templateId;
        private int _templateTypeId;
        private int _formatId;
        private ReportEmail _email;
        private Guid _languageId;
        private string _parameters;
        private int _status;
        private bool _isActive;
        private string _userName;

        public string Name => _name;
        public string Description => _description;
        public bool NotifyWhenComplete => _notifyWhenComplete;
        public Guid TemplateId => _templateId;
        public ReportFormat ReportFormat { get; private set; }
        public TemplateType TemplateType { get; private set; }
        public Guid LanguageId => _languageId;
        public string Parameters => _parameters;
        public ReportEmail Email => _email;
        public bool IsActive => _isActive;
        public ReportStatus ReportStatus { get; private set; }
        public string UserName => _userName;

        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public bool AuditIsDeleted { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public Guid TenantId { get; set; }
        #endregion default properties

        private Report() { }

        public static Report Create(string name, string description, bool notifyWhenComplete, Guid templateId, int templateTypeId, int formatId, ReportEmail email, Guid languageId, string parameters, string userName)
        {
            return new Report
            {
                _name = name,
                _description = description,
                _notifyWhenComplete = notifyWhenComplete,
                _templateId = templateId,
                _templateTypeId = templateTypeId,
                _formatId = formatId,
                _email = email,
                _languageId = languageId,
                _parameters = parameters,
                _status = ReportStatus.Unknown.Id,
                _isActive = true,
                _userName = userName
            };
        }

        public void WhenReportStartToGenerate()
        {
            _status = ReportStatus.Generating.Id;
            AddEvent(new ReportGeneratigFired(_id, _status, TenantId));
        }

        public void WhenReportGeneratingCompleted(string filePath, Guid fileId, int frequenycId, string creationDate, string format, string title, string locations, string dateRange, string emailTo, string emailBody, string emailSubject, string languageSortName, string repeatOn,  bool wasRetry)
        {
            if (frequenycId == ScheduleFrequency.OneTime.Id)
                _status = ReportStatus.Ready.Id;

            else if (frequenycId == ScheduleFrequency.Repeat.Id)
            {
                _status = ReportStatus.Scheduled.Id;

                if (!wasRetry)
                    this.ChangeDateRange(repeatOn);
            }

            if (_notifyWhenComplete)
                AddEvent(new ReportGeneratingCompleted(_id, filePath, fileId, _status, TenantId, _name, _description, _userName, creationDate, format, title, locations, dateRange, emailTo, emailBody, emailSubject, languageSortName));
        }

        public void WhenReportGeneratingFailed(int frequenycId, Guid fileId, string repeatOn, bool wasRetry)
        {
            _status = ReportStatus.Failed.Id;

            if (frequenycId == ScheduleFrequency.Repeat.Id && !wasRetry)
                this.ChangeDateRange(repeatOn);

            AddEvent(new ReportGeneratingFailed(_id, fileId, _status, TenantId));
        }

        public void DeActivateReport()
        {
            if (_status != ReportStatus.Generating.Id)
                _isActive = false;
        }

        public void ScheduleOneTime()
        {
            _status = ReportStatus.Generating.Id;
            AddEvent(new OneTimeReportGenerate(_id));
        }

        public void WhenScheduleReportCreated()
        {
            _status = ReportStatus.Scheduled.Id;
            AddEvent(new ScheduleReportCreated(_id));
        }

        public void WhenDeleted()
            => AddEvent(new ReportDeleted(_id));


        #region private methods

        private void ChangeDateRange(string repeatOn)
        {
            if (_templateTypeId == TemplateType.Daily.Id)
            {
                var dateRangeStartDate = this.DateRange.Item1.AddDays(1);

                var dateRangeEndDate = this.DateRange.Item2.AddDays(1);

                _parameters = SetDateRange(dateRangeStartDate, dateRangeEndDate);
            }
            else if (_templateTypeId == TemplateType.Weekly.Id)
            {
                int maxValue = GetWeeklyRepeatOnMaxValue(repeatOn);

                if (maxValue <= ScheduleWeeklyRepeatOn.Current.Id)
                {
                    var dateRangeStartDate = this.DateRange.Item1.AddDays(7);

                    var dateRangeEndDate = this.DateRange.Item2.AddDays(7);

                    _parameters = SetDateRange(dateRangeStartDate, dateRangeEndDate);
                }
            }
            else if (_templateTypeId == TemplateType.Monthly.Id)
            {
                var dateRangeStartDate = this.DateRange.Item1.AddMonths(1);

                var dateRangeEndDate = this.DateRange.Item2.AddMonths(1);

                _parameters = SetDateRange(dateRangeStartDate, dateRangeEndDate);
            }
        }

        private (DateTime, DateTime) DateRange
        {
            get
            {
                var parameters = JsonConvert.DeserializeObject<ReportParameters>(_parameters);

                if (parameters is not null)
                    //todo: technical dept bunu silmeyin elastigin calisma parametereri ile alakali. Su sekilde istedigimiz gibi calisir. En sonda yeniden review edicez.
                    return (DateTime.Parse(parameters.DateRangeStartDate.ToUniversalTime().ToString()), (DateTime.Parse(parameters.DateRangeEndDate.ToUniversalTime().ToString())));
                    

                return (DateTime.UtcNow, DateTime.UtcNow);
            }
        }

        private string SetDateRange(DateTime dateRangeStartDate, DateTime dateRangeEndDate)
        {
            var parameters = JsonConvert.DeserializeObject<ReportParameters>(_parameters);

            parameters.DateRangeStartDate = dateRangeStartDate;
            parameters.DateRangeEndDate = dateRangeEndDate;

            return JsonConvert.SerializeObject(parameters);
        }

        private int GetWeeklyRepeatOnMaxValue(string repeatOn)
        {
            int[] array = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(repeatOn);

            int maxValue = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > maxValue)
                {
                    maxValue = array[i];
                }
            }

            return maxValue;
        }

        #endregion private methods
    }
}
