﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class ReportGeneratingFiredHandler : ICommandHandler<ReportGeneratingFired>
    {
        private readonly IHubContext<ReportNotificationsHub, IReportNotificationsHub> _hubContext;
        private readonly ILogger<ReportGeneratingFiredHandler> _logger;
        public ReportGeneratingFiredHandler(IHubContext<ReportNotificationsHub, IReportNotificationsHub> hubContext, ILogger<ReportGeneratingFiredHandler> logger)
        {
            _logger = logger;
            _hubContext = hubContext;
        }

        public async Task HandleAsync(ReportGeneratingFired command)
        {
            _logger.LogInformation($"ReportGeneratigFiredHandler : {command.TenantId}");

            await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .ReportGeneratingFiredReceived(command);
        }
    }
}

