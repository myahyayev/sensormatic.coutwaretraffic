﻿using System;
using Sensormatic.Tool.Ioc;

namespace CountwareTraffic.Services.Reporting.Application
{
    public interface IIdentityService : ITransientDependency
    {
        Guid UserId { get; }
        string UserName { get; }
    }
}
