﻿using Sensormatic.Tool.Ioc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Core
{
    public interface  IReportLanguageRepository : IRepository<ReportLanguage>, IScopedDependency
    {
        Task<IEnumerable<ReportLanguage>> GetAllReportLanguagesAsync();
    }
}
