﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleDeliveryTime : Enumeration
    {
        public static ScheduleDeliveryTime Unknown = new(1, nameof(Unknown));
        public static ScheduleDeliveryTime RunAutomatically = new(2, nameof(RunAutomatically));
        public static ScheduleDeliveryTime SchedulAtSetTime = new(3, nameof(SchedulAtSetTime));
        public ScheduleDeliveryTime(int id, string name) : base(id, name) { }

        public static IEnumerable<ScheduleDeliveryTime> List() => new[] { Unknown, RunAutomatically, SchedulAtSetTime };


        public static ScheduleDeliveryTime FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
                throw new ScheduleDeliveryTimeNameNotFoundException(List(), name);

            return state;
        }

        public static ScheduleDeliveryTime From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
                throw new ScheduleDeliveryTimeIdNotFoundException(List(), id);

            return state;
        }
    }
}
