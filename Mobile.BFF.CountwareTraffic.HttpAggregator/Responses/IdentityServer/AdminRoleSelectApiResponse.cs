﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer
{
    public class AdminRoleSelectApiResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<AdminRoleSelectApiResponseItem> Data { get; set; }
    }

    public class AdminRoleSelectApiResponseItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
