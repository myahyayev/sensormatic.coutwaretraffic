﻿using Microsoft.AspNetCore.Mvc;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System.ComponentModel.DataAnnotations;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ResetPasswordWithTokenApiRequest : SensormaticRequestValidate
    {
        [FromForm(Name = "password")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string Password { get; set; }

        [FromForm(Name = "token")]
        public string Token { get; set; }

        [FromForm(Name = "confirmPassword")]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string ConfirmPassword { get; set; }

        public override void Validate()
        {
            if (Password.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1244 Value cannot be null", nameof(Password)));

            if (Token.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1245 Value cannot be null", nameof(Token)));

            if (ConfirmPassword.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1246 Value cannot be null", nameof(ConfirmPassword)));

            if ((!Password.IsNullOrWhiteSpace() && !Password.IsValidPassword()) || (!ConfirmPassword.IsNullOrWhiteSpace() && !ConfirmPassword.IsValidPassword()))
            {
                ValidateResults.Add(new ErrorResult($"#E1247 Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)"));

                return;
            }

            if (!Password.IsNullOrWhiteSpace() && !ConfirmPassword.IsNullOrWhiteSpace() && !object.Equals(Password, ConfirmPassword))
                ValidateResults.Add(new ErrorResult($"#E1248 The Password didn't match."));
        }
    }
}