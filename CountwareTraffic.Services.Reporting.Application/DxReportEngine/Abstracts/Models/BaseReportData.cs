﻿namespace CountwareTraffic.Services.Reporting.Engine
{
    public class BaseReportData
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        //public byte[] Logo { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }
}
