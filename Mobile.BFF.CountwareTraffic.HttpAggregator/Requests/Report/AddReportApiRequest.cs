﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AddReportApiRequest
    {
        public string Name { get; set; }
        public Guid TemplateId { get; set; }
        public string Description { get; set; }
        public int FormatId { get; set; }
        public int TemplateTypeId { get; set; }
        public bool NotifyWhenComplete { get; set; }
        public int FrequencyId { get; set; }
        public DateTime? StartsOn { get; set; }
        public DateTime? EndsOn { get; set; }
        public string RepeatOn { get; set; }
        public int DeliveryTimeId { get; set; }
        public string AtSetTime { get; set; }
        public string AtSetTimeZone { get; set; }
        public string EmailTo { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public Guid LanguageId { get; set; }
        public List<Location> Locations { get; set; }
        public DateTime DateRangeStartDate { get; set; }
        public DateTime DateRangeEndDate { get; set; }
    }
}
