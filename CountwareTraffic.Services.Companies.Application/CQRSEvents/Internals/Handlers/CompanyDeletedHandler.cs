﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CompanyDeletedHandler : IEventHandler<CompanyDeleted>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public CompanyDeletedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(CompanyDeleted command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId,
                Name = command.TenantName
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId,
                Name = command.CompanyName
            };

            await _tenantHierarchyElasticSearchRepository.DeleteComapnyAsync(tenantElasticDto, companyElasticDto);
        }
    }
}

