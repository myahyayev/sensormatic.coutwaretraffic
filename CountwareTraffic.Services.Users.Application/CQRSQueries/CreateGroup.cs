﻿using Convey.CQRS.Queries;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreateGroup : IQuery<IdentityCreateGroupResponse>
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
