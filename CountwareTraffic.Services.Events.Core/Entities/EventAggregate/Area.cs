﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Events.Core
{
    public class Area : IEntity, IDeletable
    {
        public Guid _id;
        private string _name;
        private TimeSpan? _workingHoursStart;
        private TimeSpan? _workingHoursEnd;
        private string _workingTimeZone;
        public Guid Id => _id;
        public string Name => _name;
        public TimeSpan? WorkingHoursStart => _workingHoursStart;
        public TimeSpan? WorkingHoursEnd => _workingHoursEnd;
        public string WorkingTimeZone => _workingTimeZone;

        #region default properties
        public bool AuditIsDeleted { get; set; }
        public Guid TenantId { get; set; }
        #endregion default properties

        private Area() { }

        public static Area Create(Guid id, string name, TimeSpan? workingHoursStart, TimeSpan? workingHoursEnd, string workingTimeZone, Guid tenantId)
        {
            var area = new Area
            {
                _id = id,
                _name = name,
                _workingHoursStart = workingHoursStart,
                _workingHoursEnd = workingHoursEnd,
                _workingTimeZone = workingTimeZone,
                TenantId = tenantId
            };

            return area;
        }

        public void Change(string name, TimeSpan? workingHoursStart, TimeSpan? workingHoursEnd, string workingTimeZone)
        {
            if (_name != name) _name = name;

            _workingHoursStart = workingHoursStart;
            _workingHoursEnd = workingHoursEnd;
            _workingTimeZone = workingTimeZone;
        }
    }
}
