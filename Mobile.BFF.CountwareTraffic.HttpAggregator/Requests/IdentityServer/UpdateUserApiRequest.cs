﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class UpdateUserApiRequest
    {
        [FromForm(Name = "id")]
        public Guid Id { get; set; }

        [FromForm(Name = "firstName")]
        public string FirstName { get; set; }

        [FromForm(Name = "lastName")]
        public string LastName { get; set; }

        [FromForm(Name = "email")]
        public string Email { get; set; }

        [FromForm(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [FromForm(Name = "userName")]
        public string UserName { get; set; }

        [FromForm(Name = "status")]
        public bool Status { get; set; }

        [FromForm(Name = "twoFactorEnabled")]
        public bool TwoFactorEnabled { get; set; }

        [FromForm(Name = "gender")]
        public int Gender { get; set; }

        [FromForm(Name = "birthdate")]
        public DateTime Birthdate { get; set; }

        [FromForm(Name = "userType")]
        public int UserType { get; set; }

        [FromForm(Name = "address")]
        public string Address { get; set; }

        [FromForm(Name = "allowSms")]
        public bool AllowSms { get; set; }

        [FromForm(Name = "allowEmail")]
        public bool AllowEmail { get; set; }

        [FromForm(Name = "profilePic")]
        public string ProfilePic { get; set; }

        [FromForm(Name = "areas")]
        public List<AreaDetailApi> Areas { get; set; }

        [FromForm(Name = "roles")]
        public List<string> Roles { get; set; }
    }
}
