﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantGroupId : IQuery<IdentityTenantGroupIdResponse>
    {
        public Guid Id { get; set;}
    }
}
