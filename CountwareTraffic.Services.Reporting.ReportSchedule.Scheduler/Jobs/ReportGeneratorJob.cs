﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Reporting.Application;
using Quartz;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.ReportSchedule.Scheduler
{
    [DisallowConcurrentExecution]
    public class ReportGeneratorJob : IJob
    {
        private readonly IEventDispatcher _eventDispatcher;
        public ReportGeneratorJob(IEventDispatcher eventDispatcher) => _eventDispatcher = eventDispatcher;

        public async Task Execute(IJobExecutionContext context)
        {
            Guid reportId = new Guid(context.JobDetail.Key.Name);

            await _eventDispatcher.PublishAsync(new ReportGenerate
            {
                ReportId = reportId
            });
        }
    }
}
