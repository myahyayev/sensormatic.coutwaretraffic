using Convey;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Quartz;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Scheduler;
using System;

namespace CountwareTraffic.Services.Companies.ProcessOutbox.Scheduler
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) => Configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            IoCGenerator.DoTNet.Current.Start(services, Configuration);

            services.AddDbContext<AreaDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("AreaDbConnection"), x => x.UseNetTopologySuite().EnableRetryOnFailure()));

            services.AddConvey()
                    .AddApplication()
                    .Build();

            var assembly = typeof(Sensormatic.Tool.Scheduler.Controllers.HomeController).Assembly;

            services.AddControllersWithViews()
               .AddApplicationPart(assembly)
               .AddRazorRuntimeCompilation();


            services.Configure<MvcRazorRuntimeCompilationOptions>(options => { options.FileProviders.Add(new EmbeddedFileProvider(assembly)); });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.RegisterQuartzServices(Configuration);

            services.AddSignalR();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection()
               .UseStaticFiles(new StaticFileOptions { FileProvider = new PhysicalFileProvider(AppDomain.CurrentDomain.BaseDirectory + "/wwwroot") })
               .UseRouting()
               .UseAuthorization()
               .UseEndpoints(endpoints => { endpoints.MapHub<JobHub>("/signalr"); })
               .UseEndpoints(endpoints =>
               {
                   endpoints.MapControllerRoute(
                       name: "default",
                       pattern: "{controller=Home}/{action=Index}/{id?}");
               });
        }
    }
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterQuartzServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<QuartzOptions>(configuration.GetSection("Quartz"));

            services.AddQuartz(q =>
            {
                q.UseMicrosoftDependencyInjectionJobFactory();

                q.AddJobListener<JobListener>();
                q.AddSchedulerListener<SchedulerListener>();
                q.AddTriggerListener<TriggerListener>();

                q.ScheduleJob<ProcessOutboxJob>(trigger => trigger
                                    .WithIdentity("ProcessOutboxJob.trigger", "ProcessOutboxJob.trigger.group")
                                    .StartNow()
                                    .WithDescription("1 time in 60 seconds")
                                    .WithSimpleSchedule(x => x
                                       .RepeatForever()
                                       .WithIntervalInSeconds(60)),
                                    x => x
                                       .WithIdentity("ProcessOutboxJob", "ProcessOutboxJob.group")
                                       .WithDescription("ProcessOutbox of area microservice"));
            });

            services.AddQuartzServer(options =>
            {
                options.WaitForJobsToComplete = true;
            });

            return services;
        }
    }
}
