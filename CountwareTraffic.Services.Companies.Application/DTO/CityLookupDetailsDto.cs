﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CityLookupDetailsDto : CityLookupDto
    {
        public string Name { get; set; }
        public Guid CountryId { get; set; }
    }
}
