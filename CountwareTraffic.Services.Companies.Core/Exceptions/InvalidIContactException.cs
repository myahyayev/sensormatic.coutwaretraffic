﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Core
{
    public class InvalidIContactException : DomainException
    {
        public string Field { get; set; }
        public string Value { get; }
        public InvalidIContactException(string field, string value)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1413 Contact info invalid format. {field}-{value} ") }, 400, ResponseMessageType.Error)
        {
            Field = field;
            Value = value;
        }
    }
}


   

