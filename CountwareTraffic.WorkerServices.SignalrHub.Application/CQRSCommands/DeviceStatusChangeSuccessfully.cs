﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    [Contract]
    public class DeviceStatusChangeSuccessfully : ICommand
    {
        public Guid DeviceId { get; set; }
        public string Name { get; set; }
        public int DeviceStatusId { get; set; }
        public string DeviceStatusName { get; set; }
        public Guid RecordId { get; init; }
        public Guid TenantId { get; set; }
    }
}
