﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class AddRoleToUser : IQuery<IdentityAddRoleToUserResponse>
    {
        public Guid UserId { get; set; }
        public string RoleName { get; set; }
    }
}
