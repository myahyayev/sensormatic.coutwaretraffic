﻿using System;
using System.Linq;

namespace Sensormatic.Tool.Queue
{
    public class ConsumerInstance
    {
        public static ConsumerMetadata GetConsumerMetadata<T>()
        {
            var type = typeof(T);

            try
            {
                var interfaceType = type.GetInterfaces().Where(x => x.GetGenericTypeDefinition() == typeof(IConsumer<>)).First();
                var messageType = interfaceType.GenericTypeArguments.First();
                var method = type.GetMethod("ConsumeAsync");

                var isEvent = messageType.GetInterfaces().Any(x => x == typeof(IQueueEvent));

                return new ConsumerMetadata
                {
                    IsEvent = isEvent,
                    MessageType = messageType,
                    Method = method,
                    ConsumerType = type
                };
            }
            catch (Exception ex)
            {
                throw new Exception("Unsupported Consumer. check the Queue model or make sure consumer has been injected ", ex);
            }
        }

        public static ConsumerMetadata GetConsumerMetadata(Type type)
        {
            try
            {
                var interfaceType = type.GetInterfaces().Where(x => x.GetGenericTypeDefinition() == typeof(IConsumer<>)).First();
                var messageType = interfaceType.GenericTypeArguments.First();
                var method = type.GetMethod("ConsumeAsync");

                var isEvent = messageType.GetInterfaces().Any(x => x == typeof(IQueueEvent));

                return new ConsumerMetadata
                {
                    IsEvent = isEvent,
                    MessageType = messageType,
                    Method = method,
                    ConsumerType = type
                };
            }
            catch (Exception ex)
            {
                throw new Exception("Unsupported Consumer. check the Queue model or make sure you injected consumer", ex);
            }
        }
    }
}
