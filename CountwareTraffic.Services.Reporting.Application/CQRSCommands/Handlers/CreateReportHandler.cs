﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Reporting.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class CreateReportHandler : ICommandHandler<CreateReport>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IIdentityService _identityService;
        public CreateReportHandler(IUnitOfWork unitOfWork, IIdentityService identityService)
        {
            _unitOfWork = unitOfWork;
            _identityService = identityService;
        }

        public async Task HandleAsync(CreateReport command)
        {
            var reportRepository = _unitOfWork.GetRepository<IReportRepository>();

            var reportScheduleRepository = _unitOfWork.GetRepository<IReportScheduleRepository>();

            var reportEmail = ReportEmail.Create(command.EmailTo, command.EmailSubject, command.EmailBody);

            ReportParameters reportParameters = new()
            {
                DateRangeEndDate = command.DateRangeEndDate,
                DateRangeStartDate = command.DateRangeStartDate,
                Locations = command.Locations
            };

            string jsonReportParameters = Newtonsoft.Json.JsonConvert.SerializeObject(reportParameters);

            var report = Report
                .Create(command.Name, command.Description, command.NotifyWhenComplete, command.TemplateId, command.TemplateTypeId, command.FormatId, reportEmail, command.LanguageId, jsonReportParameters, _identityService.UserName);

            var reportSchedule = ReportSchedule
                .Create(report.Id, command.FrequencyId, command.DeliveryTimeId, command.StartsOn, command.EndsOn, command.AtSetTime, command.AtSetTimeZone, command.RepeatOn, command.TemplateTypeId);

            if (command.FrequencyId == ScheduleFrequency.OneTime.Id)
                report.ScheduleOneTime();

            if(command.FrequencyId == ScheduleFrequency.Repeat.Id)
                report.WhenScheduleReportCreated();

            await reportRepository.AddAsync(report);
            await reportScheduleRepository.AddAsync(reportSchedule);

            await _unitOfWork.CommitAsync();
        }
    }
}
