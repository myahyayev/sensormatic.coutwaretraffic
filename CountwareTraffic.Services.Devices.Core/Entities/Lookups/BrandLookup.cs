﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Devices.Core
{
    public class BrandLookup : Entity<Guid>
    {
        private string _name;
        public string Name => _name;

        private BrandLookup() => _id = SequentialGuid.Current.Next(null);

        public static BrandLookup Create(string name)
        {
            if(name.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(name));

            if (name.Length > 150)
                throw new ArgumentException(nameof(name));

            return new BrandLookup
            {
                _name = name
            };
        }

        public void Change(string name)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException(nameof(name));

            if (name.Length > 150)
                throw new ArgumentException(nameof(name));

            _name = name;
        }
    }
}
