﻿using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class SubAreaCreated : Convey.CQRS.Events.IEvent
    {
        public Guid SubAreaId { get; init; }
        public string Name { get; init; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
        public string CorrelationId { get; set; }
        public string AreaName { get; set; }
        public Guid AreaId { get; set; }
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid CountryLookupId { get; set; }
        public Guid CityId { get; set; }
        public string CityName { get; set; }
        public Guid DistrictId { get; set; }
        public string DistrictName { get; set; }
    }
}
