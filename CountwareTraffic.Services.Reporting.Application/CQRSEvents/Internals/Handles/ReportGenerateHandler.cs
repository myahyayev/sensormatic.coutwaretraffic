﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Reporting.Core;
using CountwareTraffic.Services.Reporting.Engine;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportGenerateHandler : IEventHandler<ReportGenerate>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ReportInitializer _reportInitializer;
        private readonly ILogger<ReportGenerateHandler> _logger;

        public ReportGenerateHandler(IUnitOfWork unitOfWork, ReportInitializer reportInitializer, ILogger<ReportGenerateHandler> logger)
        {
            _unitOfWork = unitOfWork;
            _reportInitializer = reportInitializer;
            _logger = logger;
        }

        public async Task HandleAsync(ReportGenerate @event)
        {
            var reportRepository = _unitOfWork.GetRepository<IReportRepository>();

            var reportDetail = await reportRepository.GetReportByIdAsync(@event.ReportId);

            await _reportInitializer.GenerateReportFileAndSaveContainerAsync(
               assemblyQualifiedName: reportDetail.ReportTemplate.AssemblyQualifiedName,
               reportId: @event.ReportId,
               reportName: reportDetail.Id.ToString(),
               reportPeriodType: reportDetail.TemplateType.ToEnum<TemplateType, ReportPeriodType>(),
               reportExportype: reportDetail.Format.ToEnum<ReportFormat, ReportExportype>(),
               reportFrequencyType: reportDetail.ReportSchedule.Frequency.ToEnum<ScheduleFrequency, ReportFrequencyType>(),
               reportLocations: reportDetail.Location,
               reportStartDate: reportDetail.DateRangeStartDate,
               reportEndDate: reportDetail.DateRangeEndDate,
               reportRepeatOn: reportDetail.ReportSchedule.RepeatOn,
               reportTenantId: reportDetail.TenantId,
               reportUserName: reportDetail.UserName,
               reportEmailBody: reportDetail.ReportEmail.Body,
               reportEmailTo: reportDetail.ReportEmail.To,
               reportEmailSubject: reportDetail.ReportEmail.Subject,
               language: new Engine.ReportLanguage
                    (
                        reportDetail.ReportLanguage.Id,
                        reportDetail.ReportLanguage.Name,
                        reportDetail.ReportLanguage.ShortName,
                        reportDetail.ReportLanguage.WindowsTimeZone,
                        reportDetail.ReportLanguage.LinuxTimeZone
                     )
               );
        }
    }

    public static class ReportEnumerationMapper
    {
        public static TEnum ToEnum<TEnumeration, TEnum>(this TEnumeration enumeration) where TEnumeration : Enumeration
        {
            if (!Enum.IsDefined(typeof(TEnum), enumeration.Name))
                return default;

            return (TEnum)Enum.Parse(typeof(TEnum), enumeration.Name);
        }
    }
}

