﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleTypeIdNotFoundException : DomainException
    {
        IEnumerable<ScheduleFrequency> ScheduleTypes { get; }
        public int Id { get; }

        public ScheduleTypeIdNotFoundException(IEnumerable<ScheduleFrequency> scheduleTypes, int id)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ScheduleType Id: {String.Join(",", scheduleTypes.Select(s => s.Id))}") }, 400, ResponseMessageType.Error)
        {
            ScheduleTypes = scheduleTypes;
            Id = id;
        }
    }
}
