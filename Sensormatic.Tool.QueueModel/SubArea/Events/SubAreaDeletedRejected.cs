﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record SubAreaDeletedRejected : MessageEnvelope, IQueueEvent
    {
        public SubAreaDeletedRejected(Guid subAreaId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            SubAreaId = subAreaId;
        }
        public Guid SubAreaId { get; }
        public Guid RecordId { get; init; }
    }
}
