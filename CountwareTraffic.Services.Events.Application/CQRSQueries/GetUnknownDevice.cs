﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class GetUnknownDevice : Convey.CQRS.Queries.IQuery<UnknownDeviceDetailsDto>
    {
        public Guid Id { get; set; }
    }
}
