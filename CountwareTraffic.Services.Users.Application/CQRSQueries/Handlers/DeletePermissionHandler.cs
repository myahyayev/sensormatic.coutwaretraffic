﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeletePermissionHandler : IQueryHandler<DeletePermission, IdentityDeletePermissionResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public DeletePermissionHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityDeletePermissionResponse> HandleAsync(DeletePermission query)
        {
            return await _identityServerService.DeletePermissionAsync(query.Id);
        }
    }
}
