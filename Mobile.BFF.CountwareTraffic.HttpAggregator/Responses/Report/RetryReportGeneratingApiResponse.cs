﻿namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class RetryReportGeneratingApiResponse
    {
        public bool IsSuccess { get; set; }
    }
}
