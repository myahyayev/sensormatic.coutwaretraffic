﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportEntityTypeConfiguration : IEntityTypeConfiguration<Report>
    {
        public void Configure(EntityTypeBuilder<Report> builder)
        {
            builder.ToTable("Reports", SchemaNames.Reports);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
                .Property(x => x.Name)
                .HasField("_name")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired()
                .HasMaxLength(150);

            builder
                .Property(x => x.UserName)
                .HasField("_userName")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired()
                .HasMaxLength(150);

            builder
                .Property(x => x.IsActive)
                .HasField("_isActive")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired();

            builder
                .Property(x => x.Description)
                .HasField("_description")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasMaxLength(2500);

            builder
                .Property(x => x.NotifyWhenComplete)
                .HasField("_notifyWhenComplete")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired();

            builder
                .Property<int>("_formatId")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("FormatId")
                .IsRequired();
            builder.HasOne(o => o.ReportFormat)
                .WithMany()
                .HasForeignKey("_formatId");

            builder
                .Property<int>("_templateTypeId")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("TemplateTypeId")
                .IsRequired();
            builder.HasOne(o => o.TemplateType)
                .WithMany()
                .HasForeignKey("_templateTypeId");

            builder
                .Property<int>("_status")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("Status")
                .IsRequired();
            builder.HasOne(o => o.ReportStatus)
                .WithMany()
                .HasForeignKey("_status");


            builder
                .Property(x => x.Parameters)
                .HasField("_parameters")
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
                .HasOne<ReportTemplate>()
                .WithMany()
                .HasForeignKey(x => x.TemplateId)
                .OnDelete(DeleteBehavior.ClientSetNull);


            builder
               .HasOne<ReportLanguage>()
               .WithMany()
               .HasForeignKey(x => x.LanguageId)
               .OnDelete(DeleteBehavior.ClientSetNull);

            var emailNavigationBuilder = builder.OwnsOne(x => x.Email);
            builder
              .Navigation(x => x.Email).Metadata.SetField("_email");
            emailNavigationBuilder.Property(x => x.To).HasMaxLength(2500);
            emailNavigationBuilder.Property(x => x.Body).HasMaxLength(2500);
            emailNavigationBuilder.Property(x => x.Subject).HasMaxLength(500);
        }
    }
}
