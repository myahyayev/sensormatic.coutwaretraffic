﻿using Newtonsoft.Json;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceEventsRangeListener : MessageEnvelope, IQueueCommand
    {
        [JsonConstructor]
        public DeviceEventsRangeListener(List<DeviceEventsRangeElement> deviceEventsRange, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            Items = deviceEventsRange;
        }
        public DeviceEventsRangeListener(Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            Items = new();
        }
        public List<DeviceEventsRangeElement> Items { get; set; }
        public Guid RecordId { get; init; }
    }
    public record DeviceEventsRangeElement : MessageEnvelope
    {
        public DeviceEventsRangeElement(string description, Guid deviceId, int directionTypeId, int onTimeIOCount, DateTime eventDate, DateTime eventStartDate, DateTime eventEndDate, Guid userId, string correlationId, Guid tenantId)
        : base(userId, correlationId, tenantId)
        {
            Description = description;
            DeviceId = deviceId;
            DirectionTypeId = directionTypeId;
            EventDate = eventDate;
            EventStartDate = eventStartDate;
            EventEndDate = eventEndDate;
            OnTimeIOCount = onTimeIOCount;
        }
        public int OnTimeIOCount { get; }
        public string Description { get; }
        public Guid DeviceId { get; }
        public int DirectionTypeId { get; }
        public Guid RecordId { get; init; }
        public DateTime EventDate { get; }
        public DateTime EventStartDate { get; }
        public DateTime EventEndDate { get; }
    }

}
