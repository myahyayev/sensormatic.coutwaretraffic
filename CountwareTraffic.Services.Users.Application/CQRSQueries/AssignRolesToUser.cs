﻿using Convey.CQRS.Queries;

namespace CountwareTraffic.Services.Users.Application
{
    public class AssignRolesToUser : IQuery<IdentityAssignRoleToUserResponse>
    {
        public string UserName { get; set; }
    }
}
