﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class DeleteCountryHandler : ICommandHandler<DeleteCountry>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public DeleteCountryHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(DeleteCountry command)
        {
            var countryRepository = _unitOfWork.GetRepository<ICountryRepository>();

            var country = await countryRepository.GetAsync(command.CountryId);

            if (country is null) throw new CountryNotFoundException(command.CountryId);

            countryRepository.Remove(country);

            var company = await _unitOfWork.GetRepository<ICompanyRepository>().GetByIdAsync(country.CompanyId);

            country.WhenDeleted(_currentTenant.Id, _currentTenant.Name, company.Id, company.Name, country.Id, country.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
