﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Sensormatic.Tool.Core
{
    public static class IQueryableExtensions
    {
        public static IQueryable<TEntity> ApplyFilter<TEntity>(this IQueryable<TEntity> query, List<GridFilter> filters) where TEntity : class
        {
            foreach (var filter in filters)
            {
                Type typeBase = typeof(TEntity);

                PropertyInfo pi = typeBase.GetProperty(filter.Field, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                if (pi == null)
                    continue;

                var param = Expression.Parameter(typeof(TEntity), "p");
                var value = Expression.Property(param, filter.Field);

                Expression body = filter.Operator switch
                {
                    FilterEnum.Eq => Expression.Equal(Expression.Property(param, filter.Field), Expression.Constant(filter.Value)),
                    FilterEnum.Neq => Expression.NotEqual(Expression.Property(param, filter.Field), Expression.Constant(filter.Value)),
                    FilterEnum.StartsWith => Expression.Call(value, typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) }), Expression.Constant(filter.Value)),
                    FilterEnum.Contains => Expression.Call(value, typeof(string).GetMethod("Contains", new Type[] { typeof(string) }), Expression.Constant(filter.Value)),
                    FilterEnum.EndsWith => Expression.Call(value, typeof(string).GetMethod("EndsWith", new Type[] { typeof(string) }), Expression.Constant(filter.Value)),
                    _ => null,
                };

                if (body != null)
                    query = query.Where(Expression.Lambda<Func<TEntity, bool>>(body, param));
            }
            return query;
        }

        public static IQueryable<TEntity> ApplyOrderBy<TEntity>(this IQueryable<TEntity> query, List<SortDescriptor> sorts, params string[] navProperties) where TEntity : class
        {
            for (int i = 0; i < sorts.Count; i++)
            {
                var func = GetOrderBy<TEntity>(sorts[i].Field, sorts[i].Direction);

                if (func != null)
                    query = func(query);

                //foreach (var navProperty in navProperties)
                //{
                //    var navFunc = GetOrderBy<TEntity>(sorts[i].Field, sorts[i].Direction, navProperty);

                //    if (navFunc != null)
                //        query = navFunc(query);
                //}
            }

            return query;
        }


        private static Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> GetOrderBy<TEntity>(string orderColumn, Direction orderType) where TEntity : class
        {
            Type typeQueryable = typeof(IQueryable<TEntity>);
            ParameterExpression argQueryable = Expression.Parameter(typeQueryable, "p");
            var outerExpression = Expression.Lambda(argQueryable, argQueryable);

            Type type = typeof(TEntity);
            ParameterExpression arg = Expression.Parameter(type, "x");

            Expression expr = arg;

            PropertyInfo pi = type.GetProperty(orderColumn, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            if (pi == null)
                return null;

            expr = Expression.Property(expr, pi);
            type = pi.PropertyType;

            LambdaExpression lambda = Expression.Lambda(expr, arg);

            string methodName = orderType == Direction.Asc ? "OrderBy" : "OrderByDescending";

            MethodCallExpression resultExp = Expression.Call(typeof(Queryable), methodName, new Type[] { typeof(TEntity), type }, outerExpression.Body, Expression.Quote(lambda));

            var finalLambda = Expression.Lambda(resultExp, argQueryable);

            return (Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>)finalLambda.Compile();
        }


        //todo : Daha bitmedi lutfen kullanmayin. Mahmud Yahyayev
        private static Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> GetOrderBy<TEntity>(string orderColumn, Direction orderType, string navProperty) where TEntity : class
        {
            Type typeQueryable = typeof(IQueryable<TEntity>);
            ParameterExpression argQueryable = Expression.Parameter(typeQueryable, "p");
            var outerExpression = Expression.Lambda(argQueryable, argQueryable);

            Type typeBase = typeof(TEntity);


            Type originalType = typeBase.GetProperty(navProperty, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance).PropertyType;

            if (originalType == null)
                return null ;

            PropertyInfo pi = originalType.GetProperty(orderColumn, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            if (pi == null)
                return null;

            ParameterExpression arg = Expression.Parameter(originalType, "x");

            Expression expr = arg;

            expr = Expression.Property(expr, pi);

            LambdaExpression lambda = Expression.Lambda(expr, arg);

            string methodName = orderType == Direction.Asc ? "OrderBy" : "OrderByDescending";

            MethodCallExpression resultExp = Expression.Call(typeof(Queryable), methodName, new Type[] { typeof(TEntity), originalType }, outerExpression.Body, Expression.Quote(lambda));

            var finalLambda = Expression.Lambda(resultExp, argQueryable);

            return (Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>)finalLambda.Compile();
        }

    }
}
