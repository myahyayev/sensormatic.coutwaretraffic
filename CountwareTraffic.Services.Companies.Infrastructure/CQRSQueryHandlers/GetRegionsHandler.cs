﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using Sensormatic.Tool.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetRegionsHandler : IQueryHandler<GetRegions, PagingResult<RegionDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetRegionsHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;


        public async Task<PagingResult<RegionDetailsDto>> HandleAsync(GetRegions query)
        {
            int page = query.PagingQuery.Page;
            int limit = query.PagingQuery.Limit;

            var qres = await _unitOfWork.GetRepository<IRegionRepository>()
                                        .GetAllAsync(query.CountryId, query.PagingQuery, query.Filters, query.Sorts);

            if (qres == null)
                return PagingResult<RegionDetailsDto>.Empty;

            return new PagingResult<RegionDetailsDto>(qres.Entities.Select(region => new RegionDetailsDto
            {
                Id = region.Id,
                Name = region.Name,
                Description = region.Description,
                AuditCreateBy = region.AuditCreateBy,
                AuditCreateDate = region.AuditCreateDate,
                AuditModifiedBy = region.AuditModifiedBy,
                AuditModifiedDate = region.AuditModifiedDate,
                CountryId = region.CountryId,
                ManagerName = region.ManagerName,
                ManagerId = region.ManagerId

            }), qres.Total, page, limit, qres.Total > (limit * (page - 1)) + qres.Entities.Count);
        }
    }
}

