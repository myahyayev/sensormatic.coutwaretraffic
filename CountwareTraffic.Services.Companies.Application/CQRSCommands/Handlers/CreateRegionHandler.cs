﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CreateRegionHandler : ICommandHandler<CreateRegion>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public CreateRegionHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(CreateRegion command)
        {
            var regionRepository = _unitOfWork.GetRepository<IRegionRepository>();

            var countryRepository = _unitOfWork.GetRepository<ICountryRepository>();

            if (!await countryRepository.ExistsAsync(command.CountryId))
                throw new CountryNotFoundException(command.CountryId);

            if (await regionRepository.ExistsAsync(command.Name, command.CountryId))
                throw new RegionAlreadyExistsException(command.Name);

            var region = Region.Create(command.Name, command.Description, command.CountryId, command.ManagerId, command.ManagerName);

            await regionRepository.AddAsync(region);

            var hierarchyData = await countryRepository.GetHierarchyAsync(region.CountryId, _currentTenant.Id);

            region.WhenCreated(_currentTenant.Id, hierarchyData.CopmanyId,  hierarchyData.CountryId, region.Id, region.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
