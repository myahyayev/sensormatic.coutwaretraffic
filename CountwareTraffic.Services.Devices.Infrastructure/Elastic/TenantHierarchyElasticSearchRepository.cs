﻿using CountwareTraffic.Services.Devices.Application;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nest;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.ElasticSearch;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class TenantHierarchyElasticSearchRepository : ElasticRepository<TenantHierarchyElasticData>, ITenantHierarchyElasticSearchRepository
    {
        private readonly IQueueService _queueService;
        public TenantHierarchyElasticSearchRepository(IConfiguration configuration, ILogger<TenantHierarchyElasticData> logger, IQueueService queueService) 
            : base(configuration, logger, ElasticsearchKeys.CountwareTrafficTenantsHierarchy) 
        {
            _queueService = queueService;
        }

        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {

                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        private async Task MapAsync()
        {
            if ((await _client.Indices.ExistsAsync(_index)).Exists)
                return;

            var createIndexResponse = await _client.Indices.CreateAsync(_index, c => c
                .Map<TenantHierarchyElasticData>(m => m
                    .AutoMap()
                    ));

            if (!createIndexResponse.IsValid)
                throw createIndexResponse.OriginalException;
        }

        private async Task<TenantHierarchyElasticData> GetTenantHierarchyAsync(Guid tenantId)
        {
            await MapAsync();

            var descriptor = new QueryContainerDescriptor<TenantHierarchyElasticData>();

            descriptor.Bool(b => b.Must(m => m.Term(t => t.Field(f => f.Id).Value(tenantId))));

            var result = await _client.SearchAsync<TenantHierarchyElasticData>(
                search => search
                .Index(_index)
                .Source(sf => sf
                   .Includes(i => i
                       .Fields(f => f.Id, f => f.Parent))).Query(d => descriptor));

            if (!result.IsValid)
                throw new ElasticSearchQueryException(_index, result.ServerError.Status, result.ServerError.Error.Type, result.OriginalException);

            var elasticData = result.Documents.FirstOrDefault();

            return elasticData;
        }

        public async Task AddDeviceAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto, DeviceElasticDto deviceElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Tenant hierarchy not found with id: {tenantElasticDto.Id}"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));

                var countryChild = companyChild.Children.First(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var regionChild = countryChild.Children.First(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                var areaChild = regionChild.Children.First(u => u.Data.Id == areaElasticDto.Id);

                if (areaChild == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Area hierarchy not found with id: {areaElasticDto.Id}"));

                var subAreaChild = areaChild.Children.First(u => u.Data.Id == subAreaElasticDto.Id);

                if (subAreaChild == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"SubArea hierarchy not found with id: {subAreaElasticDto.Id}"));

                if (subAreaChild.Children == null)
                    subAreaChild.Children = new();

                subAreaChild.Children.Add(new Child
                {
                    Label = deviceElasticDto.Name,
                    Data = new()
                    {
                        HierarchyLevel = ElasticHierarchyLevel.Device,
                        Id = deviceElasticDto.Id
                    }

                });
                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Tenant hierarchy not found with id: {tenantElasticDto.Id}"));
        }

        public async Task UpdateDeviceAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto, DeviceElasticDto deviceElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null && !companyChild.Children.Exists(u => u.Data.Id == countryElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var countryChild = companyChild.Children.First(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null && !countryChild.Children.Exists(u => u.Data.Id == regionElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                var regionChild = countryChild.Children.First(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild.Children == null && !regionChild.Children.Exists(u => u.Data.Id == areaElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Area hierarchy not found with id: {regionElasticDto.Id}"));

                var areaChild = regionChild.Children.First(u => u.Data.Id == areaElasticDto.Id);

                if (areaChild.Children == null && !areaChild.Children.Exists(u => u.Data.Id == subAreaElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"SubArea hierarchy not found with id: {subAreaElasticDto.Id}"));

                var subAreaChild = areaChild.Children.First(u => u.Data.Id == subAreaElasticDto.Id);
                if (subAreaChild.Children == null && !subAreaChild.Children.Exists(u => u.Data.Id == deviceElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Device hierarchy not found with id: {subAreaElasticDto.Id}"));

                subAreaChild.Children.First(u => u.Data.Id == deviceElasticDto.Id).Label = deviceElasticDto.Name;

                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task DeleteDeviceAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto, DeviceElasticDto deviceElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var countryChild = companyChild.Children.FirstOrDefault(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                var regionChild = countryChild.Children.FirstOrDefault(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Area hierarchy not found with id: {areaElasticDto.Id}"));

                var areaChild = regionChild.Children.FirstOrDefault(u => u.Data.Id == areaElasticDto.Id);

                if (areaChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"SubArea hierarchy not found with id: {areaElasticDto.Id}"));


                var subAreaChild = areaChild.Children.FirstOrDefault(u => u.Data.Id == subAreaElasticDto.Id);

                if (subAreaChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Device hierarchy not found with id: {subAreaElasticDto.Id}"));


                var deviceChild = subAreaChild.Children.FirstOrDefault(u => u.Data.Id == deviceElasticDto.Id);

                if (deviceChild != null)
                {
                    subAreaChild.Children.Remove(deviceChild);
                    await base.UpdateAsync(elasticData);
                    _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
                }
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }
    }
}