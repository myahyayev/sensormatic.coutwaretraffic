﻿using Convey.CQRS.Queries;
using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetPermissionPagination : IQuery<IdentityPermissionPaginationResponse>
    {
        public PagingQuery Paging { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
        public string Language { get; set; }
    }
}
