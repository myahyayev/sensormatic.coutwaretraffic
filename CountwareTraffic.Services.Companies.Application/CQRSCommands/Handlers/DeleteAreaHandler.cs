﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class DeleteAreaHandler : ICommandHandler<DeleteArea>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public DeleteAreaHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(DeleteArea command)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var area = await areaRepository.GetAsync(command.AreaId, _currentTenant.Id);

            if (area is null) throw new AreaNotFoundException(command.AreaId);

            areaRepository.Remove(area);

            var regionRepository = _unitOfWork.GetRepository<IRegionRepository>();

            var hierarchyData = await regionRepository.GetHierarchyAsync(area.RegionId, _currentTenant.Id);

            area.WhenDeleted(area.Id);
            area.WhenHierarchyDeleted(_currentTenant.Id, hierarchyData.CopmanyId, hierarchyData.CountryId, hierarchyData.RegionId, area.Id, area.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
