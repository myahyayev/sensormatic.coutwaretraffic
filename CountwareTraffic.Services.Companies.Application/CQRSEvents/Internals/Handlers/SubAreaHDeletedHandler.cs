﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class SubAreaHDeletedHandler : IEventHandler<SubAreaHDeleted>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public SubAreaHDeletedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(SubAreaHDeleted command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId
            };

            CountryElasticDto countryElasticDto = new()
            {
                Id = command.CountryId
            };

            RegionElasticDto regionElasticDto = new()
            {
                Id = command.RegionId
            };

            AreaElasticDto areaElasticDto = new()
            {
                Id = command.AreaId
            };

            SubAreaElasticDto subAreaElasticDto = new()
            {
                Id = command.SubAreaId,
                Name = command.SubAreaName
            };

            await _tenantHierarchyElasticSearchRepository.DeleteSubAreaAsync(tenantElasticDto, companyElasticDto, countryElasticDto, regionElasticDto, areaElasticDto, subAreaElasticDto);
        }
    }
}

