﻿using CountwareTraffic.Services.Devices.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class BrandLookupEntityTypeConfiguration : IEntityTypeConfiguration<BrandLookup>
    {
        public void Configure(EntityTypeBuilder<BrandLookup> builder)
        {
            builder.ToTable("BrandLookups", SchemaNames.Devices);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);
        }
    }
}
