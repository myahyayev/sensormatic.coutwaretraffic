﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record EventCreated : MessageEnvelope, IQueueEvent
    {
        public EventCreated(Guid eventId, DirectionType directionType, DateTime eventDate, DateTime eventStartDate, DateTime eventEndDate, int onTimeIOCount, Guid deviceId, string deviceName, Guid subAreaId, string subAreaName, Guid areaId, string areaName, Guid cityId, string cityName, Guid districtId, string districtName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid recordId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            EventId = eventId;
            DirectionType = directionType;
            EventDate = eventDate;
            EventStartDate = eventStartDate;
            EventEndDate = eventEndDate;
            OnTimeIOCount = onTimeIOCount;
            DeviceId = deviceId;
            DeviceName = deviceName;
            SubAreaId = subAreaId;
            SubAreaName = subAreaName;
            AreaId = areaId;
            AreaName = areaName;
            CityId = cityId;
            CityName = cityName;
            DistrictId = districtId;
            DistrictName = districtName;
            RegionId = regionId;
            RegionName = regionName;
            CountryId = countryId;
            CountryName = countryName;
            CompanyId = companyId;
            CompanyName = companyName;
            RecordId = recordId;

        }
       
        //EventInfo
        public Guid EventId { get; }
        public DirectionType DirectionType { get; }
        public DateTime EventDate { get; }
        public DateTime EventStartDate { get; }
        public DateTime EventEndDate { get; }
        public int OnTimeIOCount { get; }


        //DeviceInfo
        public Guid DeviceId { get; }
        public string DeviceName { get; }
        public Guid SubAreaId { get; }
        public string SubAreaName { get; }
        public Guid AreaId { get; }
        public string AreaName { get; }
        public Guid CityId { get; }
        public string CityName { get; }
        public Guid DistrictId { get; }
        public string DistrictName { get; }
        public Guid RegionId { get; }
        public string RegionName { get; }
        public Guid CountryId { get; }
        public string CountryName { get; }
        public Guid CompanyId { get; }
        public string CompanyName { get; }

        //SystemInfo
        public Guid RecordId { get; init; }
    }

    public enum DirectionType : byte
    {
        Unknown = 1,
        Inwards = 2,
        Outward = 3
    }
}
