﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetRoleUsersHandler : IQueryHandler<GetRoleUsers, IdentityRoleUsersResponse>
    { 
        private readonly IIdentityServerService _identityServerService;

        public GetRoleUsersHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityRoleUsersResponse> HandleAsync(GetRoleUsers query)
        {
            return await _identityServerService.GetRoleUsersAsync(query.RoleId);
        }
    }
}
