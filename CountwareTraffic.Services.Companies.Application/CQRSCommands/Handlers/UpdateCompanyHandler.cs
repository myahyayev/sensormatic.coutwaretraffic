﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class UpdateCompanyHandler : ICommandHandler<UpdateCompany>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public UpdateCompanyHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(UpdateCompany command)
        {
            var companyRepository = _unitOfWork.GetRepository<ICompanyRepository>();

            var company = await companyRepository.GetAsync(command.CompanyId);

            if (company is null)
                throw new CompanyNotFoundException(command.CompanyId);

            var updateControl = await companyRepository.GetAsync(command.Name);

            if (updateControl != null && updateControl.Id != command.CompanyId)
                throw new CompanyAlreadyExistsException(command.Name);

            company.CompleteChange(
                command.Name,
                command.Description,
                new CompanyContactEntityDto
                {
                    EmailAddress = command.EmailAddress,
                    EmailAddressSecondary = command.EmailAddressSecondary,
                    GsmCountryCode = command.GsmCountryCode,
                    GsmCountryCodeSecondary = command.GsmCountryCodeSecondary,
                    GsmDialCode = command.GsmDialCode,
                    GsmDialCodeSecondary = command.GsmDialCodeSecondary,
                    GsmNumber = command.GsmNumber,
                    GsmNumberSecondary = command.GsmNumberSecondary,
                    PersonName = command.PersonName,
                    PersonNameSecondary = command.PersonNameSecondary,
                    PhoneCountryCode = command.PhoneCountryCode,
                    PhoneCountryCodeSecondary = command.PhoneCountryCodeSecondary,
                    PhoneDialCode = command.PhoneDialCode,
                    PhoneDialCodeSecondary = command.PhoneDialCodeSecondary,
                    PhoneNumber = command.PhoneNumber,
                    PhoneNumberSecondary = command.PhoneNumberSecondary
                },
                new CompanyAddressEntityDto
                {
                    City = command.City,
                    Country = command.Country,
                    Latitude = command.Latitude,
                    Longitude = command.Longitude,
                    State = command.State,
                    Street = command.Street,
                    ZipCode = command.ZipCode
                }
                );

            company.WhenChanged(_currentTenant.Id, _currentTenant.Name, company.Id, company.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
