﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Devices.Grpc
{
    [ServiceLog]
    public sealed partial class GetDeviceBrandModelRequest : SensormaticRequestValidate
    {
        internal Guid _BrandModelId
        {
            get
            {
                if (Guid.TryParse(brandModelId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { brandModelId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_BrandModelId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1501 BrandModelId cannot be null", nameof(_BrandModelId)));
        }
    }
}
