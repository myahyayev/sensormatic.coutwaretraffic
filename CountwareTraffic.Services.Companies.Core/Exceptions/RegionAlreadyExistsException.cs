﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Core
{
    public class RegionAlreadyExistsException : DomainException
    {
        public string Name { get; }

        public RegionAlreadyExistsException(string name)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1399 Region with name: {name} already exists.") }, 409, ResponseMessageType.Error)
        {
            Name = name;
        }
    }
}