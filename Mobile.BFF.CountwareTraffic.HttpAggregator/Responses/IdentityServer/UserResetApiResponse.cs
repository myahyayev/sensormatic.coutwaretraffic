﻿using System.Text.Json.Serialization;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class UserResetApiResponse
    {
        [JsonPropertyName("response")]
        public bool Response { get; set; }
    }
}
