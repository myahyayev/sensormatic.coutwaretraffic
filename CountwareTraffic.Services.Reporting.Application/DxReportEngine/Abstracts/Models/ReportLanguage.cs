﻿using System;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public class ReportLanguage
    {
        public Guid Id { get; init; }
        public string Name { get; init; }
        public string ShortName { get; init; }
        public string WindowsTimeZone { get; init; }
        public string LinuxTimeZone { get; init; }

        public ReportLanguage(Guid id, string name, string shortName, string windowsTimeZone, string linuxTimeZone)
        {
            this.Id = id;
            this.Name = name;
            this.ShortName = shortName;
            this.WindowsTimeZone = windowsTimeZone;
            this.LinuxTimeZone = linuxTimeZone;
        }
    }
}
