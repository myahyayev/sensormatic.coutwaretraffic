﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteUser : IQuery<IdentityDeleteUserResponse>
    {
        public Guid Id { get; set; }
    }
}
