﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{

    [Authorize]
    public class LookupsController : BaseController
    {
        private readonly LookupService _lookupService;
        private readonly DeviceService _deviceService;
        public LookupsController(IServiceProvider provider, ILogger<LookupsController> logger, IHttpContextAccessor contextAccessor, LookupService lookupService, DeviceService deviceService)
            : base(provider, logger, contextAccessor)
        {
            _deviceService = deviceService;
            _lookupService = lookupService;
        }
       
        [HttpGet("country/{countryId}/detail")]
        [Permissions(Permissions = new[] { Permissions.AdminCountryView, Permissions.AdminAreaAdd, Permissions.AdminAreaEdit })]
        public async Task<ApiResponse<GetCountryLookupDetails>> GetCountryLookupDetailAsync(Guid countryId)
            => Response(await _lookupService.GetCountryLookupByIdAsync(countryId), new ResultMessage("#S1094"));

        [HttpGet("country/details")]
        [Permissions(Permissions = new[] { Permissions.AdminCountryList, Permissions.AdminAreaAdd, Permissions.AdminAreaEdit })]
        public async Task<ApiResponse<IEnumerable<GetCountryLookupDetails>>> GetCountryLookupDetailsAsync()
          => Response(await _lookupService.GetCountryLookupsAsync(), new ResultMessage("#S1095"));

        [HttpGet("city/{cityId}/detail")]
        [Permissions(Permissions = new[] { Permissions.AdminCityView, Permissions.AdminAreaAdd, Permissions.AdminAreaEdit })]
        public async Task<ApiResponse<GetCityLookupDetails>> GetCityLookupDetailAsync(Guid cityId)
            => Response(await _lookupService.GetCityLookupByIdAsync(cityId), new ResultMessage("#S1096"));

        [HttpGet("city/{countryId}/details")]
        [Permissions(Permissions = new[] { Permissions.AdminCityList, Permissions.AdminAreaAdd, Permissions.AdminAreaEdit })]
        public async Task<ApiResponse<IEnumerable<GetCityLookupDetails>>> GetCityLookupDetailsAsync(Guid countryId)
           => Response(await _lookupService.GetCityLookupsAsync(countryId), new ResultMessage("#S1097"));

        [HttpGet("district/{districtId}/detail")]
        [Permissions(Permissions = new[] { Permissions.AdminDistrictView, Permissions.AdminAreaAdd, Permissions.AdminAreaEdit })]
        public async Task<ApiResponse<GetDistrictLookupDetails>> GetDistrictLookupDetailAsync(Guid districtId)
            => Response(await _lookupService.GetDistrictLookupByIdAsync(districtId), new ResultMessage("#S1098"));

        [HttpGet("district/{cityId}/details")]
        [Permissions(Permissions = new[] { Permissions.AdminDistrictList, Permissions.AdminAreaAdd, Permissions.AdminAreaEdit })]
        public async Task<ApiResponse<IEnumerable<GetDistrictLookupDetails>>> GetDistrictLookupDetailsAsync(Guid cityId)
           => Response(await _lookupService.GetDistrictLookupsAsync(cityId), new ResultMessage("#S1099"));

        [HttpGet("device-brand/details")]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandList })]
        public async Task<ApiResponse<IEnumerable<GetDeviceBrandDetails>>> GetDeviceBrandDetailsAsync()
            => Response(await _deviceService.GetDeviceBrandsAsync(), new ResultMessage("#S1100"));

        [HttpGet("device-brand/{brandId}/detail")]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandView })]
        public async Task<ApiResponse<GetDeviceBrandDetails>> GetDeviceBrandDetailAsync(Guid brandId)
           => Response(await _deviceService.GetDeviceBrandByIdAsync(brandId), new ResultMessage("#S1101"));

        [HttpGet("device-brand-model/{brandId}/details")]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandList })]
        public async Task<ApiResponse<IEnumerable<GetDeviceBrandModelDetails>>> GetDeviceBrandModelDetailsAsync(Guid brandId)
           => Response(await _deviceService.GetDeviceBrandModelsAsync(brandId), new ResultMessage("#S1102"));

        [HttpGet("device-brand-model/{modelId}/detail")]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandView })]
        public async Task<ApiResponse<GetDeviceBrandModelDetails>> GetDeviceBrandModelDetailAsync(Guid modelId)
           => Response(await _deviceService.GetDeviceBrandModelByIdAsync(modelId), new ResultMessage("#S1103"));

        [HttpPost("device-brand/add"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandAdd })]
        public async Task<ApiResponse<CrudApiResponse>> AddDeviceBrandAsync([FromBody] AddDeviceBrandApiRequest request)
        {
            await _deviceService.AddDeviceBrandAsync(request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Create }, new ResultMessage("#S1104"));
        }

        [HttpPost("device-brand-model/{brandId}/add"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandAdd})]
        public async Task<ApiResponse<CrudApiResponse>> AddAreaAsync(Guid brandId, [FromForm] AddDeviceBrandModelApiRequest request)
        {
            await _deviceService.AddDeviceBrandModelAsync(brandId, request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Create }, new ResultMessage("#S1105"));
        }

        [HttpPut("device-brand/{brandId}/change"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandEdit })]
        public async Task<ApiResponse<CrudApiResponse>> ChangeCompanyAsync(Guid brandId, [FromForm] ChangeDeviceBrandApiRequest request)
        {
            await _deviceService.ChangeDeviceBrandAsync(brandId, request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Update }, new ResultMessage("#S1106"));
        }

        [HttpPut("device-brand-model/{brandModelId}/change"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandEdit })]
        public async Task<ApiResponse<CrudApiResponse>> ChangeDeviceBrandModelAsync(Guid brandModelId, [FromForm] ChangeDeviceBrandModelApiRequest request)
        {
            await _deviceService.ChangeDeviceBrandModelAsync(brandModelId, request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Update }, new ResultMessage("#S1107"));
        }

        [HttpDelete("device-brand/{brandId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandDelete })]
        public async Task<ApiResponse<CrudApiResponse>> DeleteDeviceBrandAsync(Guid brandId)
        {
            var brandModelControl = await _deviceService.GetDeviceBrandModelsAsync(brandId);

            if (brandModelControl !=null && brandModelControl.Count() > 0)
            {
                return Response(new CrudApiResponse { IsSuccess = false, OperationType = OperationType.Delete, Id = brandId }, new ResultMessage()
                {
                    CallToActionType = CallToActionType.GoToBrandModel,
                    Description = $"#W1029 Brand Model has been defined in the Brand with id {brandId}.",
                    Title = $"#W1029 Device Brand with id {brandId} cannot be deleted."
                });
            }

            await _deviceService.DeleteDeviceBrandAsync(brandId);

            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Delete, Id = brandId }, new ResultMessage()
            {
                CallToActionType = CallToActionType.Ok,
                Description = $"#S1108 Brand with {brandId} id has been deleted.",
                Title = $"#S1108 Brand with {brandId} id has been deleted."
            });
        }

        [HttpDelete("device-brand-model/{brandModelId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminDeviceBrandDelete })]
        public async Task<ApiResponse<CrudApiResponse>> DeleteCountryAsync(Guid brandModelId)
        {
            await _deviceService.DeleteDeviceBrandModelAsync(brandModelId);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Delete }, new ResultMessage("#S1109"));
        }

    }
}
