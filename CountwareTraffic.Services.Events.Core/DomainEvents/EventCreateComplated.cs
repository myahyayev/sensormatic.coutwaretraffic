﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Events.Core
{
    public class EventCreateCompleted : IDomainEvent
    {
        //EventInfo
        public Guid EventId { get; init; }
        public DirectionType DirectionType { get; init; }
        public DateTime EventDate { get; init; }
        public DateTime EventStartDate { get; init; }
        public DateTime EventEndDate { get; init; }
        public int OnTimeIOCount { get; init; }


        //DeviceInfo
        public Guid DeviceId { get; init; }
        public string DeviceName { get; init; }
        public Guid SubAreaId { get; init; }
        public string SubAreaName { get; init; }
        public Guid AreaId { get; init; }
        public string AreaName { get; init; }
        public Guid CityId { get; init; }
        public string CityName { get; init; }
        public Guid DistrictId { get; init; }
        public string DistrictName { get; init; }
        public Guid RegionId { get; init; }
        public string RegionName { get; init; }
        public Guid CountryId { get; init; }
        public string CountryName { get; init; }
        public Guid CompanyId { get; init; }
        public string CompanyName { get; init; }
        public Guid TenantId { get; init; }


        //SystemInfo
        public Guid RecordId { get; init; }


        public EventCreateCompleted(Guid eventId, DirectionType directionType, DateTime eventDate, DateTime eventStartDate, DateTime eventEndDate,int onTimeIOCount, Guid deviceId, string deviceName, Guid subAreaId, string subAreaName, Guid areaId, string areaName, Guid cityId, string cityName, Guid districtId, string districtName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid tenantId)
        {
            EventId = eventId;
            DirectionType = directionType;
            EventDate = eventDate;
            EventStartDate = eventStartDate;
            EventEndDate = eventEndDate;
            OnTimeIOCount = onTimeIOCount;
            DeviceId = deviceId;
            DeviceName = deviceName;
            SubAreaId = subAreaId;
            SubAreaName = subAreaName;
            AreaId = areaId;
            AreaName = areaName;
            CityId = cityId;
            CityName = cityName;
            DistrictId = districtId;
            DistrictName = districtName;
            RegionId = regionId;
            RegionName = regionName;
            CountryId = countryId;
            CountryName = countryName;
            CompanyId = companyId;
            CompanyName = companyName;
            TenantId = tenantId;
            RecordId = Guid.NewGuid();
        }
    }
}
