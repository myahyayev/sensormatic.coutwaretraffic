﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRAreaCreatedFailedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaCreatedFailed>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRAreaCreatedFailedConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaCreatedFailed queueEvent)
        {
            await _commandDispatcher.SendAsync(new AreaCreatedFailed
            {
                AreaId = queueEvent.AreaId,
                AreaStatus = queueEvent.AreaStatus,
                UserId = queueEvent.UserId,
                UserName = queueEvent.UserName,
                TenantId = queueEvent.TenantId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
