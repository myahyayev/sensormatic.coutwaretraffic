﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Events.Application
{
    [Contract]
    public class UnknownDeviceProcessed : ICommand 
    {
        public Guid Id { get; set; }
    }
}
