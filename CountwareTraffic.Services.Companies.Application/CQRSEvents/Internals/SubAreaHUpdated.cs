﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class SubAreaHUpdated : Convey.CQRS.Events.IEvent
    {
        public Guid SubAreaId { get; set; }
        public string SubAreaName { get; set; }
        public Guid AreaId { get; set; }
        public Guid RegionId { get; set; }
        public Guid CountryId { get; set; }
        public Guid ComapnyId { get; init; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
    }
}
