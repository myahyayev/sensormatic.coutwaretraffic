﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class GetReportTemplatesHandler : IQueryHandler<GetReportTemplates, IEnumerable<ReportTemplateDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetReportTemplatesHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IEnumerable<ReportTemplateDetailsDto>> HandleAsync(GetReportTemplates query)
        {
            var reportTemplateRepository = _unitOfWork.GetRepository<IReportTemplateRepository>();

            var qres = await reportTemplateRepository.GetAllTemplatesAsync();

            return qres.Select(u => new ReportTemplateDetailsDto
            {
                AssemblyQualifiedName = u.AssemblyQualifiedName,
                AuditCreateBy = u.AuditCreateBy,
                AuditCreateDate = u.AuditCreateDate,
                AuditModifiedBy = u.AuditModifiedBy,
                AuditModifiedDate = u.AuditModifiedDate,
                Description = u.Description,
                Id = u.Id,
                Name = u.Name,
                TemplateTypeDtos = u.TemplateTypeEntityDtos.Select(c => new TemplateTypeDto
                {
                    TypeId = c.TypeId,
                    Name = c.Name
                })
            });
        }
    }
}
