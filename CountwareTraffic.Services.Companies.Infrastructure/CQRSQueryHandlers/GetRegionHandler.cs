﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetRegionHandler : IQueryHandler<GetRegion, RegionDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetRegionHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<RegionDetailsDto> HandleAsync(GetRegion query)
        {
            var region = await _unitOfWork.GetRepository<IRegionRepository>()
                .GetAsync(query.RegionId);

            if (region == null)
                throw new RegionNotFoundException(query.RegionId);

            return new RegionDetailsDto
            {
                Id = region.Id,
                Name = region.Name,
                Description = region.Description,
                AuditCreateBy = region.AuditCreateBy,
                AuditCreateDate = region.AuditCreateDate,
                AuditModifiedBy = region.AuditModifiedBy,
                AuditModifiedDate = region.AuditModifiedDate,
                CountryId = region.CountryId,
                ManagerId = region.ManagerId,
                ManagerName = region.ManagerName
            };
        }
    }
}
