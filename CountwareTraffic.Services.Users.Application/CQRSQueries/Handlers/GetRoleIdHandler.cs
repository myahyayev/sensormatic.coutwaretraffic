﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetRoleIdHandler : IQueryHandler<GetRoleId, IdentityRoleIdResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetRoleIdHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityRoleIdResponse> HandleAsync(GetRoleId query)
        {
            return await _identityServerService.GetRoleIdAsync(query.Id, query.Language);
        }
    }
}
