﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class HierarchyRequest
    {
        public HierarchyLevel HierarchyLevel { get; set; }
        public Guid Id { get; set; }
    }

    public class DeviceFilterRequest
    {
        public HierarchyLevel HierarchyLevel { get; set; }
        public Guid ParentId { get; set; }
    }
}
