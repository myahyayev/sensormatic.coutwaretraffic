﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class DeviceController : BaseController
    {
        private readonly DeviceService _deviceService;
        public DeviceController(IServiceProvider provider, ILogger<DeviceController> logger, IHttpContextAccessor contextAccessor, DeviceService deviceService)
            : base(provider, logger, contextAccessor.HttpContext) => _deviceService = deviceService;


        [HttpGet("{deviceId}/detail"), ServiceLog]
        public async Task<ApiResponse<GetDeviceDetails>> GetDeviceDetailAsync(Guid deviceId)
            => Response(await _deviceService.GetDeviceByIdAsync(deviceId), ResultMessage.SuccessResultMessage);


        [HttpPost("{subAreaId}/details"), ServiceLog]
        public async Task<ApiPagedResponse<GetDeviceDetails>> GetDeviceDetailsAsync(Guid subAreaId, [FromBody] DataSourceApiRequest paging)
            => await _deviceService.GetDevicesAsync(subAreaId, paging);


        [HttpGet("statuses"), ServiceLog]
        public async Task<ApiResponse<IEnumerable<GetDeviceStatus>>> GetDeviceStatusesAsync()
           => Response(await _deviceService.GetDeviceStatusesAsync(), ResultMessage.SuccessResultMessage);


        [HttpGet("types"), ServiceLog]
        public async Task<ApiResponse<IEnumerable<GetDeviceType>>> GetDeviceTypesAsync()
            => Response(await _deviceService.GetDeviceTypesAsync(), ResultMessage.SuccessResultMessage);


        [HttpPost("{subAreaId}/add"), ServiceLog]
        public async Task<ApiResponse<AddApiResponse>> AddDeviceAsync(Guid subAreaId, [FromForm] AddDeviceApiRequest request)
        {
            await _deviceService.AddDeviceAsync(subAreaId, request);
            return Response(new AddApiResponse { Created = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpPut("{deviceId}/change"), ServiceLog]
        public async Task<ApiResponse<ChangeApiResponse>> ChangeDeviceAsync(Guid deviceId, [FromForm] ChangeDeviceApiRequest request)
        {
            await _deviceService.ChangeDeviceAsync(deviceId, request);
            return Response(new ChangeApiResponse { Updated = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpDelete("{deviceId}/delete"), ServiceLog]
        public async Task<ApiResponse<DeleteApiResponse>> DeleteCountryAsync(Guid deviceId)
        {
            await _deviceService.DeleteDeviceAsync(deviceId);
            return Response(new DeleteApiResponse { Deleted = "Success" }, ResultMessage.SuccessResultMessage);
        }
    }
}
