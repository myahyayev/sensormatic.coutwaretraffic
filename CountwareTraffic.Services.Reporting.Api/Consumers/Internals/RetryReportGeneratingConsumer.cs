﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Reporting.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Api
{
    public class RetryReportGeneratingConsumer : IConsumer<Sensormatic.Tool.QueueModel.RetryReportGeneration>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        public RetryReportGeneratingConsumer(IEventDispatcher eventDispatcher)=> _eventDispatcher = eventDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.RetryReportGeneration queueCommand)
        {
            await _eventDispatcher.PublishAsync(new RetryReportGenerate
            {
                ReportId = queueCommand.ReportId,
                ReportFileId = queueCommand.ReportFileId,
                DateRangeStartDate = queueCommand.DateRangeStartDate,
                DateRangeEndDate = queueCommand.DateRangeEndDate
            });
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
