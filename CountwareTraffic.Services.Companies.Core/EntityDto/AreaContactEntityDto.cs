﻿namespace CountwareTraffic.Services.Companies.Core
{
    public class AreaContactEntityDto
    {
        public string GsmNumber { get; set; }
        public string GsmCountryCode { get; set; }
        public string GsmDialCode { get; set; }
        public string PhoneDialCode { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneCountryCode { get; set; }
        public string EmailAddress { get; set; }
    }
}
