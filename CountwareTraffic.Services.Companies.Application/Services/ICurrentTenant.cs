﻿using Sensormatic.Tool.Ioc;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public interface ICurrentTenant : ITransientDependency
    {
        Guid Id { get; }
        string Name { get; }
    }
}
