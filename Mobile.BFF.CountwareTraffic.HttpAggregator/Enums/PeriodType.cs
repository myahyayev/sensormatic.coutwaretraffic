﻿namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public enum PeriodType : byte
    {
        Today = 1,
        Yesterday = 2,
        ThisWeek = 3,
        ThisMonth = 4
    }
}
