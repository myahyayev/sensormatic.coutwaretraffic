﻿namespace CountwareTraffic.Services.Reporting.Core
{
    public class TemplateTypeEntityDto
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
    }
}
