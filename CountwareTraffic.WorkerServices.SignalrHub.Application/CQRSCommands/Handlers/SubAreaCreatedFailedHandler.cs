﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class SubAreaCreatedFailedHandler : ICommandHandler<SubAreaCreatedFailed>
    {
        private readonly IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> _hubContext;
        public SubAreaCreatedFailedHandler(IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> hubContext)
            => _hubContext = hubContext;


        public async Task HandleAsync(SubAreaCreatedFailed command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .SubAreaCreatedFailedReceived(command);
    }
}
