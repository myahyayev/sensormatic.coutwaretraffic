﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AddCompanyApiRequest : SensormaticRequestValidate
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string EmailAddress { get; set; }
        public string GsmNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string GsmDialCode { get; set; }
        public string PhoneDialCode { get; set; }
        public string PersonName { get; set; }
        public string GsmDialCodeSecondary { get; set; }
        public string GsmNumberSecondary { get; set; }
        public string PhoneDialCodeSecondary { get; set; }
        public string PhoneNumberSecondary { get; set; }
        public string EmailAddressSecondary { get; set; }
        public string PersonNameSecondary { get; set; }
        public string GsmCountryCode { get; set; }
        public string PhoneCountryCode { get; set; }
        public string GsmCountryCodeSecondary { get; set; }
        public string PhoneCountryCodeSecondary { get; set; }

        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1106 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1107 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1108 Description invalid format", nameof(Description)));

            if (!City.IsNullOrWhiteSpace() && City.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1109 City invalid format", nameof(City)));

            if (!Street.IsNullOrWhiteSpace() && Street.Length > 250)
                ValidateResults.Add(new ErrorResult($"#E1110 Street invalid format", nameof(Street)));

            if (!State.IsNullOrWhiteSpace() && State.Length > 250)
                ValidateResults.Add(new ErrorResult($"#E1111 State invalid format", nameof(State)));

            if (!Country.IsNullOrWhiteSpace() && Country.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1112 Country invalid format", nameof(Country)));

            if (!ZipCode.IsNullOrWhiteSpace() && ZipCode.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1113 ZipCode invalid format", nameof(ZipCode)));


            if (!EmailAddress.IsNullOrWhiteSpace() && (!EmailValidator.EmailIsValid(EmailAddress) || EmailAddress.Length > 120))
                ValidateResults.Add(new ErrorResult($"#E1114 Email address invalid format", nameof(EmailAddress)));

            if (!PhoneNumber.IsNullOrWhiteSpace() && PhoneNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1115 PhoneNumber invalid format", nameof(PhoneNumber)));

            if (!GsmNumber.IsNullOrWhiteSpace() && GsmNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1116 GsmNumber invalid format", nameof(GsmNumber)));

            if (!GsmDialCode.IsNullOrWhiteSpace() && GsmDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1117 GsmDialCode invalid format", nameof(GsmDialCode)));

            if (!PhoneDialCode.IsNullOrWhiteSpace() && PhoneDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1118 PhoneDialCode invalid format", nameof(PhoneDialCode)));

            if (!PersonName.IsNullOrWhiteSpace() && PersonName.Length > 120)
                ValidateResults.Add(new ErrorResult($"#E1119 PersonName invalid format", nameof(PersonName)));

            if (!GsmDialCodeSecondary.IsNullOrWhiteSpace() && GsmDialCodeSecondary.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1466 GsmDialCodeSecondary invalid format", nameof(GsmDialCodeSecondary)));

            if (!GsmNumberSecondary.IsNullOrWhiteSpace() && GsmNumberSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1120 GsmNumberSecondary invalid format", nameof(GsmNumberSecondary)));

            if (!PhoneDialCodeSecondary.IsNullOrWhiteSpace() && PhoneDialCodeSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1121 PhoneDialCodeSecondary invalid format", nameof(PhoneDialCodeSecondary)));

            if (!PhoneNumberSecondary.IsNullOrWhiteSpace() && PhoneNumberSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1122 PhoneNumberSecondary invalid format", nameof(PhoneNumberSecondary)));

            if (!EmailAddressSecondary.IsNullOrWhiteSpace() && (!EmailValidator.EmailIsValid(EmailAddressSecondary) || EmailAddressSecondary.Length > 120))
                ValidateResults.Add(new ErrorResult($"#E1123 Email address secondary invalid format", nameof(EmailAddressSecondary)));

            if (!PersonNameSecondary.IsNullOrWhiteSpace() && PersonNameSecondary.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1124 PersonNameSecondary invalid format", nameof(PersonNameSecondary)));

            if (!GsmCountryCode.IsNullOrWhiteSpace() && GsmCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1125 GsmCountryCode invalid format", nameof(GsmCountryCode)));

            if (!PhoneCountryCode.IsNullOrWhiteSpace() && PhoneCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1126 PhoneCountryCode invalid format", nameof(PhoneCountryCode)));

            if (!GsmCountryCodeSecondary.IsNullOrWhiteSpace() && GsmCountryCodeSecondary.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1127 GsmCountryCodeSecondary invalid format", nameof(GsmCountryCodeSecondary)));

            if (!PhoneCountryCodeSecondary.IsNullOrWhiteSpace() && PhoneCountryCodeSecondary.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1128 PhoneCountryCodeSecondary invalid format", nameof(PhoneCountryCodeSecondary)));
        }
    }
}
