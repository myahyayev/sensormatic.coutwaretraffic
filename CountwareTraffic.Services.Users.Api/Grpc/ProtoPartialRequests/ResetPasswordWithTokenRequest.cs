﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace CountwareTraffic.Services.Users.Grpc
{
    [ServiceLog]
    public sealed partial class ResetPasswordWithTokenRequest : SensormaticRequestValidate
    {
        public override void Validate()
        {
            if (Token.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(Token)));

            if (UserName.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(UserName)));

            if (Password.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(Password)));

            if (ConfirmPassword.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(ConfirmPassword)));

            if ((!Password.IsNullOrWhiteSpace() && !Password.IsValidPassword()) || (!ConfirmPassword.IsNullOrWhiteSpace() && !ConfirmPassword.IsValidPassword()))
            {
                ValidateResults.Add(new ErrorResult($"Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)"));

                return;
            }

            if (!Password.IsNullOrWhiteSpace() && !ConfirmPassword.IsNullOrWhiteSpace() && !object.Equals(Password, ConfirmPassword))
                ValidateResults.Add(new ErrorResult($"The Password didn't match."));
        }
    }
}
