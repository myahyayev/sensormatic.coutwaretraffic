﻿using Convey.CQRS.Commands;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Cache;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class RemoveCacheHandler : ICommandHandler<RemoveCache>
    {
        private readonly ILogger<RemoveCacheHandler> _logger;
        private readonly ICache _redisCacheService;

        public RemoveCacheHandler(ILogger<RemoveCacheHandler> logger, ICache redisCacheService)
        {
            _logger = logger;
            _redisCacheService = redisCacheService;
        }
        public async Task HandleAsync(RemoveCache request)
        {
            var redisKey = string.Format(CacheKeys.DeviceRealTimeLatestEvent, request.DeviceId);

            var result = await _redisCacheService.DeleteAsync(redisKey);

            if (result)
            {
                _logger.LogInformation($"{request.DeviceId} cache data has been deleted!");
            }
        }
    }
}
