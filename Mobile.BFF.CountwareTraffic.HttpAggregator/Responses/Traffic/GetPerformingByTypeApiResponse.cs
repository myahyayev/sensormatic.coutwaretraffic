﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetPerformingByTypeApiResponse
    {
        public int Inwards { get; set; }
        public string Name { get; set; }
        public Guid Id { get; set; }
    }
}
