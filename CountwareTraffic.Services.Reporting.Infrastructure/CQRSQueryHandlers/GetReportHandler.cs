﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class GetReportHandler : IQueryHandler<GetReport, ReportDetailDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetReportHandler(IUnitOfWork unitOfWork, IIdentityService identityService) => _unitOfWork = unitOfWork;

        public async Task<ReportDetailDto> HandleAsync(GetReport query)
        {
            var reportRepository = _unitOfWork.GetRepository<IReportRepository>();

            var report = await reportRepository.GetReportByIdAsync(query.ReportId);

            if (report == null)
                throw new ReportNotFoundException(query.ReportId);

            return new ReportDetailDto
            {
                Id = report.Id,
                Name = report.Name,
                Description = report.Description,
                FormatId = report.Format.Id,
                FormatName = report.Format.Name,
                IsActive = report.IsActive,
                Locations = report.Location.Select(u => u.Name).ToList(),
                StatusId = report.Status.Id,
                StatusName = report.Status.Name,
                TemplateName = report.ReportTemplate.Name,
                TenantId = report.TenantId,
                TemplateId = report.ReportTemplate.Id,
                TemplateTypeId = report.TemplateType.Id,
                TemplateTypeName = report.TemplateType.Name,
                AtSetTime = report.ReportSchedule.AtSetTime,
                AtSetTimeZone = report.ReportSchedule.AtSetTimeZone,
                EmailBody = report.ReportEmail.Body,
                EmailSubject = report.ReportEmail.Subject,
                EmailTo = report.ReportEmail.To,
                LanguageId = report.ReportLanguage.Id,
                LanguageName = report.ReportLanguage.Name,
                NotifyWhenComplete = report.NotifyWhenComplete,
                RepeatOn = report.ReportSchedule.RepeatOn,
                ScheduleDeliveryTimeId = report.ReportSchedule.DeliveryTime.Id,
                ScheduleDeliveryTimeName = report.ReportSchedule.DeliveryTime.Name,
                ScheduleFrequencyId = report.ReportSchedule.Frequency.Id,
                ScheduleFrequencyName = report.ReportSchedule.Frequency.Name,
                ScheduleId = report.ReportSchedule.Id,
                StartsOn = report.ReportSchedule.StartsOn,
                EndsOn = report.ReportSchedule.EndsOn,
                DateRangeStartDate = report.DateRangeStartDate,
                DateRangeEndDate = report.DateRangeEndDate,
                ReportFiles = report.ReportFile == null ? null : new()
                {
                    new ReportFileDto()
                    {
                        CreatedDate = report.ReportFile.CreatedDate,
                        FilePath = report.ReportFile.FilePath,
                        Id = report.ReportFile.Id,
                    }
                },
            };
        }
    }
}
