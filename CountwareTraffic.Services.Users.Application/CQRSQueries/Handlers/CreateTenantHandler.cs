﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreateTenantHandler : IQueryHandler<CreateTenant, IdentityCreateTenantResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public CreateTenantHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityCreateTenantResponse> HandleAsync(CreateTenant query)
        {
            return await _identityServerService.CreateTenantAsync(new IdentityCreateTenantRequest
            {
                Name = query.Name,
                Description =query.Description,
                ErpCode =query.ErpCode,
                ErpGroupName = query.ErpGroupName,
                ErpStartDate =query.ErpStartDate,
                ErpEndDate = query.ErpEndDate,
                TenantGroupId = query.TenantGroupId
            });
        }
    }
}
