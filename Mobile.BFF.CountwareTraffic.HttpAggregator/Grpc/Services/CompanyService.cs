﻿using CountwareTraffic.Services.Companies.Grpc;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class CompanyService : IScopedSelfDependency
    {
        private readonly Company.CompanyClient _companyClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;
        public CompanyService(Company.CompanyClient companyClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _companyClient = companyClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetCompanyDetails> GetCompanyByIdAsync(Guid companyId)
        {
            GetCompanyRequest grpcRequest = new() { CompanyId = companyId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_companyClient.GetCompanyByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetCompanyDetails
            {
                Id = new Guid(grpcResponse.CompanyDetail.Id),
                Name = grpcResponse.CompanyDetail.Name,
                Description = grpcResponse.CompanyDetail.Description,
                City = grpcResponse.CompanyDetail.City,
                Country = grpcResponse.CompanyDetail.Country,
                State = grpcResponse.CompanyDetail.State,
                AuditCreateBy = new Guid(grpcResponse.CompanyDetail.Audit.AuditCreateBy),
                AuditCreateDate = grpcResponse.CompanyDetail.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(grpcResponse.CompanyDetail.Audit.AuditModifiedBy),
                AuditModifiedDate = grpcResponse.CompanyDetail.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                PhoneNumberSecondary = grpcResponse.CompanyDetail.PhoneNumberSecondary,
                EmailAddress = grpcResponse.CompanyDetail.EmailAddress,
                GsmNumber = grpcResponse.CompanyDetail.GsmNumber,
                PhoneNumber = grpcResponse.CompanyDetail.PhoneNumber,
                EmailAddressSecondary = grpcResponse.CompanyDetail.EmailAddressSecondary,
                PhoneDialCodeSecondary = grpcResponse.CompanyDetail.PhoneDialCodeSecondary,
                PhoneDialCode = grpcResponse.CompanyDetail.PhoneDialCode,
                PersonNameSecondary = grpcResponse.CompanyDetail.PersonNameSecondary,
                PersonName = grpcResponse.CompanyDetail.PersonName,
                GsmDialCodeSecondary = grpcResponse.CompanyDetail.GsmDialCodeSecondary,
                GsmNumberSecondary = grpcResponse.CompanyDetail.GsmNumberSecondary,
                GsmDialCode = grpcResponse.CompanyDetail.GsmDialCode,
                GsmCountryCodeSecondary = grpcResponse.CompanyDetail.GsmCountryCodeSecondary,
                GsmCountryCode = grpcResponse.CompanyDetail.GsmCountryCode,
                PhoneCountryCodeSecondary = grpcResponse.CompanyDetail.PhoneCountryCodeSecondary,
                PhoneCountryCode = grpcResponse.CompanyDetail.PhoneCountryCode,
                Latitude = grpcResponse.CompanyDetail.Latitude,
                Longitude = grpcResponse.CompanyDetail.Longitude,
                Street = grpcResponse.CompanyDetail.Street,
                ZipCode = grpcResponse.CompanyDetail.ZipCode
            };
        }

        public async Task<ApiPagedResponse<GetCompanyDetails>> GetCompaniesAsync(DataSourceApiRequest paging)
        {
            GetCompaniesRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Companies.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Companies.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }


            var grpcResponse = await _asyncUnaryCallHandler
            .CallMethodAsync(_companyClient.GetCompaniesAsync, grpcRequest, hasClientSideLog: false);

            var companies = grpcResponse.CompanyDetails.Select(company => new GetCompanyDetails
            {
                Id = new Guid(company.Id),
                Name = company.Name,
                Description = company.Description,
                AuditCreateBy = new Guid(company.Audit.AuditCreateBy),
                AuditCreateDate = company.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(company.Audit.AuditModifiedBy),
                AuditModifiedDate = company.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                PhoneNumberSecondary = company.PhoneNumberSecondary,
                EmailAddress = company.EmailAddress,
                GsmNumber = company.GsmNumber,
                PhoneNumber = company.PhoneNumber,
                EmailAddressSecondary = company.EmailAddressSecondary,
                PhoneDialCodeSecondary = company.PhoneDialCodeSecondary,
                PhoneDialCode = company.PhoneDialCode,
                PersonNameSecondary = company.PersonNameSecondary,
                PersonName = company.PersonName,
                GsmDialCodeSecondary = company.GsmDialCodeSecondary,
                GsmNumberSecondary = company.GsmNumberSecondary,
                GsmDialCode = company.GsmDialCode,
                GsmCountryCodeSecondary = company.GsmCountryCodeSecondary,
                GsmCountryCode = company.GsmCountryCode,
                PhoneCountryCodeSecondary = company.PhoneCountryCodeSecondary,
                PhoneCountryCode = company.PhoneCountryCode,
                Latitude = company.Latitude,
                Longitude = company.Longitude,
                Street = company.Street,
                City = company.City,
                ZipCode = company.ZipCode,
                State = company.State,
                Country = company.Country
            });

            return new ApiPagedResponse<GetCompanyDetails>(companies, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task AddCompanyAsync(AddCompanyApiRequest request)
        {
            CreateCompanyRequest grpcRequest = new()
            {
                City = request.City,
                Country = request.Country,
                Description = request.Description,
                Name = request.Name,
                EmailAddress = request.EmailAddress,
                EmailAddressSecondary = request.EmailAddressSecondary,
                GsmDialCode = request.GsmDialCode,
                GsmNumberSecondary = request.GsmNumberSecondary,
                GsmDialCodeSecondary = request.GsmDialCodeSecondary,
                PersonName = request.PersonName,
                PersonNameSecondary = request.PersonNameSecondary,
                PhoneDialCode = request.PhoneDialCode,
                PhoneDialCodeSecondary = request.PhoneDialCodeSecondary,
                PhoneNumberSecondary = request.PhoneNumberSecondary,
                GsmNumber = request.GsmNumber,
                PhoneNumber = request.PhoneNumber,
                GsmCountryCodeSecondary = request.GsmCountryCodeSecondary,
                GsmCountryCode = request.GsmCountryCode,
                PhoneCountryCodeSecondary = request.PhoneCountryCodeSecondary,
                PhoneCountryCode = request.PhoneCountryCode,
                Latitude = request.Latitude,
                Longitude = request.Longitude,
                State = request.State,
                Street = request.Street,
                ZipCode = request.ZipCode
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_companyClient.AddCompanyAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task ChangeCompanyAsync(Guid companyId, ChangeCompanyApiRequest request)
        {
            UpdateCompanyRequest grpcRequest = new()
            {
                City = request.City,
                CompanyId = companyId.ToString(),
                Country = request.Country,
                State = request.State,
                ZipCode = request.ZipCode,
                Description = request.Description,
                Name = request.Name,
                Longitude = request.Longitude,
                Latitude = request.Latitude,
                Street = request.Street,
                EmailAddress = request.EmailAddress,
                EmailAddressSecondary = request.EmailAddressSecondary,
                GsmDialCode = request.GsmDialCode,
                GsmNumberSecondary = request.GsmNumberSecondary,
                GsmDialCodeSecondary = request.GsmDialCodeSecondary,
                PersonName = request.PersonName,
                PersonNameSecondary = request.PersonNameSecondary,
                PhoneDialCode = request.PhoneDialCode,
                PhoneDialCodeSecondary = request.PhoneDialCodeSecondary,
                PhoneNumberSecondary = request.PhoneNumberSecondary,
                GsmNumber = request.GsmNumber,
                PhoneNumber = request.PhoneNumber,
                GsmCountryCodeSecondary = request.GsmCountryCodeSecondary,
                GsmCountryCode = request.GsmCountryCode,
                PhoneCountryCodeSecondary = request.PhoneCountryCodeSecondary,
                PhoneCountryCode = request.PhoneCountryCode,
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_companyClient.ChangeCompanyAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task DeleteCompanyAsync(Guid companyId)
        {
            DeleteCompanyRequest grpcRequest = new() { CompanyId = companyId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_companyClient.DeleteCompanyAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task<IEnumerable<Hierarchy>> GetHierarchyAsync(Guid companyId)
        {
            GetHierarchyRequest grpcRequest = new() { Id = companyId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_companyClient.GetHierarchyAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.Hierarchies.Select(hierarchy => new Hierarchy
            {
                Id = new Guid(hierarchy.Id),
                Name = hierarchy.Name,
                HierarchyLevel = hierarchy.HierarchyLevel switch
                {
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Default => throw new NotImplementedException(),
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Tenant => HierarchyLevel.Tenant,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Company => HierarchyLevel.Company,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Country => HierarchyLevel.Country,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Region => HierarchyLevel.Region,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Area => HierarchyLevel.Area,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.SubArea => HierarchyLevel.SubArea,
                    global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Device => HierarchyLevel.Device,
                    _ => throw new NotImplementedException(),
                }
            });
        }

        public async Task<List<Parent>> GetDeviceFilterAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_companyClient.GetCompaniesKeyValueAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.KeyValues.Select(u => new Parent
            {
                Children = null,
                Expanded = true,
                Label = u.Name,
                Data = new Data

                {
                    HierarchyLevel = HierarchyLevel.Company,
                    Id = new Guid(u.Id)
                }
            }).ToList();
        }

        public async Task<GetCompaniesByTenantIdResponse> GetCompaniesByTenantIdAsync(Guid tenantId)
        {
            var grpcRequest = new GetCompaniesByTenantIdRequest
            {
                TenantId = tenantId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler
                                        .CallMethodAsync(_companyClient.GetCompaniesByTenantIdAsync, 
                                                         grpcRequest, 
                                                         hasClientSideLog: false);

            var response = new GetCompaniesByTenantIdResponse();

            foreach (var item in grpcResponse.Data)
            {
                response.Data.Add(new GetCompaniesByTenantIdItem
                {
                    Id = item.Id,
                    Name = item.Name,
                });
            }

            return response;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
