﻿using System.Threading.Tasks;

namespace Sensormatic.Tool.Email
{
    public interface IEmailService
    {
        Task<EmailResponse> SendAsync(EmailRequest request);
    }
}
