﻿using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer
{
    public class CreateAdminRoleApiRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Permissions { get; set; }
    }
}
