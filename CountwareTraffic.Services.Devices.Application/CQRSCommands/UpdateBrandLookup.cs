﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Devices.Application
{
    [Contract]
    public class UpdateBrandLookup : ICommand
    {
        public Guid BrandId { get; set; }
        public string Name { get; set; }
    }
}
