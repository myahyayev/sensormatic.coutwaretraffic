﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public interface IUnknownDeviceNotificationsHub
    {
        Task UnknownDeviceCreatedSuccessfullyReceived(UnknownDeviceCreatedSuccessfully command);
    }

    //[Authorize]
    public class UnknownDeviceNotificationsHub : Hub<IUnknownDeviceNotificationsHub>
    {
        public async Task UnknownDeviceCreatedSuccessfullyReceived(UnknownDeviceCreatedSuccessfully command)
            => await Clients.All.UnknownDeviceCreatedSuccessfullyReceived(command);
    }
}
