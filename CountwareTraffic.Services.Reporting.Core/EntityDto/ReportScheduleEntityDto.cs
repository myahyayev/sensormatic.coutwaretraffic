﻿using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportScheduleEntityDto
    {
        public Guid Id { get; set; }
        public ScheduleFrequency Frequency { get; set; }
        public ScheduleDeliveryTime DeliveryTime { get; set; }
        public DateTime? StartsOn { get; set; }
        public DateTime? EndsOn { get; set; }
        public string AtSetTime { get; set; }
        public string AtSetTimeZone { get; set; }
        public string RepeatOn { get; set; }
    }
}
