﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class BulkEventsController : BaseController
    {
        private readonly TrafficService _trafficService;

        public BulkEventsController(IServiceProvider provider, ILogger<TrafficController> logger, IHttpContextAccessor contextAccessor, EventService eventService, 
            TrafficService trafficService,UserService userService
            )
            : base(provider, logger, contextAccessor)
        {
            _trafficService = trafficService;
        }

        [HttpPost("by-time-period")]
        [Permissions(Permissions = new[] { Permissions.DashboardTrafficView })]
        public async Task<ApiResponse<List<TimePeriodApiResponse>>> GetEventsByTimePeriodAsync(TimePeriodApiRequest request)
        {
            return Response(await _trafficService.GetEventsByTimePeriodAsync(request), new ResultMessage("#S1143"));
        }
    }
}
