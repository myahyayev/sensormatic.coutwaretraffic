﻿using System;

namespace Sensormatic.Tool.Ioc
{
    public interface ITransientSelfDependency : IDisposable { }
}
