﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteTokenHandler : IQueryHandler<DeleteToken, IdentityDeleteTokenResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public DeleteTokenHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityDeleteTokenResponse> HandleAsync(DeleteToken query)
        {
            return await _identityServerService.DeleteTokenAsync(query.Id);
        }
    }
}
