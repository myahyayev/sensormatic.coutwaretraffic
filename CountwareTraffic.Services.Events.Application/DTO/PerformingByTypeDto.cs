﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class PerformingByTypeDto
    {
        public int Inwards { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
