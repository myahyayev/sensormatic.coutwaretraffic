﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Ioc;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportDbContext : DbContext, IScopedSelfDependency
    {
        private readonly ReportDbProvider _provider;
        private readonly ICurrentTenant _currentTenant;

        public ReportDbContext(DbContextOptions options)
          : base(options) { }

        public ReportDbContext(DbContextOptions options, ReportDbProvider provider, ICurrentTenant currentTenant)
            : base(options)
        {
            _provider = provider;
            _currentTenant = currentTenant;
        }

        #region DBSETs
        public virtual DbSet<ReportTemplate> ReportTemplates { get; set; }
        public virtual DbSet<TemplateType> TemplateTypes { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<ReportFormat> ReportFormats { get; set; }
        public virtual DbSet<ReportLanguage> ReportLanguages { get; set; }
        public virtual DbSet<ReportSchedule> ReportSchedules { get; set; }
        public virtual DbSet<ScheduleFrequency> ScheduleFrequencies { get; set; }
        public virtual DbSet<ScheduleDeliveryTime> ScheduleDeliveryTimes { get; set; }
        public virtual DbSet<ScheduleWeeklyRepeatOn> ScheduleWeeklyRepeatOns { get; set; }
        public virtual DbSet<ReportStatus> ReportStatuses { get; set; }
        public virtual DbSet<ReportFile> ReportFiles { get; set; }
        public DbSet<OutboxMessage> OutboxMessages { get; set; }
        #endregion DBSETs

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies(false);

            if (_provider != null)
                optionsBuilder.AddInterceptors(new SaveChangesInterceptor(_provider));
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new ReportEntityTypeConfiguration());
            builder.ApplyConfiguration(new ReportFormatEntityTypeConfiguration());
            builder.ApplyConfiguration(new ReportLanguageEntityTypeConfiguration());
            builder.ApplyConfiguration(new ReportScheduleEntityTypeConfiguration());
            builder.ApplyConfiguration(new ReportTemplateEntityTypeConfiguration());
            builder.ApplyConfiguration(new ReportTemplateTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new ScheduleDeliveryTimeEntityTypeConfiguration());
            builder.ApplyConfiguration(new ScheduleFrequencyEntityTypeConfiguration());
            builder.ApplyConfiguration(new ScheduleWeeklyRepeatOnEntityTypeConfiguration());
            builder.ApplyConfiguration(new TemplateTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new OutboxMessageEntityTypeConfiguration());
            builder.ApplyConfiguration(new ReportStatusEntityTypeConfiguration());
            builder.ApplyConfiguration(new ReportFileEntityTypeConfiguration());
        }
    }
}
