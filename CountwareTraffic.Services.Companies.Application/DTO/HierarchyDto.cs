﻿using CountwareTraffic.Services.Companies.Core;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class HierarchyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }
}
