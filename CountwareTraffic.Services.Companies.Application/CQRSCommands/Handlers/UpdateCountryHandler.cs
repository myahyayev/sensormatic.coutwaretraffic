﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class UpdateCountryHandler : ICommandHandler<UpdateCountry>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public UpdateCountryHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(UpdateCountry command)
        {
            var countryRepository = _unitOfWork.GetRepository<ICountryRepository>();

            var country = await countryRepository.GetAsync(command.CountryId);

            if (country is null)
                throw new CountryNotFoundException(command.CountryId);


            var updateControl = await  countryRepository.GetAsync(command.Name, command.CompanyId);
            if (updateControl != null &&  updateControl.Id != command.CountryId)
                throw new CountryAlreadyExistsException(command.Name);

            country.CompleteChange(command.CountryLookupId, command.Iso, command.Iso3, command.IsoNumeric, command.Name, command.Capital, command.ContinentCode, command.CurrencyCode);

            var company = await _unitOfWork.GetRepository<ICompanyRepository>().GetByIdAsync(command.CompanyId);

            country.WhenChanged(_currentTenant.Id, company.Id, country.Id, country.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
