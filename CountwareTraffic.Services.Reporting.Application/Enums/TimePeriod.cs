﻿namespace CountwareTraffic.Services.Reporting.Application
{
    public enum TimePeriod
    {
        Hourly = 1,
        Daily = 2,
        Monthly = 3,
        Weekly = 4
    }
}
