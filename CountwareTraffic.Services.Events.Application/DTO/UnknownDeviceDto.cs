﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class UnknownDeviceDto
    {
        public Guid Id { get; set; }
    }
}
