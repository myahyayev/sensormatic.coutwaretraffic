﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;

namespace Sensormatic.Tool.Efcore
{
    internal class MultiTenantInterceptor : InterceptorGenerator<IMultiTenant>
    {
        private readonly Guid? _tenantId;
        public MultiTenantInterceptor(Guid? tenantId) => _tenantId = tenantId;
        public override void OnAfterError(string execptionMessage) { }
        public override void OnAfterInsert() { }

        public override void OnBeforeDelete(IMultiTenant item, EntityEntry entityEntry, DbContext dbContext) { }

        public override void OnBeforeInsert(IMultiTenant item, EntityEntry entityEntry, DbContext dbContext)
            => item.TenantId = _tenantId != null ? _tenantId.Value : Guid.Empty;

        public override void OnBeforeUpdate(IMultiTenant item, EntityEntry entityEntry, DbContext dbContext) { }

    }
}
