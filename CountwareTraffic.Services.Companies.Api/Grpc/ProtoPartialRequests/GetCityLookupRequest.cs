﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    public sealed partial class GetCityLookupRequest : SensormaticRequestValidate
    {
        internal Guid _CityId
        {
            get
            {
                if (Guid.TryParse(id_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.id_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_CityId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1314 CityId cannot be null", nameof(_CityId)));
        }
    }
}
