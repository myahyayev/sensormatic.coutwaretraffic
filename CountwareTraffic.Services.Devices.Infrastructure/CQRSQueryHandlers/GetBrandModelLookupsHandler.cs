﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetBrandModelLookupsHandler : IQueryHandler<GetBrandModelLookups, List<BrandModelLookupDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetBrandModelLookupsHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<List<BrandModelLookupDetailsDto>> HandleAsync(GetBrandModelLookups query)
        {
            var brandModelLookups = await _unitOfWork.GetRepository<IBrandModelLookupRepository>().GetAllAsync(query.BrandId);

            if (brandModelLookups == null) return null;

            return brandModelLookups.Select(districtLookup => new BrandModelLookupDetailsDto
            {
                Id = districtLookup.Id,
                Name = districtLookup.Name,
                BrandId = districtLookup.BrandId
            }).ToList();
        }
    }
}
