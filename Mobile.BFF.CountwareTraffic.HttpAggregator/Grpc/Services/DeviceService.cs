﻿using CountwareTraffic.Services.Devices.Grpc;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class DeviceService : IScopedSelfDependency
    {
        private readonly Device.DeviceClient _deviceClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;
        public DeviceService(Device.DeviceClient deviceClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _deviceClient = deviceClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetDeviceDetails> GetDeviceByIdAsync(Guid deviceId)
        {
            GetDeviceRequest grpcRequest = new() { DeviceId = deviceId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDeviceByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetDeviceDetails
            {
                Id = new Guid(grpcResponse.DeviceDetail.Id),
                Name = grpcResponse.DeviceDetail.Name,
                Description = grpcResponse.DeviceDetail.Description,
                AuditCreateBy = new Guid(grpcResponse.DeviceDetail.Audit.AuditCreateBy),
                AuditCreateDate = grpcResponse.DeviceDetail.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(grpcResponse.DeviceDetail.Audit.AuditModifiedBy),
                AuditModifiedDate = grpcResponse.DeviceDetail.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                DeviceStatusId = grpcResponse.DeviceDetail.DeviceStatusId,
                DeviceStatusName = grpcResponse.DeviceDetail.DeviceStatusName,
                DeviceTypeId = grpcResponse.DeviceDetail.DeviceTypeId,
                DeviceTypeName = grpcResponse.DeviceDetail.DeviceTypeName,
                Identity = grpcResponse.DeviceDetail.Identity,
                IpAddress = grpcResponse.DeviceDetail.IpAddress,
                MacAddress = grpcResponse.DeviceDetail.MacAddress,
                ModelId = new Guid(grpcResponse.DeviceDetail.ModelId),
                BrandId = new Guid(grpcResponse.DeviceDetail.BrandId),
                BrandName = grpcResponse.DeviceDetail.BrandName,
                Note = grpcResponse.DeviceDetail.Note,
                ControlFrequency = grpcResponse.DeviceDetail.ControlFrequency,
                Firmware = grpcResponse.DeviceDetail.Firmware,
                IsActive = grpcResponse.DeviceDetail.IsActive,
                ModelName = grpcResponse.DeviceDetail.ModelName,
                Password = grpcResponse.DeviceDetail.Password,
                Port = grpcResponse.DeviceDetail.Port,
                UniqueId = grpcResponse.DeviceDetail.UniqueId,
                SubAreaId = new Guid(grpcResponse.DeviceDetail.SubAreaId)
            };
        }

        public async Task<ApiPagedResponse<GetDeviceDetails>> GetDevicesAsync(Guid subAreaId, DataSourceApiRequest paging)
        {
            GetDevicesRequest grpcRequest = new()
            {
                SubAreaId = subAreaId.ToString(),
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Devices.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Devices.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Devices.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Devices.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDevicesAsync, grpcRequest, hasClientSideLog: false);

            var devices = grpcResponse.DeviceDetails.Select(device => new GetDeviceDetails
            {
                Id = new Guid(device.Id),
                Name = device.Name,
                Description = device.Description,
                AuditCreateBy = new Guid(device.Audit.AuditCreateBy),
                AuditCreateDate = device.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(device.Audit.AuditModifiedBy),
                AuditModifiedDate = device.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                DeviceStatusId = device.DeviceStatusId,
                DeviceStatusName = device.DeviceStatusName,
                DeviceTypeId = device.DeviceTypeId,
                DeviceTypeName = device.DeviceTypeName,
                Identity = device.Identity,
                IpAddress = device.IpAddress,
                MacAddress = device.MacAddress,
                ModelId = new Guid(device.ModelId),
                BrandId = new Guid(device.BrandId),
                BrandName = device.BrandName,
                Note = device.Note,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.Firmware,
                IsActive = device.IsActive,
                ModelName = device.ModelName,
                Password = device.Password,
                Port = device.Port,
                UniqueId = device.UniqueId,
                SubAreaId = new Guid(device.SubAreaId)
            });

            return new ApiPagedResponse<GetDeviceDetails>(devices, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task<ApiPagedResponse<GetDeviceDetails>> GetDevicesAsync(DataSourceApiRequest paging)
        {
            GetDevicesWithoutParentRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Devices.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Devices.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Devices.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Devices.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDevicesWithoutParentAsync, grpcRequest, hasClientSideLog: false);

            var devices = grpcResponse.DeviceDetails.Select(device => new GetDeviceDetails
            {
                Id = new Guid(device.Id),
                Name = device.Name,
                Description = device.Description,
                AuditCreateBy = new Guid(device.Audit.AuditCreateBy),
                AuditCreateDate = device.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(device.Audit.AuditModifiedBy),
                AuditModifiedDate = device.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                DeviceStatusId = device.DeviceStatusId,
                DeviceStatusName = device.DeviceStatusName,
                DeviceTypeId = device.DeviceTypeId,
                DeviceTypeName = device.DeviceTypeName,
                Identity = device.Identity,
                IpAddress = device.IpAddress,
                MacAddress = device.MacAddress,
                ModelId = new Guid(device.ModelId),
                BrandId = new Guid(device.BrandId),
                BrandName = device.BrandName,
                Note = device.Note,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.Firmware,
                IsActive = device.IsActive,
                ModelName = device.ModelName,
                Password = device.Password,
                Port = device.Port,
                UniqueId = device.UniqueId,
                SubAreaId = new Guid(device.SubAreaId)
            });

            return new ApiPagedResponse<GetDeviceDetails>(devices, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task<ApiPagedResponse<GetDeviceDetails>> GetDevicesAsync(GetDeviceFilterApiRequest request)
        {
            GetDeviceFilterRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = request.DataSourceApiRequest.Paging.Limit,
                        Page = request.DataSourceApiRequest.Paging.Page
                    }
                }
            };

            request.DataSourceApiRequest.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Devices.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Devices.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Devices.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));


            foreach (var filter in request.DataSourceApiRequest.Filters)
            {
                global::CountwareTraffic.Services.Devices.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            if (request.HierarchyTreeSelectFilters != null && request.HierarchyTreeSelectFilters.Count > 0)
            {
                //tenant is default no need to add it to filter
                foreach (var hierarchyTreeSelectFilter in request
                                                            .HierarchyTreeSelectFilters
                                                            .Where(hf=>hf.HierarchyLevel!= HierarchyLevel.Tenant))
                {
                    global::CountwareTraffic.Services.Devices.Grpc.HierarchyTreeSelectFilterRequest hierarchyTreeSelectFilterRequest = new();

                    hierarchyTreeSelectFilterRequest.NodeFilter = new()
                    {


                        HierarchyLevel = hierarchyTreeSelectFilter.HierarchyLevel switch
                        {   
                            HierarchyLevel.Tenant => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Tenant,
                            HierarchyLevel.Company => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Company,
                            HierarchyLevel.Country => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Country,
                            HierarchyLevel.Region => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Region,
                            HierarchyLevel.Area => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Area,
                            HierarchyLevel.SubArea => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.SubArea,
                            HierarchyLevel.Device => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Device,
                            _ => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Tenant
                        },
                        Id = hierarchyTreeSelectFilter.Id.ToString()
                    };

                    grpcRequest.HierarchyTreeSelectFilters.Add(hierarchyTreeSelectFilterRequest);
                }
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDevicesTreeSelectFilterAsync, grpcRequest, hasClientSideLog: false);

            var devices = grpcResponse.DeviceDetails.Select(device => new GetDeviceDetails
            {
                Id = new Guid(device.Id),
                Name = device.Name,
                Description = device.Description,
                AuditCreateBy = new Guid(device.Audit.AuditCreateBy),
                AuditCreateDate = device.Audit.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(device.Audit.AuditModifiedBy),
                AuditModifiedDate = device.Audit.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                DeviceStatusId = device.DeviceStatusId,
                DeviceStatusName = device.DeviceStatusName,
                DeviceTypeId = device.DeviceTypeId,
                DeviceTypeName = device.DeviceTypeName,
                Identity = device.Identity,
                IpAddress = device.IpAddress,
                MacAddress = device.MacAddress,
                ModelId = new Guid(device.ModelId),
                BrandId = new Guid(device.BrandId),
                BrandName = device.BrandName,
                Note = device.Note,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.Firmware,
                IsActive = device.IsActive,
                ModelName = device.ModelName,
                Password = device.Password,
                Port = device.Port,
                UniqueId = device.UniqueId,
                SubAreaId = new Guid(device.SubAreaId)
            });

            return new ApiPagedResponse<GetDeviceDetails>(devices, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task AddDeviceAsync(Guid subAreaId, AddDeviceApiRequest request)
        {
            CreateDeviceRequest grpcRequest = new()
            {
                SubAreaId = subAreaId.ToString(),
                Name = request.Name,
                UniqueId = request.UniqueId,
                Description = request.Description,
                DeviceTypeId = request.DeviceTypeId,
                Identity = request.Identity,
                IpAddress = request.IpAddress,
                MacAddress = request.MacAddress,
                BrandId = request.BrandId.ToString(),
                ControlFrequency = request.ControlFrequency,
                Firmware = request.Firmware,
                ModelId = request.ModelId.ToString(),
                Note = request.Note,
                Password = request.Password,
                Port = request.Port,
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.AddDeviceAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task ChangeDeviceAsync(Guid deviceId, ChangeDeviceApiRequest request)
        {
            UpdateDeviceRequest grpcRequest = new()
            {
                DeviceId = deviceId.ToString(),
                DeviceTypeId = request.DeviceTypeId,
                Port = request.Port,
                Password = request.Password,
                BrandId = request.BrandId.ToString(),
                ControlFrequency = request.ControlFrequency,
                Firmware = request.Firmware,
                ModelId = request.ModelId.ToString(),
                Note = request.Note,
                IsActive = request.IsActive,
                MacAddress = request.MacAddress,
                Description = request.Description,
                Identity = request.Identity,
                IpAddress = request.IpAddress,
                Name = request.Name,
                UniqueId = request.UniqueId
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.ChangeDeviceAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task DeleteDeviceAsync(Guid deviceId)
        {
            DeleteDeviceRequest grpcRequest = new() { DeviceId = deviceId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.DeleteDeviceAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task<IEnumerable<GetDeviceType>> GetDeviceTypesAsync()
        {
            GetDeviceTypesRequest grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
              .CallMethodAsync(_deviceClient.GetDeviceTypesAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.DeviceTypes.Select(type => new GetDeviceType
            {
                Id = type.Id,
                Name = type.Name
            });
        }

        public async Task<IEnumerable<GetDeviceStatus>> GetDeviceStatusesAsync()
        {
            GetDeviceStatusesRequest grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
              .CallMethodAsync(_deviceClient.GetDeviceStatusesAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.DeviceStatuses.Select(type => new GetDeviceStatus
            {
                Id = type.Id,
                Name = type.Name
            });
        }

        public async Task<GetDeviceMoreInformation> GetDeviceMoreInformationAsync(Guid deviceId)
        {
            GetDeviceMoreInformationRequest grpcRequest = new() { DeviceId = deviceId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDeviceMoreInformationAsync, grpcRequest, hasClientSideLog: false);

            return new GetDeviceMoreInformation
            {
                DeviceEndpoint = new()
                {
                    EndpointAddress = grpcResponse.DeviceEndpoint.EndpointAddress,
                    SampleRequest = grpcResponse.DeviceEndpoint.SampleRequest,
                },

                DeviceQueue = new()
                {
                    QueueModel = grpcResponse.DeviceQueue.QueueModel,
                    QueueName = grpcResponse.DeviceQueue.QueueName,
                },
                DeviceHierarchy = new()
                {
                    CompanyId = new Guid(grpcResponse.DeviceHierarchy.CompanyId),
                    CompanyName = grpcResponse.DeviceHierarchy.CompanyName,
                    CountryId = new Guid(grpcResponse.DeviceHierarchy.CountryId),
                    CountryName = grpcResponse.DeviceHierarchy.CountryName,
                    RegionId = new Guid(grpcResponse.DeviceHierarchy.RegionId),
                    RegionName = grpcResponse.DeviceHierarchy.RegionName,
                    AreaId = new Guid(grpcResponse.DeviceHierarchy.AreaId),
                    AreaName = grpcResponse.DeviceHierarchy.AreaName,
                    SubAreaId = new Guid(grpcResponse.DeviceHierarchy.SubAreaId),
                    SubAreaName = grpcResponse.DeviceHierarchy.SubAreaName
                }
            };
        }

        public async Task<IEnumerable<Hierarchy>> GetHierarchyAsync(Guid deviceId)
        {
            GetHierarchyRequest grpcRequest = new() { Id = deviceId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.GetHierarchyAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.Hierarchies.Select(hierarchy => new Hierarchy
            {
                Id = new Guid(hierarchy.Id),
                Name = hierarchy.Name,
                HierarchyLevel = hierarchy.HierarchyLevel switch
                {
                    global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Default => throw new NotImplementedException(),
                    global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Tenant => HierarchyLevel.Tenant,
                    global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Company => HierarchyLevel.Company,
                    global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Country => HierarchyLevel.Country,
                    global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Region => HierarchyLevel.Region,
                    global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Area => HierarchyLevel.Area,
                    global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.SubArea => HierarchyLevel.SubArea,
                    global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Device => HierarchyLevel.Device,
                    _ => throw new NotImplementedException(),
                }
            });
        }

        public async Task<IEnumerable<GetDeviceBrandDetails>> GetDeviceBrandsAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDeviceBrandsAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.DeviceBrands.Select(u => new GetDeviceBrandDetails
            {
                Id = new Guid(u.Id),
                Name = u.Name
            });
        }

        public async Task<GetDeviceBrandDetails> GetDeviceBrandByIdAsync(Guid brandId)
        {
            GetDeviceBrandRequest grpcRequest = new() { BrandId = brandId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDeviceBrandByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetDeviceBrandDetails
            {
                Id = new Guid(grpcResponse.DeviceBrand.Id),
                Name = grpcResponse.DeviceBrand.Name
            };
        }

        public async Task<GetDeviceBrandModelDetails> GetDeviceBrandModelByIdAsync(Guid modelId)
        {
            GetDeviceBrandModelRequest grpcRequest = new() { BrandModelId = modelId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDeviceBrandModelByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetDeviceBrandModelDetails
            {
                Id = new Guid(grpcResponse.DeviceBrandModel.Id),
                Name = grpcResponse.DeviceBrandModel.Name,
                BrandId = new Guid(grpcResponse.DeviceBrandModel.BrandId),
            };
        }

        public async Task<IEnumerable<GetDeviceBrandModelDetails>> GetDeviceBrandModelsAsync(Guid brandId)
        {
            GetDeviceBrandModelsRequest grpcRequest = new() { BrandId = brandId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_deviceClient.GetDeviceBrandModelsAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.DeviceBrandModels.Select(u => new GetDeviceBrandModelDetails
            {
                Id = new Guid(u.Id),
                Name = u.Name,
                BrandId = new Guid(u.BrandId)
            });
        }

        public async Task AddDeviceBrandAsync(AddDeviceBrandApiRequest request)
        {
            CreateDeviceBrandRequest grpcRequest = new() { Name = request.Name, };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.AddDeviceBrandAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task AddDeviceBrandModelAsync(Guid brandId, AddDeviceBrandModelApiRequest request)
        {
            CreateDeviceBrandModelRequest grpcRequest = new() { Name = request.Name, BrandId = brandId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.AddDeviceBrandModelAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task ChangeDeviceBrandAsync(Guid brandId, ChangeDeviceBrandApiRequest request)
        {
            UpdateDeviceBrandRequest grpcRequest = new()
            {
                Name = request.Name,
                BrandId = brandId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.ChangeDeviceBrandAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task ChangeDeviceBrandModelAsync(Guid brandModelId, ChangeDeviceBrandModelApiRequest request)
        {
            UpdateDeviceBrandModelRequest grpcRequest = new()
            {
                Name = request.Name,
                 BrandModelId = brandModelId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.ChangeDeviceBrandModelAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task DeleteDeviceBrandAsync(Guid brandId)
        {
            DeleteDeviceBrandRequest grpcRequest = new() {  BrandId = brandId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.DeleteDeviceBrandAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task DeleteDeviceBrandModelAsync(Guid brandModelId)
        {
            DeleteDeviceBrandModelRequest grpcRequest = new() { BrandModelId = brandModelId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_deviceClient.DeleteDeviceBrandModelAsync, grpcRequest, hasClientSideLog: false);
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}





























/*
                    switch (hierarchyTreeSelectFilter.HierarchyLevel)
                    {
                        case HierarchyLevel.Company:
                            hierarchyTreeSelectFilterRequest.CompanyParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Company,
                                Id = hierarchyTreeSelectFilter.Id.ToString(),
                            };
                            break;


                        case HierarchyLevel.Country:
                            hierarchyTreeSelectFilterRequest.CountryParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Country,
                                Id = hierarchyTreeSelectFilter.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.CompanyParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Company,
                                Id = hierarchyTreeSelectFilter.ParentNode.Id.ToString(),
                            };
                            break;


                        case HierarchyLevel.Region:
                            hierarchyTreeSelectFilterRequest.RegionParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Region,
                                Id = hierarchyTreeSelectFilter.ParentNode.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.CountryParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Country,
                                Id = hierarchyTreeSelectFilter.ParentNode.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.CompanyParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Company,
                                Id = hierarchyTreeSelectFilter.ParentNode.ParentNode.Id.ToString(),
                            };
                            break;


                        case HierarchyLevel.Area:
                            hierarchyTreeSelectFilterRequest.AreaParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Area,
                                Id = hierarchyTreeSelectFilter.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.RegionParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Region,
                                Id = hierarchyTreeSelectFilter.ParentNode.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.CountryParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Country,
                                Id = hierarchyTreeSelectFilter.ParentNode.ParentNode.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.CompanyParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Company,
                                Id = hierarchyTreeSelectFilter.ParentNode.ParentNode.ParentNode.Id.ToString(),
                            };
                            break;


                        case HierarchyLevel.SubArea:
                            hierarchyTreeSelectFilterRequest.SubAreaParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.SubArea,
                                Id = hierarchyTreeSelectFilter.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.AreaParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Area,
                                Id = hierarchyTreeSelectFilter.ParentNode.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.RegionParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Region,
                                Id = hierarchyTreeSelectFilter.ParentNode.ParentNode.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.CountryParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Country,
                                Id = hierarchyTreeSelectFilter.ParentNode.ParentNode.ParentNode.Id.ToString(),
                            };
                            hierarchyTreeSelectFilterRequest.CompanyParent = new()
                            {
                                HierarchyLevel = global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Company,
                                Id = hierarchyTreeSelectFilter.ParentNode.ParentNode.ParentNode.ParentNode.Id.ToString(),
                            };
                            break;
                    }
                    */