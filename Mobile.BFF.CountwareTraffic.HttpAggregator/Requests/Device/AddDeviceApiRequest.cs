﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;
using System.Net;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AddDeviceApiRequest : SensormaticRequestValidate
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int DeviceTypeId { get; set; }
        public string IpAddress { get; set; }
        public int Port { get; set; }
        public string Identity { get; set; }
        public string Password { get; set; }
        public string UniqueId { get; set; }
        public string MacAddress { get; set; }
        public Guid BrandId { get; set; }
        public int ControlFrequency { get; set; }
        public string Firmware { get; set; }
        public Guid ModelId { get; set; }
        public string Note { get; set; }
        public Guid? UnknownDeviceId { get; set; }
        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1198 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1199 Name invalid format", nameof(Name)));

            if (ModelId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1200 ModelId cannot be null", nameof(ModelId)));

            if (BrandId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1201 BrandId cannot be null", nameof(BrandId)));

            if (!Firmware.IsNullOrWhiteSpace() && Firmware.Length > 50)
                ValidateResults.Add(new ErrorResult($"#E1202 FirmWare invalid format", nameof(Firmware)));

            if (ControlFrequency < 60 || ControlFrequency > 1440)
                ValidateResults.Add(new ErrorResult($"#E1203 ControlFrequency with value: {ControlFrequency} wrong format", nameof(ControlFrequency)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1204 Description invalid format", nameof(Description)));

            if (DeviceTypeId < 1)
                ValidateResults.Add(new ErrorResult($"#E1205 DeviceTypeId with id: {DeviceTypeId} invalid format", nameof(DeviceTypeId)));

            if (!IPAddress.TryParse(IpAddress, out IPAddress ipA))
                ValidateResults.Add(new ErrorResult($"#E1206 IpAddress with value: {IpAddress} wrong format", nameof(IpAddress)));

            if (MacAddress.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1207 MacAddress Value cannot be null", nameof(MacAddress)));

            if (!MacAddress.IsNullOrWhiteSpace() && MacAddress.Length > 17)
                ValidateResults.Add(new ErrorResult($"#E1208 MacAddress invalid format", nameof(MacAddress)));

            if (!Identity.IsNullOrWhiteSpace() && Identity.Length > 17)
                ValidateResults.Add(new ErrorResult($"#E1209 Identity invalid format", nameof(Identity)));

            if (!Password.IsNullOrWhiteSpace() && Password.Length > 16)
                ValidateResults.Add(new ErrorResult($"#E1210 Password invalid format", nameof(Password)));

            if (!UniqueId.IsNullOrWhiteSpace() && UniqueId.Length > 75)
                ValidateResults.Add(new ErrorResult($"#E1211 UniqueId invalid format", nameof(UniqueId)));

            if (Port < 1 || Port > 65535)
                ValidateResults.Add(new ErrorResult($"#E1212 Port with value: {Port} wrong format", nameof(Port)));
        }
    }
}
