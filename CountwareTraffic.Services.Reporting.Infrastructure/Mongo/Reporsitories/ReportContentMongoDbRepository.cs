﻿using CountwareTraffic.Services.Reporting.Application;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.MongoDb;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public interface IReportContentMongoDbRepository : ISingletonDependency
    {
        Task AddAsync(ReportContent emailTemple);
        Task<ReportContentDto> GetAsync(Guid reportId);
    }

    public class ReportContentMongoDbRepository : MongoDbRepository<ReportContent>, IReportContentMongoDbRepository
    {
        public ReportContentMongoDbRepository(IConfiguration configuration) : base(configuration, "ScheduleReports", "Reports") { }

        public async new Task AddAsync(ReportContent report)
        {
            await base.AddAsync(report);
        }

        public async Task<ReportContentDto> GetAsync(Guid reportId)
        {
            var template = await Collection.Find(x => x.ReportId == reportId.ToString())
                 .Project(x => new ReportContentDto
                 {
                     ReportBase64Content = x.ReportBase64Content,
                     ReportCreatedDate = x.ReportCreatedDate,
                     ReportFormat = x.ReportFormat,
                     ReportId = x.ReportId,
                     ReportName = x.ReportName,

                 })
                 .FirstOrDefaultAsync();
            return template;
        }
    }
}
