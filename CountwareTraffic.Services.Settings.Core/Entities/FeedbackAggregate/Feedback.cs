﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Settings.Core
{
    public class Feedback : AggregateRoot, IDateable, ITraceable, IMultiTenant
    {
        private string _subject;
        private string _message;
        private string _name;
        private string _contactNumber;
        private string _email;
        private DateTime _receivedDate;


        public string Subject => _subject;
        public string Message => _message;
        public string Name => _name;
        public string ContactNumber => _contactNumber;
        public string Email => _email;
        public DateTime ReceivedDate => _receivedDate;


        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public Guid TenantId { get; set; }
        #endregion default properties

        private Feedback() { }

        public static Feedback Create(string subject, string message, string name, string contactName, string email)
        {
            if (string.IsNullOrWhiteSpace(subject))
                throw new ArgumentNullException(nameof(subject));

            if (string.IsNullOrWhiteSpace(message))
                throw new ArgumentNullException(nameof(message));

            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name));

            var feedback = new Feedback
            {
                _name = name,
                _message = message,
                _subject = subject,
                _receivedDate = DateTime.Now,
                _email = email,
                _contactNumber = contactName
            };

            return feedback;
        }
    }
}


//CREATE TABLE dbo.Feedback
//  ([ID] INT IDENTITY(1,1) PRIMARY KEY, --Primary key of your table
//   [Subject] VARCHAR(500),
//   [Message] VARCHAR(MAX),--limit the datatype as per your allowed characters
//   [Name]  VARCHAR(150),
//   [Contactnumber] VARCHAR(20),
//   [Email] VARCHAR(150),
//   [ReceivedDate] DATETIME Default(GETDATE()) --the date you received the feedback 
//   )