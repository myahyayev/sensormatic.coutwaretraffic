﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record AreaDeletedRejected : MessageEnvelope, IQueueEvent
    {
        public AreaDeletedRejected(Guid areaId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            AreaId = areaId;
        }
        public Guid AreaId { get; }
        public Guid RecordId { get; init; }
    }
}
