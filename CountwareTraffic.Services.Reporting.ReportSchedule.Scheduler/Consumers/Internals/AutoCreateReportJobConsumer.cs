﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using Microsoft.Extensions.Options;
using Quartz;
using Quartz.Spi;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.ReportSchedule.Scheduler
{
    public class AutoCreateReportJobConsumer : IConsumer<Sensormatic.Tool.QueueModel.ScheduleReportCreated>, ITransientSelfDependency
    {
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IJobFactory _jobFactory;
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly TimeZoneSettings _timeZoneSettings;
        public AutoCreateReportJobConsumer(ISchedulerFactory schedulerFactory,  IJobFactory jobFactory, IQueryDispatcher queryDispatcher, IOptions<TimeZoneSettings> timeZoneSettings)
        {
            _schedulerFactory = schedulerFactory;
            _jobFactory = jobFactory;
            _queryDispatcher = queryDispatcher;
            _timeZoneSettings = timeZoneSettings.Value;
        }

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.ScheduleReportCreated queuEvent)
        {
            if (queuEvent.ReportId == System.Guid.Empty)
                return;

            var reportDetail = await _queryDispatcher.QueryAsync(new GetReport { ReportId = queuEvent.ReportId });

            var scheduler = await _schedulerFactory.GetScheduler();

            scheduler.JobFactory = _jobFactory;

            var jobSchedule = new JobSchedule(
                        jobType: typeof(ReportGeneratorJob),
                        jobName: reportDetail.Id.ToString(),
                        description:reportDetail.Name,
                        tenantId: reportDetail.TenantId,
                        startsOn: reportDetail.StartsOn,
                        endsOn: reportDetail.EndsOn,
                        atSetTime: reportDetail.AtSetTime,
                        atSetTimeZone: reportDetail.AtSetTimeZone,
                        repeatOn: reportDetail.RepeatOn,
                        templateType: TemplateType.FromValue<TemplateType>(reportDetail.TemplateTypeId));


            var job = jobSchedule.CreateJob();

            var trigger = jobSchedule.CreateTrigger(_timeZoneSettings);

            await scheduler.ScheduleJob(job, trigger);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
