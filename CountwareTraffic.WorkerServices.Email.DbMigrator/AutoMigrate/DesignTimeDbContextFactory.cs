﻿using CountwareTraffic.WorkerServices.Email.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace CountwareTraffic.WorkerServices.Email.DbMigrator
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<EmailDbContext>
    {
        public EmailDbContext CreateDbContext(string[] args)
        {
            var hostConfig = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();

            var builder = new DbContextOptionsBuilder<EmailDbContext>();
            var connectionString = hostConfig.GetConnectionString("EmailDbConnection");
            builder.UseSqlServer(connectionString, x => x.EnableRetryOnFailure()/*, x => x.UseNetTopologySuite()*/);


            return new EmailDbContext(builder.Options);
        }
    }
}
