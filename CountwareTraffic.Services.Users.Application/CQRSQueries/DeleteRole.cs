﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteRole : IQuery<IdentityDeleteRoleResponse>
    {
        public Guid Id { get; set; }
    }
}
