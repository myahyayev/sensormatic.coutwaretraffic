﻿using System;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class MigrationEventDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime EndDate { get; set; }
        public int EnterCount { get; set; }
        public Int64 Id { get; set; }
        public int ExitCount { get; set; }
        //public int CountDiffrent { get; set; }
    }
}
