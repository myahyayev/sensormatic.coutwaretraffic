﻿using Sensormatic.Tool.Ioc;

namespace CountwareTraffic.Services.Reporting.Application
{
    public interface ICorrelationService : ITransientDependency
    {
        string CorrelationId { get; }
    }
}
