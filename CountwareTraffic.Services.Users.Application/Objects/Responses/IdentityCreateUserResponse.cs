﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityCreateUserResponse
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public int Status { get; set; }
        public string TraceId { get; set; }
        public string Message { get; set; }
        public IdentityCreateUserErrors Errors { get; set; }
    }

    public class IdentityCreateUserErrors
    {
        public List<string> Email { get; set; }
        public List<string> FirstName { get; set; }
        public List<string> LastName { get; set; }
        public List<string> UserName { get; set; }
        public List<string> PhoneNumber { get; set; }
        public List<string> Password { get; set; }
        public List<string> ConfirmPassword { get; set; }
    }
}
