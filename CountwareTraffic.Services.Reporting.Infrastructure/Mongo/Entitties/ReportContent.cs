﻿using MongoDB.Bson;
using Sensormatic.Tool.MongoDb;
using System;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportContent : MongoDbEntity
    {
        public static ReportContent Create(Guid reportId, string reportName, string reportFormat, string reportBase64Content)
        {
            return new ReportContent
            {
                Id = ObjectId.GenerateNewId().ToString(),
                ReportId = reportId.ToString(),
                ReportName = reportName,
                ReportFormat = reportFormat,
                ReportBase64Content = reportBase64Content,
                ReportCreatedDate = DateTime.Now,
            };
        }
        public string ReportId { get; init; }
        public string ReportName { get; init; }
        public string ReportFormat { get; init; }
        public string ReportBase64Content { get; init; }
        public DateTime ReportCreatedDate { get; init; }
    }
}
