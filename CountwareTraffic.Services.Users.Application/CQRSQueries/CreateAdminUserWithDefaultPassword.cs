﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Collections.Generic;
using System;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries
{
    public class CreateAdminUserWithDefaultPassword : IQuery<IdentityCreateAdminUserWithDefaultPasswordResponse>
    {
        public CreateAdminUserWithDefaultPassword()
        {
            Roles = new();
            TenantIds = new();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public bool Status { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public int Gender { get; set; }
        public DateTime Birthdate { get; set; }
        public int UserType { get; set; }
        public string Address { get; set; }
        public bool AllowSms { get; set; }
        public bool AllowEmail { get; set; }
        public string ProfilePic { get; set; }
        public List<string> Roles { get; set; }
        public string Domain { get; set; }
        public string Language { get; set; }
        public List<string> TenantIds { get; set; }
    }
}
