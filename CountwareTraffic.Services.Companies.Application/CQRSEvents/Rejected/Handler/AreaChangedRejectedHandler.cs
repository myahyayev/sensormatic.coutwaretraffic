﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaChangedRejectedHandler : IEventHandler<AreaChangedRejected>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public AreaChangedRejectedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(AreaChangedRejected command)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var area = await areaRepository.GetAsync(command.AreaId, command.TenantId);

            area.WhenChangedRejected(command.OldName);

            await _unitOfWork.CommitAsync();

            _queueService.Publish(new AreaChangedFailed
            {
                RecordId = Guid.NewGuid(),
                AreaId = command.AreaId,
                OldName = command.OldName,
                UserId = command.UserId,
                NewName = command.Name,
                UserName =String.Empty,
                TenantId = command.TenantId
            });
        }
    }
}
