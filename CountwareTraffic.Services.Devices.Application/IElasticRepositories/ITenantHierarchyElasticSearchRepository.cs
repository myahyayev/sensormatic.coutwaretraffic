﻿using Sensormatic.Tool.Ioc;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public interface ITenantHierarchyElasticSearchRepository : IScopedDependency 
    {
        Task AddDeviceAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto, DeviceElasticDto deviceElasticDto);
        Task UpdateDeviceAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto, DeviceElasticDto deviceElasticDto);
        Task DeleteDeviceAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto, DeviceElasticDto deviceElasticDto);
    }
}
