﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [Authorize]
    public class AreaService : Area.AreaBase
    {
        private readonly ILogger<AreaService> _logger;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        public AreaService(ILogger<AreaService> logger, ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }

        public override async Task<GetAreaDetailResponse> GetAreaById(GetAreaRequest request, ServerCallContext context)
        {
            var area = await _queryDispatcher.QueryAsync(new GetArea { AreaId = request._AreaId });

            GetAreaDetailResponse response = new();

            response.AreaDetail = new()
            {
                Id = area.Id.ToString(),
                Name = area.Name,
                Description = area.Description,
                RegionId = area.RegionId.ToString(),

                AreaTypeName = area.AreaTypeName,
                AreaTypeId = area.AreaTypeId,

                Latitude = area.Latitude,
                Longitude = area.Longitude,
                Street = area.Street,

                ManagerName = area.ManagerName,
                ManagerId = area.ManagerId.HasValue ? area.ManagerId.ToString() : null,
                PhoneNumber = area.PhoneNumber,
                GsmNumber = area.GsmNumber,
                EmailAddress = area.EmailAddress,
                GsmCountryCode = area.GsmCountryCode,
                PhoneDialCode = area.PhoneDialCode,
                PhoneCountryCode = area.PhoneCountryCode,
                GsmDialCode = area.GsmDialCode,
                CityId = area.CityId.ToString(),
                DistrictId = area.DistrictId.ToString(),
                CountryLookupId = area.CountryLookupId.ToString(),
                Audit = new Audit
                {
                    AuditCreateBy = area.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(area.AuditCreateDate),
                    AuditModifiedBy = area.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(area.AuditModifiedDate),
                },
                WorkingHoursStart = area.WorkingHoursStart?.Ticks,
                WorkingHoursEnd = area.WorkingHoursEnd?.Ticks,
                WorkingTimeZone = area.WorkingTimeZone
            };
            return response;
        }

        public override async Task<AreaPagingResponse> GetAreas(GetAreasRequest request, ServerCallContext context)
        {
            var pagingAreas = await _queryDispatcher.QueryAsync(new GetAreas
            {
                RegionId = request._RegionId,

                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            AreaPagingResponse response = new()
            {
                TotalCount = pagingAreas.TotalCount,
                HasNextPage = pagingAreas.HasNextPage,
                Page = pagingAreas.Page,
                Limit = pagingAreas.Limit,
                Next = pagingAreas.Next,
                Prev = pagingAreas.Prev
            };

            pagingAreas.Data.ToList().ForEach(area => response.AreaDetails.Add(new AreaDetail
            {
                Id = area.Id.ToString(),
                Name = area.Name,
                Description = area.Description,
                RegionId = area.RegionId.ToString(),
                CityId = area.CityId.ToString(),
                AreaTypeName = area.AreaTypeName,
                AreaTypeId = area.AreaTypeId,
                DistrictId = area.DistrictId.ToString(),
                Latitude = area.Latitude,
                Longitude = area.Longitude,
                Street = area.Street,

                ManagerName = area.ManagerName,
                ManagerId = area.ManagerId.HasValue ? area.ManagerId.ToString() : null,
                PhoneNumber = area.PhoneNumber,
                GsmNumber = area.GsmNumber,
                EmailAddress = area.EmailAddress,
                GsmCountryCode = area.GsmCountryCode,
                PhoneDialCode = area.PhoneDialCode,
                PhoneCountryCode = area.PhoneCountryCode,
                GsmDialCode = area.GsmDialCode,
                CountryLookupId = area.CountryLookupId.ToString(),
                Audit = new Audit
                {
                    AuditCreateBy = area.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(area.AuditCreateDate),
                    AuditModifiedBy = area.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(area.AuditModifiedDate),
                },
                WorkingHoursStart = area.WorkingHoursStart?.Ticks,
                WorkingHoursEnd = area.WorkingHoursEnd?.Ticks,
                WorkingTimeZone = area.WorkingTimeZone
            }));
            return response;
        }

        public override async Task<CreateSuccessResponse> AddArea(CreateAreaRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new CreateArea
            {
                DistrictId = request._DistrictId,
                EmailAddress = request.EmailAddress,
                AreaTypeId = request.AreaTypeId,
                Description = request.Description,
                GsmNumber = request.GsmNumber,
                Latitude = request.Latitude.HasValue ? request.Latitude.Value : 0,
                Longitude = request.Longitude.HasValue ? request.Longitude.Value : 0,
                Name = request.Name,
                PhoneNumber = request.PhoneNumber,
                Street = request.Street,
                CityId = request._CityId,
                GsmCountryCode = request.GsmCountryCode,
                GsmDialCode = request.GsmDialCode,
                ManagerId = request._ManagerId,
                ManagerName = request.ManagerName,
                PhoneCountryCode = request.PhoneCountryCode,
                PhoneDialCode = request.PhoneDialCode,
                RegionId = request._RegionId,
                WorkingHoursStart = request.WorkingHoursStart != null ? TimeSpan.FromTicks(request.WorkingHoursStart.Value) : null,
                WorkingHoursEnd = request.WorkingHoursEnd != null ? TimeSpan.FromTicks(request.WorkingHoursEnd.Value) : null,
                WorkingTimeZone = request.WorkingTimeZone
            });

            return new CreateSuccessResponse { Created = "Created" };
        }

        public override async Task<UpdateSuccessResponse> ChangeArea(UpdateAreaRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UpdateArea
            {
                DistrictId = request._DistrictId,
                EmailAddress = request.EmailAddress,
                AreaTypeId = request.AreaTypeId,
                Description = request.Description,
                GsmNumber = request.GsmNumber,
                Latitude = request.Latitude.HasValue ? request.Latitude.Value : 0,
                Longitude = request.Longitude.HasValue ? request.Longitude.Value : 0,
                Name = request.Name,
                PhoneNumber = request.PhoneNumber,
                Street = request.Street,
                CityId = request._CityId,
                GsmCountryCode = request.GsmCountryCode,
                GsmDialCode = request.GsmDialCode,
                ManagerId = request._ManagerId,
                ManagerName = request.ManagerName,
                PhoneCountryCode = request.PhoneCountryCode,
                PhoneDialCode = request.PhoneDialCode,
                AreaId = request._AreaId,
                WorkingHoursStart = request.WorkingHoursStart != null ? TimeSpan.FromTicks(request.WorkingHoursStart.Value) : null,
                WorkingHoursEnd = request.WorkingHoursEnd != null ? TimeSpan.FromTicks(request.WorkingHoursEnd.Value) : null,
                WorkingTimeZone = request.WorkingTimeZone
            });

            return new UpdateSuccessResponse { Updated = "Updated" };
        }

        public override async Task<DeleteSuccessResponse> DeleteArea(DeleteAreaRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new DeleteArea { AreaId = request._AreaId });
            return new DeleteSuccessResponse { Deleted = "Deleted" };
        }

        public override async Task<GetAreaTypesResponse> GetAreaTypes(GetAreaTypesRequest request, ServerCallContext context)
        {
            var areaTypes = await _queryDispatcher.QueryAsync(new GetAreaTypes { });

            GetAreaTypesResponse response = new();

            areaTypes.ToList().ForEach(type => response.AreaTypes.Add(new AreaType
            {
                Id = type.Id,
                Name = type.Name
            }));

            return response;
        }

        public override async Task<GetHierarchyResponse> GetHierarchy(GetHierarchyRequest request, ServerCallContext context)
        {
            var hierarchies = await _queryDispatcher.QueryAsync(new GetAreaHierarchy { Id = request._Id });

            GetHierarchyResponse response = new();

            hierarchies.ForEach(hierarchy => response.Hierarchies.Add(new Hierarchy
            {
                Id = hierarchy.Id.ToString(),
                Name = hierarchy.Name,
                HierarchyLevel = hierarchy.HierarchyLevel switch
                {
                    Core.HierarchyLevel.Tenant => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Tenant,
                    Core.HierarchyLevel.Company => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Company,
                    Core.HierarchyLevel.Country => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Country,
                    Core.HierarchyLevel.Region => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Region,
                    Core.HierarchyLevel.Area => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Area,
                    Core.HierarchyLevel.SubArea => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.SubArea,
                    Core.HierarchyLevel.Device => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Device,
                    _ => throw new NotImplementedException(),
                }
            }));

            return response;
        }

        public override async Task<KeyValueResponse> GetAreasKeyValue(KeyValueRequest request, ServerCallContext context)
        {
            var keyValues = await _queryDispatcher.QueryAsync(new GetAreaKeyValue() { ParentId = request._ParentId });

            KeyValueResponse response = new KeyValueResponse();

            keyValues.ForEach(u => response.KeyValues.Add(new KeyValue
            {
                Id = u.Id.ToString(),
                Name = u.Name
            }));

            return response;
        }
    }
}
