﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public class DeviceNotRegisteredException : AppException
    {
        public string IpAddress { get; }
        public string MacAddress { get; }


        public DeviceNotRegisteredException(string ipAddress,string macAddress)
            : base(new List<ErrorResult>() { new ErrorResult($"Device not found IpAddress: {ipAddress}, MacAddress: {macAddress}") } , 404, ResponseMessageType.Error)
        {
            IpAddress = ipAddress;
            MacAddress = macAddress;
        }
    }
}
