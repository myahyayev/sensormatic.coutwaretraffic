﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Settings.Core
{
    public class Note : AggregateRoot, IDateable, ITraceable, IMultiTenant, IAuditable
    {
        private bool _isAffectToReport;
        private bool _isVisible;
        private DateTime _beginDate;
        private DateTime _endDate;
        private string _subject;
        private string _content;

        public bool IsAffectToReport => _isAffectToReport;
        public bool IsVisible => _isVisible;
        public DateTime BeginDate => _beginDate;
        public DateTime EndDate => _endDate;
        public string Subject => _subject;
        public string Content => _content;


        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public Guid TenantId { get; set; }
        #endregion default properties


        private Note() { }

        public static Note Create(string subject, string content, DateTime begindate, DateTime endDate, bool isAffectToReport, bool isVisible)
        {
            if (string.IsNullOrWhiteSpace(subject))
                throw new ArgumentNullException(nameof(subject));

            if (string.IsNullOrWhiteSpace(content))
                throw new ArgumentNullException(nameof(content));

            if (begindate == DateTime.MinValue)
                throw new ArgumentNullException(nameof(begindate));

            if (endDate == DateTime.MinValue)
                throw new ArgumentNullException(nameof(endDate));

            if (begindate > endDate)
                throw new NoteDateInvalidFormatException();

            Note note = new()
            {
                _isAffectToReport = isAffectToReport,
                _isVisible = isVisible,
                _beginDate = begindate,
                _endDate = endDate,
                _subject = subject,
                _content = content
            };

            return note;
        }

        public void WhenCreated()
            => AddEvent(new NoteCreated(_subject, _content, _beginDate, _endDate, _isAffectToReport, _isVisible, TenantId, AuditCreateDate, AuditCreateBy, AuditModifiedDate, AuditModifiedBy));
    }
}
