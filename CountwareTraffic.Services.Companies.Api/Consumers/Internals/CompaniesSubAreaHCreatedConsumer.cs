﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesSubAreaHCreatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.SubAreaHCreated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesSubAreaHCreatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.SubAreaHCreated queuEvent)
        {
            var subAreaHCreated = _eventMapper.Map(queuEvent) as SubAreaHCreated;
            await _eventDispatcher.PublishAsync(subAreaHCreated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
