﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record CountryDeleted : MessageEnvelope, IQueueEvent
    {
        public CountryDeleted(Guid countryId, string countryName, Guid companyId, Guid recordId, Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            CountryId = countryId;
            CountryName = countryName;
            CompanyId = companyId;
            RecordId = recordId;
        }
        public Guid CountryId { get; }
        public string CountryName { get; }
        public Guid CompanyId { get; }
        public Guid RecordId { get; init; }
    }
}
