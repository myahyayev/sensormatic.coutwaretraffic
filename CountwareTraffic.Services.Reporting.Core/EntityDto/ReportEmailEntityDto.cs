﻿namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportEmailEntityDto
    {
        public string To { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
    }
}
