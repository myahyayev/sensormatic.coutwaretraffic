﻿using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application.Objects.Requests
{
    public class IdentityCreateAdminRoleRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Permissions { get; set; }
    }
}
