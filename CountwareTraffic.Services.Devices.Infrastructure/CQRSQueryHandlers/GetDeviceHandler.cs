﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class GetDeviceHandler : IQueryHandler<GetDevice, DeviceDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GetDeviceHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<DeviceDetailsDto> HandleAsync(GetDevice query)
        {
            var device = await _unitOfWork.GetRepository<IDeviceRepository>()
                .GetAsync(query.DeviceId, _currentTenant.Id);

            if (device == null)
                throw new DeviceNotFoundException(query.DeviceId);

            return new DeviceDetailsDto
            {
                Id = device.Id,
                Name = device.Name,
                Description = device.Description,
                AuditCreateBy = device.AuditCreateBy,
                AuditCreateDate = device.AuditCreateDate,
                AuditModifiedBy = device.AuditModifiedBy,
                AuditModifiedDate = device.AuditModifiedDate,
                DeviceTypeName = device.DeviceType.Name,
                DeviceStatusName = device.DeviceStatus.Name,
                SubAreaId = device.SubAreaId,
                DeviceStatusId = device.DeviceStatus.Id,
                DeviceTypeId = device.DeviceType.Id,
                BrandId = device.BrandId,
                BrandName = device.BrandName,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.FirmWare,
                IsActive = device.IsActive,
                Note = device.Note,
                ModelId = device.ModelId,
                ModelName = device.ModelName,
                Identity = device.ConnectionInfo?.Identity,
                IpAddress = device.ConnectionInfo?.IpAddress,
                MacAddress = device.ConnectionInfo?.MacAddress,
                Password = device.ConnectionInfo?.Password,
                Port = device.ConnectionInfo == null ? 0 : device.ConnectionInfo.Port,
                UniqueId = device.ConnectionInfo?.UniqueId
            };
        }
    }
}
