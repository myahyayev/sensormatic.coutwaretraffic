﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ForgotPasswordHandler : IQueryHandler<ForgotPassword, IdentityForgotPasswordResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public ForgotPasswordHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityForgotPasswordResponse> HandleAsync(ForgotPassword query)
        {
            return await _identityServerService.ForgotPasswordAsync(query.UserName, query.Domain, query.Language);
        }
    }
}
