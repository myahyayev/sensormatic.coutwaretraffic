﻿using CountwareTraffic.Services.Companies.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class CompanyEntityTypeConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.ToTable("Companies", SchemaNames.Areas);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
               .Property(x => x.Description)
               .HasField("_description")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .HasMaxLength(2500);

            builder
              .Property(x => x.ErpCode)
              .HasField("_erpCode")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .HasMaxLength(80);

            builder
             .Property(x => x.ErpStartDate)
             .HasField("_erpStartDate")
             .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
             .Property(x => x.ErpEndDate)
             .HasField("_erpEndDate")
             .UsePropertyAccessMode(PropertyAccessMode.Field);


            var addressNavigationBuilder = builder.OwnsOne(x => x.Address);
            builder
               .Navigation(x => x.Address).Metadata.SetField("_address");
            addressNavigationBuilder.Property(x => x.City).HasMaxLength(100);
            addressNavigationBuilder.Property(x => x.Country).HasMaxLength(100);
            addressNavigationBuilder.Property(x => x.State).HasMaxLength(250);
            addressNavigationBuilder.Property(x => x.Street).HasMaxLength(250);
            addressNavigationBuilder.Property(x => x.ZipCode).HasMaxLength(20);
            addressNavigationBuilder.Property(x => x.Location).IsRequired(false);

            var contactNavigationBuilder = builder.OwnsOne(x => x.Contact);
            builder
              .Navigation(x => x.Contact).Metadata.SetField("_contact");
            contactNavigationBuilder.Property(x => x.GsmDialCode).HasMaxLength(6);
            contactNavigationBuilder.Property(x => x.PhoneDialCode).HasMaxLength(6);
            contactNavigationBuilder.Property(x => x.EmailAddress).HasMaxLength(120);
            contactNavigationBuilder.Property(x => x.GsmNumber).HasMaxLength(20);
            contactNavigationBuilder.Property(x => x.PhoneNumber).HasMaxLength(20);
            contactNavigationBuilder.Property(x => x.PersonName).HasMaxLength(120);
            contactNavigationBuilder.Property(x => x.GsmDialCodeSecondary).HasMaxLength(6);
            contactNavigationBuilder.Property(x => x.GsmNumberSecondary).HasMaxLength(20);
            contactNavigationBuilder.Property(x => x.PhoneDialCodeSecondary).HasMaxLength(6);
            contactNavigationBuilder.Property(x => x.PhoneNumberSecondary).HasMaxLength(20);
            contactNavigationBuilder.Property(x => x.EmailAddressSecondary).HasMaxLength(120);
            contactNavigationBuilder.Property(x => x.PersonNameSecondary).HasMaxLength(120);
            contactNavigationBuilder.Property(x => x.GsmCountryCode).HasMaxLength(2);
            contactNavigationBuilder.Property(x => x.PhoneCountryCode).HasMaxLength(2);
            contactNavigationBuilder.Property(x => x.GsmCountryCodeSecondary).HasMaxLength(2);
            contactNavigationBuilder.Property(x => x.PhoneCountryCodeSecondary).HasMaxLength(2);
        }
    }
}
