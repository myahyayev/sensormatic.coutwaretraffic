﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Companies.Core
{
    public class AreaStatusIdNotFoundException : DomainException
    {
        IEnumerable<AreaStatus> AreaStatuses { get; }
        public int Id { get; }

        public AreaStatusIdNotFoundException(IEnumerable<AreaStatus> areaStatuses, int id)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1459 Possible values for AreaStatus Id: {String.Join(",", areaStatuses.Select(s => s.Id))}") }, 400, ResponseMessageType.Error)
        {
            AreaStatuses = areaStatuses;
            Id = id;
        }
    }
}
