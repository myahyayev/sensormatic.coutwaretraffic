﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class AreaHSubscriber : BackgroundService
    {
        private readonly IQueueService _queueService;
        private readonly ILogger<AreaHSubscriber> _logger;

        public AreaHSubscriber(ILogger<AreaHSubscriber> logger, IQueueService queueService)
        {
            _logger = logger;
            _queueService = queueService;
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var template = new QueueConfigTemplate
            {
                PrefetchCount = 1,
                RetryCount = 0,
                RetryIntervalSeconds = 0,
                ExcludeExceptions = new List<Type> { typeof(Exception), typeof(HttpRequestException), typeof(ArgumentNullException) },
                AutoScale = true,
                ScaleUpTo = 10
            };

            _queueService.Subscribe<CompaniesAreaHCreatedConsumer>(Queues.CountwareTrafficCompaniesAreaHCreated, template);
            _queueService.Subscribe<CompaniesAreaHUpdatedConsumer>(Queues.CountwareTrafficCompaniesAreaHChanged, template);
            _queueService.Subscribe<CompaniesAreaHDeletedConsumer>(Queues.CountwareTrafficCompaniesAreaHDeleted, template);

            return Task.CompletedTask;
        }

        public async override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Area forcing to stop...");

            _queueService.StopConsumers();

            await Task.Delay(5000);

            await base.StopAsync(cancellationToken);
        }
    }
}

