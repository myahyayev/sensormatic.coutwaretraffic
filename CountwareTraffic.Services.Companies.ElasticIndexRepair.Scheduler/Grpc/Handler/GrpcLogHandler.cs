﻿using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Grpc.Common;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Handler
{
    public class GrpcLogHandler : ILogProvider
    {
        private readonly ILogger<GrpcLogHandler> _logger;
        public GrpcLogHandler(ILogger<GrpcLogHandler> logger) => _logger = logger;

        public async Task WriteLogAsync(WebServiceLog data)
        {
            //Istersen logla
        }
    }
}
