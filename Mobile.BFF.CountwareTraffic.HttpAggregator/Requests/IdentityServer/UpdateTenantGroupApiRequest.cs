﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class UpdateTenantGroupApiRequest
    {
        [FromForm(Name = "id")]
        public Guid Id { get; set; }

        [FromForm(Name = "name")]
        public string Name { get; set; }

        [FromForm(Name = "description")]
        public string Description { get; set; }
    }
}
