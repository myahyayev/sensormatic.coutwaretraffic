﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using DevExpress.XtraRichEdit.Export;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public class ScheduleReport : XtraReport, IScheduleReport, IResourceable
    {
        private object _reportData;
        public string ReportName { get; set; }

        public event Action ReportDataSourceChanged;
        protected internal string ReportCreator { get; set; }
        protected internal ReportExportype ExportFormat { get; set; }
        protected internal ReportFrequencyType FrequencyType { get; set; }
        protected internal string EmailBody { get; set; }
        protected internal string EmailSubject { get; set; }
        protected internal ReportPeriodType ReportPeriodType { get; set; }
        protected internal string EmailTo { get; set; }
        protected internal Guid ReportId { get; set; }
        protected internal List<Location> ReportLocations { get; set; }
        protected internal Guid TenantId { get; set; }
        protected internal ReportLanguage Language { get; set; }
        protected internal string RepeatOn { get;set;}
        protected internal DateTime StartDate { get; set; }
        protected internal DateTime EndDate { get; set; }
        public virtual string ResourceKey { get; }
        private IList<ResourceDto> Resources { get; init; }

        private readonly TimeZoneSettings _timeZoneSettings;

        protected readonly IResourceMongoDbRepository _resourceMongoDbRepository;

        //private object _documentOptions { get; set; }
        /// <summary>
        /// Default constructor it's important!
        /// </summary>
        public ScheduleReport() { }

        /// <summary>
        /// DI constructor
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="timeZoneSettings"></param>
        public ScheduleReport(IServiceProvider serviceProvider, IOptions<TimeZoneSettings> timeZoneSettings)
        {
            _resourceMongoDbRepository = serviceProvider.GetRequiredService<IResourceMongoDbRepository>();
            _timeZoneSettings = timeZoneSettings.Value;

            ReportDataSourceChanged += () =>
            {
                RefreshData();
            };

            RefreshData();

            Resources = _resourceMongoDbRepository.GetByAssemblyQualifiedNameAsync(ResourceKey).GetAwaiter().GetResult();
        }

        public object ReportData
        {
            get => _reportData;
            set
            {
                _reportData = value;
                OnReportDataChanged();
            }
        }

        public virtual void RefreshData() => SetResources();

        protected virtual void OnReportDataChanged() => ReportDataSourceChanged?.Invoke();

        public virtual async ValueTask<MemoryStream> ExportAsync()
        {
            var memoryStream = new MemoryStream();
            //var pdfDocumentOptions = new PdfDocumentOptions();
            //pdfDocumentOptions.Subject = Report.Name;
            //pdfDocumentOptions.Application = "countware traffic";
            //pdfDocumentOptions.Producer = "Securitas Technology";
            //pdfDocumentOptions. = "Securitas Technology";
            //PdfExportOptions options = new PdfExportOptions();
            //options.PdfACompatibility = PdfACompatibility.PdfA3b;
            //options.PasswordSecurityOptions.PermissionsPassword = "pwd";
            //options.ShowPrintDialogOnOpen = true;
            //IList<string> result = options.Validate();


            //if (result.Count > 0)
            //    Console.WriteLine(String.Join(Environment.NewLine, result));
            //else
            //    report.ExportToPdf("Result.pdf", options);
            switch (ExportFormat)
            {
                case ReportExportype.PDF:
                    await ExportToPdfAsync(memoryStream, new() { PdfACompatibility = PdfACompatibility.PdfA3b });
                    break;

                case ReportExportype.XLS:
                    await ExportToXlsAsync(memoryStream, new() { ExportMode = XlsExportMode.SingleFilePageByPage, });
                    break;

                case ReportExportype.HTML:
                    await ExportToHtmlAsync(memoryStream, new() { ExportMode = HtmlExportMode.SingleFilePageByPage });
                    break;

                case ReportExportype.Unknown:
                    throw new NotImplementedException();
            }

            return memoryStream;
        }

        public virtual async ValueTask<string> ExportBase64Async()
        {
            MemoryStream memoryStream = await ExportAsync();
            byte[] bytes = memoryStream.GetBuffer();

            return Convert.ToBase64String(bytes);
        }

        public virtual Task LoadDataSourceFromDataBaseAsync() => Task.CompletedTask;

        public virtual void SetResources() { }

        public string SubTitleValue => String.Join(", ", ReportLocations.Select(i => i.Name));
       
        public string ReportCreatedDate
        { 
            get
            {
                var dateRange = new DateRange();
                dateRange.To = dateRange.From = DateTime.Now;
                dateRange.CultureInfo = new CultureInfo(Language.ShortName);
                dateRange.TimeZoneSettings = _timeZoneSettings;
                dateRange.WindowsTimeZone = Language.WindowsTimeZone;
                dateRange.LinuxTimeZone = Language.LinuxTimeZone;

                return dateRange.DateRangeLongName();
            } 
        }

        public string TitleValue => GetResouceContentByName(nameof(TitleValue));

        protected string GetResouceContentByName(string name)
        {
            string content = name;

            var resource = Resources
                .FirstOrDefault(u => u.ResouceKey == $"{ResourceKey}.{name}"
                    && u.LanguageId.ToLower() == Language.Id.ToString().ToLower());
                

            if (resource != null)
                content = resource.Content;

            return content;
        }

        public string ReportDateRange
        {
            get
            {
                var dateRange = new DateRange();
                dateRange.Type = DateRangeType.ByDate;
                dateRange.From = StartDate;
                dateRange.To = EndDate;
                dateRange.CultureInfo = new CultureInfo(Language.ShortName);
                dateRange.TimeZoneSettings = _timeZoneSettings;
                dateRange.WindowsTimeZone = Language.WindowsTimeZone;
                dateRange.LinuxTimeZone = Language.LinuxTimeZone; 
                return dateRange.DateRangeLongName();
            }
        }
    }
}
