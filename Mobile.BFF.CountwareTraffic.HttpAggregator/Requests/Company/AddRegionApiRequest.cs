﻿using Sensormatic.Tool.Core;
using System;
using Sensormatic.Tool.Common;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AddRegionApiRequest : SensormaticRequestValidate
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? ManagerId { get; set; }
        public string ManagerName { get; set; }

        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1137 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1138 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1139 Description invalid format", nameof(Description)));

            if (!ManagerName.IsNullOrWhiteSpace() && ManagerName.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1140 ManagerName invalid format", nameof(ManagerName)));
        }
    }
}
