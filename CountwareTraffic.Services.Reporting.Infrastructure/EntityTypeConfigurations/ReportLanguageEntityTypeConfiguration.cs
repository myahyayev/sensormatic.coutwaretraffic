﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportLanguageEntityTypeConfiguration : IEntityTypeConfiguration<ReportLanguage>
    {
        public void Configure(EntityTypeBuilder<ReportLanguage> builder)
        {
            builder.ToTable("ReportLanguages", SchemaNames.Reports);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired();

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder
              .Property(x => x.ShortName)
              .HasField("_shortName")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .IsRequired()
              .HasMaxLength(10);

            builder
             .Property(x => x.LinuxTimeZone)
             .HasField("_linuxTimeZone")
             .UsePropertyAccessMode(PropertyAccessMode.Field)
             //.IsRequired()
             .HasMaxLength(500);

            builder
             .Property(x => x.WindowsTimeZone)
             .HasField("_windowsTimeZone")
             .UsePropertyAccessMode(PropertyAccessMode.Field)
             //.IsRequired()
             .HasMaxLength(500);

            builder
              .Property(x => x.Description)
              .HasField("_description")
              .UsePropertyAccessMode(PropertyAccessMode.Field)
              .HasMaxLength(2500);
        }
    }
}
