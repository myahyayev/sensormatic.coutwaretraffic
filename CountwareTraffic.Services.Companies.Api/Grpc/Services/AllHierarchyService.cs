﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [Authorize]
    public class AllHierarchyService : AllHierarchy.AllHierarchyBase
    {
        private readonly ILogger<AllHierarchyService> _logger;
        private readonly IQueryDispatcher _queryDispatcher;
        public AllHierarchyService(ILogger<AllHierarchyService> logger, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _queryDispatcher = queryDispatcher;
        }

        public override async Task<GetAllHierarchyResponse> GetAllHierarchy(Empty request, ServerCallContext context)
        {
            GetAllHierarchy getAllHierarchy = new() { };

            string jsonValue = await _queryDispatcher.QueryAsync(getAllHierarchy);

            GetAllHierarchyResponse response = new();
            response.JsonValue = jsonValue;

            return response;
        }
    } 
}
