﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class AdminPermissionPaginationHandler
        : IQueryHandler<AdminPermissionPagination, IdentityAdminPermissionPaginationResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public AdminPermissionPaginationHandler(IIdentityServerService identityServerService)
        {
            _identityServerService = identityServerService;
        }

        public async Task<IdentityAdminPermissionPaginationResponse> HandleAsync(AdminPermissionPagination query)
        {
            return await _identityServerService.AdminPermissionPaginationRequestAsync(new Objects.Requests.IdentityAdminPermissionPaginationRequest
            {
                Filters = query.Filters,
                Paging = query.Paging,
                Sorts = query.Sorts,
            },
            query.Language
            );
        }
    }
}
