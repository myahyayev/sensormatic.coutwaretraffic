﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class Location
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public LocationLevel LocationLevel { get; set; }
    }
}
