﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ResetPasswordWithTokenHandler : IQueryHandler<ResetPasswordWithToken, IdentityResetPasswordWithTokenResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public ResetPasswordWithTokenHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<IdentityResetPasswordWithTokenResponse> HandleAsync(ResetPasswordWithToken query)
        {
            return await _identityServerService.ResetPasswordWithTokenAsync(new IdentityResetPasswordWithTokenRequest
            {
                Token = query.Token,
                UserName = query.UserName,
                Password = query.Password,
                ConfirmPassword = query.ConfirmPassword
            });
        }
    }
}
