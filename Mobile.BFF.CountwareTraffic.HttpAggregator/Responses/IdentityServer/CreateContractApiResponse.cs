﻿using System.Text.Json.Serialization;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer
{
    public class CreateContractApiResponse
    {
        [JsonPropertyName("status")]
        public int Status { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
