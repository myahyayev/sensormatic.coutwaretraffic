﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public record AreaCreated : Convey.CQRS.Events.IEvent
    {
        public Guid TenantId { get; set; }
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid RecordId { get; set; }
        public TimeSpan? WorkingHoursStart { get; set; }
        public TimeSpan? WorkingHoursEnd { get; set; }
        public string WorkingTimeZone { get; set; }
        public Guid UserId { get; set; }
    }
}
