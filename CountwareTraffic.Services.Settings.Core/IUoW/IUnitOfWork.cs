﻿using Sensormatic.Tool.Ioc;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Settings.Core
{
    public interface IUnitOfWork : IScopedDependency
    {
        T GetRepository<T>() where T : IRepository;
        int Commit();
        Task<int> CommitAsync();
    }
}
