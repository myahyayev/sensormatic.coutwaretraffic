﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Infrastructure.Localization;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Grpc
{
    [Authorize]
    public class TrafficService : Traffic.TrafficBase
    {
        private readonly ILogger<TrafficService> _logger;
        private readonly IQueryDispatcher _queryDispatcher;
        public TrafficService(ILogger<TrafficService> logger, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _queryDispatcher = queryDispatcher;
        }

        public override async Task<GetWidgetPerformingResponse> GetWidgetPerforming(GetWidgetPerformingRequest request, ServerCallContext context)
        {
            var widgetPerformingResponse = await _queryDispatcher.QueryAsync(new GetWidgetPerforming()
            {
                NodeFilters = request.HierarchyTreeSelectFilters.Select(u => new Application.NodeFilter
                {
                    Id = new Guid(u.NodeFilter.Id),
                    HierarchyLevel = u.NodeFilter.HierarchyLevel switch
                    {
                        HierarchyLevel.Tenant => Application.HierarchyLevel.Tenant,
                        HierarchyLevel.Company => Application.HierarchyLevel.Company,
                        HierarchyLevel.Country => Application.HierarchyLevel.Country,
                        HierarchyLevel.Region => Application.HierarchyLevel.Region,
                        HierarchyLevel.Area => Application.HierarchyLevel.Area,
                        HierarchyLevel.SubArea => Application.HierarchyLevel.SubArea,
                        HierarchyLevel.Device => Application.HierarchyLevel.Device,
                        HierarchyLevel.Default => Application.HierarchyLevel.Tenant,
                        _ => Application.HierarchyLevel.Tenant,
                    }
                }).ToList(),

                CurrentStartDate = request.CurrentStartDate.ToDateTimeOffset().LocalDateTime,
                CurrentEndDate = request.CurrentEndDate.ToDateTimeOffset().LocalDateTime,
                PreviusStartDate = request.PreviusStartDate.ToDateTimeOffset().LocalDateTime,
                PreviusEndDate = request.PreviusEndDate.ToDateTimeOffset().LocalDateTime
            });

            return new GetWidgetPerformingResponse()
            {
                InwardsPrevius = widgetPerformingResponse.InwardsPrevius,
                InwardsCurrent = widgetPerformingResponse.InwardsCurrent
            };
        }

        public override async Task<GetPerformingByTypeResponse> GetPerformingByType(GetPerformingByTypeRequest request, ServerCallContext context)
        {
            var queryResponse = await _queryDispatcher.QueryAsync(new GetPerformingByType()
            {
                EndDate = request.EndDate.ToDateTimeOffset().LocalDateTime,
                StartDate = request.StartDate.ToDateTimeOffset().LocalDateTime,
                Size = request.Size,

                NodeFilters = request.HierarchyTreeSelectFilters.Select(u => new Application.NodeFilter
                {
                    Id = new Guid(u.NodeFilter.Id),
                    HierarchyLevel = u.NodeFilter.HierarchyLevel switch
                    {
                        HierarchyLevel.Tenant => Application.HierarchyLevel.Tenant,
                        HierarchyLevel.Company => Application.HierarchyLevel.Company,
                        HierarchyLevel.Country => Application.HierarchyLevel.Country,
                        HierarchyLevel.Region => Application.HierarchyLevel.Region,
                        HierarchyLevel.Area => Application.HierarchyLevel.Area,
                        HierarchyLevel.SubArea => Application.HierarchyLevel.SubArea,
                        HierarchyLevel.Device => Application.HierarchyLevel.Device,
                        HierarchyLevel.Default => Application.HierarchyLevel.Tenant,
                        _ => Application.HierarchyLevel.Tenant,
                    }
                }).ToList(),

                Direction = request.Direction switch
                {
                    Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                    Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                    Direction.Default => throw new NotImplementedException(),
                    _ => throw new NotImplementedException(),
                },

                PerformType = request.PerformType switch
                {
                    PerformType.Default => Application.PerformType.Device,
                    PerformType.ByTenant => Application.PerformType.Tenant,
                    PerformType.ByCompany => Application.PerformType.Company,
                    PerformType.ByCountry => Application.PerformType.Country,
                    PerformType.ByRegion => Application.PerformType.Region,
                    PerformType.ByArea => Application.PerformType.Area,
                    PerformType.BySubArea => Application.PerformType.SubArea,
                    PerformType.ByDevice => Application.PerformType.Device,
                    _ => Application.PerformType.Device,
                }
            });

            var grpcResponse = new GetPerformingByTypeResponse();

            queryResponse.ForEach(u => grpcResponse.PerformingsByType.Add(new PerformingByType
            {
                Inwards = u.Inwards,
                Id = u.Id.ToString(),
                Name = u.Name
            }));

            return grpcResponse;
        }

        public override async Task<GetTrafficByTimePeriodResponse> GetTrafficByTimePeriod(GetTrafficByTimePeriodRequest request, ServerCallContext context)
        {
            var trafficByTimeResponse = await _queryDispatcher.QueryAsync(new GetTrafficByTimePeriod()
            {
                NodeFilters = request.HierarchyTreeSelectFilters.Select(u => new Application.NodeFilter
                {
                    Id = new Guid(u.NodeFilter.Id),
                    HierarchyLevel = u.NodeFilter.HierarchyLevel switch
                    {
                        HierarchyLevel.Tenant => Application.HierarchyLevel.Tenant,
                        HierarchyLevel.Company => Application.HierarchyLevel.Company,
                        HierarchyLevel.Country => Application.HierarchyLevel.Country,
                        HierarchyLevel.Region => Application.HierarchyLevel.Region,
                        HierarchyLevel.Area => Application.HierarchyLevel.Area,
                        HierarchyLevel.SubArea => Application.HierarchyLevel.SubArea,
                        HierarchyLevel.Device => Application.HierarchyLevel.Device,
                        HierarchyLevel.Default => Application.HierarchyLevel.Tenant,
                        _ => Application.HierarchyLevel.Tenant,
                    }
                }).ToList(),
                CurrentStartDate = request.CurrentStartDate.ToDateTimeOffset().LocalDateTime,
                CurrentEndDate = request.CurrentEndDate.ToDateTimeOffset().LocalDateTime,
                PreviusStartDate = request.PreviusStartDate.ToDateTimeOffset().LocalDateTime,
                PreviusEndDate = request.PreviusEndDate.ToDateTimeOffset().LocalDateTime,

                TimePeriod = request.TimePeriod switch
                {
                    TimePeriod.Default => throw new NotImplementedException(),
                    TimePeriod.Hourly => Application.TimePeriod.Hourly,
                    TimePeriod.Daily => Application.TimePeriod.Daily,
                    TimePeriod.Monthly => Application.TimePeriod.Monthly,
                    _ => throw new NotImplementedException(),
                }
            });

            GetTrafficByTimePeriodResponse grpcResponse = new();

            trafficByTimeResponse.Categories.ForEach(u => grpcResponse.Categories.Add(
                                                    LocalizationDictionary.Localizations[request.Language][u]));

            var currentSeries = new Series() { Name = trafficByTimeResponse.CurrentSeries.Name };
            trafficByTimeResponse.CurrentSeries.Data.ForEach(u => currentSeries.Data.Add(u));
            grpcResponse.Series.Add(currentSeries);

            var previusSeries = new Series() { Name = trafficByTimeResponse.PreviusSeries.Name };
            trafficByTimeResponse.PreviusSeries.Data.ForEach(u => previusSeries.Data.Add(u));
            grpcResponse.Series.Add(previusSeries);

            return grpcResponse;
        }

        public override async Task<GetCountOfVisitorsInsideResponse> GetCountOfVisitorsInside(GetCountOfVisitorsInsideRequest request, ServerCallContext context)
        {
            var countOfVisitorsInsideReponse = await _queryDispatcher.QueryAsync(new GetCountOfVisitorsInside()
            {
                NodeFilters = request.HierarchyTreeSelectFilters.Select(u => new Application.NodeFilter
                {
                    Id = new Guid(u.NodeFilter.Id),
                    HierarchyLevel = u.NodeFilter.HierarchyLevel switch
                    {
                        HierarchyLevel.Tenant => Application.HierarchyLevel.Tenant,
                        HierarchyLevel.Company => Application.HierarchyLevel.Company,
                        HierarchyLevel.Country => Application.HierarchyLevel.Country,
                        HierarchyLevel.Region => Application.HierarchyLevel.Region,
                        HierarchyLevel.Area => Application.HierarchyLevel.Area,
                        HierarchyLevel.SubArea => Application.HierarchyLevel.SubArea,
                        HierarchyLevel.Device => Application.HierarchyLevel.Device,
                        HierarchyLevel.Default => Application.HierarchyLevel.Tenant,
                        _ => Application.HierarchyLevel.Tenant,
                    }
                }).ToList(),

                CurrentStartDate = request.CurrentStartDate.ToDateTimeOffset().LocalDateTime,
            });

            return new()
            {
                CountOfVisitorsInside = countOfVisitorsInsideReponse.CountOfVisitorsInside
            };
        }

        public override async Task<GetVisitorDurationResponse> GetVisitorDuration(GetVisitorDurationRequest request, ServerCallContext context)
        {
            var visitDuration = await _queryDispatcher.QueryAsync(new GetVisitorDuration()
            {
                NodeFilters = request.HierarchyTreeSelectFilters.Select(u => new Application.NodeFilter
                {
                    Id = new Guid(u.NodeFilter.Id),
                    HierarchyLevel = u.NodeFilter.HierarchyLevel switch
                    {
                        HierarchyLevel.Tenant => Application.HierarchyLevel.Tenant,
                        HierarchyLevel.Company => Application.HierarchyLevel.Company,
                        HierarchyLevel.Country => Application.HierarchyLevel.Country,
                        HierarchyLevel.Region => Application.HierarchyLevel.Region,
                        HierarchyLevel.Area => Application.HierarchyLevel.Area,
                        HierarchyLevel.SubArea => Application.HierarchyLevel.SubArea,
                        HierarchyLevel.Device => Application.HierarchyLevel.Device,
                        HierarchyLevel.Default => Application.HierarchyLevel.Tenant,
                        _ => Application.HierarchyLevel.Tenant,
                    }
                }).ToList(),

                CurrentStartDate = request.CurrentStartDate.ToDateTimeOffset().LocalDateTime,
            });

            return new()
            {
                LastOutwardsDate = Timestamp.FromDateTimeOffset(visitDuration.LastOutwardsDate),
                LastInwardsDate = Timestamp.FromDateTimeOffset(visitDuration.LastInwardsDate)
            };
        }

        public override async Task<TimePeriodResponse> GetEventsByTimePeriod(TimePeriodRequest request, ServerCallContext context)
        {
            var eventsByTimeResponse = await _queryDispatcher.QueryAsync(new GetEventsByTimePeriod()
            {
                StartDate = request.StartDate.ToDateTimeOffset().LocalDateTime,
                EndDate = request.EndDate.ToDateTimeOffset().LocalDateTime
            });

            TimePeriodResponse grpcResponse = new();

            eventsByTimeResponse.ForEach(u => grpcResponse.Data.Add(new TimePeriodResponseData {
                AreaId = u.AreaId.ToString(),
                AreaName = u.AreaName,
                SubAreaId = u.SubAreaId.ToString(),
                SubAreaName = u.SubAreaName,
                Date = Timestamp.FromDateTimeOffset(u.Date),
                EnterCount = u.EnterCount,
                ExitCount = u.ExitCount
            }));

            return grpcResponse;
        }
    }
}
