﻿using Convey.CQRS.Commands;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;
using CountwareTraffic.WorkerServices.SignalrHub.Application;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRDeviceCreatedSuccessfullyConsumer : IConsumer<Sensormatic.Tool.QueueModel.DeviceCreatedSuccessfully>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRDeviceCreatedSuccessfullyConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.DeviceCreatedSuccessfully queueEvent)
        {
            await _commandDispatcher.SendAsync(new DeviceCreatedSuccessfully
            {
                DeviceId = queueEvent.DeviceId,
                DeviceCreationStatus = queueEvent.DeviceCreationStatus,
                UserId = queueEvent.UserId,
                UserName = queueEvent.UserName,
                TenantId = queueEvent.TenantId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
