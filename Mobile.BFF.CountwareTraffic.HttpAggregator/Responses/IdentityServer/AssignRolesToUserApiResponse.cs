﻿using System.Text.Json.Serialization;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AssignRolesToUserApiResponse
    {
        [JsonPropertyName("status")]
        public int Status { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
