﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetReportTemplateViewApiResponse
    {
        public Guid Id { get; set; }
        public string FilePath { get; set; }
    }
}
