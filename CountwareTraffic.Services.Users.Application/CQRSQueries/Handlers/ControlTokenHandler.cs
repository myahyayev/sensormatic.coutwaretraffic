﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ControlTokenHandler : IQueryHandler<ControlToken, bool>
    {
        private readonly IIdentityServerService _identityServerService;

        public ControlTokenHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<bool> HandleAsync(ControlToken query)
        {
            return await _identityServerService.ControlTokenAsync(query.Token);
        }
    }
}
