﻿using CountwareTraffic.Services.Companies.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public interface ITenantHierarchyElasticSearchRepository : IScopedDependency 
    {
        Task<bool> IsIndicesExistsAsync();
        Task AddOrUpdateTenantHierarchyAsync(TenantHierarchyEntityDto tenantHierarchy);
        Task AddCompanyAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto);
        Task UpdateCompanyAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto);
        Task DeleteComapnyAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto);
        Task AddCountryAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto);
        Task UpdateCountryAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto);
        Task DeleteCountryAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto);
        Task AddRegionAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto);
        Task UpdateRegionAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto);
        Task DeleteRegionAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto);
        Task AddAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto);
        Task UpdateAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto);
        Task DeleteAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto);
        Task AddSubAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto);
        Task UpdateSubAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto);
        Task DeleteSubAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto);
        Task<string> GetTenantHierarchyJsonFormatAsync(Guid tenantId);
    }
}
