﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesAreaHUpdatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaHUpdated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesAreaHUpdatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaHUpdated queuEvent)
        {
            var areaUpdated = _eventMapper.Map(queuEvent) as AreaHUpdated;
            await _eventDispatcher.PublishAsync(areaUpdated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
