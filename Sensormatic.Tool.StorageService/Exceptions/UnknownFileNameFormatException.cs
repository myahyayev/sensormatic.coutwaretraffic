﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace Sensormatic.Tool.StorageService
{
    public class UnknownFileNameFormatException : StorageException
    {
        public string FileName { get; set; }

        public UnknownFileNameFormatException(string fileName)
            : base(new List<ErrorResult>() { new ErrorResult($"FileName with name: {fileName} unknown format.") }, 422, ResponseMessageType.Error)
        {
            FileName = fileName;
        }
    }
}
