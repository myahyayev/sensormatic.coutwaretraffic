﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Common;
using System.Linq;
using System.Threading.Tasks;
namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetDeviceByIPAddressAndMacHandler : IQueryHandler<GetDeviceFormIpAddressAndMac, DeviceDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<GetDeviceByIPAddressAndMacHandler> _logger;
        public GetDeviceByIPAddressAndMacHandler(IUnitOfWork unitOfWork, ILogger<GetDeviceByIPAddressAndMacHandler> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<DeviceDto> HandleAsync(GetDeviceFormIpAddressAndMac query)
        {
            var device = await _unitOfWork
                .GetRepository<IDeviceRepository>()
                .GetByIPAddressAndMacAsync(query.IpAddress, query.MacAddress);

            if (device == null)
                return default;

            var area = await _unitOfWork
                .GetRepository<IAreaRepository>()
                .GetByIdAsync(device.AreaId);

            if (area == null)
                return default;

            return new DeviceDto
            {
                Id = device.Id,
                Name = device.Name,
                TenantId = device.TenantId,
                ControlFrequency = device.ControlFrequency,
                IsActive = device.IsActive,
                AreaWorkingHoursStart = area.WorkingHoursStart,
                AreaWorkingHoursEnd = area.WorkingHoursEnd,
                AreaWorkingTimeZone = area.WorkingTimeZone?.Deserialize<TimeZoneValues>()
            };
        }
    }
}
