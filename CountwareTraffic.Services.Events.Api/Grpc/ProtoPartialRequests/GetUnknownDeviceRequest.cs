﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Events.Grpc
{
    [ServiceLog]
    public sealed partial class GetUnknownDeviceRequest : SensormaticRequestValidate
    {
        internal Guid _UnknownDeviceId
        {
            get
            {
                if (Guid.TryParse(unknownDeviceId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { unknownDeviceId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_UnknownDeviceId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1529 UnknownDeviceId cannot be null", nameof(_UnknownDeviceId)));
        }
    }
}
