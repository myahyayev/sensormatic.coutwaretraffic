﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class ComapniesErpCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ErpCode",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(80)",
                maxLength: 80,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ErpEndDate",
                schema: "areas",
                table: "Companies",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ErpStartDate",
                schema: "areas",
                table: "Companies",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ErpCode",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "ErpEndDate",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "ErpStartDate",
                schema: "areas",
                table: "Companies");
        }
    }
}
