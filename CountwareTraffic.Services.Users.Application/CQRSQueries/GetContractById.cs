﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries
{
    public class GetContractById : IQuery<IdentityGetContractByIdResponse>
    {
        public Guid Id { get; set; }
    }
}
