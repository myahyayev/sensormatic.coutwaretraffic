﻿namespace Sensormatic.Tool.Common
{
    public static class Queues
    {
        #region CountwareTraffic
        //Company
        public const string CountwareTrafficCompaniesCompanyCreated = "Countware.Traffic.Companies.CompanyCreated";
        public const string CountwareTrafficCompaniesCompanyChanged = "Countware.Traffic.Companies.CompanyChanged";
        public const string CountwareTrafficCompaniesCompanyDeleted = "Countware.Traffic.Companies.CompanyDeleted";
        public const string CountwareTrafficCompaniesCountryCreated = "Countware.Traffic.Companies.CountryCreated";
        public const string CountwareTrafficCompaniesCountryChanged = "Countware.Traffic.Companies.CountryChanged";
        public const string CountwareTrafficCompaniesCountryDeleted = "Countware.Traffic.Companies.CountryDeleted";
        public const string CountwareTrafficCompaniesRegionCreated = "Countware.Traffic.Companies.RegionCreated";
        public const string CountwareTrafficCompaniesRegionChanged = "Countware.Traffic.Companies.RegionChanged";
        public const string CountwareTrafficCompaniesRegionDeleted = "Countware.Traffic.Companies.RegionDeleted";
        public const string CountwareTrafficCompaniesAreaCreated = "Countware.Traffic.Companies.AreaCreated";
        public const string CountwareTrafficCompaniesAreaChanged = "Countware.Traffic.Companies.AreaChanged";
        public const string CountwareTrafficCompaniesAreaDeleted = "Countware.Traffic.Companies.AreaDeleted";
        public const string CountwareTrafficCompaniesAreaCreatedCompleted = "Countware.Traffic.Companies.AreaCreatedCompleted";
        public const string CountwareTrafficCompaniesAreaCreatedRejected = "Countware.Traffic.Companies.AreaCreatedRejected";
        public const string CountwareTrafficCompaniesAreaChangedRejected = "Countware.Traffic.Companies.AreaChangedRejected";
        public const string CountwareTrafficCompaniesAreaDeletedRejected = "Countware.Traffic.Companies.AreaDeletedRejected";
        public const string CountwareTrafficCompaniesAreaHCreated = "Countware.Traffic.Companies.AreaHCreated";
        public const string CountwareTrafficCompaniesAreaHChanged = "Countware.Traffic.Companies.AreaHChanged";
        public const string CountwareTrafficCompaniesAreaHDeleted = "Countware.Traffic.Companies.AreaHDeleted";
        public const string CountwareTrafficCompaniesSubAreaHCreated = "Countware.Traffic.Companies.SubAreaHCreated";
        public const string CountwareTrafficCompaniesSubAreaHChanged = "Countware.Traffic.Companies.SubAreaHChanged";
        public const string CountwareTrafficCompaniesSubAreaHDeleted = "Countware.Traffic.Companies.SubAreaHDeleted";
        public const string CountwareTrafficCompaniesSubAreaCreated = "Countware.Traffic.Companies.SubAreaCreated";
        public const string CountwareTrafficCompaniesSubAreaChanged = "Countware.Traffic.Companies.SubAreaChanged";
        public const string CountwareTrafficCompaniesSubAreaDeleted = "Countware.Traffic.Companies.SubAreaDeleted";
        public const string CountwareTrafficCompaniesSubAreaCreatedCompleted = "Countware.Traffic.Companies.SubAreaCreatedCompleted";
        public const string CountwareTrafficCompaniesSubAreaCreatedRejected = "Countware.Traffic.Companies.SubAreaCreatedRejected";
        public const string CountwareTrafficCompaniesSubAreaChangedRejected = "Countware.Traffic.Companies.SubAreaChangedRejected";
        public const string CountwareTrafficCompaniesSubAreaDeletedRejected = "Countware.Traffic.Companies.SubAreaDeletedRejected";

        //Device
        public const string CountwareTrafficDevicesDeviceCreated = "Countware.Traffic.Devices.DeviceCreated";
        public const string CountwareTrafficDevicesDeviceChanged = "Countware.Traffic.Devices.DeviceChanged";
        public const string CountwareTrafficDevicesDeviceDeleted = "Countware.Traffic.Devices.DeviceDeleted";
        public const string CountwareTrafficDevicesDeviceCreatedCompleted = "Countware.Traffic.Devices.DeviceCreatedCompleted";
        public const string CountwareTrafficDevicesDeviceCreatedRejected = "Countware.Traffic.Devices.DeviceCreatedRejected";
        public const string CountwareTrafficDevicesDeviceChangedRejected = "Countware.Traffic.Devices.DeviceChangedRejected";
        public const string CountwareTrafficDevicesDeviceDeletedRejected = "Countware.Traffic.Devices.DeviceDeletedRejected";
        public const string CountwareTrafficDevicesDeviceStatusChanged = "Countware.Traffic.Devices.DeviceStatusChanged";
        public const string CountwareTrafficDevicesDeviceHCreated = "Countware.Traffic.Devices.DeviceHCreated";
        public const string CountwareTrafficDevicesDeviceHChanged = "Countware.Traffic.Devices.DeviceHChanged";
        public const string CountwareTrafficDevicesDeviceHDeleted = "Countware.Traffic.Devices.DeviceHDeleted";


        //Event
        public const string CountwareTrafficEventsEventCreated = "Countware.Traffic.Events.EventCreated";
        public static string CountwareTrafficEventsDeviceEventsListener = "Countware.Traffic.Events.<<{0}>>.DeviceEventsListener";
        public static string CountwareTrafficEventsDeviceMigrationEventsListener = "Countware.Traffic.Events.<<{0}>>.DeviceMigrationEventsListener";
        public const string CountwareTrafficEventsAutoCreateDeviceEventsConsumer = "Countware.Traffic.Events.AutoCreateDeviceEventsConsumer";
        public const string CountwareTrafficEventsAutoDeleteDeviceEventsConsumer = "Countware.Traffic.Events.AutoDeleteDeviceEventsConsumer";
        public const string CountwareTrafficEventsAutoCreateDeviceEventsJob = "Countware.Traffic.Events.AutoCreateDeviceEventsJob";
        public const string CountwareTrafficEventsAutoDeleteDeviceEventsJob = "Countware.Traffic.Events.AutoDeleteDeviceEventsJob";
        //public const string CountwareTrafficEventsAutoCreateDeviceEventsEndpoint = "Countware.Traffic.Events.AutoCreateDeviceEventsEndpoint";
        //public const string CountwareTrafficEventsAutoDeleteDeviceEventsEndpoint = "Countware.Traffic.Events.AutoDeleteDeviceEventsEndpoint";
        public const string CountwareTrafficEventsUnknownDeviceCreated = "Countware.Traffic.Events.UnknownDeviceCreated";
        

        //Sms
        public const string CountwareTrafficSendTemplatedSms = "Countware.Traffic.Sms.SendTemplatedSms";
        public const string CountwareTrafficSendSms = "Countware.Traffic.Sms.SendSms";

        //Mail
        public const string CountwareTrafficSendTemplatedEmail = "Countware.Traffic.Email.SendTemplatedEmail";
        public const string CountwareTrafficSendEmail = "Countware.Traffic.Email.SendEmail";

        //PushNotification
        public const string CountwareTrafficSendTemplatedPushNotification = "Countware.Traffic.PushNotification.SendTemplatedPushNotification";
        public const string CountwareTrafficSendPushNotification = "Countware.Traffic.PushNotification.SendPushNotification";

        //SignalRHub
        public const string CountwareTrafficSignalRAreaCreatedSuccessfully = "Countware.Traffic.SignalRHub.AreaCreatedSuccessfully";
        public const string CountwareTrafficSignalRAreaCreatedFailed = "Countware.Traffic.SignalRHub.AreaCreatedFailed";
        public const string CountwareTrafficSignalRAreaChangedSuccessfully = "Countware.Traffic.SignalRHub.AreaChangedSuccessfully";
        public const string CountwareTrafficSignalRAreaChangedFailed = "Countware.Traffic.SignalRHub.AreaChangedFailed";
        public const string CountwareTrafficSignalRAreaDeletedSuccessfully = "Countware.Traffic.SignalRHub.AreaDeletedSuccessfully";
        public const string CountwareTrafficSignalRAreaDeletedFailed = "Countware.Traffic.SignalRHub.AreaDeletedFailed";

        public const string CountwareTrafficSignalRSubAreaCreatedSuccessfully = "Countware.Traffic.SignalRHub.SubAreaCreatedSuccessfully";
        public const string CountwareTrafficSignalRSubAreaCreatedFailed = "Countware.Traffic.SignalRHub.SubAreaCreatedFailed";
        public const string CountwareTrafficSignalRSubAreaChangedSuccessfully = "Countware.Traffic.SignalRHub.SubAreaChangedSuccessfully";
        public const string CountwareTrafficSignalRSubAreaChangedFailed = "Countware.Traffic.SignalRHub.SubAreaChangedFailed";
        public const string CountwareTrafficSignalRSubAreaDeletedSuccessfully = "Countware.Traffic.SignalRHub.SubAreaDeletedSuccessfully";
        public const string CountwareTrafficSignalRSubAreaDeletedFailed = "Countware.Traffic.SignalRHub.SubAreaDeletedFailed";

        public const string CountwareTrafficSignalRDeviceCreatedSuccessfully = "Countware.Traffic.SignalRHub.DeviceCreatedSuccessfully";
        public const string CountwareTrafficSignalRDeviceCreatedFailed = "Countware.Traffic.SignalRHub.DeviceCreatedFailed";
        public const string CountwareTrafficSignalRDeviceChangedSuccessfully = "Countware.Traffic.SignalRHub.DeviceChangedSuccessfully";
        public const string CountwareTrafficSignalRDeviceChangedFailed = "Countware.Traffic.SignalRHub.DeviceChangedFailed";
        public const string CountwareTrafficSignalRDeviceDeletedSuccessfully = "Countware.Traffic.SignalRHub.DeviceDeletedSuccessfully";
        public const string CountwareTrafficSignalRDeviceDeletedFailed = "Countware.Traffic.SignalRHub.DeviceDeletedFailed";
        public const string CountwareTrafficSignalRUnknownDeviceCreatedSuccessfully = "Countware.Traffic.SignalR.UnknownDeviceCreatedSuccessfully";
        public const string CountwareTrafficSignalRDeviceStatusChangedSuccessfully = "Countware.Traffic.SignalR.DeviceStatusChangedSuccessfully";
        public const string CountwareTrafficSignalRReportingGeneratingCompleted = "Countware.Traffic.SignalR.ReportGeneratingCompleted";
        public const string CountwareTrafficSignalRReportGeneratigFired = "Countware.Traffic.SignalR.ReportGeneratigFired";
        public const string CountwareTrafficSignalRReportGeneratigFailed = "Countware.Traffic.SignalR.ReportGeneratigFailed";

        //Audit
        public const string CountwareTrafficAudit = "Countware.Traffic.Audit";


        //ElasticHierarchy
        public const string CountwareTrafficElasticHierarchyChanged = "Countware.Traffic.Elastic.HierarchyChanged";


        //Reporting
        public const string CountwareTrafficReportingAutoCreateReportJob = "Countware.Traffic.Reporting.AutoCreateReportJob";
        public const string CountwareTrafficReportingAutoDeleteReportJob = "Countware.Traffic.Reporting.AutoDeleteReportJob";
        public const string CountwareTrafficReportingGenerateOneTime = "Countware.Traffic.Reporting.GenerateOneTime";
        public const string CountwareTrafficReportingGeneratingCompleted = "Countware.Traffic.Reporting.ReportGeneratingCompleted";
        public const string CountwareTrafficReportingRetryGenerating = "Countware.Traffic.Reporting.RetryGenerating";


        #endregion CountwareTraffic

    }
}
