﻿namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class RefreshTokenApiRequest
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
