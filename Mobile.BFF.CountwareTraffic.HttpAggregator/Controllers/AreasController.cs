﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class AreasController : BaseController
    {
        private readonly AreaService _areaService;
        private readonly SubAreaService _subAreaService;
        public AreasController(IServiceProvider provider, ILogger<AreasController> logger, IHttpContextAccessor contextAccessor, AreaService areaService, SubAreaService subAreaService)
            : base(provider, logger, contextAccessor)
        {
            _areaService = areaService;
            _subAreaService = subAreaService;
        }


        [HttpGet("{areaId}/detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminAreaView, Permissions.AreaView })]
        public async Task<ApiResponse<GetAreaDetails>> GetAreaDetailAsync(Guid areaId)
            => Response(await _areaService.GetAreaByIdAsync(areaId), new ResultMessage("#S1066"));



        [HttpPost("{regionId}/details"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminAreaList })]
        public async Task<ApiPagedResponse<GetAreaDetails>> GetAreaDetailsAsync(Guid regionId, [FromBody] DataSourceApiRequest paging)
            => await _areaService.GetAreasAsync(regionId, paging);



        [HttpPost("{regionId}/add"), ServiceLog]
        //[Permissions(Permissions = new[] { Permissions.AreaAdd },
        //             Roles = new[] { Roles.SuperAdmin, Roles.Admin},
        //             Scopes = new[] { Scopes.AdminApp})]
        [Permissions(Permissions = new[] { Permissions.AdminAreaAdd })]
        public async Task<ApiResponse<CrudApiResponse>> AddAreaAsync(Guid regionId, [FromForm] AddAreaApiRequest request)
        {
            await _areaService.AddAreaAsync(regionId, request);
            return Response(new CrudApiResponse {   IsSuccess = true, OperationType = OperationType.Create }, new ResultMessage("#S1067"));
        }



        [HttpPut("{areaId}/change"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminAreaEdit })]
        public async Task<ApiResponse<CrudApiResponse>> ChangeAreaAsync(Guid areaId, [FromForm] ChangeAreaApiRequest request)
        {
            await _areaService.ChangeAreaAsync(areaId, request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Update}, new ResultMessage("#S1068"));
        }



        [HttpDelete("{areaId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminAreaDelete })]
        public async Task<ApiResponse<CrudApiResponse>> DeleteAreaAsync(Guid areaId)
        {
            var subAreaControl = await _subAreaService.GetSubAreasAsync(areaId, new DataSourceApiRequest());

            if (subAreaControl.TotalCount > 0)
            {
                return Response(new CrudApiResponse { IsSuccess = false, OperationType = OperationType.Delete,  Id = areaId }, new ResultMessage()
                {
                    CallToActionType = CallToActionType.GoToSubAreas,
                    Description = $"#W1026 A subArea was defined in the Area with id {areaId}.",
                    Title = $"#W1026 Area with id {areaId} cannot be deleted."
                });
            }

            await _areaService.CancelAreaAsync(areaId);

            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Delete, Id = areaId }, new ResultMessage()
            {
                CallToActionType = CallToActionType.Ok,
                Description = $"#S1064 Area with {areaId} id was deleted.",
                Title = $"#S1064 Area with {areaId} id was deleted."
            });
        }



        [HttpGet("types"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminAreaView })]
        public async Task<ApiResponse<IEnumerable<GetAreaType>>> GetAreaTypesAsync()
            => Response(await _areaService.GetAreaTypesAsync(), new ResultMessage("#S1069"));
    }
}
