﻿using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nest;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.ElasticSearch;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class TenantHierarchyElasticSearchRepository : ElasticRepository<TenantHierarchyElasticData>, ITenantHierarchyElasticSearchRepository
    {
        private readonly IQueueService _queueService;
        public TenantHierarchyElasticSearchRepository(IConfiguration configuration, ILogger<TenantHierarchyElasticData> logger, IQueueService queueService)
            : base(configuration, logger, ElasticsearchKeys.CountwareTrafficTenantsHierarchy)
        {
            _queueService = queueService;
        }

        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {

                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        private async Task MapAsync()
        {
            if ((await _client.Indices.ExistsAsync(_index)).Exists)
                return;

            var createIndexResponse = await _client.Indices.CreateAsync(_index, c => c
                .Map<TenantHierarchyElasticData>(m => m
                    .AutoMap()
                    ));

            if (!createIndexResponse.IsValid)
                throw createIndexResponse.OriginalException;
        }
        private async Task<TenantHierarchyElasticData> GetTenantHierarchyAsync(Guid tenantId)
        {
            await MapAsync();

            var descriptor = new QueryContainerDescriptor<TenantHierarchyElasticData>();

            descriptor.Bool(b => b.Must(m => m.Term(t => t.Field(f => f.Id).Value(tenantId))));

            var result = await _client.SearchAsync<TenantHierarchyElasticData>(
                search => search
                .Index(_index)
                .Source(sf => sf
                   .Includes(i => i
                       .Fields(f => f.Id, f => f.Parent))).Query(d => descriptor));

            if (!result.IsValid)
                throw new ElasticSearchQueryException(_index, result.ServerError.Status, result.ServerError.Error.Type, result.OriginalException);

            var elasticData = result.Documents.FirstOrDefault();

            return elasticData;
        }

        public async Task<bool> IsIndicesExistsAsync()
        {
            return (await _client.Indices.ExistsAsync(_index)).Exists;
        }

        public async Task AddOrUpdateTenantHierarchyAsync(TenantHierarchyEntityDto tenantHierarchy)
        {
            await MapAsync();

            var mapedData = new TenantHierarchyElasticData(tenantHierarchy.Parent.Id)
            {
                Parent = new Parent
                {
                    Label = tenantHierarchy.Parent.Name,
                    Data = new()
                    {
                        Id = tenantHierarchy.Parent.Id,
                        HierarchyLevel = ElasticHierarchyLevel.Tenant
                    },
                    Children = tenantHierarchy.Parent.Children.Select(x => new Child
                    {
                        Data = new()
                        {
                            Id = x.Id,
                            HierarchyLevel = (ElasticHierarchyLevel)x.HierarchyLevel
                        },
                        Label = x.Name,
                        Children = x.Children.Select(x => new Child
                        {
                            Data = new()
                            {
                                Id = x.Id,
                                HierarchyLevel = (ElasticHierarchyLevel)x.HierarchyLevel
                            },
                            Label = x.Name,
                            Children = x.Children.Select(x => new Child
                            {
                                Data = new()
                                {
                                    Id = x.Id,
                                    HierarchyLevel = (ElasticHierarchyLevel)x.HierarchyLevel
                                },
                                Label = x.Name,
                                Children = x.Children.Select(x => new Child
                                {
                                    Data = new()
                                    {
                                        Id = x.Id,
                                        HierarchyLevel = (ElasticHierarchyLevel)x.HierarchyLevel
                                    },
                                    Label = x.Name,
                                    Children = x.Children.Select(x => new Child
                                    {
                                        Data = new()
                                        {
                                            Id = x.Id,
                                            HierarchyLevel = (ElasticHierarchyLevel)x.HierarchyLevel
                                        },
                                        Label = x.Name,
                                        Children = x.Children.Select(x => new Child
                                        {
                                            Data = new()
                                            {
                                                Id = x.Id,
                                                HierarchyLevel = (ElasticHierarchyLevel)x.HierarchyLevel
                                            },
                                            Label = x.Name,
                                            Children = null,

                                        }).ToList(),
                                    }).ToList(),
                                }).ToList(),
                            }).ToList(),
                        }).ToList(),
                    }).ToList()
                }
            };

            await base.AddAsync(mapedData);
        }

        public async Task AddCompanyAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
            {
                await base.AddAsync(new TenantHierarchyElasticData(tenantElasticDto.Id)
                {
                    Parent = new Parent
                    {
                        Label = tenantElasticDto.Name,
                        Data = new()
                        {
                            Id = tenantElasticDto.Id,
                            HierarchyLevel = ElasticHierarchyLevel.Tenant
                        },
                        Children = new()
                        {
                            new Child
                            {
                                Children = null,
                                Data = new()
                                {
                                    Id = companyElasticDto.Id,
                                    HierarchyLevel = ElasticHierarchyLevel.Company
                                },
                                Label = companyElasticDto.Name
                            }
                        }
                    }
                });
            }
            else
            {
                if (elasticData.Parent.Children == null)
                    elasticData.Parent.Children = new();

                elasticData.Parent.Children.Add(new Child
                {
                    Label = companyElasticDto.Name,
                    Data = new()
                    {
                        Id = companyElasticDto.Id,
                        HierarchyLevel = ElasticHierarchyLevel.Company
                    },
                    Children = null
                });

                await base.UpdateAsync(elasticData);

                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
        }

        public async Task UpdateCompanyAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id).Label = companyElasticDto.Name;
                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
        }

        public async Task DeleteComapnyAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            var child = elasticData.Parent.Children.FirstOrDefault(u => u.Data.Id == companyElasticDto.Id);

            if (child != null)
            {
                elasticData.Parent.Children.Remove(child);
                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
        }

        public async Task AddCountryAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Tenant hierarchy not found with id: {tenantElasticDto.Id}"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    companyChild.Children = new();

                companyChild.Children.Add(new Child
                {
                    Label = countryElasticDto.Name,
                    Data = new()
                    {
                        Id = countryElasticDto.Id,
                        HierarchyLevel = ElasticHierarchyLevel.Country
                    }
                });

                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task UpdateCountryAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null && !companyChild.Children.Exists(u => u.Data.Id == countryElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                companyChild.Children.First(u => u.Data.Id == countryElasticDto.Id).Label = countryElasticDto.Name;

                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task DeleteCountryAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var countryChild = companyChild.Children.FirstOrDefault(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild != null)
                {
                    companyChild.Children.Remove(countryChild);
                    await base.UpdateAsync(elasticData);
                    _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
                }
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task AddRegionAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Tenant hierarchy not found with id: {tenantElasticDto.Id}"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));

                if (companyChild.Children.Exists(u => u.Data.Id == countryElasticDto.Id))
                {
                    var countryChild = companyChild.Children.First(u => u.Data.Id == countryElasticDto.Id);

                    if (countryChild.Children == null)
                        countryChild.Children = new();

                    countryChild.Children.Add(new Child
                    {
                        Label = regionElasticDto.Name,
                        Data = new()
                        {
                            HierarchyLevel = ElasticHierarchyLevel.Region,
                            Id = regionElasticDto.Id
                        },
                    });
                    await base.UpdateAsync(elasticData);
                    _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
                }
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Tenant hierarchy not found with id: {tenantElasticDto.Id}"));
        }

        public async Task UpdateRegionAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null && !companyChild.Children.Exists(u => u.Data.Id == countryElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var countryChild = companyChild.Children.First(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null && !countryChild.Children.Exists(u => u.Data.Id == regionElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                countryChild.Children.First(u => u.Data.Id == regionElasticDto.Id).Label = regionElasticDto.Name;

                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task DeleteRegionAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var countryChild = companyChild.Children.FirstOrDefault(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {countryElasticDto.Id}"));

                var regionChild = countryChild.Children.FirstOrDefault(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild != null)
                {
                    countryChild.Children.Remove(regionChild);
                    await base.UpdateAsync(elasticData);
                    _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
                }
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task AddAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Tenant hierarchy not found with id: {tenantElasticDto.Id}"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));

                var countryChild = companyChild.Children.First(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var regionChild = countryChild.Children.First(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                if (regionChild.Children == null)
                    regionChild.Children = new();

                regionChild.Children.Add(new Child
                {
                    Label = areaElasticDto.Name,
                    Data = new()
                    {
                        HierarchyLevel = ElasticHierarchyLevel.Area,
                        Id = areaElasticDto.Id
                    }
                });

                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Tenant hierarchy not found with id: {tenantElasticDto.Id}"));
        }

        public async Task UpdateAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null && !companyChild.Children.Exists(u => u.Data.Id == countryElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var countryChild = companyChild.Children.First(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null && !countryChild.Children.Exists(u => u.Data.Id == regionElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                var regionChild = countryChild.Children.First(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild.Children == null && !regionChild.Children.Exists(u => u.Data.Id == areaElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Area hierarchy not found with id: {regionElasticDto.Id}"));

                regionChild.Children.First(u => u.Data.Id == areaElasticDto.Id).Label = areaElasticDto.Name;

                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task DeleteAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var countryChild = companyChild.Children.FirstOrDefault(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                var regionChild = countryChild.Children.FirstOrDefault(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Area hierarchy not found with id: {areaElasticDto.Id}"));


                var areaChild = regionChild.Children.FirstOrDefault(u => u.Data.Id == areaElasticDto.Id);

                if (areaChild != null)
                {
                    regionChild.Children.Remove(areaChild);
                    await base.UpdateAsync(elasticData);
                    _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
                }
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task AddSubAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Tenant hierarchy not found with id: {tenantElasticDto.Id}"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));

                var countryChild = companyChild.Children.First(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var regionChild = countryChild.Children.First(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                var areaChild = regionChild.Children.First(u => u.Data.Id == areaElasticDto.Id);

                if (areaChild == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Area hierarchy not found with id: {areaElasticDto.Id}"));


                if (areaChild.Children == null)
                    areaChild.Children = new();

                areaChild.Children.Add(new Child
                {
                    Label = subAreaElasticDto.Name,
                    Data = new()
                    {
                        HierarchyLevel = ElasticHierarchyLevel.SubArea,
                        Id = subAreaElasticDto.Id
                    }
                });

                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Tenant hierarchy not found with id: {tenantElasticDto.Id}"));
        }

        public async Task UpdateSubAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null && !companyChild.Children.Exists(u => u.Data.Id == countryElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var countryChild = companyChild.Children.First(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null && !countryChild.Children.Exists(u => u.Data.Id == regionElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                var regionChild = countryChild.Children.First(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild.Children == null && !regionChild.Children.Exists(u => u.Data.Id == areaElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Area hierarchy not found with id: {regionElasticDto.Id}"));

                var areaChild = regionChild.Children.First(u => u.Data.Id == areaElasticDto.Id);

                if (areaChild.Children == null && !areaChild.Children.Exists(u => u.Data.Id == subAreaElasticDto.Id))
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"SubArea hierarchy not found with id: {subAreaElasticDto.Id}"));

                areaChild.Children.First(u => u.Data.Id == subAreaElasticDto.Id).Label = subAreaElasticDto.Name;

                await base.UpdateAsync(elasticData);
                _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task DeleteSubAreaAsync(TenantElasticDto tenantElasticDto, CompanyElasticDto companyElasticDto, CountryElasticDto countryElasticDto, RegionElasticDto regionElasticDto, AreaElasticDto areaElasticDto, SubAreaElasticDto subAreaElasticDto)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

            if (elasticData == null)
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception("Tenant hierarchy not found"));

            if (elasticData.Parent.Children.Exists(u => u.Data.Id == companyElasticDto.Id))
            {
                var companyChild = elasticData.Parent.Children.First(u => u.Data.Id == companyElasticDto.Id);

                if (companyChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Country hierarchy not found with id: {countryElasticDto.Id}"));

                var countryChild = companyChild.Children.FirstOrDefault(u => u.Data.Id == countryElasticDto.Id);

                if (countryChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Region hierarchy not found with id: {regionElasticDto.Id}"));

                var regionChild = countryChild.Children.FirstOrDefault(u => u.Data.Id == regionElasticDto.Id);

                if (regionChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Area hierarchy not found with id: {areaElasticDto.Id}"));

                var areaChild = regionChild.Children.FirstOrDefault(u => u.Data.Id == areaElasticDto.Id);

                if (areaChild.Children == null)
                    throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"SubArea hierarchy not found with id: {areaElasticDto.Id}"));


                var subAreaChild = areaChild.Children.FirstOrDefault(u => u.Data.Id == subAreaElasticDto.Id);

                if (subAreaChild != null)
                {
                    areaChild.Children.Remove(subAreaChild);
                    await base.UpdateAsync(elasticData);
                    _queueService.Send(Queues.CountwareTrafficElasticHierarchyChanged, new ElasticHierarchyChanged(tenantElasticDto.Id));
                }
            }
            else
                throw new ElasticSearchQueryException(_index, 404, "not found", new Exception($"Company hierarchy not found with id: {companyElasticDto.Id}"));
        }

        public async Task<string> GetTenantHierarchyJsonFormatAsync(Guid tenantId)
        {
            var elasticData = await GetTenantHierarchyAsync(tenantId);
            return Newtonsoft.Json.JsonConvert.SerializeObject(elasticData);
        }

        //in memory sonra yeniden dusunuruz not Mahmud Yahyayev
        //public async Task<TenantHierarchyDto> GetByCompanyAsync(GridFilter gridFilter, TenantElasticDto tenantElasticDto)
        //{
        //    var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

        //    TenantHierarchyDto result  = new TenantHierarchyDto
        //    {
        //        Parent = new Application.Parent
        //        {
        //            Id = elasticData.Parent.Id,
        //            Data = new Application.Data
        //            {
        //                Name = elasticData.Parent.Data.Name,
        //                HierarchyLevel = (HierarchyLevel)Enum.Parse(typeof(HierarchyLevel), elasticData.Parent.Data.HierarchyLevel.ToString())
        //            }
        //        }
        //    };

        //    switch (gridFilter.Operator)
        //    {
        //        case FilterEnum.Eq:
        //            elasticData.Parent.Children.Where(u => u.Data.Name == gridFilter.Value).Select(u => new Application.Child
        //            {
        //                Id = u.Id,
        //                Data = new Application.Data
        //                {
        //                    HierarchyLevel = (HierarchyLevel)Enum.Parse(typeof(HierarchyLevel), u.Data.HierarchyLevel.ToString()),
        //                    Name = u.Data.Name
        //                }
        //            });
        //            break;


        //        case FilterEnum.Neq:
        //            elasticData.Parent.Children.Where(u => u.Data.Name != gridFilter.Value).Select(u => new Application.Child
        //            {
        //                Id = u.Id,
        //                Data = new Application.Data
        //                {
        //                    HierarchyLevel = (HierarchyLevel)Enum.Parse(typeof(HierarchyLevel), u.Data.HierarchyLevel.ToString()),
        //                    Name = u.Data.Name
        //                }
        //            });
        //            break;


        //        case FilterEnum.StartsWith:
        //            elasticData.Parent.Children.Where(u => u.Data.Name.StartsWith(gridFilter.Value)).Select(u => new Application.Child
        //            {
        //                Id = u.Id,
        //                Data = new Application.Data
        //                {
        //                    HierarchyLevel = (HierarchyLevel)Enum.Parse(typeof(HierarchyLevel), u.Data.HierarchyLevel.ToString()),
        //                    Name = u.Data.Name
        //                }
        //            });


        //            break;
        //        case FilterEnum.Contains:
        //            elasticData.Parent.Children.Where(u => u.Data.Name.Contains(gridFilter.Value)).Select(u => new Application.Child
        //            {
        //                Id = u.Id,
        //                Data = new Application.Data
        //                {
        //                    HierarchyLevel = (HierarchyLevel)Enum.Parse(typeof(HierarchyLevel), u.Data.HierarchyLevel.ToString()),
        //                    Name = u.Data.Name
        //                }
        //            });
        //            break;


        //        case FilterEnum.EndsWith:
        //            elasticData.Parent.Children.Where(u => u.Data.Name.EndsWith(gridFilter.Value)).Select(u => new Application.Child
        //            {
        //                Id = u.Id,
        //                Data = new Application.Data
        //                {
        //                    HierarchyLevel = (HierarchyLevel)Enum.Parse(typeof(HierarchyLevel), u.Data.HierarchyLevel.ToString()),
        //                    Name = u.Data.Name
        //                }
        //            });
        //            break;
        //        default:
        //            break;
        //    }
        //    return result;
        //}

        //public async Task<TenantHierarchyDto> GetByCountriesAsync(GridFilter gridFilter, TenantElasticDto tenantElasticDto)
        //{
        //    var elasticData = await GetTenantHierarchyAsync(tenantElasticDto.Id);

        //    TenantHierarchyDto result = new TenantHierarchyDto
        //    {
        //        Parent = new Application.Parent
        //        {
        //            Id = elasticData.Parent.Id,
        //            Data = new Application.Data
        //            {
        //                Name = elasticData.Parent.Data.Name,
        //                HierarchyLevel = (HierarchyLevel)Enum.Parse(typeof(HierarchyLevel), elasticData.Parent.Data.HierarchyLevel.ToString())
        //            }
        //        }
        //    };

        //    switch (gridFilter.Operator)
        //    {
        //        case FilterEnum.Eq:
        //            break;
        //        case FilterEnum.Neq:
        //            break;
        //        case FilterEnum.StartsWith:
        //            break;
        //        case FilterEnum.Contains:
        //            break;
        //        case FilterEnum.EndsWith:
        //            break;
        //        default:
        //            break;
        //    }
        //}
    }
}