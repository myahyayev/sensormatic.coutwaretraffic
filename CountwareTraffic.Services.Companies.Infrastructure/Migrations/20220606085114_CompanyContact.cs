﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class CompanyContact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Contact_FaxNumber",
                schema: "areas",
                table: "Companies",
                newName: "Contact_PhoneNumberSecondary");

            migrationBuilder.AddColumn<string>(
                name: "Contact_EmailAddressSecondary",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(120)",
                maxLength: 120,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_GsmDialCode",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(6)",
                maxLength: 6,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_GsmDialCodeSecondary",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(6)",
                maxLength: 6,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_GsmNumberSecondary",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_PersonName",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(120)",
                maxLength: 120,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_PersonNameSecondary",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(120)",
                maxLength: 120,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_PhoneDialCode",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(6)",
                maxLength: 6,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_PhoneDialCodeSecondary",
                schema: "areas",
                table: "Companies",
                type: "nvarchar(6)",
                maxLength: 6,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Contact_EmailAddressSecondary",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_GsmDialCode",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_GsmDialCodeSecondary",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_GsmNumberSecondary",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_PersonName",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_PersonNameSecondary",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_PhoneDialCode",
                schema: "areas",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Contact_PhoneDialCodeSecondary",
                schema: "areas",
                table: "Companies");

            migrationBuilder.RenameColumn(
                name: "Contact_PhoneNumberSecondary",
                schema: "areas",
                table: "Companies",
                newName: "Contact_FaxNumber");
        }
    }
}
