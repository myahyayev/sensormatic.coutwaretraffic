﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace CountwareTraffic.Services.Users.Grpc
{
    [ServiceLog]
    public sealed partial class ChangePasswordRequest : SensormaticRequestValidate
    {
        public override void Validate()
        {
            if (UserName.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(UserName)));

            if (CurrentPassword.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(CurrentPassword)));

            if (NewPassword.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(NewPassword)));

            if (ConfirmPassword.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(ConfirmPassword)));

            if (!CurrentPassword.IsNullOrWhiteSpace() && !NewPassword.IsNullOrWhiteSpace() && object.Equals(CurrentPassword, NewPassword))
            {
                ValidateResults.Add(new ErrorResult($"New password cannot be the same as the current password"));

                return;
            }

            if ((!NewPassword.IsNullOrWhiteSpace() && !NewPassword.IsValidPassword()) || (!ConfirmPassword.IsNullOrWhiteSpace() && !ConfirmPassword.IsValidPassword()))
            {
                ValidateResults.Add(new ErrorResult($"Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)"));

                return;
            }

            if (!NewPassword.IsNullOrWhiteSpace() && !ConfirmPassword.IsNullOrWhiteSpace() && !object.Equals(NewPassword, ConfirmPassword))
                ValidateResults.Add(new ErrorResult($"The Password didn't match."));
        }
    }
}
