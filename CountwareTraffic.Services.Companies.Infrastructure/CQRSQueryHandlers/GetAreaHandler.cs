﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetAreaHandler : IQueryHandler<GetArea, AreaDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;

        public GetAreaHandler(IUnitOfWork unitOfWork,
            ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<AreaDetailsDto> HandleAsync(GetArea query)
        {
            var area = await _unitOfWork.GetRepository<IAreaRepository>()
                .GetAsync(query.AreaId, _currentTenant.Id);

            if (area == null)
                throw new AreaNotFoundException(query.AreaId);

            var region = await _unitOfWork.GetRepository<IRegionRepository>().GetByIdAsync(area.RegionId);

            var country = await _unitOfWork.GetRepository<ICountryRepository>().GetByIdAsync(region.CountryId);

            return new AreaDetailsDto
            {
                Id = area.Id,
                Name = area.Name,
                Description = area.Description,
                AuditCreateBy = area.AuditCreateBy,
                AuditCreateDate = area.AuditCreateDate,
                AuditModifiedBy = area.AuditModifiedBy,
                AuditModifiedDate = area.AuditModifiedDate,
                EmailAddress = area.Contact?.EmailAddress,
                GsmNumber = area.Contact?.GsmNumber,
                PhoneNumber = area.Contact?.PhoneNumber,
                GsmCountryCode = area.Contact?.GsmCountryCode,
                GsmDialCode = area.Contact?.GsmDialCode,
                ManagerId = area.ManagerId,
                ManagerName = area.ManagerName,
                PhoneCountryCode = area.Contact?.PhoneCountryCode,
                PhoneDialCode = area.Contact?.PhoneDialCode,
                Latitude = area.Address?.Location?.Y,
                Longitude = area.Address?.Location?.X,
                Street = area.Address?.Street,
                AreaTypeName = area.AreaType.Name,
                AreaTypeId = area.AreaType.Id,
                DistrictId = area.DistrictId,
                CityId = area.CityId,
                RegionId = area.RegionId,
                CountryLookupId = country.CountryLookupId,
                WorkingHoursStart = area.WorkingHoursStart,
                WorkingHoursEnd = area.WorkingHoursEnd,
                WorkingTimeZone = area.WorkingTimeZone
            };
        }
    }
}
