﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Events.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application.CQRSEvents.External.Handlers
{
    public class AreaCreatedHandler : IEventHandler<AreaCreated>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public AreaCreatedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(AreaCreated @event)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            if (!await areaRepository.ExistsAsync(@event.AreaId))
            {
                try
                {
                    var area = Area.Create(@event.AreaId, @event.AreaName, @event.WorkingHoursStart, @event.WorkingHoursEnd, @event.WorkingTimeZone, @event.TenantId);

                    await areaRepository.AddAsync(area);

                    await _unitOfWork.CommitAsync();

                    _queueService.Publish(new AreaCreatedCompleted(@event.AreaId, @event.AreaName, @event.UserId, "", @event.TenantId));
                }
                catch (Exception ex)
                {
                    _queueService.Publish(new AreaCreatedRejected(@event.AreaId, @event.AreaName, @event.UserId, "", @event.TenantId));
                }
            }
        }
    }
}

