﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class RemovePermissionRoleHandler : IQueryHandler<RemovePermissionRole, IdentityRemovePermissionRoleResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public RemovePermissionRoleHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityRemovePermissionRoleResponse> HandleAsync(RemovePermissionRole query)
        {
            return await _identityServerService.RemovePermissionRoleAsync(new IdentityRemovePermissionRoleRequest
            {
                Id = query.Id,
                PermissionId = query.PermissionId,
                RoleId = query.RoleId
            });
        }
    }
}
