﻿namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityRefreshTokenResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
