﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetGroupSelectHandler : IQueryHandler<GetGroupSelect, IdentityGroupSelectResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetGroupSelectHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityGroupSelectResponse> HandleAsync(GetGroupSelect query)
        {
            return await _identityServerService.GetGroupSelectAsync();
        }
    }
}
