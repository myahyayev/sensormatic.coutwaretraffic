﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Reporting.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class DeleteReportHandler : ICommandHandler<DeleteReport>
    {
        private readonly IUnitOfWork _unitOfWork;
        public DeleteReportHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task HandleAsync(DeleteReport command)
        {
            var reportRepository = _unitOfWork.GetRepository<IReportRepository>();

            var report = await reportRepository.GetAsync(command.ReportId);


            if (report is null)
                throw new ReportNotFoundException(command.ReportId);

            reportRepository.Remove(report);

            var scheduleFrequency = await _unitOfWork.GetRepository<IReportScheduleRepository>().GetScheduleFrequencyAsync(command.ReportId);

            if (scheduleFrequency.Id == ScheduleFrequency.Repeat.Id)
            {
                report.WhenDeleted();
            }

            var reportScheduleRepository = _unitOfWork.GetRepository<IReportScheduleRepository>();

            var reportSchedule = await reportScheduleRepository.GetByReportIdAsync(command.ReportId);

            if (reportSchedule is not null)
                reportScheduleRepository.Remove(reportSchedule);

            await _unitOfWork.CommitAsync();
        }
    }
}
