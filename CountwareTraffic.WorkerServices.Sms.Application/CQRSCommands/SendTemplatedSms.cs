﻿using Convey.CQRS.Commands;
using Sensormatic.Tool.Common;
using System;

namespace CountwareTraffic.WorkerServices.Sms.Application
{
    [Contract]
    public class SendTemplatedSms : ICommand
    {
        public ISmsTemplate Template { get; set; }
        public Guid TenantId { get; set; }
    }
}
