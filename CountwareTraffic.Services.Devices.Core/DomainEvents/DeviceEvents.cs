﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Devices.Core
{
    public class DeviceChanged : IDomainEvent
    {
        public Guid DeviceId { get; init; }
        public string Name { get; init; }
        public string OldName { get; init; }
        public Guid RecordId { get; init; }
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
        public bool IsActive { get; set; }
        public int ControlFrequency { get; set; }

        public DeviceChanged(Guid deviceId, string name, string macAddress,  string ipAddress, bool isActive, string oldName,  int controlFrequency)
        {
            DeviceId = deviceId;
            Name = name;
            OldName = oldName;
            RecordId = Guid.NewGuid();
            IpAddress = ipAddress;
            MacAddress = macAddress;
            IsActive = isActive;
            ControlFrequency = controlFrequency;
        }
    }

    public class DeviceDeleted : IDomainEvent
    {
        public Guid DeviceId { get; init; }
        public Guid RecordId { get; init; }
        public string Name { get; init; }
        public DeviceDeleted(Guid deviceId, string name)
        {
            DeviceId = deviceId;
            RecordId = Guid.NewGuid();
            Name = name;
        }
    }

    public class DeviceCreated : IDomainEvent
    {
        public Guid DeviceId { get; init; }
        public string Name { get; init; }
        public string MacAddress { get; init; }
        public string IpAddress { get; init; }
        public Guid SubAreaId { get; init; }
        public string SubAreaName { get; init; }
        public Guid RecordId { get; init; }
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid RegionId { get; init; }
        public string RegionName { get; init; }
        public Guid CountryId { get; init; }
        public string CountryName { get; init; }
        public Guid CompanyId { get; init; }
        public string CompanyName { get; init; }
        public Guid CountryLookupId { get; init; }
        public Guid CityId { get; init; }
        public string CityName { get; init; }
        public Guid DistrictId { get; init; }
        public string DistrictName { get; init; }
        public int ControlFrequency { get; set; }

        public DeviceCreated(Guid deviceId, string name, string macAddress, string ipAddress, Guid subAreaId, string subAreaName, Guid areaId, string areaName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid countryLookupId, Guid cityId, string cityName, Guid districtId, string districtName, int controlFrequency)
        {
            DeviceId = deviceId;
            Name = name;
            MacAddress = macAddress;
            IpAddress = ipAddress;
            SubAreaId = subAreaId;
            SubAreaName = subAreaName;
            RecordId = Guid.NewGuid();
            AreaId = areaId;
            AreaName = areaName;
            RegionId = regionId;
            RegionName = regionName;
            CountryId = countryId;
            CountryName = countryName;
            CompanyId = companyId;
            CompanyName = companyName;
            CountryLookupId = countryLookupId;
            CityId = cityId;
            CityName = cityName;
            DistrictId = districtId;
            DistrictName = districtName;
            ControlFrequency = controlFrequency;
        }
    }

    public class DeviceHierarchyBase
    {
        public Guid DeviceId { get; set; }
        public string DeviceName { get; set; }
        public Guid SubAreaId { get; set; }
        public Guid AreaId { get; set; }
        public Guid RegionId { get; set; }
        public Guid CountryId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid TenantId { get; set; }
        public Guid RecordId { get; init; }

        public DeviceHierarchyBase(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, Guid deviceId, string deviceName)
        {
            DeviceId = deviceId;
            DeviceName = deviceName;
            SubAreaId = subAreaId;
            AreaId = areaId;
            RegionId = regionId;
            CompanyId = companyId;
            RecordId = Guid.NewGuid();
            TenantId = tenantId;
            CountryId = countryId;
        }
    }

    public class DeviceHierarchyUpdated : DeviceHierarchyBase, IDomainEvent
    {
        public DeviceHierarchyUpdated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, Guid deviceId, string deviceName)
            : base(tenantId, companyId, countryId, regionId, areaId, subAreaId, deviceId, deviceName)
        {
        }
    }

    public class DeviceHierarchyDeleted : DeviceHierarchyBase, IDomainEvent
    {
        public DeviceHierarchyDeleted(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, Guid deviceId, string deviceName)
            : base(tenantId, companyId, countryId, regionId, areaId, subAreaId, deviceId, deviceName)
        {
        }
    }

    public class DeviceHierarchyCreated : DeviceHierarchyBase, IDomainEvent
    {
        public DeviceHierarchyCreated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, Guid deviceId, string deviceName)
           : base(tenantId, companyId, countryId, regionId, areaId, subAreaId, deviceId, deviceName)
        {
        }
    }
}
