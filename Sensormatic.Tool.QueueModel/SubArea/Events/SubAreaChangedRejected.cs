﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record SubAreaChangedRejected : MessageEnvelope, IQueueEvent
    {
        public SubAreaChangedRejected(Guid subAreaId, string name, string oldName, Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            SubAreaId = subAreaId;
            Name = name;
            OldName = oldName;
        }
        public Guid SubAreaId { get; }
        public string Name { get; }
        public string OldName { get; }
        public Guid RecordId { get; init; }
    }
}
