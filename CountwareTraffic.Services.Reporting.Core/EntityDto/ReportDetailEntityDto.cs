﻿namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportDetailEntityDto : ReportEntityDto
    {
        public ReportScheduleEntityDto ReportSchedule { get; set; }
    }
}
