﻿using Sensormatic.Tool.Ioc;
using System;

namespace CountwareTraffic.Services.Settings.Application
{
    public interface IIdentityService : ITransientDependency
    {
         Guid UserId { get; }
    }
}
