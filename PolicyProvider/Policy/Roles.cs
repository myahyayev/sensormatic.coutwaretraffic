﻿namespace Countware.Traffic.CrossCC.PolicyProvider
{
    public class Roles
    {
        public const string SuperAdmin = "super_admin";
        public const string Admin = "admin";
        public const string ClientAdmin = "client_admin";
        public const string User = "user";
    }
}
