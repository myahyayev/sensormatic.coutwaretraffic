﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetDeviceBrandModelDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid BrandId { get; set; }
    }
}
