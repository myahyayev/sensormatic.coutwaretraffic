﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using Sensormatic.Tool.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class GetDevicesHandler : IQueryHandler<GetDevices, PagingResult<DeviceDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GetDevicesHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task<PagingResult<DeviceDetailsDto>> HandleAsync(GetDevices query)
        {
            int page = query.PagingQuery.Page;
            int limit = query.PagingQuery.Limit;

            var qres = await _unitOfWork.GetRepository<IDeviceRepository>()
                                        .GetAllAsync(query.SubAreaId, _currentTenant.Id, query.PagingQuery, query.Filters, query.Sorts);

            if (qres == null)
                return PagingResult<DeviceDetailsDto>.Empty;

            return new PagingResult<DeviceDetailsDto>(qres.Entities.Select(device => new DeviceDetailsDto
            {
                Id = device.Id,
                Name = device.Name,
                Description = device.Description,
                AuditCreateBy = device.AuditCreateBy,
                AuditCreateDate = device.AuditCreateDate,
                AuditModifiedBy = device.AuditModifiedBy,
                AuditModifiedDate = device.AuditModifiedDate,
                DeviceTypeName = device.DeviceType.Name,
                DeviceStatusName = device.DeviceStatus.Name,
                BrandId = device.BrandId,
                BrandName = device.BrandName,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.FirmWare,
                IsActive = device.IsActive,
                ModelId = device.ModelId,
                 Note = device.Note,
                ModelName = device.ModelName,
                SubAreaId = device.SubAreaId,
                DeviceStatusId = device.DeviceStatus.Id,
                DeviceTypeId = device.DeviceType.Id,
                Identity = device.ConnectionInfo?.Identity,
                IpAddress = device.ConnectionInfo?.IpAddress,
                MacAddress = device.ConnectionInfo?.MacAddress,
                Password = device.ConnectionInfo?.Password,
                Port = device.ConnectionInfo == null ? 0 : device.ConnectionInfo.Port,
                UniqueId = device.ConnectionInfo?.UniqueId
            }), qres.Total, page, limit, qres.Total > (limit * (page - 1)) + qres.Entities.Count);
        }
    }
}
