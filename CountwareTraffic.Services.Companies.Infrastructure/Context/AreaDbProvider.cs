﻿using Convey.CQRS.Commands;
using Countware.Traffic.CrossCC.AuditLog;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class AreaDbProvider : InterceptorDefination, IScopedSelfDependency
    {
        private readonly ICollection<Audit> _audits;
        private readonly ICollection<IDomainEventRaisable> _domainEventRaisableList;
        private readonly IQueueService _queueService;
        private readonly IQueueEventMapper _queueEventMapper;
        private readonly ICurrentTenant _currentTenant;
        private readonly IIdentityService _identityService;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly ICorrelationService _correlationService;
        public AreaDbProvider(IQueueEventMapper queueEventMapper, ICurrentTenant currentTenant, IIdentityService identityService, ICorrelationService correlationService, IQueueService queueService, ICommandDispatcher commandDispatcher)
        {
            _queueService = queueService;
            _queueEventMapper = queueEventMapper;
            _currentTenant = currentTenant;
            _identityService = identityService;
            _correlationService = correlationService;
            _audits = new List<Audit>();
            _domainEventRaisableList = new List<IDomainEventRaisable>();
            _commandDispatcher = commandDispatcher;

            #region Auditable
            OnAuditBeforeDelete += (item, entityEntry, dbContext) => { _audits.Add(AuditTrailHelper.AuditTrailFactory(dbContext, entityEntry)); };
            OnAuditBeforeInsert += (item, entityEntry, dbContext) => { _audits.Add(AuditTrailHelper.AuditTrailFactory(dbContext, entityEntry)); };
            OnAuditBeforeUpdate += (item, entityEntry, dbContext) => { _audits.Add(AuditTrailHelper.AuditTrailFactory(dbContext, entityEntry)); };

            OnAuditAfterError += (exMessage) => { _audits.Clear(); };

            OnAuditAfterSuccess += () =>
            {
                var auditList = new AuditList<Audit>(_audits.ToList(), UserId, _correlationService.CorrelationId, TenantId);
                _queueService.Send(Queues.CountwareTrafficAudit, auditList);
                _audits.Clear();
            };
            #endregion Auditable


            #region Domain event Raisable
            OnDomainEventRaiseDelete += (item, entityEntry, dbContext) => { _domainEventRaisableList.Add(item); };
            OnDomainEventRaiseInsert += (item, entityEntry, dbContext) => { _domainEventRaisableList.Add(item); };
            OnDomainEventRaiseUpdate += (item, entityEntry, dbContext) => { _domainEventRaisableList.Add(item); };

            OnDomainEventRaiseAfterError += (exMessage) => { _domainEventRaisableList.Clear(); };
            OnDomainEventRaiseAfterSuccess += () => {
                foreach (var raisable in _domainEventRaisableList)
                {
                    var queueEvents = _queueEventMapper.MapAll(raisable.Events, UserId, CorrelationId, TenantId);

                    foreach (var queueEvent in queueEvents)
                    {
                        if (queueEvent != null)
                        {
                            string message = null;

                            try
                            {
                                _queueService.Publish(queueEvent);
                            }
                            catch (Exception ex) { message = ex.Message ?? ex.InnerException?.Message; }
                            finally
                            {
                                _commandDispatcher.SendAsync(new ProcessOutbox
                                {
                                    Exception = message,
                                    RecordId = queueEvent.RecordId
                                });
                            }
                        }
                    }

                    raisable.ClearEvents();
                }

                _domainEventRaisableList.Clear();
            };
            #endregion Domain event Raisable
        }

        public override Guid TenantId => _currentTenant.Id;

        public override Guid UserId => _identityService.UserId;

        public override string CorrelationId => _correlationService.CorrelationId;

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
