﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetSubAreaKeyValueHandler : IQueryHandler<GetSubAreaKeyValue, List<KeyValueDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetSubAreaKeyValueHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<KeyValueDto>> HandleAsync(GetSubAreaKeyValue query)
        {
            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            var keyValues = await subAreaRepository.GetAllKeyValueAsync(query.ParentId);

            return keyValues.Select(u => new KeyValueDto
            {
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }
}


