﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CreateSubAreaHandler : ICommandHandler<CreateSubArea>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public CreateSubAreaHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }
        

        public async Task HandleAsync(CreateSubArea command)
        {
            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var districtLookupRepository = _unitOfWork.GetRepository<IDistrictLookupRepository>();

            var area = await areaRepository.GetAsync(command.AreaId, _currentTenant.Id);

            if (area == null)
                throw new AreaNotFoundException(command.AreaId);

            if ((await subAreaRepository.ExistsAsync(command.Name, _currentTenant.Id )))
                throw new SubAreaAlreadyExistsException(command.Name);

            var subArea = SubArea.Create(command.Name, command.Description, command.AreaId);

            await subAreaRepository.AddAsync(subArea);

            var hierarchy = await areaRepository.GetHierarchyAsync(subArea.AreaId, _currentTenant.Id);

            var districthierarchy  = await districtLookupRepository.GetDistrictHierarchy(area.DistrictId);

            subArea.WhenCreated(subArea.Id, subArea.Name, area.Id, area.Name, hierarchy.RegionId, hierarchy.RegionName, hierarchy.CountryId, hierarchy.CountryName, hierarchy.CopmanyId, hierarchy.CopmanyName, districthierarchy.CountryId, districthierarchy.CityId, districthierarchy.CityName, districthierarchy.DistrictId, districthierarchy.DistrictName);

            subArea.WhenHierarchyCreated(_currentTenant.Id, hierarchy.CopmanyId, hierarchy.CountryId, hierarchy.RegionId, hierarchy.AreaId, subArea.Id, subArea.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
