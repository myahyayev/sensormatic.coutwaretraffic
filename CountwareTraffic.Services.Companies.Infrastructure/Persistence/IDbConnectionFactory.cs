﻿using System.Data;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateDbConnection(string connectionName);
    }
}
