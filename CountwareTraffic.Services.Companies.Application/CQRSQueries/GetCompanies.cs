﻿using Convey.CQRS.Queries;
using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetCompanies : IQuery<PagingResult<CompanyDetailsDto>>
    {
        public PagingQuery PagingQuery { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
    }
}
