﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Application
{
    public class GetTrafficByTimePeriod : IQuery<TrafficByTimePeriodDto>
    {
        public List<NodeFilter> NodeFilters { get; set; }
        public DateTime CurrentStartDate { get; set; }
        public DateTime CurrentEndDate { get; set; }
        public DateTime PreviusStartDate { get; set; }
        public DateTime PreviusEndDate { get; set; }
        public TimePeriod TimePeriod { get; set; }
    }
}
