﻿namespace Sensormatic.Tool.Common
{
    public static class ElasticsearchKeys
    {
        public const int From = 0;
        public const int ZeroSize = 0;
        public const int MinSize = 1;
        public const int DefaultSize = 10;

        public static string CountwareTrafficEventsEvent = "countware_traffic_events_event";
        public static string CountwareTrafficTenantsHierarchy = "countware_traffic_tenants_hierarchy";
    }
}
