﻿namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer
{
    public class CreateAdminUserWithDefaultPasswordApiResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
