﻿using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application
{
    public class AuthorityNullModel
    {
        [JsonPropertyName("auth_token")]
        public string AuthToken { get; set; }
    }
}
