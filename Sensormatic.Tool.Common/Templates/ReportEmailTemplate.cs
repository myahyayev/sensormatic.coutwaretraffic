﻿using System;
using System.Collections.Generic;

namespace Sensormatic.Tool.Common
{
    public abstract class ReportEmailTemplate : IEmailTemplate, IEmailReceiver, IHasEmailSubject
    {
        public abstract string TemplateName {get;}
        public List<Guid> UserIds { get; set; }
        public string ReportHeader { get; set; }
        public string ReportEmailBody { get; set; }
        public string ReportUrl { get; set; }
        public string ReportCreationDate { get; set; }
        public string ReportName { get; set; }
        public string ReportDescription { get; set; }
        public string ReportFileFormat { get; set; }
        public string ReportDateRange { get; set; }
        public string ReportLocations { get; set; }
        public string ReportCreatedBy { get; set; }
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public List<string> Bc { get; set; }
        public string EmailSubject { get; set; }
    }
}
