﻿using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class AreaRepository : Repository<Area>, IAreaRepository
    {
        private readonly new AreaDbContext _context;
        private readonly ICurrentTenant _currentTenant;

        public AreaRepository(AreaDbContext context, ICurrentTenant currentTenant) : base(context)
        {
            _context = context;
            _currentTenant = currentTenant;
        }


        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible

        public async Task<Area> GetAsync(string name) => await base.GetQuery(_currentTenant.Id).SingleOrDefaultAsync(x => x.Name == name);

        public async Task<Area> GetAsync(Guid id, Guid tenantId) => await base.GetQuery(tenantId).Include(x => x.Address)
                                                                          .Include(x => x.Contact)
                                                                          .Include(x => x.AreaType)
                                                                          .SingleOrDefaultAsync(x => x.Id == id);
        
        public async Task<bool> ExistsAsync(string name) => await base.GetQuery(_currentTenant.Id).AnyAsync(u => u.Name == name);

        public async Task<bool> ExistsAsync(Guid id) => await base.GetQuery(_currentTenant.Id).AnyAsync(u => u.Id == id);

        public async Task<QueryablePagingValue<Area>> GetAllAsync(Guid regionId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts)
        {
            var query = base.GetQuery(_currentTenant.Id);

            query = query.ApplyFilter(filters);

            foreach (var filter in filters)
            {
                if (filter.Field.ToLower() == "phonenumber")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.Contact.PhoneNumber == filter.Value),
                        FilterEnum.Neq => query.Where(u => u.Contact.PhoneNumber != filter.Value),
                        FilterEnum.StartsWith => query.Where(u => u.Contact.PhoneNumber.StartsWith(filter.Value)),
                        FilterEnum.Contains => query.Where(u => u.Contact.PhoneNumber.Contains(filter.Value)),
                        FilterEnum.EndsWith => query.Where(u => u.Contact.PhoneNumber.EndsWith(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }
                if (filter.Field.ToLower() == "emailaddress")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.Contact.EmailAddress == filter.Value),
                        FilterEnum.Neq => query.Where(u => u.Contact.EmailAddress != filter.Value),
                        FilterEnum.StartsWith => query.Where(u => u.Contact.EmailAddress.StartsWith(filter.Value)),
                        FilterEnum.Contains => query.Where(u => u.Contact.EmailAddress.Contains(filter.Value)),
                        FilterEnum.EndsWith => query.Where(u => u.Contact.EmailAddress.EndsWith(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }

                if (filter.Field.ToLower() == "areatypeid")
                {
                    query = filter.Operator switch
                    {
                        FilterEnum.Eq => query.Where(u => u.AreaType.Id == Int32.Parse(filter.Value)),
                        FilterEnum.Neq => query.Where(u => u.AreaType.Id != Int32.Parse(filter.Value)),
                        _ => throw new NotImplementedException(),
                    };
                }
            }

            query = query.ApplyOrderBy(sorts);

            foreach (var sort in sorts)
            {
                if (sort.Field.ToLower() == "phonenumber")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.Contact.PhoneNumber),
                        Direction.Desc => query.OrderByDescending(u => u.Contact.PhoneNumber),
                        _ => throw new NotImplementedException(),
                    };
                }
                if (sort.Field.ToLower() == "emailaddress")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.Contact.EmailAddress),
                        Direction.Desc => query.OrderByDescending(u => u.Contact.EmailAddress),
                        _ => throw new NotImplementedException(),
                    };
                }

                if (sort.Field.ToLower() == "areatypeid")
                {
                    query = sort.Direction switch
                    {
                        Direction.Asc => query.OrderBy(u => u.AreaType.Id),
                        Direction.Desc => query.OrderByDescending(u => u.AreaType.Id),
                        _ => throw new NotImplementedException(),
                    };
                }
            }

            var total = await query.CountAsync(u => u.RegionId == regionId);

            if (total > 0)
            {
                query = query.Where(u => u.RegionId == regionId)
                                  .Include(x => x.Address)
                                  .Include(x => x.AreaType)
                                  .Include(x => x.Contact)
                                  .Skip((paging.Page - 1) * paging.Limit)
                                  .Take(paging.Limit);

                var result = await query.ToListAsync();

                return new QueryablePagingValue<Area>(result, total);
            }

            return null;
        }

        public async Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName)
        {
            var data = await GetHierarchyAsync(id, tenantId);

            var response = new List<Hierarchy>();

            response.Add(new Hierarchy() { Id = tenantId, Name = tenantName, HierarchyLevel = HierarchyLevel.Tenant });
            response.Add(new Hierarchy() { Id = data.CopmanyId, Name = data.CopmanyName, HierarchyLevel = HierarchyLevel.Company });
            response.Add(new Hierarchy() { Id = data.CountryId, Name = data.CountryName, HierarchyLevel = HierarchyLevel.Country });
            response.Add(new Hierarchy() { Id = data.RegionId, Name = data.RegionName, HierarchyLevel = HierarchyLevel.Region });
            response.Add(new Hierarchy() { Id = data.AreaId, Name = data.AreaName, HierarchyLevel = HierarchyLevel.Area });
            return response;
        }

        public async Task<HierarchyCteData> GetHierarchyAsync(Guid id, Guid tenantId)
        {
            var sql = @"WITH Companies (Id, Name) AS
                            (
                               SELECT
                                  Id,
                                  Name
                                  FROM [areas].[Companies] AS Companies WHERE AuditIsDeleted = 0 AND TenantId = @TenantId
                            ),

                            Countries (Id, CompanyId, Name) AS
                            (
                               SELECT
                                  Id,
                                  CompanyId,
                                  Name
	                              FROM [areas].[Countries] AS Countries  WHERE AuditIsDeleted = 0  AND TenantId = @TenantId
                            ),

                            Regions (Id, CountryId, Name) AS
                            (
                               SELECT
                                  Id,
                                  CountryId,
                                  Name
	                              FROM [areas].[Regions] AS Regions  WHERE  AuditIsDeleted = 0  AND TenantId = @TenantId
                            ),

                            Areas (Id, RegionId, Name) AS
                            (
                               SELECT
                                  Id,
                                  RegionId,
                                  Name
	                              FROM [areas].[Areas] AS Regions  WHERE Id = @Id AND AuditIsDeleted = 0  AND TenantId = @TenantId
                            )

                            SELECT 
	                            Companies.Id AS CopmanyId, 
	                            Companies.Name AS CopmanyName, 
	                            Countries.Id  AS CountryId, 
	                            Countries.Name AS CountryName,
	                            Regions.Id AS RegionId, 
	                            Regions.Name AS RegionName,
	                            Areas.Id  AS AreaId,
	                            Areas.Name AS AreaName
	                            FROM Areas
	                            INNER JOIN Regions      ON Areas.RegionId  = Regions.Id
                                INNER JOIN Countries  ON Regions.CountryId = Countries.Id
	                            INNER JOIN Companies  ON Countries.CompanyId = Companies.Id";

            var connection = _context.Database.GetDbConnection();

            var data = await connection.QueryFirstOrDefaultAsync<HierarchyCteData>(sql, new
            {
                @TenantId = tenantId,
                @Id = id
            });

            return data;
        }

        public async Task<List<KeyValue>> GetAllKeyValueAsync(Guid parentId)
        {
            var query = base.GetQuery(_currentTenant.Id);

            return await query.Where(u => u.RegionId == parentId).Select(x => new KeyValue { Id = x.Id, Name = x.Name }).ToListAsync();
        }

        /*
        public async Task<QueryablePagingValue<Area>> GetAllAsync(int page, int limit, Guid districtId)
        {
            var query = base.GetQuery(orderBy: x => x.OrderBy(y => y.Id));

            #region denemeler
            //Net core 3.1 ve sonrasi calismaz. Gereksiz yere gorup by yapmis oluruz.
            //var grouppedAreas = await query
            //                            .Include(x => x.Address)
            //                            .Include(x=>x.AreaType)
            //                            .Include(x => x.Contact)
            //                            .OrderBy(x => x.Id)
            //                            .Select(x => x)
            //                            .Skip((page - 1) * limit)
            //                            .Take(limit)
            //                            .GroupBy(x => new { Total = query.Count() })
            //                            .FirstOrDefaultAsync();

            //var tsqlQueryControl = query
            //                    .Include(x => x.Address)
            //                    .Include(x => x.AreaType)
            //                    .Include(x => x.Contact)
            //                    .OrderBy(x => x.Id)
            //                    .Select(x => new { Data = x, Total = query.Count() })
            //                    .Skip((page - 1) * limit)
            //                    .Take(limit);


            //string tsqlQuery = tsqlQueryControl.ToQueryString();


            //Buda kotu bir yontem her seferde sayar.
            //var result = await query
            //                        .Include(x => x.Address)
            //                        .Include(x => x.AreaType)
            //                        .Include(x => x.Contact)
            //                        .OrderBy(x => x.Id)
            //                        .Select(x => new { Data = x, Total = query.Count() })
            //                        .Skip((page - 1) * limit)
            //                        .Take(limit)
            //                        .ToListAsync();

            //.Net 5 EfCore 5 versiyonulari icin en optimize yontem iki defa veri tabanina gitmek gibi.
            #endregion

            var total = await query.CountAsync(x=>x.DistrictId == districtId);

            if (total > 0)
            {
                var result = await query
                                    .Where(u => u.DistrictId == districtId)
                                    .Include(x => x.Address)
                                    .Include(x => x.AreaType)
                                    .Include(x => x.Contact)
                                    .Skip((page - 1) * limit)
                                    .Take(limit)
                                    .ToListAsync();

                return new QueryablePagingValue<Area>(result, total);
            }
            return null;
        }
        */
    }
}
