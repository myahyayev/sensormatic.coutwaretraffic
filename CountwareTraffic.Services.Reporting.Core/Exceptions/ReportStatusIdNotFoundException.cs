﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportStatusIdNotFoundException : DomainException
    {
        IEnumerable<ReportStatus> ReportStatuses { get; }
        public int Id { get; }

        public ReportStatusIdNotFoundException(IEnumerable<ReportStatus> reportStatuses, int id)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ReportStatus Id: {String.Join(",", reportStatuses.Select(s => s.Id))}") }, 400, ResponseMessageType.Error)
        {
            ReportStatuses = reportStatuses;
            Id = id;
        }
    }
}
