﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class CrudApiResponse
    {
        public Guid Id { get; set; }
        public OperationType OperationType { get; set; }
        public bool IsSuccess { get; set; }
    }
    public enum OperationType
    {
        Create = 1,
        Update = 2,
        Delete = 3
    }
}
