﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetRolePaginationHandler : IQueryHandler<GetRolePagination, IdentityRolePaginationResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetRolePaginationHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityRolePaginationResponse> HandleAsync(GetRolePagination query)
        {
            return await _identityServerService.GetRolePaginationAsync(new IdentityRolePaginationRequest
            {
                Paging = query.Paging,
                Filters = query.Filters,
                Sorts = query.Sorts,
                Language = query.Language
            });
        }
    }
}
