﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityGetTenantIdResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ErpCode { get; set; }
        public string ErpGroupName { get; set; }
        public DateTime ErpStartDate { get; set; }
        public DateTime ErpEndDate { get; set; }
        public IdentityTenantGroupData TenantGroup { get; set; }
    }
}
