﻿namespace CountwareTraffic.Services.Reporting.Application
{
    public enum GrowthStatus : byte
    {
        Equality = 1,
        Increase = 2,
        Decrease = 3
    }
}
