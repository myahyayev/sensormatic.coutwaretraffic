﻿using CountwareTraffic.Services.Companies.Application;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class EventMapper : IEventMapper
    {
        public IEnumerable<Convey.CQRS.Events.IEvent> MapAll(IEnumerable<Sensormatic.Tool.Queue.IQueueEvent> events)
              => events.Select(Map);

        public Convey.CQRS.Events.IEvent Map(Sensormatic.Tool.Queue.IQueueEvent @event)
        {
            switch (@event)
            {
                case Sensormatic.Tool.QueueModel.AreaCreatedCompleted e:
                    return new AreaCreatedCompleted
                    {
                        AreaId = e.AreaId,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.AreaCreatedRejected e:
                    return new AreaCreatedRejected
                    {
                        AreaId = e.AreaId,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.AreaChangedRejected e:
                    return new AreaChangedRejected
                    {
                        AreaId = e.AreaId,
                        Name = e.Name,
                        OldName = e.OldName,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.AreaDeletedRejected e:
                    return new AreaDeletedRejected
                    {
                        AreaId = e.AreaId,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };
                case Sensormatic.Tool.QueueModel.SubAreaCreatedCompleted e:
                    return new SubAreaCreatedCompleted
                    {
                        SubAreaId = e.SubAreaId,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.SubAreaCreatedRejected e:
                    return new SubAreaCreatedRejected
                    {
                        SubAreaId = e.SubAreaId,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.SubAreaChangedRejected e:
                    return new SubAreaChangedRejected
                    {
                        SubAreaId = e.SubAreaId,
                        Name = e.Name,
                        OldName = e.OldName,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.SubAreaDeletedRejected e:
                    return new SubAreaDeletedRejected
                    {
                        SubAreaId = e.SubAreaId,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.CompanyCreated e:
                    return new CompanyCreated
                    {
                        ComapnyId = e.CompanyId,
                        CompanyName = e.CompanyName,
                        TenantName = e.TenantName,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };
                case Sensormatic.Tool.QueueModel.CompanyDeleted e:
                    return new CompanyDeleted
                    {
                        ComapnyId = e.CompanyId,
                        CompanyName = e.CompanyName,
                        TenantName = e.TenantName,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.CompanyUpdated e:
                    return new CompanyUpdated
                    {
                        ComapnyId = e.CompanyId,
                        CompanyName = e.CompanyName,
                        TenantName = e.TenantName,
                        UserId = e.UserId,
                        TenantId = e.TenantId
                    };

                case Sensormatic.Tool.QueueModel.CountryCreated e:
                    return new CountryCreated
                    {
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId,
                        CountryName = e.CountryName
                    };

                case Sensormatic.Tool.QueueModel.CountryUpdated e:
                    return new CountryUpdated
                    {
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId,
                        CountryName = e.CountryName
                    };

                case Sensormatic.Tool.QueueModel.CountryDeleted e:
                    return new CountryDeleted
                    {
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId,
                        CountryName = e.CountryName
                    };


                case Sensormatic.Tool.QueueModel.RegionCreated e:
                    return new RegionCreated
                    {
                        RegionId = e.RegionId,
                        RegionName = e.RegionName,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId,
                    };

                case Sensormatic.Tool.QueueModel.RegionUpdated e:
                    return new RegionUpdated
                    {
                        RegionId = e.RegionId,
                        RegionName = e.RegionName,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId,
                    };

                case Sensormatic.Tool.QueueModel.RegionDeleted e:
                    return new RegionDeleted
                    {
                        RegionId = e.RegionId,
                        RegionName = e.RegionName,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId,
                    };

                case Sensormatic.Tool.QueueModel.AreaHCreated e:
                    return new AreaHCreated
                    {
                        AreaId = e.AreaId,
                        AreaName = e.AreaName,
                        RegionId = e.RegionId,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId
                    };

                case Sensormatic.Tool.QueueModel.AreaHUpdated e:
                    return new AreaHUpdated
                    {
                        AreaId = e.AreaId,
                        AreaName = e.AreaName,
                        RegionId = e.RegionId,
                        CompanyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId
                    };

                case Sensormatic.Tool.QueueModel.AreaHDeleted e:
                    return new AreaHDeleted
                    {
                        AreaId = e.AreaId,
                        AreaName = e.AreaName,
                        RegionId = e.RegionId,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId
                    };


                case Sensormatic.Tool.QueueModel.SubAreaHCreated e:
                    return new SubAreaHCreated
                    {
                        SubAreaId = e.SubAreaId,
                        SubAreaName = e.SubAreaName,
                        AreaId = e.AreaId,
                        RegionId = e.RegionId,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId
                    };

                case Sensormatic.Tool.QueueModel.SubAreaHUpdated e:
                    return new SubAreaHUpdated
                    {
                        SubAreaId = e.SubAreaId,
                        SubAreaName = e.SubAreaName,
                        AreaId = e.AreaId,
                        RegionId = e.RegionId,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId
                    };

                case Sensormatic.Tool.QueueModel.SubAreaHDeleted e:
                    return new SubAreaHDeleted
                    {
                        SubAreaId = e.SubAreaId,
                        SubAreaName = e.SubAreaName,
                        AreaId = e.AreaId,
                        RegionId = e.RegionId,
                        ComapnyId = e.CompanyId,
                        UserId = e.UserId,
                        TenantId = e.TenantId,
                        CountryId = e.CountryId
                    };
            }
            return null;
        }
    }
}
