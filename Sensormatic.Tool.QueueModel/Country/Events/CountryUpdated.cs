﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record CountryUpdated : MessageEnvelope, IQueueEvent
    {
        public CountryUpdated(Guid countryId, string countryName, Guid companyId,  Guid recordId, Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            CountryId = countryId;
            CountryName = countryName;
            CompanyId = companyId;
            RecordId = recordId;
        }
        public Guid CountryId { get; }
        public string CountryName { get; }
        public Guid CompanyId { get; }
        public Guid RecordId { get; init; }
    }
}
