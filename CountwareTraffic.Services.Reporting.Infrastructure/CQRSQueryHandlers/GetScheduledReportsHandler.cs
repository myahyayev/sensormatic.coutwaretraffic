﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class GetScheduledReportsHandler : IQueryHandler<GetScheduledReports, IEnumerable<ReportDetailDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetScheduledReportsHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;
        public async Task<IEnumerable<ReportDetailDto>> HandleAsync(GetScheduledReports query)
        {
            var reportSceduleRepository = _unitOfWork.GetRepository<IReportScheduleRepository>();

            var scheduledReports = await reportSceduleRepository.GetScheduledReportsAsync();

            return scheduledReports.Select(reportSchedule => new ReportDetailDto
            {
                Id = reportSchedule.Report.Id,
                Name = reportSchedule.Report.Name,
                Description = reportSchedule.Report.Description,
                FormatId = reportSchedule.Report.Format.Id,
                FormatName = reportSchedule.Report.Format.Name,
                IsActive = reportSchedule.Report.IsActive,
                Locations = reportSchedule.Report.Location.Select(u => u.Name).ToList(),
                StatusId = reportSchedule.Report.Status.Id,
                StatusName = reportSchedule.Report.Status.Name,
                TemplateName = reportSchedule.Report.ReportTemplate.Name,
                TemplateId = reportSchedule.Report.ReportTemplate.Id,
                TemplateTypeId = reportSchedule.Report.TemplateType.Id,
                TemplateTypeName = reportSchedule.Report.TemplateType.Name,
                AtSetTime = reportSchedule.AtSetTime,
                AtSetTimeZone = reportSchedule.AtSetTimeZone,
                EmailBody = reportSchedule.Report.ReportEmail.Body,
                EmailSubject = reportSchedule.Report.ReportEmail.Subject,
                EmailTo = reportSchedule.Report.ReportEmail.To,
                LanguageId = reportSchedule.Report.ReportLanguage.Id,
                LanguageName = reportSchedule.Report.ReportLanguage.Name,
                NotifyWhenComplete = reportSchedule.Report.NotifyWhenComplete,
                RepeatOn = reportSchedule.RepeatOn,
                ScheduleDeliveryTimeId = reportSchedule.DeliveryTime.Id,
                ScheduleDeliveryTimeName = reportSchedule.DeliveryTime.Name,
                ScheduleFrequencyId = reportSchedule.Frequency.Id,
                ScheduleFrequencyName = reportSchedule.Frequency.Name,
                ScheduleId = reportSchedule.Id,
                StartsOn = reportSchedule.StartsOn,
                TenantId = reportSchedule.Report.TenantId,
                EndsOn = reportSchedule.EndsOn,
                DateRangeStartDate = reportSchedule.Report.DateRangeStartDate,
                DateRangeEndDate = reportSchedule.Report.DateRangeEndDate
            });
        }
    }
}
