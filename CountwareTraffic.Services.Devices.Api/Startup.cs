using Convey;
using Countware.Traffic.CrossCC.Observability;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Grpc;
using CountwareTraffic.Services.Devices.Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Sensormatic.Tool.ApplicationInsights;
using Sensormatic.Tool.Grpc.Server;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace CountwareTraffic.Services.Devices.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            IoCGenerator.DoTNet.Current.Start(services, Configuration);

            services.AddApplicationInsights(Configuration);

            services.AddDbContext<DeviceDbContext>(options =>
                                                 options.UseSqlServer(Configuration.GetConnectionString("DeviceDbConnection"), x => x.UseNetTopologySuite().EnableRetryOnFailure()));

            services.AddConvey()
                    .AddApplication()
                    .Build();


            services.AddGrpc(options =>
            {
                options.RegistrServerInterceptors();
                options.EnableDetailedErrors = true;
                options.MaxReceiveMessageSize = int.MaxValue;
                options.MaxSendMessageSize = int.MaxValue;
            });

            services.AddHostedService<SubAreaSubscriber>();
            services.AddHostedService<DeviceSubscriber>();
            services.AddHostedService<DeviceHSubscriber>();
            services.AddHostedService<AutoScaler>();

            services.AddCors(o =>
            {
                o.AddPolicy("AllowAll", builder =>
                {
                    builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding");
                });
            });

            services.ConfigureAuthService();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection()
              .UseRouting()
              .UseAuthentication()
              .UseAuthorization()
              .UseCors("AllowAll")
              .UseGrpcWeb()
              .UserCorrelationId()
              .UseEndpoints(endpoints =>
              {
                  endpoints.MapGrpcService<DeviceService>().RequireCors("AllowAll").EnableGrpcWeb();

                  endpoints.MapGet("/", async context =>
                  {
                      context.Response.ContentType = "text/html;charset=utf-8";
                      await context.Response.WriteAsync($"<h1>{Assembly.GetExecutingAssembly().GetName().Name} v{Assembly.GetExecutingAssembly().GetName().Version.ToString()} is running</h1>");
                  });
              });
        }
    }
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureAuthService(this IServiceCollection services)
        {
            var authenticationConfig = services.BuildServiceProvider().GetRequiredService<IOptions<AuthenticationConfig>>().Value;

            services
                  .AddAuthentication(options =>
                  {
                      options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                      options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                  })
                   .AddJwtBearer(cfg =>
                   {
                       cfg.RequireHttpsMetadata = true;
                       cfg.SaveToken = true;
                       cfg.TokenValidationParameters = new TokenValidationParameters()
                       {
                           IssuerSigningKey = new SymmetricSecurityKey(
                               Encoding.UTF8.GetBytes(authenticationConfig.SignKey)),
                           ValidateAudience = false,
                           ValidateIssuer = false,
                           ValidateLifetime = true,
                           RequireExpirationTime = true,
                           ClockSkew = TimeSpan.Zero,
                           ValidateIssuerSigningKey = true
                       };
                   });

            services.AddAuthorization();

            return services;

        }
    }
}
