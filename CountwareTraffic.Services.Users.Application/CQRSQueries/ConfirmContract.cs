﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries
{
    public class ConfirmContract : IQuery<IdentityConfirmContractResponse>
    {
        public Guid ContractId { get; set; }
    }
}
