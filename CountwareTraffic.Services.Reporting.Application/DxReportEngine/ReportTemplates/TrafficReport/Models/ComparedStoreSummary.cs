﻿namespace CountwareTraffic.Services.Reporting.Engine.TrafficReport
{
    public record ComparedStoreSummary : Engine.BaseComparedStoreSummary
    {
        public Traffic Traffic { get; set; }
        public Weather Weather { get; set; }
        public ComparedStoreSummary()
        {
            Traffic = new Traffic();
            Weather = new Weather();
        }
    }
}
