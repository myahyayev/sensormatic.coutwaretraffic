﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRReportGeneratingCompletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.ReportGeneratingCompleted>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRReportGeneratingCompletedConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.ReportGeneratingCompleted queueEvent)
        {
            await _commandDispatcher.SendAsync(new ReportGeneratingCompleted
            {
                ReportId = queueEvent.ReportId,
                FilePath = queueEvent.FilePath,
                ReportStatus = queueEvent.ReportStatus,
                TenantId = queueEvent.TenantId,
                FileId = queueEvent.FileId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
