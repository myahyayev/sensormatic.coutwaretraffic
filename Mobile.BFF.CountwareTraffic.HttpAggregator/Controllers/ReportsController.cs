﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class ReportsController : BaseController
    {
        private readonly ReportService _reportService;
        public ReportsController(IServiceProvider provider, ILogger<ReportsController> logger, IHttpContextAccessor contextAccessor, ReportService reportService)
            : base(provider, logger, contextAccessor) => _reportService = reportService;


        [HttpGet("all-templates")]
        [Permissions(Permissions = new[] { Permissions.ReportView })]
        public async Task<ApiResponse<IEnumerable<GetReportTemplatesApiResponse>>> GetPerformingByTypeAsync()
            => Response(await _reportService.GetReportTemplatesAsync(), new ResultMessage("#S1115"));

        [HttpGet("all-lookup")]
        [Permissions(Permissions = new[] { Permissions.ReportView })]
        public async Task<ApiResponse<GetReportLookupsApiResponse>> GetReportLookupsAsync()
            => Response(await _reportService.GetReportLookupsAsync(), new ResultMessage("#S1116"));

        [HttpPost("details")]
        [Permissions(Permissions = new[] { Permissions.ReportList })]
        public async Task<ApiPagedResponse<GetReportsDetail>> GetReportsAsync([FromBody] DataSourceApiRequest paging)
            => await _reportService.GetReportsAsync(paging);

        [HttpGet("{reportId}/detail")]
        [Permissions(Permissions = new[] { Permissions.ReportView })]
        public async Task<ApiResponse<GetGetReportByIdApiResponse>> GetReportByIdAsync(Guid reportId)
            => Response(await _reportService.GetReportByIdAsync(reportId), new ResultMessage("#S1117"));

        [HttpGet("all-languages")]
        [Permissions(Permissions = new[] { Permissions.ReportView })]
        public async Task<ApiResponse<IEnumerable<GetReportLanguagesApiResponse>>> GetReportLanguagesAsync()
            => Response(await _reportService.GetReportLanguagesAsync(), new ResultMessage("#S1118"));

        [HttpDelete("{reportId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.ReportDelete })]
        public async Task<ApiResponse<CrudApiResponse>> DeleteReportAsync(Guid reportId)
        {
            await _reportService.CancelReportAsync(reportId);

            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Delete, Id = reportId }, new ResultMessage()
            {
                CallToActionType = CallToActionType.Ok,
                Description = $"#S1119 Report with {reportId} id has been deleted.",
                Title = $"#S1119 Report with {reportId} id has been deleted."
            });
        }

        [HttpPost("add"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.ReportAdd })]
        public async Task<ApiResponse<CrudApiResponse>> AddReportAsync([FromBody] AddReportApiRequest request)
        {
            await _reportService.CreateReportAsync(request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Create }, new ResultMessage("#S1120"));
        }

        [HttpGet("template/{reportTemplateId}/view")]
        [Permissions(Permissions = new[] { Permissions.ReportView })]
        public async Task<ApiResponse<GetReportTemplateViewApiResponse>> GetReportLookupsAsync(Guid reportTemplateId)
           => Response(await _reportService.GetReportTemplateViewAsync(reportTemplateId), new ResultMessage("#S1121"));

        [HttpPost("{reportId}/files"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.ReportView })]
        public async Task<ApiPagedResponse<ReportFile>> GetAreaDetailsAsync(Guid reportId, [FromBody] DataSourceApiRequest paging)
         => await _reportService.GetReportFilesAsync(reportId, paging);

        [HttpPost("{reportId}/retry-generating"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.ReportAdd })]
        public async Task<ApiResponse<RetryReportGeneratingApiResponse>> RetryReportGeneratingAsync(Guid reportId, [FromBody] RetryReportGeneratingApiRequest request)
            => Response(await _reportService.RetryReportGeneratingAsync(reportId, request), new ResultMessage("#S1122"));
    }
}
