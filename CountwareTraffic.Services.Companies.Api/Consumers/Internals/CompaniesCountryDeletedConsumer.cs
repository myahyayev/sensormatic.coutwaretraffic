﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesCountryDeletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.CountryDeleted>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesCountryDeletedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.CountryDeleted queuEvent)
        {
            var countryDeleted = _eventMapper.Map(queuEvent) as CountryDeleted;
            await _eventDispatcher.PublishAsync(countryDeleted);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
