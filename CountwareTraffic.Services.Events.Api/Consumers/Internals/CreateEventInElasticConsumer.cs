﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    public class CreateEventInElasticConsumer : IConsumer<Sensormatic.Tool.QueueModel.EventCreated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper; 
        private readonly ILogger<CreateEventInElasticConsumer> _logger;
        public CreateEventInElasticConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper,ILogger<CreateEventInElasticConsumer> logger)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
            _logger = logger;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.EventCreated queuEvent)
        {
            var eventCreated = _eventMapper.Map(queuEvent) as EventCreated;
            _logger.LogInformation($"EventId:{eventCreated.EventId} consumed");
            await _eventDispatcher.PublishAsync(eventCreated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
