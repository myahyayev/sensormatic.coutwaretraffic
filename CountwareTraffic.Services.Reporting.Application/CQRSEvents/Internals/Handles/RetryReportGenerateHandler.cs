﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Reporting.Core;
using CountwareTraffic.Services.Reporting.Engine;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Application
{

    public class RetryReportGenerateHandler : IEventHandler<RetryReportGenerate>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ReportInitializer _reportInitializer;
        private readonly ILogger<RetryReportGenerateHandler> _logger;

        public RetryReportGenerateHandler(IUnitOfWork unitOfWork, ReportInitializer reportInitializer, ILogger<RetryReportGenerateHandler> logger)
        {
            _unitOfWork = unitOfWork;
            _reportInitializer = reportInitializer;
            _logger = logger;
        }

        public async Task HandleAsync(RetryReportGenerate @event)
        {
            var reportRepository = _unitOfWork.GetRepository<IReportRepository>();

            var reportDetail = await reportRepository.GetReportByIdAsync(@event.ReportId);

            await _reportInitializer.RetryGenerateReportFileAndSaveContainerAsync(
               assemblyQualifiedName: reportDetail.ReportTemplate.AssemblyQualifiedName,
               reportId: @event.ReportId,
               reportName: reportDetail.Id.ToString(),
               reportPeriodType: reportDetail.TemplateType.ToEnum<TemplateType, ReportPeriodType>(),
               reportExportype: reportDetail.Format.ToEnum<ReportFormat, ReportExportype>(),
               reportFrequencyType: reportDetail.ReportSchedule.Frequency.ToEnum<ScheduleFrequency, ReportFrequencyType>(),
               reportLocations: reportDetail.Location,
               reportStartDate: (reportDetail.ReportSchedule.Frequency == ScheduleFrequency.Repeat && @event.DateRangeStartDate.HasValue)
                                        ? @event.DateRangeStartDate.Value
                                        : reportDetail.DateRangeStartDate,
               reportEndDate: (reportDetail.ReportSchedule.Frequency == ScheduleFrequency.Repeat && @event.DateRangeStartDate.HasValue)
                                        ? @event.DateRangeEndDate.Value
                                        : reportDetail.DateRangeEndDate,
               reportRepeatOn: reportDetail.ReportSchedule.RepeatOn,
               reportTenantId: reportDetail.TenantId,
               reportUserName: reportDetail.UserName,
               reportEmailBody: reportDetail.ReportEmail.Body,
               reportEmailTo: reportDetail.ReportEmail.To,
               reportEmailSubject: reportDetail.ReportEmail.Subject,
               language: new Engine.ReportLanguage
                    (
                        reportDetail.ReportLanguage.Id,
                        reportDetail.ReportLanguage.Name,
                        reportDetail.ReportLanguage.ShortName,
                        reportDetail.ReportLanguage.WindowsTimeZone,
                        reportDetail.ReportLanguage.LinuxTimeZone
                     ),
                reportFileId: @event.ReportFileId
               );
        }
    }
}
