﻿using CountwareTraffic.Services.Reporting.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public interface IReportBuilder
    {
        IScheduleReport Build();
        IReportBuilder WithDatasource(object datasource);
        IReportBuilder WithExportFormat(ReportExportype expportFormat);
        Task<IReportBuilder> WithLoadingDataSourceFromDataBaseAsync();
        IReportBuilder WithPeriodType(ReportPeriodType reportPeriodType);
        IReportBuilder WithName(string reportName);
        IReportBuilder WithParameters(List<Location> locations, DateTime startdate, DateTime endDate);
        IReportBuilder WithTenantId(Guid tenantId);
        IReportBuilder WithLanguage(ReportLanguage language);
        IReportBuilder WithCreator(string creator);
        IReportBuilder WithId(Guid reportId);
        IReportBuilder WithFrequencyType(ReportFrequencyType frequencyType);
        IReportBuilder WithEmailBody(string emailBody);
        IReportBuilder WithEmailSubject(string emailSubject);
        IReportBuilder WithRepeatOn(string repeatOn);
        IReportBuilder WithEmailTo(string to);
    }
}