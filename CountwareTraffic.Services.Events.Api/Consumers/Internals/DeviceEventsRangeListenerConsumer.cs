﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    /// <summary>
    /// DeviceEventsRangeListenerConsumer consumes a chunk of bulk records of EventInection data
    /// </summary>
    public class DeviceEventsRangeListenerConsumer : IConsumer<Sensormatic.Tool.QueueModel.DeviceEventsRangeListener>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;
        public DeviceEventsRangeListenerConsumer(ICommandDispatcher commandDispatcher) => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.DeviceEventsRangeListener queueCommand)
        {

            if (queueCommand is not null && queueCommand?.Items.Count > 0)
            {

                var deviceId = queueCommand.Items.FirstOrDefault().DeviceId;


                if (!queueCommand.Items.All(i => i.DeviceId == deviceId))
                    throw new Exception("DeviceEventsRangeListener items should be assigned to one device");

                CreateEventRange createEventRange = new CreateEventRange(deviceId);

                foreach (var deviceEventsRangeItem in queueCommand.Items)
                {
                    createEventRange.Items.Add(new()
                    {
                        Description = deviceEventsRangeItem.Description,
                        DeviceId = deviceEventsRangeItem.DeviceId,
                        DirectionTypeId = deviceEventsRangeItem.DirectionTypeId,
                        EventDate = deviceEventsRangeItem.EventDate,
                        EventStartDate = deviceEventsRangeItem.EventStartDate,
                        EventEndDate = deviceEventsRangeItem.EventEndDate,
                        TenantId = deviceEventsRangeItem.TenantId,
                        OnTimeIOCount = deviceEventsRangeItem.OnTimeIOCount
                    });
                }
                await _commandDispatcher.SendAsync(createEventRange);
            }
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
