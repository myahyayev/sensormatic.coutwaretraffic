﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class SubAreaDeletedFailedHandler : ICommandHandler<SubAreaDeletedFailed>
    {
        private readonly IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> _hubContext;
        public SubAreaDeletedFailedHandler(IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> hubContext) 
            => _hubContext = hubContext;

        public async Task HandleAsync(SubAreaDeletedFailed command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .SubAreaDeletedFailedReceived(command);
    }
}