﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class CreateBrandModelLookupHandler : ICommandHandler<CreateBrandModelLookup>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateBrandModelLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task HandleAsync(CreateBrandModelLookup command)
        {
            var brandModelRepository = _unitOfWork.GetRepository<IBrandModelLookupRepository>();

            var brandModel = await brandModelRepository.GetByNameAsync(command.Name);

            if (brandModel is not null)
                throw new BrandModelAlreadyExistsException(command.Name);

            var brandLookup = BrandModelLookup.Create(command.Name, command.BrandId);

            await brandModelRepository.AddAsync(brandLookup);
            await _unitOfWork.CommitAsync();
        }
    }
}
