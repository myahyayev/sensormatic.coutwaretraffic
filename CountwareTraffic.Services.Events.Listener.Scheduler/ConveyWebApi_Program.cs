//using Convey;
//using Convey.CQRS.Commands;
//using Convey.Docs.Swagger;
//using Convey.WebApi;
//using Convey.WebApi.CQRS;
//using Convey.WebApi.Swagger;
//using CountwareTraffic.Services.Events.Application;
//using CountwareTraffic.Services.Events.Infrastructure;
//using Microsoft.AspNetCore;
//using Microsoft.AspNetCore.Builder;
//using Microsoft.AspNetCore.Hosting;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Hosting;
//using Microsoft.Extensions.Logging;
//using Sensormatic.Tool.Api;
//using Sensormatic.Tool.Ioc;
//using Serilog;
//using Serilog.Sinks.SystemConsole.Themes;
//using System;
//using System.IO;
//using System.Threading.Tasks;

//namespace CountwareTraffic.Services.Events.Listener.Scheduler
//{
//    public class Program
//    {
//        public static async Task Main(string[] args)
//        {
//            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

//            var configuration = new ConfigurationBuilder()
//                .SetBasePath(Directory.GetCurrentDirectory())
//                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
//                .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
//                .AddEnvironmentVariables()
//                .Build();

//            Log.Logger = new LoggerConfiguration()
//                .ReadFrom.Configuration(configuration)
//                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}", theme: AnsiConsoleTheme.Code)
//                .CreateLogger();
//            try
//            {
//                Log.Information("Starting host...");
//                await CreateHostBuilder(args);
//            }
//            catch (Exception ex) { Log.Fatal(ex, "Host terminated unexpectedly."); }
//            finally { Log.CloseAndFlush(); }
//        }

//        private static async Task CreateHostBuilder(string[] args) =>

//              await WebHost.CreateDefaultBuilder(args)
//             .UseSerilog()
//             .ConfigureServices((hostContext, services) =>
//                 {
//                     IoCGenerator.DoTNet.Current.Start(services, hostContext.Configuration);

//                     services.AddDbContext<EventDbContext>(options =>
//                                                 options.UseSqlServer(hostContext.Configuration.GetConnectionString("EventDbConnection"), x => x.UseNetTopologySuite()));

//                     services.AddCors(setupAction =>
//                     {
//                         setupAction.AddPolicy("AllowAll", builder =>
//                         {
//                             builder.SetIsOriginAllowed((host) => true);
//                             builder.AllowAnyMethod();
//                             builder.AllowAnyHeader();
//                             builder.AllowCredentials();
//                         });
//                     });

//                     services.AddHostedService<AutoCreateDeviceEventEndpointSubscriber>();
//                     services.AddHostedService<AutoDeleteDeviceEventEndpointSubscriber>();

//                     services.AddSingleton<IEndpointsBuilder, EndpointsBuilder>();

//                     services.AddConvey()
//                        .AddWebApi()
//                        .AddWebApiSwaggerDocs()
//                        .AddApplication()
//                        .Build();
//                 })
//             .Configure(app =>
//              {
//                  var _commandDispatcher = app.ApplicationServices.GetRequiredService<ICommandDispatcher>();

//                  app
//                    .UseSwaggerDocs()
//                    .UseSensormaticExceptionMiddleware()
//                    .UseCors("AllowAll")
//                    .UsePublicContracts<ContractAttribute>()
//                    .UseDispatcherEndpoints(endpoints =>
//                    {
//                        //#region Brickstream Test XML Format
//                        //endpoints.Post($"api/v1/devices/realtime/events/xml", context: async (ctx) =>
//                        //{
//                        //    StreamReader stream = new(ctx?.Request?.Body);

//                        //    if (stream != null)
//                        //    {
//                        //        await _commandDispatcher.SendAsync(new BrickstreamEventLogXmlFormat()
//                        //        {
//                        //            Xml = await stream.ReadToEndAsync()
//                        //        });
//                        //    }
//                        //    else
//                        //        Log.Error("xml file is empty!");
//                        //});
//                        //#endregion Brickstream XML Format 


//                        #region Brickstream XML Format
//                        endpoints.Post($"api/v1/brickstream/realtime/events/xml", context: async (ctx) =>
//                        {
//                            StreamReader stream = new(ctx?.Request?.Body);
//                            if (stream != null)
//                            {
//                                string xml = await stream.ReadToEndAsync();
//                                Log.Information($@"Brickstream Xml file : {xml}");
//                                await _commandDispatcher.SendAsync(new BrickstreamEventLogXmlFormat()
//                                {
//                                    Xml = xml
//                                });
//                            }
//                            else
//                                Log.Error("Brickstream xml file is empty!");
//                        });
//                        #endregion Brickstream XML Format 


//                        #region Hikvision XML Format
//                        endpoints.Post($"api/v1/hikvision/realtime/events/xml", context: async (ctx) =>
//                        {
//                            StreamReader stream = new(ctx?.Request?.Body);
//                            if (stream != null)
//                            {
//                                string xml = await stream.ReadToEndAsync();
//                                Log.Information($@"Hikvision Xml file : {xml}");
//                                await _commandDispatcher.SendAsync(new HikvisionEventLogXmlFormat()
//                                {
//                                    Xml = xml
//                                });
//                            }
//                            else
//                                Log.Error("Hikvision xml file is empty!");
//                        });
//                        #endregion Brickstream XML Format 


//                        #region Redis chache 
//                        //TODO KENDIME NOT Convey HttpDelete'e bos zamaninda bak.  MAHMUD YAHYAYEV
//                        //Testlerimizi saglikli yamak icin redisi temizlemeye ihtiyac vardir. Live den sonra silinicektir.
//                        endpoints.Post<RemoveCache>("api/v1/devices/latestevent-remove",
//                         beforeDispatch: (eventLog, ctx) =>
//                                 {
//                                     Log.Information($"Api Request: {{DeviceId}}", eventLog.DeviceId);
//                                     return Task.CompletedTask;
//                                 },
//                         afterDispatch: (eventLog, ctx) =>
//                                {
//                                    return ctx.Response.Created(data: new { Removed = "Removed" });
//                                });


//                        //Migration esnasinda suanda kullanilan her cihaz iicn baslangic event datasinin belli alanlarini redise koymak icin gelistirilmisdir. Migration islemi bittikten sonra siline bilir.
//                        endpoints.Post<AddCacheDefaultValue>($"api/v1/devices/latestevent-defaultvalue-add",
//                         beforeDispatch: (eventLog, ctx) =>
//                         {
//                             Log.Information($"Api Request: {{DeviceId}} {{TotalEnterCount}} {{TotalExitCount}}", eventLog.DeviceId, eventLog.TotalEnterCount, eventLog.TotalExitCount);
//                             return Task.CompletedTask;
//                         },
//                         afterDispatch: (eventLog, ctx) =>
//                         {
//                             return ctx.Response.Created(data: new { Created = "Created" });
//                         });
//                        #endregion Redis chache 


//                        #region Brickstream Test
//                        //Sadece test amaclidir. Lutfen kullanmayin. Her test ettiginde macadres ve ippadresi degistir.
//                        //<MacAddress>00:b0:9d:27:50:a7</MacAddress>   <IpAddress>192.168.25.112</IpAddress>
//                        endpoints.Post($"api/v1/brickstream/test/realtime/events/xml", context: async (ctx) =>
//                        {
//                            string xml = @"<?xml version=""1.0""?>
//                        <RealTimeMetrics  SiteId=""Countware Azure Test Site ID"" >
//                        <Properties>
//                        <Version>5</Version>
//                        <TransmitTime>1657022402</TransmitTime>
//                        <MacAddress>00:b0:9d:27:50:a7</MacAddress>
//                        <IpAddress>192.168.25.112</IpAddress>
//                        <HostName>Cam-19353767</HostName>
//                        <HttpPort>80</HttpPort>
//                        <HttpsPort>443</HttpsPort>
//                        <Timezone>3</Timezone>
//                        <TimezoneName>(GMT 03:00) Nairobi</TimezoneName>
//                        <DST>0</DST>
//                        <HwPlatform>2310</HwPlatform>
//                        <SerialNumber>19353767</SerialNumber>
//                        <DeviceType>0</DeviceType>
//                        <SwRelease>5.9.21.621</SwRelease>
//                        </Properties>
//                        <RTReport  Date=""2022-07-05T15:00:02"" >
//                        <RTObject  Id=""0"" DeviceId =""Countware Azure Test Device ID"" Devicename =""Countware Azure Test Device Nam"" ObjectType =""0"" Name =""Countware Azure Name"" ExternalId =""2"" >
//                        <RTCount  TotalEnters=""811"" TotalExits =""790"" />
//                        </RTObject>
//                        </RTReport>
//                        </RealTimeMetrics>";

//                            await _commandDispatcher.SendAsync(new BrickstreamEventLogXmlFormat()
//                            {
//                                Xml = xml
//                            });
//                        });
//                        #endregion Brickstream Test 


//                        #region Hikvision Test
//                        //Sadece test amaclidir. Lutfen kullanmayin. Her test ettiginde macadres ve ippadresi degistir.
//                        //<MacAddress>00:b0:9d:27:50:a7</MacAddress>   <IpAddress>192.168.25.112</IpAddress>
//                        endpoints.Post($"api/v1/hikvision/test/realtime/events/xml", context: async (ctx) =>
//                        {
//                            string xml = @"<?xml version=""1.0"" encoding =""UTF -8"" ?>
//<EventNotificationAlert version=""1.0"" xmlns =""urn:psialliance-org"">
//<ipAddress>192.168.21.21</ipAddress>
//<protocolType>HTTP</protocolType>
//<macAddress>58:50:ed:73:2f:42</macAddress>
//<channelID>1</channelID>
//<dateTime>2022-07-18T08:25:05+00:00</dateTime>
//<activePostCount>1</activePostCount>
//<eventType>PeopleCounting</eventType>
//<eventState>active</eventState>
//<eventDescription>peopleCounting alarm</eventDescription>
//<channelName>Mahmud Test Cihazi</channelName>
//<peopleCounting>
//<statisticalMethods>timeRange</statisticalMethods>
//<TimeRange>
//<startTime>2022-07-18T08:24:00+00:00</startTime>
//<endTime>2022-07-18T08:25:00+00:00</endTime>
//</TimeRange>
//<enter>0</enter>
//<exit>0</exit>
//<regionsID>1</regionsID>
//</peopleCounting>
//<childCounting>
//<enter>0</enter>
//<exit>0</exit>
//</childCounting>
//</EventNotificationAlert>";

//                            await _commandDispatcher.SendAsync(new HikvisionEventLogXmlFormat()
//                            {
//                                Xml = xml
//                            });
//                        });
//                        #endregion Hikvision Test 

//                    });
//              })
//              .Build()
//              .RunAsync();
//    }
//}




















































































////#region HIKVISION XML Format
////endpoints.Post<CommonEventLogXmlFormat>($"api/v1/devices/realtime/events/xml",
////    beforeDispatch: (eventLog, ctx) =>
////    {
////        if (ctx.Request?.Body != null)
////        {
////            StreamReader stream = new (ctx.Request.Body);

////            eventLog.Xml = stream.ReadToEnd();

////            if(eventLog.Xml.IsNullOrWhiteSpace())
////                throw new Exception("Hikvision xml null");

////            Log.Information($"Api Request:", eventLog.Xml);
////            return Task.CompletedTask;
////        }
////        else
////            throw new Exception("Hikvision Input stream null");
////    },

////afterDispatch: (eventLog, ctx) =>
////{
////    Log.Information($"Api Response: success");
////    return ctx.Response.Created(data: new { Created = "Created" });
////});
////#endregion HIKVISION XML Format



////#region HIKVISON POST DATA
////endpoints.Post<CommonEventLog>($"api/v1/devices/event/",
////    beforeDispatch: (eventLog, ctx) =>
////    {
////        if (eventLog.EventDate == DateTime.MinValue)
////            eventLog.EventDate = DateTime.UtcNow;

////        Log.Information($"Api Request: {{DeviceId}}  {{DeviceName}}  {{RequestId}}  {{Description}} {{DirectionTypeId}} {{EventDate}}",
////                eventLog.DeviceId,
////                eventLog.DeviceName,
////                eventLog.RequestId,
////                eventLog.Description,
////                eventLog.DirectionTypeId,
////                eventLog.EventDate);

////        return Task.CompletedTask;
////    },

////afterDispatch: (eventLog, ctx) =>
////{
////    Log.Information($"Api Response: {{DeviceId}}  {{DeviceName}}  {{RequestId}}  {{Description}} {{DirectionTypeId}} {{EventDate}}",
////        eventLog.DeviceId,
////        eventLog.DeviceName,
////        eventLog.RequestId,
////        eventLog.Description,
////        eventLog.DirectionTypeId,
////        eventLog.EventDate);

////    return ctx.Response.Created(data: new { Created = "Created" });
////});
////#endregion HIKVISON POST DATA


////#region HIKVISON POST DATA PER DEVICE
////foreach (var device in devices)
////{
////    endpoints.Post<EventLog>($"api/v1/device/{device.Id}/event/",
////        beforeDispatch: (eventLog, ctx) =>
////         {
////             //Eger lazimsa gelen istek uzerinden Ip adresi ile cihazin Ip adresininin esit oldugunun kontrolunu yapa bilirsiz.

////             eventLog.DeviceId = device.Id;
////             eventLog.DeviceName = device.Name;
////             eventLog.TenantId = device.TenantId;

////             if (eventLog.EventDate == DateTime.MinValue)
////                 eventLog.EventDate = DateTime.UtcNow;

////             Log.Information($"Api Request: {{DeviceId}}  {{DeviceName}}  {{RequestId}}  {{Description}} {{DirectionTypeId}} {{EventDate}}",
////                 eventLog.DeviceId,
////                 eventLog.DeviceName,
////                 eventLog.RequestId,
////                 eventLog.Description,
////                 eventLog.DirectionTypeId,
////                 eventLog.EventDate);

////             return Task.CompletedTask;
////         },
////        afterDispatch: (eventLog, ctx) =>
////        {
////            Log.Information($"Api Response: {{DeviceId}}  {{DeviceName}}  {{RequestId}}  {{Description}} {{DirectionTypeId}} {{EventDate}}",
////                eventLog.DeviceId,
////                eventLog.DeviceName,
////                eventLog.RequestId,
////                eventLog.Description,
////                eventLog.DirectionTypeId,
////                eventLog.EventDate);

////            return ctx.Response.Created(data: new { Created = "Created" });
////        });
////}
////#endregion HIKVISON POST DATA PER DEVICE



////endpoints.Get("", ctx => ctx.Response.WriteAsync(ctx.RequestServices.GetService<AppOptions>().Name));

///*
// * 
// * XNamespace xmlns = "urn:psialliance-org";

//                               if (ctx.Request?.Body != null)
//                               {
//                                   StreamReader stream = new(ctx.Request.Body);

//                                   string xml = stream.ReadToEnd();

//                                   if (!xml.IsNullOrWhiteSpace())
//                                   {
//                                       XDocument doc = XDocument.Parse(xml.Trim());

//                                       var metrics = doc.Descendants(xmlns + "EventNotificationAlert");

//                                       foreach (var metric in metrics)
//                                       {
//                                           var macAdress = metric.Descendants(xmlns + "macAddress").FirstOrDefault()?.Value;

//                                           var ipAdress = metric.Descendants(xmlns + "ipAddress").FirstOrDefault()?.Value;

//                                           var eventType = metric.Descendants(xmlns + "eventType").FirstOrDefault()?.Value;

//                                           var staticticMethod = metric.Descendants(xmlns + "statisticalMethods").FirstOrDefault()?.Value;

//                                           var devicename = metric.Descendants(xmlns + "channelName").FirstOrDefault()?.Value;

//                                           if (macAdress.IsNullOrWhiteSpace() || ipAdress.IsNullOrWhiteSpace() || eventType.IsNullOrWhiteSpace() || staticticMethod.IsNullOrWhiteSpace() || devicename.IsNullOrWhiteSpace())
//                                           {
//                                               throw new Exception($"One or more parameter error.Mac:{macAdress},IpAdress:{ipAdress},evenType:{eventType},statisticMethod:{staticticMethod},deviceName:{devicename}");
//                                           }
//                                           else
//                                           {
//                                               if (eventType == "PeopleCounting")
//                                               {
//                                                   XElement properties = metric.Descendants(xmlns + "Properties").FirstOrDefault();
//                                               }
//                                           }
//                                       }
//                                   }




//                                   if (eventLog.EventDate == DateTime.MinValue)
//                                       eventLog.EventDate = DateTime.UtcNow;

//                                   Log.Information($"Api Request: {{DeviceId}}  {{DeviceName}}  {{RequestId}}  {{Description}} {{DirectionTypeId}} {{EventDate}}",
//                                           eventLog.DeviceId,
//                                           eventLog.DeviceName,
//                                           eventLog.RequestId,
//                                           eventLog.Description,
//                                           eventLog.DirectionTypeId,
//                                           eventLog.EventDate);

//                                   return Task.CompletedTask;
// * 
// * 
// */