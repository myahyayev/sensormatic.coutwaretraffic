﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetWidgetPerformingApiRequest
    {
        public List<HierarchyTreeSelectFilterRequest> HierarchyTreeSelectFilters { get; set; }
        public DateTime CurrentStartDate { get; set; }
        public DateTime CurrentEndDate { get; set; }
        public DateTime PreviusEndDate { get; set; }
        public DateTime PreviusStartDate { get; set; }
    }
}