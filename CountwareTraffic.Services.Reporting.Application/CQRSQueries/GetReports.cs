﻿using Convey.CQRS.Queries;
using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class GetReports : IQuery<PagingResult<ReportDetailDto>>
    {
        public PagingQuery PagingQuery { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
    }
}
