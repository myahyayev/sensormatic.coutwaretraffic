﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Settings.Core
{
    public class NoteDateInvalidFormatException : DomainException
    {
        public NoteDateInvalidFormatException()
            : base(new List<ErrorResult>() { new ErrorResult($"The BeginDate must be less than or equal to the EndDate") }, 404, ResponseMessageType.Error)
        {
        }
    }
}
