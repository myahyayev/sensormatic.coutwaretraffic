﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class AreaCreatedFailedHandler : ICommandHandler<AreaCreatedFailed>
    {
        private readonly IHubContext<AreaNotificationsHub, IAreaNotificationsHub> _hubContext;
        public AreaCreatedFailedHandler(IHubContext<AreaNotificationsHub, IAreaNotificationsHub> hubContext)
            => _hubContext = hubContext;


        public async Task HandleAsync(AreaCreatedFailed command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .AreaCreatedFailedReceived(command);
    }
}
