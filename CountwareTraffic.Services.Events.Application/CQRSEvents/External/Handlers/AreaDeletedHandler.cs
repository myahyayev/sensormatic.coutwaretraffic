﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Events.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application.CQRSEvents.External.Handlers
{
    public class AreaDeletedHandler : IEventHandler<AreaDeleted>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public AreaDeletedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(AreaDeleted @event)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var area = await areaRepository.GetAsync(@event.AreaId);

            if (area != null)
            {
                try
                {
                    areaRepository.Remove(area);

                    await _unitOfWork.CommitAsync();

                    _queueService.Publish(new AreaDeletedSuccessfully
                    {
                        RecordId = Guid.NewGuid(),
                        AreaId = @event.AreaId,
                        UserId = @event.UserId,
                        UserName = String.Empty,
                        TenantId = @event.TenantId
                    });
                }
                catch { _queueService.Publish(new AreaDeletedRejected(@event.AreaId, @event.UserId, "", @event.TenantId)); }
            }
        }
    }
}

