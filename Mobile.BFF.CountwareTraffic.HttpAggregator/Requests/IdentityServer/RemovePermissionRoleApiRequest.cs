﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class RemovePermissionRoleApiRequest
    {
        [FromForm(Name = "id")]
        public Guid Id { get; set; }

        [FromForm(Name = "permissionId")]
        public Guid PermissionId { get; set; }

        [FromForm(Name = "roleId")]
        public Guid RoleId { get; set; }
    }
}
