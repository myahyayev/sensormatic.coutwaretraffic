﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class SubAreaChangedSuccessfullyHandler : ICommandHandler<SubAreaChangedSuccessfully>
    {
        private readonly IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> _hubContext;
        public SubAreaChangedSuccessfullyHandler(IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> hubContext) 
            => _hubContext = hubContext;


        public async Task HandleAsync(SubAreaChangedSuccessfully command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .SubAreaChangedSuccessfullyReceived(command);
    }
}
