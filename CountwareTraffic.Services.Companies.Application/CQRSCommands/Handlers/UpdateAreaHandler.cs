﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class UpdateAreaHandler : ICommandHandler<UpdateArea>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public UpdateAreaHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }


        public async Task HandleAsync(UpdateArea command)
        {
            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var area = await areaRepository.GetAsync(command.AreaId, _currentTenant.Id);

            if (area is null)
                throw new AreaNotFoundException(command.AreaId);

            var updateControl = await areaRepository.GetAsync(command.Name);
            if (updateControl != null && updateControl.Id != command.AreaId)
                throw new AreaAlreadyExistsException(command.Name);

            area.CompleteChange(command.Name, command.Description, command.DistrictId, command.CityId, command.AreaTypeId, command.ManagerName, command.ManagerId,
                    new AreaAddressEntityDto
                    {
                        Latitude = command.Latitude,
                        Longitude = command.Longitude,
                        Street = command.Street
                    },
                    new AreaContactEntityDto
                    {
                        EmailAddress = command.EmailAddress,
                        GsmCountryCode = command.GsmCountryCode,
                        GsmDialCode = command.GsmDialCode,
                        GsmNumber = command.GsmNumber,
                        PhoneCountryCode = command.PhoneCountryCode,
                        PhoneDialCode = command.PhoneDialCode,
                        PhoneNumber = command.PhoneNumber
                    },
                    command.WorkingHoursStart,
                    command.WorkingHoursEnd,
                    command.WorkingTimeZone);

            var regionRepository = _unitOfWork.GetRepository<IRegionRepository>();

            var hierarchyData = await regionRepository.GetHierarchyAsync(area.RegionId, _currentTenant.Id);

            area.WhenHierarchyChanged(_currentTenant.Id, hierarchyData.CopmanyId, hierarchyData.CountryId, hierarchyData.RegionId, area.Id, area.Name);


            await _unitOfWork.CommitAsync();
        }
    }
}
