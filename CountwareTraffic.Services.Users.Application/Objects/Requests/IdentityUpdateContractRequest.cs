﻿using System;
using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application.Objects.Requests
{
    public class IdentityUpdateContractRequest
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("version")]
        public string Version { get; set; }

        [JsonPropertyName("releaseDate")]
        public DateTime ReleaseDate { get; set; }

        [JsonPropertyName("contractBody")]
        public string ContractBody { get; set; }

        [JsonPropertyName("contractType")]
        public string ContractType { get; set; }

        [JsonPropertyName("language")]
        public string Language { get; set; }

        [JsonPropertyName("appType")]
        public int AppType { get; set; }
    }
}
