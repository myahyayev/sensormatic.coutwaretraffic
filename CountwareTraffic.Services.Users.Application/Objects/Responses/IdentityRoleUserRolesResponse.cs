﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityRoleUserRolesResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }

        public List<IdentityRoleResponse> Data { get; set; }
    }
}
