﻿namespace CountwareTraffic.Services.Users.Application
{
    public enum LoginStatus : byte
    {
        Unknown = 0,
        FirstLogin = 1,
        AlreadyLogged = 2,
        AuthorityNull = 3,
        VerifyToken = 4,
    }
}
