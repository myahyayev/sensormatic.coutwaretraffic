﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class OneTimeReportGenerate : IDomainEvent
    {
        public Guid ReportId { get; set; }
        public Guid RecordId { get ; init; }

        public OneTimeReportGenerate(Guid reportId)
        {
            ReportId = reportId;
            RecordId = Guid.NewGuid();
        }
    }
}
