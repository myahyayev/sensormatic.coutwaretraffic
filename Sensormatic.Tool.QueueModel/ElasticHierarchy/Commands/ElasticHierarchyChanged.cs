﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record ElasticHierarchyChanged : IQueueCommand
    {
        public ElasticHierarchyChanged(Guid tenantId)
        {
            TenantId = tenantId;
        }

        public Guid TenantId { get; set; }
        public Guid RecordId { get; init; }
    }
}