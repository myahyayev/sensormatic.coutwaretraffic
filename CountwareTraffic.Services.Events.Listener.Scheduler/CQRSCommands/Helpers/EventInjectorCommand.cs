﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Events.Application;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Events.Listener.Scheduler.CQRSCommands
{

    [Contract]
    public class EventInjectorCommand : ICommand
    {
        public string MacAddress { get; set; }
        public List<MigrationEventDto> Events { get; set; }
        public EventInjectorCommand() => Events = new List<MigrationEventDto>();

    }
}
