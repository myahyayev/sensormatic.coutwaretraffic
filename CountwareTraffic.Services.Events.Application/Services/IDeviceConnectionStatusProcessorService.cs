﻿using Sensormatic.Tool.Ioc;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public interface IDeviceConnectionStatusProcessorService : ISingletonDependency
    {
        public Task ProcessDeviceConnectionStatus(DeviceDto device);
    }
}
