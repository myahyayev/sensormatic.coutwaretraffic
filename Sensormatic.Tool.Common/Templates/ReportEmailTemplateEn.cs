﻿namespace Sensormatic.Tool.Common
{
    public class ReportEmailTemplateEn : ReportEmailTemplate
    {
        public override string TemplateName => EmailTemplates.CountwareReportEmailEn;
    }
}
