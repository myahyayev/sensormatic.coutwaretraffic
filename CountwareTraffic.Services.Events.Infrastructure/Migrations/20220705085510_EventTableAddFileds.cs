﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Events.Infrastructure.Migrations
{
    public partial class EventTableAddFileds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OnTimeIOCount",
                schema: "events",
                table: "Events",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalEnterCount",
                schema: "events",
                table: "Events",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalExitCount",
                schema: "events",
                table: "Events",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OnTimeIOCount",
                schema: "events",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "TotalEnterCount",
                schema: "events",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "TotalExitCount",
                schema: "events",
                table: "Events");
        }
    }
}
