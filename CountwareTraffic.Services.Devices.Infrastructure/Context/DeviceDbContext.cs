﻿using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Ioc;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class DeviceDbContext : DbContext, IScopedSelfDependency
    {
        private readonly DeviceDbProvider _provider;
        private readonly ICurrentTenant _currentTenant;

        public DeviceDbContext(DbContextOptions options)
          : base(options) { }

        public DeviceDbContext(DbContextOptions options, DeviceDbProvider provider, ICurrentTenant currentTenant)
            : base(options)
        {
            _provider = provider;
            _currentTenant = currentTenant;
        }

        #region DBSETs
        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<SubArea> SubAreas { get; set; }
        public virtual DbSet<DeviceStatus> DeviceStatuses { get; set; }
        public virtual DbSet<DeviceType> DeviceTypes { get; set; }
        public virtual DbSet<DeviceCreationStatus> DeviceCreationStatuses { get; set; }
        public virtual DbSet<BrandLookup> BrandLookups { get; set; }
        public virtual DbSet<BrandModelLookup> BrandModelLookups { get; set; }
        public DbSet<OutboxMessage> OutboxMessages { get; set; }
        #endregion DBSETs

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies(false);

            if (_provider != null)
                optionsBuilder.AddInterceptors(new SaveChangesInterceptor(_provider));
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new DeviceEntityTypeConfiguration());
            builder.ApplyConfiguration(new SubAreaEntityTypeConfiguration());
            builder.ApplyConfiguration(new DeviceStatusEntityTypeConfiguration());
            builder.ApplyConfiguration(new DeviceTypeEntityTypeConfiguration());
            builder.ApplyConfiguration(new DeviceCreationStatusEntityTypeConfiguration());
            builder.ApplyConfiguration(new OutboxMessageEntityTypeConfiguration());
            builder.ApplyConfiguration(new BrandLookupEntityTypeConfiguration());
            builder.ApplyConfiguration(new BrandModelLookupEntityTypeConfiguration());
        }
    }
}
