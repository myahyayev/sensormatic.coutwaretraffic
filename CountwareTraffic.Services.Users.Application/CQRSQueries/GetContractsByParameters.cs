﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries
{
    public class GetContractsByParameters : IQuery<IdentityGetContractsByParametersResponse>
    {
        public string Language { get; set; }
        public int AppType { get; set; }
        public string ContractType { get; set; }
    }
}
