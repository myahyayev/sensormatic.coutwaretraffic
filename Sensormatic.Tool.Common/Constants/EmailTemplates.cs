﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sensormatic.Tool.Common
{
    public class EmailTemplates
    {
        public const string CountwareUnknownDeviceEventEmail = "UnknownDeviceEventEmail";
        public const string CountwareReportEmailEn = "ReportEmailEn";
        public const string CountwareReportEmailTr = "ReportEmailTr";
    }
}
