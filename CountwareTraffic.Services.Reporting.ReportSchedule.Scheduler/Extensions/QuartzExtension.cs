﻿using CountwareTraffic.Services.Reporting.Application;
using Newtonsoft.Json;
using Quartz;
using Sensormatic.Tool.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.ReportSchedule.Scheduler
{
    public static class QuartzExtension
    {
        public static ITrigger CreateTrigger(this JobSchedule schedule, TimeZoneSettings timeZoneSettings)
        {
            string cronSchedule = "";
            string scheduleExplain = "";
            string[] timeDataTemp = schedule.AtSetTime.Split(":");

            int hour = 23;
            if (timeDataTemp.Length >= 1)
            {
                if (!int.TryParse(timeDataTemp[0], out hour))
                    hour = 23;
                if (hour > 23)
                    hour = 23;
                if (hour < 0)
                    hour = 0;
            }

            int minute = 59;
            if (timeDataTemp.Length >= 2)
            {
                if (!int.TryParse(timeDataTemp[1], out minute))
                    minute = 59;
                if (minute > 59)
                    minute = 59;
                if (minute < 0)
                    minute = 0;
            }

            //DateTime timeData = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hour, minute, 0);
            DateTime startsOn = schedule.StartsOn.Value;
            DateTime endsOn = schedule.EndsOn.Value;
            var timeZoneInfo = TimeZoneInfo.Utc;

            if (schedule.AtSetTimeZone != null)
            {
                var timeZoneValues = JsonConvert.DeserializeObject<TimeZoneValues>(schedule.AtSetTimeZone);

                if(timeZoneSettings.ProvideByLinux)
                    timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneExtensions.TimeZoneChangeWindowsToLinux(timeZoneValues.Value));
                else
                    timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneValues.Value);
            }

            if (schedule.TemplateType == Core.TemplateType.Daily)
            {
                cronSchedule = $"0 {minute} {hour} ? * *";

                scheduleExplain = $"Every Day => {hour.ToString().PadLeft(2,'0')} : {minute.ToString().PadLeft(2, '0')}  {timeZoneInfo.DisplayName}";
            }

            else if (schedule.TemplateType == Core.TemplateType.Weekly)
            {
                int[] weekDays = JsonConvert.DeserializeObject<int[]>(schedule.RepeatOn);
                string weekDaysText = "";

                scheduleExplain = $"{WeekDaysToName(weekDays)} => {hour.ToString().PadLeft(2, '0')} : {minute.ToString().PadLeft(2, '0')}  {timeZoneInfo.DisplayName}";

                weekDays = weekDays.Select(wd => wd - 1).ToArray();
                weekDays.ToList().ForEach(wd => weekDaysText += wd + ",");

                cronSchedule = $"0 {minute} {hour} ? * { weekDaysText.TrimEnd(',')}";
            }

            else if (schedule.TemplateType == Core.TemplateType.Monthly)
            {
                MonthlyRepeat monthlyRepeat = JsonConvert.DeserializeObject<MonthlyRepeat>(schedule.RepeatOn.Trim('[').Trim(']'));

                scheduleExplain = $"{monthlyRepeat.Value} => {hour.ToString().PadLeft(2, '0')} : {minute.ToString().PadLeft(2, '0')}  {timeZoneInfo.DisplayName}";

                string cronValue = monthlyRepeat.Value.ToLower().Equals("last") ? "L" : monthlyRepeat.Value;

                cronSchedule = $"0 {minute} {hour} {cronValue} * ? ";
            }

            if (cronSchedule != string.Empty)
            {
                var triggerBuilder = TriggerBuilder
                    .Create()
                    .WithIdentity($"{schedule.JobName}.trigger", $"{schedule.TenantId}.trigger.group")
                    .WithDescription(scheduleExplain)
                    .ForJob(schedule.JobName, $"{schedule.TenantId}.group")
                    .EndAt(endsOn)
                    .WithCronSchedule(cronSchedule, x => x.InTimeZone(timeZoneInfo));

                //todo: only for release mode
                //avoid to start older date time setted scheduled jobs in release mode
#if !DEBUG
                if (startsOn > DateTime.UtcNow)
#endif
                triggerBuilder.StartAt(startsOn);
                
                return triggerBuilder.Build();
            }
            else
                return null;
        }

        public static IJobDetail CreateJob(this JobSchedule schedule)
        {
            var jobType = schedule.JobType;

            return JobBuilder
                .Create(jobType)
                .WithIdentity(schedule.JobName, $"{schedule.TenantId}.group")
                .WithDescription($"{schedule.Description} => {schedule.TemplateType.Name}")
                .Build();
        }

        private static string WeekDaysToName(int[] weekDays)
            => String.Join(", ", weekDays.Select(i => Core.ScheduleWeeklyRepeatOn.FromValue<Core.ScheduleWeeklyRepeatOn>(i).Name));

    }
}