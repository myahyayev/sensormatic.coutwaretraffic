﻿using System.Collections.Generic;

namespace Sensormatic.Tool.Core
{
    public interface ISensormaticValidate
    {
        bool IsValid { get; }
        ICollection<ErrorResult> ValidateResults { get; }
    }
}
