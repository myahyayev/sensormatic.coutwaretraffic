﻿using Microsoft.AspNetCore.Mvc;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class TokenApiRequest :  SensormaticRequestValidate
    {
        [FromForm(Name = "username")]
        public string UserName { get; set; }

        [FromForm(Name = "password")]
        public string Password { get; set; }

        [FromForm(Name = "tenantId")]
        public Guid? TenantId { get; set; }

        public override void Validate()
        {
            if (UserName.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1249 Value cannot be null", nameof(UserName)));

            if (Password.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1250 Value cannot be null", nameof(Password)));
        }
    }
}
