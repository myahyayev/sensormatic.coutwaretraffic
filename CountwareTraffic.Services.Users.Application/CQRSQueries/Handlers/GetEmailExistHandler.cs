﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetEmailExistHandler : IQueryHandler<GetEmailExist, IdentityUserNameExistResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetEmailExistHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<IdentityUserNameExistResponse> HandleAsync(GetEmailExist query)
        {
            return await _identityServerService.GetEmailExistAsync(query.Email);
        }
    }
}
