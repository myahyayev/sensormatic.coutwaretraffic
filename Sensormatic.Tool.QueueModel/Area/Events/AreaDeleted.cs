﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record AreaDeleted : MessageEnvelope, IQueueEvent
    {
        public AreaDeleted(Guid areaId, Guid recordId, Guid userId, string correlationId, Guid tenantId)
             : base(userId, correlationId, tenantId)
        {
            AreaId = areaId;
            RecordId = recordId;
        }
        public Guid AreaId { get; }
        public Guid RecordId { get; init; }
    }
}
