﻿using Convey.CQRS.Queries;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserGrid : IQuery<IdentityUserGridResponse> 
    {
        public PagingQuery PagingQuery { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }

    }
}
