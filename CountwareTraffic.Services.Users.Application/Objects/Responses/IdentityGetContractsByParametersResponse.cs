﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application.Objects.Responses
{
    public class IdentityGetContractsByParametersResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public List<IdentityGetContractsByParametersItem> Data { get; set; }
    }

    public class IdentityGetContractsByParametersItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string ContractType { get; set; }
        public int AppType { get; set; }
    }
}
