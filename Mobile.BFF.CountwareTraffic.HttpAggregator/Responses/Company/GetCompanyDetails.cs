﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetCompanyDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public string EmailAddress { get; set; }
        public string GsmNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string GsmDialCode { get; set; }
        public string PhoneDialCode { get; set; }
        public string PersonName { get; set; }
        public string GsmDialCodeSecondary { get; set; }
        public string GsmNumberSecondary { get; set; }
        public string PhoneDialCodeSecondary { get; set; }
        public string PhoneNumberSecondary { get; set; }
        public string EmailAddressSecondary { get; set; }
        public string PersonNameSecondary { get; set; }
        public string GsmCountryCode { get; set; }
        public string PhoneCountryCode { get; set; }
        public string GsmCountryCodeSecondary { get; set; }
        public string PhoneCountryCodeSecondary { get; set; }
    }
}
