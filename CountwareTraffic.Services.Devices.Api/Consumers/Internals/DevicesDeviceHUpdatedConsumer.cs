﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Devices.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Api
{
    public class DevicesDeviceHUpdatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.DeviceHUpdated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public DevicesDeviceHUpdatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.DeviceHUpdated queuEvent)
        {
            var deviceHUpdated = _eventMapper.Map(queuEvent) as DeviceHUpdated;
            await _eventDispatcher.PublishAsync(deviceHUpdated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
