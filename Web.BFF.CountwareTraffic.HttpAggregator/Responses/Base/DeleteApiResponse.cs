﻿using System;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class DeleteApiResponse
    {
        public Guid Id { get; set; }
        public string Deleted { get; set; }
    }
}
