﻿using System;
using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application
{
    public class AreaDetail
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("level")]
        public string Level { get; set; }
    }
}
