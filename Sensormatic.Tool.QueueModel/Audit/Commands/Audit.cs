﻿using Newtonsoft.Json;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;

namespace Sensormatic.Tool.QueueModel
{
    public record AuditList<T> : MessageEnvelope, IQueueCommand
    {
        public AuditList(List<T> items, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            Items = items;
            RecordId = Guid.NewGuid();
        }

        [JsonProperty("ITEMS")]
        public List<T> Items { get;}
        public Guid RecordId { get; init; }
    }
}
