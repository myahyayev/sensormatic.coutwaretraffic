﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Api
{
    public class AutoDeleteDeviceEventsConsumer : IConsumer<Sensormatic.Tool.QueueModel.DeviceDeletedSuccessfully>, ITransientSelfDependency
    {
        private readonly IQueueService _queueService;
        public AutoDeleteDeviceEventsConsumer(IQueueService queueService) => _queueService = queueService;

        public Task ConsumeAsync(Sensormatic.Tool.QueueModel.DeviceDeletedSuccessfully queuEvent)
        {
            //device event queue
            string EventQueueName = string.Format(Queues.CountwareTrafficEventsDeviceEventsListener, queuEvent.DeviceId);

            _queueService.DeleteQueue(EventQueueName);
            _queueService.DeleteExchange($"dlx.{EventQueueName}");

            //device event migration queue
            string migrationQueueName = string.Format(Queues.CountwareTrafficEventsDeviceMigrationEventsListener, queuEvent.DeviceId);

            _queueService.DeleteQueue(migrationQueueName);
            _queueService.DeleteExchange($"dlx.{migrationQueueName}");

            return Task.CompletedTask;
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
