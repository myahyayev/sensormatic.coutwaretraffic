﻿using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityResetPasswordWithTokenResponse
    {

        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
