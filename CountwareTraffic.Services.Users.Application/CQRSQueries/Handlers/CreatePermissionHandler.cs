﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreatePermissionHandler : IQueryHandler<CreatePermission, IdentityCreatePermissionResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public CreatePermissionHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityCreatePermissionResponse> HandleAsync(CreatePermission query)
        {
            return await _identityServerService.CreatePermissionAsync(new IdentityCreatePermissionsRequest
            {
                Name = query.Name,
                Description = query.Description
            });
        }
    }
}
