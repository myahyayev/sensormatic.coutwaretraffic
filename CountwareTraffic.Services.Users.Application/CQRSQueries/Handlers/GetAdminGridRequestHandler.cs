﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class GetAdminGridRequestHandler : IQueryHandler<GetAdminGridRequest, IdentityAdminGridResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetAdminGridRequestHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityAdminGridResponse> HandleAsync(GetAdminGridRequest query)
        {
            return await _identityServerService.GetAdminGridAsync(new IdentityAdminGridRequest
            {
                Filters = query.Filters,
                Paging = query.PagingQuery,
                Sorts = query.Sorts,
            });
        }
    }
}
