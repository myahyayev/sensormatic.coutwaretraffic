﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesAreaHDeletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaHDeleted>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesAreaHDeletedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaHDeleted queuEvent)
        {
            var areaDeleted = _eventMapper.Map(queuEvent) as AreaHDeleted;
            await _eventDispatcher.PublishAsync(areaDeleted);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
