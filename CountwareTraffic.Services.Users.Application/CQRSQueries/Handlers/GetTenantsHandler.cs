﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantsHandler : IQueryHandler<GetTenants, IdentityTenantsResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetTenantsHandler(IIdentityServerService identityServerService) => _identityServerService = identityServerService;

        public async Task<IdentityTenantsResponse> HandleAsync(GetTenants query) => await _identityServerService.GetTenantsAsync();
    }
}
