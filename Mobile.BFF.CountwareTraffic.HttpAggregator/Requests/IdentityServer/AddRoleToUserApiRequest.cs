﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AddRoleToUserApiRequest
    {
        [FromForm(Name = "userId")]
        public Guid UserId { get; set; }

        [FromForm(Name = "roleName")]
        public string RoleName { get; set; }
    }
}
