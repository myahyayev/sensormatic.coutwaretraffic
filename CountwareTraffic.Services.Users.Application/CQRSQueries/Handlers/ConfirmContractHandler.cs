﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.CQRSQueries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ConfirmContractHandler : IQueryHandler<ConfirmContract, IdentityConfirmContractResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public ConfirmContractHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityConfirmContractResponse> HandleAsync(ConfirmContract query)
        {
            return await _identityServerService.ConfirmContractAsync(query.ContractId);
        }
    }
}
