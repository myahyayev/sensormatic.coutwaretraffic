﻿using System;
using Microsoft.AspNetCore.Authorization;

namespace Countware.Traffic.CrossCC.PolicyProvider
{
    public class PermissionsRequirement : IAuthorizationRequirement, IIdentifiable
    {
        public string Permissions { get; }
        public Guid Identifier { get; set; }

        public PermissionsRequirement(string permissions, Guid identifier)
        {
            Permissions = permissions;
            Identifier = identifier;
        }
    }
}
