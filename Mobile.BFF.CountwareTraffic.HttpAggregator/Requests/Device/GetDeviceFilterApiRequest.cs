﻿using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetDeviceFilterApiRequest
    {
        public List<HierarchyTreeSelectFilterRequest> HierarchyTreeSelectFilters { get; set; }
        public DataSourceApiRequest DataSourceApiRequest { get; set; }
    }
}
