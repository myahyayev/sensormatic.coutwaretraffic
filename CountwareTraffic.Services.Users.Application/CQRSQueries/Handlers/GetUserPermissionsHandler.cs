﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetUserPermissionsHandler : IQueryHandler<GetUserPermissions, IdentityUserPermissionsResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetUserPermissionsHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityUserPermissionsResponse> HandleAsync(GetUserPermissions query)
        {
            return await _identityServerService.GetUserPermissionsAsync(query.UserId, query.Language);
        }
    }
}
