﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer
{
    public class AdminPaginationApiRequest
    {
        public PagingQuery Paging { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }

        public AdminPaginationApiRequest()
        {
            Paging = new PagingQuery();
            Filters = new List<GridFilter>();
            Sorts = new List<SortDescriptor>();
        }
    }
}
