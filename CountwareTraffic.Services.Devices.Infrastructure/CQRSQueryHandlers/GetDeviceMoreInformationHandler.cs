﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class GetDeviceMoreInformationHandler : IQueryHandler<GetDeviceMoreInformation, DeviceMoreInformationDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        private readonly IIdentityService _identityService;
        private readonly ICorrelationService _correlationService;
        public GetDeviceMoreInformationHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant, IIdentityService identityService, ICorrelationService correlationService)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
            _identityService = identityService;
            _correlationService = correlationService;
        }

        public async Task<DeviceMoreInformationDto> HandleAsync(GetDeviceMoreInformation query)
        {
            DeviceMoreInformationDto response = new();

            var sampleQueueModelJson = Newtonsoft.Json.JsonConvert.SerializeObject(new DeviceEventsListener
                (
                $"Total enter count: 111, Total exit count: 112, Current count: 1, On time io count: 1", 
                query.DeviceId,
                2,
                1,
                DateTime.Now, 
                DateTime.Now, 
                DateTime.Now, 
                Guid.NewGuid(), 
                Guid.NewGuid().ToString(),
                _currentTenant.Id)
                );


            response.DeviceQueue = new DeviceQueue
            {
                QueueName = string.Format(Sensormatic.Tool.Common.Queues.CountwareTrafficEventsDeviceEventsListener, query.DeviceId),
                QueueModel = sampleQueueModelJson
            };

            response.DeviceEndpoint = new DeviceEndpoint
            {
                EndpointAddress = $"http://event-listener.test.sensormatic.com.tr/api/v1/devices/realtime/events/xml",
                SampleRequest = @"<?xml version=""1.0""?>
<RealTimeMetrics  SiteId=""Sample SiteId"">
<Properties>
<Version>5</Version>
<TransmitTime>1656692940</TransmitTime>
<MacAddress>device mac address</MacAddress>
<IpAddress>device ip address</IpAddress>
<HostName>Cam-19353767</HostName>
<HttpPort>80</HttpPort>
<HttpsPort>443</HttpsPort>
<Timezone>3</Timezone>
<TimezoneName>(GMT 03:00) Nairobi</TimezoneName>
<DST>0</DST>
<HwPlatform>2310</HwPlatform>
<SerialNumber>19353767</SerialNumber>
<DeviceType>0</DeviceType>
<SwRelease>5.9.21.621</SwRelease>
</Properties>
<RTReport  Date=""2022-07-01T19:29:00"">
<RTObject  Id=""0"" DeviceId =""Sample DeviceId"" Devicename =""Sample DeviceName"" ObjectType =""0"" Name =""Sample AzureName"" ExternalId =""2"" >
<RTCount  TotalEnters=""152"" TotalExits =""155"" />
</RTObject>
</RTReport>
</RealTimeMetrics>"
            };

            var deviceHierarchy = await _unitOfWork.GetRepository<IDeviceRepository>().GetDeviceHierarchyAsync(query.DeviceId, _currentTenant.Id);

            response.DeviceHierarchy = new DeviceHierarchy
            {
                CompanyId = deviceHierarchy.CompanyId,
                CompanyName = deviceHierarchy.CompanyName,
                CountryName = deviceHierarchy.CountryName,
                CountryId = deviceHierarchy.CountryId,
                RegionId = deviceHierarchy.RegionId,
                RegionName = deviceHierarchy.RegionName,
                AreaName = deviceHierarchy.AreaName,
                AreaId = deviceHierarchy.AreaId,
                SubAreaName = deviceHierarchy.SubAreaName,
                SubAreaId = deviceHierarchy.SubAreaId
            };

            return response;
        }
    }
}