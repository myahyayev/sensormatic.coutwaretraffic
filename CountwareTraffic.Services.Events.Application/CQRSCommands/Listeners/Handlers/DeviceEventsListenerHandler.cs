﻿using Convey.CQRS.Commands;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class DeviceEventsListenerHandler : ICommandHandler<DeviceEventsListener>
    {
        private readonly IQueueService _queueService;
        public DeviceEventsListenerHandler(IQueueService queueService) => _queueService = queueService;

        public async Task HandleAsync(DeviceEventsListener command)
        {
            string queueName = string.Format(String.Format(Queues.CountwareTrafficEventsDeviceEventsListener, command.DeviceId));

            Sensormatic.Tool.QueueModel.DeviceEventsListener deviceEventsListener = new(command.Description, command.DeviceId, command.DirectionTypeId, command.OnTimeIOCount, command.EventDate, command.EventStartDate, command.EventEndDate, command.UserId, String.Empty, command.TenantId);

            _queueService.Send(queueName, deviceEventsListener);
        }
    }
}
