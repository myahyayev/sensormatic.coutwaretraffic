﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public interface IReportNotificationsHub
    {
        Task JoinReportProcess(string groupName);
        Task ReportGeneratingCompletedReceived(ReportGeneratingCompleted command);
        Task ReportGeneratingFiredReceived(ReportGeneratingFired command);
        Task ReportGeneratingFailedReceived(ReportGeneratingFailed command);
    }


    //[Authorize]
    public class ReportNotificationsHub : Hub<IReportNotificationsHub>
    {
        public async Task JoinReportProcess(string groupName)
            => await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

        public async Task ReportGeneratingCompletedReceived(ReportGeneratingCompleted command)
            => await Clients.Group(command.TenantId.ToString()).ReportGeneratingCompletedReceived(command);

        public async Task ReportGeneratingFiredReceived(ReportGeneratingFired command)
           => await Clients.Group(command.TenantId.ToString()).ReportGeneratingFiredReceived(command);

        public async Task ReportGeneratingFailedReceived(ReportGeneratingFailed command)
            => await Clients.Group(command.TenantId.ToString()).ReportGeneratingFailedReceived(command);
    }
}
