﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportNotFoundException : AppException
    {
        public Guid Id { get; }
        public ReportNotFoundException(Guid id)
            : base(new List<ErrorResult>() { new ErrorResult($"Report with id: {id} was not found") }, 404, ResponseMessageType.Error)
        {
            Id = id;
        }
    }
}
