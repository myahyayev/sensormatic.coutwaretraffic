﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class GetReportTemplateViewHandler : IQueryHandler<GetReportTemplateView, ReportTemplateViewDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetReportTemplateViewHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<ReportTemplateViewDto> HandleAsync(GetReportTemplateView query)
        {
            var repository = _unitOfWork.GetRepository<IReportTemplateRepository>();

            var reportTemplate = await repository.GetByIdAsync(query.ReportTemplateId);

            if (reportTemplate == null)
                throw new ReportTemplateNotFoundException(query.ReportTemplateId);

            return new ReportTemplateViewDto
            {
                Id = reportTemplate.Id,
                FilePath = reportTemplate.FilePath
            };
        }
    }
}
