﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class DeviceDeletedFailedHandler : ICommandHandler<DeviceDeletedFailed>
    {
        private readonly IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> _hubContext;
        public DeviceDeletedFailedHandler(IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> hubContext) 
            => _hubContext = hubContext;


        public async Task HandleAsync(DeviceDeletedFailed command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .DeviceDeletedFailedReceived(command);
    }
}
