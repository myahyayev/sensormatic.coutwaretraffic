﻿using System;

namespace Sensormatic.Tool.ElasticSearch
{
    public interface IMultiTenant
    {
        public Guid TenantId { get; set; }
    }
}
