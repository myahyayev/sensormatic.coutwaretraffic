﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System;

namespace CountwareTraffic.Services.Users.Application.CQRSCommands
{
    public class UpdateContract : IQuery<IdentityUpdateContractResponse>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Version { get; set; }

        public DateTime ReleaseDate { get; set; }

        public string ContractBody { get; set; }

        public string ContractType { get; set; }

        public string Language { get; set; }

        public int AppType { get; set; }
    }
}
