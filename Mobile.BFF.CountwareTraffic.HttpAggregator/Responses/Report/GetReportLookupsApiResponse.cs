﻿
using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetReportLookupsApiResponse
    {
        public IEnumerable<EnumResponse> ReportFormats { get; set; }
        public IEnumerable<EnumResponse> ScheduleFrequencies { get; set; }
        public IEnumerable<EnumResponse> ScheduleWeeklyRepeatOns { get; set; }
        public IEnumerable<EnumResponse> ScheduleDeliveryTimes { get; set; }
    }
}
