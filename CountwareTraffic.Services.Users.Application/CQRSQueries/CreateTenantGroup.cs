﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreateTenantGroup : IQuery<IdentityCreateTenantGroupResponse>
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
