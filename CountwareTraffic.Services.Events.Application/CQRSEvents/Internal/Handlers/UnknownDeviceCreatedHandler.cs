﻿using Convey.CQRS.Events;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application
{
    public class UnknownDeviceCreatedHandler : IEventHandler<UnknownDeviceCreated>
    {
        private readonly IQueueService _queueService;
        private readonly IIdentityService _identityService;
        public UnknownDeviceCreatedHandler( IQueueService queueService, IIdentityService identityService)
        {
            _queueService = queueService;
            _identityService = identityService;
        }

        public async Task HandleAsync(UnknownDeviceCreated @event)
        {
            //unknown device icin MAIL atma islemleri
            var emailCommand = new SendTemplatedEmail
            {
                EmailTemplate = new UnknownDeviceEventEmailTemplate
                {
                    DeviceName = @event.DeviceName,
                    HttpPort = @event.HttpPort.ToString(),
                    HttpsPort = @event.HttpsPort.ToString(),
                    IpAddress = @event.IpAddress,
                    MacAddress = @event.MacAddress,
                    SerialNumber = @event.SerialNumber,
                    UserIds = new System.Collections.Generic.List<Guid>
                    {
                        _identityService.UserId
                    }
                },
                EmailTemplateAssemblyName = typeof(UnknownDeviceEventEmailTemplate).AssemblyQualifiedName,
            };

            _queueService.Send(Queues.CountwareTrafficSendTemplatedEmail, emailCommand);



            //unknown device icin SignalR islemleri
            UnknownDeviceCreatedSuccessfully unknownDeviceCreatedSuccessfully = new(@event.DeviceName, @event.MacAddress, @event.IpAddress, @event.SerialNumber, @event.HttpPort, @event.HttpsPort);

            _queueService.Send(Queues.CountwareTrafficSignalRUnknownDeviceCreatedSuccessfully, unknownDeviceCreatedSuccessfully);
        }
    }
}

