﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.Company
{
    public class GetCompaniesByTenantIdRequest
    {
        public Guid TenantId { get; set; }
    }
}
