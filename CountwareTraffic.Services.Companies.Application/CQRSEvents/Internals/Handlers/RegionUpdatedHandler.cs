﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class RegionUpdatedHandler : IEventHandler<RegionUpdated>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public RegionUpdatedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(RegionUpdated command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId,
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId,
            };

            CountryElasticDto countryElasticDto = new()
            {
                Id = command.CountryId,
            };

            RegionElasticDto regionElasticDto = new()
            {
                Id = command.RegionId,
                Name = command.RegionName
            };

            await _tenantHierarchyElasticSearchRepository.UpdateRegionAsync(tenantElasticDto, companyElasticDto, countryElasticDto, regionElasticDto);
        }
    }
}

