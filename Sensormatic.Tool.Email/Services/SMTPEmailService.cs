﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sensormatic.Tool.Email
{

    public class SMTPEmailService : IEmailService
    {
        private readonly SmtpOptions _smtpOptions;
        private SmtpClient _smtpClient;
        private readonly ILogger<SMTPEmailService> _logger;

        public SMTPEmailService(IOptions<SmtpOptions> smtpOptions, ILogger<SMTPEmailService> logger)
        {
            _smtpOptions = smtpOptions.Value;
            _smtpClient = new SmtpClient();
            _smtpClient.Connect(_smtpOptions.Server, _smtpOptions.Port, true);
            _smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");
            _smtpClient.Authenticate(_smtpOptions.User, _smtpOptions.Password);
            _logger = logger;
        }

        public async Task<EmailResponse> SendAsync(EmailRequest request)
        {
            var email = new MimeMessage
            {
                Sender = MailboxAddress.Parse(_smtpOptions.User),
                Subject = request.Subject
            };

            if (request.To != null && request.To.Count > 0)
                request.To.ToList().ForEach(x => email.To.Add(MailboxAddress.Parse(x)));

            if (request.Cc != null && request.Cc.Count > 0)
                request.Cc.ToList().ForEach(x => email.Cc.Add(MailboxAddress.Parse(x)));

            if (request.Bc != null && request.Bc.Count > 0)
                request.Bc.ToList().ForEach(x => email.Bcc.Add(MailboxAddress.Parse(x)));


            var builder = new BodyBuilder();

            if (request.Attachments != null)
            {
                byte[] fileBytes;

                foreach (var file in request.Attachments)
                {
                    if (file.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            file.CopyTo(ms);
                            fileBytes = ms.ToArray();
                        }

                        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                    }
                }
            }

            if (request.IsHtml)
                builder.HtmlBody = request.Body;
            else
                builder.TextBody = request.Body;

            email.Body = builder.ToMessageBody();

            if (!_smtpClient.IsConnected)
            {
                _smtpClient.Connect(_smtpOptions.Server, _smtpOptions.Port, true);
                _smtpClient.AuthenticationMechanisms.Remove("XOAUTH2");
                _smtpClient.Authenticate(_smtpOptions.User, _smtpOptions.Password);
            }

            await _smtpClient.SendAsync(email);

            _logger.LogInformation($"Email successfully send to {{EmailAddresses}} Email: {{EmailBody}}", string.Join(',', request.To), request.Body);

            return new EmailResponse { IsSuccess = true, Message = "Success" };
        }


        public void Dispose() => GC.SuppressFinalize(this);
    }
}
