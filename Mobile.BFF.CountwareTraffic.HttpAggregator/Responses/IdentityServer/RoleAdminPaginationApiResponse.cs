﻿using System.Collections.Generic;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer
{
    public class RoleAdminPaginationApiResponse
    {
        public int TotalCount { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
        public int HasNextPage { get; set; }
        public int Next { get; set; }
        public int Prev { get; set; }
        public List<RoleAdminPermissionResponse> Data { get; set; }
    }

    public class RoleAdminPermissionResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<RoleAdminPermissionItemResponse> Permissions { get; set; }
    }

    public class RoleAdminPermissionItemResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public string DescriptionLabel { get; set; }
    }
}
