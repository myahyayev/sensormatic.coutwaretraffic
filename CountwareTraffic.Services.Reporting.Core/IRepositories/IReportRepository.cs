﻿using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Core
{
    public interface IReportRepository : IRepository<Report>, IScopedDependency
    {
        Task<QueryablePagingValue<ReportDetailEntityDto>> GetAllReports(PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts);
        Task<ReportDetailEntityDto> GetReportByIdAsync(Guid reportId, Guid? tenantId = null, bool includeIsActive = true);
        Task<ReportDetailEntityDto> GetReportByIdAsync(Guid reportId);
        Task<Report> GetAsync(Guid id);
    }
}
