﻿using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportLanguageEntityDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string WindowsTimeZone { get; set; }
        public string LinuxTimeZone { get; set; }
    }
}
