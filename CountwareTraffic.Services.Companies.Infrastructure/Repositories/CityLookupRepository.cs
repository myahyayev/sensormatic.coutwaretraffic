﻿using CountwareTraffic.Services.Companies.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class CityLookupRepository : Repository<CityLookup>, ICityLookupRepository
    {
        private readonly new AreaDbContext _context;

        public CityLookupRepository(AreaDbContext context) : base(context) => _context = context;

        public async Task<IEnumerable<CityLookup>> GetAllAsync(Guid countryId) => await _context.CityLookups.Where(u => u.CountryId == countryId).OrderBy(u => u.Name).ToListAsync();

        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible
    }
}
