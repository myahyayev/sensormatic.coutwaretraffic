﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetCityLookupsHandler : IQueryHandler<GetCityLookups, List<CityLookupDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetCityLookupsHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<List<CityLookupDetailsDto>> HandleAsync(GetCityLookups query)
        {
            var cityLookups = await _unitOfWork.GetRepository<ICityLookupRepository>().GetAllAsync(query.CountryId);

            if (cityLookups == null) return null;

            return cityLookups.Select(cityLookup => new CityLookupDetailsDto
            {
                Id = cityLookup.Id,
                Name = cityLookup.Name,
                CountryId = cityLookup.CountryId
            }).ToList();
        }
    }
}
