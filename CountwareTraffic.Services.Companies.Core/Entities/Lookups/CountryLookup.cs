﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class CountryLookup : Entity<Guid>
    {
        private string _iso;
        private string _iso3;
        private int _isoNumeric;
        private string _name;
        private string _capital;
        private string _continentCode;
        private string _currencyCode;

        public string Iso => _iso;
        public string Iso3 => _iso3;
        public int IsoNumeric => _isoNumeric;
        public string Name => _name;
        public string Capital => _capital;
        public string ContinentCode => _continentCode;
        public string CurrencyCode => _currencyCode;

        public CountryLookup()
        {
            _id = SequentialGuid.Current.Next(null);
        }       
    }
}
