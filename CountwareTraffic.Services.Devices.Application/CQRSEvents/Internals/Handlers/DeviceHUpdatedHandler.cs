﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeviceHUpdatedHandler : IEventHandler<DeviceHUpdated>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public DeviceHUpdatedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(DeviceHUpdated command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId
            };

            CountryElasticDto countryElasticDto = new()
            {
                Id = command.CountryId
            };

            RegionElasticDto regionElasticDto = new()
            {
                Id = command.RegionId
            };

            AreaElasticDto areaElasticDto = new()
            {
                Id = command.AreaId
            };

            SubAreaElasticDto subAreaElasticDto = new()
            {
                Id = command.SubAreaId,
            };

            DeviceElasticDto deviceElasticDto = new()
            {
                Id = command.DeviceId,
                Name = command.DeviceName
            };

            await _tenantHierarchyElasticSearchRepository.UpdateDeviceAsync(tenantElasticDto, companyElasticDto, countryElasticDto, regionElasticDto, areaElasticDto, subAreaElasticDto, deviceElasticDto);
        }
    }
}

