﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class CitiesController : BaseController
    {
        private readonly CityService _cityService;
        public CitiesController(IServiceProvider provider, ILogger<CitiesController> logger, IHttpContextAccessor contextAccessor, CityService cityService) 
            : base(provider, logger, contextAccessor.HttpContext) => _cityService = cityService;


        [HttpGet("{cityId}/detail"), ServiceLog]
        public async Task<ApiResponse<GetCityDetails>> GetCityDetailAsync(Guid cityId)
            => Response(await _cityService.GetCityByIdAsync(cityId), ResultMessage.SuccessResultMessage);


        [HttpPost("{countryId}/details"), ServiceLog]
        public async Task<ApiPagedResponse<GetCityDetails>> GetCityDetailsAsync(Guid countryId, [FromBody] DataSourceApiRequest paging)
            => await _cityService.GetCitiesAsync(countryId, paging);


        [HttpPost("{countryId}/add"), ServiceLog]
        public async Task<ApiResponse<AddApiResponse>> AddCityAsync(Guid countryId, [FromForm] AddCityApiRequest request)
        {
            await _cityService.AddCityAsync(countryId, request);
            return Response(new AddApiResponse { Created = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpPut("{cityId}/change"), ServiceLog]
        public async Task<ApiResponse<ChangeApiResponse>> ChangeCityAsync(Guid cityId, [FromForm] ChangeCityApiRequest request)
        {
            await _cityService.ChangeCityAsync(cityId, request);
            return Response(new ChangeApiResponse { Updated = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpDelete("{cityId}/delete"), ServiceLog]
        public async Task<ApiResponse<DeleteApiResponse>> DeleteCityAsync(Guid cityId)
        {
            await _cityService.DeleteCityAsync(cityId);
            return Response(new DeleteApiResponse { Deleted = "Success" }, ResultMessage.SuccessResultMessage);
        }
    }
}
