﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application.Objects.Requests
{
    public class IdentityRoleAdminPaginationRequest
    {
        public PagingQuery Paging { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
        public IdentityRoleAdminPaginationRequest()
        {
            Paging = new PagingQuery();
            Filters = new List<GridFilter>();
            Sorts = new List<SortDescriptor>();
        }
    }
}
