﻿using CountwareTraffic.Services.Events.Grpc;
using Google.Protobuf.WellKnownTypes;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class TrafficService : IScopedSelfDependency
    {
        private readonly Traffic.TrafficClient _trafficClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;
        public TrafficService(Traffic.TrafficClient trafficClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _trafficClient = trafficClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetWidgetPerformingApiResponse> GetWidgetPerformingAsync(GetWidgetPerformingApiRequest request)
        {
            GetWidgetPerformingRequest grpcRequest = new()
            {
                CurrentStartDate = Timestamp.FromDateTimeOffset(request.CurrentStartDate),
                CurrentEndDate = Timestamp.FromDateTimeOffset(request.CurrentEndDate),
                PreviusStartDate = Timestamp.FromDateTimeOffset(request.PreviusStartDate),
                PreviusEndDate = Timestamp.FromDateTimeOffset(request.PreviusEndDate)
            };

            if (request.HierarchyTreeSelectFilters != null && request.HierarchyTreeSelectFilters.Count > 0)
            {
                foreach (var hierarchyTreeSelectFilter in request.HierarchyTreeSelectFilters)
                {
                    global::CountwareTraffic.Services.Events.Grpc.HierarchyTreeSelectFilterRequest hierarchyTreeSelectFilterRequest = new();

                    hierarchyTreeSelectFilterRequest.NodeFilter = new()
                    {
                        HierarchyLevel = hierarchyTreeSelectFilter.HierarchyLevel switch
                        {
                            HierarchyLevel.Tenant => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant,
                            HierarchyLevel.Company => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Company,
                            HierarchyLevel.Country => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Country,
                            HierarchyLevel.Region => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Region,
                            HierarchyLevel.Area => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Area,
                            HierarchyLevel.SubArea => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.SubArea,
                            HierarchyLevel.Device => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Device,
                            _ => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant
                        },
                        Id = hierarchyTreeSelectFilter.Id.ToString()
                    };

                    grpcRequest.HierarchyTreeSelectFilters.Add(hierarchyTreeSelectFilterRequest);
                }
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_trafficClient.GetWidgetPerformingAsync, grpcRequest, hasClientSideLog: false);

            return new GetWidgetPerformingApiResponse
            {
                InwardsCurrent = grpcResponse.InwardsCurrent,
                InwardsPrevius = grpcResponse.InwardsPrevius
            };
        }

        public async Task<IEnumerable<GetPerformingByTypeApiResponse>> GetPerformingByTypeAsync(GetPerformingByTypeApiRequest request)
        {
            GetPerformingByTypeRequest grpcRequest = new()
            {
                StartDate = Timestamp.FromDateTimeOffset(request.StartDate),
                EndDate = Timestamp.FromDateTimeOffset(request.EndDate),
                Size = request.Size,
                Direction = request.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Events.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Events.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                },
                PerformType = request.PerformType switch
                {
                    PerformType.Tenant => global::CountwareTraffic.Services.Events.Grpc.PerformType.ByTenant,
                    PerformType.Company => global::CountwareTraffic.Services.Events.Grpc.PerformType.ByCompany,
                    PerformType.Country => global::CountwareTraffic.Services.Events.Grpc.PerformType.ByCountry,
                    PerformType.Region => global::CountwareTraffic.Services.Events.Grpc.PerformType.ByRegion,
                    PerformType.Area => global::CountwareTraffic.Services.Events.Grpc.PerformType.ByArea,
                    PerformType.SubArea => global::CountwareTraffic.Services.Events.Grpc.PerformType.BySubArea,
                    PerformType.Device => global::CountwareTraffic.Services.Events.Grpc.PerformType.ByDevice,
                    _ => global::CountwareTraffic.Services.Events.Grpc.PerformType.ByDevice,
                }
            };

            if (request.HierarchyTreeSelectFilters != null && request.HierarchyTreeSelectFilters.Count > 0)
            {
                foreach (var hierarchyTreeSelectFilter in request.HierarchyTreeSelectFilters)
                {
                    global::CountwareTraffic.Services.Events.Grpc.HierarchyTreeSelectFilterRequest hierarchyTreeSelectFilterRequest = new();

                    hierarchyTreeSelectFilterRequest.NodeFilter = new()
                    {
                        HierarchyLevel = hierarchyTreeSelectFilter.HierarchyLevel switch
                        {
                            HierarchyLevel.Tenant => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant,
                            HierarchyLevel.Company => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Company,
                            HierarchyLevel.Country => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Country,
                            HierarchyLevel.Region => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Region,
                            HierarchyLevel.Area => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Area,
                            HierarchyLevel.SubArea => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.SubArea,
                            HierarchyLevel.Device => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Device,
                            _ => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant
                        },
                        Id = hierarchyTreeSelectFilter.Id.ToString()
                    };

                    grpcRequest.HierarchyTreeSelectFilters.Add(hierarchyTreeSelectFilterRequest);
                }
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_trafficClient.GetPerformingByTypeAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.PerformingsByType.Select(u => new GetPerformingByTypeApiResponse
            {
                Inwards = u.Inwards,
                Name = u.Name,
                Id = new Guid(u.Id)
            });
        }

        public async Task<GetTrafficByTimePeriodApiResponse> GetTrafficByTimePeriodAsync(GetTrafficByTimePeriodApiRequest request, string language)
        {
            GetTrafficByTimePeriodRequest grpcRequest = new()
            {
                CurrentStartDate = Timestamp.FromDateTimeOffset(request.CurrentStartDate),
                CurrentEndDate = Timestamp.FromDateTimeOffset(request.CurrentEndDate),
                PreviusStartDate = Timestamp.FromDateTimeOffset(request.PreviusStartDate),
                PreviusEndDate = Timestamp.FromDateTimeOffset(request.PreviusEndDate),
                Language = language,

                TimePeriod = request.TimePeriod switch
                {
                    TimePeriod.Hourly => global::CountwareTraffic.Services.Events.Grpc.TimePeriod.Hourly,
                    TimePeriod.Daily => global::CountwareTraffic.Services.Events.Grpc.TimePeriod.Daily,
                    TimePeriod.Monthly => global::CountwareTraffic.Services.Events.Grpc.TimePeriod.Monthly,
                    _ => throw new NotImplementedException(),
                }
            };

            if (request.HierarchyTreeSelectFilters != null && request.HierarchyTreeSelectFilters.Count > 0)
            {
                foreach (var hierarchyTreeSelectFilter in request.HierarchyTreeSelectFilters)
                {
                    global::CountwareTraffic.Services.Events.Grpc.HierarchyTreeSelectFilterRequest hierarchyTreeSelectFilterRequest = new();

                    hierarchyTreeSelectFilterRequest.NodeFilter = new()
                    {
                        HierarchyLevel = hierarchyTreeSelectFilter.HierarchyLevel switch
                        {
                            HierarchyLevel.Tenant => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant,
                            HierarchyLevel.Company => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Company,
                            HierarchyLevel.Country => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Country,
                            HierarchyLevel.Region => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Region,
                            HierarchyLevel.Area => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Area,
                            HierarchyLevel.SubArea => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.SubArea,
                            HierarchyLevel.Device => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Device,
                            _ => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant
                        },
                        Id = hierarchyTreeSelectFilter.Id.ToString()
                    };

                    grpcRequest.HierarchyTreeSelectFilters.Add(hierarchyTreeSelectFilterRequest);
                }
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_trafficClient.GetTrafficByTimePeriodAsync, grpcRequest, hasClientSideLog: false);


            GetTrafficByTimePeriodApiResponse response = new();

            response.Categories = grpcResponse.Categories.Select(u => u);

            response.Series = grpcResponse.Series.Select(u => new Series
            {
                Color = u.Color,
                Name = u.Name,
                Data = u.Data.Select(u => u)
            });

            return response;
        }

        public async Task<GetCountOfVisitorsInsideApiResponse> GetCountOfVisitorsInsideAsync(GetCountOfVisitorsInsideApiRequest request)
        {
            GetCountOfVisitorsInsideRequest grpcRequest = new()
            {
                CurrentStartDate = Timestamp.FromDateTimeOffset(request.CurrentStartDate),
            };

            if (request.HierarchyTreeSelectFilters != null && request.HierarchyTreeSelectFilters.Count > 0)
            {
                foreach (var hierarchyTreeSelectFilter in request.HierarchyTreeSelectFilters)
                {
                    global::CountwareTraffic.Services.Events.Grpc.HierarchyTreeSelectFilterRequest hierarchyTreeSelectFilterRequest = new();

                    hierarchyTreeSelectFilterRequest.NodeFilter = new()
                    {
                        HierarchyLevel = hierarchyTreeSelectFilter.HierarchyLevel switch
                        {
                            HierarchyLevel.Tenant => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant,
                            HierarchyLevel.Company => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Company,
                            HierarchyLevel.Country => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Country,
                            HierarchyLevel.Region => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Region,
                            HierarchyLevel.Area => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Area,
                            HierarchyLevel.SubArea => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.SubArea,
                            HierarchyLevel.Device => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Device,
                            _ => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant
                        },
                        Id = hierarchyTreeSelectFilter.Id.ToString()
                    };

                    grpcRequest.HierarchyTreeSelectFilters.Add(hierarchyTreeSelectFilterRequest);
                }
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_trafficClient.GetCountOfVisitorsInsideAsync, grpcRequest, hasClientSideLog: false);

            return new()
            {
                CountOfVisitorsInside = grpcResponse.CountOfVisitorsInside
            };
        }

        public async Task<GetVisitorDurationApiResponse> GetVisitorDurationAsync(GetVisitorDurationApiRequest request)
        {
            GetVisitorDurationRequest grpcRequest = new()
            {
                CurrentStartDate = Timestamp.FromDateTimeOffset(request.CurrentStartDate),
            };


            if (request.HierarchyTreeSelectFilters != null && request.HierarchyTreeSelectFilters.Count > 0)
            {
                foreach (var hierarchyTreeSelectFilter in request.HierarchyTreeSelectFilters)
                {
                    global::CountwareTraffic.Services.Events.Grpc.HierarchyTreeSelectFilterRequest hierarchyTreeSelectFilterRequest = new();

                    hierarchyTreeSelectFilterRequest.NodeFilter = new()
                    {
                        HierarchyLevel = hierarchyTreeSelectFilter.HierarchyLevel switch
                        {
                            HierarchyLevel.Tenant => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant,
                            HierarchyLevel.Company => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Company,
                            HierarchyLevel.Country => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Country,
                            HierarchyLevel.Region => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Region,
                            HierarchyLevel.Area => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Area,
                            HierarchyLevel.SubArea => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.SubArea,
                            HierarchyLevel.Device => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Device,
                            _ => global::CountwareTraffic.Services.Events.Grpc.HierarchyLevel.Tenant
                        },
                        Id = hierarchyTreeSelectFilter.Id.ToString()
                    };

                    grpcRequest.HierarchyTreeSelectFilters.Add(hierarchyTreeSelectFilterRequest);
                }
            }

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_trafficClient.GetVisitorDurationAsync, grpcRequest, hasClientSideLog: false);

            var lastOutwardsDate = grpcResponse.LastOutwardsDate.ToDateTimeOffset().LocalDateTime;
            var lastInwardsDate = grpcResponse.LastInwardsDate.ToDateTimeOffset().LocalDateTime;

            return new()
            {
                Duration = lastOutwardsDate > lastInwardsDate ? lastOutwardsDate - lastInwardsDate : lastInwardsDate - lastOutwardsDate
            };
        }

        public async Task<List<TimePeriodApiResponse>> GetEventsByTimePeriodAsync(TimePeriodApiRequest request)
        {
            TimePeriodRequest grpcRequest = new()
            {
                StartDate = Timestamp.FromDateTimeOffset(request.StartDate),
                EndDate = Timestamp.FromDateTimeOffset(request.EndDate)
            };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_trafficClient.GetEventsByTimePeriodAsync, grpcRequest, hasClientSideLog: false);


            List<TimePeriodApiResponse> response = new List<TimePeriodApiResponse>();
            return grpcResponse.Data.Select(v => new TimePeriodApiResponse
            {
                AreaId = Guid.Parse(v.AreaId),
                AreaName = v.AreaName,
                SubAreaId = Guid.Parse(v.SubAreaId),
                SubAreaName = v.SubAreaName,
                Date = v.Date.ToDateTime(),
                EnterCount = v.EnterCount,
                ExitCount = v.ExitCount
            }).ToList();
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
