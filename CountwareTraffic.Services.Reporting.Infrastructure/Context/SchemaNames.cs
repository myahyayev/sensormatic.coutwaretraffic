﻿namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    internal sealed class SchemaNames
    {
        internal const string Application = "report.app";
        internal const string Reports = "reports";
    }
}
