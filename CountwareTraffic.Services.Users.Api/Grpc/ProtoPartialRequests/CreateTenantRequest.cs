﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace CountwareTraffic.Services.Users.Grpc
{
    [ServiceLog]
    public sealed partial class CreateTenantRequest : SensormaticRequestValidate
    {
        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1471 Tenant Name cannot be null", nameof(Name)));
            if (!Name.IsValidSubdomain())
                ValidateResults.Add(new ErrorResult($"#E1472 Tenant Name is invalid", nameof(Name)));
            if (Name.Length < 2 || Name.Length > 50)
                ValidateResults.Add(new ErrorResult($"#E1473 Tenant Name length must be 2-50 character", nameof(Name)));
        }
    }
}
