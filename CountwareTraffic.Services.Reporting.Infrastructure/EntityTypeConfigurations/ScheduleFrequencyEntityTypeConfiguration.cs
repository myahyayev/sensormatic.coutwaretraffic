﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ScheduleFrequencyEntityTypeConfiguration : IEntityTypeConfiguration<ScheduleFrequency>
    {
        public void Configure(EntityTypeBuilder<ScheduleFrequency> builder)
        {
            builder.ToTable("ScheduleFrequencies", SchemaNames.Reports);

            builder
               .HasKey(x => x.Id);

            builder
                .Property(x => x.Id)
                .HasDefaultValue(1)
                .ValueGeneratedNever()
                .IsRequired();

            builder
                .Property(x => x.Name)
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}
