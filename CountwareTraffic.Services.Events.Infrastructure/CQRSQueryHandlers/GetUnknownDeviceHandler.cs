﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetUnknownDeviceHandler : IQueryHandler<GetUnknownDevice, UnknownDeviceDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<GetUnknownDeviceHandler> _logger;
        public GetUnknownDeviceHandler(IUnitOfWork unitOfWork, ILogger<GetUnknownDeviceHandler> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<UnknownDeviceDetailsDto> HandleAsync(GetUnknownDevice query)
        {
            var unknownDevice = await _unitOfWork
                                        .GetRepository<IUnknownDeviceRepository>()
                                        .GetByIdAsync(query.Id);

            if (unknownDevice == null)
                throw new UnknownDeviceNotFoundException(query.Id);

            return new UnknownDeviceDetailsDto
            {
                Id = unknownDevice.Id,
                Name = unknownDevice.Name,
                HttpPort = unknownDevice.HttpPort,
                HttpsPort = unknownDevice.HttpsPort,
                IpAddress = unknownDevice.IpAddress,
                MacAddress = unknownDevice.MacAddress,
                OccurredOn = unknownDevice.OccurredOn,
                SerialNumber = unknownDevice.SerialNumber
            };
        }
    }
}
