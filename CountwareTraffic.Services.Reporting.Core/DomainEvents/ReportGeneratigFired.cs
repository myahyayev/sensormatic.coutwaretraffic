﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportGeneratigFired : IDomainEvent
    {
        public Guid ReportId { get; init; }
        public Guid RecordId { get; init; }
        public int ReportStatus { get; init; }
        public Guid TenantId { get; init; }

        public ReportGeneratigFired(Guid reportId, int reportStatus, Guid tenantId)
        {
            ReportId = reportId;
            ReportStatus = reportStatus;
            RecordId = Guid.NewGuid();
            TenantId = tenantId;
        }
    }
}
