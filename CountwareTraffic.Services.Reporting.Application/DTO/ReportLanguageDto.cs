﻿using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportLanguageDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
