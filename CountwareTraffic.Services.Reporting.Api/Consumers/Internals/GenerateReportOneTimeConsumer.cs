﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Reporting.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Api
{
    public class GenerateReportOneTimeConsumer : IConsumer<Sensormatic.Tool.QueueModel.OneTimeReportGenerate>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public GenerateReportOneTimeConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.OneTimeReportGenerate queuEvent)
        {
            var oneTimeReportGenerate = _eventMapper.Map(queuEvent) as ReportGenerate;
            await _eventDispatcher.PublishAsync(oneTimeReportGenerate);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
