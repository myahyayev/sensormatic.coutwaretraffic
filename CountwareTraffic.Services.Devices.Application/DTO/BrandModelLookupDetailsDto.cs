﻿using System;

namespace CountwareTraffic.Services.Devices.Application
{
    public class BrandModelLookupDetailsDto : BrandModelLookupDto
    {
        public string Name { get; set; }
        public Guid BrandId { get; set; }
    }
}
