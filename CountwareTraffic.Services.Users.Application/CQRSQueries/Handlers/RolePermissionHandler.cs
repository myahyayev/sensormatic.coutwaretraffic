﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class RolePermissionHandler : IQueryHandler<RolePermission, IdentityCreateRolePermissionResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public RolePermissionHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityCreateRolePermissionResponse> HandleAsync(RolePermission query)
        {
            return await _identityServerService.RolePermissionAsync(new IdentityCreateRolePermissionRequest
            {
                PermissionId = query.PermissionId,
                RoleId = query.RoleId,
                StartDate = query.StartDate,
                EndDate = query.EndDate
            });
        }
    }
}
