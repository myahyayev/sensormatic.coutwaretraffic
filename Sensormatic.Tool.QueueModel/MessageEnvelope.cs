﻿using System;

namespace Sensormatic.Tool.QueueModel
{
    public abstract record MessageEnvelope
    {
        public MessageEnvelope(Guid userId, string correlationId, Guid tenantId)
        {
            UserId = userId;
            CorrelationId = correlationId;
            TenantId = tenantId;
        }

        public Guid UserId { get; }
        public string CorrelationId { get; }
        public Guid TenantId { get; }
    }
}
