﻿using CountwareTraffic.Services.Events.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.Text.Json;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class EventEntityTypeConfiguration : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder.ToTable("Events", SchemaNames.Events);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.Description)
               .HasField("_description")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .HasMaxLength(2500);

            builder
              .Property(x => x.EventDate)
              .HasField("_eventDate")
              .UsePropertyAccessMode(PropertyAccessMode.Field);


            builder
              .Property(x => x.EventStartDate)
              .HasField("_eventStartDate")
              .IsRequired()
              .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
             .Property(x => x.EventEndDate)
             .HasField("_eventEndDate")
             .IsRequired()
             .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
            .Property(x => x.OnTimeIOCount)
            .HasField("_onTimeIOCount")
            .IsRequired()
            .UsePropertyAccessMode(PropertyAccessMode.Field);


            builder.HasOne<Device>()
                .WithMany()
                .HasForeignKey(x => x.DeviceId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder
                .Property<int>("_directionTypeId")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("DirectionTypeId")
                .IsRequired();

            builder.HasOne(o => o.DirectionType)
                   .WithMany()
                   .HasForeignKey("_directionTypeId");
        }
    }
}
