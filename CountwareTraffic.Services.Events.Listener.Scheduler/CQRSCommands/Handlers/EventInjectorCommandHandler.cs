﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using CountwareTraffic.Services.Events.Listener.Scheduler.CQRSCommands;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class EventInjectorCommandHandler : EventLogXmlFormatHandlerBase<EventInjectorCommandHandler>, ICommandHandler<EventInjectorCommand>
    {

        private DeviceMigrationEventsListener _deviceMigrationEventsListener;

        public EventInjectorCommandHandler(ILogger<EventInjectorCommandHandler> logger, IOptions<TimeZoneSettings> timeZoneSettings, IIdentityService identityService, IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher, ICache redisCacheService, IQueueService queueService) : base(logger, timeZoneSettings, identityService, queryDispatcher, commandDispatcher, redisCacheService, queueService, null)
        {
            _deviceMigrationEventsListener = new DeviceMigrationEventsListener();

        }

        public async Task HandleAsync(EventInjectorCommand injectionData)
        {


            if (injectionData is not null)
            {
                var currentDevice =
                    await _queryDispatcher.QueryAsync(new GetDeviceByMac() { MacAddress = injectionData.MacAddress });
                
                await IsDeviceValid(currentDevice,new DeviceDto() {MacAddress = injectionData.MacAddress }, false);

                DateTime expiryTime = DateTime.Today.AddDays(1);
                TimeSpan timeRemaining = expiryTime - DateTime.Now;

                if (injectionData.Events is not null && injectionData.Events.Count > 0)
                {
                    _deviceMigrationEventsListener.DeviceId = currentDevice.Id;
                    _deviceMigrationEventsListener.TenantId = currentDevice.TenantId;
                    _deviceMigrationEventsListener.RecordId = Guid.NewGuid();

                    foreach (var deviceEvent in injectionData.Events)
                    {

                        if (deviceEvent.ExitCount == 0 && deviceEvent.EnterCount == 0)
                            continue;

                        if (!IsEventAreaWorkingTimeZoneValid(currentDevice, deviceEvent.StartDate, deviceEvent.EndDate))
                            continue;

                        //skip duplicate device events 
                        var redisKey = string.Format(CacheKeys.DeviceMigrationEventSet, currentDevice.Id);
                        var redisValue = string.Format("{0}", deviceEvent.Id);

                        if (!await _redisCacheService.AddSetAsync(redisKey, redisValue, null, timeRemaining))
                        {
                            continue;
                        }
                       
                        var splittedEventsList = new List<DeviceEventsRangeElement>();

                        //initial default values for event
                        DeviceEventsRangeElement tmp = new()
                        {
                            DeviceId = currentDevice.Id,
                            UserId = _identityService.UserId,
                            EventDate = deviceEvent.EventDate,
                            EventStartDate = deviceEvent.StartDate,
                            EventEndDate = deviceEvent.EndDate,
                            DeviceName = currentDevice.Name,
                            TenantId = currentDevice.TenantId,
                        };

                        //make Outward event
                        if (deviceEvent.ExitCount > 0)
                        {
                            splittedEventsList
                                .Add(tmp with
                                {
                                    OnTimeIOCount = deviceEvent.ExitCount,
                                    RecordId = Guid.NewGuid(),
                                    DirectionTypeId = DirectionType.Outward.Id,
                                    Description = $"Injection Event = DeviceName: {currentDevice.Name}",
                                });
                        }

                        //make Inward event
                        if (deviceEvent.EnterCount > 0)
                        {
                            splittedEventsList
                                .Add(tmp with
                                {
                                    OnTimeIOCount = deviceEvent.EnterCount,
                                    RecordId = Guid.NewGuid(),
                                    DirectionTypeId = DirectionType.Inwards.Id,
                                    Description = $"Injection Event = DeviceName: {currentDevice.Name}",
                                });
                        }

                        _deviceMigrationEventsListener.Items.AddRange(splittedEventsList);

                    }
                    if (_deviceMigrationEventsListener.Items.Count > 0)
                        //bulk data send
                        await _commandDispatcher.SendAsync(_deviceMigrationEventsListener);


                }
            }
        }

        public override DeviceDto ParseDeviceEventXml(string xml)
        {
            throw new NotImplementedException();
        }
    }
}

