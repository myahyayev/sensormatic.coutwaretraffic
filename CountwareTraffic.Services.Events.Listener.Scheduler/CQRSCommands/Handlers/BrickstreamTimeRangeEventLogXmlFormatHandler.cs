﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class BrickstreamTimeRangeEventLogXmlFormatHandler : EventLogXmlFormatHandlerBase<BrickstreamTimeRangeEventLogXmlFormatHandler>, ICommandHandler<BrickstreamTimeRangeEventLogXmlFormat>
    {


        public BrickstreamTimeRangeEventLogXmlFormatHandler(ILogger<BrickstreamTimeRangeEventLogXmlFormatHandler> logger, IOptions<TimeZoneSettings> timeZoneSettings, IIdentityService identityService, IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher, ICache redisCacheService, IQueueService queueService, IDeviceConnectionStatusProcessorService deviceConnectionStatusProcessorService) : base(logger, timeZoneSettings, identityService, queryDispatcher, commandDispatcher, redisCacheService, queueService,deviceConnectionStatusProcessorService)
        {

        }

        public async Task HandleAsync(BrickstreamTimeRangeEventLogXmlFormat eventLog)
        {
            var xml = HttpUtility.UrlDecode(eventLog.Xml);

            if (xml.IsNullOrWhiteSpace())
            {
                _logger.LogError("xml file is empty!");
                return;
            }


            var objectFromXml = ParseDeviceEventXml(xml);

            if (objectFromXml != null)
            {
                var currentDevice =
                    await _queryDispatcher.QueryAsync(new GetDeviceByMac() { MacAddress = objectFromXml.MacAddress });

                await IsDeviceValid(currentDevice,objectFromXml);

                await ProcessDeviceConnectionStatus(currentDevice);

                DateTime expiryTime = DateTime.Today.AddHours(23).AddMinutes(59);
                TimeSpan timeRemaining = expiryTime - DateTime.Now;
                //Note:Brickstream wont send duplicate events automatically,just added if user triggers event push manually multiple times

                //skip duplicate device events 
                var redisKey = string.Format(CacheKeys.DeviceTimeRangeEventSet, currentDevice.Id);

                if (objectFromXml.Events != null && objectFromXml.Events.Count > 0)
                {
                    foreach (var deviceEvent in objectFromXml.Events)
                    {
                        if (deviceEvent.ExitCount == 0 && deviceEvent.EnterCount == 0)
                            continue;

                        if (!IsEventAreaWorkingTimeZoneValid(currentDevice, deviceEvent.StartDate, deviceEvent.EndDate))
                            continue;


                        var redisValue = string.Format("{0}_{1}", deviceEvent.StartDate.ToString("MM_dd_yyyy_HH_mm_ss"), deviceEvent.EndDate.ToString("MM_dd_yyyy_HH_mm_ss"));

                        if (!await _redisCacheService.AddSetAsync(redisKey, redisValue, null, timeRemaining))
                        {
                            continue;
                        }

                        var splittedEventsList = new List<DeviceEventsListener>();

                        //initial default values for event
                        DeviceEventsListener tmp = new DeviceEventsListener
                        {
                            DeviceId = currentDevice.Id,
                            TotalEnterCount = deviceEvent.EnterCount,
                            TotalExitCount = deviceEvent.ExitCount,
                            UserId = _identityService.UserId,
                            EventDate = deviceEvent.EventDate,
                            EventStartDate = deviceEvent.StartDate,
                            EventEndDate = deviceEvent.EndDate,
                            DeviceName = currentDevice.Name,
                            DirectionTypeId = DirectionType.Unknown.Id,
                            TenantId = currentDevice.TenantId,
                        };

                        //make Outward event
                        if (deviceEvent.ExitCount > 0)
                        {

                            splittedEventsList
                                .Add(tmp with
                                {
                                    OnTimeIOCount = deviceEvent.ExitCount,
                                    RecordId = Guid.NewGuid(),
                                    DirectionTypeId = DirectionType.Outward.Id,
                                    Description = $"TimeRange Event = DeviceName: {currentDevice.Name}",
                                }
                                    );

                        }

                        //make Inward event
                        if (deviceEvent.EnterCount > 0)
                        {

                            splittedEventsList
                                .Add(tmp with
                                {
                                    OnTimeIOCount = deviceEvent.EnterCount,
                                    RecordId = Guid.NewGuid(),
                                    DirectionTypeId = DirectionType.Inwards.Id,
                                    Description = $"TimeRange Event = DeviceName: {currentDevice.Name}",
                                }
                                    );
                        }




                        if (splittedEventsList.Any())
                        {
                            foreach (var currentSplittedEvent in splittedEventsList)
                            {
                                await _commandDispatcher.SendAsync(currentSplittedEvent);
                            }
                        }


                    }
                }


            }
            else
            {
                _logger.LogError("xml format is not valid!");
                throw new Exception("xml format is not valid!");
            }

        }
        public override DeviceDto ParseDeviceEventXml(string xml)
        {
            DeviceDto deviceEventInfo = null;

            var xmlDocuemnt = XDocument.Parse(xml.Trim());
            var metrics = xmlDocuemnt.Descendants("Metrics");

            if (metrics != null && metrics.Count() > 0)
            {
                foreach (var metric in metrics)
                {
                    var properties = metric.Descendants("Properties").FirstOrDefault();

                    if (properties != null)
                    {

                        deviceEventInfo = new DeviceDto()
                        {
                            MacAddress = properties.Descendants("MacAddress").FirstOrDefault()?.Value,
                            IpAddress = properties.Descendants("IpAddress").FirstOrDefault()?.Value,
                            SerialNumber = properties.Descendants("SerialNumber").FirstOrDefault()?.Value,
                            HttpPort = properties.Descendants("HttpPort").FirstOrDefault()?.Value,
                            HttpsPort = properties.Descendants("HttpsPort").FirstOrDefault()?.Value,
                            ModelName = properties.Descendants("HwPlatform").FirstOrDefault()?.Value,
                            FirmWare = properties.Descendants("SwRelease").FirstOrDefault()?.Value,
                            DeviceId = metric.Attribute("DeviceId").Value ?? "",
                            DeviceName = metric.Attribute("Devicename").Value ?? "",
                        BrandName = "Brickstream"
                        };
                        byte timeZoneOffset = Convert.ToByte(properties.Descendants("Timezone").FirstOrDefault()?.Value ?? "0");
                        TimeSpan timeZoneOffsetTimeSpan = new TimeSpan(timeZoneOffset, 0, 0);
                        //string timeZoneName = properties.Descendants("TimezoneName").FirstOrDefault()?.Value;

                        foreach (XElement reportData in metric.Descendants("ReportData"))
                        {
                            foreach (XElement report in reportData.Descendants("Report"))
                            {
                                DateTime eventDate = DateTime.Parse(report.Attribute("Date").Value);
                                //DateTime dt = eventDate.ToUniversalTime();


                                foreach (XElement objectItem in report.Descendants("Object"))
                                {
                        

                                    foreach (XElement reportItem in objectItem.Descendants("Count"))
                                    {
                                        TimeSpan tmStart = TimeSpan.Parse(reportItem.Attribute("StartTime").Value)
                                                                   .Subtract(timeZoneOffsetTimeSpan);

                                        TimeSpan tmEnd = TimeSpan.Parse(reportItem.Attribute("EndTime").Value)
                                                                 .Subtract(timeZoneOffsetTimeSpan);

                                        DateTime UTCTime = eventDate.Date;


                                        int enter = Int32.Parse(reportItem.Attribute("Enters").Value);
                                        int exit = Int32.Parse(reportItem.Attribute("Exits").Value);
                                        //int status = Int32.Parse(reportItem.Attribute("Status").Value);

                                        deviceEventInfo.Events.Add(new EventDto
                                        {
                                            EventDate = UTCTime.Add(tmStart),//event date has no time part
                                            StartDate = UTCTime.Add(tmStart),
                                            EndDate = UTCTime.Add(tmEnd),
                                            EnterCount = enter,
                                            ExitCount = exit,
                                        });

                                    }
                                }
                            }
                        }
                    }
                }
            }

            return deviceEventInfo;
        }


    }
}

