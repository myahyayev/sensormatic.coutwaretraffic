﻿using CountwareTraffic.Services.Companies.Grpc;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AllHierarchyService : IScopedSelfDependency
    {
        private readonly AllHierarchy.AllHierarchyClient _allHierarchyClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;

        public AllHierarchyService(AllHierarchy.AllHierarchyClient allHierarchyClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _allHierarchyClient = allHierarchyClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<Parent> GetAllHierarchyAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_allHierarchyClient.GetAllHierarchyAsync, grpcRequest, hasClientSideLog: false);

            HierarchyTree hierarchyTree = Newtonsoft.Json.JsonConvert.DeserializeObject<HierarchyTree>(grpcResponse.JsonValue);

            return hierarchyTree != null ? hierarchyTree.Parent : null;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
