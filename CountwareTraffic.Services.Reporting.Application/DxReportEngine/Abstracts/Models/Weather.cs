﻿using System;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public class Weather
    {
        public Weather()
        {
            //todo:this is temprory just for showing report with dummy weather data
            Description = "-";
           // Temperature = new ValueObject<float> { Unit = "C", Value = new Random().Next(0,50) };
            //Icon = new WebClient().DownloadData("https://ssl.gstatic.com/onebox/weather/48/rain_light.png");
            //File.ReadAllBytes("https://ssl.gstatic.com/onebox/weather/48/partly_cloudy.png");
        }
        public string Description { get; set; }
        public ValueObject<float> Humidity { get; set; }
        public ValueObject<float> Wind { get; set; }
        public ValueObject<float> Temperature { get; set; }
        public byte[] Icon { get; set; }
    }
}
