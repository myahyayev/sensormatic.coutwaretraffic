﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Devices.Application
{
    [Contract]
    public class DeleteBrandModelLookup : ICommand
    {
        public Guid BrandModelId { get; set; }
    }
}

