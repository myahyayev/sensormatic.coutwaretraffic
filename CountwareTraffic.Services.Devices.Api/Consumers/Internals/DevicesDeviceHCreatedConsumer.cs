﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Devices.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Api
{
    public class DevicesDeviceHCreatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.DeviceHCreated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public DevicesDeviceHCreatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.DeviceHCreated queuEvent)
        {
            var deviceHCreated = _eventMapper.Map(queuEvent) as DeviceHCreated;
            await _eventDispatcher.PublishAsync(deviceHCreated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
