﻿namespace CountwareTraffic.Services.Reporting.Engine
{
    partial class WeeklyTrafficReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView1 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.XYDiagram xyDiagram2 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SeriesKeyColorColorizer seriesKeyColorColorizer1 = new DevExpress.XtraCharts.SeriesKeyColorColorizer();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeeklyTrafficReport));
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            this.objectDataSource2 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.MainDetail = new DevExpress.XtraReports.UI.DetailBand();
            this.SummaryByStore = new DevExpress.XtraReports.UI.DetailReportBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.SummaryByStoreHeaderTable = new DevExpress.XtraReports.UI.XRTable();
            this.SummaryTableHeaderRow = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.table3 = new DevExpress.XtraReports.UI.XRTable();
            this.tableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.SummaryByStoreFooterTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.SummaryByStoreFooterTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.SummaryByStoreSubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.SummaryByStoreTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupCaption2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupData2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailCaption2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailData2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupFooterBackground3 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailData3_Odd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ComparedStoresSummary = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.ComparedStoresSummarySubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.ComparedStoresSummaryTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.ComparedStoresSummaryHeaderTable = new DevExpress.XtraReports.UI.XRTable();
            this.ComparedStoresSummaryTableHeaderRow = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter1 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.ComparedStoresSummaryFooterTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByDayCharts = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TrafficByDayChartsTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByDayChartsSubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByDayChart = new DevExpress.XtraReports.UI.XRChart();
            this.ReportFooter2 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.TrafficByDayChartsFooterText = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByStoreCharts = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.TrafficByStoreChartsTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficByStoreChartsSubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.TrafficbyStoreChart = new DevExpress.XtraReports.UI.XRChart();
            this.ReportFooter3 = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.TrafficByStoreChartsFooter = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.CreatedDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.pageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.CorporationLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.tip = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.SubTitleLbl = new DevExpress.XtraReports.UI.XRLabel();
            this.TitleLbl = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.ApplicationLogo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.SummaryByDay = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.reportHeaderBand1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.ComparedStoresSummaryByDayTableHeaderRow = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ComparedStoresSummaryByDaySubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.ComparedStoresSummaryByDayTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.ComparedStoresSummaryByDayFooterTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.SummaryByStoreByDay = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.weatherIcon = new DevExpress.XtraReports.UI.XRPictureBox();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.SummaryByStoreByDayTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.SummaryByStoreByDaySubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.SummaryByStoreByDayHeaderTable = new DevExpress.XtraReports.UI.XRTable();
            this.SummaryByStoreByDayHeaderRow = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.SummaryByStoreByDayFooterTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.SummaryByStoreByDayFooter = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryByStoreHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComparedStoresSummaryHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficByDayChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficbyStoreChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryByStoreByDayHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // objectDataSource2
            // 
            this.objectDataSource2.DataSource = typeof(global::CountwareTraffic.Services.Reporting.Engine.TrafficReport.WeeklyTrafficReportData);
            this.objectDataSource2.Name = "objectDataSource2";
            // 
            // TopMargin
            // 
            this.TopMargin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.StylePriority.UseBackColor = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.BackColor = System.Drawing.Color.Empty;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.StylePriority.UseBackColor = false;
            // 
            // xrLabel111
            // 
            this.xrLabel111.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CreatedBy]")});
            this.xrLabel111.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel111.ForeColor = System.Drawing.Color.Gray;
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(0F, 69.16666F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.SizeF = new System.Drawing.SizeF(300.9584F, 24.19433F);
            this.xrLabel111.StylePriority.UseFont = false;
            this.xrLabel111.StylePriority.UseForeColor = false;
            this.xrLabel111.Text = "Note:this is footer text";
            // 
            // MainDetail
            // 
            this.MainDetail.HeightF = 12.70832F;
            this.MainDetail.KeepTogether = true;
            this.MainDetail.Name = "MainDetail";
            this.MainDetail.Visible = false;
            // 
            // SummaryByStore
            // 
            this.SummaryByStore.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.GroupHeader1,
            this.Detail1,
            this.GroupFooter1,
            this.GroupHeader3});
            this.SummaryByStore.DataMember = "SummaryByStore.Content";
            this.SummaryByStore.DataSource = this.objectDataSource2;
            this.SummaryByStore.Expanded = false;
            this.SummaryByStore.Level = 4;
            this.SummaryByStore.Name = "SummaryByStore";
            this.SummaryByStore.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.SummaryByStoreHeaderTable});
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("StoreId", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 40F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // SummaryByStoreHeaderTable
            // 
            this.SummaryByStoreHeaderTable.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.SummaryByStoreHeaderTable.Name = "SummaryByStoreHeaderTable";
            this.SummaryByStoreHeaderTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.SummaryTableHeaderRow});
            this.SummaryByStoreHeaderTable.SizeF = new System.Drawing.SizeF(1130.128F, 40F);
            // 
            // SummaryTableHeaderRow
            // 
            this.SummaryTableHeaderRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell4,
            this.tableCell5,
            this.tableCell8,
            this.xrTableCell5});
            this.SummaryTableHeaderRow.Name = "SummaryTableHeaderRow";
            this.SummaryTableHeaderRow.Weight = 1D;
            // 
            // tableCell4
            // 
            this.tableCell4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell4.Name = "tableCell4";
            this.tableCell4.StyleName = "DetailCaption2";
            this.tableCell4.StylePriority.UseBorderColor = false;
            this.tableCell4.StylePriority.UseTextAlignment = false;
            this.tableCell4.Tag = "StoreName";
            this.tableCell4.Text = "Store Name";
            this.tableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell4.Weight = 0.05193371125697218D;
            // 
            // tableCell5
            // 
            this.tableCell5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell5.Name = "tableCell5";
            this.tableCell5.StyleName = "DetailCaption2";
            this.tableCell5.StylePriority.UseBorderColor = false;
            this.tableCell5.StylePriority.UseTextAlignment = false;
            this.tableCell5.Tag = "Date";
            this.tableCell5.Text = "Date";
            this.tableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell5.Weight = 0.051990428491887591D;
            // 
            // tableCell8
            // 
            this.tableCell8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell8.Name = "tableCell8";
            this.tableCell8.StyleName = "DetailCaption2";
            this.tableCell8.StylePriority.UseBorderColor = false;
            this.tableCell8.Tag = "TrafficInBound";
            this.tableCell8.Text = "Traffic InBound";
            this.tableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell8.Weight = 0.040893402970253012D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StyleName = "DetailCaption2";
            this.xrTableCell5.StylePriority.UseBorderColor = false;
            this.xrTableCell5.Tag = "TrafficOutBound";
            this.xrTableCell5.Text = "Traffic OutBound";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.0408934006954306D;
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table3});
            this.Detail1.HeightF = 40F;
            this.Detail1.KeepTogether = true;
            this.Detail1.Name = "Detail1";
            this.Detail1.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("order", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            // 
            // table3
            // 
            this.table3.BorderColor = System.Drawing.Color.Black;
            this.table3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.table3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.table3.BorderWidth = 1F;
            this.table3.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.table3.Name = "table3";
            this.table3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow3});
            this.table3.SizeF = new System.Drawing.SizeF(1130.109F, 40F);
            this.table3.StylePriority.UseBorderColor = false;
            this.table3.StylePriority.UseBorderWidth = false;
            // 
            // tableRow3
            // 
            this.tableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell29,
            this.tableCell30,
            this.tableCell33,
            this.xrTableCell6});
            this.tableRow3.Name = "tableRow3";
            this.tableRow3.Weight = 11.5D;
            // 
            // tableCell29
            // 
            this.tableCell29.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell29.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.tableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.tableCell29.BorderWidth = 1F;
            this.tableCell29.EvenStyleName = "DetailData2";
            this.tableCell29.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StoreName]")});
            this.tableCell29.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableCell29.KeepTogether = true;
            this.tableCell29.Name = "tableCell29";
            this.tableCell29.OddStyleName = "DetailData3_Odd";
            this.tableCell29.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Merge;
            this.tableCell29.StylePriority.UseBackColor = false;
            this.tableCell29.StylePriority.UseBorderDashStyle = false;
            this.tableCell29.StylePriority.UseBorders = false;
            this.tableCell29.StylePriority.UseBorderWidth = false;
            this.tableCell29.StylePriority.UseFont = false;
            this.tableCell29.StylePriority.UseTextAlignment = false;
            this.tableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell29.Weight = 0.047723838198863354D;
            // 
            // tableCell30
            // 
            this.tableCell30.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell30.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.tableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableCell30.BorderWidth = 1F;
            this.tableCell30.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CurrentPeriod].[Caption]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]")});
            this.tableCell30.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableCell30.Name = "tableCell30";
            this.tableCell30.OddStyleName = "DetailData3_Odd";
            this.tableCell30.StyleName = "DetailData2";
            this.tableCell30.StylePriority.UseFont = false;
            this.tableCell30.StylePriority.UseTextAlignment = false;
            this.tableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tableCell30.Weight = 0.047775956807239714D;
            // 
            // tableCell33
            // 
            this.tableCell33.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tableCell33.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.tableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.tableCell33.BorderWidth = 1F;
            this.tableCell33.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[InBound].[Value],[Traffic].[InBound].[Unit],[Traffic].[InBound]" +
                    ".[Indicator])\n"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[InBound].[ForeColor].[IsEmpty], false,[Traffic].[InBound].[ForeCol" +
                    "or])")});
            this.tableCell33.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableCell33.Name = "tableCell33";
            this.tableCell33.OddStyleName = "DetailData3_Odd";
            this.tableCell33.StyleName = "DetailData2";
            this.tableCell33.StylePriority.UseBackColor = false;
            this.tableCell33.StylePriority.UseFont = false;
            this.tableCell33.StylePriority.UseForeColor = false;
            this.tableCell33.StylePriority.UseTextAlignment = false;
            this.tableCell33.Weight = 0.037578485553104779D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell6.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell6.BorderWidth = 1F;
            this.xrTableCell6.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[OutBound].[ForeColor].[IsEmpty], false,[Traffic].[OutBound].[ForeC" +
                    "olor] )\n"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[OutBound].[Value],[Traffic].[OutBound].[Unit],[Traffic].[OutBou" +
                    "nd].[Indicator])\n")});
            this.xrTableCell6.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.OddStyleName = "DetailData3_Odd";
            this.xrTableCell6.StyleName = "DetailData2";
            this.xrTableCell6.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseBorderWidth = false;
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UseForeColor = false;
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 0.037575582146058724D;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.SummaryByStoreFooterTitle,
            this.xrLabel3,
            this.xrLabel1,
            this.SummaryByStoreFooterTotal});
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 77.68239F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.GroupFooter1.StylePriority.UseBorders = false;
            // 
            // SummaryByStoreFooterTitle
            // 
            this.SummaryByStoreFooterTitle.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreFooterTitle.ForeColor = System.Drawing.Color.Gray;
            this.SummaryByStoreFooterTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 42.24382F);
            this.SummaryByStoreFooterTitle.Name = "SummaryByStoreFooterTitle";
            this.SummaryByStoreFooterTitle.SizeF = new System.Drawing.SizeF(1130.109F, 24.19433F);
            this.SummaryByStoreFooterTitle.StyleName = "tip";
            this.SummaryByStoreFooterTitle.Text = "Footertitle";
            this.SummaryByStoreFooterTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum(Iif([Traffic].[InBound].[IsComparedRow],0 ,[Traffic].[InBound].[Value]))")});
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(652.4215F, 2.999924F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(248.8533F, 23F);
            this.xrLabel3.StyleName = "Title";
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseFont = false;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel3.Summary = xrSummary1;
            this.xrLabel3.Text = "xrLabel3";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum(Iif([Traffic].[OutBound].[IsComparedRow],0 ,[Traffic].[OutBound].[Value]))" +
                    "\n")});
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(901.2748F, 2.999924F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(248.8341F, 23F);
            this.xrLabel1.StyleName = "Title";
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel1.Summary = xrSummary2;
            this.xrLabel1.Text = "xrLabel3";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // SummaryByStoreFooterTotal
            // 
            this.SummaryByStoreFooterTotal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.SummaryByStoreFooterTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.SummaryByStoreFooterTotal.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreFooterTotal.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.SummaryByStoreFooterTotal.Name = "SummaryByStoreFooterTotal";
            this.SummaryByStoreFooterTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(455, 0, 0, 0, 100F);
            this.SummaryByStoreFooterTotal.SizeF = new System.Drawing.SizeF(1130.109F, 29.19439F);
            this.SummaryByStoreFooterTotal.StyleName = "Title";
            this.SummaryByStoreFooterTotal.StylePriority.UseBorderColor = false;
            this.SummaryByStoreFooterTotal.StylePriority.UseBorders = false;
            this.SummaryByStoreFooterTotal.StylePriority.UseFont = false;
            this.SummaryByStoreFooterTotal.StylePriority.UsePadding = false;
            this.SummaryByStoreFooterTotal.StylePriority.UseTextAlignment = false;
            this.SummaryByStoreFooterTotal.Tag = "Total";
            this.SummaryByStoreFooterTotal.Text = "Total";
            this.SummaryByStoreFooterTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.SummaryByStoreSubTitle,
            this.SummaryByStoreTitle});
            this.GroupHeader3.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader3.HeightF = 114.5963F;
            this.GroupHeader3.KeepTogether = true;
            this.GroupHeader3.Level = 1;
            this.GroupHeader3.Name = "GroupHeader3";
            this.GroupHeader3.RepeatEveryPage = true;
            // 
            // xrLabel4
            // 
            this.xrLabel4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StoreName]\n")});
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 76.42603F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.SizeF = new System.Drawing.SizeF(1133.78F, 24.19433F);
            this.xrLabel4.StyleName = "Title";
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Summary by Store";
            // 
            // SummaryByStoreSubTitle
            // 
            this.SummaryByStoreSubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreSubTitle.LocationFloat = new DevExpress.Utils.PointFloat(621.0416F, 76.42606F);
            this.SummaryByStoreSubTitle.Name = "SummaryByStoreSubTitle";
            this.SummaryByStoreSubTitle.SizeF = new System.Drawing.SizeF(155.67F, 24.19433F);
            this.SummaryByStoreSubTitle.StyleName = "Title";
            this.SummaryByStoreSubTitle.StylePriority.UseFont = false;
            this.SummaryByStoreSubTitle.Text = "Summary by Store";
            this.SummaryByStoreSubTitle.Visible = false;
            // 
            // SummaryByStoreTitle
            // 
            this.SummaryByStoreTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 40.38433F);
            this.SummaryByStoreTitle.Name = "SummaryByStoreTitle";
            this.SummaryByStoreTitle.SizeF = new System.Drawing.SizeF(1133.78F, 24.19432F);
            this.SummaryByStoreTitle.StyleName = "Title";
            this.SummaryByStoreTitle.StylePriority.UseFont = false;
            this.SummaryByStoreTitle.Text = "Summary by Store";
            // 
            // Title
            // 
            this.Title.BackColor = System.Drawing.Color.Transparent;
            this.Title.BorderColor = System.Drawing.Color.Black;
            this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Title.BorderWidth = 1F;
            this.Title.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.Title.Name = "Title";
            this.Title.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // GroupCaption2
            // 
            this.GroupCaption2.BackColor = System.Drawing.Color.White;
            this.GroupCaption2.BorderColor = System.Drawing.Color.Black;
            this.GroupCaption2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GroupCaption2.BorderWidth = 1F;
            this.GroupCaption2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupCaption2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.GroupCaption2.Name = "GroupCaption2";
            this.GroupCaption2.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GroupCaption2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupData2
            // 
            this.GroupData2.BackColor = System.Drawing.Color.White;
            this.GroupData2.BorderColor = System.Drawing.Color.White;
            this.GroupData2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GroupData2.BorderWidth = 2F;
            this.GroupData2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupData2.ForeColor = System.Drawing.Color.Black;
            this.GroupData2.Name = "GroupData2";
            this.GroupData2.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GroupData2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailCaption2
            // 
            this.DetailCaption2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.DetailCaption2.BorderColor = System.Drawing.Color.Transparent;
            this.DetailCaption2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DetailCaption2.BorderWidth = 0F;
            this.DetailCaption2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailCaption2.ForeColor = System.Drawing.Color.White;
            this.DetailCaption2.Name = "DetailCaption2";
            this.DetailCaption2.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
            this.DetailCaption2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailData2
            // 
            this.DetailData2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.DetailData2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.DetailData2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DetailData2.BorderWidth = 0F;
            this.DetailData2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailData2.ForeColor = System.Drawing.Color.Black;
            this.DetailData2.Name = "DetailData2";
            this.DetailData2.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 6, 6, 100F);
            this.DetailData2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupFooterBackground3
            // 
            this.GroupFooterBackground3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(117)))), ((int)(((byte)(129)))));
            this.GroupFooterBackground3.BorderColor = System.Drawing.Color.White;
            this.GroupFooterBackground3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GroupFooterBackground3.BorderWidth = 2F;
            this.GroupFooterBackground3.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupFooterBackground3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(228)))), ((int)(((byte)(228)))));
            this.GroupFooterBackground3.Name = "GroupFooterBackground3";
            this.GroupFooterBackground3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GroupFooterBackground3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailData3_Odd
            // 
            this.DetailData3_Odd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.DetailData3_Odd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.DetailData3_Odd.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.DetailData3_Odd.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DetailData3_Odd.BorderWidth = 0F;
            this.DetailData3_Odd.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailData3_Odd.ForeColor = System.Drawing.Color.Black;
            this.DetailData3_Odd.Name = "DetailData3_Odd";
            this.DetailData3_Odd.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 6, 6, 100F);
            this.DetailData3_Odd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageInfo
            // 
            this.PageInfo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PageInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(70)))), ((int)(((byte)(80)))));
            this.PageInfo.Name = "PageInfo";
            this.PageInfo.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
            // 
            // ComparedStoresSummary
            // 
            this.ComparedStoresSummary.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.ReportHeader1,
            this.ReportFooter1});
            this.ComparedStoresSummary.DataMember = "ComparedStoresSummary.Content";
            this.ComparedStoresSummary.DataSource = this.objectDataSource2;
            this.ComparedStoresSummary.Expanded = false;
            this.ComparedStoresSummary.Level = 0;
            this.ComparedStoresSummary.Name = "ComparedStoresSummary";
            // 
            // Detail4
            // 
            this.Detail4.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.Detail4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail4.HeightF = 40F;
            this.Detail4.Name = "Detail4";
            this.Detail4.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("order", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail4.StylePriority.UseBorderDashStyle = false;
            this.Detail4.StylePriority.UseBorders = false;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.Color.Black;
            this.xrTable1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable1.BorderWidth = 1F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1130.11F, 40F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell11,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow1.Weight = 11.5D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell1.BorderWidth = 1F;
            this.xrTableCell1.EvenStyleName = "DetailData2";
            this.xrTableCell1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CurrentPeriod].[Caption]")});
            this.xrTableCell1.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.OddStyleName = "DetailData3_Odd";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseBorderWidth = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "xrTableCell1";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.072545136183890135D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.BorderWidth = 0F;
            this.xrTableCell11.EvenStyleName = "DetailData2";
            this.xrTableCell11.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[InBound].[Value],[Traffic].[InBound].[Unit],[Traffic].[InBound]" +
                    ".[Indicator])"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[InBound].[ForeColor].[IsEmpty], false,[Traffic].[InBound].[ForeCol" +
                    "or] )\n")});
            this.xrTableCell11.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.OddStyleName = "DetailData3_Odd";
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UseForeColor = false;
            this.xrTableCell11.Text = "xrTableCell11";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.097560702240805139D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell3.BorderWidth = 1F;
            this.xrTableCell3.EvenStyleName = "DetailData2";
            this.xrTableCell3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[OutBound].[Value],[Traffic].[OutBound].[Unit],[Traffic].[OutBou" +
                    "nd].[Indicator])\n"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[OutBound].[ForeColor].[IsEmpty], false,[Traffic].[OutBound].[ForeC" +
                    "olor])\n")});
            this.xrTableCell3.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.OddStyleName = "DetailData3_Odd";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseBorderWidth = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseForeColor = false;
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.097560706144588866D;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ComparedStoresSummarySubTitle,
            this.ComparedStoresSummaryTitle,
            this.ComparedStoresSummaryHeaderTable});
            this.ReportHeader1.HeightF = 153.69F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // ComparedStoresSummarySubTitle
            // 
            this.ComparedStoresSummarySubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComparedStoresSummarySubTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 73.49567F);
            this.ComparedStoresSummarySubTitle.Name = "ComparedStoresSummarySubTitle";
            this.ComparedStoresSummarySubTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.ComparedStoresSummarySubTitle.StyleName = "Title";
            this.ComparedStoresSummarySubTitle.StylePriority.UseFont = false;
            this.ComparedStoresSummarySubTitle.Text = "LLY Comp Stores";
            // 
            // ComparedStoresSummaryTitle
            // 
            this.ComparedStoresSummaryTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComparedStoresSummaryTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 37.45403F);
            this.ComparedStoresSummaryTitle.Name = "ComparedStoresSummaryTitle";
            this.ComparedStoresSummaryTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.ComparedStoresSummaryTitle.StyleName = "Title";
            this.ComparedStoresSummaryTitle.StylePriority.UseFont = false;
            this.ComparedStoresSummaryTitle.Text = "Summary";
            // 
            // ComparedStoresSummaryHeaderTable
            // 
            this.ComparedStoresSummaryHeaderTable.LocationFloat = new DevExpress.Utils.PointFloat(20F, 113.69F);
            this.ComparedStoresSummaryHeaderTable.Name = "ComparedStoresSummaryHeaderTable";
            this.ComparedStoresSummaryHeaderTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.ComparedStoresSummaryTableHeaderRow});
            this.ComparedStoresSummaryHeaderTable.SizeF = new System.Drawing.SizeF(1130.11F, 39.99999F);
            // 
            // ComparedStoresSummaryTableHeaderRow
            // 
            this.ComparedStoresSummaryTableHeaderRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell45,
            this.xrTableCell4});
            this.ComparedStoresSummaryTableHeaderRow.Name = "ComparedStoresSummaryTableHeaderRow";
            this.ComparedStoresSummaryTableHeaderRow.Weight = 1D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell42.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StyleName = "DetailCaption2";
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.Tag = "Date";
            this.xrTableCell42.Text = "Date";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell42.Weight = 0.027895164578078582D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StyleName = "DetailCaption2";
            this.xrTableCell45.StylePriority.UseBorderColor = false;
            this.xrTableCell45.Tag = "TrafficInBound";
            this.xrTableCell45.Text = "Traffic InBound";
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell45.Weight = 0.03751418711465454D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StyleName = "DetailCaption2";
            this.xrTableCell4.StylePriority.UseBorderColor = false;
            this.xrTableCell4.Tag = "TrafficOutBound";
            this.xrTableCell4.Text = "Traffic OutBound";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell4.Weight = 0.037514184862537055D;
            // 
            // ReportFooter1
            // 
            this.ReportFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.ComparedStoresSummaryFooterTitle});
            this.ReportFooter1.HeightF = 34.31646F;
            this.ReportFooter1.KeepTogether = true;
            this.ReportFooter1.Name = "ReportFooter1";
            // 
            // xrLine2
            // 
            this.xrLine2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(1130.11F, 2F);
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // ComparedStoresSummaryFooterTitle
            // 
            this.ComparedStoresSummaryFooterTitle.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComparedStoresSummaryFooterTitle.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComparedStoresSummaryFooterTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 10F);
            this.ComparedStoresSummaryFooterTitle.Name = "ComparedStoresSummaryFooterTitle";
            this.ComparedStoresSummaryFooterTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.ComparedStoresSummaryFooterTitle.StyleName = "tip";
            this.ComparedStoresSummaryFooterTitle.Text = "Note:this is footer text";
            this.ComparedStoresSummaryFooterTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TrafficByDayCharts
            // 
            this.TrafficByDayCharts.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportFooter2});
            this.TrafficByDayCharts.Expanded = false;
            this.TrafficByDayCharts.Level = 2;
            this.TrafficByDayCharts.Name = "TrafficByDayCharts";
            this.TrafficByDayCharts.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TrafficByDayChartsTitle,
            this.TrafficByDayChartsSubTitle,
            this.TrafficByDayChart});
            this.Detail.HeightF = 615.5027F;
            this.Detail.Name = "Detail";
            // 
            // TrafficByDayChartsTitle
            // 
            this.TrafficByDayChartsTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByDayChartsTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 41.00004F);
            this.TrafficByDayChartsTitle.Name = "TrafficByDayChartsTitle";
            this.TrafficByDayChartsTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.TrafficByDayChartsTitle.StyleName = "Title";
            this.TrafficByDayChartsTitle.StylePriority.UseFont = false;
            this.TrafficByDayChartsTitle.Text = "Traffic by Day";
            // 
            // TrafficByDayChartsSubTitle
            // 
            this.TrafficByDayChartsSubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByDayChartsSubTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 77.04175F);
            this.TrafficByDayChartsSubTitle.Name = "TrafficByDayChartsSubTitle";
            this.TrafficByDayChartsSubTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TrafficByDayChartsSubTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.TrafficByDayChartsSubTitle.StyleName = "Title";
            this.TrafficByDayChartsSubTitle.StylePriority.UseFont = false;
            this.TrafficByDayChartsSubTitle.StylePriority.UsePadding = false;
            this.TrafficByDayChartsSubTitle.Text = "Traffic by Day";
            // 
            // TrafficByDayChart
            // 
            this.TrafficByDayChart.BorderColor = System.Drawing.Color.Black;
            this.TrafficByDayChart.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TrafficByDayChart.DataSource = this.objectDataSource2;
            xyDiagram1.AxisX.GridLines.Visible = true;
            xyDiagram1.AxisX.Label.Alignment = DevExpress.XtraCharts.AxisLabelAlignment.Center;
            xyDiagram1.AxisX.Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram1.AxisX.Label.ResolveOverlappingOptions.AllowHide = false;
            xyDiagram1.AxisX.LabelVisibilityMode = DevExpress.XtraCharts.AxisLabelVisibilityMode.AutoGeneratedAndCustom;
            xyDiagram1.AxisX.Title.Font = new System.Drawing.Font("Arial", 12F);
            xyDiagram1.AxisX.Visibility = DevExpress.Utils.DefaultBoolean.True;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram1.AxisY.Title.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisXZooming = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYZooming = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.PaneLayout.Direction = DevExpress.XtraCharts.PaneLayoutDirection.Horizontal;
            xyDiagram1.Rotated = true;
            this.TrafficByDayChart.Diagram = xyDiagram1;
            this.TrafficByDayChart.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Gradient;
            this.TrafficByDayChart.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.TrafficByDayChart.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.TrafficByDayChart.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.TrafficByDayChart.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 112.6666F);
            this.TrafficByDayChart.Name = "TrafficByDayChart";
            this.TrafficByDayChart.PaletteName = "CountwareChartPalleteColor";
            this.TrafficByDayChart.PaletteRepository.Add("CountwareChartPalleteColor", new DevExpress.XtraCharts.Palette("CountwareChartPalleteColor", DevExpress.XtraCharts.PaletteScaleMode.Extrapolate, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(39)))), ((int)(((byte)(63))))), System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(39)))), ((int)(((byte)(63)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(95)))), ((int)(((byte)(255))))), System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(95)))), ((int)(((byte)(255)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(31)))), ((int)(((byte)(48))))), System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(31)))), ((int)(((byte)(48)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(141)))), ((int)(((byte)(163))))), System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(141)))), ((int)(((byte)(163)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(194)))), ((int)(((byte)(221))))), System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(194)))), ((int)(((byte)(221)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(99)))), ((int)(((byte)(140))))), System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(99)))), ((int)(((byte)(140)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(180)))), ((int)(((byte)(252))))), System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(180)))), ((int)(((byte)(252)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158))))), System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))), System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(34)))), ((int)(((byte)(76))))), System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(34)))), ((int)(((byte)(76)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(86)))), ((int)(((byte)(90))))), System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(86)))), ((int)(((byte)(90)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160))))), System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(216)))), ((int)(((byte)(214))))), System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(216)))), ((int)(((byte)(214)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(24)))), ((int)(((byte)(52))))), System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(24)))), ((int)(((byte)(52)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238))))), System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))), System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))))}));
            this.TrafficByDayChart.SeriesDataMember = "TrafficByDayChart.Content.CurrentPeriod.Caption";
            series1.ArgumentDataMember = "TrafficByDayChart.Content.CurrentPeriod.Caption";
            series1.DataSorted = true;
            sideBySideBarSeriesLabel1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sideBySideBarSeriesLabel1.Position = DevExpress.XtraCharts.BarSeriesLabelPosition.Top;
            series1.Label = sideBySideBarSeriesLabel1;
            series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.Name = "InBound";
            series1.ValueDataMembersSerializable = "TrafficByDayChart.Content.Traffic.InBound.Value";
            sideBySideBarSeriesView1.BarWidth = 0.2D;
            sideBySideBarSeriesView1.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            sideBySideBarSeriesView1.Border.Visibility = DevExpress.Utils.DefaultBoolean.False;
            series1.View = sideBySideBarSeriesView1;
            series2.ArgumentDataMember = "TrafficByDayChart.Content.CurrentPeriod.Caption";
            sideBySideBarSeriesLabel2.Position = DevExpress.XtraCharts.BarSeriesLabelPosition.TopInside;
            series2.Label = sideBySideBarSeriesLabel2;
            series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series2.Name = "OUT";
            series2.ValueDataMembersSerializable = "TrafficByDayChart.Content.Traffic.OutBound.Value";
            series2.Visible = false;
            this.TrafficByDayChart.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            this.TrafficByDayChart.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            this.TrafficByDayChart.SeriesTemplate.SeriesDataMember = "TrafficByDayChart.Content.CurrentPeriod.Caption";
            this.TrafficByDayChart.SeriesTemplate.ValueDataMembersSerializable = "TrafficByDayChart.Content.Traffic.InBound.Value";
            this.TrafficByDayChart.SeriesTemplate.Visible = false;
            this.TrafficByDayChart.SizeF = new System.Drawing.SizeF(1130.11F, 492.8361F);
            // 
            // ReportFooter2
            // 
            this.ReportFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TrafficByDayChartsFooterText});
            this.ReportFooter2.HeightF = 33.41071F;
            this.ReportFooter2.KeepTogether = true;
            this.ReportFooter2.Name = "ReportFooter2";
            // 
            // TrafficByDayChartsFooterText
            // 
            this.TrafficByDayChartsFooterText.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByDayChartsFooterText.ForeColor = System.Drawing.Color.Gray;
            this.TrafficByDayChartsFooterText.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 9.216384F);
            this.TrafficByDayChartsFooterText.Name = "TrafficByDayChartsFooterText";
            this.TrafficByDayChartsFooterText.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.TrafficByDayChartsFooterText.StyleName = "tip";
            this.TrafficByDayChartsFooterText.Text = "Note:this is footer text";
            this.TrafficByDayChartsFooterText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TrafficByStoreCharts
            // 
            this.TrafficByStoreCharts.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportFooter3});
            this.TrafficByStoreCharts.Expanded = false;
            this.TrafficByStoreCharts.Level = 3;
            this.TrafficByStoreCharts.Name = "TrafficByStoreCharts";
            this.TrafficByStoreCharts.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TrafficByStoreChartsTitle,
            this.TrafficByStoreChartsSubTitle,
            this.TrafficbyStoreChart});
            this.Detail2.HeightF = 604.364F;
            this.Detail2.Name = "Detail2";
            // 
            // TrafficByStoreChartsTitle
            // 
            this.TrafficByStoreChartsTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByStoreChartsTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 38.99999F);
            this.TrafficByStoreChartsTitle.Name = "TrafficByStoreChartsTitle";
            this.TrafficByStoreChartsTitle.SizeF = new System.Drawing.SizeF(1130.109F, 24.19432F);
            this.TrafficByStoreChartsTitle.StyleName = "Title";
            this.TrafficByStoreChartsTitle.StylePriority.UseFont = false;
            this.TrafficByStoreChartsTitle.Text = "Traffic by Store";
            // 
            // TrafficByStoreChartsSubTitle
            // 
            this.TrafficByStoreChartsSubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByStoreChartsSubTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 75.04174F);
            this.TrafficByStoreChartsSubTitle.Name = "TrafficByStoreChartsSubTitle";
            this.TrafficByStoreChartsSubTitle.SizeF = new System.Drawing.SizeF(1130.109F, 24.19433F);
            this.TrafficByStoreChartsSubTitle.StyleName = "Title";
            this.TrafficByStoreChartsSubTitle.StylePriority.UseFont = false;
            this.TrafficByStoreChartsSubTitle.Text = "Traffic by Store";
            // 
            // TrafficbyStoreChart
            // 
            this.TrafficbyStoreChart.AutoLayout = true;
            this.TrafficbyStoreChart.BorderColor = System.Drawing.Color.Black;
            this.TrafficbyStoreChart.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TrafficbyStoreChart.DataMember = "TrafficByStoreChart.Content";
            this.TrafficbyStoreChart.DataSource = this.objectDataSource2;
            xyDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisY.Label.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            xyDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram2.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram2.DefaultPane.EnableAxisXZooming = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram2.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram2.DefaultPane.EnableAxisYZooming = DevExpress.Utils.DefaultBoolean.False;
            this.TrafficbyStoreChart.Diagram = xyDiagram2;
            this.TrafficbyStoreChart.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Gradient;
            this.TrafficbyStoreChart.IndicatorsPaletteName = "Equity";
            this.TrafficbyStoreChart.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.TrafficbyStoreChart.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.TrafficbyStoreChart.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.TrafficbyStoreChart.Legend.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficbyStoreChart.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 108.236F);
            this.TrafficbyStoreChart.Name = "TrafficbyStoreChart";
            this.TrafficbyStoreChart.PaletteName = "Palette 1";
            this.TrafficbyStoreChart.PaletteRepository.Add("Palette 1", new DevExpress.XtraCharts.Palette("Palette 1", DevExpress.XtraCharts.PaletteScaleMode.Extrapolate, new DevExpress.XtraCharts.PaletteEntry[] {
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(39)))), ((int)(((byte)(63))))), System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(39)))), ((int)(((byte)(63)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(95)))), ((int)(((byte)(255))))), System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(95)))), ((int)(((byte)(255)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(31)))), ((int)(((byte)(48))))), System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(31)))), ((int)(((byte)(48)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(141)))), ((int)(((byte)(163))))), System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(141)))), ((int)(((byte)(163)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(194)))), ((int)(((byte)(221))))), System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(194)))), ((int)(((byte)(221)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(99)))), ((int)(((byte)(140))))), System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(99)))), ((int)(((byte)(140)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(180)))), ((int)(((byte)(252))))), System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(180)))), ((int)(((byte)(252)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158))))), System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))), System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(34)))), ((int)(((byte)(76))))), System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(34)))), ((int)(((byte)(76)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(86)))), ((int)(((byte)(90))))), System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(86)))), ((int)(((byte)(90)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160))))), System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(216)))), ((int)(((byte)(214))))), System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(216)))), ((int)(((byte)(214)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(24)))), ((int)(((byte)(52))))), System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(24)))), ((int)(((byte)(52)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238))))), System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))))),
                new DevExpress.XtraCharts.PaletteEntry(System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))), System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(20)))), ((int)(((byte)(50))))))}));
            this.TrafficbyStoreChart.SeriesDataMember = "StoreName";
            this.TrafficbyStoreChart.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.TrafficbyStoreChart.SeriesTemplate.ArgumentDataMember = "StoreName";
            sideBySideBarSeriesLabel3.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Gradient;
            sideBySideBarSeriesLabel3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sideBySideBarSeriesLabel3.MaxWidth = 100;
            sideBySideBarSeriesLabel3.Position = DevExpress.XtraCharts.BarSeriesLabelPosition.Top;
            this.TrafficbyStoreChart.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.TrafficbyStoreChart.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.TrafficbyStoreChart.SeriesTemplate.SeriesColorizer = seriesKeyColorColorizer1;
            this.TrafficbyStoreChart.SeriesTemplate.SeriesDataMember = "StoreName";
            this.TrafficbyStoreChart.SeriesTemplate.ValueDataMembersSerializable = "Traffic.InBound.Value";
            this.TrafficbyStoreChart.SizeF = new System.Drawing.SizeF(1130.11F, 458.8132F);
            // 
            // ReportFooter3
            // 
            this.ReportFooter3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TrafficByStoreChartsFooter});
            this.ReportFooter3.HeightF = 34.59072F;
            this.ReportFooter3.Name = "ReportFooter3";
            // 
            // TrafficByStoreChartsFooter
            // 
            this.TrafficByStoreChartsFooter.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrafficByStoreChartsFooter.ForeColor = System.Drawing.Color.Gray;
            this.TrafficByStoreChartsFooter.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 9.39632F);
            this.TrafficByStoreChartsFooter.Name = "TrafficByStoreChartsFooter";
            this.TrafficByStoreChartsFooter.SizeF = new System.Drawing.SizeF(1130.109F, 24.19433F);
            this.TrafficByStoreChartsFooter.StyleName = "tip";
            this.TrafficByStoreChartsFooter.Text = "Note:this is footer text";
            this.TrafficByStoreChartsFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.PageFooter.HeightF = 75F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.StylePriority.UseBackColor = false;
            // 
            // xrPanel1
            // 
            this.xrPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.CreatedDate,
            this.xrLabel2,
            this.pageInfo2,
            this.CorporationLogo});
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(1169F, 75F);
            this.xrPanel1.StylePriority.UseBackColor = false;
            // 
            // CreatedDate
            // 
            this.CreatedDate.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CreatedDate]")});
            this.CreatedDate.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatedDate.ForeColor = System.Drawing.Color.Gray;
            this.CreatedDate.LocationFloat = new DevExpress.Utils.PointFloat(961.7339F, 42.21865F);
            this.CreatedDate.Name = "CreatedDate";
            this.CreatedDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.CreatedDate.SizeF = new System.Drawing.SizeF(188.3751F, 24.19434F);
            this.CreatedDate.StyleName = "PageInfo";
            this.CreatedDate.StylePriority.UsePadding = false;
            this.CreatedDate.Text = "created date";
            this.CreatedDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel2
            // 
            this.xrLabel2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CreatedBy]")});
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.ForeColor = System.Drawing.Color.Gray;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(961.7339F, 15.07733F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.SizeF = new System.Drawing.SizeF(188.3751F, 24.19434F);
            this.xrLabel2.StyleName = "Title";
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseForeColor = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Note:this is footer text";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // pageInfo2
            // 
            this.pageInfo2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.pageInfo2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(556.2967F, 42.00002F);
            this.pageInfo2.Name = "pageInfo2";
            this.pageInfo2.SizeF = new System.Drawing.SizeF(133.9902F, 23.00001F);
            this.pageInfo2.StyleName = "PageInfo";
            this.pageInfo2.StylePriority.UseBorders = false;
            this.pageInfo2.StylePriority.UseFont = false;
            this.pageInfo2.StylePriority.UseTextAlignment = false;
            this.pageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.pageInfo2.TextFormatString = "{0}/{1}";
            // 
            // CorporationLogo
            // 
            this.CorporationLogo.BackColor = System.Drawing.Color.Transparent;
            this.CorporationLogo.ImageSource = new DevExpress.XtraPrinting.Drawing.ImageSource("img", resources.GetString("CorporationLogo.ImageSource"));
            this.CorporationLogo.LocationFloat = new DevExpress.Utils.PointFloat(0F, 12.53866F);
            this.CorporationLogo.Name = "CorporationLogo";
            this.CorporationLogo.NavigateUrl = "https://www.securitastechnology.com.tr/";
            this.CorporationLogo.SizeF = new System.Drawing.SizeF(136F, 52.68F);
            this.CorporationLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.CorporationLogo.StylePriority.UseBackColor = false;
            // 
            // tip
            // 
            this.tip.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.tip.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.tip.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.tip.BorderWidth = 2F;
            this.tip.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tip.ForeColor = System.Drawing.SystemColors.GrayText;
            this.tip.Name = "tip";
            this.tip.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100F);
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel2});
            this.PageHeader.HeightF = 75F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrPanel2
            // 
            this.xrPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.SubTitleLbl,
            this.TitleLbl,
            this.xrLine1,
            this.ApplicationLogo});
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(1169F, 75F);
            this.xrPanel2.StylePriority.UseBackColor = false;
            // 
            // SubTitleLbl
            // 
            this.SubTitleLbl.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SubTitle]")});
            this.SubTitleLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubTitleLbl.LocationFloat = new DevExpress.Utils.PointFloat(188.17F, 38.70831F);
            this.SubTitleLbl.Name = "SubTitleLbl";
            this.SubTitleLbl.SizeF = new System.Drawing.SizeF(961.9391F, 31.29169F);
            this.SubTitleLbl.StylePriority.UseFont = false;
            this.SubTitleLbl.StylePriority.UseTextAlignment = false;
            this.SubTitleLbl.Text = "titleLbl";
            this.SubTitleLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TitleLbl
            // 
            this.TitleLbl.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Title]")});
            this.TitleLbl.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLbl.LocationFloat = new DevExpress.Utils.PointFloat(188.17F, 3.916668F);
            this.TitleLbl.Name = "TitleLbl";
            this.TitleLbl.SizeF = new System.Drawing.SizeF(961.94F, 34.79166F);
            this.TitleLbl.StyleName = "Title";
            this.TitleLbl.StylePriority.UseFont = false;
            this.TitleLbl.StylePriority.UseTextAlignment = false;
            this.TitleLbl.Text = "TrafficSummaryReport";
            this.TitleLbl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.BorderColor = System.Drawing.Color.Transparent;
            this.xrLine1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(158)))), ((int)(((byte)(160)))));
            this.xrLine1.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine1.LineWidth = 2F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(171.0867F, 8.083355F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(2.083344F, 52.68497F);
            this.xrLine1.StylePriority.UseBorderColor = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // ApplicationLogo
            // 
            this.ApplicationLogo.BackColor = System.Drawing.Color.Transparent;
            this.ApplicationLogo.ImageSource = new DevExpress.XtraPrinting.Drawing.ImageSource("img", resources.GetString("ApplicationLogo.ImageSource"));
            this.ApplicationLogo.LocationFloat = new DevExpress.Utils.PointFloat(20F, 10.08335F);
            this.ApplicationLogo.Name = "ApplicationLogo";
            this.ApplicationLogo.NavigateUrl = "https://Traffic.countware.net/";
            this.ApplicationLogo.SizeF = new System.Drawing.SizeF(136F, 52.68497F);
            this.ApplicationLogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.ApplicationLogo.StylePriority.UseBackColor = false;
            this.ApplicationLogo.StylePriority.UseBorderColor = false;
            // 
            // SummaryByDay
            // 
            this.SummaryByDay.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.reportHeaderBand1,
            this.ReportFooter});
            this.SummaryByDay.DataMember = "ComparedStoresSummaryByDay.Content";
            this.SummaryByDay.DataSource = this.objectDataSource2;
            this.SummaryByDay.Expanded = false;
            this.SummaryByDay.Level = 1;
            this.SummaryByDay.Name = "SummaryByDay";
            this.SummaryByDay.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail3.HeightF = 40F;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable2
            // 
            this.xrTable2.BorderColor = System.Drawing.Color.Black;
            this.xrTable2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable2.BorderWidth = 1F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1130.109F, 40F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.BackColor = System.Drawing.Color.Transparent;
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow2.Weight = 11.5D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell7.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell7.BorderWidth = 1F;
            this.xrTableCell7.EvenStyleName = "DetailData2";
            this.xrTableCell7.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CurrentPeriod].[Caption]")});
            this.xrTableCell7.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.OddStyleName = "DetailData3_Odd";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseBorderWidth = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "xrTableCell1";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.14294222914694274D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell8.BorderWidth = 1F;
            this.xrTableCell8.EvenStyleName = "DetailData2";
            this.xrTableCell8.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[InBound].[Value],[Traffic].[InBound].[Unit],[Traffic].[InBound]" +
                    ".[Indicator])"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[InBound].[ForeColor].[IsEmpty], false,[Traffic].[InBound].[ForeCol" +
                    "or] )\n")});
            this.xrTableCell8.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.OddStyleName = "DetailData3_Odd";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseBorderWidth = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseForeColor = false;
            this.xrTableCell8.Text = "xrTableCell11";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.12472400587194152D;
            // 
            // reportHeaderBand1
            // 
            this.reportHeaderBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.ComparedStoresSummaryByDaySubTitle,
            this.ComparedStoresSummaryByDayTitle});
            this.reportHeaderBand1.HeightF = 153.111F;
            this.reportHeaderBand1.Name = "reportHeaderBand1";
            // 
            // xrTable3
            // 
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(20F, 113.111F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.ComparedStoresSummaryByDayTableHeaderRow});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1130.109F, 39.99999F);
            // 
            // ComparedStoresSummaryByDayTableHeaderRow
            // 
            this.ComparedStoresSummaryByDayTableHeaderRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell12});
            this.ComparedStoresSummaryByDayTableHeaderRow.Name = "ComparedStoresSummaryByDayTableHeaderRow";
            this.ComparedStoresSummaryByDayTableHeaderRow.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.BackColor = System.Drawing.Color.Transparent;
            this.xrTableCell10.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StyleName = "DetailCaption2";
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Tag = "Date";
            this.xrTableCell10.Text = "Date";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.054964353521467384D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StyleName = "DetailCaption2";
            this.xrTableCell12.StylePriority.UseBorderColor = false;
            this.xrTableCell12.Tag = "TrafficInBound";
            this.xrTableCell12.Text = "Traffic InBound";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.047959075468732966D;
            // 
            // ComparedStoresSummaryByDaySubTitle
            // 
            this.ComparedStoresSummaryByDaySubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComparedStoresSummaryByDaySubTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 72.91669F);
            this.ComparedStoresSummaryByDaySubTitle.Name = "ComparedStoresSummaryByDaySubTitle";
            this.ComparedStoresSummaryByDaySubTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.ComparedStoresSummaryByDaySubTitle.StyleName = "Title";
            this.ComparedStoresSummaryByDaySubTitle.StylePriority.UseFont = false;
            this.ComparedStoresSummaryByDaySubTitle.Text = "ComparedStoresSummaryByDaySubTitle";
            // 
            // ComparedStoresSummaryByDayTitle
            // 
            this.ComparedStoresSummaryByDayTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComparedStoresSummaryByDayTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 36.87505F);
            this.ComparedStoresSummaryByDayTitle.Name = "ComparedStoresSummaryByDayTitle";
            this.ComparedStoresSummaryByDayTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.ComparedStoresSummaryByDayTitle.StyleName = "Title";
            this.ComparedStoresSummaryByDayTitle.StylePriority.UseFont = false;
            this.ComparedStoresSummaryByDayTitle.Text = "Summary by Day";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ComparedStoresSummaryByDayFooterTitle,
            this.xrLine3});
            this.ReportFooter.HeightF = 35.75757F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // ComparedStoresSummaryByDayFooterTitle
            // 
            this.ComparedStoresSummaryByDayFooterTitle.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComparedStoresSummaryByDayFooterTitle.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComparedStoresSummaryByDayFooterTitle.LocationFloat = new DevExpress.Utils.PointFloat(20F, 10F);
            this.ComparedStoresSummaryByDayFooterTitle.Name = "ComparedStoresSummaryByDayFooterTitle";
            this.ComparedStoresSummaryByDayFooterTitle.SizeF = new System.Drawing.SizeF(1130.11F, 24.19433F);
            this.ComparedStoresSummaryByDayFooterTitle.StyleName = "tip";
            this.ComparedStoresSummaryByDayFooterTitle.Text = "Note:this is footer text";
            this.ComparedStoresSummaryByDayFooterTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(1130.11F, 2F);
            this.xrLine3.StylePriority.UseForeColor = false;
            // 
            // SummaryByStoreByDay
            // 
            this.SummaryByStoreByDay.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.GroupHeader2,
            this.GroupHeader4,
            this.GroupFooter2});
            this.SummaryByStoreByDay.DataMember = "SummaryByStoreByDay.Content";
            this.SummaryByStoreByDay.DataSource = this.objectDataSource2;
            this.SummaryByStoreByDay.Expanded = false;
            this.SummaryByStoreByDay.Level = 5;
            this.SummaryByStoreByDay.Name = "SummaryByStoreByDay";
            // 
            // Detail5
            // 
            this.Detail5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.Detail5.HeightF = 40F;
            this.Detail5.Name = "Detail5";
            this.Detail5.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Order", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            // 
            // xrTable5
            // 
            this.xrTable5.BorderColor = System.Drawing.Color.Black;
            this.xrTable5.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.BorderWidth = 1F;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(1130.109F, 40F);
            this.xrTable5.StylePriority.UseBorderColor = false;
            this.xrTable5.StylePriority.UseBorderWidth = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell23,
            this.xrTableCell24});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 11.5D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell19.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell19.BorderWidth = 1F;
            this.xrTableCell19.EvenStyleName = "DetailData2";
            this.xrTableCell19.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StoreName]")});
            this.xrTableCell19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell19.KeepTogether = true;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.OddStyleName = "DetailData3_Odd";
            this.xrTableCell19.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Merge;
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.StylePriority.UseBorderWidth = false;
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 0.047723838198863354D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell20.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.BorderWidth = 1F;
            this.xrTableCell20.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[CurrentPeriod].[Caption]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]")});
            this.xrTableCell20.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.OddStyleName = "DetailData3_Odd";
            this.xrTableCell20.StyleName = "DetailData2";
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell20.Weight = 0.0477759473756437D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell21.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.BorderWidth = 1F;
            this.xrTableCell21.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Traffic].[InBound].[Value],[Traffic].[InBound].[Unit],[Traffic].[InBound]" +
                    ".[Indicator])\n"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Font.Bold", "[IsBold]"),
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ForeColor", "Iif([Traffic].[InBound].[ForeColor].[IsEmpty], false,[Traffic].[InBound].[ForeCol" +
                    "or])")});
            this.xrTableCell21.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.OddStyleName = "DetailData3_Odd";
            this.xrTableCell21.StyleName = "DetailData2";
            this.xrTableCell21.StylePriority.UseBackColor = false;
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UseForeColor = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Weight = 0.037578494554925526D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell23.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.BorderWidth = 1F;
            this.xrTableCell23.CanShrink = true;
            this.xrTableCell23.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "Concat([Weather].[Description],\' \',[Weather].[Temperature].[Value],\' \',[Weather]." +
                    "[Temperature].[Unit])")});
            this.xrTableCell23.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.NullValueText = "-";
            this.xrTableCell23.OddStyleName = "DetailData3_Odd";
            this.xrTableCell23.RowSpan = 2;
            this.xrTableCell23.StyleName = "DetailData2";
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell23.Weight = 0.03092182479632866D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell24.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell24.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell24.BorderWidth = 1F;
            this.xrTableCell24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.weatherIcon});
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.OddStyleName = "DetailData3_Odd";
            this.xrTableCell24.StyleName = "DetailData2";
            this.xrTableCell24.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseBorderWidth = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell24.Weight = 0.0066537431280571137D;
            // 
            // weatherIcon
            // 
            this.weatherIcon.BorderWidth = 0F;
            this.weatherIcon.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ImageSource", "[Weather].[Icon]")});
            this.weatherIcon.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.weatherIcon.Name = "weatherIcon";
            this.weatherIcon.SizeF = new System.Drawing.SizeF(42.5F, 40F);
            this.weatherIcon.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.weatherIcon.StylePriority.UseBorderWidth = false;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.SummaryByStoreByDayTitle,
            this.SummaryByStoreByDaySubTitle,
            this.xrLabel7});
            this.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader2.HeightF = 113.6445F;
            this.GroupHeader2.KeepTogether = true;
            this.GroupHeader2.Level = 1;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.RepeatEveryPage = true;
            // 
            // SummaryByStoreByDayTitle
            // 
            this.SummaryByStoreByDayTitle.Font = new System.Drawing.Font("Arial", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreByDayTitle.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 39.76394F);
            this.SummaryByStoreByDayTitle.Name = "SummaryByStoreByDayTitle";
            this.SummaryByStoreByDayTitle.SizeF = new System.Drawing.SizeF(1133.78F, 24.19432F);
            this.SummaryByStoreByDayTitle.StyleName = "Title";
            this.SummaryByStoreByDayTitle.StylePriority.UseFont = false;
            this.SummaryByStoreByDayTitle.Text = "SummaryByStoreByDayTitle";
            // 
            // SummaryByStoreByDaySubTitle
            // 
            this.SummaryByStoreByDaySubTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreByDaySubTitle.LocationFloat = new DevExpress.Utils.PointFloat(621.0416F, 75.80567F);
            this.SummaryByStoreByDaySubTitle.Name = "SummaryByStoreByDaySubTitle";
            this.SummaryByStoreByDaySubTitle.SizeF = new System.Drawing.SizeF(155.67F, 24.19433F);
            this.SummaryByStoreByDaySubTitle.StyleName = "Title";
            this.SummaryByStoreByDaySubTitle.StylePriority.UseFont = false;
            this.SummaryByStoreByDaySubTitle.Text = "SummaryByStoreByDaySubTitle";
            this.SummaryByStoreByDaySubTitle.Visible = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StoreName]\n")});
            this.xrLabel7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(19.99999F, 75.80564F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.SizeF = new System.Drawing.SizeF(1133.78F, 24.19433F);
            this.xrLabel7.StyleName = "Title";
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Summary by Store";
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.SummaryByStoreByDayHeaderTable});
            this.GroupHeader4.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("StoreId", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader4.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader4.HeightF = 40F;
            this.GroupHeader4.KeepTogether = true;
            this.GroupHeader4.Name = "GroupHeader4";
            this.GroupHeader4.RepeatEveryPage = true;
            // 
            // SummaryByStoreByDayHeaderTable
            // 
            this.SummaryByStoreByDayHeaderTable.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.SummaryByStoreByDayHeaderTable.Name = "SummaryByStoreByDayHeaderTable";
            this.SummaryByStoreByDayHeaderTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.SummaryByStoreByDayHeaderRow});
            this.SummaryByStoreByDayHeaderTable.SizeF = new System.Drawing.SizeF(1130.109F, 40F);
            // 
            // SummaryByStoreByDayHeaderRow
            // 
            this.SummaryByStoreByDayHeaderRow.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell18});
            this.SummaryByStoreByDayHeaderRow.Name = "SummaryByStoreByDayHeaderRow";
            this.SummaryByStoreByDayHeaderRow.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StyleName = "DetailCaption2";
            this.xrTableCell14.StylePriority.UseBorderColor = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Tag = "StoreName";
            this.xrTableCell14.Text = "Store Name";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 0.05193371125697218D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StyleName = "DetailCaption2";
            this.xrTableCell15.StylePriority.UseBorderColor = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Tag = "Date";
            this.xrTableCell15.Text = "Date";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.0519904190921704D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StyleName = "DetailCaption2";
            this.xrTableCell16.StylePriority.UseBorderColor = false;
            this.xrTableCell16.Tag = "TrafficInBound";
            this.xrTableCell16.Text = "Traffic InBound";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.040893416137462418D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StyleName = "DetailCaption2";
            this.xrTableCell18.StylePriority.UseBorderColor = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Tag = "Weather";
            this.xrTableCell18.Text = "Weather";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell18.Weight = 0.040890236780554742D;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel8,
            this.SummaryByStoreByDayFooterTotal,
            this.SummaryByStoreByDayFooter});
            this.GroupFooter2.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter2.HeightF = 66.43815F;
            this.GroupFooter2.Name = "GroupFooter2";
            this.GroupFooter2.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum(Iif([Traffic].[InBound].[IsComparedRow],0 ,[Traffic].[InBound].[Value]))")});
            this.xrLabel8.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(652.4215F, 0F);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(248.8533F, 29.19444F);
            this.xrLabel8.StyleName = "Title";
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.xrLabel8.Summary = xrSummary3;
            this.xrLabel8.Text = "xrLabel3";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // SummaryByStoreByDayFooterTotal
            // 
            this.SummaryByStoreByDayFooterTotal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(65)))), ((int)(((byte)(158)))));
            this.SummaryByStoreByDayFooterTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.SummaryByStoreByDayFooterTotal.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreByDayFooterTotal.LocationFloat = new DevExpress.Utils.PointFloat(20F, 0F);
            this.SummaryByStoreByDayFooterTotal.Name = "SummaryByStoreByDayFooterTotal";
            this.SummaryByStoreByDayFooterTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(455, 0, 0, 0, 100F);
            this.SummaryByStoreByDayFooterTotal.SizeF = new System.Drawing.SizeF(1130.109F, 29.19439F);
            this.SummaryByStoreByDayFooterTotal.StyleName = "Title";
            this.SummaryByStoreByDayFooterTotal.StylePriority.UseBorderColor = false;
            this.SummaryByStoreByDayFooterTotal.StylePriority.UseBorders = false;
            this.SummaryByStoreByDayFooterTotal.StylePriority.UseFont = false;
            this.SummaryByStoreByDayFooterTotal.StylePriority.UsePadding = false;
            this.SummaryByStoreByDayFooterTotal.StylePriority.UseTextAlignment = false;
            this.SummaryByStoreByDayFooterTotal.Tag = "Total";
            this.SummaryByStoreByDayFooterTotal.Text = "Total";
            this.SummaryByStoreByDayFooterTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // SummaryByStoreByDayFooter
            // 
            this.SummaryByStoreByDayFooter.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryByStoreByDayFooter.ForeColor = System.Drawing.Color.Gray;
            this.SummaryByStoreByDayFooter.LocationFloat = new DevExpress.Utils.PointFloat(20F, 42.24382F);
            this.SummaryByStoreByDayFooter.Name = "SummaryByStoreByDayFooter";
            this.SummaryByStoreByDayFooter.SizeF = new System.Drawing.SizeF(1130.109F, 24.19433F);
            this.SummaryByStoreByDayFooter.StyleName = "tip";
            this.SummaryByStoreByDayFooter.Text = "Footertitle";
            this.SummaryByStoreByDayFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // WeeklyTrafficReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.BottomMargin,
            this.MainDetail,
            this.SummaryByStore,
            this.ComparedStoresSummary,
            this.TrafficByDayCharts,
            this.TrafficByStoreCharts,
            this.PageFooter,
            this.PageHeader,
            this.SummaryByDay,
            this.SummaryByStoreByDay});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource2});
            this.DataSource = this.objectDataSource2;
            this.DrawWatermark = true;
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.GroupCaption2,
            this.GroupData2,
            this.DetailCaption2,
            this.DetailData2,
            this.GroupFooterBackground3,
            this.DetailData3_Odd,
            this.PageInfo,
            this.tip});
            this.Version = "22.1";
            this.Watermark.Font = new System.Drawing.Font("Calibri", 120F);
            this.Watermark.TextTransparency = 193;
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryByStoreHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComparedStoresSummaryHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficByDayChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficbyStoreChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryByStoreByDayHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin; 
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.DetailBand MainDetail;
        private DevExpress.XtraReports.UI.DetailReportBand SummaryByStore;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRControlStyle Title;
        private DevExpress.XtraReports.UI.XRControlStyle GroupCaption2;
        private DevExpress.XtraReports.UI.XRControlStyle GroupData2;
        private DevExpress.XtraReports.UI.XRControlStyle DetailCaption2;
        private DevExpress.XtraReports.UI.XRControlStyle DetailData2;
        private DevExpress.XtraReports.UI.XRControlStyle GroupFooterBackground3;
        private DevExpress.XtraReports.UI.XRControlStyle DetailData3_Odd;
        private DevExpress.XtraReports.UI.XRControlStyle PageInfo;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource2;
        private DevExpress.XtraReports.UI.DetailReportBand ComparedStoresSummary;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable ComparedStoresSummaryHeaderTable;
        private DevExpress.XtraReports.UI.XRTableRow ComparedStoresSummaryTableHeaderRow;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRLabel ComparedStoresSummaryTitle;
        private DevExpress.XtraReports.UI.XRLabel ComparedStoresSummarySubTitle;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreFooterTitle;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter1;
        private DevExpress.XtraReports.UI.XRLabel ComparedStoresSummaryFooterTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.DetailReportBand TrafficByDayCharts;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRChart TrafficByDayChart;
        private DevExpress.XtraReports.UI.DetailReportBand TrafficByStoreCharts;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRLabel TrafficByDayChartsTitle;
        private DevExpress.XtraReports.UI.XRLabel TrafficByDayChartsSubTitle;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter2;
        private DevExpress.XtraReports.UI.XRLabel TrafficByDayChartsFooterText;
        private DevExpress.XtraReports.UI.XRChart TrafficbyStoreChart;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel TrafficByStoreChartsTitle;
        private DevExpress.XtraReports.UI.XRLabel TrafficByStoreChartsSubTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreSubTitle;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreTitle;
        private DevExpress.XtraReports.UI.XRTable SummaryByStoreHeaderTable;
        private DevExpress.XtraReports.UI.XRTableRow SummaryTableHeaderRow;
        private DevExpress.XtraReports.UI.XRTableCell tableCell4;
        private DevExpress.XtraReports.UI.XRTableCell tableCell5;
        private DevExpress.XtraReports.UI.XRTableCell tableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRPageInfo pageInfo2;
        private DevExpress.XtraReports.UI.XRPictureBox CorporationLogo;
        private DevExpress.XtraReports.UI.XRPanel xrPanel2;
        private DevExpress.XtraReports.UI.XRLabel SubTitleLbl;
        private DevExpress.XtraReports.UI.XRLabel TitleLbl;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRPictureBox ApplicationLogo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRControlStyle tip;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter3;
        private DevExpress.XtraReports.UI.XRLabel TrafficByStoreChartsFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRTable table3;
        private DevExpress.XtraReports.UI.XRTableRow tableRow3;
        private DevExpress.XtraReports.UI.XRTableCell tableCell29;
        private DevExpress.XtraReports.UI.XRTableCell tableCell30;
        private DevExpress.XtraReports.UI.XRTableCell tableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel CreatedDate;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreFooterTotal;
        private DevExpress.XtraReports.UI.DetailReportBand SummaryByDay;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.ReportHeaderBand reportHeaderBand1;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow ComparedStoresSummaryByDayTableHeaderRow;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRLabel ComparedStoresSummaryByDaySubTitle;
        private DevExpress.XtraReports.UI.XRLabel ComparedStoresSummaryByDayTitle;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel ComparedStoresSummaryByDayFooterTitle;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.DetailReportBand SummaryByStoreByDay;
        private DevExpress.XtraReports.UI.DetailBand Detail5;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreByDayTitle;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreByDaySubTitle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable SummaryByStoreByDayHeaderTable;
        private DevExpress.XtraReports.UI.XRTableRow SummaryByStoreByDayHeaderRow;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreByDayFooterTotal;
        private DevExpress.XtraReports.UI.XRLabel SummaryByStoreByDayFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRPictureBox weatherIcon;
    }
}
