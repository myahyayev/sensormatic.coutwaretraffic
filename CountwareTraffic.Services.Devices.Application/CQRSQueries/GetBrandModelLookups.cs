﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class GetBrandModelLookups : IQuery<List<BrandModelLookupDetailsDto>>
    {
        public Guid BrandId { get; set; }
    }
}
