﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class SubAreaCreatedSuccessfullyHandler : ICommandHandler<SubAreaCreatedSuccessfully>
    {
        private readonly IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> _hubContext;
        public SubAreaCreatedSuccessfullyHandler(IHubContext<SubAreaNotificationsHub, ISubAreaNotificationsHub> hubContext) 
            => _hubContext = hubContext;

        public async Task HandleAsync(SubAreaCreatedSuccessfully command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .SubAreaCreatedSuccessfullyReceived(command);
    }
}
