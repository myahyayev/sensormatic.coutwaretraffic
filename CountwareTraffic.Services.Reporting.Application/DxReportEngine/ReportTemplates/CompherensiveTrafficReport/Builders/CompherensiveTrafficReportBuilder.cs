﻿using CountwareTraffic.Services.Reporting.Application;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Ioc;
using System;

namespace CountwareTraffic.Services.Reporting.Engine
{
    internal class CompherensiveTrafficReportBuilder : ReportBuilder, IScopedSelfDependency
    {

        public CompherensiveTrafficReportBuilder(IServiceProvider serviceProvider, IOptions<TimeZoneSettings> timeZoneSettings) : base(serviceProvider, timeZoneSettings) { }

        public override IReportBuilder WithPeriodType(ReportPeriodType reportPeriodType)
        {
            switch (reportPeriodType)
            {
                case ReportPeriodType.Unknown:
                    break;
                case ReportPeriodType.Daily:
                    //Report = new DailyCompherensiveTrafficReport(ServiceProvider);
                    break;
                case ReportPeriodType.Weekly:
                    //Report = new WeeklyCompherensiveTrafficReport(ServiceProvider);
                    break;
                case ReportPeriodType.Monthly:
                    // Report = new MonthlyCompherensiveTrafficReport(ServiceProvider);
                    break;
            }

            base.WithPeriodType(reportPeriodType);

            return this;
        }
    }
}
