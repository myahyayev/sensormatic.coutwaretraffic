﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportSchedule : IEntity<Guid>, IDeletable
    {
        private Guid _id;
        private Guid _reportId;
        private int _frequency;
        private int _deliveryTimeId;
        private DateTime? _startsOn;
        private DateTime? _endsOn;
        private string _atSetTime;
        private string _atSetTimeZone;
        private string _repeatOn;
        private DateTime? _lastRunTime;

        public Guid Id => _id;
        public Guid ReportId => _reportId;
        public ScheduleFrequency ScheduleFrequency { get; private set; }
        public ScheduleDeliveryTime ScheduleDeliveryTime { get; private set; }
        public DateTime? StartsOn => _startsOn;
        public DateTime? EndsOn => _endsOn;
        public string AtSetTime => _atSetTime;
        public string AtSetTimeZone => _atSetTimeZone;
        public string RepeatOn => _repeatOn;
        public DateTime? LastRunTime => _lastRunTime;
        public bool AuditIsDeleted { get; set; }

        private ReportSchedule() { }

        public static ReportSchedule Create(Guid reportId, int frequency, int deliveryTimeId, DateTime? startsOn, DateTime? endsOn, string atSetTime, string atSetTimeZone, string repeatOn, int templateTypeId)
        {
            ReportSchedule reportSchedule = new();
            reportSchedule._reportId = reportId;
            reportSchedule._frequency = frequency;
            reportSchedule._deliveryTimeId = deliveryTimeId;

            if (frequency == ScheduleFrequency.Repeat.Id)
            {
                if (startsOn == null)
                    startsOn = DateTime.Now;

                reportSchedule._startsOn = startsOn;
                reportSchedule._endsOn = endsOn;

                if (deliveryTimeId == ScheduleDeliveryTime.RunAutomatically.Id)
                {
                    reportSchedule._atSetTime = "11:59 PM";
                }
                else if (deliveryTimeId == ScheduleDeliveryTime.SchedulAtSetTime.Id)
                {
                    reportSchedule._atSetTime = atSetTime;
                    reportSchedule._atSetTimeZone = atSetTimeZone;
                }

                if (templateTypeId == TemplateType.Weekly.Id || templateTypeId == TemplateType.Monthly.Id)
                {
                    reportSchedule._repeatOn = repeatOn;
                }
            }

            return reportSchedule;
        }

        public void SetLastRunTime(DateTime lastRunTime) 
            => _lastRunTime = lastRunTime;

    }
}
