using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sensormatic.Tool.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.PushNotification.Consumer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureHostOptions(options =>
                {
                    options.BackgroundServiceExceptionBehavior = BackgroundServiceExceptionBehavior.Ignore;
                })
                .ConfigureServices((hostContext, services) =>
                {           //todo:disabled for empty app
                    services//.AddApplicationInsights(hostContext.Configuration)
                            .AddHostedService<Worker>();
                });
    }
}
