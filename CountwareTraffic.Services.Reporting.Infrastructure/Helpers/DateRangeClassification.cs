﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class DateRangeClassification : Classification
    {
        public static DateRangeClassification Today = new(1, nameof(Today), "Today");
        public static DateRangeClassification ThisWeek = new(2, nameof(ThisWeek), "This Week");
        public static DateRangeClassification LastWeek = new(3, nameof(LastWeek), "Last Week");
        public static DateRangeClassification ThisMonth = new(4, nameof(ThisMonth), "This Month");
        public static DateRangeClassification LastMonth = new(5, nameof(LastMonth), "Last Month");
        public static DateRangeClassification LastYear = new(6, nameof(LastYear), "Last Year");
        public static DateRangeClassification LastLastYear = new(7, nameof(LastLastYear), "Last Last Year");
        public static DateRangeClassification Today_vs_LastWeek = new(8, nameof(Today_vs_LastWeek), "Today vs Last Week");
        public static DateRangeClassification Today_vs_LastYear = new(9, nameof(Today_vs_LastYear), "Today vs Last Year");
        public static DateRangeClassification Today_vs_LastLastYear = new(10, nameof(Today_vs_LastLastYear), "Today vs Last Last Year");
        public static DateRangeClassification ThisWeek_vs_LastWeek = new(11, nameof(ThisWeek_vs_LastWeek), "This Week vs Last Week");
        public static DateRangeClassification ThisWeek_vs_LastYear = new(12, nameof(ThisWeek_vs_LastYear), "This Week vs Last Year");
        public static DateRangeClassification ThisWeek_vs_LastlastYear = new(13, nameof(ThisWeek_vs_LastlastYear), "This Week vs Last Last Year");
        public static DateRangeClassification ThisMonth_vs_LastMonth = new(14, nameof(ThisMonth_vs_LastMonth), "This Month vs Last Month");
        public static DateRangeClassification ThisMonth_vs_LastYear = new(15, nameof(ThisMonth_vs_LastYear), "This Month vs Last Year");
        public static DateRangeClassification ThisMonth_vs_LastLastYear = new(16, nameof(ThisMonth_vs_LastLastYear), "This Month vs Last Last Year");

        public DateRangeClassification(int id, string name, string displayName) 
            : base(id, name, displayName, "") { }

        public static IEnumerable<DateRangeClassification> List() => new[]
        {
            Today, ThisWeek,
            LastWeek,
            ThisMonth,
            LastMonth,
            LastYear,
            LastLastYear,
            Today_vs_LastWeek,
            Today_vs_LastYear,
            Today_vs_LastLastYear,
            ThisWeek_vs_LastWeek,
            ThisWeek_vs_LastYear,
            ThisWeek_vs_LastlastYear,
            ThisMonth_vs_LastMonth,
            ThisMonth_vs_LastYear,
            ThisMonth_vs_LastLastYear
        };

        public override string ResourceKey
            => $"{GetType().FullName}.{Name}";

        public static DateRangeClassification FromName(string name)
            => List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

        public static DateRangeClassification From(int id)
            => List().SingleOrDefault(s => s.Id == id);

        public static DateRangeClassification FromDisplayName(string displayName)
            => List().SingleOrDefault(s => s.DisplayName == displayName);
    }
}
