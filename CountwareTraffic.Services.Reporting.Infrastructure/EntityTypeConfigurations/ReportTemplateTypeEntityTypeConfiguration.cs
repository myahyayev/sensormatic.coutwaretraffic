﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportTemplateTypeEntityTypeConfiguration : IEntityTypeConfiguration<ReportTemplateType>
    {
        public void Configure(EntityTypeBuilder<ReportTemplateType> builder)
        {
            builder.ToTable("ReportTemplateTypes", SchemaNames.Reports);

            builder
                .HasKey(x => new { x.TypeId, x.ReportTemplateId });

            builder
                .HasOne<ReportTemplate>()
                .WithMany(x => x.Types)
                .HasForeignKey(x => x.ReportTemplateId)
                .IsRequired();


            builder
                .HasOne<TemplateType>()
                .WithMany()
                .HasForeignKey(x => x.TypeId)
                .IsRequired();

            builder.HasIndex(x => new { x.TypeId, x.ReportTemplateId });
        }
    }
}