﻿using Convey.CQRS.Commands;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    [Contract]
    public class DeleteRegion : ICommand
    {
        public Guid RegionId { get; set; }
    }
}
