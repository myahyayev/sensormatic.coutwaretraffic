﻿using CountwareTraffic.Services.Companies.Application;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Companies.Infrastructure.Services
{
    public class QueueEventMapper : IQueueEventMapper
    {
        public List<Sensormatic.Tool.Queue.IQueueEvent> MapAll(IEnumerable<Sensormatic.Tool.Efcore.IDomainEvent> events, Guid userId, string correlationId, Guid tenantId)
             => events.Select(@event => Map(@event, userId, correlationId, tenantId)).ToList();

        public Sensormatic.Tool.Queue.IQueueEvent Map(Sensormatic.Tool.Efcore.IDomainEvent @event, Guid userId, string correlationId, Guid tenantId)
        {
            switch (@event)
            {
                case Core.AreaCreated e:
                    return new Sensormatic.Tool.QueueModel.AreaCreated(
                        e.AreaId,
                        e.AreaName,
                        e.RecordId,
                        e.WorkingHoursStart,
                        e.WorkingHoursEnd,
                        e.WorkingTimeZone,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.AreaChanged e:
                    return new Sensormatic.Tool.QueueModel.AreaChanged(
                        e.AreaId,
                        e.AreaName,
                        e.OldAreaName,
                        e.RecordId,
                        e.WorkingHoursStart,
                        e.OldWorkingHoursStart,
                        e.WorkingHoursEnd,
                        e.OldWorkingHoursEnd,
                        e.WorkingTimeZone,
                        e.OldWorkingTimeZone,
                        userId,
                        correlationId,
                        tenantId
                    );
                case Core.AreaDeleted e:
                    return new Sensormatic.Tool.QueueModel.AreaDeleted(
                        e.AreaId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );
                case Core.SubAreaCreated e:
                    return new Sensormatic.Tool.QueueModel.SubAreaCreated(
                        e.SubAreaId,
                        e.Name,
                        e.AreaId,
                        e.AreaName,
                        e.RegionId,
                        e.RegionName,
                        e.CountryId,
                        e.CountryName,
                        e.CompanyId,
                        e.CompanyName,
                        e.CountryLookupId,
                        e.CityId,
                        e.CityName,
                        e.DistrictId,
                        e.DistrictName,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );


                case Core.SubAreaChanged e:
                    return new Sensormatic.Tool.QueueModel.SubAreaChanged(
                        e.SubAreaId,
                        e.Name,
                        e.OldName,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );


                case Core.SubAreaDeleted e:
                    return new Sensormatic.Tool.QueueModel.SubAreaDeleted(
                        e.SubAreaId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.CompanyHierarchyCreated e:
                    return new Sensormatic.Tool.QueueModel.CompanyCreated(
                        e.CompanyId,
                        e.CompanyName,
                        e.TenantName,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.CompanyHierarchyDeleted e:
                    return new Sensormatic.Tool.QueueModel.CompanyDeleted(
                        e.CompanyId,
                        e.CompanyName,
                        e.TenantName,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.CompanyHierarchyUpdated e:
                    return new Sensormatic.Tool.QueueModel.CompanyUpdated(
                        e.CompanyId,
                        e.CompanyName,
                        e.TenantName,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );


                case Core.CountryHierarchyCreated e:
                    return new Sensormatic.Tool.QueueModel.CountryCreated(
                        e.CountryId,
                        e.CountryName,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.CountryHierarchyUpdated e:
                    return new Sensormatic.Tool.QueueModel.CountryUpdated(
                        e.CountryId,
                        e.CountryName,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.CountryHierarchyDeleted e:
                    return new Sensormatic.Tool.QueueModel.CountryDeleted(
                        e.CountryId,
                        e.CountryName,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.RegionHierarchyCreated e:
                    return new Sensormatic.Tool.QueueModel.RegionCreated(
                        e.RegionId,
                        e.RegionName,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.RegionHierarchyDeleted e:
                    return new Sensormatic.Tool.QueueModel.RegionDeleted(
                        e.RegionId,
                        e.RegionName,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.RegionHierarchyUpdated e:
                    return new Sensormatic.Tool.QueueModel.RegionUpdated(
                        e.RegionId,
                        e.RegionName,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.AreaHierarchyCreated e:
                    return new Sensormatic.Tool.QueueModel.AreaHCreated(
                        e.AreaId,
                        e.AreaName,
                        e.RegionId,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.AreaHierarchyUpdated e:
                    return new Sensormatic.Tool.QueueModel.AreaHUpdated(
                        e.AreaId,
                        e.AreaName,
                        e.RegionId,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.AreaHierarchyDeleted e:
                    return new Sensormatic.Tool.QueueModel.AreaHDeleted(
                        e.AreaId,
                        e.AreaName,
                        e.RegionId,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );


                case Core.SubAreaHierarchyCreated e:
                    return new Sensormatic.Tool.QueueModel.SubAreaHCreated(
                        e.SubAreaId,
                        e.SubAreaName,
                        e.AreaId,
                        e.RegionId,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.SubAreaHierarchyUpdated e:
                    return new Sensormatic.Tool.QueueModel.SubAreaHUpdated(
                        e.SubAreaId,
                        e.SubAreaName,
                        e.AreaId,
                        e.RegionId,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.SubAreaHierarchyDeleted e:
                    return new Sensormatic.Tool.QueueModel.SubAreaHDeleted(
                        e.SubAreaId,
                        e.SubAreaName,
                        e.AreaId,
                        e.RegionId,
                        e.CountryId,
                        e.CompanyId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );


                case Core.AreaTypeChanged e:
                    return null;
            }

            return null;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
