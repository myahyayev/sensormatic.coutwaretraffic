﻿using CountwareTraffic.Services.Reporting.Application;
using Microsoft.Extensions.Options;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Ioc;
using System;

namespace CountwareTraffic.Services.Reporting.Engine
{
    public class TrafficReportBuilder : ReportBuilder, IScopedSelfDependency
    {
        public TrafficReportBuilder(IServiceProvider serviceProvider, IOptions<TimeZoneSettings> timeZoneSettings)
            : base(serviceProvider, timeZoneSettings) { }

        public override TrafficReportBuilder WithPeriodType(ReportPeriodType reportPeriodType)
        {
            switch (reportPeriodType)
            {
                case ReportPeriodType.Unknown:
                    break;
                case ReportPeriodType.Daily:
                    Report = new DailyTrafficReport(ServiceProvider, TimeZoneSettings);
                    break;
                case ReportPeriodType.Weekly:
                    Report = new WeeklyTrafficReport(ServiceProvider, TimeZoneSettings);
                    break;
                case ReportPeriodType.Monthly:
                    Report = new MonthlyTrafficReport(ServiceProvider, TimeZoneSettings);
                    break;
            }

            base.WithPeriodType(reportPeriodType);
            return this;
        }

       
    }
}
