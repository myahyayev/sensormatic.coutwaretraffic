﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class DeviceCreatedSuccessfullyHandler : ICommandHandler<DeviceCreatedSuccessfully>
    {
        private readonly IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> _hubContext;
        public DeviceCreatedSuccessfullyHandler(IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> hubContext)
            => _hubContext = hubContext;

        public async Task HandleAsync(DeviceCreatedSuccessfully command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .DeviceCreatedSuccessfullyReceived(command);
    }
}
