﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Events.Infrastructure.Migrations
{
    public partial class ChnageDeviceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "AreaId",
                schema: "events",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "AreaName",
                schema: "events",
                table: "Devices",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "CityId",
                schema: "events",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "CityName",
                schema: "events",
                table: "Devices",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "CompanyId",
                schema: "events",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "CompanyLookupId",
                schema: "events",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                schema: "events",
                table: "Devices",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "CountryId",
                schema: "events",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "CountryName",
                schema: "events",
                table: "Devices",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "DistrictId",
                schema: "events",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "DistrictName",
                schema: "events",
                table: "Devices",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "RegionId",
                schema: "events",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "RegionName",
                schema: "events",
                table: "Devices",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaId",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "AreaName",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "CityId",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "CityName",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "CompanyLookupId",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "CountryId",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "CountryName",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "DistrictId",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "DistrictName",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "RegionId",
                schema: "events",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "RegionName",
                schema: "events",
                table: "Devices");
        }
    }
}
