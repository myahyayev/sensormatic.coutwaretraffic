﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class EventsController : BaseController
    {
        private readonly EventService _eventService;
        public EventsController(IServiceProvider provider, ILogger<EventsController> logger, IHttpContextAccessor contextAccessor, EventService eventService)
            : base(provider, logger, contextAccessor) => _eventService = eventService;


       
    }
}
