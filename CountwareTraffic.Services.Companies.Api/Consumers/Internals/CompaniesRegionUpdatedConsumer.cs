﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesRegionUpdatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.RegionUpdated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesRegionUpdatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.RegionUpdated queuEvent)
        {
            var regionUpdated = _eventMapper.Map(queuEvent) as RegionUpdated;
            await _eventDispatcher.PublishAsync(regionUpdated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
