﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Reporting.Grpc
{
    [ServiceLog]
    public sealed partial class GetReportFilesRequest : SensormaticRequestValidate
    {
        internal DataSourceRequest _DataSourceRequest
        {
            get
            {
                if (this.dataSourceRequest_.PagingRequest == null)
                    this.dataSourceRequest_.PagingRequest = new PagingRequest() { Limit = 10, Page = 1 };

                if (this.dataSourceRequest_.PagingRequest.Limit < 1)
                    this.dataSourceRequest_.PagingRequest.Limit = 10;

                if (this.dataSourceRequest_.PagingRequest.Page < 1)
                    this.dataSourceRequest_.PagingRequest.Page = 1;

                return this.dataSourceRequest_;
            }
            set { this.dataSourceRequest_ = value; }
        }

        internal Guid _ReportId
        {
            get
            {
                if (Guid.TryParse(reportId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { reportId_ = value.ToString(); }
        }


        public override void Validate()
        {
            if (_ReportId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1534 ReportId cannot be null", nameof(_ReportId)));
        }
    }
}
