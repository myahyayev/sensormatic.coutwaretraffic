﻿using Sensormatic.Tool.Core;

namespace CountwareTraffic.Services.Devices.Grpc
{
    [ServiceLog]
    public sealed partial class GetDevicesWithoutParentRequest : SensormaticRequestValidate
    {
        internal DataSourceRequest _DataSourceRequest
        {
            get
            {
                if (this.dataSourceRequest_.PagingRequest == null)
                    this.dataSourceRequest_.PagingRequest = new PagingRequest() { Limit = 10, Page = 1 };

                if (this.dataSourceRequest_.PagingRequest.Limit < 1)
                    this.dataSourceRequest_.PagingRequest.Limit = 10;

                if (this.dataSourceRequest_.PagingRequest.Page < 1)
                    this.dataSourceRequest_.PagingRequest.Page = 1;

                return this.dataSourceRequest_;
            }
            set { this.dataSourceRequest_ = value; }
        }


        public override void Validate()
        {
            
        }
    }
}
