﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class ControlOtpTokenHandler : IQueryHandler<ControlOtpToken, IdentityTokenResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public ControlOtpTokenHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityTokenResponse> HandleAsync(ControlOtpToken query)
        {
            return await _identityServerService.ControlOtpTokenAsync(new IdentityTokenRequest
            {
                Token = query.Token,
                Payload = new Payload
                {
                    Password = query.Password,
                    UserName = query.UserName,
                    TenantId = query.TenantId,
                    Language = query.Language,
                    Otp = query.Otp
                }
            });
        }
    }
}
