﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class UpdatePermissionHandler : IQueryHandler<UpdatePermission, IdentityUpdatePermissionResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public UpdatePermissionHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityUpdatePermissionResponse> HandleAsync(UpdatePermission query)
        {
            return await _identityServerService.UpdatePermissionAsync(new IdentityUpdatePermissionsRequest
            {
                Id = query.Id,
                Name = query.Name,
                Description = query.Description
            });
        }
    }
}
