﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Reporting.Core;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class RetryReportGenerationHandler : ICommandHandler<RetryReportGeneration>
    {
        private readonly IQueueService _queueService;
        private readonly IUnitOfWork _unitOfWork;
        public RetryReportGenerationHandler(IQueueService queueService, IUnitOfWork unitOfWork)
        {
            _queueService = queueService;
            _unitOfWork = unitOfWork;
        }

        public async Task HandleAsync(RetryReportGeneration command)
        {
            var report = await _unitOfWork.GetRepository<IReportRepository>()
                .GetReportByIdAsync(command.ReportId);

            bool addQueue = false;

            if (report.Status != ReportStatus.Generating)
            {
                if (report.ReportSchedule.Frequency.Id == ScheduleFrequency.Repeat.Id)
                {
                    var reportFile = await _unitOfWork.GetRepository<IReportFileRepository>()
                        .GetByIdWithReportIdAsync(command.ReportFileId.Value, command.ReportId);

                    if (reportFile != null && reportFile.FilePath == OperationStatus.Failed.ToString())
                    {
                        addQueue = true;
                    }
                }
                else if((report.ReportSchedule.Frequency.Id == ScheduleFrequency.OneTime.Id))
                {
                    addQueue = true;
                }
            }

            if (addQueue)
            {
                _queueService.Send(Queues.CountwareTrafficReportingRetryGenerating, new Sensormatic.Tool.QueueModel.RetryReportGeneration
                {
                    DateRangeEndDate = command.DateRangeEndDate,
                    DateRangeStartDate = command.DateRangeStartDate,
                    ReportFileId = command.ReportFileId,
                    ReportId = command.ReportId
                });
            }
        }
    }
}
