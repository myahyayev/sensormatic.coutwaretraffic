﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class DeviceDeletedSuccessfullyHandler : ICommandHandler<DeviceDeletedSuccessfully>
    {
        private readonly IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> _hubContext;
        public DeviceDeletedSuccessfullyHandler(IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> hubContext) 
            => _hubContext = hubContext;


        public async Task HandleAsync(DeviceDeletedSuccessfully command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .DeviceDeletedSuccessfullyReceived(command);
    }
}
