﻿using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Responses;
using CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler.Grpc.Services;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.ElasticIndexRepair.Scheduler
{
    public class ElasticSearchTenantsHierarchyIndexRepairJob : IJob
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly ITenantHierarchyElasticSearchRepository _eventElasticSearchRepository;
        private readonly UserService _userService;
        private readonly DeviceService _deviceService;
        private readonly ILogger<ElasticSearchTenantsHierarchyIndexRepairJob> _logger;

        public ElasticSearchTenantsHierarchyIndexRepairJob(ICompanyRepository companyRepository,
            ITenantHierarchyElasticSearchRepository eventElasticSearchRepository,
            UserService userService,
            DeviceService deviceService,
            ILogger<ElasticSearchTenantsHierarchyIndexRepairJob> logger)
        {
            _companyRepository = companyRepository;
            _eventElasticSearchRepository = eventElasticSearchRepository;
            _userService = userService;
            _deviceService = deviceService;
            _logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            _logger.LogInformation("ElasticSearchTenantsHierarchyIndexRepairJob starting...");

            await Run();

            _logger.LogInformation("ElasticSearchTenantsHierarchyIndexRepairJob ended...");
        }

        private async Task Run()
        {
            var tenants = await _userService.GetTenantsAsync();

            foreach (var tenant in tenants)
            {
                var tenantHierarchy = await _companyRepository.GetTenantHierarchyAsync(tenant.TenantId, tenant.Name);
                await PrepareDevices(tenant, tenantHierarchy);

                await _eventElasticSearchRepository.AddOrUpdateTenantHierarchyAsync(tenantHierarchy);

                _logger.LogInformation($"{tenant.Name} hierarchy Synced!");
            }
        }

        private async Task PrepareDevices(GetTenantDetails tenant, TenantHierarchyEntityDto hierarchy)
        {
            var devices = await GetDevices(tenant.TenantId);

            if (!devices.Any()) return;

            var subAreas = hierarchy
                .Parent
                .Children
                .SelectMany(x => x.Children.SelectMany(x => x.Children.SelectMany(x => x.Children.SelectMany(x => x.Children))));

            foreach (var subArea in subAreas)
            {
                var device = devices.Where(x => x.SubAreaId == subArea.Id).Select(x => new Core.Child
                {
                    Id = x.Id,
                    Name = x.Name,
                    HierarchyLevel = HierarchyLevel.Device,
                });
                subArea.Children.AddRange(device);
            }
        }

        private async Task<List<GetDeviceDetails>> GetDevices(Guid tenantId)
        {
            var tenantDevices = new List<GetDeviceDetails>();

            var request = new Grpc.Requests.Base.DataSourceApiRequest();
            request.Paging.Page = 1;
            request.Paging.Limit = 100;
            request.Sorts.Add(new Sensormatic.Tool.Core.SortDescriptor { Field = "AuditCreateDate", Direction = Sensormatic.Tool.Core.Direction.Asc });

            var hasNextPage = false;

            do
            {

                var deviceResponse = await _deviceService.GetDevicesAsync(tenantId, request);
                tenantDevices.AddRange(deviceResponse.Data);

                request.Paging.Page++;
                hasNextPage = deviceResponse.HasNextPage;

            } while (hasNextPage);

            return tenantDevices;
        }
    }
}
