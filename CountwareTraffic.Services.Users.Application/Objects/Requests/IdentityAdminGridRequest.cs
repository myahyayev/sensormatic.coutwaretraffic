﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityAdminGridRequest
    {
        public PagingQuery Paging { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
        public IdentityAdminGridRequest()
        {
            Paging = new PagingQuery();
            Filters = new List<GridFilter>();
            Sorts = new List<SortDescriptor>();
        }
    }
}
