﻿using System;
using Microsoft.AspNetCore.Authorization;

namespace Countware.Traffic.CrossCC.PolicyProvider
{
    public class RolesRequirement : IAuthorizationRequirement, IIdentifiable
    {
        public string Roles { get; }
        public Guid Identifier { get; set; }
        public RolesRequirement(string roles, Guid identifier)
        {
            Roles = roles;
            Identifier = identifier;
        }
    }
}
