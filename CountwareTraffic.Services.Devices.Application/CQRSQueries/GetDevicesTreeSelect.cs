﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Core;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class GetDevicesTreeSelect: IQuery<PagingResult<DeviceDetailsDto>>
    {
        public PagingQuery PagingQuery { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
        public List<NodeFilter> NodeFilters { get; set; }
    }

    public class NodeFilter
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public HierarchyLevel HierarchyLevel { get; set; }
    }
}
