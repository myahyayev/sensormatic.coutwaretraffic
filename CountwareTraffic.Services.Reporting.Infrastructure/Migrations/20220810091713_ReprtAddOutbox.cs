﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Reporting.Infrastructure.Migrations
{
    public partial class ReprtAddOutbox : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "report.app");

            migrationBuilder.CreateTable(
                name: "OutboxMessages",
                schema: "report.app",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EventRecordId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OccurredOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Data = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProcessedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastException = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsTryFromQueue = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutboxMessages", x => x.Id);
                });

            migrationBuilder.Sql(@"CREATE INDEX [NC_IX_IsTryFromQueue_OccurredOn_ProcessedDate] ON [report.app].[OutboxMessages] ([IsTryFromQueue] ASC,[OccurredOn] ASC, [ProcessedDate] ASC) ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OutboxMessages",
                schema: "report.app");
        }
    }
}
