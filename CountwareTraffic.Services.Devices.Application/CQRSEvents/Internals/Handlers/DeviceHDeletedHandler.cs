﻿using Convey.CQRS.Events;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeviceHDeletedHandler : IEventHandler<DeviceHDeleted>
    {
        private readonly ITenantHierarchyElasticSearchRepository _tenantHierarchyElasticSearchRepository;
        public DeviceHDeletedHandler(ITenantHierarchyElasticSearchRepository tenantHierarchyElasticSearchRepository )
        {
            _tenantHierarchyElasticSearchRepository = tenantHierarchyElasticSearchRepository;
        }

        public async Task HandleAsync(DeviceHDeleted command)
        {
            TenantElasticDto tenantElasticDto = new()
            {
                Id = command.TenantId
            };

            CompanyElasticDto companyElasticDto = new()
            {
                Id = command.ComapnyId
            };

            CountryElasticDto countryElasticDto = new()
            {
                Id = command.CountryId
            };

            RegionElasticDto regionElasticDto = new()
            {
                Id = command.RegionId
            };

            AreaElasticDto areaElasticDto = new()
            {
                Id = command.AreaId
            };

            SubAreaElasticDto subAreaElasticDto = new()
            {
                Id = command.SubAreaId,
            };

            DeviceElasticDto deviceElasticDto = new()
            {
                Id = command.DeviceId,
                Name = command.DeviceName
            };

            await _tenantHierarchyElasticSearchRepository.DeleteDeviceAsync(tenantElasticDto, companyElasticDto, countryElasticDto, regionElasticDto, areaElasticDto, subAreaElasticDto, deviceElasticDto);
        }
    }
}

