﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class GetBrandLookupHandler : IQueryHandler<GetBrandLookup, BrandLookupDetailsDto>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetBrandLookupHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<BrandLookupDetailsDto> HandleAsync(GetBrandLookup query)
        {
            var brandLookup = await _unitOfWork.GetRepository<IBrandLookupRepository>().GetByIdAsync(query.BrandId);

            if (brandLookup == null)
                throw new BrandLookupNotFoundException(query.BrandId);

            return new BrandLookupDetailsDto
            {
                Id = brandLookup.Id,
                Name = brandLookup.Name
            };
        }
    }
}
