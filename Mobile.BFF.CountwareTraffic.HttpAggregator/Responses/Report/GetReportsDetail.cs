﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetReportsDetail
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TemplateName { get; set; }
        public int TemplateTypeId { get; set; }
        public string TemplateTypeName { get; set; }
        public int ReportFormatId { get; set; }
        public string ReportFormatName { get; set; }
        public int ReportStatusId { get; set; }
        public string ReportStatusName { get; set; }
        public bool IsActive { get; set; }
        public List<string> Locations { get; set; }
        public Guid TemplateId { get; set; }
        public List<ReportFile> ReportFiles { get; set; }
        public int FrequencyId { get; set; }
        public string FrequencyName { get; set; }
    }
}
