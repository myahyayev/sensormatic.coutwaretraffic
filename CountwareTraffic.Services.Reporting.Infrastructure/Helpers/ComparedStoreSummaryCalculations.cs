﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Engine;
using CountwareTraffic.Services.Reporting.Engine.TrafficReport;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ComparedStoreSummaryCalculations : IScopedSelfDependency
    {
        private readonly IResourceMongoDbRepository _resourceMongoDbRepository;
        private readonly Color _highIndicatorColor= (Color) new ColorConverter().ConvertFromString("#8BC34A");
        private readonly Color _lowIndicatorColor= (Color) new ColorConverter().ConvertFromString("#FF5252");
        public ComparedStoreSummaryCalculations(IResourceMongoDbRepository resourceMongoDbRepository)
        {
            _resourceMongoDbRepository = resourceMongoDbRepository;
        }
        public async Task<IList<ComparingType>> GenerateComparedStoreSummaries<ComparingType>(IEnumerable<ComparingType> compareCandidates, ComparingType camparingTarget, Guid storeId, string storeName, Guid languageId)
        {
            List<ComparingType> response = new List<ComparingType>();

            if (camparingTarget is not null)
            {
                foreach (ComparingType compareCandidate in compareCandidates)
                {
                    if (camparingTarget is ComparedStoreSummary t)
                    {
                        ComparedStoreSummary comparedStoreSummary = (ComparedStoreSummary)Convert.ChangeType(compareCandidate, typeof(ComparedStoreSummary));
                        ValueObject<double> inBoundValueObject = ValueObject<double>.CreateDefault();
                        ValueObject<double> outBoundValueObject = ValueObject<double>.CreateDefault();

                        inBoundValueObject.IsComparedRow = true;
                        outBoundValueObject.IsComparedRow = true;


                        #region INBOUND

                        if (comparedStoreSummary.Traffic.InBound.Value == 0 && t.Traffic.InBound.Value == 0)
                        {
                            inBoundValueObject.Value = 0;
                        }
                        else if (comparedStoreSummary.Traffic.InBound.Value == 0)
                        {
                            inBoundValueObject.Value = t.Traffic.InBound.Value;
                            inBoundValueObject.Indicator = '▲';
                            inBoundValueObject.ForeColor = _highIndicatorColor;
                        }
                        else if (t.Traffic.InBound.Value == 0)
                        {
                            inBoundValueObject.Value = comparedStoreSummary.Traffic.InBound.Value;
                            inBoundValueObject.Indicator = '▼';
                            inBoundValueObject.ForeColor = _lowIndicatorColor;
                        }
                        else
                        {
                            var inBoundValue = Math.Round((t.Traffic.InBound.Value - comparedStoreSummary.Traffic.InBound.Value) / comparedStoreSummary.Traffic.InBound.Value * 100, 2);

                            inBoundValueObject.Value = inBoundValue;
                            inBoundValueObject.Unit = "%";

                            if (inBoundValue < 0)
                            {
                                inBoundValueObject.Indicator = '▼';
                                inBoundValueObject.ForeColor = _lowIndicatorColor;
                            }

                            else if (inBoundValue > 0)
                            {
                                inBoundValueObject.Indicator = '▲';
                                inBoundValueObject.ForeColor = _highIndicatorColor;
                            }
                        }
                        #endregion INBOUND





                        #region OUTBAND

                        if (comparedStoreSummary.Traffic.OutBound.Value == 0 && t.Traffic.OutBound.Value == 0)
                        {
                            outBoundValueObject.Value = 0;
                        }
                        else if (comparedStoreSummary.Traffic.OutBound.Value == 0)
                        {
                            outBoundValueObject.Value = t.Traffic.OutBound.Value;
                            outBoundValueObject.Indicator = '▲';
                            outBoundValueObject.ForeColor = _highIndicatorColor;
                        }
                        else if (t.Traffic.OutBound.Value == 0)
                        {
                            outBoundValueObject.Value = comparedStoreSummary.Traffic.OutBound.Value;
                            outBoundValueObject.Indicator = '▼';
                            outBoundValueObject.ForeColor = _lowIndicatorColor;
                        }
                        else
                        {
                            var outBoundValue = Math.Round((t.Traffic.OutBound.Value - comparedStoreSummary.Traffic.OutBound.Value) / comparedStoreSummary.Traffic.OutBound.Value * 100, 2);

                            outBoundValueObject.Value = outBoundValue;
                            outBoundValueObject.Unit = "%";

                            if (outBoundValue < 0)
                            {
                                outBoundValueObject.Indicator = '▼';
                                outBoundValueObject.ForeColor = _lowIndicatorColor;
                            }

                            else if (outBoundValue > 0)
                            {
                                outBoundValueObject.Indicator = '▲';
                                outBoundValueObject.ForeColor = _highIndicatorColor;
                            }
                        }

                        #endregion OUTBAND

                        string captionKey = $"{t.CurrentPeriod.CaptionKey}_vs_{comparedStoreSummary.CurrentPeriod.CaptionKey}";

                        ComparedStoreSummary compared = new()
                        {
                            CurrentPeriod = new()
                            {
                                Caption = (await _resourceMongoDbRepository.GetAsync(languageId, DateRangeClassification.FromName(captionKey).ResourceKey)).Content,
                                CaptionKey = captionKey
                            },
                            IsBold = true,
                            StoreId = storeId,
                            StoreName = storeName,
                            Traffic = new()
                            {
                                InBound = inBoundValueObject,
                                OutBound = outBoundValueObject
                                ,
                            },
                            Order = DateRangeClassification.FromName(captionKey).Id,
                        };

                        response.Add((ComparingType)Convert.ChangeType(compared, typeof(ComparingType)));
                    }
                }
            }
            return response;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
