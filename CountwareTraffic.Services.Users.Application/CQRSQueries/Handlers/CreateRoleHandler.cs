﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class CreateRoleHandler : IQueryHandler<CreateRole, IdentityCreateRoleResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public CreateRoleHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityCreateRoleResponse> HandleAsync(CreateRole query)
        {
            return await _identityServerService.CreateRoleAsync(new IdentityCreateRoleRequest
            {
                Name = query.Name,
                Description = query.Description,
                Permissions = query.Permissions
            });
        }
    }
}
