﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class UserChangeStatusApiResponse
    {
        public string Type { get; set; }
        public string Message { get; set; }

        public List<UserChangeStatusApiResponseError> Errors { get; set; }
    }

    public class UserChangeStatusApiResponseError
    {
        public string Message { get; set; }
    }
}
