﻿using CountwareTraffic.Services.Devices.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace countwaretraffic.services.devices.DbMigrator
{
    public class designtimedbcontextfactory : IDesignTimeDbContextFactory<DeviceDbContext>
    {
        public DeviceDbContext CreateDbContext(string[] args)
        {
            var hostconfig = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("aspnetcore_environment")}.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();

            var builder = new DbContextOptionsBuilder<DeviceDbContext>();
            var connectionstring = hostconfig.GetConnectionString("devicedbconnection");
            builder.UseSqlServer(connectionstring, x => x.UseNetTopologySuite().EnableRetryOnFailure());


            return new DeviceDbContext(builder.Options);
        }
    }
}
