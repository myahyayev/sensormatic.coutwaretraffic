﻿using System.Collections.Generic;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer
{
    public class GetAdminUserByIdApiResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public bool Status { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public int Gender { get; set; }
        public DateTime Birthdate { get; set; }
        public int UserType { get; set; }
        public string Address { get; set; }
        public bool AllowSms { get; set; }
        public bool AllowEmail { get; set; }
        public string ProfilePic { get; set; }
        public List<RoleDetailApi> Roles { get; set; }
        public List<GetAdminUserTenant> Tenants { get; set; }
    }

    public class GetAdminUserTenant
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
