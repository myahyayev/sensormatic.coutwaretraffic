﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Events.Application;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class GetTrafficByTimePeriodHandler : IQueryHandler<GetTrafficByTimePeriod, TrafficByTimePeriodDto>
    {
        private readonly IEventElasticSearchRepository _eventElasticSearchRepository;
        private readonly ILogger<GetTrafficByTimePeriodHandler> _logger;
        public GetTrafficByTimePeriodHandler(IEventElasticSearchRepository eventElasticSearchRepository, ILogger<GetTrafficByTimePeriodHandler> logger)
        {
            _eventElasticSearchRepository = eventElasticSearchRepository;
            _logger = logger;
        }

        public async Task<TrafficByTimePeriodDto> HandleAsync(GetTrafficByTimePeriod query)
            => await _eventElasticSearchRepository.GetTrafficByTimePeriodAsync(query.NodeFilters, query.CurrentStartDate, query.CurrentEndDate, query.PreviusStartDate, query.PreviusEndDate, query.TimePeriod);
    }
}
