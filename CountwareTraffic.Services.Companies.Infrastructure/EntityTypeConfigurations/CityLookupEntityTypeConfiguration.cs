﻿using CountwareTraffic.Services.Companies.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class CityLookupEntityTypeConfiguration : IEntityTypeConfiguration<CityLookup>
    {
        public void Configure(EntityTypeBuilder<CityLookup> builder)
        {
            builder.ToTable("CityLookups", SchemaNames.Areas);

            builder
                .Property(x => x.Id)
                .HasField("_id")
                .IsRequired()
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.Name)
               .HasField("_name")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .IsRequired()
               .HasMaxLength(150);

            builder.HasOne<CountryLookup>().WithMany().HasForeignKey(x => x.CountryId);
        }
    }
}
