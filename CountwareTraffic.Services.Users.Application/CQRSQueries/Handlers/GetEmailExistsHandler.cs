﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetEmailExistsHandler : IQueryHandler<GetEmailExists,bool>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetEmailExistsHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<bool> HandleAsync(GetEmailExists query)
        {
            return await _identityServerService.GetEmailExistsAsync(query.Email);
        }
    }
}
