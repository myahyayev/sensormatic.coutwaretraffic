﻿using Convey.CQRS.Commands;
using CountwareTraffic.WorkerServices.SignalrHub.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Consumer
{
    public class SignalRAreaCreatedSuccessfullyConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaCreatedSuccessfully>, ITransientSelfDependency
    {
        private readonly ICommandDispatcher _commandDispatcher;

        public SignalRAreaCreatedSuccessfullyConsumer(ICommandDispatcher commandDispatcher)
            => _commandDispatcher = commandDispatcher;

        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaCreatedSuccessfully queueEvent)
        {
            await _commandDispatcher.SendAsync(new AreaCreatedSuccessfully
            {
                AreaId = queueEvent.AreaId,
                AreaStatus = queueEvent.AreaStatus,
                UserId = queueEvent.UserId,
                UserName = queueEvent.UserName,
                TenantId = queueEvent.TenantId
            });
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
