﻿using Sensormatic.Tool.Ioc;

namespace CountwareTraffic.Services.Settings.Application
{
    public interface ICorrelationService : ITransientDependency
    {
        string CorrelationId { get; }
    }
}
