﻿using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using CountwareTraffic.Services.Events.Infrastructure.Localization;
using Elasticsearch.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nest;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using Sensormatic.Tool.ElasticSearch;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class EventElasticSearchRepository : ElasticRepository<EventCreateElasticData>, IEventElasticSearchRepository
    {
        private readonly ICurrentTenant _currentTenant;
        private readonly ILogger<EventCreateElasticData> _logger;
        public EventElasticSearchRepository(IConfiguration configuration, ILogger<EventCreateElasticData> logger, ICurrentTenant currentTenant)
            : base(configuration, logger, ElasticsearchKeys.CountwareTrafficEventsEvent)
        {
            _logger = logger;
            _currentTenant = currentTenant;
        }

        private static EventCreateElasticData MapToEntity(EventCreateElasticDto dto)
        {
            return new EventCreateElasticData(dto.TenantId, dto.EventId)
            {
                DeviceId = dto.DeviceId,
                DeviceName = dto.DeviceName,
                EventDate = dto.EventDate,
                EventStartDate = dto.EventStartDate,
                EventEndtDate = dto.EventEndtDate,
                AreaId = dto.AreaId,
                SubAreaName = dto.SubAreaName,
                SubAreaId = dto.SubAreaId,
                RegionName = dto.RegionName,
                AreaName = dto.AreaName,
                CityId = dto.CityId,
                CityName = dto.CityName,
                CompanyId = dto.CompanyId,
                CompanyName = dto.CompanyName,
                CountryId = dto.CountryId,
                CountryName = dto.CountryName,
                DirectionType = dto.DirectionType,
                DistrictId = dto.DistrictId,
                DistrictName = dto.DistrictName,
                OnTimeIOCount = dto.OnTimeIOCount,
                RegionId = dto.RegionId
            };
        }

        private async Task MapAsync()
        {
            if ((await _client.Indices.ExistsAsync(_index)).Exists)
                return;

            var createIndexResponse = await _client.Indices.CreateAsync(_index, c => c
                .Map<EventCreateElasticData>(m => m
                    .AutoMap()
                    ));

            if (!createIndexResponse.IsValid)
                throw createIndexResponse.OriginalException;
        }

        public async Task<bool> IsIndicesExistsAsync()
        {
            return (await _client.Indices.ExistsAsync(_index)).Exists;
        }
        public async Task AddAsync(EventCreateElasticDto data)
        {
            await MapAsync();
            _logger.LogInformation($"EventId:{data.EventId} sent to EventElasticSearchRepository.AddAsync()");
            await base.AddAsync(MapToEntity(data));
        }
        public async Task AddRangeAsync(List<EventCreateElasticDto> data)
        {
            await MapAsync();
            _logger.LogInformation($"Event count:{data.Count} sent to EventElasticSearchRepository.AddRangeAsync()");
            await base.AddRangeAsync(data.Select(x => MapToEntity(x)).ToList());
        }


        public async Task<WidgetPerformingDto> GetWidgetPerformingAsync(List<NodeFilter> nodeFilters, DateTime currentStartDate, DateTime currentEndDate, DateTime previusStartDate, DateTime previusEndDate)
        {
            await this.CheckElasticIndexAsync();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
                .Must(must)
                .MustNot(mustNot)
                .Filter(filter));

            must.Add(m => m
              .Term(t => t.Field(f => f.TenantId).Value(_currentTenant.Id)));

            mustNot.Add(m => m
                .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Outward.Id)));

            this.GenerateNodeFilterQuery(nodeFilters, ref must);


            #region CURRENT  
            filter.Add(f =>
                    f.DateRange(d => d.Field(f => f.EventDate)
                        .GreaterThanOrEquals(currentStartDate.ToUniversalTime())
                        .LessThanOrEquals(currentEndDate.ToUniversalTime())));


            searchDescriptor
               .Size(0)
               .Index(_index)
                   .Query(u => u.Bool(boolContainerGlobal))
                   .Aggregations(aggregation => AggregationContainerDescriptor());

            var resultCurrent = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);
            this.ValidateElasticResult(resultCurrent);
            var inwardsCount = ((Nest.ValueAggregate)resultCurrent.Aggregations.First(u => u.Key == "on_time_inwards_count").Value).Value;
            #endregion CURRENT END


            #region PREVIUS
            searchDescriptor = new();
            filter = new();

            filter.Add(f =>
                f.DateRange(d => d.Field(f => f.EventDate)
                    .GreaterThanOrEquals(previusStartDate)
                    .LessThanOrEquals(previusEndDate)));

            searchDescriptor
               .Size(0)
               .Index(_index)
                   .Query(u => u.Bool(boolContainerGlobal))
                   .Aggregations(aggregation => AggregationContainerDescriptor());

            var resultPrevius = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);
            this.ValidateElasticResult(resultPrevius);
            var inwardsCountLatest = ((Nest.ValueAggregate)resultPrevius.Aggregations.First(u => u.Key == "on_time_inwards_count").Value).Value;
            #endregion PREVIUS END

            return new WidgetPerformingDto
            {
                InwardsCurrent = inwardsCount.HasValue ? (int)inwardsCount : 0,
                InwardsPrevius = inwardsCountLatest.HasValue ? (int)inwardsCountLatest : 0,
            };
        }

        public async Task<List<PerformingByTypeDto>> GetPerformingByTypeAsync(List<NodeFilter> nodeFilters, DateTime startDate, DateTime endDate, Direction direction, PerformType performType, int size = 10)
        {
            await this.CheckElasticIndexAsync();

            var response = new List<PerformingByTypeDto>();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
               .Must(must)
               .MustNot(mustNot)
               .Filter(filter));

            must.Add(m => m
                .Term(t => t.Field(f => f.TenantId).Value(_currentTenant.Id)));

            mustNot.Add(m => m
                .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Outward.Id)));


            this.GenerateNodeFilterQuery(nodeFilters, ref must);


            filter.Add(f =>
                f.DateRange(d => d.Field(f => f.EventDate)
                    .GreaterThanOrEquals(startDate.ToUniversalTime())
                    .LessThanOrEquals(endDate.ToUniversalTime())));

            TermsOrderDescriptor<EventCreateElasticData> termsOrderDescriptor = new();
            switch (direction)
            {
                case Direction.Asc:
                    termsOrderDescriptor.Ascending("sum_traffic");
                    break;
                case Direction.Desc:
                    termsOrderDescriptor.Descending("sum_traffic");
                    break;
                default:
                    break;
            }

            Tuple<Field, Field> fieldGroupBy = performType switch
            {
                PerformType.Company => new Tuple<Field, Field>(new Field("companyId"), new Field("companyName")),
                PerformType.Country => new Tuple<Field, Field>(new Field("countryId"), new Field("countryName")),
                PerformType.Region => new Tuple<Field, Field>(new Field("regionId"), new Field("regionName")),
                PerformType.Area => new Tuple<Field, Field>(new Field("areaId"), new Field("areaName")),
                PerformType.SubArea => new Tuple<Field, Field>(new Field("subAreaId"), new Field("subAreaName")),
                PerformType.Device => new Tuple<Field, Field>(new Field("deviceId"), new Field("deviceName")),
                _ => new Tuple<Field, Field>(new Field("deviceId"), new Field("deviceName"))
            };

            searchDescriptor
                .Index(_index)
                .Size(0)
                .Query(u => u.Bool(boolContainerGlobal))
                .Aggregations(a => a
                    .Terms("group_by", t => t.Field(fieldGroupBy.Item1)
                        .Size(size)
                        .Order(o => termsOrderDescriptor)
                        .Aggregations(aa => aa
                            .Sum("sum_traffic", t => t.Field(f => f.OnTimeIOCount))
                            .TopHits("top_hit", t => t
                                .Size(1)
                                .Source(s => s
                                    .Includes(i => i.Field(fieldGroupBy.Item2)))))));

#if DEBUG
            var jsonLatest = this.SearchDescriptorToJson(searchDescriptor);
#endif
            var result = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(result);

            var items = ((Nest.BucketAggregate)result.Aggregations.First(u => u.Key == "group_by").Value).Items;

            foreach (Nest.KeyedBucket<object> keyedBucket in items)
            {
                response.Add(new PerformingByTypeDto
                {
                    Id = new Guid(keyedBucket.Key.ToString()),
                    Inwards = (int)keyedBucket.Sum("sum_traffic").Value,
                    Name = performType switch
                    {
                        PerformType.Company => keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().CompanyName,
                        PerformType.Country => keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().CountryName,
                        PerformType.Region => keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().RegionName,
                        PerformType.Area => keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().AreaName,
                        PerformType.SubArea => keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().SubAreaName,
                        PerformType.Device => keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().DeviceName,
                        _ => keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().DeviceName,
                    }
                });
            }
            return response;
        }

        public async Task<TrafficByTimePeriodDto> GetTrafficByTimePeriodAsync(List<NodeFilter> nodeFilters, DateTime currentStartDate, DateTime currentEndDate, DateTime previusStartDate, DateTime previusEndDate, TimePeriod timePeriod)
        {
            await this.CheckElasticIndexAsync();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var response = new TrafficByTimePeriodDto();
            response.Categories = this.InitilalizeCategories(timePeriod);

            string calendar_interval = timePeriod switch
            {
                TimePeriod.Hourly => "1H",
                TimePeriod.Daily => "1D",
                TimePeriod.Monthly => "1M",
                _ => throw new NotImplementedException(),
            };

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
               .Must(must)
               .MustNot(mustNot)
               .Filter(filter));

            must.Add(m => m
              .Term(t => t.Field(f => f.TenantId).Value(_currentTenant.Id)));

            mustNot.Add(m => m
                .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Outward.Id)));

            this.GenerateNodeFilterQuery(nodeFilters, ref must);


            #region CURRENT
            filter.Add(f =>
               f.DateRange(d => d.Field(f => f.EventDate)
                   .GreaterThanOrEquals(currentStartDate.ToUniversalTime())
                   .LessThanOrEquals(currentEndDate.ToUniversalTime())));

            searchDescriptor
               .Size(0)
               .Index(_index)
                   .Query(u => u.Bool(boolContainerGlobal))
                   .Aggregations(a => a
                        .DateHistogram("per_timeperiod", u => u.Field(f => f.EventDate)
                            .CalendarInterval(new DateMathTime(calendar_interval))
                            .Aggregations(aa => aa
                                .Sum("sum_traffic", t => t.Field(f => f.OnTimeIOCount)))));

            var resultCurrent = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(resultCurrent);

            var currentItems = ((Nest.BucketAggregate)resultCurrent.Aggregations.First(u => u.Key == "per_timeperiod").Value).Items;

            List<ElasticTrafficByTimePeriodDto> elasticDtoCurrent = new();

            foreach (DateHistogramBucket item in currentItems)
            {
                string category = timePeriod switch
                {
                    TimePeriod.Hourly => item.Date.ToString("hh tt"),
                    TimePeriod.Daily => item.Date.DayOfWeek.ToString(),
                    TimePeriod.Monthly => item.Date.ToString("MMMM", CultureInfo.InvariantCulture),
                    _ => throw new NotImplementedException(),
                };

                int value = (int)((Nest.ValueAggregate)item.Values.First()).Value;

                elasticDtoCurrent.Add(new ElasticTrafficByTimePeriodDto
                {
                    Category = category,
                    Value = value
                });
            }
            #endregion Current END


            #region PREVIUS
            searchDescriptor = new();
            filter = new();
            filter.Add(f =>
               f.DateRange(d => d.Field(f => f.EventDate)
                   .GreaterThanOrEquals(previusStartDate.ToUniversalTime())
                   .LessThanOrEquals(previusEndDate.ToUniversalTime())));

            searchDescriptor
               .Size(0)
               .Index(_index)
                   .Query(u => u.Bool(boolContainerGlobal))
                   .Aggregations(a => a
                        .DateHistogram("per_timeperiod", u => u.Field(f => f.EventDate)
                            .CalendarInterval(new DateMathTime(calendar_interval))
                            .Aggregations(aa => aa
                                .Sum("sum_traffic", t => t.Field(f => f.OnTimeIOCount)))));

            var resultPrevius = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(resultPrevius);

            var previusItems = ((Nest.BucketAggregate)resultPrevius.Aggregations.First(u => u.Key == "per_timeperiod").Value).Items;


            List<ElasticTrafficByTimePeriodDto> elasticDtoPrevius = new();

            foreach (DateHistogramBucket item in previusItems)
            {
                //Devreye gir kullaniciin timezonuna gore  item.Date
                string category = timePeriod switch
                {
                    TimePeriod.Hourly => item.Date.ToString("hh tt"),
                    TimePeriod.Daily => item.Date.DayOfWeek.ToString(),
                    TimePeriod.Monthly => item.Date.ToString("MMMM", CultureInfo.InvariantCulture),
                    _ => throw new NotImplementedException(),
                };

                int value = (int)((Nest.ValueAggregate)item.Values.First()).Value;

                elasticDtoPrevius.Add(new ElasticTrafficByTimePeriodDto
                {
                    Category = category,
                    Value = value
                });
            }
            #endregion PREVIUS END

            foreach (var category in response.Categories)
            {
                var current = elasticDtoCurrent.Where(u => u.Category == category).FirstOrDefault();
                if (current != null)
                    response.CurrentSeries.Data.Add(current.Value);
                else
                    response.CurrentSeries.Data.Add(0);

                var previus = elasticDtoPrevius.Where(u => u.Category == category).FirstOrDefault();
                if (previus != null)
                    response.PreviusSeries.Data.Add(previus.Value);
                else
                    response.PreviusSeries.Data.Add(0);
            }


            return response;
        }

        public async Task<CountOfVisitorsInsideDto> GetCountOfVisitorsInside(List<NodeFilter> nodeFilters, DateTime startDate)
        {
            await this.CheckElasticIndexAsync();

            CountOfVisitorsInsideDto response = new();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
               .Must(must)
               .MustNot(mustNot)
               .Filter(filter));

            must.Add(m => m
                .Term(t => t.Field(f => f.TenantId).Value(_currentTenant.Id)));
             mustNot.Add(m => m
                .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Unknown.Id)));

            filter.Add(f =>
               f.DateRange(d => d.Field(f => f.EventDate)
                   .GreaterThanOrEquals(startDate.ToUniversalTime())
                   /*.LessThanOrEquals(endDate.ToUniversalTime())*/));

            this.GenerateNodeFilterQuery(nodeFilters, ref must);

            searchDescriptor
              .Index(_index)
              .Size(0)
              .Query(u => u.Bool(boolContainerGlobal))
              .Aggregations(a => a
                  .Terms("group_by", t => t.Field(f => f.DirectionType.Id)
                         .Aggregations(agg1 => agg1
                                   .Sum("sum_traffic", t => t.Field(f => f.OnTimeIOCount)))));


#if DEBUG
            var jsonLatest = this.SearchDescriptorToJson(searchDescriptor);
#endif
            var result = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(result);

            var groupedItems = ((Nest.BucketAggregate)result.Aggregations.First(u => u.Key == "group_by").Value).Items;

            foreach (Nest.KeyedBucket<object> keyedBucket in groupedItems)
            {
                var directionType = DirectionType.FromValue<DirectionType>(Convert.ToInt32(keyedBucket.Key));
                if (directionType == DirectionType.Inwards)
                {
                    response.CountOfVisitorsInside += (int)keyedBucket.Sum("sum_traffic").Value; 
                }
                else if (directionType == DirectionType.Outward)
                {
                    response.CountOfVisitorsInside -= (int)keyedBucket.Sum("sum_traffic").Value;
                }
            }

            return response;
        }


        public async Task<VisitorDurationDto> GetVisitorDurationAsync(List<NodeFilter> nodeFilters, DateTime currentStartDate)
        {
            await this.CheckElasticIndexAsync();

            VisitorDurationDto response = new();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var mustNot = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
               .Must(must)
               .MustNot(mustNot)
               .Filter(filter));

            must.Add(m => m
                .Term(t => t.Field(f => f.TenantId).Value(_currentTenant.Id)));

            mustNot.Add(m => m
             .Term(t => t.Field(f => f.DirectionType.Id).Value(DirectionType.Unknown.Id)));

            this.GenerateNodeFilterQuery(nodeFilters, ref must);


            searchDescriptor
               .Size(0)
               .Index(_index)
                   .Query(u => u.Bool(boolContainerGlobal))
                   .Aggregations(agg => agg
                      .MultiTerms("group_by", mt => mt
                        .CollectMode(TermsAggregationCollectMode.BreadthFirst)
                        .Terms(t => t.Field(f => f.AreaId), t => t.Field(f => f.DirectionType.Id))
                        .Aggregations(aa => aa
                          .TopHits("top_hit", t => t
                               .Size(1)
                               .Source(s => s
                                   .Includes(i => i.Field(f => f.EventDate)))
                               .Sort(so => so
                                   .Descending(d => d.EventDate))))));

#if DEBUG
            var json = this.SearchDescriptorToJson(searchDescriptor);
#endif
            var result = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(result);

            Dictionary<Guid, DateTime> lastInwardsDatesStoreByStore = new();
            Dictionary<Guid, DateTime> lastOutwardsDatesStoreByStore = new();
            List<TimeSpan> durationsStoreByStore = new();

            var groupedItems = ((Nest.BucketAggregate)result.Aggregations.First(u => u.Key == "group_by").Value).Items;

            foreach (Nest.KeyedBucket<object> keyedBucket in groupedItems)
            {
                var directionType = DirectionType.FromValue<DirectionType>(Convert.ToInt32(keyedBucket.KeyAsString.Split("|")[1]));
                var storeId = new Guid(keyedBucket.KeyAsString.Split("|")[0]);
                var eventDate = keyedBucket.TopHits("top_hit").Documents<EventCreateElasticData>().FirstOrDefault().EventDate;

                if (directionType == DirectionType.Inwards)
                {
                    lastInwardsDatesStoreByStore.Add(storeId, eventDate);
                }
                if (directionType == DirectionType.Outward)
                {
                    lastOutwardsDatesStoreByStore.Add(storeId, eventDate);
                }
            }

            //suanda sadece bir magaza icin bakilmisdir. Duruma gore tum magazalar icin yap her birinin timespanini bul timespanlarin ortalamasina bak.

            if (lastInwardsDatesStoreByStore.Count > 0)
            {
                var firsStoreInwards = lastInwardsDatesStoreByStore.FirstOrDefault();

                var firsStoreOutwards = lastOutwardsDatesStoreByStore.Where(u => u.Key == firsStoreInwards.Key).FirstOrDefault();

                response.LastInwardsDate = firsStoreInwards.Value;
                response.LastOutwardsDate = firsStoreOutwards.Value;
            }

            return response;
        }

        public async Task<QueryablePagingValue<EventDetailsDto>> GetEventsAsync(Guid deviceId, int page, int size, Guid tenantId)
        {
            if (!(await _client.Indices.ExistsAsync(_index)).Exists)
                throw new ElasticIndexNotFoundException(_index);

            var descriptor = new QueryContainerDescriptor<EventCreateElasticData>();

            if (deviceId != Guid.Empty)
                descriptor.Bool(b => b.Must(m => m.Term(t => t.Field(f => f.DeviceId).Value(deviceId))));

            descriptor.Bool(b => b.Filter(m => m.Term(t => t.Field(f => f.TenantId).Value(tenantId))));

            var totalCount = await _client.CountAsync<EventCreateElasticData>(search => search
                                                                                              .Index(_index)
                                                                                              .Query(d => descriptor));

            if (!totalCount.IsValid)
                throw new ElasticSearchQueryException(_index, totalCount.ServerError.Status, totalCount.ServerError.Error.Type, totalCount.OriginalException);

            if (totalCount.Count < 1)
                return null;

            var total = (int)totalCount.Count;

            var result = await _client.SearchAsync<EventCreateElasticData>(
               search => search
               .Index(_index)
               .Size(size)
               .From((page - 1) * size)
               .Source(sf => sf
                    .Includes(i => i
                        .Fields(f => f.DeviceId
                              )
                           )
                      )
               .Query(d => descriptor));

            if (!result.IsValid)
                throw new ElasticSearchQueryException(_index, result.ServerError.Status, result.ServerError.Error.Type, result.OriginalException);


            var events = result.Documents.Select(x => new EventDetailsDto
            {
                DeviceName = x.DeviceName,
                DeviceId = x.DeviceId,
                EventDate = x.EventDate,
                Id = x.Id
            }).ToList();

            return new QueryablePagingValue<EventDetailsDto>(events, total);
        }


        public async Task<int> GetEventCountAsync(DateTime startDate)
        {
            await this.CheckElasticIndexAsync();

            var descriptor = new QueryContainerDescriptor<EventCreateElasticData>();

            descriptor.Bool(b => b.Filter(m => m.DateRange(t => t.Field(f => f.EventDate).GreaterThanOrEquals(startDate.ToUniversalTime()))));

            var countResponse = await _client.CountAsync<EventCreateElasticData>(search => search
                                                                                              .Index(_index)
                                                                                                .Query(d => descriptor));


            if (!countResponse.IsValid)
                throw new ElasticSearchQueryException(_index, countResponse.ServerError.Status, countResponse.ServerError.Error.Type, countResponse.OriginalException);


            return (int)countResponse.Count;    
        }

        public async Task<List<EventsByTimePeriodDto>> GetEventsByTimePeriodAsync(DateTime StartDate, DateTime EndDate)
        {
            await this.CheckElasticIndexAsync();

            SearchDescriptor<EventCreateElasticData> searchDescriptor = new();

            var must = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();
            var filter = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerGlobal = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
               .Must(must)
               .Filter(filter));

            must.Add(m => m
              .Term(t => t.Field(f => f.TenantId).Value(_currentTenant.Id)));

            #region CURRENT
            filter.Add(f =>
               f.DateRange(d => d.Field(f => f.EventDate)
                   .GreaterThanOrEquals(StartDate.ToUniversalTime())
                   .LessThanOrEquals(EndDate.ToUniversalTime())));

            searchDescriptor
                .Size(10000)
                .Index(_index)
                .Query(q => q.Bool(boolContainerGlobal));


            var resultCurrent = await _client.SearchAsync<EventCreateElasticData>(search => searchDescriptor);

            this.ValidateElasticResult(resultCurrent);

            var currentItems = resultCurrent.Documents.ToList();

            List<EventsByTimePeriodDto> elasticDtoCurrent = new();

            #endregion Current END

            foreach (var item in currentItems)
            {
                var data = new EventsByTimePeriodDto
                {
                    AreaId = item.AreaId,
                    AreaName = item.AreaName,
                    SubAreaId = item.SubAreaId,
                    SubAreaName = item.SubAreaName,
                    Date = item.EventDate
                };
                if(item.DirectionType.Equals(DirectionType.Inwards))
                    data.EnterCount = item.OnTimeIOCount;
                else if(item.DirectionType.Equals(DirectionType.Outward))
                    data.ExitCount = item.OnTimeIOCount;
                elasticDtoCurrent.Add(data);
            }

            return elasticDtoCurrent;
        }

        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {

                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion


        #region private methods
        private async Task CheckElasticIndexAsync()
        {
            if (!(await _client.Indices.ExistsAsync(_index)).Exists)
                throw new ElasticIndexNotFoundException(_index);
        }

        private string SearchDescriptorToJson(SearchDescriptor<EventCreateElasticData> searchDescriptor)
        {
            string jsonQuery = _client.RequestResponseSerializer.SerializeToString(searchDescriptor);

            _logger.LogInformation($"Elastic Query: {jsonQuery}");
            return jsonQuery;
        }

        private void ValidateElasticResult(ISearchResponse<EventCreateElasticData> response)
        {
            if (!response.IsValid)
                throw new ElasticSearchQueryException(_index, response.ServerError.Status, response.ServerError.Error.Type, response.OriginalException);
        }

        private AggregationContainerDescriptor<EventCreateElasticData> AggregationContainerDescriptor()
        {
            AggregationContainerDescriptor<EventCreateElasticData> aggDescriptor = new();
            aggDescriptor.Sum("on_time_inwards_count", sumDescriptor => sumDescriptor.Field(f => f.OnTimeIOCount));

            return aggDescriptor;
        }

        private List<string> InitilalizeCategories(TimePeriod timePeriod)
        {
            var categories = timePeriod switch
            {
                TimePeriod.Hourly => new List<string>
                {
                    "12 AM",
                    "01 AM",
                    "02 AM",
                    "03 AM",
                    "04 AM",
                    "05 AM",
                    "06 AM",
                    "07 AM",
                    "08 AM",
                    "09 AM",
                    "10 AM",
                    "11 AM",
                    "12 PM",
                    "01 PM",
                    "02 PM",
                    "03 PM",
                    "04 PM",
                    "05 PM",
                    "06 PM",
                    "07 PM",
                    "08 PM",
                    "09 PM",
                    "10 PM",
                    "11 PM",
                },
                TimePeriod.Daily => new List<string>
                {
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday",
                    "Sunday"
                },
                TimePeriod.Monthly => new List<string>
                {
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                },
                _ => throw new NotImplementedException(),
            };

            return categories;
        }

        private void GenerateNodeFilterQuery(List<NodeFilter> nodeFilters, ref List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>> must)
        {
            var should = new List<Func<QueryContainerDescriptor<EventCreateElasticData>, QueryContainer>>();

            var boolContainerInner = new Func<BoolQueryDescriptor<EventCreateElasticData>, IBoolQuery>(b => b
                .Should(should));

            foreach (var node in nodeFilters)
            {
                switch (node.HierarchyLevel)
                {
                    case (HierarchyLevel.Tenant):
                        break;

                    case (HierarchyLevel.Company):
                        should.Add(s => s.Term(new Field("companyId"), node.Id));
                        break;

                    case (HierarchyLevel.Country):
                        should.Add(s => s.Term(new Field("countryId"), node.Id));
                        break;

                    case (HierarchyLevel.Region):
                        should.Add(s => s.Term(new Field("regionId"), node.Id));
                        break;

                    case (HierarchyLevel.Area):
                        should.Add(s => s.Term(new Field("areaId"), node.Id));
                        break;

                    case (HierarchyLevel.SubArea):
                        should.Add(s => s.Term(new Field("subAreaId"), node.Id));
                        break;

                    case (HierarchyLevel.Device):
                        should.Add(s => s.Term(new Field("deviceId"), node.Id));
                        break;
                }
            }

            must.Add(m => m.Bool(boolContainerInner));
        }

        private DateTime AvgOfDates(DateTime[] dates)
        {
            var count = dates.Length;

            double temp = 0D;

            for (int i = 0; i < count; i++)
            {
                temp += dates[i].Ticks / (double)count;
            }

            return new DateTime((long)temp);
        }


        #endregion private methods
    }
}
