﻿using CountwareTraffic.Services.Companies.Core;
using System;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class CountryLookupRepository : Repository<CountryLookup>, ICountryLookupRepository
    {
        private readonly new AreaDbContext _context;

        public CountryLookupRepository(AreaDbContext context) : base(context) => _context = context;



        #region disposible
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion disposible
    }
}
