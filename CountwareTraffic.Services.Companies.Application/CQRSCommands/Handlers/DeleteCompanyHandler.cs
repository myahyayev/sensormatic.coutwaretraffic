﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class DeleteCompanyHandler : ICommandHandler<DeleteCompany>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;

        public DeleteCompanyHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(DeleteCompany command)
        {
            var companyRepository = _unitOfWork.GetRepository<ICompanyRepository>();

            var company = await companyRepository.GetAsync(command.CompanyId);

            if (company is null) throw new CompanyNotFoundException(command.CompanyId);

            companyRepository.Remove(company);

            company.WhenDeleted(_currentTenant.Id, _currentTenant.Name, company.Id, company.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
