﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Core;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Application.CQRSEvents.External.Handlers
{
    public class AreaChangedHandler : IEventHandler<AreaChanged>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        public AreaChangedHandler(IUnitOfWork unitOfWork, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
        }

        public async Task HandleAsync(AreaChanged @event)
        {
            var area = await _unitOfWork.GetRepository<IAreaRepository>()
                  .GetAsync(@event.AreaId);

            if (area != null)
            {
                try
                {
                    area.Change(@event.AreaName, @event.WorkingHoursStart, @event.WorkingHoursEnd, @event.WorkingTimeZone);

                    await _unitOfWork.CommitAsync();

                    _queueService.Publish(new AreaChangedSuccessfully
                    {
                        RecordId = Guid.NewGuid(),
                        NewName = @event.AreaName,
                        OldName = @event.OldAreaName,
                        AreaId = @event.AreaId,
                        UserId = @event.UserId,
                        UserName = String.Empty,
                        TenantId = @event.TenantId
                    });

                }
                catch (Exception)
                {
                    _queueService.Publish(new AreaChangedRejected(@event.AreaId, @event.AreaName, @event.OldAreaName, @event.UserId, "", @event.TenantId));
                }
            }
        }
    }
}

