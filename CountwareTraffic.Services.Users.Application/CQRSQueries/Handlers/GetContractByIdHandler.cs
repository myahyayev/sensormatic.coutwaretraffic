﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class GetContractByIdHandler : IQueryHandler<GetContractById, IdentityGetContractByIdResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetContractByIdHandler(IIdentityServerService identityServerService)
        {
            _identityServerService = identityServerService;
        }

        public async Task<IdentityGetContractByIdResponse> HandleAsync(GetContractById query)
        {
            return await _identityServerService.GetContractByIdAsync(new Objects.Requests.IdentityGetContractByIdRequest
            {
                Id = query.Id
            });
        }
    }
}
