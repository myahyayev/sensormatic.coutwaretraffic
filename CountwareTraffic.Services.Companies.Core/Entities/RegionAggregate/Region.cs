﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class Region : AggregateRoot, IAuditable, IDateable, IDeletable, ITraceable, IMultiTenant
    {
        private string _name;
        private string _description;
        private Guid? _managerId;
        private string _managerName;
        private Guid _countryId;

        public string Name => _name;
        public string Description => _description;
        public Guid? ManagerId => _managerId;
        public string ManagerName => _managerName;
        public Guid CountryId => _countryId;


        #region default properties
        public DateTime AuditCreateDate { get; set; }
        public DateTime AuditModifiedDate { get; set; }
        public bool AuditIsDeleted { get; set; }
        public Guid AuditCreateBy { get; set; }
        public Guid AuditModifiedBy { get; set; }
        public Guid TenantId { get; set; }
        #endregion default properties


        private Region() { }

        public static Region Create(string name, string description, Guid countryId, Guid? managerId, string managerName)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1439 " + nameof(name));

            if (name.Length > 150)
                throw new ArgumentException("#E1440 " + nameof(name));

            if (!description.IsNullOrWhiteSpace() && description.Length > 2500)
                throw new ArgumentException("#E1441 " + nameof(description));

            if (!managerName.IsNullOrWhiteSpace() && managerName.Length > 100)
                throw new ArgumentException("#E1442 " + nameof(managerName));

            return new Region
            {
                _name = name,
                _countryId = countryId,
                _description = description,
                _managerId = managerId,
                _managerName = managerName
            };
        }

        public void CompleteChange(string name, string description, Guid? managerId, string managerName)
        {
            if (name.IsNullOrWhiteSpace())
                throw new ArgumentNullException("#E1443 " + nameof(name));

            if (name.Length > 150)
                throw new ArgumentException("#E1444 " + nameof(name));

            if (!description.IsNullOrWhiteSpace() && description.Length > 2500)
                throw new ArgumentException("#E1445 " + nameof(description));

            if (!managerName.IsNullOrWhiteSpace() && managerName.Length > 100)
                throw new ArgumentException("#E1446 " + nameof(managerName));

            _name = name;
            _description = description;
            _managerId = managerId;
            _managerName = managerName;
        }

        public void WhenCreated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, string regionName)
           => AddEvent(new RegionHierarchyCreated(tenantId, companyId, countryId, regionId,  regionName));

        public void WhenChanged(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, string regionName)
            => AddEvent(new RegionHierarchyUpdated(tenantId, companyId, countryId, regionId, regionName));

        public void WhenDeleted(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, string regionName)
            => AddEvent(new RegionHierarchyDeleted(tenantId, companyId, countryId, regionId, regionName));
    }
}
