﻿using System;
using Microsoft.AspNetCore.Authorization;

namespace Countware.Traffic.CrossCC.PolicyProvider
{
    public class ScopesRequirement : IAuthorizationRequirement, IIdentifiable
    {
        public string Scopes { get; }
        public Guid Identifier { get; set; }
        public ScopesRequirement(string scopes, Guid identifier)
        {
            Scopes = scopes;
            Identifier = identifier;
        }
    }
}
