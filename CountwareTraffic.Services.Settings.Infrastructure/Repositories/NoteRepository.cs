﻿using CountwareTraffic.Services.Settings.Core;
using System;

namespace CountwareTraffic.Services.Settings.Infrastructure
{
    public class NoteRepository : Repository<Note>, INoteRepository
    {
        private bool _disposed;

        private readonly new SettingsDbContext _context;

        public NoteRepository(SettingsDbContext context) : base(context)
            => _context = context;

        #region dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion dispose

    }
}
