﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityDeleteTenantGroupResponse
    {
        public int Status { get; set; }
        public string Message { get; set; }

        public List<IdentityDeleteTenantGroupErrorResult> ErrorResults { get; set; }
    }

    public class IdentityDeleteTenantGroupErrorResult
    {
        public string Message { get; set; }
    }
}
