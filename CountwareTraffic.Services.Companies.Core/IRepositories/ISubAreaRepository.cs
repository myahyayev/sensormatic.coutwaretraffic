﻿using Sensormatic.Tool.Core;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Core
{
    public interface ISubAreaRepository : IRepository<SubArea>, IScopedDependency
    {
        Task<SubArea> GetAsync(Guid id, Guid tenantId);
        Task<bool> ExistsAsync(string name, Guid tenantId);
        Task<QueryablePagingValue<SubArea>> GetAllAsync(Guid areaId, Guid tenantId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts);
        Task<SubArea> GetDeletedAsync(Guid id, Guid tenantId);
        Task<List<Hierarchy>> GetHierarchyAsync(Guid id, Guid tenantId, string tenantName);
        Task<List<KeyValue>> GetAllKeyValueAsync(Guid parentId);
    }
}
