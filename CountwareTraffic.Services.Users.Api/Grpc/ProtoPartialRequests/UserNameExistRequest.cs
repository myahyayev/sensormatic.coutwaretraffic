﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;

namespace CountwareTraffic.Services.Users.Grpc
{
    [ServiceLog]
    public sealed partial class UserNameExistRequest : SensormaticRequestValidate
    {
        public override void Validate()
        {
            if (UserName.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"Value cannot be null", nameof(UserName)));
            if (!UserName.IsValidUserName())
                ValidateResults.Add(new ErrorResult($"Value is invalid", nameof(UserName)));
            if (UserName.Length < 3 || UserName.Length > 50)
                ValidateResults.Add(new ErrorResult($"UserName length must be 3-50 character", nameof(UserName)));
        }
    }
}

