﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class RemoveRoleFromUserHandler : IQueryHandler<RemoveRoleFromUser, IdentityRemoveRoleFromUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public RemoveRoleFromUserHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityRemoveRoleFromUserResponse> HandleAsync(RemoveRoleFromUser query)
        {
            return await _identityServerService.RemoveRoleFromUserAsync(query.UserId, query.RoleName);
        }
    }
}
