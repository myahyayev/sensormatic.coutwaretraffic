﻿using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportFileRepository : Repository<ReportFile>, IReportFileRepository
    {
        private readonly new ReportDbContext _context;
        private readonly ICurrentTenant _currentTenant;
        public ReportFileRepository(ReportDbContext context, ICurrentTenant currentTenant) : base(context)
        {
            _context = context;
            _currentTenant = currentTenant;
        }

        public async Task<QueryablePagingValue<ReportFile>> GetByReportId(Guid reportId, PagingQuery paging, List<GridFilter> filters, List<SortDescriptor> sorts)
        {
            var query = base.GetQuery(_currentTenant.Id);

            query = query.ApplyFilter(filters);

            query = query.ApplyOrderBy(sorts);

            var total = await query.CountAsync(u => u.ReportId == reportId);

            if (total > 0)
            {
                query = query.Where(u => u.ReportId == reportId)
                                  .Skip((paging.Page - 1) * paging.Limit)
                                  .Take(paging.Limit);

                var result = await query.ToListAsync();

                return new QueryablePagingValue<ReportFile>(result, total);
            }

            return null;
        }

        public async Task<ReportFile> GetByIdWithReportIdAsync(Guid id, Guid reportId)
            => await base.GetQuery().SingleOrDefaultAsync(x => x.Id == id && x.ReportId == reportId);


        public async Task<ReportFile> GetFirstByReportId(Guid reportId)
            => await base.GetQuery().FirstOrDefaultAsync(x => x.ReportId == reportId);

        #region dispose
        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion dispose
    }
}
