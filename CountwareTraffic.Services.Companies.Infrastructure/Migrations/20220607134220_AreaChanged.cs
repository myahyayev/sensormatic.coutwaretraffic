﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class AreaChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Contact_FaxNumber",
                schema: "areas",
                table: "Areas");

            migrationBuilder.AlterColumn<string>(
                name: "Contact_PhoneNumber",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(16)",
                oldMaxLength: 16,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Contact_GsmNumber",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Contact_EmailAddress",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(120)",
                maxLength: 120,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address_Street",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_GsmCountryCode",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(2)",
                maxLength: 2,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_GsmDialCode",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(6)",
                maxLength: 6,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_PhoneCountryCode",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(2)",
                maxLength: 2,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_PhoneDialCode",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(6)",
                maxLength: 6,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ManagerId",
                schema: "areas",
                table: "Areas",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ManagerName",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Contact_GsmCountryCode",
                schema: "areas",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "Contact_GsmDialCode",
                schema: "areas",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "Contact_PhoneCountryCode",
                schema: "areas",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "Contact_PhoneDialCode",
                schema: "areas",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "ManagerId",
                schema: "areas",
                table: "Areas");

            migrationBuilder.DropColumn(
                name: "ManagerName",
                schema: "areas",
                table: "Areas");

            migrationBuilder.AlterColumn<string>(
                name: "Contact_PhoneNumber",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(16)",
                maxLength: 16,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Contact_GsmNumber",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)",
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Contact_EmailAddress",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(120)",
                oldMaxLength: 120,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address_Street",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contact_FaxNumber",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);
        }
    }
}
