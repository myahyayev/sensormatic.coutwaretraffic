﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class DeviceStatusChangeSyccessfullyHandler : ICommandHandler<DeviceStatusChangeSuccessfully>
    {
        private readonly IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> _hubContext;
        public DeviceStatusChangeSyccessfullyHandler(IHubContext<DeviceNotificationsHub, IDeviceNotificationsHub> hubContext)
            => _hubContext = hubContext;


        public async Task HandleAsync(DeviceStatusChangeSuccessfully command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .DeviceStatusChangedSuccessfullyReceived(command);
    }
}
