﻿using CountwareTraffic.Services.Reporting.Application;
using Newtonsoft.Json;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using Sensormatic.Tool.QueueModel;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Api
{
    public class ReportGeneratingCompletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.ReportGeneratingCompleted>, ITransientSelfDependency
    {
        private readonly IEventMapper _eventMapper;
        private readonly IQueueService _queueService;
        public ReportGeneratingCompletedConsumer(IEventMapper eventMapper, IQueueService queueService)
        {
            _eventMapper = eventMapper;
            _queueService = queueService;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.ReportGeneratingCompleted queuEvent)
        {
            var sendTemplatedEmail = new SendTemplatedEmail();

            if (queuEvent.LanguageShortName == "en-US")
            {
                sendTemplatedEmail.EmailTemplateAssemblyName = typeof(ReportEmailTemplateEn).AssemblyQualifiedName;

                sendTemplatedEmail.EmailTemplate = new ReportEmailTemplateEn
                {
                    ReportCreatedBy = queuEvent.CreatedBy,
                    ReportEmailBody = queuEvent.EmailBody,
                    ReportFileFormat = queuEvent.Format,
                    ReportDescription = queuEvent.Description,
                    EmailSubject = queuEvent.EmailSubject,
                    ReportHeader = queuEvent.Title,
                    ReportLocations = queuEvent.Locations,
                    ReportName = queuEvent.Name,
                    ReportUrl = queuEvent.FilePath,
                    To = JsonConvert.DeserializeObject<List<string>>(queuEvent.EmailTo),
                    UserIds = new System.Collections.Generic.List<Guid> { Guid.NewGuid() },
                    ReportCreationDate = queuEvent.CreationDate,
                    ReportDateRange = queuEvent.DateRange,
                };
            }

            if (queuEvent.LanguageShortName == "tr-TR")
            {
                sendTemplatedEmail.EmailTemplateAssemblyName = typeof(ReportEmailTemplateTr).AssemblyQualifiedName;

                sendTemplatedEmail.EmailTemplate = new ReportEmailTemplateTr
                {
                    ReportCreatedBy = queuEvent.CreatedBy,
                    ReportEmailBody = queuEvent.EmailBody,
                    ReportFileFormat = queuEvent.Format,
                    ReportDescription = queuEvent.Description,
                    EmailSubject = queuEvent.EmailSubject,
                    ReportHeader = queuEvent.Title,
                    ReportLocations = queuEvent.Locations,
                    ReportName = queuEvent.Name,
                    ReportUrl = queuEvent.FilePath,
                    To = JsonConvert.DeserializeObject<List<string>>(queuEvent.EmailTo),
                    UserIds = new System.Collections.Generic.List<Guid> { Guid.NewGuid() },
                    ReportCreationDate = queuEvent.CreationDate,
                    ReportDateRange = queuEvent.DateRange,
                };
            }

            _queueService.Send(Queues.CountwareTrafficSendTemplatedEmail, sendTemplatedEmail);

        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
