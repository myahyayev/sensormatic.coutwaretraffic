﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class UpdateUserHandler : IQueryHandler<UpdateUser,IdentityUpdateUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public UpdateUserHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public async Task<IdentityUpdateUserResponse> HandleAsync(UpdateUser query)
        {
            return await _identityServerService.UpdateUserAsync(new IdentityUpdateUserRequest
            {
                Id = query.Id,
                FirstName = query.FirstName,
                LastName = query.LastName,
                Email = query.Email,
                PhoneNumber = query.PhoneNumber,
                UserName = query.UserName,
                Status = query.Status,
                TwoFactorEnabled = query.TwoFactorEnabled,
                Gender = query.Gender,
                Birthdate = query.Birthdate,
                UserType = query.UserType,
                Address = query.Address,
                AllowSms = query.AllowSms,
                AllowEmail = query.AllowEmail,
                ProfilePic = query.ProfilePic,
                Areas = query.Areas,
                Roles = query.Roles
            });
        }
    }
}
