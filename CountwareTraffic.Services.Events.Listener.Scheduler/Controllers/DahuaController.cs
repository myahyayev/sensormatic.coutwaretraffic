﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]

    //ControllerBase was used instead of BaseController due to log traffic,use it in testing.
    public class DahuaController : ControllerBase
    {
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly ILogger<DahuaController> _logger;
        public DahuaController(
            ILogger<DahuaController> logger,
            ICommandDispatcher commandDispatcher)
        {
            _commandDispatcher = commandDispatcher;
            _logger = logger;
        }


        [HttpPost("realtime/events/xml")]
        public async Task<OkObjectResult> CreateEventAsync()
        {
            _logger.LogInformation($"Dahua start to read xml body.....");
            using (StreamReader sReader = new StreamReader(Request.Body))
            {
                string xmlBody = await sReader.ReadToEndAsync();

                _logger.LogInformation($"Dahua xml request: {xmlBody}");

                await _commandDispatcher.SendAsync(new DahuaEventLogXmlFormat()
                {
                    Xml = xmlBody
                });
            }
            return Ok(new { Success = "Success" });
        }
    }
}
