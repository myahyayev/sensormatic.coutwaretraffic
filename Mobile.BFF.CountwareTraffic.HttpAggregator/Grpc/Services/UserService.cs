﻿using CountwareTraffic.Services.Users.Grpc;
using Google.Protobuf.WellKnownTypes;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Enums;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Requests.IdentityServer;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Grpc.Common;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class UserService : IScopedSelfDependency
    {
        private readonly User.UserClient _userClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;

        public UserService(User.UserClient userClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _userClient = userClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<IEnumerable<GetTenantDetails>> GetTenantsAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_userClient.GetTenantsAsync, grpcRequest, hasClientSideLog: false);


            return grpcResponse.Tenants.Select(tenant => new GetTenantDetails
            {
                TenantId = new Guid(tenant.TenantId),
                Name = tenant.Name,
                Description = tenant.Description
            });
        }

        public async Task<TokenApiResponse> GetTokenAsync(TokenApiRequest request, string language)
        {
            GetTokenRequest grpcRequest = new()
            {
                Password = request.Password,
                UserName = request.UserName,
                TenantId = request.TenantId.HasValue ? request.TenantId.ToString() : "",
                Language = language
            };

            //todo: sadece tokenlar icin.
            var grpcResponse = await _asyncUnaryCallHandler
               .CallForTokenAsync(_userClient.GetTokenAsync, grpcRequest, hasClientSideLog: false);

            TokenApiResponse tokenResponse = new()
            {
                LoginStatus = grpcResponse.LoginStatus switch
                {
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.Unknown => LoginStatus.Unknown,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.FirstLogin => LoginStatus.FirstLogin,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.AlreadyLogged => LoginStatus.AlreadyLogged,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.AuthorityNull => LoginStatus.AuthorityNull,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.VerifyToken => LoginStatus.VerifyToken,
                    _ => throw new NotImplementedException(),
                }
            };


            tokenResponse.VerifyToken = grpcResponse.VerifyToken != null
                ?
                new VerifyTokenModel
                {
                    Authority = grpcResponse.VerifyToken.Authority,
                    VerifyToken = grpcResponse.VerifyToken.VerifyToken,
                    TimeOut = grpcResponse.VerifyToken.TimeOut,
                    Parameters = grpcResponse.VerifyToken.Parameters.Select(u => u).ToArray()
                }
                : null;



            tokenResponse.AuthorityNull = grpcResponse.AuthorityNull != null
                ?
                new AuthorityNullModel
                {
                    AuthToken = grpcResponse.AuthorityNull.AuthToken
                }
                : null;


            tokenResponse.AlreadyLogged = grpcResponse.AlreadyLogged != null
                ?
                new AlreadyLoggedModel
                {
                    AccessToken = grpcResponse.AlreadyLogged.AccessToken,
                    Email = grpcResponse.AlreadyLogged.Email,
                    ExpireTime = grpcResponse.AlreadyLogged.ExpireTime.ToDateTimeOffset().LocalDateTime,
                    FamilyName = grpcResponse.AlreadyLogged.FamilyName,
                    UniqueName = grpcResponse.AlreadyLogged.UniqueName,
                    PhoneNumber = grpcResponse.AlreadyLogged.PhoneNumber,
                    GivenName = grpcResponse.AlreadyLogged.GivenName,
                    RefreshToken = grpcResponse.AlreadyLogged.RefreshToken,
                    Gender = grpcResponse.AlreadyLogged.Gender switch
                    {
                        Gender.Male => (int)Gender.Male,
                        Gender.Femail => (int)Gender.Femail,
                        _ => throw new NotImplementedException()
                    }
                }
                : null;


            tokenResponse.FirstLogin = grpcResponse.FirstLogin != null
                ? new FirstLoginModel
                {
                    Message = grpcResponse.FirstLogin.Message
                }
                : null;

            tokenResponse.OtpDetail = grpcResponse.OtpDetail != null
                ? new OtpDetailModel
                {
                    ReferenceText = grpcResponse.OtpDetail.ReferenceText
                }
                : null;

            return tokenResponse;
        }

        public async Task<ChangePasswordApiResponse> ResetPasswordWithTokenAsync(string userName, ResetPasswordWithTokenApiRequest request)
        {
            ResetPasswordWithTokenRequest grpcRequest = new()
            {
                Token = request.Token,
                UserName = userName,
                Password = request.Password,
                ConfirmPassword = request.ConfirmPassword
            };

            var grpcResponse = await _asyncUnaryCallHandler
             .CallMethodAsync(_userClient.ResetPasswordWithTokenAsync, grpcRequest, hasClientSideLog: false);

            return new ChangePasswordApiResponse
            {
                Message = grpcResponse.Message
            };
        }

        public async Task<ChangePasswordApiResponse> ChangePasswordAsync(string userName, ChangePasswordApiRequest request)
        {
            ChangePasswordRequest grpcRequest = new()
            {
                ConfirmPassword = request.ConfirmPassword,
                CurrentPassword = request.CurrentPassword,
                NewPassword = request.NewPassword,
                UserName = userName
            };

            var grpcResponse = await _asyncUnaryCallHandler
             .CallMethodAsync(_userClient.ChangePasswordAsync, grpcRequest, hasClientSideLog: false);

            return new ChangePasswordApiResponse
            {
                Message = grpcResponse.Message
            };
        }

        public async Task<RefreshTokenApiResponse> RefreshTokenAsync(string accessToken, string refreshToken)
        {
            RefreshTokenRequest grpcRequest = new()
            {
                AccessToken = accessToken,
                RefreshTokenKey = refreshToken
            };

            var grpcResponse = await _asyncUnaryCallHandler
            .CallMethodAsync(_userClient.RefreshTokenAsync, grpcRequest, hasClientSideLog: false);

            return new RefreshTokenApiResponse
            {
                AccessToken = grpcResponse.AccessToken,
                RefreshToken = grpcResponse.RefreshTokenKey
            };
        }

        public async Task<ApiPagedResponse<UserApiResponse>> GetUserGridAsync(DataSourceApiRequest paging)
        {
            UserGridRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Users.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Users.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Users.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Users.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }
            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_userClient.GetUserGridAsync, grpcRequest, hasClientSideLog: false);


            var users = grpcResponse.UserGridData.Select(user => new UserApiResponse
            {
                Id = Guid.Parse(user.Id),
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                UserName = user.UserName,
                Status = user.Status.HasValue ? user.Status.Value : false,
                TwoFactorEnabled = user.TwoFactorEnabled.HasValue ? user.TwoFactorEnabled.Value : false,
                Gender = user.Gender,
                Birthdate = user.Birthdate.ToDateTime(),
                UserType = user.UserType,
                Address = user.Address,
                AllowSms = user.AllowSms.HasValue ? user.AllowSms.Value : false,
                AllowEmail = user.AllowEmail.HasValue ? user.AllowEmail.Value : false,
                ProfilePic = user.ProfilePic,
                Areas = user.Areas.ToList().Select(area => new AreaDetailApi { Id = Guid.Parse(area.Id), Level = int.Parse(area.Level) }).ToList(),
                Roles = user.Roles.ToList()
            });

            return new ApiPagedResponse<UserApiResponse>(users, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage == 0 ? false : true);
        }

        public async Task<UserApiResponse> GetUserAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
            .CallMethodAsync(_userClient.GetUserAsync, grpcRequest, hasClientSideLog: false);

            return new UserApiResponse
            {
                Id = Guid.Parse(grpcResponse.Id),
                FirstName = grpcResponse.FirstName,
                LastName = grpcResponse.LastName,
                Email = grpcResponse.Email,
                PhoneNumber = grpcResponse.PhoneNumber,
                UserName = grpcResponse.UserName,
                Status = grpcResponse.Status.HasValue ? grpcResponse.Status.Value : false,
                TwoFactorEnabled = grpcResponse.TwoFactorEnabled.HasValue ? grpcResponse.TwoFactorEnabled.Value : false,
                Gender = grpcResponse.Gender,
                Birthdate = grpcResponse.Birthdate.ToDateTime(),
                Address = grpcResponse.Address,
                AllowSms = grpcResponse.AllowSms.HasValue ? grpcResponse.AllowSms.Value : false,
                AllowEmail = grpcResponse.AllowEmail.HasValue ? grpcResponse.AllowEmail.Value : false,
                ProfilePic = grpcResponse.ProfilePic
            };
        }

        public async Task<CreateUserApiResponse> CreateUserAsync(CreateUserApiRequest request)
        {
            CreateUserRequest grpcRequest = new()
            {
                Id = request.Id.ToString(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                UserName = request.UserName,
                Status = request.Status,
                TwoFactorEnabled = request.TwoFactorEnabled,
                Gender = request.Gender,
                Birthdate = Timestamp.FromDateTimeOffset(request.Birthdate),
                UserType = request.UserType,
                Address = request.Address,
                AllowSms = request.AllowSms,
                AllowEmail = request.AllowEmail,
                ProfilePic = request.ProfilePic,
                Password = request.Password,
                ConfirmPassword = request.ConfirmPassword
            };
            request.Areas.ToList().ForEach(area => grpcRequest.Areas.Add(new AreaDetail { Id = area.Id.ToString(), Level = area.Level.ToString() }));
            request.Roles.ToList().ForEach(role => grpcRequest.Roles.Add(role));

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.CreateUserAsync, grpcRequest, hasClientSideLog: false);

            var response = new CreateUserApiResponse
            {
                Type = grpcResponse.Type,
                Title = grpcResponse.Title,
                Status = grpcResponse.Status,
                TraceId = grpcResponse.TraceId,
                Message = grpcResponse.Message
            };

            response.Errors = new CreateUserApiErrors { };

            if (grpcResponse.Errors != null)
            {
                if (grpcResponse.Errors.Email != null)
                    grpcResponse.Errors.Email.ToList().ForEach(email => response.Errors.Email.Add(email));
                if (grpcResponse.Errors.FirstName != null)
                    grpcResponse.Errors.FirstName.ToList().ForEach(firstName => response.Errors.FirstName.Add(firstName));
                if (grpcResponse.Errors.LastName != null)
                    grpcResponse.Errors.LastName.ToList().ForEach(lastName => response.Errors.LastName.Add(lastName));
                if (grpcResponse.Errors.UserName != null)
                    grpcResponse.Errors.UserName.ToList().ForEach(userName => response.Errors.UserName.Add(userName));
                if (grpcResponse.Errors.PhoneNumber != null)
                    grpcResponse.Errors.PhoneNumber.ToList().ForEach(phoneNumber => response.Errors.PhoneNumber.Add(phoneNumber));
                if (grpcResponse.Errors.Password != null)
                    grpcResponse.Errors.Password.ToList().ForEach(password => response.Errors.Password.Add(password));
                if (grpcResponse.Errors.ConfirmPassword != null)
                    grpcResponse.Errors.ConfirmPassword.ToList().ForEach(confirmPassword => response.Errors.ConfirmPassword.Add(confirmPassword));
            }

            return response;
        }

        public async Task<UpdateUserApiResponse> UpdateUserAsync(UpdateUserApiRequest request)
        {
            UpdateUserRequest grpcRequest = new()
            {
                Id = request.Id.ToString(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                UserName = request.UserName,
                Status = request.Status,
                TwoFactorEnabled = request.TwoFactorEnabled,
                Gender = request.Gender,
                Birthdate = Timestamp.FromDateTimeOffset(request.Birthdate),
                UserType = request.UserType,
                Address = request.Address,
                AllowSms = request.AllowSms,
                AllowEmail = request.AllowEmail,
                ProfilePic = request.ProfilePic
            };
            request.Areas.ToList().ForEach(area => grpcRequest.Areas.Add(new AreaDetail { Id = area.Id.ToString(), Level = area.Level.ToString() }));
            request.Roles.ToList().ForEach(role => grpcRequest.Roles.Add(role));

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.UpdateUserAsync, grpcRequest, hasClientSideLog: false);

            var response = new UpdateUserApiResponse
            {
                Type = grpcResponse.Type,
                Title = grpcResponse.Title,
                Status = grpcResponse.Status,
                TraceId = grpcResponse.TraceId,
                Message = grpcResponse.Message
            };

            response.Errors = new UpdateUserApiErrors { };
            if (grpcResponse.Errors != null)
            {
                if (grpcResponse.Errors.Email != null)
                    grpcResponse.Errors.Email.ToList().ForEach(email => response.Errors.Email.Add(email));
                if (grpcResponse.Errors.FirstName != null)
                    grpcResponse.Errors.FirstName.ToList().ForEach(firstName => response.Errors.FirstName.Add(firstName));
                if (grpcResponse.Errors.LastName != null)
                    grpcResponse.Errors.LastName.ToList().ForEach(lastName => response.Errors.LastName.Add(lastName));
                if (grpcResponse.Errors.UserName != null)
                    grpcResponse.Errors.UserName.ToList().ForEach(userName => response.Errors.UserName.Add(userName));
                if (grpcResponse.Errors.PhoneNumber != null)
                    grpcResponse.Errors.PhoneNumber.ToList().ForEach(phoneNumber => response.Errors.PhoneNumber.Add(phoneNumber));
            }

            return response;
        }

        public async Task<GetUserIdApiResponse> GetUserIdAsync(Guid userId)
        {
            UserIdRequest grpcRequest = new()
            {
                Id = userId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetUserIdAsync, grpcRequest, hasClientSideLog: false);

            var response = new GetUserIdApiResponse
            {
                Id = Guid.Parse(grpcResponse.Id),
                FirstName = grpcResponse.FirstName,
                LastName = grpcResponse.LastName,
                Email = grpcResponse.Email,
                PhoneNumber = grpcResponse.PhoneNumber,
                UserName = grpcResponse.UserName,
                Status = grpcResponse.Status.HasValue ? grpcResponse.Status.Value : false,
                TwoFactorEnabled = grpcResponse.TwoFactorEnabled.HasValue ? grpcResponse.TwoFactorEnabled.Value : false,
                Gender = grpcResponse.Gender,
                Birthdate = grpcResponse.Birthdate.ToDateTime(),
                UserType = grpcResponse.UserType,
                Address = grpcResponse.Address,
                AllowSms = grpcResponse.AllowSms.HasValue ? grpcResponse.AllowSms.Value : false,
                AllowEmail = grpcResponse.AllowEmail.HasValue ? grpcResponse.AllowEmail.Value : false,
                ProfilePic = grpcResponse.ProfilePic,
                Areas = new List<AreaDetailApi>(),
                Roles = new List<RoleDetailApi>()
            };
            grpcResponse.Areas.ToList().ForEach(area => response.Areas.Add(new AreaDetailApi { Id = Guid.Parse(area.Id), Level = int.Parse(area.Level) }));
            grpcResponse.Roles.ToList().ForEach(role => response.Roles.Add(new RoleDetailApi { Id = Guid.Parse(role.Id), Name = role.Name }));

            return response;
        }

        public async Task<DeleteUserApiResponse> DeleteUserAsync(Guid userId)
        {
            UserIdRequest grpcRequest = new()
            {
                Id = userId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.DeleteUserAsync, grpcRequest, hasClientSideLog: false);

            var response = new DeleteUserApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };

            return response;
        }

        public async Task<UserApiResponse> GetUserUserNameAsync(string userName)
        {
            UserNameRequest grpcRequest = new()
            {
                UserName = userName
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetUserUserNameAsync, grpcRequest, hasClientSideLog: false);

            var response = new UserApiResponse
            {
                Id = Guid.Parse(grpcResponse.Id),
                FirstName = grpcResponse.FirstName,
                LastName = grpcResponse.LastName,
                Email = grpcResponse.Email,
                PhoneNumber = grpcResponse.PhoneNumber,
                UserName = grpcResponse.UserName,
                Status = grpcResponse.Status.HasValue ? grpcResponse.Status.Value : false,
                TwoFactorEnabled = grpcResponse.TwoFactorEnabled.HasValue ? grpcResponse.TwoFactorEnabled.Value : false,
                Gender = grpcResponse.Gender,
                Birthdate = grpcResponse.Birthdate.ToDateTime(),
                Address = grpcResponse.Address,
                AllowSms = grpcResponse.AllowSms.HasValue ? grpcResponse.AllowSms.Value : false,
                AllowEmail = grpcResponse.AllowEmail.HasValue ? grpcResponse.AllowEmail.Value : false,
                ProfilePic = grpcResponse.ProfilePic
            };

            return response;
        }

        public async Task<UpdateUserApiResponse> CreateUserWithDefaultPasswordAsync(UpdateUserApiRequest request, string domain, string language)
        {
            UpdateUserRequest grpcRequest = new()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                UserName = request.UserName,
                Status = request.Status,
                TwoFactorEnabled = request.TwoFactorEnabled,
                Gender = request.Gender,
                Birthdate = Timestamp.FromDateTimeOffset(request.Birthdate),
                UserType = request.UserType,
                Address = request.Address,
                AllowSms = request.AllowSms,
                AllowEmail = request.AllowEmail,
                ProfilePic = request.ProfilePic,
                Domain = domain,
                Language = language
            };
            request.Areas.ToList().ForEach(area => grpcRequest.Areas.Add(new AreaDetail { Id = area.Id.ToString(), Level = area.Level.ToString() }));
            request.Roles.ToList().ForEach(role => grpcRequest.Roles.Add(role));

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.CreateUserWithDefaultPasswordAsync, grpcRequest, hasClientSideLog: false);

            var response = new UpdateUserApiResponse
            {
                Type = grpcResponse.Type,
                Title = grpcResponse.Title,
                Status = grpcResponse.Status,
                TraceId = grpcResponse.TraceId,
                Message = grpcResponse.Message
            };

            response.Errors = new UpdateUserApiErrors { };
            if (grpcResponse.Errors != null)
            {
                if (grpcResponse.Errors.Email != null)
                    grpcResponse.Errors.Email.ToList().ForEach(email => response.Errors.Email.Add(email));
                if (grpcResponse.Errors.FirstName != null)
                    grpcResponse.Errors.FirstName.ToList().ForEach(firstName => response.Errors.FirstName.Add(firstName));
                if (grpcResponse.Errors.LastName != null)
                    grpcResponse.Errors.LastName.ToList().ForEach(lastName => response.Errors.LastName.Add(lastName));
                if (grpcResponse.Errors.UserName != null)
                    grpcResponse.Errors.UserName.ToList().ForEach(userName => response.Errors.UserName.Add(userName));
                if (grpcResponse.Errors.PhoneNumber != null)
                    grpcResponse.Errors.PhoneNumber.ToList().ForEach(phoneNumber => response.Errors.PhoneNumber.Add(phoneNumber));
            }

            return response;
        }

        public async Task<UserNameExistApiResponse> GetUserNameExistAsync(string userName)
        {
            UserNameExistRequest grpcRequest = new()
            {
                UserName = userName
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetUserNameExistAsync, grpcRequest, hasClientSideLog: false);

            var response = new UserNameExistApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = grpcResponse.Data.HasValue ? grpcResponse.Data.Value : false
            };

            return response;
        }

        public async Task<EmailExistApiResponse> GetEmailExistAsync(string email)
        {
            EmailExistRequest grpcRequest = new()
            {
                Email = email
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetEmailExistAsync, grpcRequest, hasClientSideLog: false);

            var response = new EmailExistApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = grpcResponse.Data.HasValue ? grpcResponse.Data.Value : false
            };

            return response;
        }

        public async Task<ResetPasswordApiResponse> ResetPasswordAsync(ResetPasswordApiRequest request)
        {
            ResetPasswordRequest grpcRequest = new()
            {
                UserName = request.UserName
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.ResetPasswordAsync, grpcRequest, hasClientSideLog: false);

            var response = new ResetPasswordApiResponse
            {
                Message = grpcResponse.Message
            };

            return response;
        }

        public async Task<UserChangeStatusApiResponse> GetUserChangeStatusAsync(Guid userId)
        {
            UserChangeStatusRequest grpcRequest = new()
            {
                Id = userId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetUserChangeStatusAsync, grpcRequest, hasClientSideLog: false);

            return new UserChangeStatusApiResponse
            {
                Type = grpcResponse.Type,
                Message = grpcResponse.Message,
                Errors = grpcResponse.Errors.Select(error => new UserChangeStatusApiResponseError { Message = error.Message }).ToList()
            };
        }

        public async Task<UserNameExistsApiResponse> GetUserNameExistsAsync(string userName)
        {
            UserNameExistsRequest grpcRequest = new()
            {
                UserName = userName
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetUserNameExistsAsync, grpcRequest, hasClientSideLog: false);

            return new UserNameExistsApiResponse
            {
                Type = grpcResponse.Type,
                Message = grpcResponse.Message,
                Errors = grpcResponse.Errors.Select(error => new UserNameExistsApiResponseError { Message = error.Message }).ToList()
            };
        }

        public async Task<EmailExistsApiResponse> GetEmailExistsAsync(string email)
        {
            EmailExistsRequest grpcRequest = new()
            {
                Email = email
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetEmailExistsAsync, grpcRequest, hasClientSideLog: false);

            return new EmailExistsApiResponse
            {
                Response = grpcResponse.Response.HasValue ? grpcResponse.Response.Value : false
            };
        }

        public async Task<UserAvatarApiResponse> GetUserAvatarAsync(Guid userId)
        {
            UserAvatarRequest grpcRequest = new()
            {
                Id = userId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetUserAvatarAsync, grpcRequest, hasClientSideLog: false);

            return new UserAvatarApiResponse
            {
                Response = grpcResponse.Response.HasValue ? grpcResponse.Response.Value : false
            };
        }

        public async Task<UserResetApiResponse> UserResetAsync(Guid userId)
        {
            UserResetRequest grpcRequest = new()
            {
                Id = userId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.UserResetAsync, grpcRequest, hasClientSideLog: false);

            return new UserResetApiResponse
            {
                Response = grpcResponse.Response.HasValue ? grpcResponse.Response.Value : false
            };
        }

        public async Task<ApiPagedResponse<TenantGroupPaginationApiResponse>> GetTenantGroupPaginationAsync(DataSourceApiRequest paging)
        {
            TenantGroupPaginationRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Users.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Users.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Users.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Users.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetTenantGroupPaginationAsync, grpcRequest, hasClientSideLog: false);

            var tenantGroup = grpcResponse.Data.Select(data => new TenantGroupPaginationApiResponse
            {
                Id = Guid.Parse(data.TenantId),
                Name = data.Name,
                Description = data.Description
            });

            return new ApiPagedResponse<TenantGroupPaginationApiResponse>(tenantGroup, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage == 0 ? false : true);
        }

        public async Task<TenantGroupApiResponse> GetTenantGroupAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetTenantGroupAsync, grpcRequest, hasClientSideLog: false);

            return new TenantGroupApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = grpcResponse.Data.ToList().Select(data => new TenantGroupApiData
                {
                    Id = Guid.Parse(data.Id),
                    Name = data.Name,
                    Description = data.Description
                }).ToList()
            };
        }

        public async Task<CreateTenantGroupApiResponse> CreateTenantGroupAsync(CreateTenantGroupApiRequest request)
        {
            CreateTenantGroupRequest grpcRequest = new()
            {
                Name = request.Name,
                Description = request.Description
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.CreateTenantGroupAsync, grpcRequest, hasClientSideLog: false);

            return new CreateTenantGroupApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<UpdateTenantGroupApiResponse> UpdateTenantGroupAsync(UpdateTenantGroupApiRequest request)
        {
            UpdateTenantGroupRequest grpcRequest = new()
            {
                Id = request.Id.ToString(),
                Name = request.Name,
                Description = request.Description
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.UpdateTenantGroupAsync, grpcRequest, hasClientSideLog: false);

            return new UpdateTenantGroupApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<DeleteTenantGroupApiResponse> DeleteTenantGroupAsync(Guid tenantGroupId)
        {
            DeleteTenantGroupRequest grpcRequest = new()
            {
                Id = tenantGroupId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.DeleteTenantGroupAsync, grpcRequest, hasClientSideLog: false);

            return new DeleteTenantGroupApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<TenantGroupIdApiResponse> GetTenantGroupAsync(Guid id)
        {
            TenantGroupIdRequest grpcRequest = new()
            {
                Id = id.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetTenantGroupIdAsync, grpcRequest, hasClientSideLog: false);

            return new TenantGroupIdApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = new TenantGroupApiData
                {
                    Id = Guid.Parse(grpcResponse.Data.Id),
                    Name = grpcResponse.Data.Name,
                    Description = grpcResponse.Data.Description
                }
            };
        }

        public async Task<TenantGroupApiResponse> GetTenantGroupSelectAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetTenantGroupSelectAsync, grpcRequest, hasClientSideLog: false);

            return new TenantGroupApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = grpcResponse.Data.Select(data => new TenantGroupApiData
                {
                    Id = Guid.Parse(data.Id),
                    Name = data.Name,
                    Description = data.Description
                }).ToList()
            };
        }

        public async Task<CreateTenantApiResponse> CreateTenantAsync(CreateTenantApiRequest request)
        {
            CreateTenantRequest grpcRequest = new()
            {
                Name = request.Name,
                Description = request.Description,
                ErpCode = request.ErpCode,
                ErpGroupName = request.ErpGroupName,
                ErpStartDate = Timestamp.FromDateTimeOffset(request.ErpStartDate),
                ErpEndDate = Timestamp.FromDateTimeOffset(request.ErpEndDate),
                TenantGroupId = request.TenantGroupId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.CreateTenantAsync, grpcRequest, hasClientSideLog: false);

            return new CreateTenantApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<UpdateTenantApiResponse> UpdateTenantAsync(UpdateTenantApiRequest request)
        {
            UpdateTenantRequest grpcRequest = new()
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                ErpCode = request.ErpCode,
                ErpGroupName = request.ErpGroupName,
                ErpStartDate = Timestamp.FromDateTimeOffset(request.ErpStartDate),
                ErpEndDate = Timestamp.FromDateTimeOffset(request.ErpEndDate),
                TenantGroupId = request.TenantGroupId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.UpdateTenantAsync, grpcRequest, hasClientSideLog: false);

            return new UpdateTenantApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<TenantSelectApiResponse> GetTenantSelectAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetTenantSelectAsync, grpcRequest, hasClientSideLog: false);

            return new TenantSelectApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = grpcResponse.Data.Select(data => new TenantSelectApiData
                {
                    Id = Guid.Parse(data.Id),
                    Name = data.Name
                }).ToList()
            };
        }

        public async Task<ApiPagedResponse<RolePaginationApiResponse>> GetRolePaginationAsync(DataSourceApiRequest paging, string language)
        {
            GetRolePaginationRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                },
                Language = language
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Users.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Users.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Users.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Users.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetRolePaginationAsync, grpcRequest, hasClientSideLog: false);

            var roles = grpcResponse.Data.Select(data => new RolePaginationApiResponse
            {
                Id = Guid.Parse(data.Id),
                Name = data.Name,
                Description = data.Description,
                Permissions = data.Permissions.Select(permission => new RolePaginationPermissionApiData
                {
                    Id = Guid.Parse(permission.Id),
                    Name = permission.Name,
                    Description = permission.Description,
                    Label = permission.Label,
                    DescriptionLabel = permission.Label
                }).ToList()
            });

            return new ApiPagedResponse<RolePaginationApiResponse>(roles, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage == 0 ? false : true);

        }

        public async Task<RolesApiResponse> GetRoleAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetRoleAsync, grpcRequest, hasClientSideLog: false);

            return new RolesApiResponse
            {
                Data = grpcResponse.Data.Select(data => new RoleApiResponse
                {
                    Id = Guid.Parse(data.Id),
                    Name = data.Name,
                    Description = data.Description
                }).ToList()
            };
        }

        public async Task<CreateRoleApiResponse> CreateRoleAsync(CreateRoleApiRequest request)
        {
            CreateRoleRequest grpcRequest = new()
            {
                Name = request.Name,
                Description = request.Description
            };
            request.Permissions.ForEach(permission => grpcRequest.Permissions.Add(permission));

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.CreateRoleAsync, grpcRequest, hasClientSideLog: false);

            return new CreateRoleApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<UpdateRoleApiResponse> UpdateRoleAsync(UpdateRoleApiRequest request)
        {
            UpdateRoleRequest grpcRequest = new()
            {
                Id = request.Id.ToString(),
                Name = request.Name,
                Description = request.Description
            };
            request.Permissions.ForEach(permission => grpcRequest.Permissions.Add(permission));

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.UpdateRoleAsync, grpcRequest, hasClientSideLog: false);

            return new UpdateRoleApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<RoleIdApiResponse> GetRoleIdAsync(Guid roleId, string language)
        {
            RoleIdRequest grpcRequest = new()
            {
                Id = roleId.ToString(),
                Language = language
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetRoleIdAsync, grpcRequest, hasClientSideLog: false);

            return new RoleIdApiResponse
            {
                Message = grpcResponse.Message,
                Status = grpcResponse.Status,
                Data = new RoleIdApiResponseData
                {
                    Id = Guid.Parse(grpcResponse.Data.Id),
                    Name = grpcResponse.Data.Name,
                    Description = grpcResponse.Data.Description,
                    PermissionRoles = grpcResponse.Data.Permissions.Select(permission => new RoleIdPermissionApiResponse
                    {
                        Id = Guid.Parse(permission.Id),
                        Name = permission.Name,
                        Description = permission.Description,
                        Label = permission.Label,
                        DescriptionLabel = permission.DescriptionLabel
                    }).ToList()
                }
            };
        }

        public async Task<DeleteRoleApiResponse> DeleteRoleAsync(Guid roleId)
        {
            DeleteRoleRequest grpcRequest = new()
            {
                Id = roleId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.DeleteRoleAsync, grpcRequest, hasClientSideLog: false);

            return new DeleteRoleApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<AddRoleToUserApiResponse> AddRoleToUserAsync(AddRoleToUserApiRequest request)
        {
            AddRoleToUserRequest grpcRequest = new()
            {
                UserId = request.UserId.ToString(),
                RoleName = request.RoleName
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.AddRoleToUserAsync, grpcRequest, hasClientSideLog: false);

            return new AddRoleToUserApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<RemoveRoleFromUserApiResponse> RemoveRoleFromUserAsync(RemoveRoleFromUserApiRequest request)
        {
            RemoveRoleFromUserRequest grpcRequest = new()
            {
                UserId = request.UserId.ToString(),
                RoleName = request.RoleName
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.RemoveRoleFromUserAsync, grpcRequest, hasClientSideLog: false);

            return new RemoveRoleFromUserApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<AssignRolesToUserApiResponse> AssignRolesToUserAsync(string userName)
        {
            AssignRolesToUserRequest grpcRequest = new()
            {
                UserName = userName
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.AssignRolesToUserAsync, grpcRequest, hasClientSideLog: false);

            return new AssignRolesToUserApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<RoleSelectApiResponse> GetRoleSelectAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetRoleSelectAsync, grpcRequest, hasClientSideLog: false);

            var response = new RoleSelectApiResponse
            {
                Message = grpcResponse.Message,
                Status = grpcResponse.Status,
                Data = grpcResponse.Data.ToList().Select(data => new RoleSelectApiData
                {
                    Id = Guid.Parse(data.Id),
                    Name = data.Name
                }).ToList()
            };

            return response;
        }

        public async Task<ApiPagedResponse<PermissionApiData>> GetPermissionPaginationAsync(DataSourceApiRequest paging, string language)
        {
            GetPermissionPaginationRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                },
                Language = language
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Users.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Users.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Users.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Users.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetPermissionPaginationAsync, grpcRequest, hasClientSideLog: false);

            var permissions = grpcResponse.Data.Select(data => new PermissionApiData
            {
                Id = Guid.Parse(data.Id),
                Name = data.Name,
                Description = data.Description,
                Label = data.Label,
                DescriptionLabel = data.DescriptionLabel
            });

            return new ApiPagedResponse<PermissionApiData>(permissions, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage == 0 ? false : true);

        }

        public async Task<PermissionApiResponse> GetPermissionAsync(Guid permissionId, string language)
        {
            GetPermissionRequest grpcRequest = new()
            {
                Id = permissionId.ToString(),
                Language = language
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetPermissionAsync, grpcRequest, hasClientSideLog: false);

            return new PermissionApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = new PermissionApiData
                {
                    Id = Guid.Parse(grpcResponse.Data.Id),
                    Name = grpcResponse.Data.Name,
                    Description = grpcResponse.Data.Description,
                    Label = grpcResponse.Data.Label,
                    DescriptionLabel = grpcResponse.Data.DescriptionLabel
                }
            };
        }

        public async Task<DeletePermissionApiResponse> DeletePermissionAsync(Guid permissionId)
        {
            DeletePermissionRequest grpcRequest = new()
            {
                Id = permissionId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.DeletePermissionAsync, grpcRequest, hasClientSideLog: false);

            return new DeletePermissionApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<CreatePermissionApiResponse> CreatePermissionAsync(CreatePermissionApiRequest request)
        {
            CreatePermissionRequest grpcRequest = new()
            {
                Name = request.Name,
                Description = request.Description
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.CreatePermissionAsync, grpcRequest, hasClientSideLog: false);

            return new CreatePermissionApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<UpdatePermissionApiResponse> UpdatePermissionAsync(UpdatePermissionApiRequest request)
        {
            UpdatePermissionRequest grpcRequest = new()
            {
                Id = request.Id.ToString(),
                Name = request.Name,
                Description = request.Description
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.UpdatePermissionAsync, grpcRequest, hasClientSideLog: false);

            return new UpdatePermissionApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<RolePermissionApiResponse> RolePermissionAsync(RolePermissionApiRequest request)
        {
            RolePermissionRequest grpcRequest = new()
            {
                PermissionId = request.PermissionId.ToString(),
                RoleId = request.RoleId,
                StartDate = Timestamp.FromDateTimeOffset(request.StartDate),
                EndDate = Timestamp.FromDateTimeOffset(request.EndDate)
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.RolePermissionAsync, grpcRequest, hasClientSideLog: false);

            return new RolePermissionApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<RolePermissionsApiResponse> RolePermissionsAsync(RolePermissionsApiRequest request)
        {
            RolePermissionsRequest grpcRequest = new()
            {
                RoleId = request.RoleId,
                StartDate = Timestamp.FromDateTimeOffset(request.StartDate),
                EndDate = Timestamp.FromDateTimeOffset(request.EndDate)
            };
            request.PermissionIds.ForEach(permission => grpcRequest.PermissionIds.Add(permission.ToString()));

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.RolePermissionsAsync, grpcRequest, hasClientSideLog: false);

            return new RolePermissionsApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<RemovePermissionRoleApiResponse> RemovePermissionRoleAsync(RemovePermissionRoleApiRequest request)
        {
            RemovePermissionRoleRequest grpcRequest = new()
            {
                Id = request.Id.ToString(),
                PermissionId = request.PermissionId.ToString(),
                RoleId = request.RoleId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.RemovePermissionRoleAsync, grpcRequest, hasClientSideLog: false);

            return new RemovePermissionRoleApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<UpdatePermissionRoleApiResponse> UpdatePermissionRoleAsync(UpdatePermissionRoleApiRequest request)
        {
            UpdatePermissionRoleRequest grpcRequest = new()
            {
                Id = request.Id.ToString(),
                PermissionId = request.PermissionId.ToString(),
                RoleId = request.RoleId.ToString(),
                StartDate = Timestamp.FromDateTimeOffset(request.StartDate),
                EndDate = Timestamp.FromDateTimeOffset(request.EndDate)
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.UpdatePermissionRoleAsync, grpcRequest, hasClientSideLog: false);

            return new UpdatePermissionRoleApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<PermissionSelectApiResponse> GetPermissionSelectAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetPermissionSelectAsync, grpcRequest, hasClientSideLog: false);

            return new PermissionSelectApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = grpcResponse.Data.Select(data => new PermissionSelectApiResponseData
                {
                    Id = Guid.Parse(data.Id),
                    Name = data.Name
                }).ToList()
            };
        }

        public async Task<GroupApiResponse> GetGroupAsync(Guid groupId)
        {
            GetGroupRequest grpcRequest = new()
            {
                Id = groupId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetGroupAsync, grpcRequest, hasClientSideLog: false);

            return new GroupApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = grpcResponse.Data.Select(data => new GroupData
                {
                    Id = Guid.Parse(data.Id),
                    Name = data.Name,
                    Description = data.Description
                }).ToList()
            };
        }

        public async Task<DeleteGroupApiResponse> DeleteGroupAsync(Guid groupId)
        {
            DeleteGroupRequest grpcRequest = new()
            {
                Id = groupId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.DeleteGroupAsync, grpcRequest, hasClientSideLog: false);

            return new DeleteGroupApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<CreateGroupApiResponse> CreateGroupAsync(CreateGroupApiRequest request)
        {
            CreateGroupRequest grpcRequest = new()
            {
                Name = request.Name,
                Description = request.Description
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.CreateGroupAsync, grpcRequest, hasClientSideLog: false);

            return new CreateGroupApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<UpdateGroupApiResponse> UpdateGroupAsync(UpdateGroupApiRequest request)
        {
            UpdateGroupRequest grpcRequest = new()
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.UpdateGroupAsync, grpcRequest, hasClientSideLog: false);

            return new UpdateGroupApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<GroupAddUserApiResponse> GroupAddUserAsync(GroupAddUserApiRequest request)
        {
            GroupAddUserRequest grpcRequest = new()
            {
                UserId = request.UserId.ToString(),
                GroupId = request.GroupId.ToString(),
                StartDate = Timestamp.FromDateTimeOffset(request.StartDate),
                EndDate = Timestamp.FromDateTimeOffset(request.EndDate)
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GroupAddUserAsync, grpcRequest, hasClientSideLog: false);

            return new GroupAddUserApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<GroupSelectApiResponse> GetGroupSelectAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetGroupSelectAsync, grpcRequest, hasClientSideLog: false);

            return new GroupSelectApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message,
                Data = grpcResponse.Data.Select(data => new GroupSelectApiResponseData
                {
                    Id = Guid.Parse(data.Id),
                    Name = data.Name
                }).ToList()
            };
        }

        public async Task<DeleteTokenApiResponse> DeleteTokenAsync(Guid tokenId)
        {
            DeleteTokenRequest grpcRequest = new()
            {
                Id = tokenId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.DeleteTokenAsync, grpcRequest, hasClientSideLog: false);

            return new DeleteTokenApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<List<string>> GetRoleUsersAsync(Guid roleId)
        {
            GetRoleUsersRequest grpcRequest = new()
            {
                RoleId = roleId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetRoleUsersAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.Data.ToList();
        }

        public async Task<List<string>> GetUserRolesAsync(string userName)
        {
            GetUserRolesRequest grpcRequest = new()
            {
                UserName = userName
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetUserRolesAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.Data.ToList();
        }

        public async Task<List<UserPermissionApiResponse>> GetUserPermissionsAsync(Guid userId, string language)
        {
            GetUserPermissionsRequest grpcRequest = new()
            {
                UserId = userId.ToString(),
                Language = language
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetUserPermissionsAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.Data.Select(data => new UserPermissionApiResponse
            {
                Id = Guid.Parse(data.Id),
                Name = data.Name,
                Description = data.Description,
                Label = data.Label,
                DescriptionLabel = data.DescriptionLabel
            }).ToList();
        }

        public async Task<bool> ControlTokenAsync(string token)
        {
            ControlTokenRequest grpcRequest = new()
            {
                Token = token
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.ControlTokenAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.Response.HasValue ? grpcResponse.Response.Value : false;
        }

        public async Task<ForgotPasswordApiResponse> ForgotPasswordAsync(string UserName, string Domain, string Language)
        {
            ForgotPasswordRequest grpcRequest = new()
            {
                UserName = UserName,
                Domain = Domain,
                Language = Language
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.ForgotPasswordAsync, grpcRequest, hasClientSideLog: false);

            return new ForgotPasswordApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<ForgotPasswordApiResponse> ForgotPasswordTokenAsync(ForgotPasswordApiRequest request)
        {
            ForgotPasswordTokenRequest grpcRequest = new()
            {
                Token = request.Token,
                UserName = request.UserName,
                Password = request.Password,
                ConfirmPassword = request.ConfirmPassword
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.ForgotPasswordTokenAsync, grpcRequest, hasClientSideLog: false);

            return new ForgotPasswordApiResponse
            {
                Status = grpcResponse.Status,
                Message = grpcResponse.Message
            };
        }

        public async Task<TokenApiResponse> ControlOtpTokenAsync(ControlOtpTokenApiRequest request, string language)
        {
            ControlOtpTokenRequest grpcRequest = new()
            {
                Password = request.Password,
                UserName = request.UserName,
                TenantId = request.TenantId.HasValue ? request.TenantId.ToString() : "",
                Language = language,
                Token = request.Token,
                Otp = request.Otp
            };

            //todo: sadece tokenlar icin.
            var grpcResponse = await _asyncUnaryCallHandler
               .CallForTokenAsync(_userClient.ControlOtpTokenAsync, grpcRequest, hasClientSideLog: false);

            TokenApiResponse tokenResponse = new()
            {
                LoginStatus = grpcResponse.LoginStatus switch
                {
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.Unknown => LoginStatus.Unknown,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.FirstLogin => LoginStatus.FirstLogin,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.AlreadyLogged => LoginStatus.AlreadyLogged,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.AuthorityNull => LoginStatus.AuthorityNull,
                    global::CountwareTraffic.Services.Users.Grpc.LoginStatus.VerifyToken => LoginStatus.VerifyToken,
                    _ => throw new NotImplementedException(),
                }
            };


            tokenResponse.VerifyToken = grpcResponse.VerifyToken != null
                ?
                new VerifyTokenModel
                {
                    Authority = grpcResponse.VerifyToken.Authority,
                    VerifyToken = grpcResponse.VerifyToken.VerifyToken,
                    TimeOut = grpcResponse.VerifyToken.TimeOut,
                    Parameters = grpcResponse.VerifyToken.Parameters.Select(u => u).ToArray()
                }
                : null;



            tokenResponse.AuthorityNull = grpcResponse.AuthorityNull != null
                ?
                new AuthorityNullModel
                {
                    AuthToken = grpcResponse.AuthorityNull.AuthToken
                }
                : null;


            tokenResponse.AlreadyLogged = grpcResponse.AlreadyLogged != null
                ?
                new AlreadyLoggedModel
                {
                    AccessToken = grpcResponse.AlreadyLogged.AccessToken,
                    Email = grpcResponse.AlreadyLogged.Email,
                    ExpireTime = grpcResponse.AlreadyLogged.ExpireTime.ToDateTimeOffset().LocalDateTime,
                    FamilyName = grpcResponse.AlreadyLogged.FamilyName,
                    UniqueName = grpcResponse.AlreadyLogged.UniqueName,
                    PhoneNumber = grpcResponse.AlreadyLogged.PhoneNumber,
                    GivenName = grpcResponse.AlreadyLogged.GivenName,
                    RefreshToken = grpcResponse.AlreadyLogged.RefreshToken,
                    Gender = grpcResponse.AlreadyLogged.Gender switch
                    {
                        Gender.Male => (int)Gender.Male,
                        Gender.Femail => (int)Gender.Femail,
                        _ => throw new NotImplementedException()
                    }
                }
                : null;


            tokenResponse.FirstLogin = grpcResponse.FirstLogin != null
                ? new FirstLoginModel
                {
                    Message = grpcResponse.FirstLogin.Message
                }
                : null;

            return tokenResponse;
        }

        public async Task<GetTenantIdApiResponse> GetTenantIdAsync(Guid id)
        {
            GetTenantIdRequest grpcRequest = new()
            {
                Id = id.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetTenantIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetTenantIdApiResponse
            {
                Id = Guid.Parse(grpcResponse.Id),
                Name = grpcResponse.Name,
                Description = grpcResponse.Description,
                ErpCode = grpcResponse.ErpCode,
                ErpGroupName = grpcResponse.ErpGroupName,
                ErpStartDate = grpcResponse.ErpStartDate.ToDateTime(),
                ErpEndDate = grpcResponse.ErpEndDate.ToDateTime(),
                TenantGroup = new TenantGroupApiData
                {
                    Id = Guid.Parse(grpcResponse.TenantGroup.Id),
                    Name = grpcResponse.TenantGroup.Name,
                    Description = grpcResponse.TenantGroup.Description
                }
            };
        }

        public async Task<ApiPagedResponse<TenantPaginationApiResponse>> GetTenantPaginationAsync(DataSourceApiRequest paging)
        {
            TenantPaginationRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Users.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Users.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Users.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Users.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler.
                CallMethodAsync(_userClient.GetTenantPaginationAsync, grpcRequest, hasClientSideLog: false);

            var tenant = grpcResponse.Data.Select(data => new TenantPaginationApiResponse
            {
                Id = Guid.Parse(data.Id),
                Name = data.Name,
                Description = data.Description,
                ErpCode = data.ErpCode,
                ErpGroupName = data.ErpGroupName,
                ErpStartDate = data.ErpStartDate.ToDateTime(),
                ErpEndDate = data.ErpEndDate.ToDateTime(),
                TenantGroup = new TenantGroupApiData
                {
                    Id = Guid.Parse(data.TenantGroup.Id),
                    Name = data.TenantGroup.Name,
                    Description = data.TenantGroup.Description
                }
            });

            return new ApiPagedResponse<TenantPaginationApiResponse>(tenant, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage == 0 ? false : true);
        }

        public async Task<(GetContractByIdApiResponse result, int status, string message)> GetContractByIdAsync(Guid id)
        {
            GetContractsByIdRequest grpcRequest = new()
            {
                Id = id.ToString()
            };

            var result = await _asyncUnaryCallHandler.CallMethodAsync(
                                                _userClient.GetContractsByIdAsync,
                                                grpcRequest,
                                                hasClientSideLog: false
                                                );

            if (result.Status != 200)
            {
                return (null, result.Status, result.Message);
            }

            return (new GetContractByIdApiResponse
            {
                Id = result.Id.ToGuid(),
                AppType = result.AppType,
                ContractBody = result.ContractBody,
                ContractType = result.ContractType,
                Language = result.Language,
                Name = result.Name,
                ReleaseDate = result.ReleaseDate.ToDateTime(),
                Version = result.Version,
            }, result.Status, result.Message);
        }

        public async Task<CreateContractApiResponse> CreateContractAsync(CreateContractApiRequest request)
        {
            CreateContractRequest grpcRequest = new()
            {
                AppType = (int)AppType.CwTraffic,
                ContractBody = request.ContractBody,
                ContractType = request.ContractType,
                Language = request.Language,
                Name = request.Name,
                ReleaseDate = request.ReleaseDate.ToTimestamp(),
                Version = request.Version
            };

            var result = await _asyncUnaryCallHandler.CallMethodAsync(
                                               _userClient.CreateContractAsync,
                                               grpcRequest,
                                               hasClientSideLog: false
                                               );

            return new CreateContractApiResponse
            {
                Status = result.Status,
                Message = result.Message
            };
        }

        public async Task<UpdateContractApiResponse> UpdateContractAsync(UpdateContractApiRequest request)
        {
            UpdateContractRequest grpcRequest = new()
            {
                Id = request.Id.ToString(),
                AppType = (int)AppType.CwTraffic,
                ContractBody = request.ContractBody,
                ContractType = request.ContractType,
                Language = request.Language,
                Name = request.Name,
                ReleaseDate = request.ReleaseDate.ToTimestamp(),
                Version = request.Version
            };

            var result = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.UpdateContractAsync, grpcRequest, hasClientSideLog: false);

            return new UpdateContractApiResponse
            {
                Status = result.Status,
                Message = result.Message
            };
        }

        public async Task<GetContractsByParametersApiResponse> GetContractsByParametersAsync(GetContractsByParametersApiRequest request)
        {
            var grpcRequest = new GetContractsByParametersRequest
            {
                AppType = request.AppType,
                ContractType = request.ContractType,
                Language = request.Language,
            };

            var result = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.GetContractsByParametersAsync, grpcRequest, hasClientSideLog: false);

            var model = new GetContractsByParametersApiResponse
            {
                Message = result.Message,
                Status = result.Status,
                Data = new()
            };

            foreach (var contract in result.Data)
            {
                model.Data.Add(new GetContractsByParametersItemApiResponse
                {
                    AppType = contract.AppType,
                    ContractType = contract.ContractType,
                    Id = contract.Id.ToGuid(),
                    Name = contract.Name,
                    ReleaseDate = contract.ReleaseDate.ToDateTime(),
                    Version = contract.Version
                });
            }

            return model;
        }

        public async Task<ApiPagedResponse<AdminPermissionPaginationApiResponse>> AdminPermissionPaginationAsync(DataSourceApiRequest paging, string language)
        {
            AdminPermissionPaginationRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                },
                Language = language
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Users.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Users.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Users.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Users.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.AdminPermissionPaginationAsync,
                                                                      grpcRequest,
                                                                      hasClientSideLog: false);

            return new ApiPagedResponse<AdminPermissionPaginationApiResponse>(grpcResponse.Data.Select(x => new AdminPermissionPaginationApiResponse
            {
                Description = x.Description,
                DescriptionLabel = x.DescriptionLabel,
                Id = x.Id.ToGuid(),
                Label = x.Label,
                Name = x.Name
            }), grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage != 0);
        }

        public async Task<GetAdminPermissionSelectApiResponse> GetAdminPermissionSelect()
        {
            Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.GetAdminPermissionSelectAsync,
                                                                            grpcRequest,
                                                                            hasClientSideLog: false);

            return new GetAdminPermissionSelectApiResponse
            {
                Message = grpcResponse.Message,
                Status = grpcResponse.Status,
                Data = grpcResponse.Data.Select(x => new GetAdminPermissionSelectApiResponseItem
                {
                    Id = x.Id.ToGuid(),
                    Name = x.Name
                }).ToList()
            };
        }

        public async Task<CreateAdminRoleApiResponse> CreateAdminRole(CreateAdminRoleApiRequest request)
        {
            var grpcRequest = new CreateAdminRoleRequest
            {
                Description = request.Description,
                Name = request.Name
            };

            request.Permissions.ForEach(p => grpcRequest.Permissions.Add(p));

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.CreateAdminRoleAsync,
                                                                        grpcRequest,
                                                                        hasClientSideLog: false);

            return new CreateAdminRoleApiResponse
            {
                Message = grpcResponse.Message,
                Status = grpcResponse.Status
            };
        }

        public async Task<AdminRoleSelectApiResponse> GetRoleAdminSelect()
        {
            Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.GetRoleAdminPermissionSelectAsync,
                                                                  grpcRequest,
                                                                  hasClientSideLog: false);

            var model = new AdminRoleSelectApiResponse
            {
                Message = grpcResponse.Message,
                Status = grpcResponse.Status,
                Data = new()
            };

            foreach (var item in grpcResponse.Data)
            {
                model.Data.Add(new AdminRoleSelectApiResponseItem
                {
                    Id = item.Id.ToGuid(),
                    Name = item.Name
                });
            }

            return model;
        }

        public async Task<ApiPagedResponse<AdminGridApiResponseItem>> GetAdminGrid(DataSourceApiRequest request)
        {
            AdminGridRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = request.Paging.Limit,
                        Page = request.Paging.Page
                    }
                }
            };

            request.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Users.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Users.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Users.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in request.Filters)
            {
                global::CountwareTraffic.Services.Users.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.GetAdminGridAsync,
                                                                            grpcRequest,
                                                                            hasClientSideLog: false);

            var users = grpcResponse.Data.Select(user => new AdminGridApiResponseItem
            {
                Id = Guid.Parse(user.Id),
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                UserName = user.UserName,
                Status = user.Status.HasValue ? user.Status.Value : false,
                TwoFactorEnabled = user.TwoFactorEnabled.HasValue ? user.TwoFactorEnabled.Value : false,
                Gender = user.Gender,
                Birthdate = user.Birthdate.ToDateTime(),
                UserType = user.UserType,
                Address = user.Address,
                AllowSms = user.AllowSms.HasValue ? user.AllowSms.Value : false,
                AllowEmail = user.AllowEmail.HasValue ? user.AllowEmail.Value : false,
                ProfilePic = user.ProfilePic,
                Areas = user.Areas.ToList()
                                  .Select(area => new AreaDetailApi
                                  {
                                      Id = Guid.Parse(area.Id),
                                      Level = int.Parse(area.Level)
                                  })
                                  .ToList(),
                Roles = user.Roles.ToList(),
                Domain = user.Domain,
                Language = user.Language
            });

            return new ApiPagedResponse<AdminGridApiResponseItem>(users,
                                                              grpcResponse.TotalCount,
                                                              grpcResponse.Page,
                                                              grpcResponse.Limit,
                                                              grpcResponse.HasNextPage == 0 ? false : true);
        }

        public async Task<ApiPagedResponse<RoleAdminPermissionResponse>> RoleAdminPagination(DataSourceApiRequest paging, string language)
        {
            RoleAdminPaginationRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                },
                Language = language
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Users.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Users.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Users.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Users.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Users.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.RoleAdminPaginationAsync,
                                                                            grpcRequest,
                                                                            hasClientSideLog: false);

            var data = grpcResponse.Data.Select(x => new RoleAdminPermissionResponse
            {
                Description = x.Description,
                Id = x.Id.ToGuid(),
                Name = x.Name,
                Permissions = x.Data.Select(x => new RoleAdminPermissionItemResponse
                {
                    Id = x.Id.ToGuid(),
                    Name = x.Name,
                    Description = x.Description,
                    DescriptionLabel = x.DescriptionLabel,
                    Label = x.Label,
                }).ToList()
            }).ToList();

            return new ApiPagedResponse<RoleAdminPermissionResponse>(data,
                                                                     grpcResponse.TotalCount,
                                                                     grpcResponse.Page,
                                                                     grpcResponse.Limit,
                                                                     grpcResponse.HasNextPage != 0);
        }

        public async Task<GetAdminUserByIdApiResponse> GetAdminUserByIdApiAsync(GetAdminUserByIdApiRequest request)
        {
            GetAdminUserByIdRequest grpcRequest = new() { Id = request.Id.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.GetAdminUserByIdAsync,
                                                                  grpcRequest,
                                                                  hasClientSideLog: false);

            var response = new GetAdminUserByIdApiResponse
            {
                Id = grpcResponse.Id.ToGuid(),
                Address = grpcResponse.Address,
                AllowEmail = grpcResponse.AllowEmail.HasValue && grpcResponse.AllowEmail.Value,
                AllowSms = grpcResponse.AllowSms.HasValue && grpcResponse.AllowSms.Value,
                Birthdate = grpcResponse.Birthdate.ToDateTime(),
                Email = grpcResponse.Email,
                FirstName = grpcResponse.FirstName,
                LastName = grpcResponse.LastName,
                Gender = grpcResponse.Gender,
                PhoneNumber = grpcResponse.PhoneNumber,
                ProfilePic = grpcResponse.ProfilePic,
                Status = grpcResponse.Status.HasValue && grpcResponse.Status.Value,
                UserType = grpcResponse.UserType,
                UserName = grpcResponse.UserName,
                TwoFactorEnabled = grpcResponse.TwoFactorEnabled.HasValue && grpcResponse.TwoFactorEnabled.Value,
                Roles = grpcResponse.Roles.Select(x => new RoleDetailApi
                {
                    Id = x.Id.ToGuid(),
                    Name = x.Name
                }).ToList(),
                Tenants = grpcResponse.Tenants.Select(x => new GetAdminUserTenant
                {
                    Id = x.Id.ToGuid(),
                    Name = x.Name
                }).ToList()
            };

            return response;
        }

        public async Task<CreateAdminUserWithDefaultPasswordApiResponse> CreateAdminUserWithDefaultPasswordAsync(CreateAdminUserWithDefaultPasswordApiRequest request, string language, string domain)
        {
            CreateAdminUserWithDefaultPasswordRequest grpcRequest = new()
            {
                Address = request.Address,
                AllowEmail = request.AllowEmail,
                AllowSms = request.AllowSms,
                Birthdate = Timestamp.FromDateTimeOffset(request.BirthDate),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Gender = request.Gender,
                PhoneNumber = request.PhoneNumber,
                ProfilePic = request.ProfilePic,
                Status = request.Status,
                UserType = request.UserType,
                UserName = request.UserName,
                Domain = domain,
                Language = language,
                TwoFactorEnabled = request.TwoFactorEnabled,
            };

            request.Roles.ForEach(r => grpcRequest.Roles.Add(r));
            request.TenantIds.ForEach(t => grpcRequest.Tenants.Add(t));

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(
                                                            _userClient.CreateAdminUserWithDefaultPasswordAsync,
                                                            grpcRequest,
                                                            hasClientSideLog: false);

            return new CreateAdminUserWithDefaultPasswordApiResponse
            {
                Message = grpcResponse.Message,
                Status = grpcResponse.Status
            };
        }

        public async Task<UpdateAdminUserApiResponse> UpdateAdminUserAsync(UpdateAdminUserApiRequest request, string language, string domain)
        {
            var grpcRequest = new UpdateAdminUserRequest
            {
                Id = request.Id.ToString(),
                Address = request.Address,
                AllowEmail = request.AllowEmail,
                AllowSms = request.AllowSms,
                Birthdate = Timestamp.FromDateTimeOffset(request.BirthDate),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Gender = request.Gender,
                PhoneNumber = request.PhoneNumber,
                ProfilePic = request.ProfilePic,
                Status = request.Status,
                UserType = request.UserType,
                UserName = request.UserName,
                Domain = domain,
                Language = language,
                TwoFactorEnabled = request.TwoFactorEnabled
            };

            request.Roles?.ForEach(r => grpcRequest.Roles.Add(r));
            request.TenantIds?.ForEach(t => grpcRequest.Tenants.Add(t));

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(
                                                          _userClient.UpdateAdminUserAsync,
                                                          grpcRequest,
                                                          hasClientSideLog: false);

            return new UpdateAdminUserApiResponse
            {
                Message = grpcResponse.Message,
                Status = grpcResponse.Status
            };
        }

        public async Task<List<CheckContractsApiResponse>> CheckContractsAsync(string language)
        {
            var grpcRequest = new CheckContractsRequest
            {
                Language = language
            };

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(_userClient.CheckContractsAsync,
                                                                            grpcRequest,
                                                                            hasClientSideLog: false);
            
            return grpcResponse.Data.Select(data=>new CheckContractsApiResponse
            {
                ContractId=Guid.Parse(data.ContractId),
                ContractType=data.ContractType,
                Version=data.Version
            }).ToList();
        }

        public async Task<bool> ConfirmContractAsync(Guid contractId)
        {
            var grpcRequest = new ConfirmContractRequest
            {
               ContractId = contractId.ToString()
            };

            var grpcResponse = await _asyncUnaryCallHandler.CallMethodAsync(
                                                          _userClient.ConfirmContractAsync,
                                                          grpcRequest,
                                                          hasClientSideLog: false);

            return grpcResponse.Response??false;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
