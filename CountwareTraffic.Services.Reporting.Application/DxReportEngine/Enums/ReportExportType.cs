﻿
namespace CountwareTraffic.Services.Reporting.Engine
{
    public enum ReportExportype : byte
    {
        Unknown = 1,
        PDF = 2,
        XLS = 3,
        HTML = 4,
        CSV = 5,
    }
}
