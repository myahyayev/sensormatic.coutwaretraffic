﻿using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public interface IPagingResponse
    {
        public int TotalCount { get; }
        public int Page { get; }
        public int Limit { get; }
        public bool HasNextPage { get; }
        public int Prev { get; }
        public int Next { get; }
    }

    public class ApiPagedResponse<T> : IPagingResponse
    {
        public ApiPagedResponse(IEnumerable<T> data, int totalCount, int page, int limit, bool hasNextPage)
        {
            Data = data;
            TotalCount = totalCount;
            Page = page;
            Limit = limit;
            HasNextPage = hasNextPage;
            Next = HasNextPage ? page + 1 : 0;
            Prev = page > 1 ? page - 1 : 0;
        }

        public IEnumerable<T> Data { get; }
        public int TotalCount { get; }
        public int Page { get; }
        public int Limit { get; }
        public bool HasNextPage { get; }
        public int Next { get; }
        public int Prev { get; }
    }
}
