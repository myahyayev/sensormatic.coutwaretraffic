﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesCompanyDeletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.CompanyDeleted>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesCompanyDeletedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.CompanyDeleted queuEvent)
        {
            var companyDeleted = _eventMapper.Map(queuEvent) as CompanyDeleted;
            await _eventDispatcher.PublishAsync(companyDeleted);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
