﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class RegionDeleted : Convey.CQRS.Events.IEvent
    {
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public Guid CountryId { get; set; }
        public Guid ComapnyId { get; init; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
    }
}
