﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class SubAreaDeleted : IDomainEvent
    {
        public Guid SubAreaId { get; init; }
        public Guid RecordId { get; init; }
        public SubAreaDeleted(Guid subAreaId)
        {
            SubAreaId = subAreaId;
            RecordId = Guid.NewGuid();
        }
    }

    public class SubAreaCreated : IDomainEvent
    {
        public Guid SubAreaId { get; init; }
        public string Name { get; init; }
        public Guid RecordId { get; init; }
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid RegionId { get; init; }
        public string RegionName { get; init; }
        public Guid CountryId { get; init; }
        public string CountryName { get; init; }
        public Guid CompanyId { get; init; }
        public string CompanyName { get; init; }
        public Guid CountryLookupId { get; init; }
        public Guid CityId { get; init; }
        public string CityName { get; init; }
        public Guid DistrictId { get; init; }
        public string DistrictName { get; init; }

        public SubAreaCreated(Guid subAreaId, string name, Guid areaId, string areaName, Guid regionId, string regionName, Guid countryId, string countryName, Guid companyId, string companyName, Guid countryLookupId, Guid cityId, string cityName, Guid districtId, string districtName )
        {
            SubAreaId = subAreaId;
            Name = name;
            RecordId = Guid.NewGuid();
            AreaId = areaId;
            AreaName = areaName;
            RegionId = regionId;
            RegionName = regionName;
            CountryId = countryId;
            CountryName = countryName;
            CompanyId = companyId;
            CompanyName = companyName;
            CountryLookupId = countryLookupId;
            CityId = cityId;
            CityName = cityName;
            DistrictId = districtId;
            DistrictName = districtName;
        }
    }

    public class SubAreaChanged : IDomainEvent
    {
        public Guid SubAreaId { get; init; }
        public string Name { get; init; }
        public string OldName { get; set; }
        public Guid RecordId { get; init; }

        public SubAreaChanged(Guid subAreaId, string name, string oldName)
        {
            SubAreaId = subAreaId;
            Name = name;
            OldName = oldName;
            RecordId = Guid.NewGuid();
        }
    }


    public class SubAreaHierarchyBase
    {
        public Guid SubAreaId { get; set; }
        public string SubAreaName { get; set; }
        public Guid AreaId { get; set; }
        public Guid RegionId { get; set; }
        public Guid CountryId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid TenantId { get; set; }
        public Guid RecordId { get; init; }

        public SubAreaHierarchyBase(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, string subAreaName)
        {
            SubAreaId = subAreaId;
            SubAreaName = subAreaName;
            AreaId = areaId;
            RegionId = regionId;
            CompanyId = companyId;
            RecordId = Guid.NewGuid();
            TenantId = tenantId;
            CountryId = countryId;
        }
    }


    public class SubAreaHierarchyUpdated : SubAreaHierarchyBase, IDomainEvent
    {
        public SubAreaHierarchyUpdated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, string subAreaName) 
            : base(tenantId, companyId, countryId, regionId, areaId, subAreaId, subAreaName)
        {
        }
    }

    public class SubAreaHierarchyDeleted : SubAreaHierarchyBase, IDomainEvent
    {
        public SubAreaHierarchyDeleted(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, string subAreaName)
            : base(tenantId, companyId, countryId, regionId, areaId, subAreaId, subAreaName)
        {
        }
    }

    public class SubAreaHierarchyCreated : SubAreaHierarchyBase, IDomainEvent
    {
        public SubAreaHierarchyCreated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, Guid subAreaId, string subAreaName)
           : base(tenantId, companyId, countryId, regionId, areaId, subAreaId, subAreaName)
        {
        }
    }
}
