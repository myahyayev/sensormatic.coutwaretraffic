﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class EmailExistsApiResponse
    {
        [JsonPropertyName("response")]
        public bool Response { get; set; } 
    }
}
