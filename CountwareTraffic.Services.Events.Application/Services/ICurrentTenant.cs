﻿using Sensormatic.Tool.Ioc;
using System;

namespace CountwareTraffic.Services.Events.Application
{
    public interface ICurrentTenant : ITransientDependency
    {
        Guid Id { get; }
    }
}
