﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TemplateName { get; set; }
        public Guid TemplateId { get; set; }
        public int TemplateTypeId { get; set; }
        public string TemplateTypeName { get; set; }
        public int FormatId { get; set; }
        public string FormatName { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public bool IsActive { get; set; }
        public List<string> Locations { get; set; }
        public List<ReportFileDto> ReportFiles { get; set; }
        public Guid  TenantId { get; set; }
    }
}
