﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class RegioHierarchyBase
    {
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public Guid CountryId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid TenantId { get; set; }
        public Guid RecordId { get; init; }

        public RegioHierarchyBase(Guid tenantId, Guid companyId,  Guid countryId, Guid regionId, string regionName)
        {
            RegionId = regionId;
            RegionName = regionName;
            CompanyId = companyId;
            RecordId = Guid.NewGuid();
            TenantId = tenantId;
            CountryId = countryId;
        }
    }

    public class RegionHierarchyUpdated : RegioHierarchyBase, IDomainEvent
    {
        public RegionHierarchyUpdated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, string regionName)
            : base(tenantId, companyId, countryId, regionId, regionName)
        {
        }
    }

    public class RegionHierarchyDeleted : RegioHierarchyBase, IDomainEvent
    {
        public RegionHierarchyDeleted(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, string regionName) 
            : base(tenantId, companyId, countryId, regionId, regionName)
        {
        }
    }

    public class RegionHierarchyCreated : RegioHierarchyBase, IDomainEvent
    {
        public RegionHierarchyCreated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, string regionName) 
            : base(tenantId, companyId, countryId, regionId, regionName)
        {
        }
    }
}
