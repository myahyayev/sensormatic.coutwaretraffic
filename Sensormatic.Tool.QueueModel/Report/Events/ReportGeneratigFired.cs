﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record ReportGeneratigFired : MessageEnvelope, IQueueEvent
    {
        public ReportGeneratigFired(Guid reportId, int reportStatus, Guid recordId, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            ReportId = reportId;
            RecordId = recordId;
            ReportStatus = reportStatus;
        }

        public Guid ReportId { get; init; }
        public Guid RecordId { get; init; }
        public int ReportStatus { get; init; }
    }
}
