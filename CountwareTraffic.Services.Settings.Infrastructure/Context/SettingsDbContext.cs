﻿using CountwareTraffic.Services.Settings.Application;
using CountwareTraffic.Services.Settings.Core;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Ioc;
using System;

namespace CountwareTraffic.Services.Settings.Infrastructure
{
    public class SettingsDbContext : DbContext, IScopedSelfDependency
    {
        private readonly SettingsDbProvider _provider;
        private readonly ICurrentTenant _currentTenant;

        public SettingsDbContext(DbContextOptions options)
          : base(options) { }

        public SettingsDbContext(DbContextOptions options, SettingsDbProvider provider, ICurrentTenant currentTenant)
            : base(options)
        {
            _provider = provider;
            _currentTenant = currentTenant;
        }


        #region DBSETs
        public virtual DbSet<Note> Notes { get; set; }
        #endregion DBSETs


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies(false);

            if (_provider != null)
                optionsBuilder.AddInterceptors(new SaveChangesInterceptor(_provider));
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            //firstMigrate
            Guid tenantId = _currentTenant == null ? Guid.Empty : _currentTenant.Id;

            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new NoteEntityTypeConfiguration(tenantId));
        }
    }
}
