﻿namespace CountwareTraffic.Services.Reporting.Engine
{
    public enum ReportPeriodType : byte
    {
        Unknown = 1,
        Daily = 2,
        Weekly = 3,
        Monthly = 4
    }
}
