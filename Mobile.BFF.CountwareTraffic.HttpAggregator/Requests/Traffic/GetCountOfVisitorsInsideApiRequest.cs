﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetCountOfVisitorsInsideApiRequest
    {
        public List<HierarchyTreeSelectFilterRequest> HierarchyTreeSelectFilters { get; set; }
        public DateTime CurrentStartDate { get; set; }
    }
}
