﻿using CountwareTraffic.Services.Reporting.Application;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class QueueEventMapper : IQueueEventMapper
    {
        public List<Sensormatic.Tool.Queue.IQueueEvent> MapAll(IEnumerable<Sensormatic.Tool.Efcore.IDomainEvent> events, string userName, Guid userId, string correlationId, Guid tenantId)
             => events.Select(e => Map(e, userName, userId, correlationId, tenantId)).ToList();

        public Sensormatic.Tool.Queue.IQueueEvent Map(Sensormatic.Tool.Efcore.IDomainEvent @event, string userName, Guid userId, string correlationId, Guid tenantId)
        {
            switch (@event)
            {
                case Core.OneTimeReportGenerate e:
                    return new Sensormatic.Tool.QueueModel.OneTimeReportGenerate(
                        e.ReportId,
                        userName,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.ScheduleReportCreated e:
                    return new Sensormatic.Tool.QueueModel.ScheduleReportCreated(
                        e.ReportId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.ReportDeleted e:
                    return new Sensormatic.Tool.QueueModel.ReportDeleted(
                        e.ReportId,
                        e.RecordId,
                        userId,
                        correlationId,
                        tenantId
                    );

                case Core.ReportGeneratingCompleted e:
                    return new Sensormatic.Tool.QueueModel.ReportGeneratingCompleted(
                        e.ReportId,
                        e.FilePath,
                        e.FileId,
                        e.ReportStatus,
                        e.RecordId,
                        userId,
                        correlationId,
                        e.TenantId,
                        e.Name,
                        e.Description,
                        e.CreatedBy,
                        e.CreationDate,
                        e.Format,
                        e.Title,
                        e.Locations,
                        e.DateRange,
                        e.EmailTo,
                        e.EmailBody,
                        e.EmailSubject,
                        e.LanguageShortName
                    );

                case Core.ReportGeneratigFired e:
                    return new Sensormatic.Tool.QueueModel.ReportGeneratigFired(
                        e.ReportId,
                        e.ReportStatus,
                        e.RecordId, 
                        userId, 
                        correlationId,
                        e.TenantId
                        );

                case Core.ReportGeneratingFailed e:
                    return new Sensormatic.Tool.QueueModel.ReportGeneratingFailed(
                        e.ReportId,
                        e.FileId,
                        e.ReportStatus,
                        e.RecordId,
                        userId,
                        correlationId,
                        e.TenantId
                        );
            }

            return null;
        }
        public void Dispose() => GC.SuppressFinalize(this);
    }
}
