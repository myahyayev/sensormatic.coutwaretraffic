﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class CompaniesController : BaseController
    {
        private readonly CompanyService _companyService;
        public CompaniesController(IServiceProvider provider, ILogger<CompaniesController> logger, IHttpContextAccessor contextAccessor, CompanyService companyService) 
            : base(provider, logger, contextAccessor.HttpContext) => _companyService = companyService;



        [HttpGet("{companyId}/detail"), ServiceLog]
        public async Task<ApiResponse<GetCompanyDetails>> GetCompanyDetailAsync(Guid companyId)
            => Response(await _companyService.GetCompanyByIdAsync(companyId), ResultMessage.SuccessResultMessage);


        [HttpPost("details"), ServiceLog]
        public async Task<ApiPagedResponse<GetCompanyDetails>> GetCompanyDetailsAsync([FromBody] DataSourceApiRequest paging)
            => await _companyService.GetCompaniesAsync(paging);
      


        [HttpPost("add"), ServiceLog]
        public async Task<ApiResponse<AddApiResponse>> AddCompanyAsync([FromBody] AddCompanyApiRequest request)
        {
            await _companyService.AddCompanyAsync(request);
            return Response(new AddApiResponse { Created = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpPut("{companyId}/change"), ServiceLog]
        public async Task<ApiResponse<ChangeApiResponse>> ChangeCompanyAsync(Guid companyId, [FromForm] ChangeCompanyApiRequest request)
        {
            await _companyService.ChangeCompanyAsync(companyId, request);
            return Response(new ChangeApiResponse { Updated = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpDelete("{companyId}/delete"), ServiceLog]
        public async Task<ApiResponse<DeleteApiResponse>> DeleteCompanyAsync(Guid companyId)
        {
            await _companyService.DeleteCompanyAsync(companyId);
            return Response(new DeleteApiResponse { Deleted = "Success" }, ResultMessage.SuccessResultMessage);
        }
    }
}
