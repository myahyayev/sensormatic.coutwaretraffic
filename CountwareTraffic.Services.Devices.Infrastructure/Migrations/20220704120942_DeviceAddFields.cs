﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Devices.Infrastructure.Migrations
{
    public partial class DeviceAddFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Model",
                schema: "devices",
                table: "Devices");

            migrationBuilder.AddColumn<Guid>(
                name: "BrandId",
                schema: "devices",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BrandName",
                schema: "devices",
                table: "Devices",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ControlFrequency",
                schema: "devices",
                table: "Devices",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirmWare",
                schema: "devices",
                table: "Devices",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                schema: "devices",
                table: "Devices",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ModelId",
                schema: "devices",
                table: "Devices",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModelName",
                schema: "devices",
                table: "Devices",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                schema: "devices",
                table: "Devices",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BrandId",
                schema: "devices",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "BrandName",
                schema: "devices",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "ControlFrequency",
                schema: "devices",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "FirmWare",
                schema: "devices",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "IsActive",
                schema: "devices",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "ModelId",
                schema: "devices",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "ModelName",
                schema: "devices",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "Note",
                schema: "devices",
                table: "Devices");

            migrationBuilder.AddColumn<string>(
                name: "Model",
                schema: "devices",
                table: "Devices",
                type: "nvarchar(130)",
                maxLength: 130,
                nullable: true);
        }
    }
}
