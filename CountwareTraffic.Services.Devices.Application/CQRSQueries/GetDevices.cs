﻿using Convey.CQRS.Queries;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class GetDevices : IQuery<PagingResult<DeviceDetailsDto>>
    {
        public Guid? SubAreaId { get; set; }
        public PagingQuery PagingQuery { get; set; }
        public List<SortDescriptor> Sorts { get; set; }
        public List<GridFilter> Filters { get; set; }
    }
}
