﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [Authorize]
    public class RegionService : Region.RegionBase
    {
        private readonly ILogger<RegionService> _logger;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        public RegionService(ILogger<RegionService> logger, ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }

        public override async Task<GetRegionDetailResponse> GetRegionById(GetRegionRequest request, ServerCallContext context)
        {
            var region = await _queryDispatcher.QueryAsync(new GetRegion { RegionId = request._RegionId });

            GetRegionDetailResponse response = new();

            response.RegionDetail = new()
            {
                Id = region.Id.ToString(),
                Name = region.Name,
                Description = region.Description,
                CountryId = region.CountryId.ToString(),
                ManagerId = region.ManagerId.HasValue ? region.ManagerId.ToString() : null,
                ManagerName = region.ManagerName,
                Audit = new Audit
                {
                    AuditCreateBy = region.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(region.AuditCreateDate),
                    AuditModifiedBy = region.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(region.AuditModifiedDate),
                }
            };
            return response;
        }

        public override async Task<RegionPagingResponse> GetRegions(GetRegionsRequest request, ServerCallContext context)
        {
            var pagingAreas = await _queryDispatcher.QueryAsync(new GetRegions
            {
                CountryId = request._CountryId,

                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            RegionPagingResponse response = new()
            {
                TotalCount = pagingAreas.TotalCount,
                HasNextPage = pagingAreas.HasNextPage,
                Page = pagingAreas.Page,
                Limit = pagingAreas.Limit,
                Next = pagingAreas.Next,
                Prev = pagingAreas.Prev
            };

            pagingAreas.Data.ToList().ForEach(region => response.RegionDetails.Add(new RegionDetail
            {
                Id = region.Id.ToString(),
                Name = region.Name,
                Description = region.Description,
                CountryId = region.CountryId.ToString(),
                ManagerName = region.ManagerName,
                ManagerId = region.ManagerId.HasValue ? region.ManagerId.ToString() : null,
                Audit = new Audit
                {
                    AuditCreateBy = region.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(region.AuditCreateDate),
                    AuditModifiedBy = region.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(region.AuditModifiedDate),
                }
            }));
            return response;
        }

        public override async Task<CreateSuccessResponse> AddRegion(CreateRegionRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new CreateRegion
            {
                CountryId = request._CountryId,
                Description = request.Description,
                Name = request.Name,
                ManagerId = request._ManagerId,
                ManagerName = request.ManagerName
            });

            return new CreateSuccessResponse { Created = "Created" };
        }

        public override async Task<UpdateSuccessResponse> ChangeRegion(UpdateRegionRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UpdateRegion
            {
                Description = request.Description,
                Name = request.Name,
                ManagerName = request.ManagerName,
                ManagerId = request._ManagerId,
                RegionId = request._RegionId,
                CountryId = request._CountryId
            });

            return new UpdateSuccessResponse { Updated = "Updated" };
        }

        public override async Task<DeleteSuccessResponse> DeleteRegion(DeleteRegionRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new DeleteRegion { RegionId = request._RegionId });
            return new DeleteSuccessResponse { Deleted = "Deleted" };
        }

        public override async Task<GetRegionCountryIdResponse> GetRegionCountryId(GetRegionCountryIdRequest request, ServerCallContext context)
        {
            var countryLookupId = await _queryDispatcher.QueryAsync(new GetRegionCountryId { RegionId = request._RegionId });

            GetRegionCountryIdResponse response = new() { CountryLookupId = countryLookupId.ToString() };
            return response;
        }

        public override async Task<GetHierarchyResponse> GetHierarchy(GetHierarchyRequest request, ServerCallContext context)
        {
            var hierarchies = await _queryDispatcher.QueryAsync(new GetRegionHierarchy { Id = request._Id });

            GetHierarchyResponse response = new();

            hierarchies.ForEach(hierarchy => response.Hierarchies.Add(new Hierarchy
            {
                Id = hierarchy.Id.ToString(),
                Name = hierarchy.Name,
                HierarchyLevel = hierarchy.HierarchyLevel switch
                {
                    Core.HierarchyLevel.Tenant => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Tenant,
                    Core.HierarchyLevel.Company => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Company,
                    Core.HierarchyLevel.Country => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Country,
                    Core.HierarchyLevel.Region => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Region,
                    Core.HierarchyLevel.Area => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Area,
                    Core.HierarchyLevel.SubArea => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.SubArea,
                    Core.HierarchyLevel.Device => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Device,
                    _ => throw new NotImplementedException(),
                }
            }));

            return response;
        }

        public override async Task<KeyValueResponse> GetRegionsKeyValue(KeyValueRequest request, ServerCallContext context)
        {
            var keyValues = await _queryDispatcher.QueryAsync(new GetRegionKeyValue() { ParentId = request._ParentId });

            KeyValueResponse response = new KeyValueResponse();

            keyValues.ForEach(u => response.KeyValues.Add(new KeyValue
            {
                Id = u.Id.ToString(),
                Name = u.Name
            }));

            return response;
        }
    }
}
