﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityRefreshTokenRequest
    {
        [Required]
        [JsonPropertyName("accessToken")]
        public string AccessToken { get; set; }
        [Required]

        [JsonPropertyName("refreshToken")]
        public string RefreshToken { get; set; }
    }
}