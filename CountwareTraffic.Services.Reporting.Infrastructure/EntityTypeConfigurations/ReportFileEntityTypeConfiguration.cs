﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportFileEntityTypeConfiguration : IEntityTypeConfiguration<ReportFile>
    {
        public void Configure(EntityTypeBuilder<ReportFile> builder)
        {

            builder.ToTable("ReportFiles", SchemaNames.Reports);

            builder
              .Property(x => x.Id)
              .HasField("_id")
              .IsRequired()
              .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
              .Property(x => x.CreatedDate)
              .HasField("_createdDate")
              .IsRequired()
              .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
              .Property(x => x.DateRangeStartDate)
              .HasField("_dateRangeStartDate")
              .IsRequired()
              .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
             .Property(x => x.DateRangeEndDate)
             .HasField("_dateRangeEndDate")
             .IsRequired()
             .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
                .Property(x => x.FilePath)
                .HasField("_filePath")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .IsRequired()
                .HasMaxLength(2500);

            builder.HasOne<Report>().WithMany().HasForeignKey(x => x.ReportId);
        }
    }
}
