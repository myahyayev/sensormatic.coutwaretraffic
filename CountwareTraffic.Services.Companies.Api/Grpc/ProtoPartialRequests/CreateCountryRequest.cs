﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [ServiceLog]
    public sealed partial class CreateCountryRequest : SensormaticRequestValidate
    {
        internal Guid _CountryLookupId
        {
            get
            {
                if (Guid.TryParse(countryLookupId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.countryLookupId_ = value.ToString(); }
        }

        internal Guid _CompanyId
        {
            get
            {
                if (Guid.TryParse(companyId_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.companyId_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_CompanyId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1289 CompanyId cannot be null", nameof(_CompanyId)));

            if (_CountryLookupId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1290 CountryId cannot be null", nameof(_CountryLookupId)));

            if (Name.IsNullOrWhiteSpace() || Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1291 Name invalid format", nameof(Name)));

            if (Iso.IsNullOrWhiteSpace() || Iso.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1292 Iso invalid format", nameof(Iso)));

            if (Iso3.IsNullOrWhiteSpace() || Iso3.Length != 3)
                ValidateResults.Add(new ErrorResult($"#E1293 Iso3 invalid format", nameof(Iso3)));

            if (IsoNumeric <1)
                ValidateResults.Add(new ErrorResult($"#E1294 IsoNumeric must be above zero.", nameof(IsoNumeric)));

            if (Capital.IsNullOrWhiteSpace() || Capital.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1295 Capital invalid format", nameof(Capital)));

            if (!ContinentCode.IsNullOrWhiteSpace() && ContinentCode.Length > 5)
                ValidateResults.Add(new ErrorResult($"#E1296 ContinentCode invalid format", nameof(ContinentCode)));

            if (!CurrencyCode.IsNullOrWhiteSpace() && CurrencyCode.Length != 3)
                ValidateResults.Add(new ErrorResult($"#E1297 CurrencyCode invalid format", nameof(CurrencyCode)));
        }
    }
}
