﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class ReportGeneratingCompletedHandler : ICommandHandler<ReportGeneratingCompleted>
    {
        private readonly IHubContext<ReportNotificationsHub, IReportNotificationsHub> _hubContext;
        private readonly ILogger<ReportGeneratingCompletedHandler> _logger;
        public ReportGeneratingCompletedHandler(IHubContext<ReportNotificationsHub, IReportNotificationsHub> hubContext, ILogger<ReportGeneratingCompletedHandler> logger)
        {
            _logger = logger;
            _hubContext = hubContext;
        }

        public async Task HandleAsync(ReportGeneratingCompleted command)
        {
            _logger.LogInformation($"ReportGeneratingCompletedHandler : {command.TenantId}");

            await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .ReportGeneratingCompletedReceived(command);
        }
    }
}
