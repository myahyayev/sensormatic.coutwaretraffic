﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Events.Infrastructure.Migrations
{
    public partial class EventTableRefactoring : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalEnterCount",
                schema: "events",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "TotalExitCount",
                schema: "events",
                table: "Events");

            migrationBuilder.AddColumn<DateTime>(
                name: "EventEndDate",
                schema: "events",
                table: "Events",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "EventStartDate",
                schema: "events",
                table: "Events",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.Sql(@"CREATE UNIQUE INDEX [NC_IX_AuditIsDeleted_TenantId_EventStartDate_EventEndDate_DirectionTypeId_DeviceId] ON [events].[Events] ([AuditIsDeleted] DESC, [TenantId] ASC,[EventStartDate] ASC,[EventEndDate] ASC,[DirectionTypeId] ASC, [DeviceId] ASC)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP INDEX [NC_IX_AuditIsDeleted_TenantId_EventStartDate_EventEndDate_DirectionTypeId_DeviceId] ON [events].[Events]");

            migrationBuilder.DropColumn(
                name: "EventEndDate",
                schema: "events",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "EventStartDate",
                schema: "events",
                table: "Events");

            migrationBuilder.AddColumn<int>(
                name: "TotalEnterCount",
                schema: "events",
                table: "Events",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalExitCount",
                schema: "events",
                table: "Events",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
