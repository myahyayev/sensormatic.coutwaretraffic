﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class CheckContractsApiResponse
    {
        [JsonPropertyName("contractId")]
        public Guid ContractId { get; set; }

        [JsonPropertyName("contractType")]
        public string ContractType { get; set; }

        [JsonPropertyName("version")]
        public string Version { get; set; }
    }
}
