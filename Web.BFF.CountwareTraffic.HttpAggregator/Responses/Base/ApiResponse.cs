﻿namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class ApiResponse<T>
    {
        public T Data { get; set; }

        public MessageResponse Message { get; set; }
    }

    public class MessageResponse
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
    public class ResultMessage
    {
        public ApiResponseMessageType ResponseMessageType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public static ResultMessage SuccessResultMessage
            => new ResultMessage
            {
                Description = "Success",
                Title = "Success",
                ResponseMessageType = ApiResponseMessageType.Info
            };
    }

    public enum ApiResponseMessageType
    {
        Info = 1,
        Warning = 2,
        Error = 3,
        UnhandledException = 4,
        ValidationException = 5,
        Question = 6
    }
}
