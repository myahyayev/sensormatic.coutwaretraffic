﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure.QueryHandlers
{
    public class GetBrandLookupsHandler : IQueryHandler<GetBrandLookups, List<BrandLookupDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetBrandLookupsHandler(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<List<BrandLookupDetailsDto>> HandleAsync(GetBrandLookups query)
        {
            var brandLookups = await _unitOfWork.GetRepository<IBrandLookupRepository>().GetAllAsync();

            if (brandLookups == null) return null;

            return brandLookups.Select(countryLookup => new BrandLookupDetailsDto
            {
                Id = countryLookup.Id,
                Name = countryLookup.Name,
            }).ToList();
        }
    }
}
