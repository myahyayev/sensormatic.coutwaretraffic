using Convey;
using Convey.WebApi;
using CountwareTraffic.Services.Events.Application;
using CountwareTraffic.Services.Events.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.ApplicationInsights;
using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace CountwareTraffic.Services.Events.Listener.Scheduler
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;

            Env = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            ThreadPool.SetMaxThreads(32_767, 2_000);
            ThreadPool.SetMinThreads(1_000, 1_000);
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
                options.Limits.MaxRequestBodySize = int.MaxValue;
                options.Limits.MinRequestBodyDataRate = null;
            });
            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue; // if don't set default value is: 128 MB
                x.MultipartHeadersLengthLimit = int.MaxValue;
            });
            Sensormatic.Tool.Ioc.IoCGenerator.DoTNet.Current.Start(services, Configuration);

            services.AddApplicationInsights(Configuration);

            services.AddDbContext<EventDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("EventDbConnection"), x => x.UseNetTopologySuite().EnableRetryOnFailure()));

            services.AddControllers();
            //.AddXmlSerializerFormatters();

            services.AddCors(setupAction =>
            {
                setupAction.AddPolicy("AllowAll", builder =>
                {
                    builder.SetIsOriginAllowed((host) => true);
                    builder.AllowAnyMethod();
                    builder.AllowAnyHeader();
                    builder.AllowCredentials();
                });
            });



            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            if (!Env.IsProduction())
            {
                services.AddSwagger();
            }

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services
               .AddConvey()
               .AddApplication()
               .Build();

            services.AddHostedService<RedisExpiredKeysListenerService>();

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            ThreadPool.SetMaxThreads(32_767, 2_000);
            ThreadPool.SetMinThreads(1_000, 1_000);

            if (!env.IsProduction())
            {
                app.UseSwagger()
                   .UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", $"{Assembly.GetExecutingAssembly().GetName().Name} v{Assembly.GetExecutingAssembly().GetName().Version.ToString()}"));
            }

            app.UseForwardedHeaders();

            //.UseHttpsRedirection()
            app.UseRouting()
            .UseCors("AllowAll")
            .UseSensormaticExceptionMiddleware()
            .UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    context.Response.ContentType = "text/html;charset=utf-8";
                    await context.Response.WriteAsync($"<h1>{Assembly.GetExecutingAssembly().GetName().Name} v{Assembly.GetExecutingAssembly().GetName().Version.ToString()} is running</h1>");
                });

            });

        }
    }

    public static class ServiceCollectionExtensions
    {
        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(swagger =>
            {
                string assambly = Assembly.GetExecutingAssembly().GetName().Name;
                swagger.SwaggerDoc("v1", new OpenApiInfo { Title = assambly, Version = "v1" });
                // generate the XML docs that'll drive the swagger docs
                var xmlPath = Path.Combine(AppContext.BaseDirectory, $"{assambly}.xml");
                swagger.IncludeXmlComments(xmlPath);

                //swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                //{
                //    Name = "Authorization",
                //    Type = SecuritySchemeType.ApiKey,
                //    Scheme = "Bearer",
                //    BearerFormat = "JWT",
                //    In = ParameterLocation.Header,
                //    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                //});
                //swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
                //{
                //    {
                //        new OpenApiSecurityScheme
                //        {
                //            Reference = new OpenApiReference
                //            {
                //                Type = ReferenceType.SecurityScheme,
                //                Id = "Bearer"
                //            }
                //        }, new string[] {}
                //    }
                //});
            });
        }
    }
}
