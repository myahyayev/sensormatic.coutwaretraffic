﻿using CountwareTraffic.Services.Reporting.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class ReportScheduleEntityTypeConfiguration : IEntityTypeConfiguration<ReportSchedule>
    {
        public void Configure(EntityTypeBuilder<ReportSchedule> builder)
        {
            builder.ToTable("ReportSchedules", SchemaNames.Reports);

            builder
               .Property(x => x.Id)
               .HasField("_id")
               .IsRequired()
               .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property<int>("_frequency")
               .UsePropertyAccessMode(PropertyAccessMode.Field)
               .HasColumnName("Frequency")
               .IsRequired();
            builder.HasOne(o => o.ScheduleFrequency)
               .WithMany()
               .HasForeignKey("_frequency");

            builder
                .Property<int>("_deliveryTimeId")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("DeliveryTime")
                .IsRequired();
            builder.HasOne(o => o.ScheduleDeliveryTime)
                .WithMany()
                .HasForeignKey("_deliveryTimeId");


            builder
                .Property(x => x.StartsOn)
                .HasField("_startsOn")
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
               .Property(x => x.LastRunTime)
               .HasField("_lastRunTime")
               .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
                .Property(x => x.EndsOn)
                .HasField("_endsOn")
                .UsePropertyAccessMode(PropertyAccessMode.Field);

            builder
                .Property(x => x.AtSetTime)
                .HasField("_atSetTime")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasMaxLength(8);

            builder
                .Property(x => x.AtSetTimeZone)
                .HasField("_atSetTimeZone")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasMaxLength(600);

            builder
                .Property(x => x.RepeatOn)
                .HasField("_repeatOn")
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasMaxLength(2500);


            builder
                .HasOne<Report>()
                .WithMany()
                .HasForeignKey(x => x.ReportId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
