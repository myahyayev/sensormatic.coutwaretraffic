﻿using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportContentDto
    {
        public string ReportId { get; set; }
        public string ReportName { get; set; }
        public string ReportFormat { get; set; }
        public string ReportBase64Content { get; set; }
        public DateTime ReportCreatedDate { get; set; }
    }
}
