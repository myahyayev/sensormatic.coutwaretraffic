﻿using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetVisitorDurationApiResponse
    {
        public TimeSpan Duration { get; set; }
    }
}
