﻿using Sensormatic.Tool.ElasticSearch;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class TenantHierarchyElasticData : ElasticSearchEntity
    {
        public TenantHierarchyElasticData(Guid tenantId) => Id = tenantId;
        public Parent Parent { get; set; }
    }


    public class Parent
    {
        public bool Expanded { get; set; } = true;
        public string Label { get; set; }
        public Data Data { get; set; }
        public List<Child> Children { get; set; }
    }

    public class Data
    {
        public Guid Id { get; set; }
        public ElasticHierarchyLevel HierarchyLevel { get; set; }
    }

    public class Child
    {
        public bool Expanded { get; set; } = true;
        public string Label { get; set; }
        public Data Data { get; set; }
        public List<Child> Children { get; set; }
    }
}