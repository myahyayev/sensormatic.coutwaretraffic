﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class CompanyHierarchyhBase
    {
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid TenantId { get; set; }
        public string TenantName { get; set; }
        public Guid RecordId { get; init; }

        public CompanyHierarchyhBase(Guid tenantId, string tenantName, Guid companyId, string companyName)
        {
            CompanyId = companyId;
            CompanyName = companyName;
            RecordId = Guid.NewGuid();
            TenantId = tenantId;
            TenantName = tenantName;
        }
    }


    public class CompanyHierarchyCreated : CompanyHierarchyhBase, IDomainEvent
    {
        public CompanyHierarchyCreated(Guid tenantId, string tenantName, Guid companyId, string companyName)
            : base(tenantId, tenantName, companyId, companyName)
        {
        }
    }

    public class CompanyHierarchyDeleted : CompanyHierarchyhBase, IDomainEvent
    {
        public CompanyHierarchyDeleted(Guid tenantId, string tenantName, Guid companyId, string companyName) 
            : base(tenantId, tenantName, companyId, companyName)
        {
        }
    }

    public class CompanyHierarchyUpdated : CompanyHierarchyhBase, IDomainEvent
    {
        public CompanyHierarchyUpdated(Guid tenantId, string tenantName, Guid companyId, string companyName) 
            : base(tenantId, tenantName, companyId, companyName)
        {
        }
    }
}
