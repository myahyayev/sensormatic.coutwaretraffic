﻿using System;

namespace CountwareTraffic.Services.Devices
{
    public class DeviceMoreInformationDto
    {
        public DeviceHierarchy DeviceHierarchy { get; set; }
        public DeviceEndpoint DeviceEndpoint { get; set; }
        public DeviceQueue DeviceQueue { get; set; }
    }

    public class DeviceHierarchy
    {
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid SubAreaId { get; set; }
        public string SubAreaName { get; set; }
    }

    public class DeviceEndpoint
    {
        public string EndpointAddress { get; set; }
        public string SampleRequest { get; set; }
    }

    public class DeviceQueue
    {
        public string QueueName { get; set; }
        public string QueueModel { get; set; }
    }
}
