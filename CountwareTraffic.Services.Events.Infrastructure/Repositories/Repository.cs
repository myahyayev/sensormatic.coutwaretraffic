﻿using CountwareTraffic.Services.Events.Core;
using Microsoft.EntityFrameworkCore;
using Sensormatic.Tool.Efcore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        protected readonly EventDbContext _context;
        public Repository(EventDbContext context) =>
            _context = context;


        public async Task AddAsync(TEntity entity) =>
            await _context.Set<TEntity>()
                          .AddAsync(entity);

        public async Task<TEntity> GetByIdAsync(Guid id) =>
          await _context.Set<TEntity>()
                        .FindAsync(id);

        public async Task AddRangeAsync(IEnumerable<TEntity> entities) =>
            await _context.Set<TEntity>()
                          .AddRangeAsync(entities);

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate) =>
            _context.Set<TEntity>()
                    .Where(predicate);

        public async Task<IEnumerable<TEntity>> GetAllAsync() =>
            await _context.Set<TEntity>()
                    .ToListAsync();

        public void Remove(TEntity entity) =>
            _context.Set<TEntity>()
                    .Remove(entity);

        public void RemoveRange(IEnumerable<TEntity> entities) =>
            _context.Set<TEntity>()
                    .RemoveRange(entities);

        public Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate) =>
            _context.Set<TEntity>()
                    .SingleOrDefaultAsync(predicate);

        public IQueryable<TEntity> GetQuery(Guid? tenantId, Expression<Func<TEntity, bool>> predicate, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null) =>
            GetQuery(tenantId, orderBy).Where(predicate);



        public IQueryable<TEntity> GetQuery(Guid? tenantId, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query;

            if (typeof(TEntity).GetInterfaces().Any(x => x == typeof(IDeletable)))
                query = ((IQueryable<IDeletable>)_context.Set<TEntity>()
                                                         .AsQueryable())
                                                                        .Where(u => u.AuditIsDeleted == false)
                                                                        .Cast<TEntity>();
            else
                query = _context.Set<TEntity>().AsQueryable();


            if (typeof(TEntity).GetInterfaces().Any(x => x == typeof(IMultiTenant)) && tenantId.HasValue)
                query = ((IQueryable<IMultiTenant>)query).Where(u => u.TenantId == tenantId)
                                                         .Cast<TEntity>();
         

            if (orderBy != null)
                query = orderBy(query);

            return query;
        }
    }
}
