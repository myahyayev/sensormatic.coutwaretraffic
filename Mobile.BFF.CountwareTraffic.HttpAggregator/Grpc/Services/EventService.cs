﻿using CountwareTraffic.Services.Devices.Grpc;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class EventService : IScopedSelfDependency
    {
        private readonly Device.DeviceClient _deviceClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;
        public EventService(Device.DeviceClient deviceClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _deviceClient = deviceClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
