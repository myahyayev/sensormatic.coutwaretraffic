﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Reporting.Infrastructure.Migrations
{
    public partial class ReportLanguageTimeZone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LinuxTimeZone",
                schema: "reports",
                table: "ReportLanguages",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WindowsTimeZone",
                schema: "reports",
                table: "ReportLanguages",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LinuxTimeZone",
                schema: "reports",
                table: "ReportLanguages");

            migrationBuilder.DropColumn(
                name: "WindowsTimeZone",
                schema: "reports",
                table: "ReportLanguages");
        }
    }
}
