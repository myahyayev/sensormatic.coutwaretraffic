﻿using Convey.CQRS.Commands;
using CountwareTraffic.Services.Companies.Core;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Application
{
    public class DeleteSubAreaHandler : ICommandHandler<DeleteSubArea>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public DeleteSubAreaHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }

        public async Task HandleAsync(DeleteSubArea command)
        {
            var subAreaRepository = _unitOfWork.GetRepository<ISubAreaRepository>();

            var subArea = await subAreaRepository.GetAsync(command.SubAreaId, _currentTenant.Id);

            if (subArea is null)
                throw new SubAreaNotFoundException(command.SubAreaId);

            subAreaRepository.Remove(subArea);

            subArea.WhenDeleted(command.SubAreaId);

            var areaRepository = _unitOfWork.GetRepository<IAreaRepository>();

            var hierarchy = await areaRepository.GetHierarchyAsync(subArea.AreaId, _currentTenant.Id);

            subArea.WhenHierarchyDeleted(_currentTenant.Id, hierarchy.CopmanyId, hierarchy.CountryId, hierarchy.RegionId, hierarchy.AreaId, subArea.Id, subArea.Name);

            await _unitOfWork.CommitAsync();
        }
    }
}
