﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleWeeklyRepeatOnIdNotFoundException : DomainException
    {
        IEnumerable<ScheduleWeeklyRepeatOn> ScheduleWeeklyRepeatOns { get; }
        public int Id { get; }

        public ScheduleWeeklyRepeatOnIdNotFoundException(IEnumerable<ScheduleWeeklyRepeatOn> scheduleWeeklyRepeatOns, int id)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ScheduleWeeklyRepeatOn Id: {String.Join(",", scheduleWeeklyRepeatOns.Select(s => s.Id))}") }, 400, ResponseMessageType.Error)
        {
            ScheduleWeeklyRepeatOns = scheduleWeeklyRepeatOns;
            Id = id;
        }
    }
}
