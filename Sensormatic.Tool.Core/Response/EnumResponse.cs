﻿namespace Sensormatic.Tool.Core
{
    public class EnumResponse
    {
        public int Key { get; set; }
        public string Name { get; set; }
        public int? SortOrder { get; set; }
        public bool IsVisible { get; set; }
    }
}
