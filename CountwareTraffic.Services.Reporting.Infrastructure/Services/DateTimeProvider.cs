﻿using CountwareTraffic.Services.Reporting.Application;
using System;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime Now => DateTime.UtcNow;
    }
}
