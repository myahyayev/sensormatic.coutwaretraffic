﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetPermissionSelectHandler : IQueryHandler<GetPermissionSelect, IdentityPermissionSelectResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetPermissionSelectHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityPermissionSelectResponse> HandleAsync(GetPermissionSelect query)
        {
            return await _identityServerService.GetPermissionSelectAsync();
        }
    }
}
