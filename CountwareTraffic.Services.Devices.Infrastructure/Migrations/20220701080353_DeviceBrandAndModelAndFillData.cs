﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Devices.Infrastructure.Migrations
{
    public partial class DeviceBrandAndModelAndFillData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BrandLookups",
                schema: "devices",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrandLookups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BrandModelLookups",
                schema: "devices",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    BrandId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrandModelLookups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BrandModelLookups_BrandLookups_BrandId",
                        column: x => x.BrandId,
                        principalSchema: "devices",
                        principalTable: "BrandLookups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BrandModelLookups_BrandId",
                schema: "devices",
                table: "BrandModelLookups",
                column: "BrandId");


            Dictionary<Guid, string> brands = new Dictionary<Guid, string>();

            brands.Add(new Guid("FFA3A93A-C167-4DC9-A832-D847C0F6FD70"), "Hikvision");
            brands.Add(new Guid("875E20D0-2371-4083-AD33-0436397130DD"), "Brickstream");


            foreach (var brand in brands)
            {
                string insertQuery = $@"INSERT INTO[devices].[BrandLookups]
                (      
                       [Id]
                      ,[Name]
                )
                 VALUES
                 (
                         '{brand.Key}'
                       , '{brand.Value}'
                       
                 )";

                migrationBuilder.Sql(insertQuery);
            }

           
            Dictionary<Guid, string> hikvisionBrandModels = new Dictionary<Guid, string>();
            hikvisionBrandModels.Add(new Guid("432372E0-3C3A-4D3B-ADC3-AF846CE5A835"), "2510M - 25W");
            hikvisionBrandModels.Add(new Guid("09252796-B175-4B73-BF43-C895A5035D45"), "Hikvision1");

            foreach (var h in hikvisionBrandModels)
            {
                string insertQuery = $@"INSERT INTO[devices].[BrandModelLookups]
                (      
                       [Id]
                      ,[Name]
                      ,[BrandId]
                )
                 VALUES
                 (
                         '{h.Key}'
                       , '{h.Value}'
                       , '{"FFA3A93A-C167-4DC9-A832-D847C0F6FD70"}'
                       
                 )";

                migrationBuilder.Sql(insertQuery);
            }



            Dictionary<Guid, string> brikcstreamBrandModels = new Dictionary<Guid, string>();
            brikcstreamBrandModels.Add(new Guid("7B2A23E4-488E-4052-BCE4-48C17AA138AD"), "Brickstream1");
            brikcstreamBrandModels.Add(new Guid("6A93AC98-0BE6-40EE-91EE-2714F837D1F9"), "Brickstream2");

            foreach (var b in brikcstreamBrandModels)
            {
                string insertQuery = $@"INSERT INTO[devices].[BrandModelLookups]
                (      
                       [Id]
                      ,[Name]
                      ,[BrandId]
                )
                 VALUES
                 (
                         '{b.Key}'
                       , '{b.Value}'
                       , '{"875E20D0-2371-4083-AD33-0436397130DD"}'
                       
                 )";

                migrationBuilder.Sql(insertQuery);
            }


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BrandModelLookups",
                schema: "devices");

            migrationBuilder.DropTable(
                name: "BrandLookups",
                schema: "devices");
        }
    }
}
