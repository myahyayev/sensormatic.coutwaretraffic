﻿using Sensormatic.Tool.Common;
using Sensormatic.Tool.Core;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AddCountryApiRequest : SensormaticRequestValidate
    {
        public Guid CountryLookupId { get; set; }
        public string Iso { get; set; }
        public string Iso3 { get; set; }
        public int IsoNumeric { get; set; }
        public string Name { get; set; }
        public string Capital { get; set; }
        public string ContinentCode { get; set; }
        public string CurrencyCode { get; set; }

        public override void Validate()
        {
            if (CountryLookupId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1129 CountryLookupId cannot be null", nameof(CountryLookupId)));

            if (Name.IsNullOrWhiteSpace() || Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1130 Name invalid format", nameof(Name)));

            if (Iso.IsNullOrWhiteSpace() || Iso.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1131 Iso invalid format", nameof(Iso)));

            if (Iso3.IsNullOrWhiteSpace() || Iso3.Length != 3)
                ValidateResults.Add(new ErrorResult($"#E1132 Iso3 invalid format", nameof(Iso3)));

            if (IsoNumeric < 1)
                ValidateResults.Add(new ErrorResult($"#E1133 IsoNumeric must be above zero.", nameof(IsoNumeric)));

            if (Capital.IsNullOrWhiteSpace() || Capital.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1134 Capital invalid format", nameof(Capital)));

            if (!ContinentCode.IsNullOrWhiteSpace() && ContinentCode.Length > 5)
                ValidateResults.Add(new ErrorResult($"#E1135 ContinentCode invalid format", nameof(ContinentCode)));

            if (!CurrencyCode.IsNullOrWhiteSpace() && CurrencyCode.Length != 3)
                ValidateResults.Add(new ErrorResult($"#E1136 CurrencyCode invalid format", nameof(CurrencyCode)));
        }
    }

}
