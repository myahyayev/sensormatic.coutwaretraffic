﻿using CountwareTraffic.Services.Reporting.Grpc;
using Google.Protobuf.WellKnownTypes;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class ReportService : IScopedSelfDependency
    {
        private readonly Report.ReportClient _reportClient;
        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;

        public ReportService(Report.ReportClient reportClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _reportClient = reportClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<IEnumerable<GetReportTemplatesApiResponse>> GetReportTemplatesAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_reportClient.GetReportTemplatesAsync, grpcRequest, hasClientSideLog: false);


            return grpcResponse.ReportTemplates.Select(u => new GetReportTemplatesApiResponse
            {
                AuditCreateBy = new Guid(u.AuditCreateBy),
                AuditCreateDate = u.AuditCreateDate.ToDateTimeOffset().LocalDateTime,
                AuditModifiedBy = new Guid(u.AuditModifiedBy),
                AuditModifiedDate = u.AuditModifiedDate.ToDateTimeOffset().LocalDateTime,
                Description = u.Description,
                Id = new Guid(u.Id),
                Name = u.Name,
                TemplateTypes = u.TemplateTypes.Select(x => new TemplateType
                {
                    Name = x.Name,
                    TypeId = x.Id
                })
            });
        }

        public async Task<GetReportLookupsApiResponse> GetReportLookupsAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_reportClient.GetReportLookupsAsync, grpcRequest, hasClientSideLog: false);

            return new()
            {
                ReportFormats = grpcResponse.ReportFormats.Select(u => new Sensormatic.Tool.Core.EnumResponse
                {
                    IsVisible = u.IsVisible,
                    Key = u.Key,
                    Name = u.Name
                }),

                ScheduleDeliveryTimes = grpcResponse.ScheduleDeliveryTimes.Select(u => new Sensormatic.Tool.Core.EnumResponse
                {
                    IsVisible = u.IsVisible,
                    Key = u.Key,
                    Name = u.Name
                }),

                ScheduleFrequencies = grpcResponse.ScheduleFrequencies.Select(u => new Sensormatic.Tool.Core.EnumResponse
                {
                    IsVisible = u.IsVisible,
                    Key = u.Key,
                    Name = u.Name
                }),

                ScheduleWeeklyRepeatOns = grpcResponse.ScheduleWeeklyRepeatOns.Select(u => new Sensormatic.Tool.Core.EnumResponse
                {
                    IsVisible = u.IsVisible,
                    Key = u.Key,
                    Name = u.Name
                })
            };
        }

        public async Task<ApiPagedResponse<GetReportsDetail>> GetReportsAsync(DataSourceApiRequest paging)
        {
            GetReportsRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                }
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Reporting.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Reporting.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Reporting.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Reporting.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_reportClient.GetReportsAsync, grpcRequest, hasClientSideLog: false);

            var reports = grpcResponse.ReportDetails.Select(report => new GetReportsDetail
            {
                Id = new Guid(report.Id),
                Name = report.Name,
                Description = report.Description,
                IsActive = report.IsActive,
                Locations = report.Locations.Select(u => u).ToList(),
                TemplateName = report.TemplateName,
                TemplateTypeId = report.TemplateTypeId,
                ReportFormatId = report.ReportFormatId,
                ReportFormatName = report.ReportFormatName,
                ReportStatusId = report.ReportStatusId,
                ReportStatusName = report.ReportStatusName,
                TemplateTypeName = report.TemplateTypeName,
                TemplateId = new Guid(report.TemplateId),
                ReportFiles = report.ReportFiles.Select(u => new ReportFile
                {
                    Id = new Guid(u.Id),
                    FilePath = u.FilePath,
                    CreatedDate = u.CreatedDate.ToDateTimeOffset().LocalDateTime,
                    DateRangeStartDate = u.DateRangeStartDate.ToDateTimeOffset().LocalDateTime,
                    DateRangeEndDate = u.DateRangeEndDate.ToDateTimeOffset().LocalDateTime
                }).ToList(),
                FrequencyId = report.FrequencyId,
                FrequencyName = report.FrequencyName
            });

            return new ApiPagedResponse<GetReportsDetail>(reports, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }

        public async Task<GetGetReportByIdApiResponse> GetReportByIdAsync(Guid reportId)
        {
            var grpcRequest = new GetReportByIdRequest() { ReportId = reportId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_reportClient.GetReportByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetGetReportByIdApiResponse
            {
                Id = new Guid(grpcResponse.Id),
                Name = grpcResponse.Name,
                Description = grpcResponse.Description,
                IsActive = grpcResponse.IsActive,
                Locations = grpcResponse.Locations.Select(u => u).ToList(),
                ReportFormatId = grpcResponse.ReportFormatId,
                ReportFormatName = grpcResponse.ReportFormatName,
                ReportStatusId = grpcResponse.ReportStatusId,
                ReportStatusName = grpcResponse.ReportStatusName,
                TemplateName = grpcResponse.TemplateName,
                TemplateTypeId = grpcResponse.TemplateTypeId,
                TemplateTypeName = grpcResponse.TemplateTypeName,
                NotifyWhenComplete = grpcResponse.NotifyWhenComplete,
                EmailTo = grpcResponse.EmailTo,
                EmailBody = grpcResponse.EmailBody,
                EmailSubject = grpcResponse.EmailSubject,
                LanguageId = new Guid(grpcResponse.LanguageId),
                LanguageName = grpcResponse.LanguageName,
                ScheduleId = new Guid(grpcResponse.ScheduleId),
                ScheduleFrequencyId = grpcResponse.ScheduleFrequencyId,
                ScheduleFrequencyName = grpcResponse.ScheduleFrequencyName,
                ScheduleDeliveryTimeId = grpcResponse.ScheduleDeliveryTimeId,
                ScheduleDeliveryTimeName = grpcResponse.ScheduleDeliveryTimeName,
                AtSetTime = grpcResponse.AtSetTime,
                AtSetTimeZone = grpcResponse.AtSetTimeZone,
                RepeatOn = grpcResponse.RepeatOn,
                StartsOn = grpcResponse.StartsOn?.ToDateTimeOffset().LocalDateTime,
                EndsOn = grpcResponse.EndsOn?.ToDateTimeOffset().LocalDateTime,
                DateRangeEndDate = grpcResponse.DateRangeEndDate.ToDateTimeOffset().LocalDateTime,
                DateRangeStartDate = grpcResponse.DateRangeStartDate.ToDateTimeOffset().LocalDateTime,
                TemplateId = new Guid(grpcResponse.TemplateId),
                ReportFiles = grpcResponse.ReportFiles.Select(u => new ReportFile
                {
                    Id = new Guid(u.Id),
                    FilePath = u.FilePath,
                    CreatedDate = u.CreatedDate.ToDateTimeOffset().LocalDateTime
                }).ToList()
            };
        }

        public async Task<IEnumerable<GetReportLanguagesApiResponse>> GetReportLanguagesAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_reportClient.GetReportLanguagesAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.ReportLanguages.Select(r => new GetReportLanguagesApiResponse
            {
                Id = new Guid(r.Id),
                Name = r.Name,
                Description = r.Description
            });
        }

        public async Task CancelReportAsync(Guid reportId)
        {
            DeleteReportRequest grpcRequest = new() {  ReportId = reportId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_reportClient.DeleteReportAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task CreateReportAsync(AddReportApiRequest request)
        {
            CreateReportRequest grpcRequest = new()
            {
                Name = request.Name,
                TemplateId = request.TemplateId.ToString(),
                Description = request.Description,
                FormatId = request.FormatId,
                TemplateTypeId = request.TemplateTypeId,
                NotifyWhenComplete = request.NotifyWhenComplete,
                FrequencyId = request.FrequencyId,
                StartsOn = request.StartsOn.HasValue ? Timestamp.FromDateTimeOffset(request.StartsOn.Value) : null,
                EndsOn = request.EndsOn.HasValue ? Timestamp.FromDateTimeOffset(request.EndsOn.Value) : null,
                RepeatOn = request.RepeatOn,
                DeliveryTimeId = request.DeliveryTimeId,
                AtSetTime = request.AtSetTime,
                AtSetTimeZone = request.AtSetTimeZone,
                EmailTo = request.EmailTo,
                EmailSubject = request.EmailSubject,
                EmailBody = request.EmailBody,
                LanguageId = request.LanguageId.ToString(),
                DateRangeStartDate = Timestamp.FromDateTimeOffset(request.DateRangeStartDate),
                DateRangeEndDate = Timestamp.FromDateTimeOffset(request.DateRangeEndDate)
            };

            if (request.Locations != null && request.Locations.Count > 0)
            {
                foreach (var location in request.Locations)
                {
                    global::CountwareTraffic.Services.Reporting.Grpc.Location grpcLocation = new();

                    grpcLocation.Id = location.Id.ToString();
                    grpcLocation.Name = location.Name;
                    grpcLocation.LocationLevel = location.LocationLevel switch
                    {
                        LocationLevel.Tenant => global::CountwareTraffic.Services.Reporting.Grpc.LocationLevel.Tenant,
                        LocationLevel.Company => global::CountwareTraffic.Services.Reporting.Grpc.LocationLevel.Company,
                        LocationLevel.Country => global::CountwareTraffic.Services.Reporting.Grpc.LocationLevel.Country,
                        LocationLevel.Region => global::CountwareTraffic.Services.Reporting.Grpc.LocationLevel.Region,
                        LocationLevel.Area => global::CountwareTraffic.Services.Reporting.Grpc.LocationLevel.Area,
                        LocationLevel.SubArea => global::CountwareTraffic.Services.Reporting.Grpc.LocationLevel.SubArea,
                        LocationLevel.Device => global::CountwareTraffic.Services.Reporting.Grpc.LocationLevel.Device,
                        _ => global::CountwareTraffic.Services.Reporting.Grpc.LocationLevel.Tenant
                    };
                    grpcRequest.Locations.Add(grpcLocation);
                }
            }

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_reportClient.CreateReportAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task<GetReportTemplateViewApiResponse> GetReportTemplateViewAsync(Guid request)
        {
            GetReportTemplateViewRequest grpcRequest = new() { ReportTemplateId = request.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_reportClient.GetReportTemplateViewAsync, grpcRequest, hasClientSideLog: false);

            return new GetReportTemplateViewApiResponse
            {
                Id = new Guid(grpcResponse.Id),
                FilePath = grpcResponse.FilePath
            };
        }

        public async Task<ApiPagedResponse<ReportFile>> GetReportFilesAsync(Guid reportId, DataSourceApiRequest paging)
        {
            GetReportFilesRequest grpcRequest = new()
            {
                DataSourceRequest = new()
                {
                    PagingRequest = new()
                    {
                        Limit = paging.Paging.Limit,
                        Page = paging.Paging.Page
                    }
                },
                ReportId = reportId.ToString()
            };

            paging.Sorts?.ForEach(u => grpcRequest.DataSourceRequest.Sorts.Add(new global::CountwareTraffic.Services.Reporting.Grpc.SortDescriptor()
            {
                Field = u.Field,
                Direction = u.Direction switch
                {
                    Sensormatic.Tool.Core.Direction.Asc => global::CountwareTraffic.Services.Reporting.Grpc.Direction.Asc,
                    Sensormatic.Tool.Core.Direction.Desc => global::CountwareTraffic.Services.Reporting.Grpc.Direction.Desc,
                    _ => throw new NotImplementedException(),
                }
            }));

            foreach (var filter in paging.Filters)
            {
                global::CountwareTraffic.Services.Reporting.Grpc.GridFilter grpcFilter = new()
                {
                    Field = filter.Field,
                    Value = filter.Value
                };

                filter.StringValues?.ForEach(u => grpcFilter.StringValues.Add(u));
                filter.GuidValues?.ForEach(u => grpcFilter.GuidValues.Add(u.ToString()));

                grpcFilter.Operator = filter.Operator switch
                {
                    Sensormatic.Tool.Core.FilterEnum.Eq => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Eq,
                    Sensormatic.Tool.Core.FilterEnum.Neq => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Neq,
                    Sensormatic.Tool.Core.FilterEnum.StartsWith => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.StartsWith,
                    Sensormatic.Tool.Core.FilterEnum.EndsWith => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.EndsWith,
                    Sensormatic.Tool.Core.FilterEnum.Contains => global::CountwareTraffic.Services.Reporting.Grpc.FilterEnum.Contains,
                    _ => throw new NotImplementedException(),
                };

                grpcRequest.DataSourceRequest.Filters.Add(grpcFilter);
            }

            var grpcResponse = await _asyncUnaryCallHandler
                    .CallMethodAsync(_reportClient.GetReportFilesAsync, grpcRequest, hasClientSideLog: false);


            var reportFiles = grpcResponse.ReportFiles.Select(reportFile => new ReportFile
            {
                Id = new Guid(reportFile.Id),
                FilePath = reportFile.FilePath,
                CreatedDate = reportFile.CreatedDate.ToDateTimeOffset().LocalDateTime,
                DateRangeStartDate = reportFile.DateRangeStartDate.ToDateTimeOffset().LocalDateTime,
                DateRangeEndDate = reportFile.DateRangeEndDate.ToDateTimeOffset().LocalDateTime,
            });

            return new ApiPagedResponse<ReportFile>(reportFiles, grpcResponse.TotalCount, grpcResponse.Page, grpcResponse.Limit, grpcResponse.HasNextPage);
        }


        public async Task<RetryReportGeneratingApiResponse> RetryReportGeneratingAsync(Guid reportId, RetryReportGeneratingApiRequest request)
        {
            RetryReportGeneratingRequest grpcRequest = new()
            {
                ReportId = reportId.ToString(),
                ReportFileId = request.ReportFileId.HasValue ? request.ReportFileId.Value.ToString() : null,
                DateRangeStartDate = request.DateRangeStartDate.HasValue ? Timestamp.FromDateTimeOffset(request.DateRangeStartDate.Value) : null,
                DateRangeEndDate = request.DateRangeEndDate.HasValue ? Timestamp.FromDateTimeOffset(request.DateRangeEndDate.Value) : null,
            };

            var grpcResponse = await _asyncUnaryCallHandler
               .CallMethodAsync(_reportClient.RetryReportGeneratingAsync, grpcRequest, hasClientSideLog: false);

            return new RetryReportGeneratingApiResponse
            {
                IsSuccess = grpcResponse.IsSuccess
            };
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
