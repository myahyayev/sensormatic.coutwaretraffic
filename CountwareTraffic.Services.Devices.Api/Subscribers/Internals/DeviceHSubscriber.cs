﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Common;
using Sensormatic.Tool.Queue;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Api
{
    public class DeviceHSubscriber : BackgroundService
    {
        private readonly IQueueService _queueService;
        private readonly ILogger<DeviceHSubscriber> _logger;

        public DeviceHSubscriber(ILogger<DeviceHSubscriber> logger, IQueueService queueService)
        {
            _logger = logger;
            _queueService = queueService;
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var template = new QueueConfigTemplate
            {
                PrefetchCount = 1,
                RetryCount = 0,
                RetryIntervalSeconds = 0,
                ExcludeExceptions = new List<Type> { typeof(Exception), typeof(HttpRequestException), typeof(ArgumentNullException) },
                AutoScale = true,
                ScaleUpTo = 10
            };

            _queueService.Subscribe<DevicesDeviceHCreatedConsumer>(Queues.CountwareTrafficDevicesDeviceHCreated, template);
            _queueService.Subscribe<DevicesDeviceHUpdatedConsumer>(Queues.CountwareTrafficDevicesDeviceHChanged, template);
            _queueService.Subscribe<DevicesDeviceHDeletedConsumer>(Queues.CountwareTrafficDevicesDeviceHDeleted, template);

            return Task.CompletedTask;
        }

        public async override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("SubAreaH forcing to stop...");

            _queueService.StopConsumers();

            await Task.Delay(5000);

            await base.StopAsync(cancellationToken);
        }
    }
}

