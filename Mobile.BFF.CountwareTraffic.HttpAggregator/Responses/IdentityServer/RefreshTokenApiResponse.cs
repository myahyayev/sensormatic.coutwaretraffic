﻿namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class RefreshTokenApiResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
