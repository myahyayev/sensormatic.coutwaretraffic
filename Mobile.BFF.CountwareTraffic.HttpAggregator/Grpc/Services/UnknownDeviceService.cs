﻿using CountwareTraffic.Services.Events.Grpc;
using Mobile.BFF.CountwareTraffic.HttpAggregator.Grpc;
using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class UnknownDeviceService : IScopedSelfDependency
    {
        private readonly UnknownDevice.UnknownDeviceClient _unknownDeviceClient;

        private readonly AsyncUnaryCallHandler _asyncUnaryCallHandler;
        public UnknownDeviceService(UnknownDevice.UnknownDeviceClient unknownDeviceClient, AsyncUnaryCallHandler asyncUnaryCallHandler)
        {
            _unknownDeviceClient = unknownDeviceClient;
            _asyncUnaryCallHandler = asyncUnaryCallHandler;
        }

        public async Task<GetUnknownDeviceDetails> GetUnknownDeviceByIdAsync(Guid unknownDeviceId)
        {
            GetUnknownDeviceRequest grpcRequest = new() { UnknownDeviceId = unknownDeviceId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_unknownDeviceClient.GetUnknownDeviceByIdAsync, grpcRequest, hasClientSideLog: false);

            return new GetUnknownDeviceDetails
            {
                Id = new Guid(grpcResponse.UnknownDeviceDetail.Id),
                Name = grpcResponse.UnknownDeviceDetail.Name,
                HttpPort = grpcResponse.UnknownDeviceDetail.HttpPort,
                HttpsPort = grpcResponse.UnknownDeviceDetail.HttpsPort,
                IpAddress = grpcResponse.UnknownDeviceDetail.IpAddress,
                MacAddress = grpcResponse.UnknownDeviceDetail.MacAddress,
                OccurredOn = grpcResponse.UnknownDeviceDetail.OccurredOn.ToDateTimeOffset().LocalDateTime,
                SerialNumber = grpcResponse.UnknownDeviceDetail.SerialNumber,
            };
        }

        public async Task<IEnumerable<GetUnknownDeviceDetails>> GetUnknownDevicesAsync()
        {
            Google.Protobuf.WellKnownTypes.Empty grpcRequest = new();

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_unknownDeviceClient.GetUnknownDevicesAsync, grpcRequest, hasClientSideLog: false);

            return grpcResponse.UnknownDeviceDetails.Select(unknownDeviceDetail => new GetUnknownDeviceDetails
            {
                Id = new Guid(unknownDeviceDetail.Id),
                Name = unknownDeviceDetail.Name,
                HttpPort = unknownDeviceDetail.HttpPort,
                HttpsPort = unknownDeviceDetail.HttpsPort,
                IpAddress = unknownDeviceDetail.IpAddress,
                MacAddress = unknownDeviceDetail.MacAddress,
                OccurredOn = unknownDeviceDetail.OccurredOn.ToDateTimeOffset().LocalDateTime,
                SerialNumber = unknownDeviceDetail.SerialNumber,
            });
        }

        public async Task UnknownDeviceProcessedAsync(Guid unknownDeviceId)
        {
            UnknownDeviceProcessedRequest grpcRequest = new() { UnknownDeviceId = unknownDeviceId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_unknownDeviceClient.UnknownDeviceProcessedAsync, grpcRequest, hasClientSideLog: false);
        }

        public async Task<UpdateSuccessApiResponse> UnknownDeviceDismissAsync(Guid unknownDeviceId)
        {
            UnknownDeviceDismissRequest grpcRequest = new() { UnknownDeviceId = unknownDeviceId.ToString() };

            var grpcResponse = await _asyncUnaryCallHandler
                .CallMethodAsync(_unknownDeviceClient.UnknownDeviceDismissAsync, grpcRequest, hasClientSideLog: false);

            return new UpdateSuccessApiResponse
            {
                Updated = grpcResponse.Updated
            };
        }

        public void Dispose() => GC.SuppressFinalize(this);
    }
}
