﻿using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class AreaHDeleted : Convey.CQRS.Events.IEvent
    {
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid RegionId { get; set; }
        public Guid CountryId { get; set; }
        public Guid ComapnyId { get; init; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
    }
}
