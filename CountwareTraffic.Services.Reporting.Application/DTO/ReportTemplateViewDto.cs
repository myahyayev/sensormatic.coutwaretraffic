﻿using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class ReportTemplateViewDto
    {
        public Guid Id { get; set; }
        public string FilePath { get; set; }
    }
}
