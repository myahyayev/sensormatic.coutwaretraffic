﻿using Sensormatic.Tool.Core;
using System;
using Sensormatic.Tool.Common;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class AddAreaApiRequest : SensormaticRequestValidate
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string GsmNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Street { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int AreaTypeId { get; set; }
        public string ManagerName { get; set; }
        public Guid CityId { get; set; }
        public Guid DistrictId { get; set; }
        public Guid? ManagerId { get; set; }
        public string GsmCountryCode { get; set; }
        public string GsmDialCode { get; set; }
        public string PhoneDialCode { get; set; }
        public string PhoneCountryCode { get; set; }
        public TimeSpan? WorkingHoursStart { get; set; }
        public TimeSpan? WorkingHoursEnd { get; set; }
        public string WorkingTimeZone { get; set; }

        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1093 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1094 Name invalid format", nameof(Name)));

            if (!Description.IsNullOrWhiteSpace() && Description.Length > 2500)
                ValidateResults.Add(new ErrorResult($"#E1095 Description invalid format", nameof(Description)));

            if (!ManagerName.IsNullOrWhiteSpace() && ManagerName.Length > 100)
                ValidateResults.Add(new ErrorResult($"#E1096 ManagerName invalid format", nameof(ManagerName)));

            if (AreaTypeId < 1)
                ValidateResults.Add(new ErrorResult($"#E1097 AreaTypeId invalid format", nameof(AreaTypeId)));

            if (!Street.IsNullOrWhiteSpace() && Street.Length > 250)
                ValidateResults.Add(new ErrorResult($"#E1098 Street invalid format", nameof(Street)));

            if (!EmailAddress.IsNullOrWhiteSpace() && (!EmailValidator.EmailIsValid(EmailAddress) || EmailAddress.Length > 120))
                ValidateResults.Add(new ErrorResult($"#E1099 Email address invalid format", nameof(EmailAddress)));

            if (!PhoneNumber.IsNullOrWhiteSpace() && PhoneNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1100 PhoneNumber invalid format", nameof(PhoneNumber)));

            if (!GsmNumber.IsNullOrWhiteSpace() && GsmNumber.Length > 20)
                ValidateResults.Add(new ErrorResult($"#E1101 GsmNumber invalid format", nameof(GsmNumber)));

            if (!GsmDialCode.IsNullOrWhiteSpace() && GsmDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1102 GsmDialCode invalid format", nameof(GsmDialCode)));

            if (!PhoneDialCode.IsNullOrWhiteSpace() && PhoneDialCode.Length > 6)
                ValidateResults.Add(new ErrorResult($"#E1103 PhoneDialCode invalid format", nameof(PhoneDialCode)));

            if (!GsmCountryCode.IsNullOrWhiteSpace() && GsmCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1104 GsmCountryCode invalid format", nameof(GsmCountryCode)));

            if (!PhoneCountryCode.IsNullOrWhiteSpace() && PhoneCountryCode.Length != 2)
                ValidateResults.Add(new ErrorResult($"#E1105 PhoneCountryCode invalid format", nameof(PhoneCountryCode)));

            if (WorkingHoursStart == null ^ WorkingHoursEnd == null)
                ValidateResults.Add(new ErrorResult($"#E1559 AreaWorkingHour cannot be empty", $"{nameof(WorkingHoursEnd)} OR {nameof(WorkingHoursStart)}"));

            if (WorkingHoursStart != null && WorkingHoursEnd != null && WorkingHoursStart > WorkingHoursEnd)
                ValidateResults.Add(new ErrorResult($"#E1560 AreaWorkingHoursStart cannot be greater than AreaWorkingHoursEnd"));

            if (WorkingTimeZone.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1561 WorkingTimeZone cannot be null", nameof(WorkingTimeZone)));

            if (!WorkingTimeZone.IsNullOrWhiteSpace() && WorkingTimeZone.Deserialize<TimeZoneValues>() is null)
                ValidateResults.Add(new ErrorResult($"#E1562 WorkingTimeZone invalid format", nameof(WorkingTimeZone)));
        }
    }
}
