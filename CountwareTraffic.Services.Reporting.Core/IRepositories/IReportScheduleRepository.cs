﻿using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Core
{
    public interface IReportScheduleRepository : IRepository<ReportSchedule>, IScopedDependency
    {
        Task<ReportSchedule> GetByReportIdAsync(Guid reportId);
        Task<ScheduleFrequency> GetScheduleFrequencyAsync(Guid reportId);
        Task<IEnumerable<ReportScheduleDetailEntityDto>> GetScheduledReportsAsync();
    }
}
