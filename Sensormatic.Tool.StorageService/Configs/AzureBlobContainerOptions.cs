﻿using Sensormatic.Tool.Ioc;

namespace Sensormatic.Tool.StorageService
{
    public class AzureBlobContainerOptions : IConfigurationOptions
    {
        public string Name { get; set; }
    }
}
