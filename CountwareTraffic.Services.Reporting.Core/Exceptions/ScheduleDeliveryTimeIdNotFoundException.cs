﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ScheduleDeliveryTimeIdNotFoundException : DomainException
    {
        IEnumerable<ScheduleDeliveryTime> ScheduleDeliveryTimes { get; }
        public int Id { get; }

        public ScheduleDeliveryTimeIdNotFoundException(IEnumerable<ScheduleDeliveryTime> scheduleDeliveryTimes, int id)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ScheduleDeliveryTime Id: {String.Join(",", scheduleDeliveryTimes.Select(s => s.Id))}") }, 400, ResponseMessageType.Error)
        {
            ScheduleDeliveryTimes = scheduleDeliveryTimes;
            Id = id;
        }
    }
}
