﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class AddRoleToUserHandler : IQueryHandler<AddRoleToUser, IdentityAddRoleToUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public AddRoleToUserHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityAddRoleToUserResponse> HandleAsync(AddRoleToUser query)
        {
            return await _identityServerService.AddRoleToUserAsync(query.UserId, query.RoleName);
        }
    }
}
