﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public record DeviceEventsListener : Convey.CQRS.Commands.ICommand
    {
        public string Description { get; init; }
        public Guid DeviceId { get; init; }
        public int OnTimeIOCount { get; init; }
        public int TotalEnterCount { get; init; }
        public int TotalExitCount { get; init; }
        public int DirectionTypeId { get; init; }
        public Guid RecordId { get; init; }
        public DateTime EventDate { get; set; }
        public DateTime EventStartDate { get; set; }
        public DateTime EventEndDate { get; set; }
        public Guid UserId { get; set; }
        public string DeviceName { get; set; }
        public Guid TenantId { get; set; }
    }
}
