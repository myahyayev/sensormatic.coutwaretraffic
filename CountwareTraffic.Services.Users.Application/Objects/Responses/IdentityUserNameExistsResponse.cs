﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityUserNameExistsResponse
    {
        public string Type { get; set; }
        public string Message { get; set; }

        public List<IdentityUserNameExistsResponseError> Errors { get; set; }
    }

    public class IdentityUserNameExistsResponseError
    {
        public string Message { get; set; }
    }
}
