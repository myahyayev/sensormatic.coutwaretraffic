﻿using Newtonsoft.Json;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Sensormatic.Tool.Sms
{
    public static class HttpRequestFailureExcetion
    {
        public static async Task<HttpRequestFailureException> GetException(this HttpResponseMessage response)
        {
            string message = await response.Content.ReadAsStringAsync();

            try
            {
                var result = JsonConvert.DeserializeObject<ErrorModel>(message);

                return new HttpRequestFailureException(result.ErrorResults.Select(u => new ErrorResult(u.Message)).ToList(), (int)response.StatusCode, Enum.Parse<ResponseMessageType>(result.Type));
            }
            catch (Newtonsoft.Json.JsonSerializationException ex)
            {
                return new HttpRequestFailureException(new List<ErrorResult>() { new ErrorResult(message) }, (int)response.StatusCode, ResponseMessageType.Error);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
