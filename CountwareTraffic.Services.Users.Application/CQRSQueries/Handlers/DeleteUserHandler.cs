﻿using Convey.CQRS.Queries;
using System;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class DeleteUserHandler : IQueryHandler< DeleteUser, IdentityDeleteUserResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public DeleteUserHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;
        public async Task<IdentityDeleteUserResponse> HandleAsync(DeleteUser query)
        {
            return await _identityServerService.DeleteUserAsync(query.Id);
        }
    }
}
