﻿using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class CompanySeedData 
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            Dictionary<Guid, string> cities = new Dictionary<Guid, string>();
            Dictionary <Dictionary<Guid, string>, Guid> towns = new Dictionary<Dictionary<Guid, string>,Guid>();


            //migrationBuilder.Sql(@"INSERT INTO [areas].[SubAreaStatuses] ( Id, Name ) VALUES
            //                                           (1, 'Created'), (2, 'Completed'), (3, 'Rejected')");

            //migrationBuilder.Sql(@"INSERT INTO [areas].[AreaTypes] ( Id, Name ) VALUES
            //                                           (1, 'Unknown'), (2, 'Franchising'), (3, 'CompanyOwned')");



            //migrationBuilder.Sql(@"INSERT INTO [areas].[CountryLookups]
            //                           ([Id]
            //                           ,[Iso]
            //                           ,[Iso3]
            //                           ,[IsoNumeric]
            //                           ,[Name]
            //                           ,[Capital]
            //                           ,[ContinentCode]
            //                           ,[CurrencyCode])
            //                     VALUES
            //                           ('12679675-CD70-491E-B44D-485612C85952'
            //                           ,'TR'
            //                           ,'TUR'
            //                           ,'792'
            //                           ,'Turkiye'
            //                           ,'Ankara'
            //                           ,'90'
            //                           ,'TRY'
		          //                     )"
            //                  );


















            //cities.Add(new Guid("AA82F1D0-0AB1-42FA-9783-4C2D72CE3EA4"), "Adana");
            //    towns.Add(new Guid("x"), "Yüreğir");
            //    towns.Add(new Guid("x"), "Karataş");
            //    towns.Add(new Guid("x"), "Tufanbeyli");
            //    towns.Add(new Guid("x"), "Çukurova");
            //    towns.Add(new Guid("x"), "Pozantı");
            //    towns.Add(new Guid("x"), "Saimbeyli");
            //    towns.Add(new Guid("x"), "Karaisalı");
            //    towns.Add(new Guid("x"), "Aladağ");
            //    towns.Add(new Guid("x"), "Feke");
            //    towns.Add(new Guid("x"), "Yumurtalık");
            //    towns.Add(new Guid("x"), "Kozan");
            //    towns.Add(new Guid("x"), "Sarıçam");
            //    towns.Add(new Guid("x"), "Ceyhan");
            //    towns.Add(new Guid("x"), "İmamoğlu");
            //    towns.Add(new Guid("x"), "Seyhan");

            
 
 







            //cities.Add(new Guid("681E35B6-5B14-4E53-BCA1-4B5A8E635F85"), "Adiyaman");
            //     towns.Add(new Guid("x"), "Çelikhan");
            //     towns.Add(new Guid("x"), "Adıyaman Merkez");
            //     towns.Add(new Guid("x"), "Gölbaşı");
            //     towns.Add(new Guid("x"), "Tut");
            //     towns.Add(new Guid("x"), "Gerger");
            //     towns.Add(new Guid("x"), "Besni");
            //     towns.Add(new Guid("x"), "Sincik");
            //     towns.Add(new Guid("x"), "Kahta");
            //     towns.Add(new Guid("x"), "Samsat");

            


















            //cities.Add(new Guid("AF457A19-83BC-4E36-93BA-F70C901D6A28"), "Afyonkarahisar");
            //    towns.Add(new Guid("x"), "Bayat");
            //    towns.Add(new Guid("x"), "Evciler");
            //    towns.Add(new Guid("x"), "İscehisar");
            //    towns.Add(new Guid("x"), "Afyonkarahisar Merkez");
            //    towns.Add(new Guid("x"), "Emirdağ");
            //    towns.Add(new Guid("x"), "Çobanlar");
            //    towns.Add(new Guid("x"), "Çay");
            //    towns.Add(new Guid("x"), "İhsaniye");
            //    towns.Add(new Guid("x"), "Bolvadin");
            //    towns.Add(new Guid("x"), "Kızılören");
            //    towns.Add(new Guid("x"), "Sultandağı");
            //    towns.Add(new Guid("x"), "Hocalar");
            //    towns.Add(new Guid("x"), "Dazkırı");
            //    towns.Add(new Guid("x"), "Şuhut");
            //    towns.Add(new Guid("x"), "Sandıklı");
            //    towns.Add(new Guid("x"), "Başmakçı");
            //    towns.Add(new Guid("x"), "Sinanpaşa");
            //    towns.Add(new Guid("x"), "Dinar");
            








            //cities.Add(new Guid("50101B1C-3D6F-42F9-A056-BDBC31E6B554"), "Agri");
            //    towns.Add(new Guid("x"), "Diyadin");
            //    towns.Add(new Guid("x"), "Ağrı Merkez");
            //    towns.Add(new Guid("x"), "Hamur");
            //    towns.Add(new Guid("x"), "Doğubayazıt");
            //    towns.Add(new Guid("x"), "Tutak");
            //    towns.Add(new Guid("x"), "Taşlıçay");
            //    towns.Add(new Guid("x"), "Eleşkirt");
            //    towns.Add(new Guid("x"), "Patnos");
            







            //cities.Add(new Guid("BC9B1FF1-F728-47E6-B128-EFF2A29FE78B"), "Aksaray");
            //    towns.Add(new Guid("x"), "Güzelyurt");
            //    towns.Add(new Guid("x"), "Ortaköy");
            //    towns.Add(new Guid("x"), "Gülağaç");
            //    towns.Add(new Guid("x"), "Eskil");
            //    towns.Add(new Guid("x"), "Sarıyahşi");
            //    towns.Add(new Guid("x"), "Aksaray Merkez");
            //    towns.Add(new Guid("x"), "Ağaçören");

            






            //cities.Add(new Guid("035D5276-22A9-4545-BF89-4F032956B620"), "Amasya");
            //    towns.Add(new Guid("x"), "Hamamözü");
            //    towns.Add(new Guid("x"), "Taşova");
            //    towns.Add(new Guid("x"), "Gümüşhacıköy");
            //    towns.Add(new Guid("x"), "Amasya Merkez");
            //    towns.Add(new Guid("x"), "Suluova");
            //    towns.Add(new Guid("x"), "Merzifon");
            //    towns.Add(new Guid("x"), "Göynücek");

            



























            //cities.Add(new Guid("6BDBD58A-7306-4DF6-9C7C-1DD7C0A17FDA"), "Ankara");
            //    towns.Add(new Guid("x"), "Kazan");
            //    towns.Add(new Guid("x"), "Kahramankazan");
            //    towns.Add(new Guid("x"), "Pursaklar");
            //    towns.Add(new Guid("x"), "Çubuk");
            //    towns.Add(new Guid("x"), "Kızılcahamam");
            //    towns.Add(new Guid("x"), "Nallıhan");
            //    towns.Add(new Guid("x"), "Polatlı");
            //    towns.Add(new Guid("x"), "Akyurt");
            //    towns.Add(new Guid("x"), "Bala");
            //    towns.Add(new Guid("x"), "Çamlıdere");
            //    towns.Add(new Guid("x"), "Şereflikoçhisar");
            //    towns.Add(new Guid("x"), "Haymana");
            //    towns.Add(new Guid("x"), "Ayaş");
            //    towns.Add(new Guid("x"), "Altındağ");
            //    towns.Add(new Guid("x"), "Elmadağ");
            //    towns.Add(new Guid("x"), "Etimesgut");
            //    towns.Add(new Guid("x"), "Beypazarı");
            //    towns.Add(new Guid("x"), "Kalecik");
            //    towns.Add(new Guid("x"), "Çankaya");
            //    towns.Add(new Guid("x"), "Keçiören");
            //    towns.Add(new Guid("x"), "Mamak");
            //    towns.Add(new Guid("x"), "Evren");
            //    towns.Add(new Guid("x"), "Gölbaşı");
            //    towns.Add(new Guid("x"), "Sincan");
            //    towns.Add(new Guid("x"), "Yenimahalle");
            //    towns.Add(new Guid("x"), "Güdül");
            //    towns.Add(new Guid("x"), "Sincan(Temelli)");
            



















            //cities.Add(new Guid("84248BAA-501A-4DD9-BD94-D1F89A9CC130"), "Antalya");
            //    towns.Add(new Guid("x"), "Demre");
            //    towns.Add(new Guid("x"), "Gazipaşa");
            //    towns.Add(new Guid("x"), "Konyaaltı");
            //    towns.Add(new Guid("x"), "Kemer");
            //    towns.Add(new Guid("x"), "Akseki");
            //    towns.Add(new Guid("x"), "Döşemealtı");
            //    towns.Add(new Guid("x"), "Gündoğmuş");
            //    towns.Add(new Guid("x"), "Elmalı");
            //    towns.Add(new Guid("x"), "Muratpaşa");
            //    towns.Add(new Guid("x"), "Kepez");
            //    towns.Add(new Guid("x"), "Kaş");
            //    towns.Add(new Guid("x"), "Kumluca");
            //    towns.Add(new Guid("x"), "Aksu");
            //    towns.Add(new Guid("x"), "Finike");
            //    towns.Add(new Guid("x"), "İbradı");
            //    towns.Add(new Guid("x"), "Manavgat");
            //    towns.Add(new Guid("x"), "Korkuteli");
            //    towns.Add(new Guid("x"), "Serik");
            //    towns.Add(new Guid("x"), "Alanya");
            






            //cities.Add(new Guid("E38F265D-CA51-43F1-B897-3B3C8277736F"), "Ardahan");
            //    towns.Add(new Guid("x"), "Damal");
            //    towns.Add(new Guid("x"), "Hanak");
            //    towns.Add(new Guid("x"), "Posof");
            //    towns.Add(new Guid("x"), "Çıldır");
            //    towns.Add(new Guid("x"), "Ardahan Merkez");
            //    towns.Add(new Guid("x"), "Göle");

            








            //cities.Add(new Guid("2ACD65FB-A905-430D-8464-14F582132ECA"), "Artvin");
            //    towns.Add(new Guid("x"), "Şavşat");
            //    towns.Add(new Guid("x"), "Borçka");
            //    towns.Add(new Guid("x"), "Yusufeli");
            //    towns.Add(new Guid("x"), "Arhavi");
            //    towns.Add(new Guid("x"), "Hopa");
            //    towns.Add(new Guid("x"), "Artvin Merkez");
            //    towns.Add(new Guid("x"), "Ardanuç");
            //    towns.Add(new Guid("x"), "Murgul");
          
            


















            //cities.Add(new Guid("517982B9-8651-4F61-966C-69BC2E7B35FD"), "Aydin");
            //    towns.Add(new Guid("x"), "Yenipazar");
            //    towns.Add(new Guid("x"), "Sultanhisar");
            //    towns.Add(new Guid("x"), "Kuyucak");
            //    towns.Add(new Guid("x"), "İncirliova");
            //    towns.Add(new Guid("x"), "Köşk");
            //    towns.Add(new Guid("x"), "Karacasu");
            //    towns.Add(new Guid("x"), "Söke");
            //    towns.Add(new Guid("x"), "Germencik");
            //    towns.Add(new Guid("x"), "Efeler");
            //    towns.Add(new Guid("x"), "Aydın Merkez");
            //    towns.Add(new Guid("x"), "Didim");
            //    towns.Add(new Guid("x"), "Kuşadası");
            //    towns.Add(new Guid("x"), "Koçarlı");
            //    towns.Add(new Guid("x"), "Bozdoğan");
            //    towns.Add(new Guid("x"), "Karpuzlu");
            //    towns.Add(new Guid("x"), "Buharkent");
            //    towns.Add(new Guid("x"), "Çine");
            //    towns.Add(new Guid("x"), "Nazilli");
           

            





















            //cities.Add(new Guid("BABB1CBC-AE2F-4009-9AB5-484A7766EE4A"), "Balikesir");
            //    towns.Add(new Guid("x"), "Burhaniye");
            //    towns.Add(new Guid("x"), "Edremit");
            //    towns.Add(new Guid("x"), "Balya");
            //    towns.Add(new Guid("x"), "Marmara");
            //    towns.Add(new Guid("x"), "İvrindi");
            //    towns.Add(new Guid("x"), "Bigadiç");
            //    towns.Add(new Guid("x"), "Erdek");
            //    towns.Add(new Guid("x"), "Susurluk");
            //    towns.Add(new Guid("x"), "Ayvalık");
            //    towns.Add(new Guid("x"), "Sındırgı");
            //    towns.Add(new Guid("x"), "Gönen");
            //    towns.Add(new Guid("x"), "Savaştepe");
            //    towns.Add(new Guid("x"), "Karesi");
            //    towns.Add(new Guid("x"), "Gömeç");
            //    towns.Add(new Guid("x"), "Balıkesir Merkez");
            //    towns.Add(new Guid("x"), "Manyas");
            //    towns.Add(new Guid("x"), "Altıeylül");
            //    towns.Add(new Guid("x"), "Dursunbey");
            //    towns.Add(new Guid("x"), "Havran");
            //    towns.Add(new Guid("x"), "Kepsut");
            //    towns.Add(new Guid("x"), "Bandırma");
            




            //cities.Add(new Guid("54DBF968-7BE9-4DD5-AA64-30088DA21E19"), "Bartin");
            //    towns.Add(new Guid("x"), "Kurucaşile");
            //    towns.Add(new Guid("x"), "Bartın Merkez");
            //    towns.Add(new Guid("x"), "Ulus");
            //    towns.Add(new Guid("x"), "Amasra");

            






            //cities.Add(new Guid("83D6547C-A7D8-4A1A-8C26-CB1BFB3DC94D"), "Batman");
            //    towns.Add(new Guid("x"), "Beşiri");
            //    towns.Add(new Guid("x"), "Kozluk");
            //    towns.Add(new Guid("x"), "Batman Merkez");
            //    towns.Add(new Guid("x"), "Sason");
            //    towns.Add(new Guid("x"), "Hasankeyf");
            //    towns.Add(new Guid("x"), "Gercüş");
            
            



            //cities.Add(new Guid("B75711E6-4228-4D0A-AC86-ACD0EACE0527"), "Bayburt");
            //    towns.Add(new Guid("x"), "Aydıntepe");
            //    towns.Add(new Guid("x"), "Demirözü");
            //    towns.Add(new Guid("x"), "Bayburt Merkez");

            








            //cities.Add(new Guid("4422AD33-3F8B-49BD-9768-F783358A2F77"), "Bilecik");
            //    towns.Add(new Guid("x"), "Bilecik Merkez");
            //    towns.Add(new Guid("x"), "Osmaneli");
            //    towns.Add(new Guid("x"), "Gölpazarı");
            //    towns.Add(new Guid("x"), "Söğüt");
            //    towns.Add(new Guid("x"), "Bozüyük");
            //    towns.Add(new Guid("x"), "İnhisar");
            //    towns.Add(new Guid("x"), "Yenipazar");
            //    towns.Add(new Guid("x"), "Pazaryeri");
            








            //cities.Add(new Guid("A44354C2-36DF-4FE4-8F27-573A305F4B2A"), "Bingöl");
            //    towns.Add(new Guid("x"), "Karlıova");
            //    towns.Add(new Guid("x"), "Genç");
            //    towns.Add(new Guid("x"), "Yedisu");
            //    towns.Add(new Guid("x"), "Kiğı");
            //    towns.Add(new Guid("x"), "Adaklı");
            //    towns.Add(new Guid("x"), "Solhan");
            //    towns.Add(new Guid("x"), "Bingöl Merkez");
            //    towns.Add(new Guid("x"), "Yayladere");
            







            //cities.Add(new Guid("1382F76E-1E9B-4F02-ADCE-6DED9BC3D997"), "Bitlis");
            //    towns.Add(new Guid("x"), "Bitlis Merkez");
            //    towns.Add(new Guid("x"), "Ahlat");
            //    towns.Add(new Guid("x"), "Tatvan");
            //    towns.Add(new Guid("x"), "Adilcevaz");
            //    towns.Add(new Guid("x"), "Mutki");
            //    towns.Add(new Guid("x"), "Hizan");
            //    towns.Add(new Guid("x"), "Güroymak");
            









            //cities.Add(new Guid("6E3981F7-5765-452C-A927-7F1FB7AA97AA"), "Bolu");
            //    towns.Add(new Guid("x"), "Göynük");
            //    towns.Add(new Guid("x"), "Dörtdivan");
            //    towns.Add(new Guid("x"), "Yeniçağa");
            //    towns.Add(new Guid("x"), "Mudurnu");
            //    towns.Add(new Guid("x"), "Kıbrıscık");
            //    towns.Add(new Guid("x"), "Gerede");
            //    towns.Add(new Guid("x"), "Mengen");
            //    towns.Add(new Guid("x"), "Bolu Merkez");
            //    towns.Add(new Guid("x"), "Seben");
            











            //cities.Add(new Guid("E08F37F3-E758-43C3-BF4D-C55230E16F2F"), "Burdur");
            //    towns.Add(new Guid("x"), "Bucak");
            //    towns.Add(new Guid("x"), "Tefenni");
            //    towns.Add(new Guid("x"), "Burdur Merkez");
            //    towns.Add(new Guid("x"), "Gölhisar");
            //    towns.Add(new Guid("x"), "Yeşilova");
            //    towns.Add(new Guid("x"), "Çeltikçi");
            //    towns.Add(new Guid("x"), "ALTINYAYLA(DİRMIL)");
            //    towns.Add(new Guid("x"), "Kemer");
            //    towns.Add(new Guid("x"), "Karamanlı");
            //    towns.Add(new Guid("x"), "Ağlasun");
            //    towns.Add(new Guid("x"), "Çavdır");
            

















            //cities.Add(new Guid("6AC6CF39-15B4-46A4-8416-16F12E08C943"), "Bursa");
            //    towns.Add(new Guid("x"), "Büyükorhan");
            //    towns.Add(new Guid("x"), "Osmangazi");
            //    towns.Add(new Guid("x"), "Gemlik");
            //    towns.Add(new Guid("x"), "Harmancık");
            //    towns.Add(new Guid("x"), "Orhaneli");
            //    towns.Add(new Guid("x"), "İnegöl");
            //    towns.Add(new Guid("x"), "Nilüfer");
            //    towns.Add(new Guid("x"), "Mustafakemalpaşa");
            //    towns.Add(new Guid("x"), "Yıldırım");
            //    towns.Add(new Guid("x"), "Kestel");
            //    towns.Add(new Guid("x"), "Keles");
            //    towns.Add(new Guid("x"), "Orhangazi");
            //    towns.Add(new Guid("x"), "Mudanya");
            //    towns.Add(new Guid("x"), "Gürsu");
            //    towns.Add(new Guid("x"), "Karacabey");
            //    towns.Add(new Guid("x"), "İznik");
            //    towns.Add(new Guid("x"), "Yenişehir");

            //cities.Add(new Guid("633CF9EA-6B42-41A9-AD5C-7373488F9BA8"), "Çanakkale");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("D4807D29-A421-481A-A617-427F59490EB9"), "Çankiri");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("1C9AA770-82E3-4280-AEB7-DCA30B86FD0A"), "Çorum");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("BF51D9DE-2B9D-4621-AF55-F77C1E6BB994"), "Denizli");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("011B0174-581A-47B3-A919-829803611AD9"), "Diyarbakir");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("ED5CB1FC-650C-44D0-8E2D-CC0486182FFF"), "Düzce");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("85A5D31B-D6DC-48DC-83D0-80AC2D760EE6"), "Edirne");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("09E4E7DC-98CE-4283-8766-BE3AF41EBF1F"), "Elazig");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("F50D4611-38F0-4AD2-BAA5-BFBBE78F5F76"), "Erzincan");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("E9D609D3-C6B9-4387-A6E1-EE17957A2DF8"), "Erzurum");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("B82127EA-FE38-4E9D-B8C0-3ADEF4731D30"), "Eskisehir");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("BB8492CC-857E-4C6B-A28F-9B6C2E066A99"), "Gaziantep");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("1A20CED7-B63D-4362-87BD-7AA6C03718F3"), "Giresun");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("9C1A65CA-A1D3-4B89-BD13-2F35251C01DE"), "Gümüshane");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("627527DC-D1A4-4DE8-A04F-A34E9A4C84AA"), "Hakkâri");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("002341E9-31CB-41E3-B1DF-039DB55C66DF"), "Hatay");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("21D9A185-29C6-4383-A723-E8F121F26DD3"), "Igdir");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //cities.Add(new Guid("9653E79F-C6F8-4BAA-A626-CE51804D0670"), "Isparta");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");
            //    towns.Add(new Guid("x"), "xxxxx");

            //cities.Add(new Guid("A5522F8C-6F4C-4CFE-B5E8-B74461CABFB7"), "Istanbul");
            //    inserTownsToCity(migrationBuilder, towns, new Guid("A5522F8C-6F4C-4CFE-B5E8-B74461CABFB7"));
            //    //istanbul towns
            //    towns.Add(new Guid("46AD83C2-16B2-4D64-B728-06400E3A3003"), "Basaksehir");
            //    towns.Add(new Guid("5A1C80FF-7DFD-45FE-8AFF-08BFE0DDA914"), "Çatalca");
            //    towns.Add(new Guid("17792749-22AA-44A0-B377-09920C5FE3E6"), "Bayrampasa");
            //    towns.Add(new Guid("AA33B185-5BB1-4A22-9178-0C12F8346098"), "Gaziosmanpasa");
            //    towns.Add(new Guid("303EC16F-F44C-4825-988C-16F4219397CC"), "Ümraniye");
            //    towns.Add(new Guid("00C5021A-9373-4839-8701-240205206AB7"), "Sisli");
            //    towns.Add(new Guid("A0199F11-2AEE-45B2-BBF6-32BDD9549FC2"), "Sultanbeyli");
            //    towns.Add(new Guid("FCAF9ED2-9A57-45B2-8D60-37094B2FC790"), "Adalar");
            //    towns.Add(new Guid("C4071272-00E3-4D4C-A1C8-46C3C70DCF8C"), "Eyüpsultan");
            //    towns.Add(new Guid("5337030B-C4BC-4C9F-BB71-507D4AE5AD00"), "Besiktas");
            //    towns.Add(new Guid("1E174D4D-E92D-4B99-BBEE-5160D83E9822"), "Tuzla");
            //    towns.Add(new Guid("85EF35DB-5FC8-4360-852F-52E399719096"), "Küçükçekmece");
            //    towns.Add(new Guid("9352FE0B-6E1C-41C6-9457-55240A184935"), "Üsküdar");
            //    towns.Add(new Guid("A1CEF825-F1EA-40E1-9D60-5BFB010CF3B2"), "Fatih");
            //    towns.Add(new Guid("B4E01120-2BDE-4E7F-9539-5ED72ACB4AC8"), "Silivri");
            //    towns.Add(new Guid("71C2CE35-4E4B-4962-909A-6038C70B5288"), "Kadiköy");
            //    towns.Add(new Guid("2FDF801A-4821-4875-A915-67352456E232"), "Atasehir");
            //    towns.Add(new Guid("900547DD-D91A-43F4-9490-676A7C97F240"), "Esenyurt");
            //    towns.Add(new Guid("16F74AB2-C7D4-4D24-AA59-69CC111483D7"), "Arnavutköy");
            //    towns.Add(new Guid("E02FE133-870A-4276-BEAA-8DDABD4DDEA6"), "Sile");
            //    towns.Add(new Guid("069B6D2A-C094-4415-8344-94A79BC9BB87"), "Esenler");
            //    towns.Add(new Guid("3AA1E505-5164-40C1-9336-9B2005ACDFF3"), "Kagithane");
            //    towns.Add(new Guid("799D48C8-776D-47F2-8A8E-9E4B9346A523"), "Sultangazi");
            //    towns.Add(new Guid("FDB063FD-E561-476A-B921-A50FA40058BB"), "Zeytinburnu");
            //    towns.Add(new Guid("F412179A-1781-4C77-91A6-ABBEA8C57827"), "Beyoglu");
            //    towns.Add(new Guid("FB80BCC9-8930-4003-B735-AEEB33DF311D"), "Avcilar");
            //    towns.Add(new Guid("E2888C18-BC5A-4FA4-8CA3-B6AA7A6BC599"), "Maltepe");
            //    towns.Add(new Guid("AF5A5F49-BEC7-4030-BA8E-B6D6B3DE36AF"), "Beykoz");
            //    towns.Add(new Guid("117F7A8B-1FFA-4FF9-B01E-B847EA50C995"), "Güngören");
            //    towns.Add(new Guid("0E1B04AB-C5E7-47AA-B9C0-BEF5FF940365"), "Bakirköy");
            //    towns.Add(new Guid("CA6D7DE5-D5FE-4032-96A0-C1F26BB626D8"), "Bahçelievler");
            //    towns.Add(new Guid("568A57F5-EEF0-4BE7-A0BC-C5D59AC950C3"), "Beylikdüzü");
            //    towns.Add(new Guid("FE559A7E-6092-437C-8760-C8BC8DFFE1EC"), "Sancaktepe");
            //    towns.Add(new Guid("EE57BA41-DAEA-4A9F-A3B3-CCD8AF59E082"), "Pendik");
            //    towns.Add(new Guid("D92928A5-8442-49D2-BC69-D02DC430DAAF"), "Büyükçekmece");
            //    towns.Add(new Guid("982E10F6-7D63-43C5-824C-DFC69EEF6D1D"), "Çekmeköy");
            //    towns.Add(new Guid("F1A9AD9A-C139-4C9E-B19F-EE048D27DFAD"), "Sariyer");
            //    towns.Add(new Guid("00B467BF-090E-454A-8956-F98C82FE1FF1"), "Bagcilar");
            //    towns.Add(new Guid("136648B7-7111-4103-9C6B-FEC65A68724C"), "Kartal");

            //cities.Add(new Guid("C978FF08-A3F8-4FDA-9E4F-55024EBFA7B6"), "Izmir");
            //cities.Add(new Guid("2A3578A9-4736-43E0-875D-A228A408F274"), "Kahramanmaras");
            //cities.Add(new Guid("F34BAB24-E42D-4AAC-9608-0C810A730668"), "Karabük");
            //cities.Add(new Guid("B2693E98-778B-46AB-853C-0D321A198EF3"), "Karaman");
            //cities.Add(new Guid("233C6068-1C94-4A5E-9198-40927B2C0563"), "Kars");
            //cities.Add(new Guid("DBA06D1B-949A-43B6-903F-BDD2AEC66768"), "Kastamonu");
            //cities.Add(new Guid("E2857F4F-4325-4B92-89D3-7408329AA5EA"), "Kayseri");
            //cities.Add(new Guid("60E8570B-201C-4C31-8B48-C7CD6C5383E7"), "Kilis");
            //cities.Add(new Guid("A82EAC68-9641-428E-82C5-35147BD56578"), "Kirikkale");
            //cities.Add(new Guid("7AC64D0A-79B5-4887-A32E-DEF1F1843B1C"), "Kirklareli");
            //cities.Add(new Guid("77647201-6A8B-4390-BBF8-471337DA6E88"), "Kirsehir");
            //cities.Add(new Guid("AE1A06FA-8045-46FD-A1A3-B95B9B1C429A"), "Kocaeli");
            //cities.Add(new Guid("76A14E8A-EB9B-42F1-8E12-9D8DC864B1C6"), "Konya");
            //cities.Add(new Guid("0B9035D4-FF01-470A-8906-2D43C1C09863"), "Kütahya");
            //cities.Add(new Guid("17C5BF84-BDAB-4F37-AD11-FF97FC2F4120"), "Malatya");
            //cities.Add(new Guid("2C4079AC-07CA-4880-8565-D2D52A92D208"), "Manisa");
            //cities.Add(new Guid("0CCCB927-71C6-4B23-9C9B-4A6A5E57CD33"), "Mardin");
            //cities.Add(new Guid("88310C6A-563A-40EA-B713-A45DD3B7426F"), "Mersin");
            //cities.Add(new Guid("93B2CA36-1957-4FD7-ACF2-E84E58E473D1"), "Mugla");
            //cities.Add(new Guid("38CCE807-1B5E-49A5-B664-0D5BB1E95E65"), "Mus");
            //cities.Add(new Guid("2237C918-866F-4ADD-B865-28D2ED64173C"), "Nevsehir");
            //cities.Add(new Guid("19096911-55AE-4615-9A5B-4538D165BAEC"), "Nigde");
            //cities.Add(new Guid("77879F28-C5FF-4849-B04E-D6AA9738AACA"), "Ordu");
            //cities.Add(new Guid("830EFE51-BE02-44C1-9B4B-67D7DF66AC92"), "Osmaniye");
            //cities.Add(new Guid("13AA008F-43B5-43B7-BF54-D50A26DD05EB"), "Rize");
            //cities.Add(new Guid("DBA3C927-5DFD-4AD2-A202-49E62775DB52"), "Sakarya");
            //cities.Add(new Guid("A8E35E87-3F1C-4FC6-9DE2-AC290FE3283F"), "Samsun");
            //cities.Add(new Guid("44F41FC7-4ECA-46C7-9EBD-899BFF3487D4"), "Sanliurfa");
            //cities.Add(new Guid("CF161B90-CFDD-4B1C-8CBE-61EA1BB0082F"), "Siirt");
            //cities.Add(new Guid("CF8D31C6-112F-4FC6-9FD7-5992A7D3FD9A"), "Sinop");
            //cities.Add(new Guid("DAD970B6-4670-4E44-8057-6F723FDA1CF4"), "Sirnak");
            //cities.Add(new Guid("C024B108-79A8-40EA-ACCF-D9F7D7B15DB3"), "Sivas");
            //cities.Add(new Guid("3EC80199-D912-42A8-B221-4B435F4982E4"), "Tekirdag");
            //cities.Add(new Guid("D3A27944-6404-4F10-AAE2-94FA38DDE42C"), "Tokat");
            //cities.Add(new Guid("E0434953-6EAB-4239-9186-07DA662A9AAB"), "Trabzon");
            //cities.Add(new Guid("546AE5D8-3354-46F1-A8DE-EF2A020D681F"), "Tunceli");
            //cities.Add(new Guid("C656E6CC-010E-4D16-9F5C-2F2BFAE7B78D"), "Usak");
            //cities.Add(new Guid("4422449D-33F9-45AC-BB9A-EB2BFD283165"), "Van");
            //cities.Add(new Guid("DFF477C0-7BAD-4804-9654-D7AF79D58564"), "Yalova");
            //cities.Add(new Guid("49DF0B0E-3EC6-4B25-B21E-9256CC1C8205"), "Yozgat");
            //cities.Add(new Guid("F7EFBF1E-C21A-4191-8902-099A7FE7B3F7"), "Zonguldak");
            




            



        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }

        public void insertCityToCountry(MigrationBuilder migrationBuilder, Guid cityId, string cityName, Guid CountryId)
        {
            string insertQuery = $@"INSERT INTO[areas].[CityLookups]
                (      
                       [Id]
                      ,[Name]
                      ,[CountryId]
                )
                 VALUES
                 (
                         '{cityId.ToString()}'
                       , '{cityName}'
                       , {CountryId.ToString()}
                 )";

        }

        public void inserTownsToCity(MigrationBuilder migrationBuilder, Dictionary<Guid, string> towns, Guid cityId)
        {
            foreach (var town in towns)
            {
                string insertQuery = $@"INSERT INTO[areas].[DistrictLookups]
                (      
                       [Id]
                      ,[Name]
                      ,[CityId]
                )
                 VALUES
                 (
                         '{town.Key}'
                       , '{town.Value}'
                       , {cityId.ToString()}
                 )";

                migrationBuilder.Sql(insertQuery);
            }
        }

    }
}


/*
[
  "44a04add-fccd-4293-8c47-20ce47cbc56a",
  "7c257307-2aa5-40f5-a18a-661419ad2ddf",
  "8cc13ae9-57d1-4909-9e9e-54939b2a2ab3",
  "f750cb18-56d6-409e-ab0f-357e2dc69023",
  "51fe5970-6802-4578-abfd-d7a5f326cb6a",
  "6afd9239-08a1-4446-891a-d2263d722f17",
  "1be28de2-663c-4d93-aa6a-a9b69a7db52b",
  "1b718d17-b443-40db-a456-fccce0954979",
  "560d0ef8-0eb1-4725-bc0f-bade4bb9d365",
  "66b2289d-3b9e-4a9f-99f1-f364093b751e",
  "4b32d042-9b57-443c-b862-1b6f55c52744",
  "9f36773d-9cb6-4d0e-a5bb-41d66e1eb4cc",
  "321c6453-5556-40ad-aa9a-d202c2d0a669",
  "53d7fd4d-5a7a-4b39-8561-8c93729a6d72",
  "e8f946bc-f75a-47cb-8f16-6d6bde0a57a8",
  "840f1147-eede-4531-8c53-ca1a52f22b32",
  "415d9c83-17a6-49d0-821f-d01f0d853244",
  "69ef53ee-4db7-4952-90bf-f6b455f9275b",
  "cabf6774-903c-409e-b9c8-67d519cc92d8",
  "ac77688f-6afb-4ae0-b5dc-9367f985266e",
  "98a39b09-b703-404c-bd4c-4db2c1eab7cc",
  "6626919e-e268-43db-97ba-d4aac1ef78d5",
  "51320c7b-cc88-4119-9adc-202f8ef964ba",
  "336463f8-5c17-4bab-ad83-85ff96bb7f56",
  "ee7a3f5b-bbde-4e32-95ec-601f5e37bb66",
  "938cbd0b-0446-4d15-87b0-5cd4514455c5",
  "dcc40906-9556-4c97-931d-158fde155786",
  "66f53036-af01-48d7-aae7-9710b77758e3",
  "1e6d4d5e-8610-4961-9d67-4e1d76f44890",
  "f45463c7-6b10-41fc-85c8-79d301e8eaa9",
  "0cbf36cf-5875-476a-9fa6-476479f3e478",
  "60a0b2fc-a734-4cf6-8e65-0b6d71349ed4",
  "d86405b1-510c-4b0e-91a0-b6c8aaaa16df",
  "60b5d3c8-e006-4c40-91b6-f78df899371c",
  "4f8af794-2e86-4e4a-a290-1eb5b9464c17",
  "be523f13-d0c7-4ba8-8c7e-984b5170ef31",
  "8bcc3735-403e-454f-a7e6-5b0010515cfc",
  "cc18419f-b86a-464c-8ede-b52c04d09a23",
  "7946c133-91f8-4991-ad65-5e66b33fff5b",
  "1c6cc977-5c5e-48d7-a98c-076c844c30fe",
  "5a86d5f9-ce02-4aad-a677-def5f688a506",
  "45561daf-22d2-4880-b195-d134ba491d78",
  "3244e527-33c0-41cc-a2d2-2a9d1717b94f",
  "cd3ff980-f7a4-4a0e-bb78-24df1c471e0f",
  "e33d98ac-7c3a-44a6-ac11-248e29375faf",
  "719b937f-5a6e-44c6-bea5-c21006df25c9",
  "d6896384-277e-4dba-ba12-d56c74333805",
  "e1d37fb7-6555-445d-bebf-4e27a9e50f58",
  "0a919f4f-ff5a-426e-8210-5aabea46a548",
  "5221a387-9b28-4719-9274-6b2434cfd3ec",
  "25807eaf-ebe4-46ba-a5e3-68bf489f5dba",
  "5d58eea3-a4db-45cc-9dc2-567414e79aa7",
  "9ea2a10e-caf0-4489-a068-952a012d3bcd",
  "3748a748-adf6-49d8-a6a1-1e564d22e4bb",
  "eca14ea3-20f0-4d28-9d6d-7035ab587311",
  "92c66523-6a40-400a-8f28-e7eeafa79a35",
  "3b65d488-0650-425d-bb98-d7084d44f9a1",
  "f00f4404-ebe8-4fe6-8bb1-cfc1276f1610",
  "131e59fc-40ee-41a0-ba15-4188b4b9dd43",
  "ef69489a-ff7f-430c-8984-c8baa995942a",
  "b3547c44-1625-495f-b0f6-30b70cdc5e42",
  "5b7c7c75-6a8c-4bcb-be26-276376fd1b86",
  "f5f64db4-73a5-40c0-a93f-24f6b2a46bba",
  "b911bc6c-3c05-4e35-b7e4-85f9410a4b67",
  "c1ea9c4c-ccbb-4897-9889-6453031fba0b",
  "ffc899f0-e2a2-4e73-bae1-ddb3d2c074bd",
  "828dc1b4-bf45-4e2e-a781-7be711c98491",
  "cfd8980d-d24d-467a-a572-60db6490e868",
  "58761196-2556-470b-b2f1-66f811e08ca1",
  "f4dbe56b-1687-435d-9ae6-e5fab17fa39e",
  "9d3ab560-040e-4fa8-b270-dc297f51e3a1",
  "6c6a33e4-ca91-41de-bb11-1d67b7ad0c7a",
  "f901d9cf-97d8-4468-9a79-3b53d345c2ff",
  "afef6a0d-04dc-44da-9a61-8da66f543f30",
  "f731636e-52f7-41a8-8fed-6d3cf0589592",
  "a91596ad-4438-41e0-83b5-67c975826eee",
  "f67513ee-43a8-422d-95ec-62cf92a5da1c",
  "21e7f448-3067-4ed1-bdaf-7851bf98ddd1",
  "2fb52986-56c1-45a8-8fa6-59e1284bec93",
  "5d600f70-cee7-4a96-910d-26ebd477886e",
  "ae01f9a7-b0c0-458a-bd83-252b880eecc4",
  "15ff86e1-5dbb-4a24-ba3e-1653e9ecf669",
  "49d114e5-0de1-46bf-8689-f2c4880b931e",
  "05b2bc3f-9c9e-4b56-827a-5484c4570cac",
  "570e39c8-ebd1-4299-b390-d543fa03633f",
  "4f7e8296-4e52-49e5-80a0-87fd8b744fb7",
  "3262855a-0c7f-444e-9154-6ce420fcedd4",
  "3286238e-10ee-4a1d-bf3a-a7c274abf611",
  "1686a3b3-528d-4e66-a7bf-6ada048f2593",
  "40fd5fa0-a090-44b8-9e36-d0c2c3ef402f",
  "c79d4ac0-7285-4892-826a-267684126016",
  "139b19b8-2f0a-4405-a023-c976d78ab1cc",
  "bef9e44a-cc40-471a-a09b-526a62d41ec0",
  "0c3781ca-66fc-4aac-afed-cde264afb6b1",
  "ccc8042d-21b7-4c0c-891a-a372f487e099",
  "4a7e9c94-2666-4cb9-a066-ca8b07db1c9b",
  "78b5fd8c-172d-488d-ab6d-1dcd31346f07",
  "92e2c16a-50c1-4bbe-b804-98af0dff449c",
  "b077d4e0-dcb1-40c1-a260-68337021456c",
  "0552edd4-2c16-4db2-8ee3-229c58b0911f"
]
*/