﻿using System;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class ChangeApiResponse
    {
        public Guid Id { get; set; }
        public string Updated { get; set; }
    }
}
