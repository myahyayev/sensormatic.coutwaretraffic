﻿using System;

namespace CountwareTraffic.Services.Events.Application
{
    public class GetDeviceById : Convey.CQRS.Queries.IQuery<DeviceDto>
    {
        public Guid DeviceId { get; set; }
    }
}
