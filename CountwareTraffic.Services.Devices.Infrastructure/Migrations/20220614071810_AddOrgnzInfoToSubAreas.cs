﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Devices.Infrastructure.Migrations
{
    public partial class AddOrgnzInfoToSubAreas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CityId",
                schema: "devices",
                table: "SubAreas",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "CityName",
                schema: "devices",
                table: "SubAreas",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "CompanyId",
                schema: "devices",
                table: "SubAreas",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "CompanyLookupId",
                schema: "devices",
                table: "SubAreas",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                schema: "devices",
                table: "SubAreas",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "CountryId",
                schema: "devices",
                table: "SubAreas",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "CountryName",
                schema: "devices",
                table: "SubAreas",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "DistrictId",
                schema: "devices",
                table: "SubAreas",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "DistrictName",
                schema: "devices",
                table: "SubAreas",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "RegionId",
                schema: "devices",
                table: "SubAreas",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "RegionName",
                schema: "devices",
                table: "SubAreas",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CityId",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "CityName",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "CompanyLookupId",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "CountryId",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "CountryName",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "DistrictId",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "DistrictName",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "RegionId",
                schema: "devices",
                table: "SubAreas");

            migrationBuilder.DropColumn(
                name: "RegionName",
                schema: "devices",
                table: "SubAreas");
        }
    }
}
