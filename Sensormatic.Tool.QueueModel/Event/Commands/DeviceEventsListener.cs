﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceEventsListener : MessageEnvelope, IQueueCommand
    {
        public DeviceEventsListener(string description, Guid deviceId, int directionTypeId, int onTimeIOCount, DateTime eventDate, DateTime eventStartDate, DateTime eventEndDate, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            Description = description;
            DeviceId = deviceId;
            DirectionTypeId = directionTypeId;
            EventDate = eventDate; 
            EventStartDate = eventStartDate;
            EventEndDate = eventEndDate;
            OnTimeIOCount = onTimeIOCount;
        }

        public int OnTimeIOCount { get; }
        public string Description { get; }
        public Guid DeviceId { get; }
        public int DirectionTypeId { get; }
        public Guid RecordId { get; init; }
        public DateTime EventDate { get; }
        public DateTime EventStartDate { get; }
        public DateTime EventEndDate { get; }

    }
}
