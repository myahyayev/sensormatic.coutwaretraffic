﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class CompaniesRegionCreatedConsumer : IConsumer<Sensormatic.Tool.QueueModel.RegionCreated>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public CompaniesRegionCreatedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.RegionCreated queuEvent)
        {
            var regionCreated = _eventMapper.Map(queuEvent) as RegionCreated;
            await _eventDispatcher.PublishAsync(regionCreated);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
