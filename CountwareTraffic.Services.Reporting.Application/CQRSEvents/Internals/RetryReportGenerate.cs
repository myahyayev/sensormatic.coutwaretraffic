﻿using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class RetryReportGenerate : Convey.CQRS.Events.IEvent
    {
        public Guid ReportId { get; set; }
        public DateTime? DateRangeStartDate { get; set; }
        public DateTime? DateRangeEndDate { get; set; }
        public Guid? ReportFileId { get; set; }
    }
}
