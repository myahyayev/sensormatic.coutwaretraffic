﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceChanged : MessageEnvelope, IQueueEvent
    {
        public DeviceChanged(Guid deviceId, string name, string macAddress, string ipAddress, bool isActive, string oldName, int controlFrequency, Guid recordId, Guid userId, string correlationId, Guid tenantId) 
            : base(userId, correlationId, tenantId)
        {
            DeviceId = deviceId;
            Name = name;
            OldName = oldName;
            RecordId = recordId;
            MacAddress = macAddress;
            IpAddress = ipAddress;
            IsActive = isActive;
            ControlFrequency = controlFrequency;
        }
        public Guid DeviceId { get; }
        public string Name { get; }
        public string MacAddress { get; }
        public string IpAddress { get; }
        public string OldName { get; }
        public Guid RecordId { get; init; }
        public bool IsActive { get;}
        public int ControlFrequency { get; set; }
    }
}
