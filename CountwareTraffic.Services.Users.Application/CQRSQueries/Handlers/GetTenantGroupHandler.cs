﻿using Convey.CQRS.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class GetTenantGroupHandler : IQueryHandler<GetTenantGroup, IdentityTenantGroupResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetTenantGroupHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityTenantGroupResponse> HandleAsync(GetTenantGroup query)
        {
            return await _identityServerService.GetTenantGroupAsync();
        }
    }
}
