﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Web.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class AreasController : BaseController
    {
        private readonly AreaService _areaService;
        public AreasController(IServiceProvider provider, ILogger<AreasController> logger, IHttpContextAccessor contextAccessor, AreaService areaService)
            : base(provider, logger, contextAccessor.HttpContext) => _areaService = areaService;


        [HttpGet("{areaId}/detail"), ServiceLog]
        public async Task<ApiResponse<GetAreaDetails>> GetAreaDetailAsync(Guid areaId)
            => Response(await _areaService.GetAreaByIdAsync(areaId), ResultMessage.SuccessResultMessage);


        [HttpPost("{districtId}/details"), ServiceLog]
        public async Task<ApiPagedResponse<GetAreaDetails>> GetAreaDetailsAsync(Guid districtId, [FromBody] DataSourceApiRequest paging)
            => await _areaService.GetAreasAsync(districtId, paging);


        [HttpPost("{districtId}/add"), ServiceLog]
        public async Task<ApiResponse<AddApiResponse>> AddAreaAsync(Guid districtId, [FromForm] AddAreaApiRequest request)
        {
            await _areaService.AddAreaAsync(districtId, request);
            return Response(new AddApiResponse { Created = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpPut("{areaId}/change"), ServiceLog]
        public async Task<ApiResponse<ChangeApiResponse>> ChangeSubAreaAsync(Guid areaId, [FromForm] ChangeAreaApiRequest request)
        {
            await _areaService.ChangeAreaAsync(areaId, request);
            return Response(new ChangeApiResponse { Updated = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpDelete("{areaId}/delete"), ServiceLog]
        public async Task<ApiResponse<DeleteApiResponse>> DeleteSubAreaAsync(Guid areaId)
        {
            await _areaService.CancelAreaAsync(areaId);
            return Response(new DeleteApiResponse { Deleted = "Success" }, ResultMessage.SuccessResultMessage);
        }


        [HttpGet("types"), ServiceLog]
        public async Task<ApiResponse<IEnumerable<GetAreaType>>> GetAreaTypesAsync()
            => Response(await _areaService.GetAreaTypesAsync(), ResultMessage.SuccessResultMessage);
    }
}
