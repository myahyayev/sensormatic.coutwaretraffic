﻿using Countware.Traffic.CrossCC.PolicyProvider;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Api;
using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Controllers
{
    [Authorize]
    public class RegionsController : BaseController
    {
        private readonly RegionService _regionService;
        private readonly AreaService _areaService;
        public RegionsController(IServiceProvider provider, ILogger<RegionsController> logger, IHttpContextAccessor contextAccessor, RegionService regionService, AreaService areaService)
            : base(provider, logger, contextAccessor)
        {
            _regionService = regionService;
            _areaService = areaService;
        }


        [HttpGet("{regionId}/detail"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminRegionView, Permissions.RegionView })]
        public async Task<ApiResponse<GetRegionDetails>> GetRegionDetailAsync(Guid regionId)
            => Response(await _regionService.GetRegionByIdAsync(regionId), new ResultMessage("#S1110"));


        [HttpGet("{regionId}/countrylookupid"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminRegionView, Permissions.RegionView })]
        public async Task<ApiResponse<Guid>> GetRegionCountryIdAsync(Guid regionId)
            => Response(await _regionService.GetRegionCountryIdAsync(regionId), new ResultMessage("#S1111"));


        [HttpPost("{countryId}/details"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminRegionList })]
        public async Task<ApiPagedResponse<GetRegionDetails>> GetRegionDetailsAsync(Guid countryId, [FromBody] DataSourceApiRequest paging)
            => await _regionService.GetRegionsAsync(countryId, paging);


        [HttpPost("{countryId}/add"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminRegionAdd })]
        public async Task<ApiResponse<CrudApiResponse>> AddRegionAsync(Guid countryId, [FromForm] AddRegionApiRequest request)
        {
            await _regionService.AddRegionAsync(countryId, request);
            return Response(new CrudApiResponse {  IsSuccess = true, OperationType = OperationType.Create}, new ResultMessage("#S1112"));
        }


        [HttpPut("{regionId}/change"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminRegionEdit })]
        public async Task<ApiResponse<CrudApiResponse>> ChangeRegionAsync(Guid regionId, [FromForm] ChangeRegionApiRequest request)
        {
            await _regionService.ChangeRegionAsync(regionId, request);
            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Update }, new ResultMessage("#S1113"));
        }


        [HttpDelete("{regionId}/delete"), ServiceLog]
        [Permissions(Permissions = new[] { Permissions.AdminRegionDelete })]
        public async Task<ApiResponse<CrudApiResponse>> DeleteRegionAsync(Guid regionId)
        {
            var areaControl = await _areaService.GetAreasAsync(regionId, new DataSourceApiRequest());

            if (areaControl.TotalCount > 0)
            {
                return Response(new CrudApiResponse { IsSuccess = false, OperationType = OperationType.Delete, Id = regionId }, new ResultMessage()
                {
                    CallToActionType = CallToActionType.GoToAreas,
                    Description = $"#W1030 A area has been defined in the Region with id {regionId}.",
                    Title = $"#W1030 Region with id {regionId} cannot be deleted."
                });
            }

            await _regionService.CancelRegionAsync(regionId);

            return Response(new CrudApiResponse { IsSuccess = true, OperationType = OperationType.Delete, Id = regionId }, new ResultMessage()
            {
                CallToActionType = CallToActionType.Ok,
                Description = $"#S1114 Region with {regionId} id has been deleted.",
                Title = $"#S1114 Region with {regionId} id has been deleted."
            });
        }
    }
}
