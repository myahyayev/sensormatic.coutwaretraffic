﻿using System;

namespace CountwareTraffic.Services.Users.Application.Objects.Requests
{
    public class IdentityGetAdminUserByIdRequest
    {
        public Guid Id { get; set; }
    }
}
