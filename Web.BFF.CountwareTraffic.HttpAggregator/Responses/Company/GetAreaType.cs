﻿namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class GetAreaType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
