﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Users.Application.Objects.Responses;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application.CQRSQueries.Handlers
{
    public class GetAdminPermissionSelectHandler
        : IQueryHandler<GetAdminPermissionSelect, GetAdminPermissionSelectResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public GetAdminPermissionSelectHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;


        public Task<GetAdminPermissionSelectResponse> HandleAsync(GetAdminPermissionSelect query)
        {
            return _identityServerService.GetAdminPermissionSelectAsync();
        }
    }
}
