﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Reporting.Application
{
    public class GetReportTemplateView : IQuery<ReportTemplateViewDto>
    {
        public Guid ReportTemplateId { get; set; }
    }
}
