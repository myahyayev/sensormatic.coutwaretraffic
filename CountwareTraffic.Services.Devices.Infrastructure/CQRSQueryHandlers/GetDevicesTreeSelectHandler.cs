﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using CountwareTraffic.Services.Devices.Core;
using Sensormatic.Tool.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Infrastructure
{
    public class GetDevicesTreeSelectHandler : IQueryHandler<GetDevicesTreeSelect, PagingResult<DeviceDetailsDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTenant _currentTenant;
        public GetDevicesTreeSelectHandler(IUnitOfWork unitOfWork, ICurrentTenant currentTenant)
        {
            _unitOfWork = unitOfWork;
            _currentTenant = currentTenant;
        }


        public async Task<PagingResult<DeviceDetailsDto>> HandleAsync(GetDevicesTreeSelect query)
        {
            int page = query.PagingQuery.Page;
            int limit = query.PagingQuery.Limit;

            var nodeFilters = query.NodeFilters.Select(u => new Core.NodeFilter
            {
                HierarchyLevel = u.HierarchyLevel,
                Id = u.Id
            }).ToList();

            var qres = await _unitOfWork.GetRepository<IDeviceRepository>()
                                        .GetDevicesTreeSelectAsync(nodeFilters, _currentTenant.Id, query.PagingQuery, query.Filters, query.Sorts);

            if (qres == null)
                return PagingResult<DeviceDetailsDto>.Empty;

            return new PagingResult<DeviceDetailsDto>(qres.Entities.Select(device => new DeviceDetailsDto
            {
                Id = device.Id,
                Name = device.Name,
                Description = device.Description,
                AuditCreateBy = device.AuditCreateBy,
                AuditCreateDate = device.AuditCreateDate,
                AuditModifiedBy = device.AuditModifiedBy,
                AuditModifiedDate = device.AuditModifiedDate,
                DeviceTypeName = DeviceType.FromValue<DeviceType>(device.DeviceTypeId).Name,
                DeviceStatusName = DeviceStatus.FromValue<DeviceStatus>(device.DeviceStatusId).Name,
                SubAreaId = device.SubAreaId,
                DeviceStatusId = device.DeviceStatusId,
                DeviceTypeId = device.DeviceTypeId,
                Identity = device.ConnectionInfo_Identity,
                IpAddress = device.ConnectionInfo_IpAddress,
                MacAddress = device.ConnectionInfo_MacAddress,
                BrandId = device.BrandId,
                BrandName = device.BrandName,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.FirmWare,
                IsActive = device.IsActive,
                Note = device.Note,
                ModelId = device.ModelId,
                ModelName = device.ModelName,
                Password = device.ConnectionInfo_Password,
                Port = device.ConnectionInfo_Port.HasValue ? device.ConnectionInfo_Port.Value : 0,
                UniqueId = device.ConnectionInfo_UniqueId
            }), qres.Total, page, limit, qres.Total > (limit * (page - 1)) + qres.Entities.Count);
        }
    }
}
