﻿using Sensormatic.Tool.Queue;
using System;

namespace Sensormatic.Tool.QueueModel
{
    public record DeviceCreatedRejected : MessageEnvelope, IQueueEvent
    {
        public DeviceCreatedRejected(Guid deviceId, string name, Guid userId, string correlationId, Guid tenantId)
            : base(userId, correlationId, tenantId)
        {
            DeviceId = deviceId;
            Name = name;
        }
        public Guid DeviceId { get; }
        public string Name { get; }
        public Guid RecordId { get; init; }
    }
}
