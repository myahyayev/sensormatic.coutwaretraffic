﻿using Sensormatic.Tool.Efcore;
using System;

namespace CountwareTraffic.Services.Companies.Core
{
    public class AreaTypeChanged : IDomainEvent
    {
        public Area Area { get; }
        public int AreaTypeId { get; }
        public Guid RecordId { get; init; }

        public AreaTypeChanged(Area area, int areaTypeId)
        {
            Area = area;
            AreaTypeId = areaTypeId;
            RecordId = Guid.NewGuid();
        }
    }

    public class AreaCreated : IDomainEvent
    {
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid RecordId { get; init; }
        public TimeSpan? WorkingHoursStart { get; }
        public TimeSpan? WorkingHoursEnd { get; }
        public string WorkingTimeZone { get; set; }

        public AreaCreated(Guid areaId, string areaName, TimeSpan? workingHoursStart, TimeSpan? workingHoursEnd, string workingTimeZone)
        {
            AreaId = areaId;
            AreaName = areaName;
            RecordId = Guid.NewGuid();
            WorkingHoursStart = workingHoursStart;
            WorkingHoursEnd = workingHoursEnd;
            WorkingTimeZone = workingTimeZone;
        }

    }

    public class AreaChanged : IDomainEvent
    {
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public string OldAreaName { get; }
        public Guid RecordId { get; init; }
        public TimeSpan? WorkingHoursStart { get; }
        public TimeSpan? OldWorkingHoursStart { get; }
        public TimeSpan? WorkingHoursEnd { get; }
        public TimeSpan? OldWorkingHoursEnd { get; }
        public string WorkingTimeZone { get; set; }
        public string OldWorkingTimeZone { get; set; }
        public AreaChanged(Guid areaId, string areaName, string oldAreaName, TimeSpan? workingHoursStart, TimeSpan? oldWorkingHoursStart, TimeSpan? workingHoursEnd, TimeSpan? oldWorkingHoursEnd, string workingTimeZone, string oldWorkingTimeZone)
        {
            AreaId = areaId;
            AreaName = areaName;
            OldAreaName = oldAreaName;
            RecordId = Guid.NewGuid();
            WorkingHoursStart = workingHoursStart;
            OldWorkingHoursStart = oldWorkingHoursStart;
            WorkingHoursEnd = workingHoursEnd;
            OldWorkingHoursEnd = oldWorkingHoursEnd;
            WorkingTimeZone = workingTimeZone;
            OldWorkingTimeZone = oldWorkingTimeZone;
        }

    }

    public class AreaDeleted : IDomainEvent
    {
        public Guid AreaId { get; }
        public Guid RecordId { get; init; }

        public AreaDeleted(Guid areaId)
        {
            AreaId = areaId;
            RecordId = Guid.NewGuid();
        }
    }

    public class AreaHierarchyBase
    {
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid RegionId { get; set; }
        public Guid CountryId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid TenantId { get; set; }
        public Guid RecordId { get; init; }

        public AreaHierarchyBase(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, string areaName)
        {
            AreaId = areaId;
            AreaName = areaName;
            RegionId = regionId;
            CompanyId = companyId;
            RecordId = Guid.NewGuid();
            TenantId = tenantId;
            CountryId = countryId;
        }
    }

    public class AreaHierarchyUpdated : AreaHierarchyBase, IDomainEvent
    {
        public AreaHierarchyUpdated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, string areaName)
             : base(tenantId, companyId, countryId, regionId, areaId, areaName)
        {
        }
    }

    public class AreaHierarchyDeleted : AreaHierarchyBase, IDomainEvent
    {
        public AreaHierarchyDeleted(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, string areaName)
             : base(tenantId, companyId, countryId, regionId, areaId, areaName)
        {
        }
    }

    public class AreaHierarchyCreated : AreaHierarchyBase, IDomainEvent
    {
        public AreaHierarchyCreated(Guid tenantId, Guid companyId, Guid countryId, Guid regionId, Guid areaId, string areaName)
            : base(tenantId, companyId, countryId, regionId, areaId, areaName)
        {
        }
    }
}
