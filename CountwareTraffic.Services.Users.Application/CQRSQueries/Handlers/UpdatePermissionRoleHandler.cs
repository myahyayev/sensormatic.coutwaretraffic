﻿using Convey.CQRS.Queries;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Users.Application
{
    public class UpdatePermissionRoleHandler : IQueryHandler<UpdatePermissionRole, IdentityUpdatePermissionRoleResponse>
    {
        private readonly IIdentityServerService _identityServerService;

        public UpdatePermissionRoleHandler(IIdentityServerService identityServerService)
            => _identityServerService = identityServerService;

        public async Task<IdentityUpdatePermissionRoleResponse> HandleAsync(UpdatePermissionRole query)
        {
            return await _identityServerService.UpdatePermissionRoleAsync(new IdentityUpdatePermissionRoleRequest
            {
                Id = query.Id,
                PermissionId = query.PermissionId,
                RoleId = query.RoleId,
                StartDate = query.StartDate,
                EndDate = query.EndDate
            });
        }
    }
}
