﻿using Sensormatic.Tool.Ioc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Events.Core
{
    public interface IUnknownDeviceRepository : IRepository<UnknownDevice>, IScopedDependency
    {
        Task<UnknownDevice> GetByIPAddressAndMacAsync(string ipAddress, string macAddress);
        Task<bool> ExistsAsync(string ipAddress, string macAddress);
        Task<IEnumerable<UnknownDevice>> GetUnusedAsync();
    }
}
