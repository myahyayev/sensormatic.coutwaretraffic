﻿using Convey.CQRS.Events;
using CountwareTraffic.Services.Companies.Application;
using Sensormatic.Tool.Ioc;
using Sensormatic.Tool.Queue;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Api
{
    public class AreaCreatedCompletedConsumer : IConsumer<Sensormatic.Tool.QueueModel.AreaCreatedCompleted>, ITransientSelfDependency
    {
        private readonly IEventDispatcher _eventDispatcher;
        private readonly IEventMapper _eventMapper;
        public AreaCreatedCompletedConsumer(IEventDispatcher eventDispatcher, IEventMapper eventMapper)
        {
            _eventDispatcher = eventDispatcher;
            _eventMapper = eventMapper;
        }
        public async Task ConsumeAsync(Sensormatic.Tool.QueueModel.AreaCreatedCompleted queuEvent)
        {
            var areaCreatedCompleted = _eventMapper.Map(queuEvent) as AreaCreatedCompleted;
            await _eventDispatcher.PublishAsync(areaCreatedCompleted);
        }

        public void Dispose() => System.GC.SuppressFinalize(this);
    }
}
