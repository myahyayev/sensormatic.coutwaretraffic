﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class DistrictLookupNotFoundException : AppException
    {
        public Guid Id { get; }
        public DistrictLookupNotFoundException(Guid id)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1404 District with id: {id} was not found") }, 404, ResponseMessageType.Error)
        {
            Id = id;
        }
    }
}
