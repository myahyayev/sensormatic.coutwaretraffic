﻿using System.Text.Json.Serialization;

namespace Web.BFF.CountwareTraffic.HttpAggregator
{
    public class ResetPasswordApiResponse
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
