﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Core
{
    public class RegionNotFoundException : DomainException
    {
        public Guid Id { get; }

        public RegionNotFoundException(Guid id)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1393 Region with id: {id} not found.") }, 404, ResponseMessageType.Error)
        {
            Id = id;
        }
    }
}
