﻿using System.Text.Json.Serialization;

namespace CountwareTraffic.Services.Users.Application
{
    public class IdentityResetPasswordWithTokenRequest
    {
        [JsonPropertyName("token")]
        public string Token { get; set; }


        [JsonPropertyName("userName")]
        public string UserName { get; set; }


        [JsonPropertyName("password")]
        public string Password { get; set; }


        [JsonPropertyName("confirmPassword")]
        public string ConfirmPassword { get; set; }
    }
}
