﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public interface IDeviceNotificationsHub
    {
        Task JoinDevicesProcess(string groupName);
        Task DeviceDeletedFailedReceived(DeviceDeletedFailed command);
        Task DeviceDeletedSuccessfullyReceived(DeviceDeletedSuccessfully command);
        Task DeviceChangedFailedReceived(DeviceChangedFailed command);
        Task DeviceChangedSuccessfullyReceived(DeviceChangedSuccessfully command);
        Task DeviceCreatedFailedReceived(DeviceCreatedFailed command);
        Task DeviceCreatedSuccessfullyReceived(DeviceCreatedSuccessfully command);
        Task DeviceStatusChangedSuccessfullyReceived(DeviceStatusChangeSuccessfully command);
    }

    //[Authorize]
    public class DeviceNotificationsHub : Hub<IDeviceNotificationsHub>
    {
        public async Task JoinDevicesProcess(string groupName)
            => await Groups.AddToGroupAsync(Context.ConnectionId, groupName);

        public async Task DeviceDeletedFailedReceived(DeviceDeletedFailed command)
            => await Clients.Group(command.TenantId.ToString()).DeviceDeletedFailedReceived(command);

        public async Task DeviceDeletedSuccessfullyReceived(DeviceDeletedSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).DeviceDeletedSuccessfullyReceived(command);

        public async Task DeviceChangedFailedReceived(DeviceChangedFailed command)
            => await Clients.Group(command.TenantId.ToString()).DeviceChangedFailedReceived(command);

        public async Task DeviceChangedSuccessfullyReceived(DeviceChangedSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).DeviceChangedSuccessfullyReceived(command);

        public async Task DeviceCreatedFailedReceived(DeviceCreatedFailed command)
            => await Clients.Group(command.TenantId.ToString()).DeviceCreatedFailedReceived(command);

        public async Task DeviceCreatedSuccessfullyReceived(DeviceCreatedSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).DeviceCreatedSuccessfullyReceived(command);

        public async Task DeviceStatusChangedSuccessfullyReceived(DeviceStatusChangeSuccessfully command)
            => await Clients.Group(command.TenantId.ToString()).DeviceStatusChangedSuccessfullyReceived(command);
    }
}
