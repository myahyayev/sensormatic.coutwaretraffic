﻿using Convey.CQRS.Queries;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetCountryLookups : IQuery<List<CountryLookupDetailsDto>>
    {
    }
}
