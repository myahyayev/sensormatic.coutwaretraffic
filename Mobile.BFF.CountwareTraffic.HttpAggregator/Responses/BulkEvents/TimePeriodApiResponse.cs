﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class TimePeriodApiResponse
    {
            public Guid AreaId { get; set; }
            public string AreaName { get; set; }
            public Guid SubAreaId { get; set; }
            public string SubAreaName { get; set; }
            public DateTime Date { get; set; }
            public int EnterCount { get; set; }
            public int ExitCount { get; set; }
    }
}
