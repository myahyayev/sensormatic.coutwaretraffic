﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountwareTraffic.Services.Reporting.Core
{
    public class ReportStatusNameNotFoundException : DomainException
    {
        IEnumerable<ReportStatus> ReportStatuses { get; }
        public string Name { get; }

        public ReportStatusNameNotFoundException(IEnumerable<ReportStatus> reportStatuses, string name)
            : base(new List<ErrorResult>() { new ErrorResult($"Possible values for ReportStatus Name: {String.Join(",", reportStatuses.Select(s => s.Name))}") }, 400, ResponseMessageType.Error)
        {
            ReportStatuses = reportStatuses;
            Name = name;
        }
    }
}
