﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Events.Infrastructure.Migrations
{
    public partial class AreaWorkingHours : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Areas",
                schema: "events",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    WorkingHoursStart = table.Column<TimeSpan>(type: "time", nullable: true),
                    WorkingHoursEnd = table.Column<TimeSpan>(type: "time", nullable: true),
                    AuditIsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Areas", x => x.Id);
                });

            migrationBuilder.Sql(@"INSERT INTO [events].[Areas]
                                               ([Id]
                                               ,[Name]
                                               ,[WorkingHoursStart]
                                               ,[WorkingHoursEnd]
                                               ,[AuditIsDeleted]
                                               ,[TenantId])
                                    SELECT [Id]
                                               ,[Name]
                                               ,[WorkingHoursStart]
                                               ,[WorkingHoursEnd]
                                               ,[AuditIsDeleted]
                                               ,[TenantId]
		                                       FROM [areas].[Areas]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Areas",
                schema: "events");
        }
    }
}
