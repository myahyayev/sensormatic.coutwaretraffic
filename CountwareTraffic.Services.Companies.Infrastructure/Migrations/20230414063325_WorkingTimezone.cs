﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountwareTraffic.Services.Companies.Infrastructure.Migrations
{
    public partial class WorkingTimezone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "WorkingTimeZone",
                schema: "areas",
                table: "Areas",
                type: "nvarchar(600)",
                maxLength: 600,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WorkingTimeZone",
                schema: "areas",
                table: "Areas");
        }
    }
}
