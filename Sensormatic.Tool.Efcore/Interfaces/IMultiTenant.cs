﻿using System;

namespace Sensormatic.Tool.Efcore
{
    public interface IMultiTenant : IInterceptor
    {
        public Guid TenantId { get; set; }
    }
}
