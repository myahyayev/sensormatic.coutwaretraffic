﻿using Sensormatic.Tool.Core;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Companies.Application
{
    public class CountryLookupNotFoundException : AppException
    {
        public Guid Id { get; }
        public CountryLookupNotFoundException(Guid id)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1403 CountryLookup with id: {id} was not found") }, 404, ResponseMessageType.Error)
        {
            Id = id;
        }
    }
}
