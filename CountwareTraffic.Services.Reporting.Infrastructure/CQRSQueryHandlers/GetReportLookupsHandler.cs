﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Reporting.Application;
using CountwareTraffic.Services.Reporting.Core;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.Infrastructure
{
    public class GetReportLookupsHandler : IQueryHandler<GetReportLookups, ReportLookupsDto>
    {
        public async Task<ReportLookupsDto> HandleAsync(GetReportLookups query)
        {
            ReportLookupsDto reportLookupsDto = new();

            reportLookupsDto.ReportFormats = ReportFormat.List()
                .Select(u => new Sensormatic.Tool.Core.EnumResponse
                {
                    IsVisible = true,
                    Key = u.Id,
                    Name = u.Name
                }).ToList();


            reportLookupsDto.ScheduleFrequencies = ScheduleFrequency.List()
               .Select(u => new Sensormatic.Tool.Core.EnumResponse
               {
                   IsVisible = true,
                   Key = u.Id,
                   Name = u.Name
               }).ToList();


            reportLookupsDto.ScheduleWeeklyRepeatOns = ScheduleWeeklyRepeatOn.List()
               .Select(u => new Sensormatic.Tool.Core.EnumResponse
               {
                   IsVisible = true,
                   Key = u.Id,
                   Name = u.Name
               }).ToList();


            reportLookupsDto.ScheduleDeliveryTimes = ScheduleDeliveryTime.List()
             .Select(u => new Sensormatic.Tool.Core.EnumResponse
             {
                 IsVisible = true,
                 Key = u.Id,
                 Name = u.Name
             }).ToList();

            return reportLookupsDto;
        }
    }
}
