﻿using Sensormatic.Tool.Core;
using System;
using System.Net;
using Sensormatic.Tool.Common;

namespace CountwareTraffic.Services.Devices.Grpc
{
    [ServiceLog]
    public sealed partial class CreateDeviceBrandRequest : SensormaticRequestValidate
    {
        public override void Validate()
        {
            if (Name.IsNullOrWhiteSpace())
                ValidateResults.Add(new ErrorResult($"#E1496 Name Value cannot be null", nameof(Name)));

            if (!Name.IsNullOrWhiteSpace() && Name.Length > 150)
                ValidateResults.Add(new ErrorResult($"#E1497 Name invalid format", nameof(Name)));
        }
    }
}


