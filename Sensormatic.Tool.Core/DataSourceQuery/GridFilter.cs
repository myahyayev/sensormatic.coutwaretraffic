﻿using System;
using System.Collections.Generic;

namespace Sensormatic.Tool.Core
{
    public class GridFilter
    {
        public FilterEnum Operator { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
        public List<Guid> GuidValues { get; set; }
        public List<string> StringValues { get; set; }
    }
    public enum FilterEnum
    {
        [EnumMemberInfo("Eşit", "Eşit", "C7D50FDC-621D-4D2A-8DE0-5A1B76889A12")]
        Eq = 1,

        [EnumMemberInfo("Eşit Değil", "Eşit Değil", "19247880-D6EC-4789-BA10-BFC3016F1075")]
        Neq = 2,

        [EnumMemberInfo("İle Başlayan", "İle Başlayan", "8ECEB750-FD1C-40D1-8BEB-1E5A29CF4583")]
        StartsWith = 3,

        [EnumMemberInfo("İçinde Geçen", "İçinde Geçen", "02E5ED4F-CB65-40C8-8E5C-2578498AB9F5")]
        Contains = 4,

        [EnumMemberInfo("İle Biten", "İle Biten", "F7D8D75D-A04C-4D46-A6C7-313F8A6FB308")]
        EndsWith = 5
    }
}
