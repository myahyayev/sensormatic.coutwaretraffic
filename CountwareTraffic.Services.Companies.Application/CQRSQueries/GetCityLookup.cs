﻿using Convey.CQRS.Queries;
using System;

namespace CountwareTraffic.Services.Companies.Application
{
    public class GetCityLookup : IQuery<CityLookupDetailsDto>
    {
        public Guid CityId { get; set; }
    }
}
