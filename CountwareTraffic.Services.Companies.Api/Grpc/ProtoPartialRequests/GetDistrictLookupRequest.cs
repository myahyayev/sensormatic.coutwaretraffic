﻿using Sensormatic.Tool.Core;
using System;

namespace CountwareTraffic.Services.Companies.Grpc
{
    public sealed partial class GetDistrictLookupRequest : SensormaticRequestValidate
    {
        internal Guid _DistrictId
        {
            get
            {
                if (Guid.TryParse(id_, out Guid id))
                    return id;

                return Guid.Empty;
            }
            set { this.id_ = value.ToString(); }
        }

        public override void Validate()
        {
            if (_DistrictId == Guid.Empty)
                ValidateResults.Add(new ErrorResult($"#E1320 DistrictId cannot be null", nameof(_DistrictId)));
        }
    }
}
