﻿using CountwareTraffic.Services.Events.Core;
using Sensormatic.Tool.ElasticSearch;
using System;

namespace CountwareTraffic.Services.Events.Infrastructure
{
    /// <summary>
    /// EventId is representing as Id
    /// </summary>
    public class EventCreateElasticData : ElasticSearchEntity, IMultiTenant
    {
        public EventCreateElasticData(Guid tenantId,Guid eventid)
        {
            Id = eventid;
            TenantId = tenantId;
        }

        //EventInfo
        public DirectionType DirectionType { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime EventStartDate { get; set; }
        public DateTime EventEndtDate { get; set; }
        //public int EnterCount { get; set; }
        //public int ExitCount { get; set; }
        public int OnTimeIOCount { get; set; }
        //public int CurrentCount { get; set; }

        //DeviceInfo
        public Guid DeviceId { get; set; }
        public string DeviceName { get; set; }
        public Guid SubAreaId { get; set; }
        public string SubAreaName { get; set; }
        public Guid AreaId { get; set; }
        public string AreaName { get; set; }
        public Guid CityId { get; set; }
        public string CityName { get; set; }
        public Guid DistrictId { get; set; }
        public string DistrictName { get; set; }
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }

        public Guid TenantId { get; set; }
    }
}
