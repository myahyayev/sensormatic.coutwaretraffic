﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Devices.Application;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Devices.Grpc
{
    [Authorize]
    public class DeviceService : Device.DeviceBase
    {
        private readonly ILogger<DeviceService> _logger;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        public DeviceService(ILogger<DeviceService> logger, ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }

        public async override Task<GetDeviceDetailResponse> GetDeviceById(GetDeviceRequest request, ServerCallContext context)
        {
            var device = await _queryDispatcher.QueryAsync(new GetDevice { DeviceId = request._DeviceId });

            var response = new GetDeviceDetailResponse();

            response.DeviceDetail = new DeviceDetail
            {
                Id = device.Id.ToString(),
                Name = device.Name,
                Description = device.Description,
                DeviceStatusId = device.DeviceStatusId,
                DeviceTypeId = device.DeviceTypeId,
                Identity = device.Identity,
                IpAddress = device.IpAddress,
                MacAddress = device.MacAddress,
                ModelId = device.ModelId.ToString(),
                BrandId = device.BrandId.ToString(),
                BrandName = device.BrandName,
                ModelName = device.ModelName,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.Firmware,
                IsActive = device.IsActive,
                Note = device.Note,
                Password = device.Password,
                Port = device.Port,
                SubAreaId = device.SubAreaId.ToString(),
                UniqueId = device.UniqueId,
                DeviceStatusName = device.DeviceStatusName,
                DeviceTypeName = device.DeviceTypeName,
                Audit = new Audit
                {
                    AuditCreateBy = device.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(device.AuditCreateDate),
                    AuditModifiedBy = device.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(device.AuditModifiedDate),
                }
            };
            return response;
        }
        [AllowAnonymous]
        public async override Task<DevicePagingResponse> GetDevices(GetDevicesRequest request, ServerCallContext context)
        {
            var pagingDevices = await _queryDispatcher.QueryAsync(new GetDevices
            {
                SubAreaId = request._SubAreaId,

                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Devices.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Devices.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Devices.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            DevicePagingResponse response = new()
            {
                TotalCount = pagingDevices.TotalCount,
                HasNextPage = pagingDevices.HasNextPage,
                Page = pagingDevices.Page,
                Limit = pagingDevices.Limit,
                Next = pagingDevices.Next,
                Prev = pagingDevices.Prev
            };

            pagingDevices.Data.ToList().ForEach(device => response.DeviceDetails.Add(new DeviceDetail
            {
                Id = device.Id.ToString(),
                Name = device.Name,
                Description = device.Description,
                DeviceStatusId = device.DeviceStatusId,
                DeviceTypeId = device.DeviceTypeId,
                Identity = device.Identity,
                IpAddress = device.IpAddress,
                MacAddress = device.MacAddress,
                ModelId = device.ModelId.ToString(),
                BrandId = device.BrandId.ToString(),
                BrandName = device.BrandName,
                ModelName = device.ModelName,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.Firmware,
                IsActive = device.IsActive,
                Note = device.Note,
                Password = device.Password,
                Port = device.Port,
                SubAreaId = device.SubAreaId.ToString(),
                UniqueId = device.UniqueId,
                DeviceStatusName = device.DeviceStatusName,
                DeviceTypeName = device.DeviceTypeName,
                Audit = new Audit
                {
                    AuditCreateBy = device.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(device.AuditCreateDate),
                    AuditModifiedBy = device.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(device.AuditModifiedDate),
                }
            }));
            return response;
        }

        public async override Task<CreateSuccessResponse> AddDevice(CreateDeviceRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new CreateDevice
            {
                SubAreaId = request._SubAreaId,
                Description = request.Description,
                DeviceTypeId = request.DeviceTypeId,
                Identity = request.Identity,
                IpAddress = request.IpAddress,
                MacAddress = request.MacAddress,
                ModelId = request._ModelId,
                BrandId = request._BrandId,
                ControlFrequency = request.ControlFrequency,
                Firmware = request.Firmware,
                Note = request.Note,
                Name = request.Name,
                Password = request.Password,
                Port = request.Port,
                UniqueId = request.UniqueId
            });

            return new CreateSuccessResponse { Created = "Created" };
        }

        public async override Task<UpdateSuccessResponse> ChangeDevice(UpdateDeviceRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UpdateDevice
            {
                DeviceId = request._DeviceId,
                Description = request.Description,
                DeviceTypeId = request.DeviceTypeId,
                Identity = request.Identity,
                IpAddress = request.IpAddress,
                MacAddress = request.MacAddress,
                ModelId = request._ModelId,
                BrandId = request._BrandId,
                ControlFrequency = request.ControlFrequency,
                Firmware = request.Firmware,
                IsActive = request.IsActive,
                Note = request.Note,
                Name = request.Name,
                Password = request.Password,
                Port = request.Port,
                UniqueId = request.UniqueId,
            });

            return new UpdateSuccessResponse { Updated = "Updated" };
        }

        public async override Task<DeleteSuccessResponse> DeleteDevice(DeleteDeviceRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new DeleteDevice { DeviceId = request._DeviceId });
            return new DeleteSuccessResponse { Deleted = "Deleted" };
        }

        public async override Task<GetDeviceStatusesResponse> GetDeviceStatuses(GetDeviceStatusesRequest request, ServerCallContext context)
        {
            var deviceStatuses = await _queryDispatcher.QueryAsync(new GetDeviceStatuses { });

            GetDeviceStatusesResponse response = new();

            deviceStatuses.ToList().ForEach(type => response.DeviceStatuses.Add(new DeviceStatus
            {
                Id = type.Id,
                Name = type.Name
            }));

            return response;
        }

        public async override Task<GetDeviceTypesResponse> GetDeviceTypes(GetDeviceTypesRequest request, ServerCallContext context)
        {
            var deviceTypes = await _queryDispatcher.QueryAsync(new GetDeviceTypes { });

            GetDeviceTypesResponse response = new();

            deviceTypes.ToList().ForEach(type => response.DeviceTypes.Add(new DeviceType
            {
                Id = type.Id,
                Name = type.Name
            }));

            return response;
        }

        public async override Task<DevicePagingResponse> GetDevicesWithoutParent(GetDevicesWithoutParentRequest request, ServerCallContext context)
        {
            var pagingDevices = await _queryDispatcher.QueryAsync(new GetDevices
            {
                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Devices.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Devices.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Devices.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            DevicePagingResponse response = new()
            {
                TotalCount = pagingDevices.TotalCount,
                HasNextPage = pagingDevices.HasNextPage,
                Page = pagingDevices.Page,
                Limit = pagingDevices.Limit,
                Next = pagingDevices.Next,
                Prev = pagingDevices.Prev
            };

            pagingDevices.Data.ToList().ForEach(device => response.DeviceDetails.Add(new DeviceDetail
            {
                Id = device.Id.ToString(),
                Name = device.Name,
                Description = device.Description,
                DeviceStatusId = device.DeviceStatusId,
                DeviceTypeId = device.DeviceTypeId,
                Identity = device.Identity,
                IpAddress = device.IpAddress,
                MacAddress = device.MacAddress,
                ModelId = device.ModelId.ToString(),
                BrandId = device.BrandId.ToString(),
                BrandName = device.BrandName,
                ModelName = device.ModelName,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.Firmware,
                IsActive = device.IsActive,
                Note = device.Note,
                Password = device.Password,
                Port = device.Port,
                SubAreaId = device.SubAreaId.ToString(),
                UniqueId = device.UniqueId,
                DeviceStatusName = device.DeviceStatusName,
                DeviceTypeName = device.DeviceTypeName,
                Audit = new Audit
                {
                    AuditCreateBy = device.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(device.AuditCreateDate),
                    AuditModifiedBy = device.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(device.AuditModifiedDate),
                }
            }));
            return response;
        }

        public async override Task<GetDeviceMoreInformationResponse> GetDeviceMoreInformation(GetDeviceMoreInformationRequest request, ServerCallContext context)
        {
            var deviceMoreInformation = await _queryDispatcher.QueryAsync(new GetDeviceMoreInformation { DeviceId = request._DeviceId });

            GetDeviceMoreInformationResponse response = new()
            {
                DeviceQueue = new DeviceQueue
                {
                    QueueModel = deviceMoreInformation.DeviceQueue.QueueModel,
                    QueueName = deviceMoreInformation.DeviceQueue.QueueName
                },
                DeviceEndpoint = new DeviceEndpoint
                {

                    EndpointAddress = deviceMoreInformation.DeviceEndpoint.EndpointAddress,
                    SampleRequest = deviceMoreInformation.DeviceEndpoint.SampleRequest
                },
                DeviceHierarchy = new DeviceHierarchy
                {
                    CompanyId = deviceMoreInformation.DeviceHierarchy.CompanyId.ToString(),
                    CompanyName = deviceMoreInformation.DeviceHierarchy.CompanyName,
                    CountryName = deviceMoreInformation.DeviceHierarchy.CountryName,
                    CountryId = deviceMoreInformation.DeviceHierarchy.CountryId.ToString(),
                    RegionId = deviceMoreInformation.DeviceHierarchy.RegionId.ToString(),
                    RegionName = deviceMoreInformation.DeviceHierarchy.RegionName,
                    AreaName = deviceMoreInformation.DeviceHierarchy.AreaName,
                    AreaId = deviceMoreInformation.DeviceHierarchy.AreaId.ToString(),
                    SubAreaName = deviceMoreInformation.DeviceHierarchy.SubAreaName,
                    SubAreaId = deviceMoreInformation.DeviceHierarchy.SubAreaId.ToString()
                }
            };

            return response;
        }

        public override async Task<GetHierarchyResponse> GetHierarchy(GetHierarchyRequest request, ServerCallContext context)
        {
            var hierarchies = await _queryDispatcher.QueryAsync(new GetDeviceHierarchy { Id = request._Id });

            GetHierarchyResponse response = new();

            hierarchies.ForEach(hierarchy => response.Hierarchies.Add(new Hierarchy
            {
                Id = hierarchy.Id.ToString(),
                Name = hierarchy.Name,
                HierarchyLevel = hierarchy.HierarchyLevel switch
                {
                    Core.HierarchyLevel.Tenant => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Tenant,
                    Core.HierarchyLevel.Company => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Company,
                    Core.HierarchyLevel.Country => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Country,
                    Core.HierarchyLevel.Region => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Region,
                    Core.HierarchyLevel.Area => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Area,
                    Core.HierarchyLevel.SubArea => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.SubArea,
                    Core.HierarchyLevel.Device => global::CountwareTraffic.Services.Devices.Grpc.HierarchyLevel.Device,
                    _ => throw new NotImplementedException(),
                }
            }));

            return response;
        }

        public override async Task<DevicePagingResponse> GetDevicesTreeSelectFilter(GetDeviceFilterRequest request, ServerCallContext context)
        {
            GetDevicesTreeSelect getDevicesTreeSelect = new()
            {
                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Devices.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Devices.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Devices.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Devices.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList(),

                NodeFilters = request.HierarchyTreeSelectFilters.Select(u => new Application.NodeFilter
                {
                    Id = new Guid(u.NodeFilter.Id),
                    HierarchyLevel = u.NodeFilter.HierarchyLevel switch
                    {
                        HierarchyLevel.Tenant => Core.HierarchyLevel.Tenant,
                        HierarchyLevel.Company => Core.HierarchyLevel.Company,
                        HierarchyLevel.Country => Core.HierarchyLevel.Country,
                        HierarchyLevel.Region => Core.HierarchyLevel.Region,
                        HierarchyLevel.Area => Core.HierarchyLevel.Area,
                        HierarchyLevel.SubArea => Core.HierarchyLevel.SubArea,
                        HierarchyLevel.Device => Core.HierarchyLevel.Device,
                        HierarchyLevel.Default => Core.HierarchyLevel.Tenant,
                        _ => Core.HierarchyLevel.Tenant,
                    }
                }).ToList()
            };

            var pagingDevices = await _queryDispatcher.QueryAsync(getDevicesTreeSelect);


            DevicePagingResponse response = new()
            {
                TotalCount = pagingDevices.TotalCount,
                HasNextPage = pagingDevices.HasNextPage,
                Page = pagingDevices.Page,
                Limit = pagingDevices.Limit,
                Next = pagingDevices.Next,
                Prev = pagingDevices.Prev
            };

            pagingDevices.Data.ToList().ForEach(device => response.DeviceDetails.Add(new DeviceDetail
            {
                Id = device.Id.ToString(),
                Name = device.Name,
                Description = device.Description,
                DeviceStatusId = device.DeviceStatusId,
                DeviceTypeId = device.DeviceTypeId,
                Identity = device.Identity,
                IpAddress = device.IpAddress,
                MacAddress = device.MacAddress,
                ModelId = device.ModelId.ToString(),
                BrandId = device.BrandId.ToString(),
                BrandName = device.BrandName,
                ModelName = device.ModelName,
                ControlFrequency = device.ControlFrequency,
                Firmware = device.Firmware,
                IsActive = device.IsActive,
                Note = device.Note,
                Password = device.Password,
                Port = device.Port,
                SubAreaId = device.SubAreaId.ToString(),
                UniqueId = device.UniqueId,
                DeviceStatusName = device.DeviceStatusName,
                DeviceTypeName = device.DeviceTypeName,
                Audit = new Audit
                {
                    AuditCreateBy = device.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(device.AuditCreateDate),
                    AuditModifiedBy = device.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(device.AuditModifiedDate),
                }
            }));

            return response;
        }

        public override async Task<GetDeviceBrandResponse> GetDeviceBrandById(GetDeviceBrandRequest request, ServerCallContext context)
        {
            var deviceBrand = await _queryDispatcher.QueryAsync(new GetBrandLookup {  BrandId = request._BrandId });

            var response = new GetDeviceBrandResponse();

            response.DeviceBrand = new DeviceBrand
            {
                Id = deviceBrand.Id.ToString(),
                Name = deviceBrand.Name,
            };
            return response;
        }

        public override async Task<GetDeviceBrandsResponse> GetDeviceBrands(Empty request, ServerCallContext context)
        {
            var deviceBrands = await _queryDispatcher.QueryAsync(new GetBrandLookups { });

            var response = new GetDeviceBrandsResponse();

            deviceBrands.ForEach(u => response.DeviceBrands.Add(new DeviceBrand
            {
                Id = u.Id.ToString(),
                Name = u.Name
            }));

            return response;
        }

        public override async Task<GetDeviceBrandModelResponse> GetDeviceBrandModelById(GetDeviceBrandModelRequest request, ServerCallContext context)
        {
            var deviceBrandModel = await _queryDispatcher.QueryAsync(new GetBrandModelLookup {  BrandModelId = request._BrandModelId });

            var response = new GetDeviceBrandModelResponse();

            response.DeviceBrandModel = new DeviceBrandModel
            {
                Id = deviceBrandModel.Id.ToString(),
                Name = deviceBrandModel.Name,
                BrandId = deviceBrandModel.BrandId.ToString()
            };
            return response;
        }

        public override async Task<GetDeviceBrandModelsResponse> GetDeviceBrandModels(GetDeviceBrandModelsRequest request, ServerCallContext context)
        {
            var deviceBrandModels = await _queryDispatcher.QueryAsync(new GetBrandModelLookups {   BrandId = request._BrandId });

            var response = new GetDeviceBrandModelsResponse();

            deviceBrandModels.ForEach(u => response.DeviceBrandModels.Add(new DeviceBrandModel
            {
                BrandId = u.BrandId.ToString(),
                Id = u.Id.ToString(),
                Name = u.Name
            }));

            return response;
        }

        public override async Task<CreateSuccessResponse> AddDeviceBrand(CreateDeviceBrandRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new CreateBrandLookup { Name = request.Name });
            return new CreateSuccessResponse { Created = "Created" };
        }

        public override async Task<CreateSuccessResponse> AddDeviceBrandModel(CreateDeviceBrandModelRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new CreateBrandModelLookup { Name = request.Name, BrandId = request._BrandId });
            return new CreateSuccessResponse { Created = "Created" };
        }

        public override async Task<UpdateSuccessResponse> ChangeDeviceBrand(UpdateDeviceBrandRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UpdateBrandLookup { Name = request.Name, BrandId = request._BrandId });

            return new UpdateSuccessResponse { Updated = "Updated" };
        }

        public override async Task<UpdateSuccessResponse>  ChangeDeviceBrandModel(UpdateDeviceBrandModelRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UpdateBrandModelLookup { Name = request.Name,  BrandModelId = request._BrandModelId });

            return new UpdateSuccessResponse { Updated = "Updated" };
        }

        public override async Task<DeleteSuccessResponse> DeleteDeviceBrand(DeleteDeviceBrandRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new DeleteBrandLookup {  BrandId = request._BrandId });
            return new DeleteSuccessResponse { Deleted = "Deleted" };
        }

        public override async Task<DeleteSuccessResponse> DeleteDeviceBrandModel(DeleteDeviceBrandModelRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new DeleteBrandModelLookup {  BrandModelId = request._BrandModelId });
            return new DeleteSuccessResponse { Deleted = "Deleted" }; ;
        }
    }
}
