﻿using System.Collections.Generic;
using System;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.IdentityServer
{
    public class AdminGridApiResponse
    {
        public List<AdminGridApiResponseItem> Data { get; set; }
        public int TotalCount { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
        public int HasNextPage { get; set; }
        public int Next { get; set; }
        public int Prev { get; set; }
    }

    public class AdminGridApiResponseItem
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        public bool Status { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public int Gender { get; set; }
        public DateTime Birthdate { get; set; }
        public int UserType { get; set; }
        public string Address { get; set; }
        public bool AllowSms { get; set; }
        public bool AllowEmail { get; set; }
        public string ProfilePic { get; set; }
        public List<AreaDetailApi> Areas { get; set; }
        public List<string> Roles { get; set; }
        public string Language { get; set; }
        public string Domain { get; set; }
    }
}
