﻿using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Infrastructure
{
    public class GetRegionKeyValueHandler : IQueryHandler<GetRegionKeyValue, List<KeyValueDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        public GetRegionKeyValueHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<KeyValueDto>> HandleAsync(GetRegionKeyValue query)
        {
            var regionRepository = _unitOfWork.GetRepository<IRegionRepository>();

            var keyValues = await regionRepository.GetAllKeyValueAsync(query.ParentId);

            return keyValues.Select(u => new KeyValueDto
            {
                Id = u.Id,
                Name = u.Name
            }).ToList();
        }
    }
}


