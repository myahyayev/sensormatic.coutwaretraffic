﻿using Convey.CQRS.Commands;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace CountwareTraffic.WorkerServices.SignalrHub.Application
{
    public class AreaDeletedFailedHandler : ICommandHandler<AreaDeletedFailed>
    {
        private readonly IHubContext<AreaNotificationsHub, IAreaNotificationsHub> _hubContext;
        public AreaDeletedFailedHandler(IHubContext<AreaNotificationsHub, IAreaNotificationsHub> hubContext)
            => _hubContext = hubContext;

        public async Task HandleAsync(AreaDeletedFailed command)
            => await _hubContext.Clients
                                        .Groups(command.TenantId.ToString())
                                        .AreaDeletedFailedReceived(command);
    }
}