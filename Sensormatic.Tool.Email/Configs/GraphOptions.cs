﻿using Sensormatic.Tool.Ioc;

namespace Sensormatic.Tool.Email
{

    public class GraphEmailOptions : IConfigurationOptions
    {
        public string? FromMailAddress { get; set; }
        public bool? IsReadReceiptRequested { get; set; }
        public bool? SaveSentItems { get; set; }
    }

}
