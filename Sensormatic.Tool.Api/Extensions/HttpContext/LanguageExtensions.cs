﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System.Linq;

namespace Sensormatic.Tool.Api
{
    public static class LanguageExtensions
    {
        public static string GetLanguage(this IHttpContextAccessor context)
        {
            var headers = context.HttpContext.Request.Headers;
            StringValues language;
            if (headers.TryGetValue("Content-Language", out language))
                return language.FirstOrDefault();
            else
                return "en-GB";
        }
    }
}
