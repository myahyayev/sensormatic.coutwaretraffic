﻿using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator
{
    public class GetTrafficByTimePeriodApiResponse
    {
        public IEnumerable<string> Categories { get; set; }
        public IEnumerable<Series> Series { get; set; }
    }

    public class Series
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public IEnumerable<int> Data { get; set; }
    }
}


