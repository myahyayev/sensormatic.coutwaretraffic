﻿using Sensormatic.Tool.Core;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Devices.Application
{
    public class DeviceAlreadyExistsException : AppException
    {
        public string IpAddress { get; }
        public string MacAddress { get; }

        public DeviceAlreadyExistsException(string ipAddress, string macAddress)
            : base(new List<ErrorResult>() { new ErrorResult($"#E1554 Device with ip: {ipAddress} and mac:{macAddress} already exists.") }, 409, ResponseMessageType.Error)
        {

            IpAddress = ipAddress;
            MacAddress = macAddress;
        }
    }
}
