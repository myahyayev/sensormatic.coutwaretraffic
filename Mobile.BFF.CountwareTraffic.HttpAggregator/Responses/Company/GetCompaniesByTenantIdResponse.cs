﻿using System;
using System.Collections.Generic;

namespace Mobile.BFF.CountwareTraffic.HttpAggregator.Responses.Company
{
    public class GetCompaniesByTenantIdResponse
    {
        public GetCompaniesByTenantIdResponse()
        {
            Data = new();
        }

        public List<GetCompaniesByTenantIdResponseItem> Data { get; set; }
    }

    public class GetCompaniesByTenantIdResponseItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
