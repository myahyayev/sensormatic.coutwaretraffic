﻿using Sensormatic.Tool.Ioc;
using System;
using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Application
{
    public interface IQueueEventMapper : ISingletonDependency
    {
        Sensormatic.Tool.Queue.IQueueEvent Map(Sensormatic.Tool.Efcore.IDomainEvent @event, string userName, Guid userId, string correlationId, Guid tenantId);
        List<Sensormatic.Tool.Queue.IQueueEvent> MapAll(IEnumerable<Sensormatic.Tool.Efcore.IDomainEvent> events, string userName, Guid userId, string correlationId, Guid tenantId);
    }
}
