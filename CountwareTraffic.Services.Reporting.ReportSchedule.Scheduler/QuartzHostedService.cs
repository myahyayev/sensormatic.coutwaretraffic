﻿using CountwareTraffic.Services.Reporting.Application;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Quartz;
using Quartz.Spi;
using Sensormatic.Tool.Common;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Reporting.ReportSchedule.Scheduler
{
    public class QuartzHostedService : BackgroundService
    {
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IJobFactory _jobFactory;
        private readonly IEnumerable<JobSchedule> _jobSchedules;
        private readonly ILogger<QuartzHostedService> _logger;
        private readonly TimeZoneSettings _timeZoneSettings;
        public QuartzHostedService(ILogger<QuartzHostedService> logger, ISchedulerFactory schedulerFactory, IEnumerable<JobSchedule> jobSchedules, IJobFactory jobFactory, IOptions<TimeZoneSettings> timeZoneSettings)
        {
            _schedulerFactory = schedulerFactory;
            _jobSchedules = jobSchedules;
            _jobFactory = jobFactory;
            _logger = logger;
            _timeZoneSettings = timeZoneSettings.Value;
        }

        public IScheduler Scheduler { get; set; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Scheduler = await _schedulerFactory.GetScheduler(stoppingToken);
            Scheduler.JobFactory = _jobFactory;

            foreach (var jobSchedule in _jobSchedules)
            {
                var job = jobSchedule.CreateJob();

                var trigger = jobSchedule.CreateTrigger(_timeZoneSettings);

                if (trigger != null)
                    await Scheduler.ScheduleJob(job, trigger, stoppingToken);
            }

            await Scheduler.Start(stoppingToken);
        }

        public async override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("QuartzHostedService forcing to stop...");

            await Scheduler?.Shutdown(cancellationToken);

            await Task.Delay(5000);

            await base.StopAsync(cancellationToken);
        }        
    }
}