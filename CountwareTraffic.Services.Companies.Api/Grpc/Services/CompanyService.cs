﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using CountwareTraffic.Services.Companies.Application.CQRSQueries;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Sensormatic.Tool.Common;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [Authorize]
    public class CompanyService : Company.CompanyBase
    {
        private readonly ILogger<CompanyService> _logger;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        public CompanyService(ILogger<CompanyService> logger, ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }

        public override async Task<GetCompanyDetailResponse> GetCompanyById(GetCompanyRequest request, ServerCallContext context)
        {
            var company = await _queryDispatcher.QueryAsync(new GetCompany { CompanyId = request._CompanyId });

            GetCompanyDetailResponse response = new();

            response.CompanyDetail = new CompanyDetail
            {
                Id = company.Id.ToString(),
                City = company.City,
                Country = company.Country,
                Description = company.Description,

                PhoneNumber = company.PhoneNumber,
                EmailAddress = company.EmailAddress,
                EmailAddressSecondary = company.EmailAddressSecondary,
                PhoneNumberSecondary = company.PhoneNumberSecondary,
                GsmDialCode = company.GsmDialCode,
                PhoneDialCodeSecondary = company.PhoneDialCodeSecondary,
                PhoneDialCode = company.PhoneDialCode,
                PersonNameSecondary = company.PersonNameSecondary,
                PersonName = company.PersonName,
                GsmDialCodeSecondary = company.GsmDialCodeSecondary,
                GsmNumberSecondary = company.GsmNumberSecondary,
                GsmNumber = company.GsmNumber,
                GsmCountryCode = company.GsmCountryCode,
                PhoneCountryCode = company.PhoneCountryCode,
                PhoneCountryCodeSecondary = company.PhoneCountryCodeSecondary,
                GsmCountryCodeSecondary = company.GsmCountryCodeSecondary,
                Latitude = company.Latitude,
                Longitude = company.Longitude,
                Name = company.Name,
                State = company.State,
                Street = company.Street,
                ZipCode = company.ZipCode,
                Audit = new Audit
                {
                    AuditCreateBy = company.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(company.AuditCreateDate),
                    AuditModifiedBy = company.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(company.AuditModifiedDate),
                }
            };

            return response;
        }

        public override async Task<CompanyPagingResponse> GetCompanies(GetCompaniesRequest request, ServerCallContext context)
        {
            var pagingCompanies = await _queryDispatcher.QueryAsync(new GetCompanies()
            {
                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });


            CompanyPagingResponse response = new()
            {
                TotalCount = pagingCompanies.TotalCount,
                HasNextPage = pagingCompanies.HasNextPage,
                Page = pagingCompanies.Page,
                Limit = pagingCompanies.Limit,
                Next = pagingCompanies.Next,
                Prev = pagingCompanies.Prev
            };

            pagingCompanies.Data.ToList().ForEach(company => response.CompanyDetails.Add(new CompanyDetail
            {
                Id = company.Id.ToString(),
                City = company.City,
                Country = company.Country,
                Description = company.Description,
                PhoneNumber = company.PhoneNumber,
                EmailAddress = company.EmailAddress,
                EmailAddressSecondary = company.EmailAddressSecondary,
                PhoneNumberSecondary = company.PhoneNumberSecondary,
                GsmDialCode = company.GsmDialCode,
                PhoneDialCodeSecondary = company.PhoneDialCodeSecondary,
                PhoneDialCode = company.PhoneDialCode,
                PersonNameSecondary = company.PersonNameSecondary,
                PersonName = company.PersonName,
                GsmDialCodeSecondary = company.GsmDialCodeSecondary,
                GsmNumberSecondary = company.GsmNumberSecondary,
                GsmCountryCode = company.GsmCountryCode,
                PhoneCountryCode = company.PhoneCountryCode,
                PhoneCountryCodeSecondary = company.PhoneCountryCodeSecondary,
                GsmCountryCodeSecondary = company.GsmCountryCodeSecondary,
                GsmNumber = company.GsmNumber,
                Latitude = company.Latitude,
                Longitude = company.Longitude,
                Name = company.Name,
                State = company.State,
                Street = company.Street,
                ZipCode = company.ZipCode,
                Audit = new Audit
                {
                    AuditCreateBy = company.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(company.AuditCreateDate),
                    AuditModifiedBy = company.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(company.AuditModifiedDate),
                }
            }));

            return response;
        }

        public override async Task<CreateSuccessResponse> AddCompany(CreateCompanyRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new CreateCompany
            {
                Name = request.Name,
                City = request.City,
                EmailAddress = request.EmailAddress,
                Country = request.Country,
                Description = request.Description,
                EmailAddressSecondary = request.EmailAddressSecondary,
                GsmDialCode = request.GsmDialCode,
                GsmNumberSecondary = request.GsmNumberSecondary,
                GsmDialCodeSecondary = request.GsmDialCodeSecondary,
                PersonName = request.PersonName,
                PersonNameSecondary = request.PersonNameSecondary,
                PhoneDialCode = request.PhoneDialCode,
                PhoneDialCodeSecondary = request.PhoneDialCodeSecondary,
                PhoneNumberSecondary = request.PhoneNumberSecondary,
                GsmNumber = request.GsmNumber,
                GsmCountryCode = request.GsmCountryCode,
                PhoneCountryCode = request.PhoneCountryCode,
                PhoneCountryCodeSecondary = request.PhoneCountryCodeSecondary,
                GsmCountryCodeSecondary = request.GsmCountryCodeSecondary,
                Latitude = request.Latitude.HasValue ? request.Latitude.Value : 0,
                Longitude = request.Longitude.HasValue ? request.Longitude.Value : 0,
                PhoneNumber = request.PhoneNumber,
                State = request.State,
                Street = request.Street,
                ZipCode = request.ZipCode
            });

            return new CreateSuccessResponse { Created = "Created" };
        }

        public override async Task<UpdateSuccessResponse> ChangeCompany(UpdateCompanyRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UpdateCompany
            {
                CompanyId = request._CompanyId,
                City = request.City,
                Country = request.Country,
                Description = request.Description,
                EmailAddress = request.EmailAddress,
                EmailAddressSecondary = request.EmailAddressSecondary,
                GsmDialCode = request.GsmDialCode,
                GsmNumberSecondary = request.GsmNumberSecondary,
                GsmDialCodeSecondary = request.GsmDialCodeSecondary,
                PersonName = request.PersonName,
                PersonNameSecondary = request.PersonNameSecondary,
                PhoneDialCode = request.PhoneDialCode,
                PhoneDialCodeSecondary = request.PhoneDialCodeSecondary,
                PhoneNumberSecondary = request.PhoneNumberSecondary,
                GsmNumber = request.GsmNumber,
                GsmCountryCode = request.GsmCountryCode,
                PhoneCountryCode = request.PhoneCountryCode,
                PhoneCountryCodeSecondary = request.PhoneCountryCodeSecondary,
                GsmCountryCodeSecondary = request.GsmCountryCodeSecondary,
                Latitude = request.Latitude.HasValue ? request.Latitude.Value : 0,
                Longitude = request.Longitude.HasValue ? request.Longitude.Value : 0,
                Name = request.Name,
                PhoneNumber = request.PhoneNumber,
                State = request.State,
                Street = request.Street,
                ZipCode = request.ZipCode,
            });

            return new UpdateSuccessResponse { Updated = "Updated" };
        }

        public override async Task<DeleteSuccessResponse> DeleteCompany(DeleteCompanyRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new DeleteCompany { CompanyId = request._CompanyId });
            return new DeleteSuccessResponse { Deleted = "Deleted" };
        }

        public override async Task<GetHierarchyResponse> GetHierarchy(GetHierarchyRequest request, ServerCallContext context)
        {
            var hierarchies = await _queryDispatcher.QueryAsync(new GetCompanyHierarchy { Id = request._Id });

            GetHierarchyResponse response = new();

            hierarchies.ForEach(hierarchy => response.Hierarchies.Add(new Hierarchy
            {
                Id = hierarchy.Id.ToString(),
                Name = hierarchy.Name,
                HierarchyLevel = hierarchy.HierarchyLevel switch
                {
                    Core.HierarchyLevel.Tenant => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Tenant,
                    Core.HierarchyLevel.Company => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Company,
                    Core.HierarchyLevel.Country => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Country,
                    Core.HierarchyLevel.Region => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Region,
                    Core.HierarchyLevel.Area => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Area,
                    Core.HierarchyLevel.SubArea => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.SubArea,
                    Core.HierarchyLevel.Device => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Device,
                    _ => throw new NotImplementedException(),
                }
            }));

            return response;
        }

        public override async Task<KeyValueResponse> GetCompaniesKeyValue(Empty request, ServerCallContext context)
        {
            var keyValues = await _queryDispatcher.QueryAsync(new GetCompanyKeyValue());

            KeyValueResponse response = new KeyValueResponse();

            keyValues.ForEach(u => response.KeyValues.Add(new KeyValue
            {
                Id = u.Id.ToString(),
                Name = u.Name
            }));

            return response;
        }

        public override async Task<GetCompaniesByTenantIdResponse> GetCompaniesByTenantId(GetCompaniesByTenantIdRequest request, ServerCallContext context)
        {
            var companies = await _queryDispatcher.QueryAsync(new GetCompaniesByTenantId
            {
                TenantId = request.TenantId.ToGuid()
            });

            var response = new GetCompaniesByTenantIdResponse();

            foreach (var company in companies.Data)
            {
                response.Data.Add(new GetCompaniesByTenantIdItem
                {
                    Id = company.Id.ToString(),
                    Name = company.Name
                });
            }

            return response;
        }
    }
}
