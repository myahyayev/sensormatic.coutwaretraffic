﻿using System.Collections.Generic;

namespace CountwareTraffic.Services.Reporting.Engine.CompherensiveTrafficReport.Models
{
    public class MonthlyCompherensiveTrafficReportData :BaseReportData
    {
        public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> Totals { get; set; }
        public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> TotalsByStore { get; set; }
        public BaseWrapper<List<ComparedStoreSummary>, ComparedStoreSummary> StoreSummary { get; set; }
    }
}
