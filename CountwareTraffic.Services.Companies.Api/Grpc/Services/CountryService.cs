﻿using Convey.CQRS.Commands;
using Convey.CQRS.Queries;
using CountwareTraffic.Services.Companies.Application;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CountwareTraffic.Services.Companies.Grpc
{
    [Authorize]
    public class CountryService : Country.CountryBase
    {
        private readonly ILogger<CountryService> _logger;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IQueryDispatcher _queryDispatcher;
        public CountryService(ILogger<CountryService> logger, ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _logger = logger;
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }
        public override async Task<GetCountryDetailResponse> GetCountryById(GetCountryRequest request, ServerCallContext context)
        {
            var country = await _queryDispatcher.QueryAsync(new GetCountry { CountryId = request._CountryId });

            GetCountryDetailResponse response = new();

            response.CountryDetail = new()
            {
                Id = country.Id.ToString(),
                CountryLookupId = country.CountryLookupId.ToString(),
                Name = country.Name,
                CompanyId = country.CompanyId.ToString(),
                Capital = country.Capital,
                ContinentCode = country.ContinentCode,
                CurrencyCode = country.CurrencyCode,
                Iso = country.Iso,
                Iso3 = country.Iso3,
                IsoNumeric = country.IsoNumeric,
                Audit = new Audit
                {
                    AuditCreateBy = country.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(country.AuditCreateDate),
                    AuditModifiedBy = country.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(country.AuditModifiedDate),
                }
            };
            return response;
        }

        public override async Task<CountryPagingResponse> GetCountries(GetCountriesRequest request, ServerCallContext context)
        {
            var pagingCountries = await _queryDispatcher.QueryAsync(new GetCountries
            {
                CompanyId = request._CompanyId,

                PagingQuery = new(request._DataSourceRequest.PagingRequest.Page, request._DataSourceRequest.PagingRequest.Limit),

                Sorts = request._DataSourceRequest.Sorts.Select(u => new Sensormatic.Tool.Core.SortDescriptor
                {
                    Field = u.Field,
                    Direction = u.Direction switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Asc => Sensormatic.Tool.Core.Direction.Asc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Desc => Sensormatic.Tool.Core.Direction.Desc,
                        global::CountwareTraffic.Services.Companies.Grpc.Direction.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException()
                    }
                }).ToList(),

                Filters = request._DataSourceRequest.Filters.Select(u => new Sensormatic.Tool.Core.GridFilter
                {
                    Field = u.Field,
                    Value = u.Value,
                    GuidValues = u.GuidValues.Select(u => new Guid(u)).ToList(),
                    StringValues = u.StringValues.Select(u => u).ToList(),
                    Operator = u.Operator switch
                    {
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Eq => Sensormatic.Tool.Core.FilterEnum.Eq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Neq => Sensormatic.Tool.Core.FilterEnum.Neq,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.StartsWith => Sensormatic.Tool.Core.FilterEnum.StartsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Contains => Sensormatic.Tool.Core.FilterEnum.Contains,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.EndsWith => Sensormatic.Tool.Core.FilterEnum.EndsWith,
                        global::CountwareTraffic.Services.Companies.Grpc.FilterEnum.Default => throw new System.NotImplementedException(),
                        _ => throw new System.NotImplementedException(),
                    },
                }).ToList()
            });

            CountryPagingResponse response = new()
            {
                TotalCount = pagingCountries.TotalCount,
                HasNextPage = pagingCountries.HasNextPage,
                Page = pagingCountries.Page,
                Limit = pagingCountries.Limit,
                Next = pagingCountries.Next,
                Prev = pagingCountries.Prev
            };

            pagingCountries.Data.ToList().ForEach(country => response.CountryDetails.Add(new CountryDetail
            {
                CountryLookupId = country.CountryLookupId.ToString(),
                Id = country.Id.ToString(),
                Name = country.Name,
                CompanyId = country.CompanyId.ToString(),
                Capital = country.Capital,
                ContinentCode = country.ContinentCode,
                CurrencyCode = country.CurrencyCode,
                Iso = country.Iso,
                Iso3 = country.Iso3,
                IsoNumeric = country.IsoNumeric,
                Audit = new Audit
                {
                    AuditCreateBy = country.AuditCreateBy.ToString(),
                    AuditCreateDate = Timestamp.FromDateTimeOffset(country.AuditCreateDate),
                    AuditModifiedBy = country.AuditModifiedBy.ToString(),
                    AuditModifiedDate = Timestamp.FromDateTimeOffset(country.AuditModifiedDate),
                }
            }));
            return response;
        }

        public override async Task<CreateSuccessResponse> AddCountry(CreateCountryRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new CreateCountry
            {
                CountryLookupId = request._CountryLookupId,
                CompanyId = request._CompanyId,
                Name = request.Name,
                Capital = request.Capital,
                IsoNumeric = request.IsoNumeric.HasValue ? request.IsoNumeric.Value : 0,
                ContinentCode = request.ContinentCode,
                CurrencyCode = request.CurrencyCode,
                Iso = request.Iso,
                Iso3 = request.Iso3
            });

            return new CreateSuccessResponse { Created = "Created" };
        }

        public override async Task<UpdateSuccessResponse> ChangeCountry(UpdateCountryRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new UpdateCountry
            {
                CountryLookupId = request._CountryLookupId,
                Name = request.Name,
                Capital = request.Capital,
                ContinentCode = request.ContinentCode,
                CountryId = request._CountryId,
                CurrencyCode = request.CurrencyCode,
                Iso = request.Iso,
                Iso3 = request.Iso3,
                CompanyId = request._CompanyId,
                IsoNumeric = request.IsoNumeric.HasValue ? request.IsoNumeric.Value : 0,
            });

            return new UpdateSuccessResponse { Updated = "Updated" };
        }

        public override async Task<DeleteSuccessResponse> DeleteCountry(DeleteCountryRequest request, ServerCallContext context)
        {
            await _commandDispatcher.SendAsync(new DeleteCountry {   CountryId = request._CountryId });
            return new DeleteSuccessResponse { Deleted = "Deleted" };
        }

        public override async Task<GetHierarchyResponse> GetHierarchy(GetHierarchyRequest request, ServerCallContext context)
        {
            var hierarchies = await _queryDispatcher.QueryAsync(new GetCountryHierarchy { Id = request._Id });

            GetHierarchyResponse response = new();

            hierarchies.ForEach(hierarchy => response.Hierarchies.Add(new Hierarchy
            {
                Id = hierarchy.Id.ToString(),
                Name = hierarchy.Name,
                HierarchyLevel = hierarchy.HierarchyLevel switch
                {
                    Core.HierarchyLevel.Tenant => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Tenant,
                    Core.HierarchyLevel.Company => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Company,
                    Core.HierarchyLevel.Country => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Country,
                    Core.HierarchyLevel.Region => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Region,
                    Core.HierarchyLevel.Area => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Area,
                    Core.HierarchyLevel.SubArea => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.SubArea,
                    Core.HierarchyLevel.Device => global::CountwareTraffic.Services.Companies.Grpc.HierarchyLevel.Device,
                    _ => throw new NotImplementedException(),
                }
            }));

            return response;
        }

        public override async Task<KeyValueResponse> GetCountriesKeyValue(KeyValueRequest request, ServerCallContext context)
        {
            var keyValues = await _queryDispatcher.QueryAsync(new GetCountryKeyValue() { ParentId = request._ParentId });

            KeyValueResponse response = new KeyValueResponse();

            keyValues.ForEach(u => response.KeyValues.Add(new KeyValue
            {
                Id = u.Id.ToString(),
                Name = u.Name
            }));

            return response;
        }
    }
}
